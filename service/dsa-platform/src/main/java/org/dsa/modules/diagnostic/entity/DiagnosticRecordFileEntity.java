package org.dsa.modules.diagnostic.entity;

import lombok.Data;
import lombok.NonNull;
import org.springframework.web.multipart.MultipartFile;

@Data
public class DiagnosticRecordFileEntity {

    /**
     * 会话Id
     */
    @NonNull
    private MultipartFile file;
    /**
     *功能码
     */
    @NonNull
    private String fileMd5;
}
