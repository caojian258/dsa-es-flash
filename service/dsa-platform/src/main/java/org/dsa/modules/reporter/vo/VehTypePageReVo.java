package org.dsa.modules.reporter.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.modules.reporter.pojo.PageParam;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class VehTypePageReVo extends PageParam {

    private String model;

    private String title;

}
