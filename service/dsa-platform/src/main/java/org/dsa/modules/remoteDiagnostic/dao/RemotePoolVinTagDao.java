//package org.dsa.modules.remoteDiagnostic.dao;
//
//import com.baomidou.mybatisplus.core.mapper.BaseMapper;
//import org.apache.ibatis.annotations.Insert;
//import org.apache.ibatis.annotations.Mapper;
//import org.apache.ibatis.annotations.Param;
//import org.apache.ibatis.annotations.Select;
//import org.dsa.modules.remoteDiagnostic.entity.RemotePoolVinTagEntity;
//import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuGroupEntity;
//import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuVersionEntity;
//
//import java.util.List;
//
//@Mapper
//public interface RemotePoolVinTagDao extends BaseMapper<RemotePoolVinTagEntity> {
//
//
//    @Insert("<script>"
//            + " insert into r_pool_vin_tag (pool_id,tag_id) values "
//            + " <foreach collection='list' item='item' separator=','> "
//            + " (#{poolId},#{item}) "
//            + " </foreach> "
//            + " </script> ")
//    void inserts(@Param("poolId") Long id, @Param("list") List<Long> list);
//
//    @Select("select i.tag_id from r_pool_vin_tag i where i.pool_id = #{poolId} group by i.tag_id ")
//    List<Long> getListByPool(@Param("poolId") Long id);
//
//}
