package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.dto.AppOptionDto;
import org.dsa.modules.reporter.entity.AppOverview;

import java.util.List;

@Mapper
public interface AppOverviewMapper extends BaseMapper<AppOverview> {

    AppOverview findByWorkshopPcidName(@Param("workshop") String workshop, @Param("pcid") String pcid, @Param("name") String name);


    AppOverview findByPcidAppId(@Param("pcid") String pcid, @Param("appId") Integer appId);

    List<String> getAppList();


    AppOverview findByName(@Param("workshop") String workshop, @Param("pcid") String pcid, @Param("name") String name);

    List<AppOverview> selectComponentVersion(@Param("appId") Integer appId, @Param("versionId") Integer versionId);

    List<AppOptionDto> queryAppOption();

    List<AppOptionDto> getAppNameListByPcid( @Param("pcid") String pcid);
}
