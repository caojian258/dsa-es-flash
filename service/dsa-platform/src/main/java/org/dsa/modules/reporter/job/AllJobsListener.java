package org.dsa.modules.reporter.job;

import lombok.extern.slf4j.Slf4j;
import org.dsa.modules.reporter.config.CronJobConfig;
import org.dsa.modules.reporter.config.ReportConfig;
import org.dsa.modules.reporter.dao.SchedulerJobMapper;
import org.dsa.modules.reporter.entity.ScheduleJob;
import org.dsa.modules.reporter.enums.JobStatusEnum;
import org.dsa.modules.reporter.service.ScheduleJobLogService;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;
import org.springframework.stereotype.Component;

import jakarta.annotation.Resource;
import java.util.List;

@Slf4j
@Component
public class AllJobsListener implements JobListener {

    @Resource
    ScheduleJobLogService logService;

    @Resource
    ReportConfig reportConfig;

    @Resource
    SchedulerJobMapper jobMapper;


    @Override
    public String getName() {
        return "AllJobsListener";
    }

    @Override
    public void jobToBeExecuted(JobExecutionContext jobExecutionContext) {
        JobDetail jobDetail =  jobExecutionContext.getJobDetail();

        String jobName = jobDetail.getKey().getName();
        String jobGroup = jobDetail.getKey().getGroup();
        String jobClass = jobDetail.getJobClass().getName();

        log.info("{} to be executed", jobName);
        String name = getCustomName(jobClass);
        ScheduleJob scheduleJob = jobMapper.findByJobName(jobName);
        if(null != scheduleJob){
            logService.insertLog(scheduleJob.getJobId(), scheduleJob.getJobName(), scheduleJob.getJobGroup(), JobStatusEnum.RUNNING.getValue(), null);
        }else {
            logService.insertLog(0L, jobName, name, JobStatusEnum.RUNNING.getValue(), null);
        }
    }

    @Override
    public void jobExecutionVetoed(JobExecutionContext jobExecutionContext) {
        JobDetail jobDetail =  jobExecutionContext.getJobDetail();
        log.info("{} to be Vetoed", jobDetail.getKey().getName());
        String jobClass = jobDetail.getJobClass().getName();
        String jobName = jobDetail.getKey().getName();
        String name = getCustomName(jobClass);
        ScheduleJob scheduleJob = jobMapper.findByJobName(jobName);
        if(null != scheduleJob){
            logService.insertLog(scheduleJob.getJobId(), scheduleJob.getJobName(), scheduleJob.getJobGroup(), JobStatusEnum.VETOED.getValue(), null);
        }else {
            logService.insertLog(0L, jobName, name, JobStatusEnum.VETOED.getValue(), null);
        }
    }

    @Override
    public void jobWasExecuted(JobExecutionContext jobExecutionContext, JobExecutionException e) {
        JobDetail jobDetail =  jobExecutionContext.getJobDetail();
        String jobName = jobDetail.getKey().getName();
        log.info("{} Executed", jobDetail.getKey().getName());
        log.info("JobClass {} Executed", jobDetail.getJobClass().getName());
        log.info("JobGroup {} Executed",   jobDetail.getKey().getGroup());
        String jobClass = jobDetail.getJobClass().getName();
        String name = getCustomName(jobClass);
        ScheduleJob scheduleJob = jobMapper.findByJobName(jobName);
        if(null != scheduleJob){
            logService.insertLog(scheduleJob.getJobId(), scheduleJob.getJobName(), scheduleJob.getJobGroup(), JobStatusEnum.EXECUTED.getValue(), null);
        }else {
            logService.insertLog(0L, jobName, name, JobStatusEnum.EXECUTED.getValue(), null);
        }
    }

    protected String getCustomName(String className){
        List<CronJobConfig> jobList =  reportConfig.getJobs();
        CronJobConfig jobConfig =  jobList.stream()
                .filter(job -> className.equals(job.getClassName()))
                .findAny()
                .orElse(null);
        if(null != jobConfig){
            return jobConfig.getName();
        }
        return null;
    }
}
