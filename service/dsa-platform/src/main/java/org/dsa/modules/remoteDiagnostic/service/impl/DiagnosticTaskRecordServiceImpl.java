package org.dsa.modules.remoteDiagnostic.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.remoteDiagnostic.dao.DiagnosticTaskOverviewDao;
import org.dsa.modules.remoteDiagnostic.dao.DiagnosticTaskRecordDao;
import org.dsa.modules.remoteDiagnostic.dto.page.DiagnosticTaskRecordDto;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskRecordEntity;
import org.dsa.modules.remoteDiagnostic.service.DiagnosticTaskRecordService;
import org.dsa.modules.remoteDiagnostic.vo.PublicPageReqVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DiagnosticTaskRecordServiceImpl implements DiagnosticTaskRecordService {
    @Autowired
    private DiagnosticTaskOverviewDao taskOverviewDao;
    @Autowired
    private DiagnosticTaskRecordDao taskRecordDao;

    @Override
    public Page<DiagnosticTaskRecordDto> selectPage(PublicPageReqVo vo) {
        Page<DiagnosticTaskRecordEntity> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);
//        LambdaQueryWrapper<DiagnosticTaskRecordEntity> qw = Wrappers.<DiagnosticTaskRecordEntity>lambdaQuery();
//        if (StringUtils.isNotBlank(vo.getName())) {
//            qw.apply("vin"+" ilike {0}", "%"+vo.getName().trim()+"%");
//        }
        return taskRecordDao.selectPage(page, vo.getName(), vo.getId(), vo.getType());
    }

    @Override
    public DiagnosticTaskRecordEntity getInfo(Long id) {
        return null;
    }

    @Override
    public void saveTask(DiagnosticTaskRecordEntity task) {

    }

    @Override
    public void updateTask(DiagnosticTaskRecordEntity task) {

    }

    @Override
    public void updateTask(String sessionId, String vin) {

    }

    @Override
    public List<DiagnosticTaskRecordEntity> getTasks() {
        return null;
    }
}
