package org.dsa.modules.reporter.vo.client;

import lombok.Data;

import jakarta.validation.constraints.NotNull;
import java.util.Map;

/***
 * 图表预览请求
 */
@Data
public class ClientChartReVo {

    /***
     * 图表唯一标识
     */
    private String code;

    /***
     * 数据类型
     */
    @NotNull(message = "数据类型不能为空")
    private String category;

    /***
     * 维度
     */
    @NotNull(message = "数据维度不能为空")
    private String dimension;

    /***
     * 图表类型
     */
    @NotNull(message = "图表类型不能为空")
    private String chart;

    /***
     * 最后一次在线时间 单位天
     */
    private String startDate;

    /***
     * 渠道分组
     */
    private String workshopGroup;

    /***
     * 条件
     */
    private Map<String, Object> condition;
}
