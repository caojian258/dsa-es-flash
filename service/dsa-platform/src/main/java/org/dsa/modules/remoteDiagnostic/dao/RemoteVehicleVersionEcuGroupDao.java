package org.dsa.modules.remoteDiagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuGroupEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleVersionEcuGroupEntity;

import java.util.List;

@Mapper
public interface RemoteVehicleVersionEcuGroupDao extends BaseMapper<RemoteVehicleVersionEcuGroupEntity> {



    @Insert("<script>"
            + " insert into r_vehicle_version_ecu_group (version_id,group_id,ecu_id,ecu_version_id) values "
            + " <foreach collection='ecus' item='item' separator=','> "
            + " (#{versionId},#{item.id},#{item.vehicleEcuGroupVersion.ecuId},#{item.vehicleEcuGroupVersion.ecuVersionId}) "
            + " </foreach>"
            + " </script>")
    void inserts(@Param("versionId") Long id, @Param("ecus") List<RemoteVehicleEcuGroupEntity> ecus);

    @Insert("<script>"
            + " insert into r_vehicle_version_ecu_group (version_id,group_id,ecu_id,ecu_version_id) values "
            + " <foreach collection='ecus' item='item' separator=','> "
            + " (#{versionId},#{item.groupId},#{item.ecuId},#{item.ecuVersionId}) "
            + " </foreach>"
            + " </script>")
    void inserts2(@Param("versionId") Long id, @Param("ecus") List<RemoteVehicleVersionEcuGroupEntity> ecus);

//    @Select(" select i.id,i.version_id,i.ecu_id,i.ecu_version_id,i.group_id from r_vehicle_version_ecu_group i where i.version_id = #{versionId} and i.group_id = #{groupId}" +
//            " union " +
//            " select 0 as id,null as version_id,null as ecu_id,null as ecu_version_id,null as group_id  where NOT EXISTS(select 1 from r_vehicle_version_ecu_group i where i.version_id = #{versionId} and i.group_id = #{groupId}) ")
//    @Select("select 0 as id ")
    RemoteVehicleVersionEcuGroupEntity getInfoByGroup(@Param("typeId") Long id, @Param("groupId") Long groupId);

//    @Select(" select i.id,i.version_id,i.ecu_id,i.ecu_version_id,i.group_id,i.group_id as groupId from r_vehicle_version_ecu_group i where i.version_id = #{versionId}")
//    @Results({
//            @Result(property = "ecuGroup", column = "groupId",
//                    one = @One(select = "org.dsa.modules.remoteDiagnostic.dao.RemoteVehicleEcuGroupDao.getInfo")),
//    })
    RemoteVehicleVersionEcuGroupEntity getInfoByVersion(@Param("versionId") Long id);

    List<RemoteVehicleVersionEcuGroupEntity> getListByType(@Param("typeId") Long typeId);

    List<RemoteVehicleVersionEcuGroupEntity> getListByTypes(@Param("typeIds") List<Long> typeId);

}
