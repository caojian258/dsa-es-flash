package org.dsa.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.sys.entity.SysUserEntity;

import java.util.List;
import java.util.Map;

/**
 * 系统用户
 *
 */
@Mapper
public interface SysUserDao extends BaseMapper<SysUserEntity> {
	
	/**
	 * 查询用户的所有权限
	 * @param userId  用户ID
	 */
	List<String> queryAllPerms(Long userId);
	
	/**
	 * 查询用户的所有菜单ID
	 */
	List<Long> queryAllMenuId(Long userId);
	
	/**
	 * 根据用户名，查询系统用户
	 */
	SysUserEntity queryByUserName(String username);

	/**
	 * 查找变更人员组信息
	 */
	SysUserEntity getUserGroupInfo(@Param("userId") Long userId);


	/**
	 * 根据用户ID，查询系统用户
	 * @param userId
	 * @return
	 */
	SysUserEntity queryByUserId(Long userId);


	/**
	 * 取用户列表
	 * @return
	 */
	List<SysUserEntity> getUserList();

    List<String> queryUserByRole(int roleId ,Long workshopId, Long groupId);

	List<SysUserEntity> getUserList(@Param("chineseName") String chineseName,
									@Param("roleList") List<String> roleList,
									@Param("workshopName") String workshopName);

    IPage<SysUserEntity> selectUserList(IPage<SysUserEntity> page , @Param("username") String username);

	IPage<SysUserEntity> selectUserListByNameAndCreateId(IPage<SysUserEntity> page , @Param("username") String username,@Param("createUserId") Long createUserId,@Param("workShopName") String workShopName);
    IPage<SysUserEntity> selectPage2(IPage<SysUserEntity> page,@Param("workshop")String workshop ,@Param("username") String username,@Param("userIds") List<Integer> userIds);

    List<SysUserEntity> selectRuleUserList(@Param("workshop")String workshop , @Param("username") String username, @Param("userIds") List<Integer> userIds);

	void removeNotInIds(List<String> userIdList, String workshopId);

	void saveUserInfo(SysUserEntity sysUserEntity);

    List<String> queryUserNameByRole(int roleId, Long workshopId, Long groupId);

    List<SysUserEntity> queryUserEntityByRole(int roleId, Long workshopId,Long parentId,Long groupId);

    List<String> queryPermissionsByRole(@Param("roleList") List<String> roles, @Param("owningSystem") Integer owningSystem);

    void disableByIds(@Param("userIdList") List<Long> userIdList);

    List<Long> queryUserIdByWorkshopId(long workshopId);

	void enableByIds(@Param("userIdList") List<Long> userIdList);

    void insertBatch(@Param("userList") List<SysUserEntity> userList);

	List<Map<String, Object>> workshopList(@Param("name") String name);
}
