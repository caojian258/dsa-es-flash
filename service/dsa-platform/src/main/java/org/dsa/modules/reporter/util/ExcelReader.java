package org.dsa.modules.reporter.util;

/***
 * excel reader by easyexcel
 */
public class ExcelReader {

    /***
     * 文件路径
     */
    private String path;

    /***
     * 所用时间
     */
    private long spendTime;

    public ExcelReader(){

    }

    public ExcelReader(String path){
        this.path = path;
    }

}
