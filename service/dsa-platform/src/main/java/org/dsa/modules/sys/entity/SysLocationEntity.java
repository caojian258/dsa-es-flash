package org.dsa.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("sys_location")
public class SysLocationEntity implements Serializable {

    /***
     * 主键ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /***
     * 编码
     */
    private Long code;

    /***
     * 名称
     */
    private String name;

    /***
     * 上级ID
     */
    private Long pid;

    /***
     * 级别 0国家 1身份 2城市 3 区县
     */
    private String type;
}
