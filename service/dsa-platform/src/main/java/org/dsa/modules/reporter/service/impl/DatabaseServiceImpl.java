package org.dsa.modules.reporter.service.impl;

import cn.hutool.core.date.DateUtil;
import org.dsa.modules.reporter.config.PartitionConfig;
import org.dsa.modules.reporter.dao.AnalysisCubeMapper;
import org.dsa.modules.reporter.service.DatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class DatabaseServiceImpl implements DatabaseService {

    @Lazy
    @Autowired
    AnalysisCubeMapper cubeMapper;


    @Autowired
    PartitionConfig config;

    @Override
    public void createPartitionTable() {

        //诊断客户端
        if(!StringUtils.isEmpty(config.getClient().get("table"))){
            List<String> tableList = Arrays.asList(config.getClient().get("table").split(","));

            List<String> monthList = partitionRanges();
            for (String table: tableList){
                String sourceTable = table;
                for (String month: monthList){
                    String targetTable = table + "_" + month;
                    cubeMapper.createPartitionTable(targetTable, config.getClient().get("column"),"'" + month + "'", sourceTable);
                }
            }
        }

        //诊断历史数据
        if(!StringUtils.isEmpty(config.getDiag().get("table"))){
            List<String> tableList = Arrays.asList(config.getDiag().get("table").split(","));
            List<String> monthList = partitionRanges();
            for (String table: tableList){
                String sourceTable = table;
                for (String month: monthList){
                    String targetTable = table + "_" + month;
                    cubeMapper.createPartitionTable(targetTable, config.getDiag().get("column"),"'" + month + "'", sourceTable);
                }
            }
        }

        //诊断数据 原始数据
        if(!StringUtils.isEmpty(config.getRaw().get("table"))){
            List<String> tableList = Arrays.asList(config.getRaw().get("table").split(","));
            List<String> monthList = partitionRanges();
            for (String table: tableList){
                String sourceTable = table;
                for (String month: monthList){
                    String targetTable = table + "_" + month;
                    cubeMapper.createPartitionTable(targetTable, config.getRaw().get("column"),"'" + month + "'", sourceTable);
                }
            }

            for (String month: monthList){
                String targetTable =  "raw_diagnostic_" + month;
                cubeMapper.createPartitionTable(targetTable, "start_time","'" + month + "'", "raw_diagnostic");
            }
        }

    }

    @Override
    public List<String> partitionRanges(){
        List<String> months = new ArrayList<>();
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");

        try {
            Date startDate = sdf.parse(year + "01");
            Date endDate = sdf.parse(year + "12");
            c.setTime(startDate);
            // 当前时间小于等于设定的结束时间
            while(c.getTime().compareTo(endDate) <= 0){
                String time = sdf.format(c.getTime());
                // 打印日期
                months.add(time);
                // 当前月份加1
                c.add(Calendar.MONTH, 1);
            }
            months.add(sdf.format(endDate));

        }catch (Exception ex){

        }

        return months;
    }

    public Map<String, Map<String,String>> partitionRangesMap() {
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
        SimpleDateFormat sdfDay = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdfDay1 = new SimpleDateFormat("yyyy-MM-01");
        Map<String, Map<String,String>> rangeMap = new HashMap<>();

        try {
            Date startDate = sdf.parse(year + "01");
            Date endDate = sdf.parse(year + "12");
            c.setTime(startDate);
            // 当前时间小于等于设定的结束时间
            while(c.getTime().compareTo(endDate) <= 0){
                String time = sdf.format(c.getTime());
                Map<String,String> map = new HashMap<>();
                Calendar cc2 = DateUtil.endOfMonth(c);
                String startDay = sdfDay1.format(cc2.getTime());
                String endDay = sdfDay.format(cc2.getTime());
                map.put("startDay", startDay);
                map.put("endDay", endDay);
                rangeMap.put(time, map);
                // 当前月份加1
                c.add(Calendar.MONTH, 1);
            }

            Map<String,String> map = new HashMap<>();

            Date endDateD = DateUtil.endOfMonth(endDate);

            String startDay = sdfDay1.format(endDateD);
            String endDay = sdfDay.format(endDateD);
            map.put("startDay", startDay);
            map.put("endDay", endDay);

            rangeMap.put(sdf.format(endDateD), map);

        }catch (Exception ex){

        }

        return rangeMap;
    }

}
