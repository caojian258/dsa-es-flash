package org.dsa.modules.remoteDiagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.remoteDiagnostic.dto.api.DiagnosticConditionDto;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticConditionCopyEntity;

import java.util.List;

@Mapper
public interface DiagnosticConditionCopyDao extends BaseMapper<DiagnosticConditionCopyEntity> {


    @Insert("<script>"
            + " insert into r_task_condition (raw_id,task_id,condition_name,condition_code,condition_val,unity,min_val,max_val,eq_val,in_val) values "
            + " <foreach collection='list' item='item' separator=','> "
            + " (#{item.rawId},#{taskId},#{item.conditionName},#{item.conditionCode},#{item.conditionVal},#{item.unity},#{item.minVal},#{item.maxVal},#{item.eqVal},#{item.inVal}) "
            + " </foreach>"
            + " </script>")
    void inserts(@Param("taskId") Long taskId,@Param("list") List<DiagnosticConditionCopyEntity> list);

    List<DiagnosticConditionCopyEntity> selectConditions(@Param("taskId") Long taskId);

    List<DiagnosticConditionDto> getConditionList(@Param("taskId") Long taskId);

}
