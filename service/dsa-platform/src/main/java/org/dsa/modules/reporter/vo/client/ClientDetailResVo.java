package org.dsa.modules.reporter.vo.client;

import lombok.Data;

import java.util.Date;

@Data
public class ClientDetailResVo {

    /**
     * 经销商ID
     */
    private String workshop;

    /***
     * 客户端ID
     */
    private String pcid;

    /**
     * 应用程序语言
     */
    private String applicationLocale;

    /***
     * 是否使用代理
     */
    private String proxyFlag;

    /***
     * 是否开启防火墙
     */
    private String firewallFlag;

    /***
     * 杀毒软件
     */
    private String antivirInstalled;

    /***
     * 杀毒软件标记
     */
    private String antivirFlag;

    /***
     * SmartStart版本
     */
    private String smartstartVersion;

    /***
     *车企品牌
     */
    private String brand;

    /***
     * window 版本
     */
    private String windowsVersion;

    /***
     * 计算机型号
     */
    private String windowsId;

    /***
     * 内存使用大小
     */
    private Long ramUsed;

    /***
     * 内存
     */
    private Long ramTotal;

    /***
     * 磁盘系列号
     */
    private String diskSerial;

    /***
     * 下载速度
     */
    private String downloadSpeed;

    /**
     * 磁盘合计大小
     */
    private Long diskTotal;

    /***
     * 磁盘使用大小
     */
    private Long diskUsed;

    /***
     * 系统语言
     */
    private String systemLocale;

    /***
     * 局域网IP
     */
    private String localIp;

    /***
     * wlan标记
     */
    private String wlanFlag;

    /***
     * CPU型号
     */
    private String cpuSerial;

    private String cpuArch;

    private String cpuInfo;

    /**
     * 显示器分辨率
     */
    private String desktopResolution;

    /***
     * CPU 处理器
     */
    private String processors;

    /***
     * 上传时间
     */
    private String timestampText;

    /***
     * 上传时间戳
     */
    private Long timestamp;

    /***
     * 最近一次在线时间
     */
    private Date lastOnlineTime;

    /***
     * 最近一次的登录人
     */
    private String lastUsername;

    private String tag;

    private String workshopName;

    private String workshopAddress;

    private String workshopGroup;

    private String workshopCountry;
}
