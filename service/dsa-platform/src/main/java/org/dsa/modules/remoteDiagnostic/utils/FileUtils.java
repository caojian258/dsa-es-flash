package org.dsa.modules.remoteDiagnostic.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuVersionFileEntity;

import java.io.*;
import java.util.List;
import java.util.zip.Adler32;
import java.util.zip.CheckedOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Slf4j
public class FileUtils {


    public static void zipFile(List<File> fileList,String path,String name) throws IOException {
        // 文件的压缩包路径
        String zipPath = path + File.pathSeparator + name;
        // 获取文件压缩包输出流
        try (OutputStream outputStream = new FileOutputStream(zipPath);
             CheckedOutputStream checkedOutputStream = new CheckedOutputStream(outputStream,new Adler32());
             ZipOutputStream zipOut = new ZipOutputStream(checkedOutputStream)){
            for (File file : fileList) {
                // 获取文件输入流
                InputStream fileIn = new FileInputStream(file);
                // 使用 common.io中的IOUtils获取文件字节数组
                byte[] bytes = IOUtils.toByteArray(fileIn);
                // 写入数据并刷新
                zipOut.putNextEntry(new ZipEntry(file.getName()));
                zipOut.write(bytes,0,bytes.length);
                zipOut.flush();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void zipFile(List<RemoteVehicleEcuVersionFileEntity> fileList, String path) throws IOException {
        // path : 文件的压缩包路径
        // 获取文件压缩包输出流
        try (OutputStream outputStream = new FileOutputStream(path);
             CheckedOutputStream checkedOutputStream = new CheckedOutputStream(outputStream,new Adler32());
             ZipOutputStream zipOut = new ZipOutputStream(checkedOutputStream)){
            for (RemoteVehicleEcuVersionFileEntity file : fileList) {
                // 获取文件输入流
                InputStream fileIn = new FileInputStream(file.getFilePath());
                // 使用 common.io中的IOUtils获取文件字节数组
                byte[] bytes = IOUtils.toByteArray(fileIn);
                // 写入数据并刷新
                zipOut.putNextEntry(new ZipEntry(file.getFileName()));
                zipOut.write(bytes,0,bytes.length);
                zipOut.flush();
            }
        }
    }
}
