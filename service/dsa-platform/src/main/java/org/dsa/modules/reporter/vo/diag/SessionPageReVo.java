package org.dsa.modules.reporter.vo.diag;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.modules.reporter.pojo.PageParam;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SessionPageReVo extends PageParam {

    private List<Long> vehType;

    private List<String> dates;

    private String vin;

    private Integer userId;

    private Integer diagCategory;

    private String diagSource;

    private String ctsName;

    private String dtcCode;

    private String startTime;

    private String endTime;
}
