package org.dsa.modules.remoteDiagnostic.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.dsa.modules.remoteDiagnostic.dto.flash.EcuVersionFileDto;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuVersionFileEntity;

import java.util.List;

@Mapper
public interface RemoteVehicleEcuVersionFileDao extends BaseMapper<RemoteVehicleEcuVersionFileEntity> {

    @Insert("<script>"
            + " insert into r_ecu_version_file (version_id,file_type,file_name,file_path,file_md5,flash_type,status) values "
            + " <foreach collection='files' item='item' separator=','> "
            + " (#{versionId},#{item.fileType},#{item.fileName},#{item.filePath},#{item.fileMd5},#{item.flashType},#{item.status}) "
            + " </foreach>"
            + " </script>")
    void inserts(@Param("versionId") Long id,@Param("files") List<RemoteVehicleEcuVersionFileEntity> files);

    @Select("select * from r_ecu_version_file i where i.version_id = #{id}")
    List<RemoteVehicleEcuVersionFileEntity> getList(@Param("versionId") Long id);


    List<EcuVersionFileDto> findFlashFiles(@Param("ecuVersionIds") List<Long> ids, @Param("fileType") String fileType);

    List<EcuVersionFileDto> findEcuFile(@Param("ecuVersionId") Long ecuVersionId, @Param("status") Integer status, @Param("fileType") String fileType);


    EcuVersionFileDto findEcuVersionId(@Param("ecuVersionId") Long ecuVersionId, @Param("status") Integer status, @Param("fileType") String fileType);

    List<EcuVersionFileDto> findEcuFileById(@Param("ecuVersionId") Long ecuVersionId, @Param("fileType") String fileType);
}
