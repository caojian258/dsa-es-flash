package org.dsa.modules.reporter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
@TableName("client_future_component_info")
public class FutureComponentInfo extends BaseEntity{

    @TableId(type = IdType.AUTO)
    private Long id;

    private String deviceId;

    private Integer softwareId;
    private Integer componentId;
    private String componentVersionName;
    private Integer componentVersionNumber;
    private String componentDownloadPath;
    /***
     强制更新： 1=force
     非强制更新： 0=optional
     */
    private Integer updateType;
    private String releaseNotes;
    private String featureDescription;
    private String fileSize;
    private String fileMd5;
    private String tag;
    private String sessionId;
    private Integer orderNumber;
    private Integer downloadProgress;
    private String componentDownloadUrl;

}
