package org.dsa.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@TableName("sys_internationalization")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SysInternationalizationEntity {

    private String key;

    private String value;

    private String local;

    private String unit;
}
