package org.dsa.modules.sys.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.metadata.data.ReadCellData;
import com.alibaba.excel.util.ConverterUtils;
import com.alibaba.excel.util.ListUtils;
import com.alibaba.excel.util.StringUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.ObjectUtils;
import org.dsa.modules.sys.dao.SysLogDao;
import org.dsa.modules.sys.dao.SysTranslateDao;
import org.dsa.modules.sys.entity.SysLocaleEntity;
import org.dsa.modules.sys.entity.SysLogEntity;
import org.dsa.modules.sys.entity.SysTranslateEntity;
import org.dsa.modules.sys.service.SysLocaleService;
import org.dsa.modules.sys.utils.SpringContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;


/**
 * 直接用map接收数据
 * 多语言excel导入数据拦截
 *
 * @author Jiaju Zhuang
 */
@Slf4j
public class MultiLanguageExcelImportListener extends AnalysisEventListener<Map<Integer, String>> {
    /**
     * 每隔5条存储数据库，实际使用中可以100条，然后清理list ，方便内存回收
     */
    private static final int BATCH_COUNT = 100;
    private static final int SAVE_LIMIT = 500;

    private String path;
    private List<Map<Integer, String>> cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
    private Map<Integer, String> headsMap = null;


    public MultiLanguageExcelImportListener(String path) {
        this.path = path;
    }

    /**
     * 这里会一行行的返回头
     *
     * @param headMap
     * @param context
     */
    @Override
    public void invokeHead(Map<Integer, ReadCellData<?>> headMap, AnalysisContext context) {
        log.info("解析到一条头数据:{}", JSON.toJSONString(headMap));
        headsMap = ConverterUtils.convertToStringMap(headMap, context);
        //获取语种数组,2-size()
        //表头数据校验
        if (CollectionUtils.isEmpty(headsMap)) {
            // 抛出表头数据读取异常
            throw new RRException("excel表头设置异常");
        }
        if (headsMap.size() < 2) {
            throw new RRException("至少设置一种语言");
        }
        // 如果想转成成 Map<Integer,String>
        // 方案1： 不要implements ReadListener 而是 extends AnalysisEventListener
        // 方案2： 调用 ConverterUtils.convertToStringMap(headMap, context) 自动会转换
    }

    @Override
    public void invoke(Map<Integer, String> data, AnalysisContext context) {
//        log.info("解析到一条数据:{}", JSON.toJSONString(data));
        cachedDataList.add(data);
//        if (cachedDataList.size() >= BATCH_COUNT) {
//            saveData();
//            cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
//        }

    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        //excel列 index=0 key =1vKey =2...languages
        dataTransferAndSaveFile();
        dataSaveDb();
        log.info("所有数据解析完成！");
    }

    /**
     * 翻译文件数据入库
     */
    @Transactional(rollbackFor = Exception.class)
    private void dataSaveDb() {
        SysLogDao sysLogDao = SpringContextHolder.getBean(SysLogDao.class);
        try {
            SysLocaleService sysLocaleService = SpringContextHolder.getBean(SysLocaleService.class);
            SysTranslateDao sysTranslateDao = SpringContextHolder.getBean(SysTranslateDao.class);
            //清空表，重新存储翻译数据
            sysLocaleService.truncateTable();
            sysTranslateDao.truncateTable();
            //语言数量
            int languagesNum = 2;

            //语种对应的语种id与列的map
            Map<Integer, Integer> map = new HashMap<>(languagesNum);
            //解析头数据,存表 存map
            for (int i = 1; i < languagesNum + 1; i++) {
                String locale = headsMap.get(i);
                saveLocale(locale, sysLocaleService);
                map.put(i, sysLocaleService.getIdByLocale(locale));
            }

            //插入List
            List<SysTranslateEntity> list = new ArrayList<>(16);
            cachedDataList.forEach(record -> {
                if (ObjectUtils.isNotNull(record.get(0))) {
                    //多语种
                    for (int i = 0; i < languagesNum; i++) {
                        SysTranslateEntity sysTranslateEntity = new SysTranslateEntity();
                        sysTranslateEntity.setKey(record.get(0));
                        sysTranslateEntity.setLocale(headsMap.get(i + 1));
//                        sysTranslateEntity.setLocaleId(ObjectUtils.isNotNull(map.get(i + 1)) ? map.get(i + 1) : -1);
                        sysTranslateEntity.setText(ObjectUtils.isNotNull(record.get(i + 1)) ? record.get(i + 1) : "");
                        list.add(sysTranslateEntity);
                    }
                }
            });
            // 问题记录，此处批量插入数据后未返回，造成页面卡死，数据量 2000+
            // 尝试使用分页插入解决问题
            if (list.size() > SAVE_LIMIT) {
                int page = list.size() % SAVE_LIMIT == 0 ? list.size() / SAVE_LIMIT : list.size() / SAVE_LIMIT + 1;
                for (int i = 0; i < page; i++) {
                    List<SysTranslateEntity> sysTranslateEntities = list.subList(i * SAVE_LIMIT, Math.min(((i + 1) * SAVE_LIMIT), list.size()));
                    sysTranslateDao.insertBatch(sysTranslateEntities);
                }
            } else {
                sysTranslateDao.insertBatch(list);
            }

        } catch (Exception ex) {
            log.error("翻译库持久化异常");
            ex.printStackTrace();
            saveLog(ex, sysLogDao);
        }
    }

    private void saveLog(Exception ex, SysLogDao sysLogDao) {
        SysLogEntity sysLogEntity = new SysLogEntity();
        sysLogEntity.setOperation("远程服务器翻译库持久化");
        sysLogEntity.setCreateDate(new Date());
        sysLogEntity.setMethod("MultiLanguageExcelImportListener->dataSaveDb()");
        sysLogEntity.setResult(ex.getMessage().substring(0, 200));
        sysLogDao.insert(sysLogEntity);
    }

    private void saveLocale(String locale, SysLocaleService sysLocaleService) {
        SysLocaleEntity sysLocaleEntity = new SysLocaleEntity();
        sysLocaleEntity.setLocale(locale);
        if (locale.equals("en-US")) {
            sysLocaleEntity.setName("English(US)");
        }
        if (locale.equals("zh-CN")) {
            sysLocaleEntity.setName("中文");
        }
        sysLocaleService.insert(sysLocaleEntity);
    }

    /**
     * 数据转换为json格式并存储为json文件
     *
     * @param
     */
    private void dataTransferAndSaveFile() {
        for (int j = 1; j < headsMap.keySet().size(); j++) {
            String languageName = headsMap.get(j);
            Map<String, String> firstProp = new HashMap<>();
            //一级属性set
            Set<String> firstPropSet = new TreeSet<>();
            for (int i = 0; i < cachedDataList.size(); i++) {
                //不存在二级属性
                Map<Integer, String> currentMap = cachedDataList.get(i);
                boolean hasChild = StringUtils.isNotBlank(currentMap.get(1));
                String firstPropName = currentMap.get(0);
                if (firstPropName == null) {
                    continue;
                }
                if (!firstPropSet.contains(firstPropName)) {
                    //只有一级属性
                    firstPropSet.add(firstPropName);
                    firstProp.put(firstPropName, currentMap.get(j));
                }
            }
            dataSaveJsonFile(languageName, firstProp);
        }
    }

    /**
     * 处理好的数据存储为json文件到对应文件夹
     */
    private void dataSaveJsonFile(String name, Map<String, String> language) {
        //转json存储到指定文件夹
        String jsonData = JSON.toJSONString(language, SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue,
                SerializerFeature.WriteDateUseDateFormat);
        String fileName = path + "/" + name + ".json";
        File file = new File(fileName);
//        FileWriter fileWriter = null;
        try (FileWriter fileWriter = new FileWriter(fileName)) { // 使用try-with-resources
//            fileWriter = new FileWriter(fileName,false);
            if (!file.exists()) {
                file.createNewFile();
            }
            fileWriter.write(jsonData);
            fileWriter.flush();
        } catch (IOException e) {
            log.error("{} 文件写入失败", fileName, e);
        }
    }
}