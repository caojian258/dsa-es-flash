package org.dsa.modules.reporter.job;

import lombok.extern.slf4j.Slf4j;
import org.dsa.common.utils.RedisUtils;
import org.dsa.config.SyncConfig;
import org.dsa.modules.reporter.constant.RedisConstant;
import org.dsa.modules.reporter.dto.SyncSessionLogDto;
import org.dsa.modules.reporter.service.SynchronizedService;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import jakarta.annotation.Resource;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/***
 * 实现SessionForward生成诊断会话文件
 */
@Slf4j
@Component
@DisallowConcurrentExecution
public class GenerateSessionLogJob extends QuartzJobBean {

    @Resource
    private SynchronizedService synchronizedService;

    @Resource
    SyncConfig syncConfig;

    @Resource
    RedisUtils redisUtils;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        Long size = redisUtils.lGetListSize(RedisConstant.PROCESSED_SESSION_KEY);

        while (size>0) {

            String tag = (String) redisUtils.lPop(RedisConstant.PROCESSED_SESSION_KEY);
            if (StringUtils.isEmpty(tag)) {
                size--;
                continue;
            }

            String[] tags = tag.split(":");
            if(tags.length != 2){
                size--;
                continue;
            }

            String vin = tags[0];
            String sessionId = tags[1];

            SyncSessionLogDto dto = synchronizedService.sessionLogAssemble(vin, sessionId);

            SimpleDateFormat sdf = new SimpleDateFormat(syncConfig.getDirFormat());
            String path = syncConfig.getTspUploadDir()+ File.separator + sdf.format(new Date());

            Boolean flag = synchronizedService.generateXML(dto, path, sessionId);

            if(!flag){
                redisUtils.lSet(RedisConstant.PROCESSED_SESSION_KEY, tag);
            }
            size--;
        }
    }
}
