package org.dsa.modules.reporter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@TableName("client_component_back")
public class ComponentBack extends BaseEntity{

    @TableId(type = IdType.AUTO)
    private Long id;

    private String deviceId;
    private Integer softwareId;
    private Integer softwareVersionNumber;
    private Integer componentId;
    private String componentVersionName;
    private Integer componentVersionNumber;
    private String componentVersionPath;
    /***
     强制更新： 1=force
     非强制更新： 0=optional
     */
    private Integer updateType;
    private String releaseNotes;
    private String featureDescription;
    private String fileSize;
    private String fileMd5;
    private String tag;
    private String sessionId;
}
