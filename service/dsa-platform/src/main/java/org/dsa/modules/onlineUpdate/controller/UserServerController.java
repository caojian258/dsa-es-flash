package org.dsa.modules.onlineUpdate.controller;

import org.dsa.common.utils.PageUtils;
import org.dsa.common.utils.R;
import org.dsa.modules.sys.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 后期改为Feign调用用户微服务
 * 用户相关
 */

@RestController
@RequestMapping("/user")
public class UserServerController {

    @Autowired
    private SysUserService sysUserService;


    @RequestMapping("/list")
    public R getUserList(@RequestBody Map<String,Object> params){
        try {
            return R.ok().put("page", sysUserService.queryUserList(params));
        }catch (Exception e){

            return R.error(e.getMessage());
        }
    }


}
