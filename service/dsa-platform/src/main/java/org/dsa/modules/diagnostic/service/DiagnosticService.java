package org.dsa.modules.diagnostic.service;


import org.dsa.modules.diagnostic.dto.SessionEcuVersionDto;
import org.dsa.modules.diagnostic.entity.*;

import java.util.List;
import java.util.Map;

/**
 * 诊断功能
 *
 */
public interface DiagnosticService  {

    
    /**
     * 激活诊断车辆
     */
    public void active(ActiveEntity activeEntity, String token);

    /**
     * 取消诊断激活
     */
    public void cancelActive(ActiveEntity activeEntity);
    /**
     * 已激活诊断车辆列表
     */
    List<VehicleActiveEntity> list(Map<String,Object> params);


    /**
     * 开始诊断
     */
    String begin(String vin);
    /**
     * 开始诊断
     */
    String end(String vin);

    /**
     * 车辆健康检查
     */
    String healthy(String vin);

    /**
     * 读取版本信息
     */
    String readIdentData(String vin,String[]ecuNames);
    /**
     * 读取故障码
     */
    String readDtcCode(String vin,String[] ecuNames);
    /**
     * 读取故障码冻结帧
     */
    String readDtcDataGrid(String vin,String ecuName,String dtcCode,String dtcCodeInt);
    /**
     * 清楚故障码
     */
    String clearDtc(String vin,String[] ecuNames);

    /**
     * 车辆报告
     */
    String readVehicleReport(String vin);

    /**
     * 检查车辆是否已激活
     */
    VehicleActiveEntity checkRunRedisValue(String vin);
    /**
     * 检查诊断操作是否是诊断用户
     */
    void checkOwner(String vin);

    /**
     * 解析离线/车载诊断仪操作日志
     */
    void synchronizeLogs();

    List<SessionEcuVersionDto> selectSessionEcuVersion(String sessionId);

    List<DActionDtcEntity> sessionActionDtc(String sessionId);

    List<DActionInfoEntity> sessionActionInfo(String sessionId);

    DSessionInfoEntity findSessionInfo(String sessionId, String vin);

}