package org.dsa.license;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.utils.ConfigConstant;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;


/**
 * 在项目启动时安装证书
 */
@Slf4j
//@Component
public class LicenseCheckListener implements ApplicationListener<ContextRefreshedEvent> {

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        //root application context 没有parent
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             ApplicationContext context = event.getApplicationContext().getParent();
        if(context == null){
            if(StringUtils.isNotBlank(ConfigConstant.licensePath)){
                log.info("++++++++ 开始安装证书 ++++++++");

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         LicenseVerifyParam param = new LicenseVerifyParam();
                param.setSubject(ConfigConstant.subject);
                param.setPublicAlias(ConfigConstant.publicAlias);
                param.setStorePass(ConfigConstant.storePass);
                param.setLicensePath(ConfigConstant.licensePath);
                param.setPublicKeysStorePath(ConfigConstant.publicKeysStorePath);

                LicenseVerify licenseVerify = new LicenseVerify();
                //安装证书
                licenseVerify.install(param);

                log.info("++++++++ 证书安装结束 ++++++++");

                log.info("++++++++ 开始校验证书 ++++++++");
                //安装证书
                if(!licenseVerify.verify()) {
                    ConfigurableApplicationContext ctx = (ConfigurableApplicationContext) event.getApplicationContext();
                    ctx.close();
                }

                log.info("++++++++ 证书校验结束 ++++++++");
            }
        }
    }
}
