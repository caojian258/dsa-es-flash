package org.dsa.modules.sys.utils;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.exception.RRException;
import org.dsa.common.pojo.CodeMessage;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.HttpUtils;
import org.dsa.common.utils.ObjectUtils;
import org.dsa.modules.sys.entity.InceptioResponserEntity;
import org.dsa.modules.sys.entity.UserInfoEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.annotation.Resource;

/**
 * 根据token获取赢彻用户信息
 */

public class YCUserUtils {

    @Resource
    private static final Logger logger = LoggerFactory.getLogger(YCUserUtils.class);

    public static UserInfoEntity getUserEntity(String url, String token){

        InceptioResponserEntity inceptioResponserEntity = new InceptioResponserEntity();
        //调用赢彻用户接口
        CodeMessage code = HttpUtils.post(url+token,"");
        if(StringUtils.isNotEmpty(code.getCode()) && "200".equals(code.getCode())){
            JSONObject jsonObject = JSONObject.parseObject(code.getMessage());
            inceptioResponserEntity = jsonObject.toJavaObject(inceptioResponserEntity.getClass());
            if(StringUtils.isNotEmpty(inceptioResponserEntity.getCode()) && "200".equals(inceptioResponserEntity.getCode()) ){
                //redis存放 token
                logger.info("获取赢彻用户信息成功{}",inceptioResponserEntity.getData());
                if (ObjectUtils.isNotNull(inceptioResponserEntity.getData().getUserInfo())){
                    return inceptioResponserEntity.getData().getUserInfo();
                }else{
                    logger.error("查询赢彻用户信息有误");
                    throw new RRException(Constant.Msg.CHECK_GET_SSO_USER_FILE);
                }
            }else{
                logger.error("获取赢彻用户信息失败{},{}",Integer.parseInt(inceptioResponserEntity.getCode()),inceptioResponserEntity.getMsg());
                return null;
            }
        }else{
            JSONObject jsonObject = JSONObject.parseObject(code.getMessage());
            logger.error("获取赢彻用户信息失败");
            return null;
        }
    }
}
