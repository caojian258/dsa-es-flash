package org.dsa.modules.remoteDiagnostic.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.io.FileUtils;
import org.dsa.common.utils.RedisUtils;
import org.dsa.config.SyncConfig;
import org.dsa.modules.remoteDiagnostic.config.RemoteDiagnosticConfig;
import org.dsa.modules.remoteDiagnostic.dao.*;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticConditionCopyEntity;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticFileEntity;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskFileEntity;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskRecordEntity;
import org.dsa.modules.remoteDiagnostic.service.DiagnosticFileService;
import org.dsa.modules.remoteDiagnostic.vo.CheckNameReqVo;
import org.dsa.modules.remoteDiagnostic.vo.CheckStatusReqVo;
import org.dsa.modules.remoteDiagnostic.vo.PublicPageReqVo;
import org.dsa.modules.reporter.util.FilesUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class DiagnosticFileServiceImpl implements DiagnosticFileService {

    @Autowired
    DiagnosticFileDao fileDao;
    @Autowired
    SyncConfig syncConfig;
    @Autowired
    RemoteDiagnosticConfig remoteDiagnosticConfig;
    @Autowired
    private DiagnosticTaskRecordDao taskRecordDao;
    @Autowired
    private DiagnosticTaskDetailDao taskDetailDao;
    @Autowired
    private DiagnosticTaskOverviewDao taskOverviewDao;
    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private DiagnosticTaskFileDao taskFileDao;

    @Override
    public Page<DiagnosticFileEntity> selectPage(PublicPageReqVo vo) {
        Page<DiagnosticFileEntity> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);
        LambdaQueryWrapper<DiagnosticFileEntity> qw = Wrappers.<DiagnosticFileEntity>lambdaQuery();

        if (!StringUtils.isEmpty(vo.getName())) {
//            qw.like(VehicleEcuEntity::getEcuName, vo.getName().trim());
            qw.apply("file_name" + " ilike {0}", "%" + vo.getName().trim() + "%");
        }
        if (!StringUtils.isEmpty(vo.getScript())) {
//            qw.like(VehicleEcuEntity::getEcuName, vo.getName().trim());
            qw.apply("function_name" + " ilike {0}", "%" + vo.getScript().trim() + "%");
        }

        if (!StringUtils.isEmpty(vo.getStatus())) {
            qw.eq(DiagnosticFileEntity::getStatus, vo.getStatus());
        }
        Page<DiagnosticFileEntity> resultPage = fileDao.selectPage(page, qw);

        return resultPage;
    }

    @Override
    public DiagnosticFileEntity getInfo(Long id) {
        return fileDao.selectById(id);
    }

    @Override
    @Transactional
    public DiagnosticFileEntity getInfo(String md5) {

//        DiagnosticTaskRecordEntity record = taskRecordDao.getInfoBySessionId(sessionId, vin);
//        if (record == null) {
//            return null;
//        }
//        DiagnosticTaskOverviewEntity overview = taskOverviewDao.getInfoByTask(record.getTaskId());
//        if (overview == null) {
//            return null;
//        }

//        DiagnosticTaskDetailEntity diagnosticTaskDetailEntity = new DiagnosticTaskDetailEntity();
//        diagnosticTaskDetailEntity.setVin(vin);
//        diagnosticTaskDetailEntity.setSessionId(sessionId);
//        diagnosticTaskDetailEntity.setType(TaskDetailTypeEnum.DOWNLOAD.getValue());
//        diagnosticTaskDetailEntity.setTaskId(record.getTaskId());
//        diagnosticTaskDetailEntity.setStartTime(new Date());
//        taskDetailDao.insert(diagnosticTaskDetailEntity);
        DiagnosticFileEntity diagnosticFileEntity = fileDao.selectOne(Wrappers.<DiagnosticFileEntity>lambdaQuery().eq(DiagnosticFileEntity::getFileMd5, md5));
        if (diagnosticFileEntity == null || diagnosticFileEntity.getFileLength() == null || diagnosticFileEntity.getFileLength() == 0) {
            return null;
        }
        return diagnosticFileEntity;
    }

    @Override
    public void saveFile(DiagnosticFileEntity file) throws IOException {

        if (StringUtils.isEmpty(file.getFunctionName())) {
            file.setFunctionName(file.getFileName());
        }
        File file1 = new File(file.getFilePath());
        if (!file1.exists()) {
            throw new IOException();
        }
        String md5 = DigestUtils.md5DigestAsHex(new FileInputStream(file1));
        file.setFileMd5(md5);
        file.setFileLength(file1.length());

        fileDao.insert(file);
    }

    @Override
    public void updateFile(DiagnosticFileEntity file) throws IOException {
        file.setUpdatedAt(new Date());
        File file1 = new File(file.getFilePath());
        if (!file1.exists()) {
            throw new IOException();
        }
        String md5 = DigestUtils.md5DigestAsHex(new FileInputStream(file1));
        file.setFileMd5(md5);
        file.setFileLength(file1.length());

        fileDao.updateById(file);
    }

    @Override
    public Map<String, Object> getFiles(Long id) {
        return null;
    }

    @Override
    public List<DiagnosticFileEntity> getFiles() {
        return fileDao.selectList(Wrappers.<DiagnosticFileEntity>lambdaQuery().eq(DiagnosticFileEntity::getStatus, 0));
    }

    @Override
    public Boolean check(CheckNameReqVo vo) {
        LambdaQueryWrapper<DiagnosticFileEntity> qw = Wrappers.<DiagnosticFileEntity>lambdaQuery();
        qw.eq(DiagnosticFileEntity::getFunctionName, vo.getName());
        if (vo.getId() != null) {
            qw.ne(DiagnosticFileEntity::getId, vo.getId());
        }
        qw.select(DiagnosticFileEntity::getId);
        return fileDao.selectOne(qw) == null;
    }

    @Override
    public String upload(File file) throws IOException {
        String dateFormat = syncConfig.getDirFormat();
        SimpleDateFormat format = new SimpleDateFormat(dateFormat);
        String dayDir = format.format(new Date());
        String otxName = remoteDiagnosticConfig.getFlashFileName() + "_" + System.currentTimeMillis();

        File dir = new File(remoteDiagnosticConfig.getOtx() + dayDir);

        if (!dir.exists()) {
            FilesUtil.mkdir(dir);
        }
        File destFile = new File(remoteDiagnosticConfig.getOtx() + dayDir + File.separator + otxName);

        FileUtils.copyFile(file, destFile);
        boolean delete = file.delete();

        return destFile.getPath();
    }

    @Override
    public Boolean checkStatus(CheckStatusReqVo vo) {
        LambdaQueryWrapper<DiagnosticTaskFileEntity> qw = Wrappers.<DiagnosticTaskFileEntity>lambdaQuery();
        qw.eq(DiagnosticTaskFileEntity::getFileId, vo.getId());
        qw.select(DiagnosticTaskFileEntity::getId);
        return ObjectUtils.isEmpty(taskFileDao.selectList(qw));
    }
}
