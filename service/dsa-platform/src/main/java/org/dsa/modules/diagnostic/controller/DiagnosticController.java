package org.dsa.modules.diagnostic.controller;

import org.dsa.common.annotation.SysLog;
import org.dsa.common.utils.R;
import org.dsa.modules.diagnostic.entity.*;
import org.dsa.modules.diagnostic.service.DiagnosticRespService;
import org.dsa.modules.diagnostic.service.DiagnosticService;
import org.dsa.modules.sys.controller.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import jakarta.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 诊断功能
 *
 */
@RestController
@RequestMapping("diagnostic")
public class DiagnosticController extends AbstractController {
	@Autowired
	private DiagnosticService diagnosticService;

	@Autowired
	private DiagnosticRespService diagnosticRespService;
	
	/**
	 * 诊断激活
	 */
	@SysLog("诊断激活")
	@RequestMapping("/active")
	public R active(@RequestBody ActiveEntity activeEntity, HttpServletRequest req){
		diagnosticService.active(activeEntity,req.getHeader("token"));
		return R.ok();
	}
	/**
	 * 取消诊断激活
	 */
	@SysLog("取消诊断激活")
	@RequestMapping("/cancelActive")
	public R cancelActive(@RequestBody ActiveEntity activeEntity){
		diagnosticService.cancelActive(activeEntity);
		return R.ok();
	}

	/**
	 * 远程诊断激活列表
	 */
	@PostMapping("/list")
	public R list(@RequestBody Map<String,Object> params){
		List<VehicleActiveEntity> l =diagnosticService.list(params);
		return R.ok().put("list",l);
	}

	/**
	 * 开始诊断
	 */
	@SysLog("开始诊断")
	@RequestMapping("/begin")
	public R begin(@RequestParam String vin){
		String actionId = diagnosticService.begin(vin);
		return R.ok().put("actionId",actionId);
	}

	/**
	 * 开始诊断 执行结果查询
	 */
	@RequestMapping("/beginResp")
	public R beginRsep(@RequestParam String actionId){
		return diagnosticRespService.beginResp(actionId);
	}


	/**
	 * 检查诊断操作是否是诊断用户
	 */
	@RequestMapping("/checkOwner")
	public R checkOwner(@RequestParam String vin){
		diagnosticService.checkOwner(vin);
		return R.ok();
	}



	/**
	 * 结束诊断
	 */
	@SysLog("结束诊断")
	@RequestMapping("/end")
	public R end(@RequestParam String vin){
		String actionId = diagnosticService.end(vin);
		return R.ok().put("actionId",actionId);
	}
	/**
	 * 结束诊断 执行结果查询
	 */
	@RequestMapping("/endResp")
	public R endResp(@RequestParam String actionId){
		return diagnosticRespService.endResp(actionId);
	}

	/**
	 * 车辆健康检查
	 */
	@SysLog("车辆健康检查")
	@RequestMapping("/healthy")
	public R healthy(@RequestParam String vin){
		String actionId = diagnosticService.healthy(vin);
		return R.ok().put("actionId",actionId);
	}
	/**
	 * 车辆健康检查 执行结果查询
	 */
	@RequestMapping("/healthyResp")
	public R healthyRsep(@RequestParam String actionId){
		return diagnosticRespService.healthyResp(actionId);
	}


	/**
	 * 读取版本信息
	 */
	@SysLog("读取版本信息")
	@RequestMapping ("/readIdent")
	public R readIdent(@RequestBody ReadIdentEntity readIdentEntity){
		String actionId=diagnosticService.readIdentData(readIdentEntity.getVin(),readIdentEntity.getEcuNames());
		return R.ok().put("actionId",actionId);
	}
	/**
	 * 读取版本信息结果
	 */
	@RequestMapping("/readIdentResp")
	public R readIdentResp(@RequestParam String actionId){
		return diagnosticRespService.readIdentDataResp(actionId);
	}
	/**
	 * 读取故障码
	 */
	@SysLog("读取故障码")
	@RequestMapping("/readDtc")
	public R readDtc(@RequestBody ReadDtcEntity readDtcEntity){
		String actionId=diagnosticService.readDtcCode(readDtcEntity.getVin(),readDtcEntity.getEcuNames());
		return R.ok().put("actionId",actionId);
	}
	/**
	 * 读取故障码结果
	 */
	@RequestMapping("/readDtcResp")
	public R readDtcResp(@RequestParam String actionId,int length){
		return diagnosticRespService.readDtcCodeResp(actionId,length);

	}
	/**
	 * 读取故障码冻结帧
	 */
	@SysLog("读取故障码冻结帧")
	@RequestMapping("/readDtcDataGrid")
	public R readDtcDataGrid(@RequestBody ReadDtcGridEntity readDtcGridEntity){
		String actionId= diagnosticService.readDtcDataGrid(readDtcGridEntity.getVin(), readDtcGridEntity.getEcuName(), readDtcGridEntity.getDtcCode(), readDtcGridEntity.getDtcCodeInt());
		return R.ok().put("actionId",actionId);
	}
	/**
	 * 读取故障码冻结帧结果
	 */
	@RequestMapping("/readDtcDataGridResp")
	public R readDtcDataGridResp(@RequestParam String actionId){
		return diagnosticRespService.readDtcDataGridResp(actionId);
	}
	/**
	 * 清除故障码
	 */
	@SysLog("清除故障码")
	@RequestMapping("/cleanDtcCode")
	public R clearDtc(@RequestBody ClearDtcEntity clearDtcEntity){
		String actionId=diagnosticService.clearDtc(clearDtcEntity.getVin(),clearDtcEntity.getEcuNames());
		return R.ok().put("actionId",actionId);
	}
	/**
	 * 清楚故障码回复
	 */
	@RequestMapping("/cleanDtcCodeResp")
	public R clearDtcResp(@RequestParam String actionId){
		return diagnosticRespService.clearDtcResp(actionId);
	}


	/**
	 * 车辆报告
	 */
	@SysLog("车辆报告")
	@RequestMapping("/vehicleReport")
	public R vehicleReport(@RequestParam String sessionId){
		String actionId=diagnosticService.readVehicleReport(sessionId);
		return R.ok().put("actionId",actionId);
	}
}
