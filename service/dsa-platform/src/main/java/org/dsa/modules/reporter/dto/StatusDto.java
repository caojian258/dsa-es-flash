package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class StatusDto implements Serializable {

    /***
     * 诊断会话
     */
    private String sessionId;

    /***
     * 经销商
     */
    private String workshop;

    /***
     * 客户端ID
     */
    private String pcid;

    /***
     * 操作人
     */
    private String username;

    /***
     * 车架号
     */
    private String vinCode;

    /***
     * 消息
     */
    private String msg;

    /***
     * 客户端的语言
     */
    private String locale;
}
