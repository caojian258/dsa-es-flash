CREATE TABLE public.d_vehicle_type (
	id bigserial NOT NULL,
	parent_id int8 NOT NULL,
	value varchar NOT NULL,
	order_num int4 NOT NULL,
	"level" int4 NULL,
	create_time timestamp NULL DEFAULT now(),
	have_odx_version int4 NOT NULL DEFAULT 0,
	have_otx_version int4 NOT NULL DEFAULT 0,
	ecu_list json NULL,
	description_ti varchar NULL, -- 描述
	remark_ti varchar NULL, -- 备注
	sdg_list json NULL, -- 扩展信息
	CONSTRAINT d_vehicle_type_pkey PRIMARY KEY (id),
	CONSTRAINT d_vehicle_type_un UNIQUE (parent_id, value)
);

-- Column comments
COMMENT ON COLUMN public.d_vehicle_type.description_ti IS '描述';
COMMENT ON COLUMN public.d_vehicle_type.remark_ti IS '备注';
COMMENT ON COLUMN public.d_vehicle_type.sdg_list IS '扩展信息';

CREATE TABLE public.d_vehicle_info (
	id serial4 NOT NULL,
	vin varchar(17) NOT NULL,
	vehicle_type_id int8 NOT NULL,
	production_date varchar(50) NULL,
	place_of_production varchar(50) NULL,
	"source" int4 NULL,
	region_code varchar NULL,
	city_code varchar NULL,
	workshop_name varchar NULL,
	version_id int8 NULL, -- 车辆版本
	create_time timestamp NULL DEFAULT now(),
	update_time timestamp NULL DEFAULT now(),
	CONSTRAINT d_vehicle_info_pkey PRIMARY KEY (id),
	CONSTRAINT d_vehicle_info_vin_code_key UNIQUE (vin)
);

COMMENT ON COLUMN public.d_vehicle_info.version_id IS '车辆版本';

CREATE TABLE public.d_vehicle_ecu_did (
	id bigserial NOT NULL,
	vehicle_type_id int8 NOT NULL,
	model varchar(100) NOT NULL,
	ecu_name varchar(50) NOT NULL,
	ecu_description varchar(512) NULL,
	ecu_description_ti varchar(512) NULL,
	did varchar(20) NOT NULL,
	did_name varchar(512) NULL,
	did_name_ti varchar(50) NULL,
	did_value varchar(500) NULL,
	create_time timestamp NULL DEFAULT now(),
	update_time timestamp NULL DEFAULT now(),
	CONSTRAINT d_vehicle_ecu_did_pkey PRIMARY KEY (id)
);