package org.dsa.config;

import org.dsa.common.utils.Constant;
import org.dsa.common.utils.IPUtils;
import org.dsa.common.utils.PortUtil;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.diagnostic.quartz.QuartzSynLoggerDetail;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import jakarta.annotation.Resource;

/**
 * @author qzq
 */
@Configuration
public class QuartzConfig {
    private final Logger log = LoggerFactory.getLogger(QuartzConfig.class);
    @Value("${spring.quartz.log-corn}")
    private String logCorn;
    @Value("${server.port}")
    private String port;
    @Resource
    private RedisUtils redisUtils;
    /**
     * 创建日志同步定时任务
     */
    @Bean
    public JobDetail quartzSynLoggerDetail() {
        JobDetail jobDetail = JobBuilder.newJob(QuartzSynLoggerDetail.class)
                .withIdentity("quartzSynLoggerDetail", "QUARTZ_SynLogger")
                .usingJobData("name", "quartz_synLogger")
                .storeDurably()
                .build();
        return jobDetail;
    }

    /**
     * 创建日志同步触发器
     */
    @Bean
    public Trigger quartzSynLoggerJobTrigger() {
        String value = IPUtils.getLocalIp()+":"+port;
        redisUtils.set(Constant.REDIS_SYN_LOG_Key,value);
        //每隔1小时秒执行一次
        CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(logCorn);
        //创建触发器 withMisfireHandlingInstructionFireAndProceed() 立即执行，下次按照正常的corn执行
        Trigger trigger = TriggerBuilder.newTrigger()
                .forJob(quartzSynLoggerDetail())
                .withIdentity("quartzSynLoggerJobTrigger", "QUARTZ_SYNLOGGER_JOB_TRIGGER")
                .withSchedule(cronScheduleBuilder.withMisfireHandlingInstructionDoNothing())
                .build();
        return trigger;
    }

    private String getLocalPort(){
        String localPort = "";
        try {
            localPort = PortUtil.getLocalPort();
        } catch (Exception e) {
            log.info("获取本服务端口号失败");
            e.printStackTrace();
        }
        return localPort;
    }

}
