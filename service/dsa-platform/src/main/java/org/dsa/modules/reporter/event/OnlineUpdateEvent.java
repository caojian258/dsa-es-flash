package org.dsa.modules.reporter.event;

import org.springframework.context.ApplicationEvent;

public class OnlineUpdateEvent extends ApplicationEvent {

    private String tag;

    private String path;

    private String name;

    public OnlineUpdateEvent(Object source) {
        super(source);
    }

    public OnlineUpdateEvent(Object source,  String tag, String path, String name){
        super(source);
        this.tag = tag;
        this.path = path;
        this.name = name;
    }

    public String getPath(){
        return path;
    }

    public String getTag(){
        return tag;
    }

    public String getName(){
        return name;
    }

}
