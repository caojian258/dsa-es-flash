package org.dsa.modules.diagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.diagnostic.entity.DSessionInfoEntity;

import java.util.List;

/**
 * 车辆诊断会话表
 */
@Mapper
public interface DSessionInfoDao extends BaseMapper<DSessionInfoEntity> {

	/**
	 * 根据sessionId修改end_time
	 *
	 * @param vin
	 * @return
	 */
	void updateSessionEndTimeByVin(@Param("vin") String vin);

	void updateSessionEndTimeAndEndStatusByVin(@Param("vin") String vin,@Param("status") Integer status);

	IPage<DSessionInfoEntity> selectSessionList(IPage<DSessionInfoEntity> page
			,@Param("vin") String vin
			,@Param("userId") Integer userId
			,@Param("vehicleTypeIds") List<String> vehicleTypeIds
			,@Param("functionCode") String functionCode
			,@Param("actionCode") String actionCode
			,@Param("actionId") String actionId
			,@Param("dtcCode") String dtcCode
			,@Param("startTime") String startTime
			,@Param("endTime") String endTime);

	void updateSessionEndTimeAndEndStatusByActionId(@Param("rid") String rid,@Param("status") Integer status);

	void updateBySessionId(DSessionInfoEntity record);

	/**
	 * 校验sessionId是否存在
	 * @param sessionId
	 * @return
	 */
	Integer checkUnique(String sessionId);

	/**
	 * 更新logPath
	 * @param sessionId
	 * @param path
	 */
	void updateLogPathBySessionId(@Param("sessionId") String sessionId,@Param("logPath") String path);

	DSessionInfoEntity findOneBySessionID(@Param("sessionId") String sessionId, @Param("vin") String vin);
}
