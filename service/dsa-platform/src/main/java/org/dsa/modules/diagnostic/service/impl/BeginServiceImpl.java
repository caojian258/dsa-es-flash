package org.dsa.modules.diagnostic.service.impl;

import ch.qos.logback.classic.Logger;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.DateUtils;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.diagnostic.dao.DActionInfoDao;
import org.dsa.modules.diagnostic.dao.DSessionActionDao;
import org.dsa.modules.diagnostic.dao.DSessionInfoDao;
import org.dsa.modules.diagnostic.entity.DActionInfoEntity;
import org.dsa.modules.diagnostic.entity.DSessionActionEntity;
import org.dsa.modules.diagnostic.entity.DSessionInfoEntity;
import org.dsa.modules.diagnostic.entity.VehicleActiveEntity;
import org.dsa.modules.diagnostic.entity.mqtt.callback.MqBodyDataBegin;
import org.dsa.modules.diagnostic.entity.mqtt.callback.MqEntity;
import org.dsa.modules.diagnostic.entity.mqtt.callback.MqHeaderEntity;
import org.dsa.modules.diagnostic.service.DiagnosticMqService;
import org.dsa.modules.diagnostic.util.DiagnosticUtils;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service("beginService")
public class BeginServiceImpl implements DiagnosticMqService {
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass());

    @Resource
    private RedisUtils redisUtils;
    @Resource
    private DSessionInfoDao dSessionInfoDao;
    @Resource
    private DSessionActionDao dSessionActionDao;
    @Resource
    private DActionInfoDao dActionInfoDao;
    @Resource
    private DiagnosticUtils diagnosticUtils;


    @Override
    public void onMessage(MqEntity mqEntity) {
        String loggerKey = DateUtils.format(new Date(), "yyyyMMdd") + "/" + mqEntity.getHeader().getRid();
        MDC.put("sessionId", loggerKey);
        logger.info("Begin MQ Call back ---- begin");
        logger.info("Begin MQ Call back ---- ActionId:{}",mqEntity.getHeader().getRid());

        //1.判断是否有有诊断执行缓存
        //1.1有缓存 更改db,删除诊断命令 key,删除 session唯一执行key.
        //1.2没有缓存 更改db

        //判断车辆信息是有缓存   key H_sessionId
        if(redisUtils.hasKey(Constant.REDIS_ACTION + mqEntity.getHeader().getRid())){

            //接收到车辆健康检查信息回调
            logger.info("Begin MQ Call back Redis key:" + Constant.REDIS_ACTION + mqEntity.getHeader().getRid());
            logger.info("Begin MQ Call back Redis value:" + redisUtils.get(Constant.REDIS_ACTION + mqEntity.getHeader().getRid()));
            String vin = redisUtils.get(Constant.REDIS_ACTION+ mqEntity.getHeader().getRid());
            if (redisUtils.hasKey(Constant.REDIS_VIN + vin)) {
                VehicleActiveEntity v = redisUtils.get(Constant.REDIS_VIN + vin, VehicleActiveEntity.class);
                logger.info("Begin MQ Call back ---- Redis key:" + Constant.REDIS_VIN + vin);
                logger.info("Begin MQ Call back ---- Redis value:" + JSONObject.toJSONString(v));

                //session 已激状态，修改为诊断中
                if(Constant.SESSION_STATUS.ACTIVE.getValue() == v.getStatus()) {
                    actionResultUpdate(mqEntity,v);
                }
            }else {
                //诊断车辆 非诊断中状态
                logger.info("Begin MQ Call ---- redis value vehicle status not peoces, select db inster and update data");
            }
        }else{
            //断开连接，清除经销商关联诊断车辆
            logger.info("Begin MQ Call  ---- vehicle is null  ");
        }

        logger.info("Begin MQ Call back ---- end");
        MDC.remove(loggerKey);
    }
    /**
    *诊断缓存 清除及修改  db数据修改
    */
    private void actionResultUpdate(MqEntity mqEntity,VehicleActiveEntity v) {
        logger.info("Begin MQ Call ---- update status by actionId:{}", mqEntity.getHeader().getRid());
        MqHeaderEntity headerEntity = mqEntity.getHeader();
        MqBodyDataBegin bodyData =  ((JSONObject)mqEntity.getBody().getData()).toJavaObject(MqBodyDataBegin.class);
        //删除redis 诊断key
        logger.info("Begin MQ Call ---- delete redis actionId:{}", Constant.REDIS_ACTION + mqEntity.getHeader().getRid());
        logger.info("Begin MQ Call ---- delete redis sessionId:{}", Constant.REDIS_RUN_VIN + v.getVin());
        redisUtils.delete(Constant.REDIS_ACTION + mqEntity.getHeader().getRid());
        redisUtils.delete(Constant.REDIS_RUN_VIN + v.getVin());

        if(v != null) {
            //设置 车辆信息 redis mq返回 成功 车辆session 修改为已激活 ，其它值不修改 车辆session 状态
            //位运算 与 判断状态 业务 执行成功
            if((Constant.ACK.FIN.getValue() & mqEntity.getHeader().getAck()) != 0) {
                //判断业务数据是否执行成功
                if(mqEntity.getBody().getStatus() == 0) {
                    v.setStatus(Constant.SESSION_STATUS.PROCES.getValue());
                    v.setSessionId(bodyData.getSessionId());
                    //保存session
                    saveSession(v);

                    //缓存执行结果-> 用于页面显示，key actionId ,value status
                    logger.info("Begin SUCCESS ---- ---- Redis key({}) setValue:{}", Constant.REDIS_ACTION_STATUS + headerEntity.getRid(), Constant.RESULTS.SUCCESS.getValue());
                    redisUtils.set(Constant.REDIS_ACTION_STATUS + headerEntity.getRid(), Constant.RESULTS.SUCCESS.getValue(),Constant.Function.BEGIN.getExpire());

                    //保存action 成功执行
                    saveAction(v, mqEntity, Constant.Function.BEGIN, Constant.Action.BEGIN, String.valueOf(Constant.RESULTS.SUCCESS.getValue()));

                    //更新车辆信息缓存
                    logger.info("Begin FINISH ---- ---- Redis key({}) setValue:{}", Constant.REDIS_VIN + v.getVin(), v);
                    redisUtils.set(Constant.REDIS_VIN + v.getVin(), v);
                }
            }else{
                //缓存执行结果-> 用于页面显示，key actionId ,value status
                logger.info("Begin FAIL ---- ---- Redis key({}) setValue:{}", Constant.REDIS_ACTION_STATUS + headerEntity.getRid(), mqEntity.getHeader().getAck());
                redisUtils.set(Constant.REDIS_ACTION_STATUS + headerEntity.getRid(), Constant.RESULTS.FAIL.getValue(),Constant.Function.BEGIN.getExpire());
            }

            //设置 车辆检测列表
            List vehicleActiveEntityList = redisUtils.get(Constant.REDIS_LIST + v.getWorkShopName(), (new ArrayList()).getClass());
            if(vehicleActiveEntityList != null && vehicleActiveEntityList.size() > 0) {
                vehicleActiveEntityList.removeIf(versionActiveEntity -> (versionActiveEntity.toString().indexOf(v.getVin()) > 0));
            } else {
                vehicleActiveEntityList = new ArrayList<VehicleActiveEntity>();
            }
            vehicleActiveEntityList.add(v);
            if (StringUtils.isNotBlank(v.getWorkShopName())){
                redisUtils.set(Constant.REDIS_LIST + v.getWorkShopName(), vehicleActiveEntityList);
            }
        }
    }

    private void saveAction(VehicleActiveEntity v, MqEntity mqEntity, Constant.Function function, Constant.Action action,String results) {

        DSessionActionEntity sa = new DSessionActionEntity();
        sa.setSessionId(v.getSessionId());
        sa.setActionId(mqEntity.getHeader().getRid());
        dSessionActionDao.insert(sa);
        logger.info("Begin save DSessionAction:{} ",JSONObject.toJSONString(sa));

        DActionInfoEntity actionInfoEntity = new DActionInfoEntity();
        actionInfoEntity.setActionId(mqEntity.getHeader().getRid());
        actionInfoEntity.setFunctionCode(function.getFunctionCode());
        actionInfoEntity.setActionCode(action.getActionCode());
        actionInfoEntity.setStartTime(new Date(v.getDiagnosticTime()));
        actionInfoEntity.setEndTime(new Date());
        actionInfoEntity.setResults(results);
        dActionInfoDao.insert(actionInfoEntity);
        logger.info("{} save DActionInfo:{} ",function.getFunctionName(),JSONObject.toJSONString(actionInfoEntity));

    }


    /**
     * 车辆激活保存session
     */
    private int saveSession(VehicleActiveEntity v) {
        DSessionInfoEntity d = new DSessionInfoEntity();
        d.setSessionId(v.getSessionId());
        d.setVin(v.getVin());
        d.setActiveUserId(v.getActiveUserId());
        d.setActiveDisplayUserName(v.getActiveUserName());
        d.setDiagnosticUserId(v.getDiagnosticUserId());
        d.setDiagnosticDisplayUserName(v.getDiagnosticUserName());
        d.setSource(Constant.SESSION_SOURCE.REMOTE.getDbName());
        d.setStartTime(new Date(v.getDiagnosticTime()));

        logger.info("Active saveSession:{}",JSONObject.toJSONString(d));
        return dSessionInfoDao.insert(d);
    }

}