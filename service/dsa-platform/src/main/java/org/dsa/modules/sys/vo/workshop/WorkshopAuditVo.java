package org.dsa.modules.sys.vo.workshop;

import lombok.Data;

import jakarta.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class WorkshopAuditVo implements Serializable {

    @NotNull(message = "Workshop Id Not NUll")
    private Long workshopId;

    @NotNull(message = "Workshop Status Not NUll")
    private Integer status;
}
