//package org.dsa.modules.remoteDiagnostic.dao;
//
//
//import com.baomidou.mybatisplus.core.mapper.BaseMapper;
//import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
//import org.apache.ibatis.annotations.Mapper;
//import org.apache.ibatis.annotations.Param;
//import org.dsa.modules.remoteDiagnostic.entity.RemoteVehiclePlatformEntity;
//
//import java.util.List;
//
//@Mapper
//public interface RemoteVehiclePlatformDao extends BaseMapper<RemoteVehiclePlatformEntity> {
//
//    Page<RemoteVehiclePlatformEntity> queryPage(Page<RemoteVehiclePlatformEntity> page, @Param("name") String name, @Param("status") Integer status, @Param("vehicleTypeIds") List<Long> vehicleTypeIds);
//}


//
//<select id="queryPage" resultType="org.dsa.modules.remoteDiagnostic.entity.RemoteVehiclePlatformEntity">
//        select t.*,dvt.value as vehicleType from r_vehicle_platform t
//        left join d_vehicle_type dvt on t.vehicle_type_id = dvt.id
//        where 1=1
//
//<if test="vehicleTypeIds!=null and vehicleTypeIds.size!=0 ">
//        and t.vehicle_type_id in
//<foreach collection="vehicleTypeIds" index="index" item="vehicleTypeId" open="(" separator="," close=")">
//        #{vehicleTypeId}
//        </foreach>
//        </if>
//
//        <if test="status!=null and status!='' ">
//        and t.status = #{status}
//        </if>
//        <if test="name!=null and name!='' ">
//        and t.name ilike concat('%',#{name},'%')
//        </if>
//
//        </select>