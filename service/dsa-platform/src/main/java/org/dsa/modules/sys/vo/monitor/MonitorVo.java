package org.dsa.modules.sys.vo.monitor;

import lombok.Data;
import lombok.ToString;
import org.dsa.modules.sys.pojo.PageParam;

@Data
@ToString(callSuper = true)
public class MonitorVo extends PageParam {

   private String relname;

   private String sortColumn = "reltuples";

}
