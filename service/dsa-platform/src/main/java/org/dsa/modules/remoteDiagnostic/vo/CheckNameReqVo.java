package org.dsa.modules.remoteDiagnostic.vo;

import lombok.Data;
import lombok.ToString;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.common.validator.group.UpdateGroup;

import jakarta.validation.constraints.NotBlank;

@Data
@ToString(callSuper = true)
public class CheckNameReqVo {

    private Long id;

    private Long groupId;

    @NotBlank(message = "类型不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String type;

    @NotBlank(message = "名称不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String name;
}
