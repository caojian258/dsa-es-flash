package org.dsa.modules.vehicle.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class VehicleInfoEntity implements Serializable
{
    private static final long serialVersionUID = 1L;

    /***
     * 整车大版本
     */
    private Long versionId;

    /***
     * 最后一层车型ID
     */
    private Long vehicleTypeId;

    /**
     * vin码
     */
    private String vin;

    /**
     * 厂商	String(32)	是	东风
     */
    private String maker;

    /**
     * 	车型	String(32)	是	重卡
     */
    private String model;

    /**
     * 车辆平台	String(32)	否	东风天龙
     */
    private String platform ="";

    /**
     * 年款	String(4)	否	2021
     */
    private String year ="";

    /**
     * 配置  如高中低配	String(10)	否	高配
     */
    private String trimLevel ="";

    /**
     * 生产日期 String(10)	否	2021-01-01
     */
    private String productionDate ="";

    /**
     * 产地 String(32)	否	湖北十堰
     */
    private String placeOfProduction ="";

    /**
     * 车辆来源 1——TSP同步 2——远程服务器添加
     */
    private Integer source;

}
