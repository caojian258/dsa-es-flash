package org.dsa.modules.onlineUpdate.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.dsa.modules.onlineUpdate.entity.RuleUserEntity;

/**
 * 规则
 * 
 */
@Mapper
public interface RuleUserDao extends BaseMapper<RuleUserEntity> {

}
