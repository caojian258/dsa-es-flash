
-- r_diag_condition
INSERT INTO public.r_diag_condition (id, condition_name, condition_code, condition_val, unity, min_val, max_val, eq_val, in_val, description_ti, status, created_at, updated_at) VALUES(3, 'Gear position', '3', 9, 'L/S/D/N/P', '', '', '', '', 'Gear position', 0, '2023-04-07 16:46:56.136', '2023-04-12 14:53:58.628');
INSERT INTO public.r_diag_condition (id, condition_name, condition_code, condition_val, unity, min_val, max_val, eq_val, in_val, description_ti, status, created_at, updated_at) VALUES(1, 'Speed', '1', 4, 'km/h', '', '', '', '', 'Speed', 0, '2023-04-07 16:41:47.393', '2023-04-12 14:54:17.853');
INSERT INTO public.r_diag_condition (id, condition_name, condition_code, condition_val, unity, min_val, max_val, eq_val, in_val, description_ti, status, created_at, updated_at) VALUES(2, 'Temperature', '2', 6, '°C', '', '', '', '', 'Temperature', 0, '2023-04-07 16:45:29.516', '2023-04-27 14:13:27.899');
INSERT INTO public.r_diag_condition (id, condition_name, condition_code, condition_val, unity, min_val, max_val, eq_val, in_val, description_ti, status, created_at, updated_at) VALUES(5, 'TSG', '4', 2, 'G', '', '', '', '', 'TSG', 0, '2023-05-30 11:40:51.637', '2023-05-30 11:40:51.637');

-- r_diag_file
INSERT INTO public.r_diag_file (id, function_name, file_name, file_path, file_md5, file_length, description_ti, status, created_at, updated_at) VALUES(3, 'EcuLogUpload', 'EcuLogUpload.otx', '/usr/file/remote_diagnostic/otx/2023-04-13/otx_1681375589854', '364adafa695f362345c1e3429de0edee', 284792, '', 0, '2023-04-13 16:46:32.108', '2023-04-13 16:46:44.760');

-- r_diag_fun
INSERT INTO public.r_diag_fun (id, fun_name, fun_val, fun_group, order_num, description_ti, status, created_at, updated_at) VALUES(2, 'CLEAR DTC', 'DTC_CLEAR', '', 0, 'CLEAR', 0, '2023-04-07 16:35:28.137', '2023-04-12 14:54:42.404');
INSERT INTO public.r_diag_fun (id, fun_name, fun_val, fun_group, order_num, description_ti, status, created_at, updated_at) VALUES(1, 'READ DTC', 'DTC_READ', '', 0, 'READ', 0, '2023-04-07 16:35:06.150', '2023-04-12 14:54:50.024');
INSERT INTO public.r_diag_fun (id, fun_name, fun_val, fun_group, order_num, description_ti, status, created_at, updated_at) VALUES(4, 'Vehicle Health State', 'VHSS_READ', '', 0, 'Vehicle Health State', 0, '2023-05-30 11:39:46.124', '2023-05-30 11:39:46.124');
INSERT INTO public.r_diag_fun (id, fun_name, fun_val, fun_group, order_num, description_ti, status, created_at, updated_at) VALUES(5, 'Identification Read', 'IDEN_READ', '', 0, '', 0, '2023-05-30 11:40:09.269', '2023-05-30 11:40:09.269');

-- r_diag_task
INSERT INTO public.r_diag_task (id, task_name, description_ti, status, created_at, updated_at) VALUES(5, 'Read DTC of ALTS and GW', 'Read DTC of ALTS and GW', 0, '2023-04-18 19:57:14.001', '2023-04-27 14:41:33.126');
INSERT INTO public.r_diag_task (id, task_name, description_ti, status, created_at, updated_at) VALUES(2, 'EcuLogUpload', 'EcuLogUpload', 0, '2023-04-11 10:22:27.341', '2023-04-27 14:42:22.746');
INSERT INTO public.r_diag_task (id, task_name, description_ti, status, created_at, updated_at) VALUES(6, 'Clear DTC of ALTS and GW', 'Clear DTC of ALTS and GW', 0, '2023-04-24 14:56:46.585', '2023-05-05 13:50:30.706');

-- r_diag_task_campaign
INSERT INTO public.r_diag_task_campaign (id, campaign_name, pool_id, task_id, start_time, end_time, model, failed_attempts_num, failed_num, execution_cycle, execution_interval, priority, gradient, description_ti, status, created_at, updated_at) VALUES(247, 'Read DTC ALTS&GW of E000', 1, 5, '2023-05-11 00:00:00.000', '2023-06-30 23:59:59.999', 'silent', NULL, NULL, NULL, NULL, 'high', '[{"gradient":"","flag":0,"gradientCondition":"","gradientValue":""}]'::json, 'XPENG D55', 0, '2023-04-27 14:43:41.746', '2023-06-07 17:32:22.949');
INSERT INTO public.r_diag_task_campaign (id, campaign_name, pool_id, task_id, start_time, end_time, model, failed_attempts_num, failed_num, execution_cycle, execution_interval, priority, gradient, description_ti, status, created_at, updated_at) VALUES(246, 'Clear DTC ALTS&GW of E000 ', 2, 6, '2023-04-24 00:00:00.000', '2023-05-30 23:59:59.999', 'silent', NULL, NULL, NULL, NULL, 'high', '[{"flag":1}]'::json, 'XPENG E28', 0, '2023-04-24 15:16:28.858', '2023-06-07 17:33:22.680');

-- r_diag_task_overview
INSERT INTO public.r_diag_task_overview (id, campaign_id, match_num, download_num, cancel_num, reject_num, success_num, acceptance_num, fail_num, created_at, updated_at) VALUES(282, 246, 1, 1, 0, 0, 1, 1, 0, '2023-04-27 14:43:11.831', '2023-05-11 15:54:46.655');
INSERT INTO public.r_diag_task_overview (id, campaign_id, match_num, download_num, cancel_num, reject_num, success_num, acceptance_num, fail_num, created_at, updated_at) VALUES(283, 247, 1, 1, 0, 0, 1, 1, 0, '2023-04-27 14:44:28.270', '2023-05-11 15:54:56.617');

-- r_ecu_group
INSERT INTO public.r_ecu_group (id, ecu_id, group_id) VALUES(44, 5, 2);
INSERT INTO public.r_ecu_group (id, ecu_id, group_id) VALUES(45, 3, 3);
INSERT INTO public.r_ecu_group (id, ecu_id, group_id) VALUES(46, 4, 3);
INSERT INTO public.r_ecu_group (id, ecu_id, group_id) VALUES(47, 8, 3);
INSERT INTO public.r_ecu_group (id, ecu_id, group_id) VALUES(48, 6, 4);
INSERT INTO public.r_ecu_group (id, ecu_id, group_id) VALUES(55, 1, 1);
INSERT INTO public.r_ecu_group (id, ecu_id, group_id) VALUES(56, 2, 1);
INSERT INTO public.r_ecu_group (id, ecu_id, group_id) VALUES(60, 10, 13);
INSERT INTO public.r_ecu_group (id, ecu_id, group_id) VALUES(61, 11, 13);
INSERT INTO public.r_ecu_group (id, ecu_id, group_id) VALUES(65, 15, 15);
INSERT INTO public.r_ecu_group (id, ecu_id, group_id) VALUES(66, 16, 15);


-- r_ecu_identification_info
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(226, 'DAFA', '8740001DB10000078', 'expect', '', NULL, 1, NULL, NULL, 5, NULL, '2023-04-27 14:55:32.577', '2023-04-27 14:55:32.577');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(227, 'DAFA', '8740001DB10000078', 'target', '', NULL, 1, NULL, NULL, 5, NULL, '2023-04-27 14:55:32.577', '2023-04-27 14:55:32.577');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(230, 'A1FA', '6660020ED10000156', 'expect', NULL, NULL, 1, NULL, NULL, NULL, 1, '2023-04-27 16:22:36.229', '2023-04-27 16:22:36.229');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(209, 'AFA1', '6660020ED10000156', 'expect', NULL, NULL, 1, NULL, NULL, NULL, 3, '2023-04-27 14:09:25.189', '2023-04-27 14:09:25.189');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(231, 'DAFA', '6660020ED10000156', 'expect', '', NULL, 1, NULL, NULL, NULL, 1, '2023-04-27 16:22:36.229', '2023-04-27 16:22:36.229');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(210, 'DA1F', '6660020ED10000156', 'expect', NULL, NULL, 1, NULL, NULL, NULL, 2, '2023-04-27 14:09:32.166', '2023-04-27 14:09:32.166');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(232, 'DAFA', '6660020ED10000156', 'target', NULL, NULL, 1, NULL, NULL, NULL, 1, '2023-04-27 16:22:36.229', '2023-04-27 16:22:36.229');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(211, 'ADA1', '8740001DB10000078', 'expect', NULL, NULL, 1, NULL, NULL, NULL, 4, '2023-04-27 14:09:37.063', '2023-04-27 14:09:37.063');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(169, 'FBAC', NULL, NULL, NULL, NULL, 1, NULL, 3, NULL, NULL, '2023-04-27 13:54:06.863', '2023-04-27 13:54:06.863');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(197, 'FA1C', NULL, NULL, 'FA1C', NULL, 1, 2, NULL, NULL, NULL, '2023-04-27 14:01:36.630', '2023-04-28 13:50:33.918');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(171, 'DAFA', NULL, NULL, NULL, NULL, 1, NULL, 4, NULL, NULL, '2023-04-27 13:54:29.139', '2023-04-27 13:54:29.139');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(216, 'FBAC', NULL, NULL, NULL, NULL, 1, 8, NULL, NULL, NULL, '2023-04-27 14:54:20.381', '2023-04-27 14:54:20.381');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(217, 'DAFA', NULL, NULL, NULL, NULL, 1, 6, NULL, NULL, NULL, '2023-04-27 14:54:27.200', '2023-04-27 14:54:27.200');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(218, 'FBAC', NULL, NULL, NULL, NULL, 1, 4, NULL, NULL, NULL, '2023-04-27 14:54:38.397', '2023-04-27 14:54:38.397');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(240, 'F180', NULL, NULL, 'part number', NULL, 1, 2, NULL, NULL, NULL, '2023-04-28 15:10:37.845', '2023-04-28 15:10:37.845');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(241, 'F180', NULL, NULL, 'part number', NULL, 1, NULL, 1, NULL, NULL, '2023-04-28 15:10:37.845', '2023-04-28 15:10:37.845');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(178, 'AFA1', NULL, NULL, NULL, NULL, 1, NULL, 8, NULL, NULL, '2023-04-27 13:55:50.038', '2023-04-27 13:55:50.038');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(242, 'F180', NULL, NULL, 'part number', NULL, 1, 1, NULL, NULL, NULL, '2023-04-28 15:15:30.421', '2023-04-28 15:15:30.421');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(243, 'F181', NULL, NULL, 'software version', NULL, 1, 1, NULL, NULL, NULL, '2023-04-28 15:15:30.421', '2023-04-28 15:15:30.421');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(246, 'DAFA', '8256001ED10000', 'expect', '', NULL, 1, NULL, NULL, 1, NULL, '2023-05-04 09:29:50.921', '2023-05-04 09:29:50.921');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(247, 'DAFA', '8256001ED10000', 'target', '', '', 1, NULL, NULL, 1, NULL, '2023-05-04 09:29:50.921', '2023-05-04 09:29:50.921');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(248, 'DAFA', '	8525100ED10000H.B', 'expect', '', NULL, 0, NULL, NULL, 6, NULL, '2023-05-04 09:32:41.965', '2023-05-04 09:32:41.965');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(249, 'DAFA', '8525100ED10000H.B', 'target', '', NULL, 1, NULL, NULL, 6, NULL, '2023-05-04 09:32:41.965', '2023-05-04 09:32:41.965');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(199, 'FBAC', NULL, NULL, NULL, NULL, 1, 2, NULL, NULL, NULL, '2023-04-27 14:01:36.630', '2023-04-27 14:01:36.630');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(219, 'FBAC', NULL, NULL, NULL, NULL, 1, 3, NULL, NULL, NULL, '2023-04-27 14:54:45.422', '2023-04-27 14:54:45.422');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(222, 'DAFA', '6660020ED10000156', 'expect', '1', NULL, 1, NULL, NULL, 2, NULL, '2023-04-27 14:55:20.358', '2023-04-27 14:55:20.358');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(223, 'DAFA', '6660020ED10000156', 'target', NULL, NULL, 1, NULL, NULL, 2, NULL, '2023-04-27 14:55:20.358', '2023-04-27 14:55:20.358');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(224, 'DAFA', '8740001DB10000078', 'expect', 'D', NULL, 1, NULL, NULL, 4, NULL, '2023-04-27 14:55:25.711', '2023-04-27 14:55:25.711');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(225, 'DAFA', '8740001DB10000078', 'target', NULL, NULL, 1, NULL, NULL, 4, NULL, '2023-04-27 14:55:25.711', '2023-04-27 14:55:25.711');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(265, 'F101', NULL, NULL, 'Part Number', NULL, 1, NULL, 13, NULL, NULL, '2023-05-17 13:18:56.944', '2023-05-17 13:18:56.944');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(262, 'F101', NULL, NULL, 'Part Number', NULL, 1, 11, NULL, NULL, NULL, '2023-05-17 13:18:28.086', '2023-05-17 13:18:56.944');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(387, 'ADA1', NULL, NULL, NULL, NULL, 1, 10, NULL, NULL, NULL, '2023-05-23 13:25:52.563', '2023-05-23 13:25:52.563');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(388, 'F101', NULL, NULL, 'Part Number', NULL, 1, 10, NULL, NULL, NULL, '2023-05-23 13:25:52.563', '2023-05-23 13:25:52.563');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(268, 'F101', '0003', 'expect', 'Part Number', NULL, 1, NULL, NULL, 12, NULL, '2023-05-17 13:26:57.381', '2023-05-17 13:26:57.381');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(269, 'F101', '0006', 'target', 'Part Number', NULL, 1, NULL, NULL, 12, NULL, '2023-05-17 13:26:57.381', '2023-05-17 13:26:57.381');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(251, 'DAF1', '005', 'expect', 'Part Number', NULL, 1, NULL, NULL, NULL, 6, '2023-05-30 11:38:13.567', '2023-05-30 11:38:13.567');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(252, 'F101', '0006', 'target', 'Part Number', NULL, 1, NULL, NULL, NULL, 6, '2023-05-30 11:38:13.567', '2023-05-30 11:38:13.567');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(253, 'F101', '8525100ED10000H.C', 'expect', 'Part Number', NULL, 1, NULL, NULL, NULL, 7, '2023-05-30 11:39:01.206', '2023-05-30 11:39:01.206');
INSERT INTO public.r_ecu_identification_info (id, did, did_val, identify_group, did_name, description_ti, status, ecu_id, group_id, ecu_version_id, version_id, created_at, updated_at) VALUES(254, 'F101', '0006', 'target', 'Part Number', NULL, 1, NULL, NULL, NULL, 7, '2023-05-30 11:39:01.206', '2023-05-30 11:39:01.206');

-- r_ecu_version
INSERT INTO public.r_ecu_version (id, version_number, version_name, version_type, is_cross, description_ti, release_node_ti, status, ecu_id, created_at, updated_at) VALUES(10, 0, 'GW-B100', 'mandatory', 1, '', '', 3, 2, '2023-04-26 16:20:10.011', '2023-04-26 16:20:10.011');
INSERT INTO public.r_ecu_version (id, version_number, version_name, version_type, is_cross, description_ti, release_node_ti, status, ecu_id, created_at, updated_at) VALUES(2, 1, '101_X', 'optional', 0, '', NULL, 3, 6, '2023-04-07 15:36:13.937', '2023-04-27 14:55:18.873');
INSERT INTO public.r_ecu_version (id, version_number, version_name, version_type, is_cross, description_ti, release_node_ti, status, ecu_id, created_at, updated_at) VALUES(4, 2, '110_X', 'mandatory', 0, '', NULL, 0, 6, '2023-04-12 09:55:38.832', '2023-04-27 14:55:24.228');
INSERT INTO public.r_ecu_version (id, version_number, version_name, version_type, is_cross, description_ti, release_node_ti, status, ecu_id, created_at, updated_at) VALUES(5, 4, '111_X', 'mandatory', 0, '', NULL, 0, 6, '2023-04-12 14:00:44.273', '2023-04-27 14:55:31.092');
INSERT INTO public.r_ecu_version (id, version_number, version_name, version_type, is_cross, description_ti, release_node_ti, status, ecu_id, created_at, updated_at) VALUES(7, 0, '201_B', 'mandatory', 0, '', NULL, 3, 1, '2023-04-13 10:02:26.297', '2023-04-27 14:56:01.649');
INSERT INTO public.r_ecu_version (id, version_number, version_name, version_type, is_cross, description_ti, release_node_ti, status, ecu_id, created_at, updated_at) VALUES(9, 7, '202_B', 'mandatory', 1, '', '', 0, 1, '2023-04-25 15:17:02.138', '2023-04-27 14:56:07.396');
INSERT INTO public.r_ecu_version (id, version_number, version_name, version_type, is_cross, description_ti, release_node_ti, status, ecu_id, created_at, updated_at) VALUES(3, 0, '3001_S', 'mandatory', 0, '', NULL, 3, 5, '2023-04-07 16:14:55.226', '2023-04-27 14:56:42.405');
INSERT INTO public.r_ecu_version (id, version_number, version_name, version_type, is_cross, description_ti, release_node_ti, status, ecu_id, created_at, updated_at) VALUES(1, 0, '100_X', 'mandatory', 0, '', NULL, 3, 6, '2023-04-07 15:25:40.760', '2023-05-04 09:29:50.870');
INSERT INTO public.r_ecu_version (id, version_number, version_name, version_type, is_cross, description_ti, release_node_ti, status, ecu_id, created_at, updated_at) VALUES(6, 5, '112_X', 'mandatory', 0, '', NULL, 3, 6, '2023-04-13 10:01:59.470', '2023-05-04 09:32:41.961');
INSERT INTO public.r_ecu_version (id, version_number, version_name, version_type, is_cross, description_ti, release_node_ti, status, ecu_id, created_at, updated_at) VALUES(12, 0, 'ABS_01', 'optional', 0, '', '', 3, 11, '2023-05-17 13:21:27.036', '2023-05-17 13:26:57.377');

-- r_ecu_version_file
INSERT INTO public.r_ecu_version_file (id, version_id, file_type, flash_type, file_name, file_md5, file_path, status, created_at, updated_at) VALUES(52, 4, 'releaseNote', NULL, 'release note.zip', '0f0af0b54f73b3a5b77877cc78d097ff', '\usr\file\remote_diagnostic\ecuVersion\2023-04-12\release note_1681265237705.zip', 0, '2023-04-27 14:55:25.711', '2023-04-27 14:55:25.711');
INSERT INTO public.r_ecu_version_file (id, version_id, file_type, flash_type, file_name, file_md5, file_path, status, created_at, updated_at) VALUES(53, 4, 'flashFile', 'APP', 'app', '45c1ccfa28a82ea738e5b887fcbbf47f', '\usr\file\remote_diagnostic\ecuVersion\2023-04-12\app_1681266110970', 0, '2023-04-27 14:55:25.711', '2023-04-27 14:55:25.711');
INSERT INTO public.r_ecu_version_file (id, version_id, file_type, flash_type, file_name, file_md5, file_path, status, created_at, updated_at) VALUES(59, 3, 'releaseNote', NULL, 'release note.zip', '0f0af0b54f73b3a5b77877cc78d097ff', '\usr\file\remote_diagnostic\ecuVersion\2023-04-07\release note_1680855940061.zip', 0, '2023-04-27 14:56:43.893', '2023-04-27 14:56:43.893');
INSERT INTO public.r_ecu_version_file (id, version_id, file_type, flash_type, file_name, file_md5, file_path, status, created_at, updated_at) VALUES(60, 6, 'releaseNote', NULL, 'release note.zip', '0f0af0b54f73b3a5b77877cc78d097ff', '\usr\file\remote_diagnostic\ecuVersion\2023-04-13\release note_1681350858554.zip', 0, '2023-05-04 09:32:41.965', '2023-05-04 09:32:41.965');
INSERT INTO public.r_ecu_version_file (id, version_id, file_type, flash_type, file_name, file_md5, file_path, status, created_at, updated_at) VALUES(61, 6, 'flashFile', 'APP', 'app', '45c1ccfa28a82ea738e5b887fcbbf47f', '\usr\file\remote_diagnostic\ecuVersion\2023-04-13\app_1681351147721', 0, '2023-05-04 09:32:41.965', '2023-05-04 09:32:41.965');
INSERT INTO public.r_ecu_version_file (id, version_id, file_type, flash_type, file_name, file_md5, file_path, status, created_at, updated_at) VALUES(62, 6, 'flashFile', 'DATA', 'data', '45c1ccfa28a82ea738e5b887fcbbf47f', '\usr\file\remote_diagnostic\ecuVersion\2023-04-13\data_1681351154803', 0, '2023-05-04 09:32:41.965', '2023-05-04 09:32:41.965');
INSERT INTO public.r_ecu_version_file (id, version_id, file_type, flash_type, file_name, file_md5, file_path, status, created_at, updated_at) VALUES(63, 6, 'testDocumentation', NULL, 'Test Documentation.xlsx', '94098fe05b1b0e0a05a55d525c3c983e', '\usr\file\remote_diagnostic\ecuVersion\2023-04-17\Test Documentation_1681712872415.xlsx', 0, '2023-05-04 09:32:41.965', '2023-05-04 09:32:41.965');
INSERT INTO public.r_ecu_version_file (id, version_id, file_type, flash_type, file_name, file_md5, file_path, status, created_at, updated_at) VALUES(65, 12, 'flashFile', 'APP', 'Chery_E0X_UWB_HUB_R220_BOOT_flashdriver_1209.hex', '833502782b8417893c4bc59ccfb24bd7', '/usr/file/remote_diagnostic/ecuVersion/2023-05-17/Chery_E0X_UWB_HUB_R220_BOOT_flashdriver_1209_1684301181886.hex', 1, '2023-05-17 13:26:57.381', '2023-05-17 13:26:57.381');
INSERT INTO public.r_ecu_version_file (id, version_id, file_type, flash_type, file_name, file_md5, file_path, status, created_at, updated_at) VALUES(66, 12, 'flashFile', 'DATA', 'Chery_EH3_UWB_HUB_R220_RS3_APP_v00.01.01_BOOT_10.1.1.hex', '824a8b7de3344a13eb7ed2d16b3b2dfe', '/usr/file/remote_diagnostic/ecuVersion/2023-05-17/Chery_EH3_UWB_HUB_R220_RS3_APP_v00_1684301195106.01.01_BOOT_10.1.1.hex', 1, '2023-05-17 13:26:57.381', '2023-05-17 13:26:57.381');

-- r_pool_depend
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(321, 2, NULL, NULL, NULL, 2);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(322, 2, 23, NULL, NULL, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(323, 2, 27, NULL, NULL, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(324, 2, 63, NULL, NULL, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(325, 2, NULL, 1, NULL, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(333, 4, 5, NULL, NULL, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(334, 4, 19, NULL, NULL, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(335, 4, 20, NULL, NULL, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(336, 4, NULL, 1, NULL, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(337, 4, NULL, 3, NULL, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(338, 4, NULL, NULL, 2, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(339, 4, NULL, NULL, 6, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(400, 1, NULL, NULL, NULL, 2);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(401, 1, 57, NULL, NULL, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(402, 1, 58, NULL, NULL, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(298, 3, NULL, NULL, NULL, 2);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(299, 3, 4, NULL, NULL, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(300, 3, 47, NULL, NULL, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(301, 3, NULL, 3, NULL, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(403, 1, 59, NULL, NULL, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(404, 1, 60, NULL, NULL, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(405, 1, 61, NULL, NULL, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(406, 1, 7, NULL, NULL, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(407, 1, 21, NULL, NULL, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(408, 1, 23, NULL, NULL, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(409, 1, 27, NULL, NULL, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(410, 1, 63, NULL, NULL, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(302, 3, NULL, NULL, 3, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(303, 3, NULL, NULL, 6, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(411, 1, 22, NULL, NULL, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(412, 1, 62, NULL, NULL, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(413, 1, NULL, 3, NULL, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(414, 1, NULL, 1, NULL, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(415, 1, NULL, NULL, 2, NULL);
INSERT INTO public.r_pool_depend (id, pool_id, vehicle_type_id, vehicle_version_id, ecu_version_id, tag_id) VALUES(416, 1, NULL, NULL, 6, NULL);

-- r_pool_vehicle
INSERT INTO public.r_pool_vehicle (id, pool_id, config_id, param_group) VALUES(38, 3, 11, 'V_SalesAreas');
INSERT INTO public.r_pool_vehicle (id, pool_id, config_id, param_group) VALUES(39, 3, 12, 'V_SalesAreas');
INSERT INTO public.r_pool_vehicle (id, pool_id, config_id, param_group) VALUES(40, 3, 9, 'V_PlaceOfProduction');
INSERT INTO public.r_pool_vehicle (id, pool_id, config_id, param_group) VALUES(41, 3, 8, 'V_ProductionDate');
INSERT INTO public.r_pool_vehicle (id, pool_id, config_id, param_group) VALUES(45, 2, 9, 'V_PlaceOfProduction');
INSERT INTO public.r_pool_vehicle (id, pool_id, config_id, param_group) VALUES(49, 4, 11, 'V_SalesAreas');
INSERT INTO public.r_pool_vehicle (id, pool_id, config_id, param_group) VALUES(50, 4, 12, 'V_SalesAreas');
INSERT INTO public.r_pool_vehicle (id, pool_id, config_id, param_group) VALUES(51, 4, 9, 'V_PlaceOfProduction');
INSERT INTO public.r_pool_vehicle (id, pool_id, config_id, param_group) VALUES(68, 1, 11, 'V_SalesAreas');
INSERT INTO public.r_pool_vehicle (id, pool_id, config_id, param_group) VALUES(69, 1, 12, 'V_SalesAreas');
INSERT INTO public.r_pool_vehicle (id, pool_id, config_id, param_group) VALUES(70, 1, 9, 'V_PlaceOfProduction');
INSERT INTO public.r_pool_vehicle (id, pool_id, config_id, param_group) VALUES(71, 1, 10, 'V_PlaceOfProduction');
INSERT INTO public.r_pool_vehicle (id, pool_id, config_id, param_group) VALUES(72, 1, 8, 'V_ProductionDate');

-- r_tag
INSERT INTO public.r_tag (id, tag_name, description_ti, created_at, updated_at) VALUES(5, 'Concept Car', 'Concept Car', '2023-05-08 14:16:09.362', '2023-05-08 14:16:09.362');
INSERT INTO public.r_tag (id, tag_name, description_ti, created_at, updated_at) VALUES(6, 'Production Car', 'production car', '2023-05-08 14:18:08.258', '2023-05-08 14:18:08.258');
INSERT INTO public.r_tag (id, tag_name, description_ti, created_at, updated_at) VALUES(2, 'Prototype Vehicle', 'Prototype vehicle', '2023-04-13 10:06:24.548', '2023-05-08 14:21:02.762');

-- r_task_condition
INSERT INTO public.r_task_condition (id, raw_id, task_id, condition_name, condition_code, condition_val, unity, min_val, max_val, eq_val, in_val, created_at, updated_at) VALUES(75, 2, 5, 'Temperature', '2', 6, '°C', '0', '100', '', '', '2023-04-27 14:41:34.604', '2023-04-27 14:41:34.604');
INSERT INTO public.r_task_condition (id, raw_id, task_id, condition_name, condition_code, condition_val, unity, min_val, max_val, eq_val, in_val, created_at, updated_at) VALUES(77, 1, 2, 'Speed', '1', 4, 'km/h', '', '180', '', '', '2023-04-27 14:42:24.223', '2023-04-27 14:42:24.223');
INSERT INTO public.r_task_condition (id, raw_id, task_id, condition_name, condition_code, condition_val, unity, min_val, max_val, eq_val, in_val, created_at, updated_at) VALUES(83, 1, 6, 'Speed', '1', 4, 'km/h', '', '35', '', '', '2023-05-05 13:50:31.764', '2023-05-05 13:50:31.764');

-- r_task_ecu
INSERT INTO public.r_task_ecu (id, task_id, fun_id, ecu_group_id, ecu_id, created_at, updated_at) VALUES(18, 6, 2, 1, 2, '2023-05-05 13:50:31.764', '2023-05-05 13:50:31.764');
INSERT INTO public.r_task_ecu (id, task_id, fun_id, ecu_group_id, ecu_id, created_at, updated_at) VALUES(19, 6, 1, 2, NULL, '2023-05-05 13:50:31.764', '2023-05-05 13:50:31.764');
INSERT INTO public.r_task_ecu (id, task_id, fun_id, ecu_group_id, ecu_id, created_at, updated_at) VALUES(20, 6, 1, 3, NULL, '2023-05-05 13:50:31.764', '2023-05-05 13:50:31.764');
INSERT INTO public.r_task_ecu (id, task_id, fun_id, ecu_group_id, ecu_id, created_at, updated_at) VALUES(21, 6, 1, 4, 6, '2023-05-05 13:50:31.764', '2023-05-05 13:50:31.764');
INSERT INTO public.r_task_ecu (id, task_id, fun_id, ecu_group_id, ecu_id, created_at, updated_at) VALUES(22, 6, 1, 1, 2, '2023-05-05 13:50:31.764', '2023-05-05 13:50:31.764');
-- r_task_file
INSERT INTO public.r_task_file (id, task_id, file_id) VALUES(3, 2, 3);
INSERT INTO public.r_task_file (id, task_id, file_id) VALUES(5, 5, 3);

-- r_vehicle_ecu
INSERT INTO public.r_vehicle_ecu (id, ecu_name, description_ti, short_name_ti, status, created_at, updated_at) VALUES(2, 'GW-B', 'UAES Gateway', 'GW-B', 0, '2023-04-07 15:23:26.112', '2023-04-27 14:01:35.173');
INSERT INTO public.r_vehicle_ecu (id, ecu_name, description_ti, short_name_ti, status, created_at, updated_at) VALUES(8, 'AMP-C', 'BOSCH  Advanced Mechanical Product', 'AMP-C', 2, '2023-04-13 09:40:43.937', '2023-04-27 14:54:18.896');
INSERT INTO public.r_vehicle_ecu (id, ecu_name, description_ti, short_name_ti, status, created_at, updated_at) VALUES(6, 'AVAS-A', 'BOSCH Acoustic Vehicle Alerting System', 'AVAS-A', 0, '2023-04-07 15:24:17.812', '2023-04-27 14:54:25.714');
INSERT INTO public.r_vehicle_ecu (id, ecu_name, description_ti, short_name_ti, status, created_at, updated_at) VALUES(5, 'ATLS-A', 'BOSCH Annotated Labeled Transition System', 'ATLS-A', 0, '2023-04-07 15:24:05.844', '2023-04-27 14:54:31.761');
INSERT INTO public.r_vehicle_ecu (id, ecu_name, description_ti, short_name_ti, status, created_at, updated_at) VALUES(4, 'AMP-B', 'BOSCH Advanced Mechanical Product', 'AMP-B', 0, '2023-04-07 15:23:47.210', '2023-04-27 14:54:36.911');
INSERT INTO public.r_vehicle_ecu (id, ecu_name, description_ti, short_name_ti, status, created_at, updated_at) VALUES(3, 'AMP-A', 'UAES Advanced Mechanical Product', 'AMP-A', 2, '2023-04-07 15:23:38.340', '2023-04-27 14:54:43.936');
INSERT INTO public.r_vehicle_ecu (id, ecu_name, description_ti, short_name_ti, status, created_at, updated_at) VALUES(1, 'GW-A', 'BOSCH Gateway', 'GW-A', 0, '2023-04-07 15:23:15.863', '2023-04-28 15:15:30.417');
INSERT INTO public.r_vehicle_ecu (id, ecu_name, description_ti, short_name_ti, status, created_at, updated_at) VALUES(11, 'ABS NeuSoft', '', 'ABS NeuSoft', 0, '2023-05-17 13:18:28.086', '2023-05-17 13:18:28.086');
INSERT INTO public.r_vehicle_ecu (id, ecu_name, description_ti, short_name_ti, status, created_at, updated_at) VALUES(10, 'ABS Bosch', '', 'ABS_Bosch', 2, '2023-05-17 13:17:57.502', '2023-05-23 13:25:52.561');
INSERT INTO public.r_vehicle_ecu (id, ecu_name, description_ti, short_name_ti, status, created_at, updated_at) VALUES(15, 'BMS_QR01', 'BMS_QR01', '', 0, '2023-06-13 10:33:37.286', '2023-06-13 10:33:47.785');
INSERT INTO public.r_vehicle_ecu (id, ecu_name, description_ti, short_name_ti, status, created_at, updated_at) VALUES(16, 'BMS_QR02', 'BMS_QR02', '', 0, '2023-06-13 10:34:00.398', '2023-06-13 10:34:00.398');

-- r_vehicle_ecu_group
INSERT INTO public.r_vehicle_ecu_group (id, group_name, description_ti, short_name_ti, status, created_at, updated_at) VALUES(2, 'ALTS', 'Annotated Labeled Transition System', 'ALTS', 0, '2023-04-07 15:22:21.142', '2023-04-27 13:53:13.897');
INSERT INTO public.r_vehicle_ecu_group (id, group_name, description_ti, short_name_ti, status, created_at, updated_at) VALUES(3, 'AMP', 'Advanced Mechanical Product', 'AMP', 0, '2023-04-07 15:22:35.739', '2023-04-27 13:54:05.415');
INSERT INTO public.r_vehicle_ecu_group (id, group_name, description_ti, short_name_ti, status, created_at, updated_at) VALUES(4, 'AVAS', 'Acoustic Vehicle Alerting System', 'AVAS', 0, '2023-04-07 15:22:56.994', '2023-04-27 13:54:27.689');
INSERT INTO public.r_vehicle_ecu_group (id, group_name, description_ti, short_name_ti, status, created_at, updated_at) VALUES(5, 'ADAS', 'Advanced Driving Assistant System', 'ADAS', 2, '2023-04-10 14:02:19.153', '2023-04-27 13:55:18.815');
INSERT INTO public.r_vehicle_ecu_group (id, group_name, description_ti, short_name_ti, status, created_at, updated_at) VALUES(8, 'BCM', 'Body Control Module', 'BCM', 2, '2023-04-13 09:32:45.043', '2023-04-27 13:55:48.588');
INSERT INTO public.r_vehicle_ecu_group (id, group_name, description_ti, short_name_ti, status, created_at, updated_at) VALUES(1, 'GW', 'Gateway', 'GW', 0, '2023-04-07 15:22:10.395', '2023-04-28 15:10:37.843');
INSERT INTO public.r_vehicle_ecu_group (id, group_name, description_ti, short_name_ti, status, created_at, updated_at) VALUES(13, 'ABS', '', 'ABS', 0, '2023-05-17 13:17:34.368', '2023-05-17 13:18:56.941');
INSERT INTO public.r_vehicle_ecu_group (id, group_name, description_ti, short_name_ti, status, created_at, updated_at) VALUES(15, 'BMS', 'Battery Management System', 'BMS', 0, '2023-06-13 10:19:34.968', '2023-06-13 10:33:15.022');

-- r_vehicle_pool
INSERT INTO public.r_vehicle_pool (id, "name", description_ti, status, created_at, updated_at) VALUES(3, 'E203', NULL, 0, '2023-04-07 16:32:03.812', '2023-04-27 14:11:20.691');
INSERT INTO public.r_vehicle_pool (id, "name", description_ti, status, created_at, updated_at) VALUES(2, 'E28', 'XPENG E28', 0, '2023-04-07 16:31:35.829', '2023-04-27 15:09:35.380');
INSERT INTO public.r_vehicle_pool (id, "name", description_ti, status, created_at, updated_at) VALUES(4, 'E202', NULL, 2, '2023-04-26 13:43:57.181', '2023-04-27 15:09:59.021');
INSERT INTO public.r_vehicle_pool (id, "name", description_ti, status, created_at, updated_at) VALUES(1, 'D55', 'XPENG D55', 0, '2023-04-07 16:30:59.129', '2023-05-05 13:54:11.656');

-- r_vehicle_tag
INSERT INTO public.r_vehicle_tag (id, vin, tag_id) VALUES(43, 'LJ1E6A2U4K7708977', 2);
INSERT INTO public.r_vehicle_tag (id, vin, tag_id) VALUES(62, 'LGAG3DV32N8021910', 2);
INSERT INTO public.r_vehicle_tag (id, vin, tag_id) VALUES(63, 'LGAG4DY30N8011563', 2);
INSERT INTO public.r_vehicle_tag (id, vin, tag_id) VALUES(64, 'LGAG3DV34N8021777', 2);
INSERT INTO public.r_vehicle_tag (id, vin, tag_id) VALUES(65, 'LGAG3DV33N8021902', 2);
INSERT INTO public.r_vehicle_tag (id, vin, tag_id) VALUES(66, 'LGAG3DV34N8021651', 2);
INSERT INTO public.r_vehicle_tag (id, vin, tag_id) VALUES(67, 'LGAG4DY37N8005694', 2);
INSERT INTO public.r_vehicle_tag (id, vin, tag_id) VALUES(68, 'LGAG4DY30N8009330', 2);
INSERT INTO public.r_vehicle_tag (id, vin, tag_id) VALUES(69, 'LGAG3DV31N8020926', 2);
INSERT INTO public.r_vehicle_tag (id, vin, tag_id) VALUES(70, 'LGAG3DV37N8019084', 2);

-- r_vehicle_type_ecu
INSERT INTO public.r_vehicle_type_ecu (id, type_id, ecu_group_id, ecu_id) VALUES(48, 7, 10, NULL);
INSERT INTO public.r_vehicle_type_ecu (id, type_id, ecu_group_id, ecu_id) VALUES(56, 58, 1, 1);
INSERT INTO public.r_vehicle_type_ecu (id, type_id, ecu_group_id, ecu_id) VALUES(57, 59, 2, NULL);
INSERT INTO public.r_vehicle_type_ecu (id, type_id, ecu_group_id, ecu_id) VALUES(58, 59, 1, NULL);
INSERT INTO public.r_vehicle_type_ecu (id, type_id, ecu_group_id, ecu_id) VALUES(60, 7, 4, 6);
INSERT INTO public.r_vehicle_type_ecu (id, type_id, ecu_group_id, ecu_id) VALUES(61, 7, 3, NULL);
INSERT INTO public.r_vehicle_type_ecu (id, type_id, ecu_group_id, ecu_id) VALUES(62, 7, 2, 5);
INSERT INTO public.r_vehicle_type_ecu (id, type_id, ecu_group_id, ecu_id) VALUES(63, 22, 1, NULL);
INSERT INTO public.r_vehicle_type_ecu (id, type_id, ecu_group_id, ecu_id) VALUES(64, 69, 13, NULL);
INSERT INTO public.r_vehicle_type_ecu (id, type_id, ecu_group_id, ecu_id) VALUES(65, 69, 8, NULL);
INSERT INTO public.r_vehicle_type_ecu (id, type_id, ecu_group_id, ecu_id) VALUES(66, 69, 5, NULL);
INSERT INTO public.r_vehicle_type_ecu (id, type_id, ecu_group_id, ecu_id) VALUES(67, 69, 4, 6);
INSERT INTO public.r_vehicle_type_ecu (id, type_id, ecu_group_id, ecu_id) VALUES(68, 69, 3, NULL);
INSERT INTO public.r_vehicle_type_ecu (id, type_id, ecu_group_id, ecu_id) VALUES(69, 69, 2, 5);
INSERT INTO public.r_vehicle_type_ecu (id, type_id, ecu_group_id, ecu_id) VALUES(70, 69, 1, NULL);

-- r_vehicle_version
INSERT INTO public.r_vehicle_version (id, type_id, version_number, version_name, version_type, is_cross, description_ti, release_node_ti, status, created_at, updated_at) VALUES(3, 4, 0, '200', 'optional', 0, '', 'Version iteration', 3, '2023-04-07 16:32:38.142', '2023-04-27 14:09:23.732');
INSERT INTO public.r_vehicle_version (id, type_id, version_number, version_name, version_type, is_cross, description_ti, release_node_ti, status, created_at, updated_at) VALUES(2, 22, 0, '101', 'mandatory', 0, '', 'Version iteration', 0, '2023-04-07 16:27:39.908', '2023-04-27 14:09:30.708');
INSERT INTO public.r_vehicle_version (id, type_id, version_number, version_name, version_type, is_cross, description_ti, release_node_ti, status, created_at, updated_at) VALUES(4, 22, 2, '102', 'mandatory', 0, '', 'Version iteration', 0, '2023-04-12 14:01:41.600', '2023-04-27 14:09:35.488');
INSERT INTO public.r_vehicle_version (id, type_id, version_number, version_name, version_type, is_cross, description_ti, release_node_ti, status, created_at, updated_at) VALUES(1, 23, 0, '100', 'mandatory', 0, '', 'Version iteration', 3, '2023-04-07 16:27:14.386', '2023-04-27 16:22:36.221');
INSERT INTO public.r_vehicle_version (id, type_id, version_number, version_name, version_type, is_cross, description_ti, release_node_ti, status, created_at, updated_at) VALUES(6, 69, 0, 'V0.1', 'optional', 0, '', '', 3, '2023-05-30 11:38:13.567', '2023-05-30 11:38:13.567');
INSERT INTO public.r_vehicle_version (id, type_id, version_number, version_name, version_type, is_cross, description_ti, release_node_ti, status, created_at, updated_at) VALUES(7, 69, 6, 'V1.0', 'optional', 0, '', '', 3, '2023-05-30 11:39:01.206', '2023-05-30 11:39:01.206');

-- r_vehicle_version_ecu_group
INSERT INTO public.r_vehicle_version_ecu_group (id, version_id, ecu_id, group_id, ecu_version_id) VALUES(23, 2, 5, 2, 3);
INSERT INTO public.r_vehicle_version_ecu_group (id, version_id, ecu_id, group_id, ecu_version_id) VALUES(24, 1, 1, 1, 7);
INSERT INTO public.r_vehicle_version_ecu_group (id, version_id, ecu_id, group_id, ecu_version_id) VALUES(25, 1, 5, 2, 3);
INSERT INTO public.r_vehicle_version_ecu_group (id, version_id, ecu_id, group_id, ecu_version_id) VALUES(26, 1, 6, 4, 2);
INSERT INTO public.r_vehicle_version_ecu_group (id, version_id, ecu_id, group_id, ecu_version_id) VALUES(28, 6, 5, 2, 3);
INSERT INTO public.r_vehicle_version_ecu_group (id, version_id, ecu_id, group_id, ecu_version_id) VALUES(29, 7, 1, 1, 7);

-- r_version_ecu_rely
INSERT INTO public.r_version_ecu_rely (id, group_id, ecu_id, ecu_version_id, priority) VALUES(35, 'b31162b97aec4f14ae778daacdc29d7d', 1, 9, NULL);
INSERT INTO public.r_version_ecu_rely (id, group_id, ecu_id, ecu_version_id, priority) VALUES(42, '3903d3ecf42b4cb08d6d6c4dd2c02e45', 2, 10, NULL);
INSERT INTO public.r_version_ecu_rely (id, group_id, ecu_id, ecu_version_id, priority) VALUES(46, 'bdc57a77619c4d2b9837cb89cad163fa', 6, 1, NULL);
INSERT INTO public.r_version_ecu_rely (id, group_id, ecu_id, ecu_version_id, priority) VALUES(47, 'c2959f3b5a064931964461226545cf45', 1, 7, NULL);
INSERT INTO public.r_version_ecu_rely (id, group_id, ecu_id, ecu_version_id, priority) VALUES(48, 'c2959f3b5a064931964461226545cf45', 5, 3, NULL);
INSERT INTO public.r_version_ecu_rely (id, group_id, ecu_id, ecu_version_id, priority) VALUES(49, 'c2959f3b5a064931964461226545cf45', 6, 6, NULL);


 -- r_diag_task_detail
INSERT INTO public.r_diag_task_detail (id, campaign_id, "type", session_id, vin, start_time, end_time, "action", code, message, created_at, updated_at) VALUES(6, 246, 'MATCH', '792917c97b824fbe9226a08896363315', 'LJ1E6A2U4K7708977', '2023-05-11 15:41:02.702', NULL, 1, '0', NULL, '2023-05-11 15:41:03.252', '2023-05-11 15:41:03.252');
INSERT INTO public.r_diag_task_detail (id, campaign_id, "type", session_id, vin, start_time, end_time, "action", code, message, created_at, updated_at) VALUES(7, 247, 'MATCH', 'adf739e3e78e4f1fa7a1181d3861b1a9', 'LJ1E6A2U4K7708977', '2023-05-11 15:41:10.827', NULL, 1, '0', NULL, '2023-05-11 15:41:11.375', '2023-05-11 15:41:11.375');
INSERT INTO public.r_diag_task_detail (id, campaign_id, "type", session_id, vin, start_time, end_time, "action", code, message, created_at, updated_at) VALUES(8, 247, 'DOWNLOAD', 'adf739e3e78e4f1fa7a1181d3861b1a9', 'LJ1E6A2U4K7708977', '2023-05-11 15:43:49.864', NULL, 2, '0', NULL, '2023-05-11 15:43:50.419', '2023-05-11 15:43:50.419');
INSERT INTO public.r_diag_task_detail (id, campaign_id, "type", session_id, vin, start_time, end_time, "action", code, message, created_at, updated_at) VALUES(9, 246, 'DOWNLOAD', '792917c97b824fbe9226a08896363315', 'LJ1E6A2U4K7708977', '2023-05-11 15:43:58.461', NULL, 2, '0', NULL, '2023-05-11 15:43:59.019', '2023-05-11 15:43:59.019');
INSERT INTO public.r_diag_task_detail (id, campaign_id, "type", session_id, vin, start_time, end_time, "action", code, message, created_at, updated_at) VALUES(10, 246, 'RemoteVerification', '792917c97b824fbe9226a08896363315', 'LJ1E6A2U4K7708977', '2023-05-11 15:44:22.719', NULL, 3, '0', NULL, '2023-05-11 15:44:23.280', '2023-05-11 15:44:23.280');
INSERT INTO public.r_diag_task_detail (id, campaign_id, "type", session_id, vin, start_time, end_time, "action", code, message, created_at, updated_at) VALUES(11, 247, 'RemoteVerification', 'adf739e3e78e4f1fa7a1181d3861b1a9', 'LJ1E6A2U4K7708977', '2023-05-11 15:44:29.808', NULL, 3, '0', NULL, '2023-05-11 15:44:30.371', '2023-05-11 15:44:30.371');
INSERT INTO public.r_diag_task_detail (id, campaign_id, "type", session_id, vin, start_time, end_time, "action", code, message, created_at, updated_at) VALUES(12, 247, 'ConfirmExecution:EXECUTE', 'adf739e3e78e4f1fa7a1181d3861b1a9', 'LJ1E6A2U4K7708977', '2023-05-11 15:53:58.027', NULL, 4, '0', NULL, '2023-05-11 15:53:58.599', '2023-05-11 15:53:58.599');
INSERT INTO public.r_diag_task_detail (id, campaign_id, "type", session_id, vin, start_time, end_time, "action", code, message, created_at, updated_at) VALUES(13, 246, 'ConfirmExecution:EXECUTE', '792917c97b824fbe9226a08896363315', 'LJ1E6A2U4K7708977', '2023-05-11 15:54:05.525', NULL, 4, '0', NULL, '2023-05-11 15:54:06.075', '2023-05-11 15:54:06.075');
INSERT INTO public.r_diag_task_detail (id, campaign_id, "type", session_id, vin, start_time, end_time, "action", code, message, created_at, updated_at) VALUES(14, 246, 'DiagnosticResult', '792917c97b824fbe9226a08896363315', 'LJ1E6A2U4K7708977', '2023-05-11 12:00:00.000', '2023-05-11 12:00:10.000', NULL, '0', NULL, '2023-05-11 15:54:46.958', '2023-05-11 15:54:46.958');
INSERT INTO public.r_diag_task_detail (id, campaign_id, "type", session_id, vin, start_time, end_time, "action", code, message, created_at, updated_at) VALUES(15, 247, 'DiagnosticResult', 'adf739e3e78e4f1fa7a1181d3861b1a9', 'LJ1E6A2U4K7708977', '2023-05-11 12:00:00.000', '2023-05-11 12:00:10.000', NULL, '0', NULL, '2023-05-11 15:54:57.147', '2023-05-11 15:54:57.147');

-- r_diag_task_record
INSERT INTO public.r_diag_task_record (id, campaign_id, session_id, vin, executions, match_status, created_at, updated_at) VALUES(1, 246, '792917c97b824fbe9226a08896363315', 'LJ1E6A2U4K7708977', 1, 0, '2023-05-11 15:35:07.911', '2023-05-11 15:54:46.655');
INSERT INTO public.r_diag_task_record (id, campaign_id, session_id, vin, executions, match_status, created_at, updated_at) VALUES(2, 247, 'adf739e3e78e4f1fa7a1181d3861b1a9', 'LJ1E6A2U4K7708977', 1, 0, '2023-05-11 15:35:07.911', '2023-05-11 15:54:56.617');

-- r_diag_task_active
INSERT INTO public.r_diag_task_active (id, campaign_id, session_id, vin, status, mes, created_at, updated_at) VALUES(4, 246, '792917c97b824fbe9226a08896363315', 'LJ1E6A2U4K7708977', 1, NULL, '2023-05-11 15:54:06.075', '2023-05-11 15:54:46.655');
INSERT INTO public.r_diag_task_active (id, campaign_id, session_id, vin, status, mes, created_at, updated_at) VALUES(3, 247, 'adf739e3e78e4f1fa7a1181d3861b1a9', 'LJ1E6A2U4K7708977', 1, NULL, '2023-05-11 15:53:58.599', '2023-05-11 15:54:56.617');

--commit;
---- 更新序列

select setval('r_diag_condition_id_seq', (select max(i.id)+1 from r_diag_condition i));
select setval('r_diag_file_id_seq', (select max(i.id)+1 from r_diag_file i));
select setval('r_diag_fun_id_seq', (select max(i.id)+1 from r_diag_fun i));
select setval('r_diag_task_active_id_seq', (select max(i.id)+1 from r_diag_task_active i));
select setval('r_diag_task_detail_id_seq', (select max(i.id)+1 from r_diag_task_detail i));
select setval('r_diag_task_id_seq', (select max(i.id)+1 from r_diag_task i));
select setval('r_diag_task_log_id_seq', (select max(i.id)+1 from r_diag_task_log i));
select setval('r_diag_task_overview_id_seq', (select max(i.id)+1 from r_diag_task_overview i));
select setval('r_diag_task_record_id_seq', (select max(i.id)+1 from r_diag_task_record i));
select setval('r_diag_task_campaign_id_seq', (select max(i.id)+1 from r_diag_task_campaign i));

select setval('r_ecu_group_id_seq', (select max(i.id)+1 from r_ecu_group i));
select setval('r_ecu_identification_info_id_seq', (select max(i.id)+1 from r_ecu_identification_info i));
select setval('r_ecu_version_file_id_seq', (select max(i.id)+1 from r_ecu_version_file i));
select setval('r_ecu_version_id_seq', (select max(i.id)+1 from r_ecu_version i));
select setval('r_pool_depend_id_seq', (select max(i.id)+1 from r_pool_depend i));
select setval('r_pool_vehicle_id_seq', (select max(i.id)+1 from r_pool_vehicle i));

select setval('r_tag_id_seq', (select max(i.id)+1 from r_tag i));
select setval('r_task_condition_id_seq', (select max(i.id)+1 from r_task_condition i));
select setval('r_task_ecu_id_seq', (select max(i.id)+1 from r_task_ecu i));
select setval('r_task_file_id_seq', (select max(i.id)+1 from r_task_file i));

select setval('r_vehicle_ecu_group_id_seq', (select max(i.id)+1 from r_vehicle_ecu_group i));
select setval('r_vehicle_ecu_id_seq', (select max(i.id)+1 from r_vehicle_ecu i));
select setval('r_vehicle_ecu_image_id_seq', (select max(i.id)+1 from r_vehicle_ecu_image i));
select setval('r_vehicle_pool_id_seq', (select max(i.id)+1 from r_vehicle_pool i));
select setval('r_vehicle_tag_id_seq', (select max(i.id)+1 from r_vehicle_tag i));
select setval('r_vehicle_type_ecu_id_seq', (select max(i.id)+1 from r_vehicle_type_ecu i));
select setval('r_vehicle_version_ecu_group_id_seq', (select max(i.id)+1 from r_vehicle_version_ecu_group i));
select setval('r_vehicle_version_id_seq', (select max(i.id)+1 from r_vehicle_version i));
select setval('r_version_ecu_rely_id_seq', (select max(i.id)+1 from r_version_ecu_rely i));
