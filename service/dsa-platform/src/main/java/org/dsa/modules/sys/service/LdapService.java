package org.dsa.modules.sys.service;

import org.dsa.modules.sys.entity.LdapUserEntity;
import org.dsa.modules.sys.entity.LdapWorkshopEntity;
import java.util.List;

public interface LdapService  {

    //获取LDAP work shop
    public List<LdapWorkshopEntity> getWorkshop();

    //新增LDAP work shop
    public void insertWorkshop(LdapWorkshopEntity ldapWorkshopEntity);

    //修改LDAP work shop
    public void updateWorkshop(LdapWorkshopEntity ldapWorkshopEntity);

    //禁用LDAP work shop
    public void disableWorkshop(LdapWorkshopEntity ldapWorkshopEntity);

    //获取LDAP User
    public List<LdapUserEntity> getLdapUser(String uid,String status,String accountCreateDate);

    //新增LDAP User
    public void insertLdapUser(LdapUserEntity ldapUserEntity);

    //修改LDAP User
    public void updateLdapUser(LdapUserEntity ldapUserEntity);

    //禁用LDAP User
    public void disableLdapUser(LdapUserEntity ldapUserEntity);

}
