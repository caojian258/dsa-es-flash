package org.dsa.modules.reporter.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.reporter.dto.AppLogStatDto;
import org.dsa.modules.reporter.entity.*;
import org.dsa.modules.reporter.vo.client.ClientHisPageReVo;
import org.dsa.modules.reporter.vo.client.UpgradeLogPageReVo;

import java.util.List;
import java.util.Map;

public interface ApplicationHistoryService {

    public AppHistory queryAppHisByDeviceId(String deviceId, Integer updateId);

    /***
     * 直接通过SQL进行统计保存到统计表
     * @param startTime
     * @param endTime
     */
    public void statsHistory(String startTime, String endTime);

    /****
     * 时间段查询 历史记录
     * @param startTime
     * @param endTime
     * @return
     */
    public List<AppLogStatDto> queryData(String startTime, String endTime);

    /***
     * 维护历史统计
     * @param dtos
     */
    public void maintainData(List<AppLogStatDto> dtos);

    /***
     * 批处理job
     * @param tag
     * @return
     */
    public List<AppHistory> queryHistoryByTag(String tag);

    /***
     * 客户端详情查询
     * @param vo
     * @return
     */
    public Page<AppHistory> queryClientHis(UpgradeLogPageReVo vo);

    /***
     * 列表页查询客户端操作历史记录
     * @param vo
     * @return
     */
    public Page<AppHistory> queryHis(ClientHisPageReVo vo);

    public void insertHisData(List<ClientUpgrade> records,
                              Map<Integer, CurrentSoftwareInfo> currentSoftwareInfoMap,
                              Map<Integer, FutureSoftwareInfo> futureSoftwareInfoMap,
                              Map<Integer, CurrentComponentInfo> currentComponentInfoMap,
                              Map<Integer, FutureComponentInfo> futureComponentInfoMap
                              );
}
