package org.dsa.modules.remoteDiagnostic.vo.EcuVersion;

import lombok.Data;
import lombok.ToString;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuVersionFileEntity;

import java.util.List;

@Data
@ToString()
public class FlashFileReqVo {

    private Long id;

    private List<RemoteVehicleEcuVersionFileEntity> files;
}
