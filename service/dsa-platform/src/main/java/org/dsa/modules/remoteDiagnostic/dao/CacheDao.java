package org.dsa.modules.remoteDiagnostic.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.remoteDiagnostic.entity.CacheEntity;

import java.util.List;

@Mapper
public interface CacheDao extends BaseMapper<CacheEntity> {

    @Insert("<script>"
            + " insert into r_cache (c_key,c_val) values "
            + " <foreach collection='values' item='item' separator=','> "
            + " (#{key},#{item}) "
            + " </foreach>"
            + " ON conflict(c_key,c_val)  DO NOTHING "
            + " </script>")
    void inserts(@Param("key") String key, @Param("values") List<String> val);
}
