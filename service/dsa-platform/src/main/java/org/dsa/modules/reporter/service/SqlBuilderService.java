package org.dsa.modules.reporter.service;


import org.dsa.modules.reporter.dto.SqlBuilderDto;

import java.util.Map;

public interface SqlBuilderService {

    public String render(SqlBuilderDto cube, Map<String, Object> conditionMap, Integer limit);

    public String buildSql(String sql, String condition, Integer top);

}
