package org.dsa.modules.onlineUpdate.service.impl;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.dsa.common.utils.PageUtils;
import org.dsa.modules.onlineUpdate.dao.OemSoftwareVersionDao;
import org.dsa.modules.onlineUpdate.dao.SoftwareDao;
import org.dsa.modules.onlineUpdate.dao.SoftwareVersionDao;
import org.dsa.modules.onlineUpdate.entity.OemSoftwareVersionEntity;
import org.dsa.modules.onlineUpdate.entity.SoftwareEntity;
import org.dsa.modules.onlineUpdate.entity.SoftwareVersionEntity;
import org.dsa.modules.onlineUpdate.service.OemSoftwareVersionService;
import org.dsa.modules.onlineUpdate.vo.CascadeOptions;
import org.dsa.modules.onlineUpdate.vo.OemSoftwareVersionVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service("oemSoftwareVersionService")
public class OemSoftwareVersionServiceImpl extends ServiceImpl<OemSoftwareVersionDao, OemSoftwareVersionEntity> implements OemSoftwareVersionService {

    @Autowired
    private SoftwareDao softwareDao;
    @Autowired
    private SoftwareVersionDao softwareVersionDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Integer oemId = MapUtil.getInt(params,"oemId");
        Integer softwareId = MapUtil.getInt(params,"softwareId");
        Integer softwareVersionId = MapUtil.getInt(params,"softwareVersionId");
        Integer pageNo = MapUtil.getInt(params,"page");
        Integer limit = MapUtil.getInt(params,"limit");
        Page<OemSoftwareVersionVo> page = new Page<>(pageNo, limit);
        Page<OemSoftwareVersionVo> res = this.baseMapper.queryPage(page,oemId,softwareId,softwareVersionId);
        return new PageUtils(res);
    }


    @Override
    public void saveOrUpdate2(OemSoftwareVersionEntity entity) {
        //参数校验
        checkParam(entity);
        if (ObjectUtil.isNull(entity.getId())){
            //新增
            this.baseMapper.insert(entity);
        }else{
            //修改
            this.baseMapper.updateById(entity);
        }
    }

    @Override
    public List<CascadeOptions> queryVersionTree() {
        //查询所有未禁用软件
        List<SoftwareEntity> softwareEntities = softwareDao.selectList(new QueryWrapper<SoftwareEntity>().eq("status",0));
        List<CascadeOptions> options = new ArrayList<>(16);
        // 根据组件查询版本
        softwareEntities.forEach(c -> {
            List<CascadeOptions> children = softwareVersionDao.selectList(new QueryWrapper<SoftwareVersionEntity>()
                            .eq("software_id", c.getId()))
                    .stream().map(v -> CascadeOptions.builder().label(v.getVersionName()).value(v.getId()).build()).collect(Collectors.toList());
            CascadeOptions childrenOption = CascadeOptions.builder().label(c.getName()).value(c.getId()).children(children).build();
            if (children.size() > 0) {
                options.add(childrenOption);
            }
        });
        return options;
    }

    private void checkParam(OemSoftwareVersionEntity entity) {
        // 校验 oemId softwareId softwareVersionId
//        Optional.ofNullable(entity.getOemId()).orElseThrow(new RRException(Constant.Msg.CHECK_ID_ERROR));

    }

}