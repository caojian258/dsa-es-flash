package org.dsa.common.aspect;

import clojure.lang.Obj;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.dsa.common.annotation.SysInternationalization;

import org.dsa.common.utils.R;
import org.dsa.modules.sys.dao.SysInternationalizationDao;
import org.dsa.modules.sys.utils.InternationalizationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.dsa.modules.sys.entity.SysInternationalizationEntity;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Aspect
@Component
@Slf4j
public class SysInternationalizationAspect {


    @Autowired
    SysInternationalizationDao sysInternationalizationDao;
    @Autowired
    private PlatformTransactionManager transactionManager;

    @Pointcut("@annotation(org.dsa.common.annotation.SysInternationalization)")
    public void internationalization() {

    }


    @Around("internationalization()")
    public Object doInternationalization(ProceedingJoinPoint point) throws Throwable {
        try {
            MethodSignature signature = (MethodSignature) point.getSignature();
            Method method = signature.getMethod();
            SysInternationalization i18n = method.getAnnotation(SysInternationalization.class);
            String value = i18n.value();
            Object proceed = null;
            R r;
            Object data;
            switch (value) {
                case "select":
                    proceed = point.proceed();
                    r = (R) proceed;
//                    System.out.println(r.get("data") instanceof List<?>);
                    data = r.get("data");
                    if (data instanceof List<?> list) {
                        for (Object o : list) {
                            Class<?> aClass = o.getClass();
                            Field[] declaredFields = aClass.getDeclaredFields();
                            for (Field declaredField : declaredFields) {
                                if (InternationalizationContext.key.containsKey(declaredField.getName())) {
                                    declaredField.setAccessible(true);
                                    Field field = aClass.getDeclaredField(InternationalizationContext.key.get(declaredField.getName()));
                                    if (field != null) {
                                        field.setAccessible(true);
                                        doSetField(declaredField, o, field);
                                    }
                                }
                            }
                        }
                    } else {
                        Class<?> aClass = data.getClass();
                        Field[] declaredFields = aClass.getDeclaredFields();
                        for (Field declaredField : declaredFields) {
                            if (InternationalizationContext.key.containsKey(declaredField.getName())) {
                                declaredField.setAccessible(true);
                                Field field = aClass.getDeclaredField(InternationalizationContext.key.get(declaredField.getName()));
                                if (field != null) {
                                    field.setAccessible(true);
                                    doSetField(declaredField, data, field);
                                }
                            }
                        }
                    }
                    break;
                case "page":
                    proceed = point.proceed();
                    r = (R) proceed;
                    data = r.get("data");

                    if (data != null) {
                        Class<?> aClass = data.getClass();

                        Field field = aClass.getDeclaredField("records");
                        if (field != null) {
                            field.setAccessible(true);
                            Object f = field.get(data);
                            if (f instanceof List<?> list) {
                                for (Object o : list) {
                                    Class<?> bClass = o.getClass();
                                    Field[] declaredFields = bClass.getDeclaredFields();
                                    for (Field declaredField : declaredFields) {
                                        if (InternationalizationContext.key.containsKey(declaredField.getName())) {
                                            declaredField.setAccessible(true);
                                            Field aField = bClass.getDeclaredField(InternationalizationContext.key.get(declaredField.getName()));
                                            if (aField != null) {
                                                aField.setAccessible(true);
                                                doSetField(declaredField, o, aField);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;
                case "update":
                    Object[] args = point.getArgs();
//                    try {
                    proceed = saveOrUpdateLocal(point, args);
//                    } catch (Exception e) {
//                        log.error(e.getMessage(), e);
//                        return R.error();
//                    }
                    break;
                default:
                    proceed = point.proceed();
                    break;
            }
            return proceed;
        } catch (Throwable e) {
            log.error(e.getMessage(), e);
            return R.error();
        }
    }

    private Object saveOrUpdateLocal(ProceedingJoinPoint point, Object[] args) throws Throwable {
        Object proceed;
        TransactionStatus transactionStatus = transactionManager.getTransaction(new DefaultTransactionDefinition());
        if (args != null && args.length != 0) {
            Object o = args[0];
            Class<?> aClass = o.getClass();

            Field[] declaredFields = aClass.getDeclaredFields();
            for (Field declaredField : declaredFields) {
                if (InternationalizationContext.key.containsKey(declaredField.getName())) {
                    declaredField.setAccessible(true);
                    Field field = aClass.getDeclaredField(InternationalizationContext.key.get(declaredField.getName()));
                    if (field != null) {
                        field.setAccessible(true);
                        SysInternationalizationEntity sysInternationalizationEntity = declaredField.get(o) == null ? null : sysInternationalizationDao.selectByKeyAndLocal(declaredField.get(o).toString(), LocaleContextHolder.getLocale().toLanguageTag());
                        if (sysInternationalizationEntity == null) {
                            String key = declaredField.get(o) == null ? null : declaredField.get(o).toString();
                            if (StringUtils.isEmpty(key)) {
                                String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                                declaredField.set(o, uuid);
                                sysInternationalizationEntity = SysInternationalizationEntity.builder().local(LocaleContextHolder.getLocale().toLanguageTag()).value(field.get(o) == null ? null : field.get(o).toString()).key(uuid).build();
                                sysInternationalizationDao.saveOrUpdate(sysInternationalizationEntity);
                            } else {
                                sysInternationalizationEntity = SysInternationalizationEntity.builder().local(LocaleContextHolder.getLocale().toLanguageTag()).value(field.get(o) == null ? null : field.get(o).toString()).key(key).build();
                                sysInternationalizationDao.saveOrUpdate(sysInternationalizationEntity);
                            }
                        } else {
                            sysInternationalizationEntity.setValue(field.get(o) == null ? null : field.get(o).toString());
                            sysInternationalizationDao.saveOrUpdate(sysInternationalizationEntity);
                        }
                    }
                }
            }
        }

        try {
            proceed = point.proceed();
            transactionManager.commit(transactionStatus);
        } catch (Throwable throwable) {
            log.error(throwable.getMessage(), throwable);
            transactionManager.rollback(transactionStatus);
            throw throwable;
        }
        return proceed;
    }

    private void doSetField(Field declaredField, Object data, Field field) throws IllegalAccessException {
        if (declaredField.get(data) != null && !StringUtils.isEmpty(declaredField.get(data).toString())) {
            List<SysInternationalizationEntity> sysInternationalizationEntities = sysInternationalizationDao.selectByKey(declaredField.get(data).toString());
            if (!sysInternationalizationEntities.isEmpty()) {
//                List<String> l = sysInternationalizationEntities.stream().filter(e -> {
//                    LocaleContextHolder.getLocale();
//                    return true;
//                }).map(SysInternationalizationEntity::getValue).toList();
                Map<String, String> l = sysInternationalizationEntities.stream().collect(Collectors.toMap(SysInternationalizationEntity::getLocal, e -> e.getValue() == null ? "" : e.getValue()));
//                field.set(data, StringUtils.isEmpty(l.get(LocaleContextHolder.getLocale().toString())) ? l.get("zh_CN") : l.get(LocaleContextHolder.getLocale().toString()));
                field.set(data, l.get(LocaleContextHolder.getLocale().toLanguageTag()));
            }
        }
    }
}
