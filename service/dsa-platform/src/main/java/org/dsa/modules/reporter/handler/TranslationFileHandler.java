package org.dsa.modules.reporter.handler;

import cn.hutool.core.util.StrUtil;
import org.dsa.modules.reporter.entity.Translation;
import org.dsa.modules.reporter.service.LocaleService;
import org.dsa.modules.reporter.util.XmlReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;

@Service
public class TranslationFileHandler{

    @Autowired
    LocaleService localeService;

    public int readFile(String path, String locale) throws Exception {
        int rows = 0;
        XmlReader reader = new XmlReader( path);


        List<Map<String, Object>> mapList =  reader.parse("ID");
        List<Translation> trs = new ArrayList<>();

        for (Map dd: mapList){
            if(null != dd.get("uid") && StrUtil.isNotBlank((String) dd.get(locale))){
                String uids = (String) dd.get("uid");
                if(uids.indexOf(",")!= -1){
                    String[] uidArray = uids.split(",");
                    for (String uid : uidArray){
                        Translation translate = new Translation();
                        translate.setKey(uid);
                        translate.setText((String) dd.get(locale));
                        translate.setLocale(locale);
                        trs.add(translate);
                    }
                }else{
                    Translation translate = new Translation();
                    translate.setKey(uids);
                    translate.setText((String) dd.get(locale));
                    translate.setLocale(locale);
                    trs.add(translate);
                }

            }
        }

        if(trs.size()>0){
            List<Translation>  utrs = trs.stream().collect(
                    collectingAndThen(
                            toCollection(() -> new TreeSet<>(Comparator.comparing(Translation::getKey))),
                            ArrayList::new
                    )
            );

            rows = utrs.size();
        }

        return rows;
    }

}
