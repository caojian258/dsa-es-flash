//package org.dsa.common.utils;
//
//import java.io.BufferedReader;
//import java.io.InputStream;
//import java.io.InputStreamReader;
//import java.util.Map;
//import java.util.Set;
//
//import org.apache.commons.httpclient.HttpClient;
//import org.apache.commons.httpclient.NameValuePair;
//import org.apache.commons.httpclient.methods.GetMethod;
//import org.apache.commons.httpclient.methods.PostMethod;
//import org.apache.commons.httpclient.methods.StringRequestEntity;
//import org.dsa.common.exception.RRException;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//
//import org.dsa.common.pojo.CodeMessage;
//
//public class ClientUtils {
//
//	private static final Logger logger = LoggerFactory.getLogger(HttpUtils.class);
//
//	/**
//	 * 发送post请求
//	 * @param json
//	 * @param baseUrl
//	 * @return
//	 */
//	public static CodeMessage post( String baseUrl,String json) throws RRException {
//
//		if(baseUrl == null || "".equals(baseUrl.trim())){
//			logger.error("[ClientUtils][post]Get the baseUrl is null.");
//			throw new RRException("baseUrl is null ",500);
//		}
//
//		HttpClient client = new HttpClient();
//		//设置3秒超时
//		client.getHttpConnectionManager().getParams().setConnectionTimeout(3000);
//		client.getParams().setIntParameter("http.socket.timeout", 3000);
//
//		logger.info("[ClientUtils][post]The request url is: {}, and request param is: {}", baseUrl, json);
//
//		PostMethod postMethod = null;
//		int status = 0;
//		CodeMessage out = new CodeMessage();
//
//		try{
//
//			postMethod = getPostMethod(baseUrl, json);
//			status = client.executeMethod(postMethod);
//
//			String entity = getResult(postMethod.getResponseBodyAsStream());
//			out.setCode(String.valueOf(status));
//			out.setMessage(entity);
//
//			if(status == 200)
//				logger.info("[ClientUtils][post]Invoke success, and return message is: {}", entity);
//			else
//				logger.info("[ClientUtils][post]Invoke failure and the response code is: {}, and return error message is: {}", status, out);
//
//		}catch(Exception e){
//			logger.info("[ClientUtils][post]Invoke failure and the param is: {}", json, e);
//			throw new RRException("param is null ",500);
//		}finally{
//			if(postMethod != null)
//				postMethod.releaseConnection();
//			client.getHttpConnectionManager().closeIdleConnections(0);
//		}
//
//		return out;
//
//	}
//
//	/**
//	 * 获取PostMethod
//	 * @param address
//	 * @param json
//	 * @return
//	 * @throws Exception
//	 */
//	private static PostMethod getPostMethod(String address, String json) throws Exception {
//
//		PostMethod post = new PostMethod(address);
//		StringRequestEntity strReqEntity = new StringRequestEntity(json,"application/json","UTF-8");
//		post.setRequestEntity(strReqEntity);
//		post.addRequestHeader("Content-Type","application/json;charset=UTF-8");
//		post.addRequestHeader("Accept", "application/json;charset=UTF-8");
//
//		return post;
//	}
//
//	/**
//	 * 发送get请求
//	 * @param param
//	 * @param baseUrl
//	 * @return
//	 */
//	public static CodeMessage get(String baseUrl,Map<String, String> param ) throws RRException {
//
//		if(baseUrl == null || "".equals(baseUrl.trim())){
//			logger.error("[ClientUtils][get]Get the baseUrl is null.");
//			throw new RRException("baseUrl is null ",500);
//		}
//
//		HttpClient client = new HttpClient();
//		//设置3秒超时
//		client.getHttpConnectionManager().getParams().setConnectionTimeout(3000);
//		client.getParams().setIntParameter("http.socket.timeout", 3000);
//
//		logger.info("[ClientUtils][get]The url is: {}, and request param is: {}", baseUrl, param.toString());
//
//		GetMethod getMethod = null;
//		int status = 0;
//		CodeMessage out = new CodeMessage();
//
//		try{
//
//			getMethod = getMethod(baseUrl, param);
//			status = client.executeMethod(getMethod);
//
//			String entity = getResult(getMethod.getResponseBodyAsStream());
//			out.setCode(String.valueOf(status));
//			out.setMessage(entity);
//
//			if(status == 200)
//				logger.info("[ClientUtils][get]Invoke success, and return message is: {}", out);
//			else
//				logger.info("[ClientUtils][get]Invoke failure and the response code is: {}, and return error message is: {}", status, out);
//
//		}catch(Exception e){
//			logger.info("[ClientUtils][get]Invoke failure and the param is: {}", param, e);
//			throw new RRException("failure ",500);
//		}finally{
//			if(getMethod != null)
//				getMethod.releaseConnection();
//			client.getHttpConnectionManager().closeIdleConnections(0);
//		}
//
//		return out;
//
//	}
//	/**
//	 * 发送get请求
//	 * @param baseUrl
//	 * @return
//	 */
//	public static CodeMessage get(String baseUrl) throws RRException {
//
//		if(baseUrl == null || "".equals(baseUrl.trim())){
//			logger.error("[ClientUtils][get]Get the baseUrl is null.");
//			throw new RRException("baseUrl is null ",500);
//		}
//
//		HttpClient client = new HttpClient();
//		//设置3秒超时
//		client.getHttpConnectionManager().getParams().setConnectionTimeout(3000);
//		client.getParams().setIntParameter("http.socket.timeout", 3000);
//
//		logger.info("[ClientUtils][get]The url is: {}", baseUrl);
//
//		GetMethod getMethod = null;
//		int status = 0;
//		CodeMessage out = new CodeMessage();
//
//		try{
//
//			getMethod = getMethod(baseUrl,null);
//			status = client.executeMethod(getMethod);
//
//			String entity = getResult(getMethod.getResponseBodyAsStream());
//			out.setCode(String.valueOf(status));
//			out.setMessage(entity);
//
//			if(status == 200)
//				logger.info("[ClientUtils][get]Invoke success, and return message is: {}", out);
//			else
//				logger.info("[ClientUtils][get]Invoke failure and the response code is: {}, and return error message is: {}", status, out);
//
//		}catch(Exception e){
//			logger.info("[ClientUtils][get]Invoke failure and the param is: {}",  e);
//			throw new RRException("failure ",500);
//		}finally{
//			if(getMethod != null)
//				getMethod.releaseConnection();
//			client.getHttpConnectionManager().closeIdleConnections(0);
//		}
//
//		return out;
//
//	}
//
//
//	/**
//	 * 取getMethod
//	 * @param address
//	 * @param param
//	 * @return
//	 * @throws Exception
//	 */
//	private static GetMethod getMethod(String address, Map<String, String> param) throws Exception {
//
//		GetMethod get = new GetMethod(address);
//
//		if(param != null && !param.isEmpty()) {
//
//			Set<String> keySet = param.keySet();
//
//			NameValuePair[] params = new NameValuePair[keySet.size()];
//
//			int i = 0;
//			for(String key : keySet) {
//				NameValuePair tempParam = new NameValuePair(key, param.get(key));
//				params[i] = tempParam;
//				i ++;
//			}
//
//			get.setQueryString(params);
//
//		}
//
//		return get;
//	}
//
//	/**
//	 * 取返回值Json格式字符串
//	 * @param responseBodyAsStream
//	 * @return
//	 * @throws Exception
//	 */
//	private static String getResult(InputStream responseBodyAsStream) throws Exception{
//
//		BufferedReader reader = new BufferedReader(new InputStreamReader(responseBodyAsStream, "UTF-8"));
//
//		StringBuffer stringBuffer = new StringBuffer();
//
//		String str = "";
//		while((str = reader.readLine()) != null){
//			stringBuffer.append(str);
//		}
//
//		if(stringBuffer.length() > 0)
//			return stringBuffer.toString();
//
//		return null;
//
//	}
//
//}
