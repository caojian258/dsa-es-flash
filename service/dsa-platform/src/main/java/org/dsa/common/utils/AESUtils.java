package org.dsa.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.Base64Utils;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * @author weishunxin
 * @since 2023-07-14
 */
@Slf4j
public class AESUtils {

    public static final String AES_KEY = "dsa@tm3.file6868";

    /**
     * 编码
     */
    private static final String ENCODING = "UTF-8";

    /**
     * 算法定义
     */
    private static final String AES_ALGORITHM = "AES";

    /**
     * 指定填充方式
     */
    private static final String CIPHER_PADDING = "AES/ECB/PKCS5Padding";

    /**
     * AES加密
     * @param content 待加密内容
     * @param aesKey  密码
     * @return 加密后的字符串
     */
    public static String encrypt(String content, String aesKey){
        if(StringUtils.isBlank(content)){
            return null;
        }

        //判断秘钥是否为16位
        if(StringUtils.isNotBlank(aesKey) && aesKey.length() == 16){
            try {
                //对密码进行编码
                byte[] bytes = aesKey.getBytes(ENCODING);

                //设置加密算法，生成秘钥
                SecretKeySpec skeySpec = new SecretKeySpec(bytes, AES_ALGORITHM);

                // "算法/模式/补码方式"
                Cipher cipher = Cipher.getInstance(CIPHER_PADDING);

                //选择加密
                cipher.init(Cipher.ENCRYPT_MODE, skeySpec);

                //根据待加密内容生成字节数组
                byte[] encrypted = cipher.doFinal(content.getBytes(ENCODING));

                //返回base64字符串
                return Base64Utils.encodeToString(encrypted);
            } catch (Exception e) {
                log.info("AES encrypt exception:", e);
                throw new RuntimeException(e);
            }
        }else {
            log.info("AES encrypt: the aesKey is null or error!");
            return null;
        }
    }

    /**
     * 解密
     *
     * @param content 待解密内容
     * @param aesKey  密码
     * @return 解密后的字符串
     */
    public static String decrypt(String content, String aesKey){
        if(StringUtils.isBlank(content)){
            return null;
        }

        //判断秘钥是否为16位
        if(StringUtils.isNotBlank(aesKey) && aesKey.length() == 16){
            try {
                //对密码进行编码
                byte[] bytes = aesKey.getBytes(ENCODING);

                //设置解密算法，生成秘钥
                SecretKeySpec skeySpec = new SecretKeySpec(bytes, AES_ALGORITHM);

                // "算法/模式/补码方式"
                Cipher cipher = Cipher.getInstance(CIPHER_PADDING);

                //选择解密
                cipher.init(Cipher.DECRYPT_MODE, skeySpec);

                //先进行Base64解码
                byte[] decodeBase64 = Base64Utils.decodeFromString(content);

                //根据待解密内容进行解密
                byte[] decrypted = cipher.doFinal(decodeBase64);

                //将字节数组转成字符串
                return new String(decrypted, ENCODING);
            } catch (Exception e) {
                log.info("AES decrypt exception:", e);
                throw new RuntimeException(e);
            }
        } else {
            log.info("AES decrypt: the aesKey is null or error!");
            return null;
        }
    }

    /*public static void main(String[] args) {
        String encrypt = encrypt("/usr/file/notice/default.rss", AES_KEY);
        System.out.println("密文：" + encrypt);

        String decrypt = decrypt(encrypt, AES_KEY);
        System.out.println("明文：" + decrypt);
    }*/

}
