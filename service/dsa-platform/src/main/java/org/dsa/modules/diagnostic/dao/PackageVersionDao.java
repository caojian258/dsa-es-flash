package org.dsa.modules.diagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.dsa.modules.diagnostic.entity.DDiagPackageVersionEntity;

/**
 * @author weishunxin
 * @since 2023-05-29
 */
@Mapper
public interface PackageVersionDao extends BaseMapper<DDiagPackageVersionEntity> {

}
