package org.dsa.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.sys.entity.SysLocales;

@Mapper
public interface SysLocalesDao extends BaseMapper<SysLocales> {

    public void truncateTable();

    public void insertData(@Param("locale") SysLocales locale);
}
