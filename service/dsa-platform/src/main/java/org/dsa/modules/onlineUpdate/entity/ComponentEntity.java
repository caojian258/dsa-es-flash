package org.dsa.modules.onlineUpdate.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.dsa.modules.onlineUpdate.utils.BaseEntity;

/**
 * 组件基础数据表
 * 
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("o_component")
public class ComponentEntity extends BaseEntity{

	/**
	 * 组件编码;客户端查找组件使用
	 */
	private Long code;
	/**
	 * 组件名称
	 */
	private String name;
	/**
	 * 启用/禁用;0启用;1禁用
	 */
	private Integer status;
	/**
	 * 多语言翻译key，生成规则：翻译key，生成规则"CNAME_"+name+"_CODE_"+code
	 */
	private String descriptionTi;

	@TableField(exist = false)
	private Boolean isRelease;
}
