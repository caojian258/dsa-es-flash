-- 工单表
CREATE TABLE `t_event` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `event_no` varchar(30) COMMENT '事件号',
  `contact_name` varchar(50) COMMENT '联系人姓名',
  `contact_no` varchar(50) COMMENT '联系人员工号',
  `contact_landline` varchar(50) COMMENT '联系人座机',
  `contact_telphone` varchar(50) COMMENT '联系人手机',
  `organ_name` varchar(50) COMMENT '机构名称',
  `priority` varchar(50) COMMENT '紧急度',
  `dep_name` varchar(50) COMMENT '部门名称',
  `contact_office_location` varchar(50) COMMENT '联系人办公地点',
  `sys_name` varchar(100) COMMENT '系统名称',
  `process_man` varchar(50) comment '处理人'
  `comment` text COMMENT '问题描述',
  `attach_url` varchar(300)  COMMENT '附件路径',
  `attach_name` varchar(100) COMMENT '附件名称',
  `event_status` varchar(50) COMMENT '事件状态',
  `created_time` datetime COMMENT '创建时间',
  `created_by` varchar(50) COMMENT '创建人',
  `modify_by` varchar(50) COMMENT '修改人',
  `modify_time` datetime COMMENT '修改时间',
  `is_delete`  varchar(6) DEFAULT 0 COMMENT '是否删除(0:正常,1：删除)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='事件表';

-- 受理表
CREATE TABLE `t_event_solve` (
	`id` bigint NOT NULL AUTO_INCREMENT,
	`event_no` varchar(30) COMMENT '事件号',
	`type` varchar(50) COMMENT '类型',
	`solution_category_id` varchar(50) COMMENT '解决方法分类',
	`solution_comment` text COMMENT '解决方法',
	`solution_attach_url` varchar(300)  COMMENT '解决方案附件路径',
	`solution_attach_name` varchar(300)  COMMENT '解决方案附件名',
	`add_event` varchar(50) COMMENT '补录事件',
	`created_time` datetime COMMENT '创建时间',
	`created_by` varchar(50) COMMENT '创建人',
	`modify_by` varchar(50) COMMENT '修改人',
	`modify_time` datetime COMMENT '修改时间',
	`is_delete`  varchar(6) DEFAULT 0 COMMENT '是否删除(0:正常,1：删除)',
	 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='受理表';

-- 事件验收和评价表
CREATE TABLE `t_event_accept` (
	`id` bigint NOT NULL AUTO_INCREMENT,
	`event_no` varchar(30) COMMENT '事件号',
	`accept_status` varchar(50) COMMENT '验收状态',
	`evaluate_detail` varchar(50) COMMENT '详细评价',
	`add_comment` text COMMENT '补充评价',
	`unsolve_comment` text COMMENT '未解决说明',
	`score` int(4) COMMENT '评分',
	`created_time` datetime COMMENT '创建时间',
	`created_by` varchar(50) COMMENT '创建人',
	`modify_by` varchar(50) COMMENT '修改人',
	`modify_time` datetime COMMENT '修改时间',
	`is_delete`  varchar(6) DEFAULT 0 COMMENT '是否删除(0:正常,1：删除)',
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='事件验收和评价表';

-- 系统与组关系表
CREATE TABLE `t_sys_group_dict` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `sys_name` VARCHAR(100) COMMENT '系统名称',
  `group_id` bigint COMMENT '组id',
  `group_name` VARCHAR(50) COMMENT '组名称',
  `del_flag` tinyint COMMENT '删除标记  -1：已删除  0：正常',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统与组关系表';

-- 添加唯一性
alter table t_sys_group_dict add unique key unique_sys_name (sys_name);
ALTER TABLE t_sys_group_dict ADD COLUMN category VARCHAR(20) DEFAULT NULL COMMENT '系统类别' AFTER id;

-- 事件状态
INSERT INTO eap_fast.sys_dict
(name, `type`, code, value, order_num, remark, del_flag)
VALUES('事件状态', 'EventStatus', 'waiting_solve', '待接收', 1, NULL, 0);

INSERT INTO eap_fast.sys_dict
(name, `type`, code, value, order_num, remark, del_flag)
VALUES('事件状态', 'EventStatus', 'processing', '处理中', 2, NULL, 0);

INSERT INTO eap_fast.sys_dict
(name, `type`, code, value, order_num, remark, del_flag)
VALUES('事件状态', 'EventStatus', 'waiting_confirm', '待验收', 3, NULL, 0);

INSERT INTO eap_fast.sys_dict
(name, `type`, code, value, order_num, remark, del_flag)
VALUES('事件状态', 'EventStatus', 'done', '已完成', 4, NULL, 0);

-- 详细评价
INSERT INTO eap_fast.sys_dict
(name, `type`, code, value, order_num, remark, del_flag)
VALUES('详细评价', 'evaluate_detail', 'handle_fast', '速度快', 1, NULL, 0);

INSERT INTO eap_fast.sys_dict
(name, `type`, code, value, order_num, remark, del_flag)
VALUES('详细评价', 'evaluate_detail', 'good_skill', '技能出众', 2, NULL, 0);

INSERT INTO eap_fast.sys_dict
(name, `type`, code, value, order_num, remark, del_flag)
VALUES('详细评价', 'evaluate_detail', 'nice', '态度好', 3, NULL, 0);

INSERT INTO eap_fast.sys_dict
(name, `type`, code, value, order_num, remark, del_flag)
VALUES('详细评价', 'evaluate_detail', 'voice_pretty', '声音磁性', 4, NULL, 0);

INSERT INTO eap_fast.sys_dict
(name, `type`, code, value, order_num, remark, del_flag)
VALUES('详细评价', 'evaluate_detail', 'patient', '有耐心', 5, NULL, 0);

-- 故障状态
INSERT INTO eap_fast.sys_dict
(name, `type`, code, value, order_num, remark, del_flag)
VALUES('解决状态', 'solve_status', 'resolved', '已解决', 1, NULL, 0);

INSERT INTO eap_fast.sys_dict
(name, `type`, code, value, order_num, remark, del_flag)
VALUES('解决状态', 'solve_status', 'unresolved', '未解决', 2, NULL, 0);


-- 机构名称
 INSERT INTO eap_fast.sys_dict
 (name, `type`, code, value, order_num, remark, del_flag)
 VALUES('机构名称', 'org_name', 'org1', '上海总部', 1, NULL, 0);

 INSERT INTO eap_fast.sys_dict
 (name, `type`, code, value, order_num, remark, del_flag)
 VALUES('机构名称', 'org_name', 'org2', '北京分公司', 2, NULL, 0);

-- 联系人办公地点
 INSERT INTO eap_fast.sys_dict
 (name, `type`, code, value, order_num, remark, del_flag)
 VALUES('联系人办公地点', 'office_location', 'beijing', '北京', 1, NULL, 0);

 INSERT INTO eap_fast.sys_dict
 (name, `type`, code, value, order_num, remark, del_flag)
 VALUES('联系人办公地点', 'office_location', 'shanghai', '上海', 2, NULL, 0);

 INSERT INTO eap_fast.sys_dict
 (name, `type`, code, value, order_num, remark, del_flag)
 VALUES('联系人办公地点', 'office_location', 'shenzhen', '深圳', 3, NULL, 0);
 
  INSERT INTO eap_fast.sys_dict
 (name, `type`, code, value, order_num, remark, del_flag)
 VALUES('联系人办公地点', 'office_location', 'guangzhou', '广州', 4, NULL, 0);
 

-- 事件类型
INSERT INTO eap_fast.sys_dict
(name, `type`, code, value, order_num, remark, del_flag)
VALUES('请求', 'event_type', 'request', '请求', 1, NULL, 0);

INSERT INTO eap_fast.sys_dict
(name, `type`, code, value, order_num, remark, del_flag)
VALUES('故障', 'event_type', 'breakdown', '故障', 2, NULL, 0);

INSERT INTO eap_fast.sys_dict
(name, `type`, code, value, order_num, remark, del_flag)
VALUES('需求', 'event_type', 'requirement', '需求', 3, NULL, 0);










