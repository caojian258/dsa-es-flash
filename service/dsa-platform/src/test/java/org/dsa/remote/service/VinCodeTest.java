package org.dsa.remote.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.remoteDiagnostic.dao.*;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuVersionEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleTagEntity;
import org.dsa.modules.remoteDiagnostic.service.RemoteVehiclePoolService;
import org.dsa.modules.remoteDiagnostic.service.RemoteVehicleTagService;
import org.dsa.modules.remoteDiagnostic.vo.VinCode.VinCodeReqPageVo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class VinCodeTest {

    @Autowired
    private RemoteVehiclePoolService vehiclePoolService;

    @Autowired
    private RemoteEcuIdentificationInfoDao identificationInfoDao;

    @Autowired
    private RemoteEcuVersionDao versionDao;

//    @Autowired
//    private RemoteEcuEcuVersionDao ecuEcuVersionDao;

    @Autowired
    private RemoteVehicleEcuDao ecuDao;
    @Autowired
    private RemoteVehicleTagService tagService;
    @Autowired
    private RemoteVehicleTagDao tagDao;
    @Autowired
    private RemoteVehicleEcuGroupDao groupDao;
    @Autowired
    private RemoteVehicleVersionEcuGroupDao remoteVehicleVersionEcuGroupDao;

    @Test
    public void method(){

        VinCodeReqPageVo vinCodeReqPageVo = new VinCodeReqPageVo();

        Page<String> vinCode = vehiclePoolService.getVinCode(vinCodeReqPageVo);

        System.out.println(vinCode);

    }
    @Test
    public void testIdentificationDao(){

        List<Map<String, String>> list = identificationInfoDao.getList(null, null, null);
        System.out.println(list);

    }

    @Test
    public void testEcuVersionDao(){

//        VehicleEcuVersionEntity ecuVersionEntity;
//        for (int i = 0; i < 1000; i++) {
//            ecuVersionEntity = new VehicleEcuVersionEntity();
//            ecuVersionEntity.setEcuId(1L);
//            ecuVersionEntity.setVersionName("1");
//            ecuVersionEntity.setVersionType("1");
//            ecuVersionEntity.setStatus(0);
//            versionDao.insert(ecuVersionEntity);
//        }

//        List<EcuEcuVersionEntity> list = ecuEcuVersionDao.getList(5L);
        RemoteVehicleEcuVersionEntity info = versionDao.getInfo(1002L);
        System.out.println(info);

    }


    @Test
    public void testEcuDao(){
        ArrayList<Long> list = new ArrayList<>();
//        list.add(1L);
//        List<VehicleEcuEntity> list1 = ecuDao.getList(list);
        List<RemoteVehicleEcuEntity> list2 = ecuDao.getListByVersionId(1L);

        System.out.println(list2);

    }


    @Test
    public void  tagTestService(){
        Map<String, String> stringStringMap = tagService.templateExcelExport();
        System.out.println(stringStringMap);
    }

    @Test
    public void  tagTestDao(){

        Map<String, RemoteVehicleTagEntity> stringVehicleTagEntityMap = tagDao.selectMap();

        System.out.println(stringVehicleTagEntityMap);
    }
    @Test
    public void  groupTestDao(){
//        List<Long> list = new ArrayList<>();
//        list.add(3L);
//
//        List<RemoteVehicleEcuGroupEntity> groupsByVersion = groupDao.getGroupsByVersion(list, -1L);
//
//        System.out.println(groupsByVersion);

        System.out.println(remoteVehicleVersionEcuGroupDao.getInfoByGroup(-1L, -1L));
    }

    @Test
    public void  ecuVersionTest(){
        List<Long> ids = new ArrayList<>();
        ids.add(21L);
        Long id = 30L;


        List<RemoteVehicleEcuEntity> list = ecuDao.getListById(ids, id);

        System.out.println(list);
    }
}
