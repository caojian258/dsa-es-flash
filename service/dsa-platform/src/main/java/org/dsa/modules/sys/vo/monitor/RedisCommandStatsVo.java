package org.dsa.modules.sys.vo.monitor;

import lombok.Data;

/***
 * REDIS CommandStats 指令返回
 */
@Data
public class RedisCommandStatsVo {
    private String cmd;
    //调用次数
    private Long calls;
    //总时间
    private Long usec;
    //平均时间
    private Double usecPerCall;
    //拒绝次数
    private Long rejectedCalls;
    //失败次数
    private Long failedCalls;
}
