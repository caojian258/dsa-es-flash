package org.dsa.modules.remoteDiagnostic.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.remoteDiagnostic.dto.api.DiagnosticTaskDto;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskCampaignEntity;
import org.dsa.modules.remoteDiagnostic.vo.CheckNameReqVo;
import org.dsa.modules.remoteDiagnostic.vo.CheckStatusReqVo;
import org.dsa.modules.remoteDiagnostic.vo.PublicPageReqVo;
import org.dsa.modules.remoteDiagnostic.vo.Task.DiagnosticApiLoggerReqVo;
import org.dsa.modules.remoteDiagnostic.vo.Task.DiagnosticApiReqVo;
import org.dsa.modules.remoteDiagnostic.vo.Task.DiagnosticApiStatusReqVo;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface DiagnosticTaskCampaignService {

    Page<DiagnosticTaskCampaignEntity> selectPage(PublicPageReqVo vo);

    DiagnosticTaskCampaignEntity getInfo(Long id);

    void saveTask(DiagnosticTaskCampaignEntity task);

    void updateTask(DiagnosticTaskCampaignEntity task);

    List<DiagnosticTaskCampaignEntity> getTasks();

    Boolean check(CheckNameReqVo vo);

    List<DiagnosticTaskDto> getDiagnosticTaskList(DiagnosticApiReqVo vo);

    void uploadTaskResult(DiagnosticApiReqVo vo);

    Map<String, Object> checkTask(DiagnosticApiReqVo vo);

    void saveLogger(DiagnosticApiLoggerReqVo vo) throws IOException;

    void saveActiveStatus(DiagnosticApiStatusReqVo vo);

    Map<String,Object> selectVehicleCode(DiagnosticApiStatusReqVo vo);

    Long selectOtxFileSize(String md5);
}
