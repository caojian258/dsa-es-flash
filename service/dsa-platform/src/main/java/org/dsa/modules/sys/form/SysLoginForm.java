package org.dsa.modules.sys.form;

import lombok.Data;

/**
 * 登录表单
 *
 */
@Data
public class SysLoginForm {
    private String username;
    private String password;
    private String equipment;
    private String loginIP;
    /**
     * 验证码
     */
    private String captcha;
    /**
     * 语言
     */
    private String locale;

}
