package org.dsa.modules.onlineUpdate.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import org.dsa.modules.onlineUpdate.utils.BaseEntity;

/**
 * ${comments}
 * 
 */
@Data
@TableName("o_rule_match_device_record")
public class RuleMatchDeviceRecordEntity extends BaseEntity {

	/**
	 * 软件版本id
	 */
	private Integer softwareVersionId;
	/**
	 * 设备id
	 */
	private String deviceId;
	/**
	 * 更新id
	 */
	private String updateId;

	/**
	 * ruleId
	 */
	private Integer ruleId;
}
