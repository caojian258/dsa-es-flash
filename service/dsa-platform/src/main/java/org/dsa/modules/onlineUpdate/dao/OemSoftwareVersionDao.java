package org.dsa.modules.onlineUpdate.dao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.dsa.common.utils.PageUtils;
import org.dsa.modules.onlineUpdate.entity.OemSoftwareVersionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.dsa.modules.onlineUpdate.vo.OemSoftwareVersionVo;

/**
 * OEM关联软件版本
 * 
 */
@Mapper
public interface OemSoftwareVersionDao extends BaseMapper<OemSoftwareVersionEntity> {

    Page<OemSoftwareVersionVo> queryPage(IPage<OemSoftwareVersionVo> page, @Param("oemId") Integer oemId, @Param("softwareId") Integer softwareId, @Param("softwareVersionId") Integer softwareVersionId);
}
