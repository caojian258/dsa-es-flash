package org.dsa.modules.diagnostic.pojo;

import lombok.Data;

/**
 * sqlite数据接受详情类
 */
@Data
public class SynLoggerInfo {

    /**
     * 主键id
     */
    private Integer id;

    /**
     * 功能码
     */
    private String functionCode;

    /**
     * 动作码
     */
    private String actionCode;

    /**
     * 诊断数据来源
     */
    private String source;

    /**
     * 记录创建时间
     */
    private String createTime;

    /**
     * 记录修改时间
     */
    private String lastUpdateTime;

    /**
     * ecu名称
     */
    private String ecuName;

    /**
     * 诊断结果数据data
     */
    private String jsonData;


    /**
     * sessionId
     */
    private String sessionId;

}
