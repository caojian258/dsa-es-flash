package org.dsa.modules.reporter.dto;

import lombok.Data;
import org.dsa.modules.reporter.entity.AppInstall;
import org.dsa.modules.reporter.entity.SystemOverview;
import org.dsa.modules.reporter.vo.client.AppInstallResVo;
import org.dsa.modules.reporter.vo.client.ClientDetailResVo;

import java.io.Serializable;
import java.util.List;

@Data
public class ClientDetailDto implements Serializable {

    /***
     * 基本信息
     */
    ClientDetailResVo info;

    /***
     * 客户端硬件有关
     */
    List<ClientBaseInfoDto> clientHardwareList;

    /***
     * 客户端软件有关
     */
    List<ClientBaseInfoDto> clientSoftwareList;

    /***
     * 客户端经销商有关
     */
    List<ClientBaseInfoDto> clientWorkshopList;

    /***
     * 应用安装记录
     */
    List<AppInstallResVo> appInstallList;
}
