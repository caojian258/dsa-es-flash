package org.dsa.common.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author weishunxin
 * @since 2023-06-02
 */
public class FileUtils {

    public static void zipFiles(List<String> filePaths, String zipPath, String zipFileName) throws IOException {
        File zipPathFile = new File(zipPath);
        if (!zipPathFile.exists()) {
            boolean b = zipPathFile.mkdir();
            if (!b) {
                throw new IOException();
            }
        }

        try(FileOutputStream fos = new FileOutputStream(zipPath + zipFileName);ZipOutputStream zos = new ZipOutputStream(fos)) {
            for (String filePath : filePaths) {
                zos.putNextEntry(new ZipEntry(new File(filePath).getName()));
                byte[] bytes = Files.readAllBytes(Paths.get(filePath));
                zos.write(bytes, 0, bytes.length);
                zos.closeEntry();
            }
        }
    }

}
