package org.dsa.modules.reporter.vo.diag;

import lombok.Data;

@Data
public class DtcChartResVo {

    private String collectMonth;

    private String dtcCode;

    private Long value;
}
