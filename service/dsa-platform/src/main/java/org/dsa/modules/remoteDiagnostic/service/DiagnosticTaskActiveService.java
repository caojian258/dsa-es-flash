package org.dsa.modules.remoteDiagnostic.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskActiveEntity;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskRecordEntity;
import org.dsa.modules.remoteDiagnostic.vo.PublicPageReqVo;

import java.util.List;

public interface DiagnosticTaskActiveService {

    Page<DiagnosticTaskActiveEntity> selectPage(PublicPageReqVo vo);

    DiagnosticTaskActiveEntity getInfo(Long id);

    void saveTask(DiagnosticTaskActiveEntity task);

    void updateTask(DiagnosticTaskActiveEntity task);

    void updateTask(String sessionId, String vin);

    List<DiagnosticTaskActiveEntity> getList();

    void terminationActive(Long id);
}
