package org.dsa.modules.reporter.document;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.Date;

@Data
public class DtcSnapshotDoc extends ElasticEntity{

    private String sessionInfoId;

    private String sessionId;

    private String ecuName;

    private String dtcCode;

    private String name;

    private String nameTi;

    private String value;

    private String valueTi;

    private String unit;

    private String unitTi;

    private String nrcHex;

    private String nrcDescription;

    private String nrcDescriptionTi;

    private String errorCode;

    private String errorMessage;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date lastUpdateTime;
}
