package org.dsa.modules.sys.vo.workshop;

import lombok.Data;

import java.io.Serializable;

@Data
public class WorkshopClientResponseVo implements Serializable {

    private Long id;

    private String code;

    private String name;

    private String shortName;

}
