#!/bin/bash
#SFTP配置信息
#用户名
USER=sftp
#密码
PASSWORD=Dsa@2022
#待上传文件根目录
SRCDIR=/data/pshare/webdav/sessionlogimport/
#FTP目录
DESDIR=/usr/file/synlogger/$(date +%Y%m%d)
#IP
IP=172.17.32.110
#端口
PORT=22
 
#获取文件
cd ${SRCDIR} ;
#目录下的所有文件
#FILES=`ls`
FILES=`find ${SRCDIR} -type f -name '*.zip' -newermt '-90minutes' ! -newermt '-30minutes'`
 
for FILE in ${FILES}
do
    echo ${FILE}
#发送文件 (关键部分）
 
lftp -u ${USER},${PASSWORD} sftp://sftp@${IP}:${PORT} <<EOF
mkdir -p ${DESDIR}
cd ${DESDIR}/
lcd ${SRCDIR}
put ${FILE}
by
EOF

done
