package org.dsa.modules.remoteDiagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import org.dsa.modules.remoteDiagnostic.pojo.BaseEntity;

import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("r_ecu_identification_info")
public class RemoteEcuIdentificationInfoEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long groupId;

    private Long ecuId;

    private Long ecuVersionId;

    private Long versionId;

    private Integer status;

    private String didVal;

    private String did;

    private String identifyGroup;

    private String didName;

    private String descriptionTi;

    @TableField(exist = false)
    private boolean editDis;

    @TableField(exist = false)
    private String tempMsg;

    @TableField(exist = false)
    private String valMsg;

    @TableField(exist = false)
    private String ecuName;
}
