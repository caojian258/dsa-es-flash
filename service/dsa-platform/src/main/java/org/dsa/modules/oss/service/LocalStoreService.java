package org.dsa.modules.oss.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.dsa.modules.oss.config.StorageConfig;
import org.dsa.modules.oss.dto.FileDto;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;


@Slf4j
public class LocalStoreService extends DiskStorageService{

    public LocalStoreService(StorageConfig config){
        this.config = config;
        makeDir();
    }

    @Override
    public FileDto upload(MultipartFile file, String filePath) {

        FileDto fileEntity = new FileDto();
        File fileDir = new File(basePath);
        //如果路径不存在则创建
        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }

        String filename = file.getOriginalFilename();
        File sourceFile = new File(basePath + File.separator + filename);
        if (sourceFile.exists()) {
            File newFile = new File(basePath + File.separator + filename);
            //备份原文件
            Long timestamp = System.currentTimeMillis();
            File oldFile = new File(basePath + File.separator + filename + timestamp);
            if (oldFile.exists()) {
                //备份文件已存在，将备份文件删除
                oldFile.delete();
            }
            newFile.renameTo(oldFile);
        }

        try {
            FileUtils.copyInputStreamToFile(file.getInputStream(), sourceFile);
        } catch (IOException e) {
            log.error("local store MultipartFile upload: {}", e.getMessage());
        }

        try {
            String md5Hex = DigestUtils.md5Hex(file.getInputStream());
            fileEntity.setFileMd5(md5Hex);
        }catch (Exception ex){
            log.error("local store upload md5 error:{}", ex.getMessage());
        }

        fileEntity.setFileSize(file.getSize());
        fileEntity.setName(file.getOriginalFilename());
        fileEntity.setUrl(filePath);
        //判断是否要存库
        return fileEntity;
    }

    @Override
    public FileDto move(String targetPath, String sourcePath) {

        File sourceFile = new File(sourcePath);

        if(!sourceFile.exists()){
            return null;
        }
        File targetFile = new File(targetPath);
        if(targetFile.exists()){
            String path = targetFile.getAbsolutePath();
            String name = targetFile.getName();
            Long timestamp = System.currentTimeMillis();
            File bakFile = new File(path + name + timestamp);
            targetFile.renameTo(bakFile);
        }
        sourceFile.renameTo(targetFile);

        FileDto entity = new FileDto();
        entity.setName(targetFile.getName());

        entity.setFileSize(targetFile.length());
        entity.setUrl(targetFile.getAbsolutePath());
        try {
            String md5Hex = DigestUtils.md5Hex(new FileInputStream(targetPath));
            entity.setFileMd5(md5Hex);
        }catch (Exception ex){
            log.error("local store move md5 error:{}", ex.getMessage());
        }

        return entity;
    }

    @Override
    public FileDto upload(InputStream input, String filePath) throws IOException {
        int index;
        byte[] bytes = new byte[1024*10];
        File file = new File(filePath);
        FileOutputStream downloadFile = new FileOutputStream(file);
        while ((index = input.read(bytes)) != -1) {
            downloadFile.write(bytes, 0, index);
            downloadFile.flush();
        }
        FileDto entity = new FileDto();
        try {
            String md5Hex = DigestUtils.md5Hex(input);
            entity.setFileMd5(md5Hex);
        }catch (Exception ex){
            log.error("local store stream md5 error:{}", ex.getMessage());
        }
        entity.setName(file.getName());
        entity.setFileSize(file.length());
        entity.setUrl(file.getAbsolutePath());
        downloadFile.close();
        input.close();
        return entity;
    }

    @Override
    public FileDto upload(byte[] bytes, String filePath) throws IOException {
        File file = new File(filePath);

        FileOutputStream downloadFile = new FileOutputStream(file);
        downloadFile.write(bytes);
        downloadFile.flush();
        downloadFile.close();

        FileDto entity = new FileDto();
        entity.setName(file.getName());
        entity.setFileSize(file.length());
        entity.setUrl(file.getAbsolutePath());
        try {
            String md5Hex = DigestUtils.md5Hex(bytes);
            entity.setFileMd5(md5Hex);
        }catch (Exception ex){
            log.error("local store byte md5 error:{}", ex.getMessage());
        }
        return entity;
    }
}
