package org.dsa.modules.vehicle.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.vehicle.entity.DVehicleInfoEntity;
import org.dsa.modules.vehicle.entity.DVehicleTypeEntity;
import org.dsa.modules.vehicle.vo.VehicleInfoExportVo;
import org.dsa.modules.vehicle.vo.VehicleInfoPageReqVo;

import java.util.List;
import java.util.Map;

/**
 * 车辆信息表
 */
@Mapper
public interface DVehicleInfoDao extends BaseMapper<DVehicleInfoEntity> {

    /**
     * 根据vin码查询车辆信息表
     */
    DVehicleInfoEntity selectOneByVin(@Param("vin") String vin);


    /**
     * 根据车辆类型id获取车辆类型详细信息
     */
//    IPage<DVehicleInfoEntity> searchAllByVehicleTypeIdAndVin(IPage<DVehicleInfoEntity> page,
//                                                             @Param("vehicleTypeIds") List<Long> vehicleTypeIds,
//                                                             @Param("vin") String vin, @Param("tag") Long tag, @Param("regular") String regular);
    Page<DVehicleInfoEntity> searchAllByVehicleTypeIdAndVin(Page<DVehicleInfoDao> page,@Param("vo") VehicleInfoPageReqVo vo);
    /**
     * 根据id获取车辆信息
     *
     * @param id
     * @return
     */
    DVehicleInfoEntity selectOneById(Long id);

//    /**
//     * 根据Id更新车辆信息
//     */
//    Integer updateInfoById(DVehicleInfoEntity entity);

    /**
     * 是否存在vin
     *
     * @param vin
     * @return
     */
    Integer selectByVin(String vin);

    /**
     * 检查同一车型下的orderNum是否已存在
     *
     * @param vehicleType
     * @return
     */
    Long checkVehicleType(DVehicleTypeEntity vehicleType);

    /**
     * 检查同一车型下的orderNum是否已存在
     *
     * @param vehicleType
     * @return
     */
    DVehicleInfoEntity checkVehicleTypeByValue(DVehicleTypeEntity vehicleType);


    Page<String> getVinCode(Page<String> page, @Param("key") String key, @Param("vin") String vin, @Param("tag") Long tag, @Param("regular") String regular);

    List<String> getVinCodeList(@Param("key") String key, @Param("vin") String vin, @Param("tag") Long tag, @Param("regular") String regular);

    List<String> vinCode(@Param("subList") List<String> subList);

    List<DVehicleInfoEntity> getAllByVehicleTypeIdAndVin(@Param("vehicleTypeIds") List<Long> vehicleTypeIds,
                                                         @Param("vin") String vin, @Param("tag") List<Long> tag, @Param("regular") String regular);

    Long countAllByVehicleTypeIdAndVin(@Param("vehicleTypeIds") List<Integer> vehicleTypeIds,
                                                         @Param("vin") String vin, @Param("tag") Long tag, @Param("regular") String regular);

    void insertVehicle(@Param("vehicle") DVehicleInfoEntity entity);

    void inserts(@Param("vehicles") List<DVehicleInfoEntity> entities);

    List<Long> searchVehicleTypeIdByVin(@Param("vin") String vin);

    List<Map<String, String>> getDidValueByVehicleTypeIdsAndEcuNameAndDid(@Param("vehicleTypeIds") List<Long> vehicleTypeIds, @Param("ecuNames") List<String> ecuNames, @Param("did") String did);

    List<VehicleInfoExportVo> getAllForExport(List<Long> vehicleTypeIds, String vin, List<Long> tag, String regular);

    String getWholeType(Long vehicleTypeId);
}