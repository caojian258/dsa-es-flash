package org.dsa.modules.reporter.vo.diag;


import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString(callSuper = true)
public class CtsChartReVo {

    private List<String> dates;

    private String startDate;

    private String endDate;

    private String vinCode;

    private List<Long> vehType;

    private String ecu;

    private Integer top;

    private String dtc;

    private String ctsName;
}
