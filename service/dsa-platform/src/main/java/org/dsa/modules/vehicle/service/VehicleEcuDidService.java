package org.dsa.modules.vehicle.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dsa.modules.vehicle.entity.VehicleEcuDid;

import java.util.List;

public interface VehicleEcuDidService extends IService<VehicleEcuDid> {

    public void batchInsertByEcu(Long VehicleTypeId, String ecuName, List<VehicleEcuDid> didList);
    /***
     * 单个新增 did_value为空时空字符串代替
     * @param did
     */
    public void oneInsert(VehicleEcuDid did);

    /***
     * 批量新增 did_value为空时空字符串代替
     * @param didList
     */
    public void batchInsert(List<VehicleEcuDid> didList);

    public void deleteByVehicleTypeEcuDid(Long VehicleTypeId,String ecuName, String did);

    public void deleteByVehicleTypeEcu(Long VehicleTypeId,String ecuName);

    public void deleteByVehicleType(Long VehicleTypeId);

    List<VehicleEcuDid> list(Long VehicleTypeId);
}
