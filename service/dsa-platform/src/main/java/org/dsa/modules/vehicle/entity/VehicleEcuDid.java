package org.dsa.modules.vehicle.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName(value = "d_vehicle_ecu_did", autoResultMap = true)
public class VehicleEcuDid implements Serializable {

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long vehicleTypeId;

    private String model;

    private String ecuName;

    private String ecuDescription;

    private String ecuDescriptionTi;

    private String did;

    private String didName;

    private String didNameTi;

    private String didValue;

    private Date createTime;

    private Date updateTime;
}
