package org.dsa.modules.remoteDiagnostic.vo;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.common.validator.group.ReqGroup;
import org.dsa.common.validator.group.UpdateGroup;
import org.hibernate.validator.constraints.Length;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

@Data
@ToString(callSuper = true)
public class ECUFillingCertificateReqVo {

    @NotNull(message = "projectId is null", groups = {ReqGroup.class})
    private String projectId;

    @NotBlank(message = "pcid is null", groups = {ReqGroup.class})
    private String deviceID;
}
