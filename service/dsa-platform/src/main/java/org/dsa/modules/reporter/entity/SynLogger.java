package org.dsa.modules.reporter.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class SynLogger implements Serializable {

    private String id;

    private String vin;

    private String userName;

    private String sessionId;

    private String functionCode;

    private String actionCode;

    private String titleEn;

    private String titleCn;

    private String startTime;

    private String endTime;

    private String result;

    private String resultMessageEn;

    private String resultMessageCn;

    private String source;

    /**
     * 日志路径
     */
    private String logPath;
}
