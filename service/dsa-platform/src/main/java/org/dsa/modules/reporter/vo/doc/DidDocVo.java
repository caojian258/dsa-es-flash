package org.dsa.modules.reporter.vo.doc;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import org.dsa.modules.reporter.document.ElasticEntity;

import java.util.Date;

@Data
public class DidDocVo extends ElasticEntity {

    private String sessionInfoId;

    private Integer recordFlag;

    private String vin;

    private String sessionId;

    private String ecuName;

    private String didHex;

    private String didName;

    private String didNameTi;

    private String parameterName;

    private String parameterNameTi;

    private String value;

    private String valueTi;

    private String nrcHex;

    private String nrcDescription;

    private String nrcDescriptionTi;

    private String errorCode;

    private String errorMessage;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date lastUpdateTime;
}
