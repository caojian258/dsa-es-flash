package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.entity.AppInstall;
import org.dsa.modules.reporter.vo.client.AppInstallResVo;

import java.util.List;

@Mapper
public interface AppInstallMapper extends BaseMapper<AppInstall> {
    List<AppInstallResVo> getListByPcid(@Param("pcid") String pcid, @Param("tag") String tag);
}
