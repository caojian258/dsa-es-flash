package org.dsa.modules.reporter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("ecu_version_overview")
public class EcuVersionOverview extends BaseEntity {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String vin;

    private String ti;

    private String ecuName;

    private String versionName;

    private String versionValue;

    private String sessionId;

    private String tag;

    private Date collectTime;

    private String vehicleVersion;

    private Integer source;
}
