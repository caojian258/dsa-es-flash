package org.dsa.remote.service;


import org.dsa.modules.remoteDiagnostic.dto.EcuDto;
import org.dsa.modules.remoteDiagnostic.dto.EcuIdentificationDto;
import org.dsa.modules.remoteDiagnostic.service.RemoteVehicleEcuService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EcuServiceTest {

    @Autowired
    RemoteVehicleEcuService remoteVehicleEcuService;


    @Test
    public void test() {

        ArrayList<EcuDto> list = new ArrayList<>();
        EcuDto ecuDto = new EcuDto();
        ecuDto.setDescription("test13");
        ecuDto.setShortName("网关1");
        ecuDto.setPartNumberName("F187");
        ecuDto.setPartNumberVal("002");
        ecuDto.setVehicleTypeId(7L);

        ecuDto.setGroupName("GW");
        ecuDto.setEcuName("GW-1");

        ArrayList<EcuIdentificationDto> identifications = new ArrayList<>();
        identifications.add(new EcuIdentificationDto("F187", "002", "partNumber", "零件号"));
        identifications.add(new EcuIdentificationDto("F189", "002", "partNumber", "软件版本"));

        ecuDto.setIdentifications(identifications);

        EcuDto ecuDto2 = new EcuDto();
        ecuDto2.setDescription("test12");
        ecuDto2.setShortName("网关2");
        ecuDto2.setPartNumberName("F187");
        ecuDto2.setPartNumberVal("003");
        ecuDto2.setVehicleTypeId(7L);

        ecuDto2.setGroupName("GW");
        ecuDto2.setEcuName("GW-2");

        ArrayList<EcuIdentificationDto> identifications2 = new ArrayList<>();
        identifications2.add(new EcuIdentificationDto("F187", "003", "partNumber", "零件号"));
        identifications2.add(new EcuIdentificationDto("F189", "002", "partNumber", "软件版本"));

        ecuDto2.setIdentifications(identifications2);


        list.add(ecuDto);
        list.add(ecuDto2);
        remoteVehicleEcuService.analyzingEcu(list);
    }

    @Test
    public void test2() {

        ArrayList<EcuDto> list = new ArrayList<>();
        EcuDto ecuDto = new EcuDto();
        ecuDto.setDescription("test13");
        ecuDto.setShortName("AA");
        ecuDto.setPartNumberName("F187");
        ecuDto.setPartNumberVal("A1A");
        ecuDto.setVehicleTypeId(7L);

        ecuDto.setGroupName("BB");
        ecuDto.setEcuName("BB");

        ArrayList<EcuIdentificationDto> identifications = new ArrayList<>();
        identifications.add(new EcuIdentificationDto("F187", "A1A", "partNumber", "零件号"));
        identifications.add(new EcuIdentificationDto("F189", "002", "partNumber", "软件版本"));

        ecuDto.setIdentifications(identifications);

        EcuDto ecuDto2 = new EcuDto();
        ecuDto2.setDescription("test12");
        ecuDto2.setShortName("BB");
        ecuDto2.setPartNumberName("F187");
        ecuDto2.setPartNumberVal("A2A");
        ecuDto2.setVehicleTypeId(7L);

        ecuDto2.setGroupName("BB");
        ecuDto2.setEcuName("BB");

        ArrayList<EcuIdentificationDto> identifications2 = new ArrayList<>();
        identifications2.add(new EcuIdentificationDto("F187", "A2A", "partNumber", "零件号"));
        identifications2.add(new EcuIdentificationDto("F189", "002", "partNumber", "软件版本"));

        ecuDto2.setIdentifications(identifications2);


        list.add(ecuDto);
        list.add(ecuDto2);
        remoteVehicleEcuService.analyzingEcu(list);
    }
}
