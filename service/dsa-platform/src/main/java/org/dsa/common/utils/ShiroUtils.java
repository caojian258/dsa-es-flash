package org.dsa.common.utils;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.dsa.common.exception.RRException;
import org.dsa.modules.sys.entity.DisplayUserEntity;

/**
 * Shiro工具类
 *
 */
public class ShiroUtils {

	public static Session getSession() {
		return SecurityUtils.getSubject().getSession();
	}

	public static Subject getSubject() {
		return SecurityUtils.getSubject();
	}

	public static DisplayUserEntity getUserEntity() {
		return (DisplayUserEntity)SecurityUtils.getSubject().getPrincipal();
	}

	public static Long getUserId() {
		if(getUserEntity()!=null){
			return Long.valueOf(getUserEntity().getUserInfo().getUserId());
		}
		return 0L;
	}

	public static String getUserName() {
		if(getUserEntity()!=null){
			return getUserEntity().getUserInfo().getUsername();
		}
		return "";
	}
	public static String getUserDisplayName() {
		if(getUserEntity()!=null){
			return getUserEntity().getUserInfo().getUserDisplayName();
		}
		return "";
	}
	public static String getWorkShop() {
		if(getUserEntity()!=null){
			return getUserEntity().getUserInfo().getWorkShop();
		}
		return "";
	}
	public static void setSessionAttribute(Object key, Object value) {
		getSession().setAttribute(key, value);
	}

	public static Object getSessionAttribute(Object key) {
		return getSession().getAttribute(key);
	}

	public static boolean isLogin() {
		return SecurityUtils.getSubject().getPrincipal() != null;
	}

	public static String getKaptcha(String key) {
		Object kaptcha = getSessionAttribute(key);
		if(kaptcha == null){
			throw new RRException("验证码已失效");
		}
		getSession().removeAttribute(key);
		return kaptcha.toString();
	}


}
