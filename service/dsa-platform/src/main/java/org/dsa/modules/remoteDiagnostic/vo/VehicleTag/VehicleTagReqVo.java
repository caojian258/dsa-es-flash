package org.dsa.modules.remoteDiagnostic.vo.VehicleTag;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;


@Data
@ToString
public class VehicleTagReqVo implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    private String vin;

    private List<Long> tags;

    private List<String> vinList;

}
