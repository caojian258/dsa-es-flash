package org.dsa.modules.sys.controller;

import org.dsa.common.utils.R;
import org.dsa.modules.sys.service.SysMonitorService;
import org.dsa.modules.sys.vo.monitor.MonitorVo;
import org.dsa.modules.sys.vo.monitor.RedisCommandStatsVo;
import org.dsa.modules.sys.vo.monitor.RedisInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisServerCommands;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import jakarta.annotation.Resource;
import java.util.*;

@RestController
@RequestMapping("/sys/monitor")
public class SysMonitorController {
    @Autowired
    private SysMonitorService sysMonitorService;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 统计数据库表的总条数 和 表大小
     * @param monitorVo 搜索条件/排序条件/分页
     * @return data
     */
    @RequestMapping("/countTable")
    public R countTable(@RequestBody MonitorVo monitorVo) {
        return R.ok().put("data", sysMonitorService.getCountTable(monitorVo));
    }

    @GetMapping("/redis")
    public R countTable() {
        Properties infos = stringRedisTemplate.execute((RedisCallback<Properties>) RedisServerCommands::info);
         Map<String, Object> data = new HashMap<>();

        Map<String, String> redisInfo = new HashMap<>();
        List<RedisInfoVo> redisInfos = new ArrayList<>();

        infos.forEach((key, value)->{
            RedisInfoVo infoVo = new RedisInfoVo();
            infoVo.setItem(key.toString());
            infoVo.setValue(value.toString());
            redisInfos.add(infoVo);
        });

        data.put("info", redisInfos);
        //数据库的 key 的数量
        Long dbSize = stringRedisTemplate.execute(RedisServerCommands::dbSize);
        RedisInfoVo dbSizeInfo = new RedisInfoVo();
        dbSizeInfo.setItem("dbSize");
        dbSizeInfo.setValue(dbSize);
        redisInfos.add(dbSizeInfo);

        data.put("dbSize", dbSize);

        Properties commandStats = stringRedisTemplate.execute((
                RedisCallback<Properties>) connection -> connection.info("commandstats"));

        List<RedisCommandStatsVo> commandstatsInfo = new ArrayList<>();

        commandStats.forEach((key, value) -> {

            String[] valArr = value.toString().split(",");
            RedisCommandStatsVo vo = new RedisCommandStatsVo();
            vo.setCmd(key.toString());
            for (int i=0;i<valArr.length;i++){
                String[] items = valArr[i].split("=");
                if(items.length==2){
                    if("calls".equals(items[0]) ){
                        vo.setCalls(Long.parseLong(items[1]));
                    }
                    if("usec".equals(items[0]) ){
                        vo.setUsec(Long.parseLong(items[1]));
                    }
                    if("usec_per_call".equals(items[0]) ){
                        vo.setUsecPerCall(Double.parseDouble(items[1]));
                    }
                    if("rejected_calls".equals(items[0]) ){
                        vo.setRejectedCalls(Long.parseLong(items[1]));
                    }
                    if("failed_calls".equals(items[0]) ){
                        vo.setFailedCalls(Long.parseLong(items[1]));
                    }
                }
            }
            commandstatsInfo.add( vo);
        });

        data.put("commandstats", commandstatsInfo);
        return R.ok().put("data", data);
    }

}
