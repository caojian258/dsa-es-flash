package org.dsa.modules.reporter.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("d_session_event_log")
public class SessionEventLog extends BaseEntity{

    @TableId(type = IdType.AUTO)
    private Long id;

    private String vin;

    private String sessionId;

    private String state;

    private String userName;

    private String usedVciIdentification;

    private String usedVciSerialNumber;

    private String timestampText;

    private String pcid;

    private String workshop;

    private String language;

    private String tag;

    private Date collectTime;

}
