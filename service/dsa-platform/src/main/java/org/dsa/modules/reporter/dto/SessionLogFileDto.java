package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class SessionLogFileDto implements Serializable {

    private String prefix;

    private String sessionId;

    private String pcid;

    private Date timestamp;

    private String fileName;

    private String vin;
}
