package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.util.Date;

@Data
public class EcuVersionHistoryDto {

    private String versionName;

    private String versionValue;

    private Date collectTime;

    private String ecuName;

    private String vehicleVersion;

    private String workshop;

    private String pcid;

    private String sessionId;

    private String tag;

    private String vin;
}
