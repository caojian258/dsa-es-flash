package org.dsa.modules.onlineUpdate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 规则
 * 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@TableName("o_rule_matched_result")
public class RuleMatchedRecordEntity{

	/**
	 * 规则集合
	 */
	private Long ruleId;
	/**
	 * 软件版本Id
	 */
	private Long softwareVersionId;

	/**
	 * 比对结果
	 */
	private String result;
	/**
	 * 升级前的软件版本id
	 */
	private Long oldSoftwareVersionId;

}
