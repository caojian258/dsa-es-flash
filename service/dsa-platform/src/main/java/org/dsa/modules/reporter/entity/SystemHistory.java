package org.dsa.modules.reporter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain = true)
@TableName("client_os_history")
public class SystemHistory implements Serializable {

    @TableId(type = IdType.AUTO)
    private Long id;

    private int failedInstallationCount;

    private int okInstallationCount;

    private int errorExceptionCount;

    private Long workshopId;

    private String workshop;

    private String pcid;

    private String username;

    private String vinCode;

    private String applicationLocale;

    private Object windowsHotfixes;

    private Object smartstartLibs;

    private Object firewalls;

    private Object proxies;

    private String proxyFlag;

    private String firewallFlag;

    private Object wlanNetworks;

    private Object readMessages;

    private Object activeNetworkInterfaces;

    private Object installationProtocol;

    private Object protocolItems;

    private Object applications;

    private String smartstartVersion;

    //车企品牌
    private String brand;

    private String windowsVersion;

    private String antivirInstalled;

    private String antivirFlag;

    //内存使用
    private Long ramUsed;

    //内存
    private Long ramTotal;

    private String diskSerial;

    private String downloadSpeed;

    //磁盘
    private Long diskTotal;

    //磁盘
    private Long diskUsed;

    //系统语言
    private String systemLocale;

    private String localIp;

    private String wlanFlag;

    private String cpuSerial;

    private String cpuArch;

    private String cpuInfo;

    //显示器分辨率
    private String desktopResolution;

    //CPU 处理器
    private String processors;

    //说明
    private String systemInfoOutput;

    //上传时间
    private String timestampText;

    private Long timestamp;

    private Long sessionstart;

    private String sessionid;

    private String tag;

    private String windowsId;

    private String collectDate;

    private Date collectTime;

    private Date createdAt;

    private Date updatedAt;

    private String clientType;

    private Object downloadProtocol;

    private String macAddress;
}
