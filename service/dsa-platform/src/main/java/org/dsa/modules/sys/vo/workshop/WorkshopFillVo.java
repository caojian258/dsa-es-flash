package org.dsa.modules.sys.vo.workshop;

import lombok.Data;

import java.io.Serializable;

@Data
public class WorkshopFillVo implements Serializable {

    private String no;

    private String code;

    private String shortName;

    private String name;

    private String countryName;

    private String region;

    private String provinceName;

    private String cityName;

    private String note;
}
