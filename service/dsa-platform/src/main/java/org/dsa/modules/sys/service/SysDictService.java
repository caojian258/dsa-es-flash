package org.dsa.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;
import org.dsa.common.utils.PageUtils;
import org.dsa.modules.sys.entity.SysDictEntity;

import java.util.List;
import java.util.Map;

/**
 * 数据字典
 *
 */
public interface SysDictService extends IService<SysDictEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 根据类型获取字典表信息
     * @param type
     * @return
     */
    List<SysDictEntity> getSysDictByType(@Param("type") String type);
}

