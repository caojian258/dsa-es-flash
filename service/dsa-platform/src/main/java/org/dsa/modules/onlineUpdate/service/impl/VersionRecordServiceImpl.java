package org.dsa.modules.onlineUpdate.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.dsa.modules.onlineUpdate.dao.VersionRecordDao;
import org.dsa.modules.onlineUpdate.entity.VersionRecordEntity;
import org.dsa.modules.onlineUpdate.service.VersionRecordService;
import org.springframework.stereotype.Service;


@Service("VersionRecordService")
public class VersionRecordServiceImpl extends ServiceImpl<VersionRecordDao, VersionRecordEntity> implements VersionRecordService {
}
