package org.dsa.modules.diagnostic.entity;


import lombok.Data;

import java.io.Serializable;

/**
 * 远程诊断 - 读取冰结帧
 */
@Data
public class ReadDtcGridEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    String vin;
    String ecuName;
    String dtcCode;
    String dtcCodeInt;

}
