package org.dsa.modules.sys.service;

import org.dsa.modules.sys.entity.FileEntity;
import org.springframework.web.multipart.MultipartFile;

import jakarta.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Map;

public interface SysFileService {

    /**
     * 文件上传
     * @param file
     * @param req
     */
    public FileEntity uploadFiles(MultipartFile file, String versionType, HttpServletRequest req) ;

    /**
     * 通用文件上传接口
     * @param file
     * @param filePath
     */
    public FileEntity uploadFile(MultipartFile file, String filePath) ;

    /**
     * 通用文件上传接口
     * @param file
     * @param filePath
     */
    public FileEntity uploadVersionFile(MultipartFile file, String filePath) ;


    /**
     * 下载文件
     * @param fileName
     */
    public InputStream downloadFiles(String fileName);

    /**
     * 删除文件
     *
     * @param url
     */
    public void deletefile(String url);

    /**
     * 多语言excel配置导入
     */
    void multiLanguageExcelImport(File file);

    /**
     * 多语言excel配置导出
     * @return
     */
    String multiLanguageExcelExport();

    /**
     * 多语言配置zip下载
     * @return
     * @throws FileNotFoundException
     */
    String multiLanguageZip() throws FileNotFoundException;

//    /**
//     * 翻译文件包下载
//     * @return
//     */
//    String translationZip() throws FileNotFoundException;

    /**
     * 车辆导入模板下载
     */
    Map<String,String> downloadVehicleTemplate(String languages);

    /**
     * 车辆信息导入
     */
    void vehicleExcelImport(File file);

    Map<String, String> downloadUserTemplate();

    void importUser(File file);
}
