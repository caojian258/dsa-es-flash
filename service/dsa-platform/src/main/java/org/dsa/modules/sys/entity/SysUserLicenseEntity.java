package org.dsa.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

/**
 * 系统用户
 *
 */
@Data
@TableName("t_user_license")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SysUserLicenseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**
	 * 用户ID
	 */
	@TableId(type = IdType.AUTO)
	private Long id;

	private String userId;

	private String userName;

	private String appCode;
	private String appName;
	private String appNameTi;

	private String oemCode;
	private String oemName;
	private String oemNameTi;
	private String productCode;
	private String productName;
	private String productNameTi;
	private String licenseTypeCode;
	private String licenseTypeName;
	private String licenseTypeNameTi;


}
