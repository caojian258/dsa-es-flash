package org.dsa.modules.remoteDiagnostic.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuGroupEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleTypeEcuGroupEntity;
import org.dsa.modules.remoteDiagnostic.vo.CheckNameReqVo;
import org.dsa.modules.remoteDiagnostic.vo.PublicPageReqVo;
import org.dsa.modules.remoteDiagnostic.vo.Vehicle.VehicleTypeEcuGroupReqVo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface RemoteVehicleEcuGroupService {

    Page<RemoteVehicleEcuGroupEntity> selectPage(PublicPageReqVo vo);

    RemoteVehicleEcuGroupEntity getInfo(Long id);

    void saveGroup(RemoteVehicleEcuGroupEntity group) throws IOException;

    void updateGroup(RemoteVehicleEcuGroupEntity group) throws IOException;

    // 获取所有的ecu且状态为可使用
    List<RemoteVehicleEcuGroupEntity> getGroups();

    List<RemoteVehicleEcuGroupEntity> getGroups(Integer status);

    List<RemoteVehicleEcuGroupEntity> getGroups(List<Long> ids);

    List<RemoteVehicleEcuGroupEntity> getGroups(List<Long> ids,Long versionId,Long typeId);

    Map<String, Object> getGroups(Long id);

    // 检查ecu是否重复
    Boolean check(CheckNameReqVo vo);

    List<RemoteVehicleEcuGroupEntity> getVehicleTypeEcuGroup(VehicleTypeEcuGroupReqVo vo);

    void saveVehicleTypeEcuGroup(VehicleTypeEcuGroupReqVo vo);

    void updateVehicleTypeEcuGroup(VehicleTypeEcuGroupReqVo vo);

    void deleteVehicleTypeEcuGroup(VehicleTypeEcuGroupReqVo vo);

    Page<RemoteVehicleEcuGroupEntity> selectEcuPage(PublicPageReqVo vo);

    // 删除车型时，删除车型与Group的关联
    void delDependByType(Long typeId);

    List<RemoteVehicleTypeEcuGroupEntity> selectGroupByType(Long typeId);
}
