package org.dsa.modules.remoteDiagnostic.dto.flash;

import lombok.Data;

import java.io.Serializable;

@Data
public class EcuVersionFileDto implements Serializable {

    //ecu version
    private Long versionId;

    //flash type APP EXE DATA
    private String flashType;
    //file type releaseNote flashFile
    private String fileType;

    private String fileName;

    private String filePath;

    private String fileMd5;

    //0增量 1全量
    private Integer status;
}
