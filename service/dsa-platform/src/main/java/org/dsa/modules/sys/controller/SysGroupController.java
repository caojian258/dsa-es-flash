package org.dsa.modules.sys.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.dsa.common.annotation.SysLog;
import org.dsa.common.utils.PageUtils;
import org.dsa.common.utils.R;
import org.dsa.modules.sys.entity.SysGroupEntity;
import org.dsa.modules.sys.service.SysGroupService;
import org.dsa.modules.sys.service.SysWorkshopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * 组管理
 *
 */
@RestController
@RequestMapping("/sys/group")
public class SysGroupController extends AbstractController {
	@Autowired
	private SysGroupService sysGroupService;
	@Autowired
	private SysWorkshopService sysWorkshopService;
	/**
	 * 列表
	 */
	@RequestMapping("/list")
	@RequiresPermissions("sys:group:list")
	public R list(@RequestParam Map<String, Object> params){

		PageUtils page = sysGroupService.queryPage(params);

		return R.ok().put("page", page);
//		for(SysWorkshopEntity sysWorkshopEntity : workshopList){
//			SysWorkshopEntity parentWorkshopEntity = sysWorkshopService.getById(sysWorkshopEntity.getParentId());
//			if(parentWorkshopEntity != null){
//				sysWorkshopEntity.setParentName(parentWorkshopEntity.getName());
//			}
//		}
	}

	/**
	 * 选择部门(添加、修改菜单)
	 */
	@RequestMapping("/select")
	//@RequiresPermissions("sys:workshop:select")
	public R select(){
		List<SysGroupEntity> groupList = sysGroupService.querySelectList();
		return R.ok().put("groupList", groupList);
	}
	
	/**
	 *查询事件转派组信息
	 */
	@RequestMapping("/transfer/select")
	//@RequiresPermissions("sys:workshop:select")
	public R transferSelect(){
		List<SysGroupEntity> groupList = sysGroupService.queryTransferGroupList();
		return R.ok().put("groupList", groupList);
	}
	
	@RequestMapping("/query")
	@RequiresPermissions("sys:group:list")
	public R queryByName(@RequestParam Map<String, Object> params){
		PageUtils page =  sysGroupService.queryPage(params);
		return R.ok().put("page",page);

	}

	@RequestMapping("/queryByWorkshopId")
	@RequiresPermissions("sys:group:list")
	public R queryByWorkshopId(Long workshopId){
		List<SysGroupEntity> groupList =  sysGroupService.queryByWorkshopId(workshopId);
		return R.ok().put("group",groupList);

	}

//	/**
//	 * 选择部门(添加、修改菜单)
//	 */
//	@RequestMapping("/select")
//	@RequiresPermissions("sys:workshop:select")
//	public R select(){
//		List<SysWorkshopEntity> workshopList = sysGroupService.queryList(new HashMap<String, Object>());
//
//		//添加一级部门
//		if(getUserId() == Constant.SUPER_ADMIN){
//			SysWorkshopEntity root = new SysWorkshopEntity();
//			root.setWorkshopId(0L);
//			root.setName("一级部门");
//			root.setParentId(-1L);
//			root.setOpen(true);
//			workshopList.add(root);
//		}
//
//		return R.ok().put("workshopList", workshopList);
//	}

//	/**
//	 * 上级部门Id(管理员则为0)
//	 */
//	@RequestMapping("/info")
//	@RequiresPermissions("sys:workshop:list")
//	public R info(){
//		long workshopId = 0;
//		if(getUserId() != Constant.SUPER_ADMIN){
//			List<SysWorkshopEntity> workshopList = sysGroupService.queryList(new HashMap<String, Object>());
//			Long parentId = null;
//			for(SysWorkshopEntity sysWorkshopEntity : workshopList){
//				if(parentId == null){
//					parentId = sysWorkshopEntity.getParentId();
//					continue;
//				}
//
//				if(parentId > sysWorkshopEntity.getParentId().longValue()){
//					parentId = sysWorkshopEntity.getParentId();
//				}
//			}
//			workshopId = parentId;
//		}
//
//		return R.ok().put("workshopId", workshopId);
//	}
//
	/**
	 * 信息
	 */
	@RequestMapping("/info/{groupId}")
	public R info(@PathVariable("groupId") Long groupId){
		SysGroupEntity group = sysGroupService.getById(groupId);

		return R.ok().put("group", group);
	}
	
	/**
	 * 保存
	 */
	@RequestMapping("/save")
	@SysLog("保存分组")
	@RequiresPermissions("sys:group:save")
	public R save(@RequestBody SysGroupEntity group){
		sysGroupService.save(group);
		
		return R.ok();
	}
	
	/**
	 * 修改
	 */
	@RequestMapping("/update")
	@SysLog("修改分组")
	@RequiresPermissions("sys:group:update")
	public R update(@RequestBody SysGroupEntity group){
		sysGroupService.updateById(group);
		
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@RequestMapping("/delete/{groupId}")
	@SysLog("删除分组")
	@RequiresPermissions("sys:group:delete")
	public R delete(@PathVariable("groupId") long groupId){
		sysGroupService.removeById(groupId);
		return R.ok();
	}
	
}
