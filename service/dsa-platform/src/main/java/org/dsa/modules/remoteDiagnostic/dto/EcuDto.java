package org.dsa.modules.remoteDiagnostic.dto;


import lombok.*;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class EcuDto {

    // 名称英文  必传
    private String groupName;

    // 业务名称  必传
    private String ecuName;

    // 车型 必传
    private Long vehicleTypeId;

    private String description;

    private String shortName;

    // 识别信息 (也需要存放零件号)
    private List<EcuIdentificationDto> identifications;

    // 零件号名称 必传
    private String partNumberName;

    // 零件号值  必传
    private String partNumberVal;

}
