CREATE TABLE public.client_application_history (
	id bigserial NOT NULL,
	device_id varchar(100) NOT NULL,
	session_id varchar(100) NOT NULL,
	user_id int8 NULL,
	username varchar(100) NULL,
	user_display_name varchar(200) NULL,
	workshop_id int8 NULL,
	workshop_name varchar(50) NULL,
	app_id int4 NOT NULL,
	app_type varchar(255) NOT NULL,
	app_name varchar(255) NOT NULL,
	app_version varchar(255) NULL,
	app_prev_version varchar(255) NULL,
	version_number int4 NULL,
	prev_version_number int4 NULL,
	status_type int4 NOT NULL,
	status int4 NOT NULL,
	update_id int4 NOT NULL,
	update_type int2 NULL,
	expire_date date NULL,
	expire_warning_days int4 NULL,
	release_notes text NULL,
	start_time timestamp NULL,
	end_time timestamp NULL,
	collect_date timestamp NOT NULL,
	tag varchar(50) NOT NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	CONSTRAINT client_application_history_pkey PRIMARY KEY (id),
	CONSTRAINT uk_update_unique UNIQUE (session_id, update_id)
);

CREATE TABLE public.client_application_installation (
	id bigserial NOT NULL,
	workshop varchar(255) NULL,
	pcid varchar(255) NOT NULL,
	app_id int4 NULL,
	collect_time timestamp NULL,
	"type" varchar(255) NULL,
	"name" varchar(255) NULL,
	"version" varchar(255) NULL,
	install_result varchar(25) NULL,
	install_time varchar(255) NULL,
	install_msg varchar(255) NULL,
	update_oid varchar(255) NULL,
	tag varchar(255) NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	collect_date varchar(20) NULL,
	CONSTRAINT client_application_installation_pkey PRIMARY KEY (id)
);
CREATE INDEX client_application_installation_workshop_idx ON public.client_application_installation USING btree (workshop, pcid, type, name);

CREATE TABLE public.client_application_overview (
	id bigserial NOT NULL,
	workshop varchar(255) NULL,
	pcid varchar(255) NULL,
	collect_time timestamp NULL,
	"type" varchar(255) NULL,
	"name" varchar(255) NULL,
	"version" varchar(255) NULL,
	app_id int4 NULL,
	prev_version varchar(255) NULL,
	upgrade_time varchar(255) NULL,
	upgrade_note varchar(255) NULL,
	tag varchar(255) NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	collect_date varchar(20) NULL,
	CONSTRAINT client_application_overview_pkey PRIMARY KEY (id)
);
CREATE UNIQUE INDEX client_application_overview_workshop_idx ON public.client_application_overview USING btree (workshop, pcid, type, name);
CREATE INDEX idx_application_name ON public.client_application_overview USING btree (name);

CREATE TABLE public.client_application_repair (
	id bigserial NOT NULL,
	app_id int4 NULL,
	workshop varchar(255) NULL,
	pcid varchar(255) NOT NULL,
	collect_date varchar(255) NULL,
	collect_time timestamp NULL,
	"type" varchar(255) NULL,
	"name" varchar(255) NULL,
	"version" varchar(255) NULL,
	"result" varchar(25) NULL,
	update_oid varchar(255) NULL,
	tag varchar(255) NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	CONSTRAINT client_application_repair_pkey PRIMARY KEY (id)
);

CREATE TABLE public.client_application_rollback (
	id bigserial NOT NULL,
	app_id int4 NULL,
	workshop varchar(255) NULL,
	pcid varchar(255) NOT NULL,
	collect_date varchar(255) NULL,
	collect_time timestamp NULL,
	"type" varchar(255) NULL,
	"name" varchar(255) NULL,
	"version" varchar(255) NULL,
	"result" varchar(25) NULL,
	update_oid varchar(255) NULL,
	tag varchar(255) NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	CONSTRAINT client_application_rollback_pkey PRIMARY KEY (id)
);

CREATE TABLE public.client_application_stats (
	id bigserial NOT NULL,
	app_id int4 NULL,
	collect_date date NOT NULL,
	app_type varchar(255) NULL,
	app_name varchar(255) NULL,
	app_version varchar(255) NOT NULL,
	version_number int4 NOT NULL,
	status_type int4 NOT NULL,
	status int2 NOT NULL,
	total int8 NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	CONSTRAINT client_application_his_pkey PRIMARY KEY (id),
	CONSTRAINT client_application_stats_un UNIQUE (app_id, collect_date, app_type, app_name, app_version, version_number, status_type, status)
);
CREATE UNIQUE INDEX client_application_stats_app_version_idx ON public.client_application_stats USING btree (app_type, app_id, version_number, status_type, status, collect_date);

CREATE TABLE public.client_application_uninstallation (
	id bigserial NOT NULL,
	workshop varchar(255) NULL,
	pcid varchar(255) NOT NULL,
	collect_date varchar(255) NULL,
	collect_time timestamp NULL,
	"type" varchar(255) NULL,
	"name" varchar(255) NULL,
	"version" varchar(255) NULL,
	"result" varchar(25) NULL,
	update_oid varchar(255) NULL,
	tag varchar(255) NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	app_id int4 NULL,
	CONSTRAINT client_application_uninstallation_pkey PRIMARY KEY (id)
);

CREATE TABLE public.client_application_version (
	id bigserial NOT NULL,
	workshop varchar(255) NULL,
	pcid varchar(255) NULL,
	"type" varchar(255) NULL,
	"name" varchar(255) NULL,
	"version" varchar(255) NULL,
	app_id int4 NULL,
	collect_time timestamp NULL,
	tag varchar(255) NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	collect_date varchar(20) NULL,
	CONSTRAINT client_application_version_pkey PRIMARY KEY (id)
);
CREATE INDEX client_application_version_workshop_idx ON public.client_application_version USING btree (workshop, pcid, type, name);


CREATE TABLE public.client_component_back (
	id bigserial NOT NULL,
	device_id varchar(100) NULL,
	software_id int4 NULL,
	component_id int4 NULL,
	component_version_name varchar(255) NULL,
	component_version_number int4 NOT NULL,
	component_version_path text NULL,
	update_type int2 NULL,
	file_size varchar(255) NULL,
	file_md5 varchar(255) NULL,
	release_notes text NULL,
	feature_description text NULL,
	tag varchar(50) NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	session_id varchar(100) NULL,
	software_version_number int4 NULL,
	CONSTRAINT client_component_back_pkey PRIMARY KEY (id)
);

CREATE TABLE public.client_current_component_info (
	id bigserial NOT NULL,
	device_id varchar(100) NULL,
	software_id int4 NULL,
	component_id int4 NULL,
	component_name varchar(255) NULL,
	component_version_name varchar(255) NULL,
	component_version_number int4 NOT NULL,
	component_install_path text NULL,
	start_update_time timestamp NULL,
	end_update_time timestamp NULL,
	update_fail_count int4 NULL,
	update_result int4 NULL,
	install_action int2 NULL,
	component_status int2 NULL,
	download_progress int4 NULL,
	file_size varchar(255) NULL,
	file_md5 varchar(255) NULL,
	release_notes text NULL,
	feature_description text NULL,
	tag varchar(50) NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	session_id varchar(100) NULL,
	software_version_number int4 NULL,
	update_type int4 NULL,
	current_update_type int4 NULL,
	CONSTRAINT client_current_component_info_pkey PRIMARY KEY (id)
);

CREATE TABLE public.client_current_software_info (
	id bigserial NOT NULL,
	device_id varchar(100) NULL,
	software_id int4 NULL,
	software_name varchar(255) NULL,
	software_version_name varchar(255) NULL,
	software_version_number int4 NOT NULL,
	software_work_path text NULL,
	software_install_path text NULL,
	start_update_time timestamp NULL,
	end_update_time timestamp NULL,
	update_fail_count int4 NULL,
	update_result int4 NULL,
	install_action int2 NULL,
	software_status int2 NULL,
	download_progress int4 NULL,
	file_size varchar(255) NULL,
	release_notes text NULL,
	feature_description text NULL,
	tag varchar(50) NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	session_id varchar(100) NULL,
	update_id varchar(200) NULL,
	CONSTRAINT client_current_software_info_pkey PRIMARY KEY (id)
);

CREATE TABLE public.client_dependence_info (
	id bigserial NOT NULL,
	device_id varchar(100) NULL,
	software_id int4 NULL,
	software_version_number int4 NULL,
	component_id int4 NULL,
	component_version_number int4 NULL,
	dependency_component_id int4 NULL,
	dependency_component_version_number int4 NULL,
	tag varchar(50) NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	session_id varchar(100) NULL,
	CONSTRAINT client_dependence_info_pkey PRIMARY KEY (id)
);

CREATE TABLE public.client_dependence_tree (
	id bigserial NOT NULL,
	pcid varchar(255) NULL,
	app_type varchar(255) NULL,
	app_id int4 NULL,
	app_name varchar(255) NULL,
	app_version varchar(255) NULL,
	app_version_number int4 NULL,
	dependency_id int4 NOT NULL DEFAULT 0,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	dependency_type varchar(20) NULL,
	CONSTRAINT client_dependence_tree_pkey PRIMARY KEY (id)
);

CREATE TABLE public.client_future_component_info (
	id bigserial NOT NULL,
	device_id varchar(100) NULL,
	software_id int4 NULL,
	component_id int4 NULL,
	component_version_name varchar(255) NULL,
	component_version_number int4 NOT NULL,
	component_download_path text NULL,
	update_type int2 NULL,
	file_size varchar(255) NULL,
	file_md5 varchar(255) NULL,
	release_notes text NULL,
	feature_description text NULL,
	tag varchar(50) NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	session_id varchar(100) NULL,
	order_number int4 NULL,
	download_progress int4 NULL,
	component_download_url varchar NULL,
	CONSTRAINT client_future_component_info_pkey PRIMARY KEY (id)
);

CREATE TABLE public.client_future_software_info (
	id bigserial NOT NULL,
	device_id varchar(100) NULL,
	software_id int4 NULL,
	software_version_name varchar(255) NULL,
	software_version_number int4 NOT NULL,
	update_type int2 NULL,
	expire_date date NULL,
	expire_warning_days int4 NULL,
	file_size varchar(255) NULL,
	release_notes text NULL,
	feature_description text NULL,
	tag varchar(50) NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	session_id varchar(100) NULL,
	download_progress int4 NULL,
	update_id varchar NULL,
	CONSTRAINT client_future_software_info_pkey PRIMARY KEY (id)
);

CREATE TABLE public.client_os_history (
	id bigserial NOT NULL,
	pcid varchar(255) NOT NULL,
	workshop_id int8 NULL,
	workshop varchar(255) NOT NULL,
	application_locale varchar(255) NULL,
	username varchar(50) NULL,
	windows_hotfixes json NULL,
	smartstart_libs json NULL,
	proxies json NULL,
	proxy_flag bpchar(3) NOT NULL,
	firewall_flag bpchar(3) NOT NULL,
	failed_installation_count int4 NULL,
	ok_installation_count int4 NULL,
	error_exception_count int4 NULL,
	smartstart_version varchar(255) NULL,
	brand varchar(50) NULL,
	windows_version varchar(255) NULL,
	windows_id varchar(255) NULL,
	antivir_installed varchar(255) NULL,
	antivir_flag bpchar(3) NOT NULL,
	ram_used int8 NULL,
	disk_serial varchar(255) NULL,
	download_speed varchar(255) NULL,
	disk_total int8 NULL,
	ram_total int8 NULL,
	system_locale varchar(255) NULL,
	local_ip varchar(255) NULL,
	wlan_networks json NULL,
	wlan_flag bpchar(3) NOT NULL,
	cpu_serial varchar(255) NULL,
	read_messages json NULL,
	processors varchar(255) NULL,
	disk_used int8 NULL,
	system_info_output text NULL,
	firewalls json NULL,
	active_network_interfaces json NULL,
	installation_protocol json NULL,
	"timestamp" int8 NULL,
	protocol_items json NULL,
	applications json NULL,
	tag varchar(50) NULL,
	desktop_resolution varchar NULL,
	timestamp_text varchar NULL,
	sessionid varchar NULL,
	sessionstart int8 NULL,
	vin_code varchar NULL,
	cpu_arch varchar NULL,
	cpu_info varchar NULL,
	collect_date varchar NULL,
	collect_time timestamp NULL,
	client_type varchar(100) NULL,
	download_protocol json NULL,
	created_at timestamp NULL,
    updated_at timestamp NULL,
    mac_address varchar(50) NULL,
	CONSTRAINT client_os_history_pkey PRIMARY KEY (id)
);

CREATE TABLE public.client_os_overview (
	id bigserial NOT NULL,
	pcid varchar(255) NOT NULL,
	workshop_id int8 NULL,
	workshop varchar(255) NOT NULL,
	application_locale varchar(255) NULL,
	proxy_flag bpchar(3) NOT NULL,
	firewall_flag bpchar(3) NOT NULL,
	smartstart_version varchar(255) NULL,
	windows_version varchar(255) NULL,
	windows_id varchar(255) NULL,
	antivir_installed varchar(255) NULL,
	antivir_flag bpchar(3) NOT NULL,
	ram_used int8 NULL,
	disk_serial varchar(255) NULL,
	download_speed varchar(255) NULL,
	disk_total int8 NULL,
	ram_total int8 NULL,
	system_locale varchar(255) NULL,
	local_ip varchar(255) NULL,
	wlan_flag bpchar(3) NOT NULL,
	cpu_serial varchar(255) NULL,
	cpu_arch varchar(200) NULL,
	cpu_info varchar(255) NULL,
	desktop_resolution varchar(255) NULL,
	processors varchar(255) NULL,
	disk_used int8 NULL,
	system_info_output text NULL,
	timestamp_text varchar(255) NULL,
	last_online_time timestamp NULL,
	last_username varchar(255) NULL,
	tag varchar(50) NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	brand varchar NULL,
	"timestamp" int8 NULL,
	runtime_version varchar NULL,
	CONSTRAINT client_os_overview_pkey PRIMARY KEY (id)
);

CREATE UNIQUE INDEX client_os_overview_pcid_workshop_idx ON public.client_os_overview USING btree (pcid, workshop);

CREATE TABLE public.client_os_protocol_item (
	id bigserial NOT NULL,
	workshop varchar(255) NOT NULL,
	pcid varchar(255) NOT NULL,
	"type" bpchar(20) NULL,
	severity varchar(255) NULL,
	collect_date varchar(50) NULL,
	timestamp_text varchar(50) NULL,
	reference_id varchar(255) NULL,
	message text NULL,
	tag varchar(255) NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	collect_time timestamp NULL,
	CONSTRAINT client_os_protocol_item_pkey PRIMARY KEY (id)
);

CREATE TABLE public.client_software_back (
	id bigserial NOT NULL,
	device_id varchar(100) NULL,
	software_id int4 NULL,
	software_version_name varchar(255) NULL,
	software_version_number int4 NOT NULL,
	update_type int2 NULL,
	validity_period timestamp NULL,
	near_validity_period_days int4 NULL,
	file_size varchar(255) NULL,
	release_notes text NULL,
	feature_description text NULL,
	tag varchar(50) NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	update_id varchar NULL,
	session_id varchar NULL,
	CONSTRAINT client_software_back_pkey PRIMARY KEY (id)
);

CREATE TABLE public.client_upgrade_result (
	id bigserial NOT NULL,
	device_id varchar(100) NOT NULL,
	update_id int4 NOT NULL,
	workshop_id int8 NULL,
	workshop_name varchar(50) NULL,
	username varchar(50) NULL,
	user_display_name varchar(200) NULL,
	software_id int4 NULL,
	component_id int4 NULL,
	start_time timestamp NULL,
	end_time timestamp NULL,
	fail_update_count int4 NULL,
	software_current_version_number int8 NULL,
	software_target_version_number int8 NULL,
	component_current_version_number int8 NULL,
	component_target_version_number int8 NULL,
	install_action int4 NOT NULL,
	update_result int4 NOT NULL,
	update_target int2 NOT NULL,
	tag varchar(50) NULL,
	ctime timestamp NULL DEFAULT now(),
	session_id varchar(100) NULL,
	remove_time timestamp NULL,
	software_current_version varchar NULL,
	component_current_version varchar NULL,
	software_target_version varchar NULL,
	component_target_version varchar NULL,
	CONSTRAINT client_upgrade_result_pkey PRIMARY KEY (id)
);
CREATE INDEX client_upgrade_result_device_id_idx ON public.client_upgrade_result USING btree (device_id);


CREATE TABLE public.client_workshop_user (
	id bigserial NOT NULL,
	workshop_id int4 NULL,
	workshop varchar(255) NULL,
	pcid varchar(255) NOT NULL,
	first_username varchar(255) NOT NULL,
	first_online_time timestamp NULL,
	first_online_ip varchar(255) NULL,
	last_online_time timestamp NULL,
	last_online_ip varchar(255) NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	last_username varchar(255) NULL,
	CONSTRAINT client_workshop_user_pkey PRIMARY KEY (id)
);


CREATE TABLE public.client_dependence_overview (
	id bigserial NOT NULL,
	device_id varchar(100) NULL,
	software_name varchar(250) NULL,
	software_version varchar(250) NULL,
	software_version_number int4 NULL,
	component_name varchar(250) NULL,
	component_version varchar(250) NULL,
	component_version_number int4 NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	CONSTRAINT client_dependence_overview_pkey PRIMARY KEY (id)
);

CREATE TABLE public.d_analysis_cube (
	id bigserial NOT NULL,
	table_name varchar(255) NULL,
	table_column varchar(255) NULL,
	dimesion varchar(255) NULL,
	"condition" text NULL,
	measure varchar(255) NULL,
	aggregator text NULL,
	visible varchar(255) NULL,
	enabled int4 NULL,
	"sql" text NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	date_column varchar(255) NULL,
	date_attribute varchar(255) NULL,
	top int4 NULL,
	code varchar NULL,
	CONSTRAINT d_analysis_cube_pkey PRIMARY KEY (id)
);


----------- workshop ------------
CREATE TABLE public.d_workshop (
	id varchar(255) NOT NULL,
	thirdpart_id varchar(255) NULL,
	"name" varchar(255) NOT NULL,
	name_en varchar(255) NULL,
	country varchar(255) NOT NULL DEFAULT 'CN'::character varying,
	province varchar(255) NULL,
	city varchar(255) NULL,
	district varchar(255) NULL,
	address varchar(255) NULL,
	brand varchar(255) NULL,
	workshop_group varchar(255) NULL,
	tag varchar(255) NULL,
	latitude numeric(18, 8) NULL,
	longitude numeric(18, 8) NULL,
	contact_name varchar(255) NULL,
	contact_way varchar(100) NULL,
	state int2 NOT NULL DEFAULT 1,
	last_login_time timestamp NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	CONSTRAINT d_workshop_pkey PRIMARY KEY (id)
);

CREATE TABLE public.d_workshop_group (
	id bigserial NOT NULL,
	"name" varchar(255) NOT NULL,
	description varchar(255) NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	parent_id int8 NULL DEFAULT 0,
	CONSTRAINT workshop_group_name_key UNIQUE (name),
	CONSTRAINT workshop_group_pkey PRIMARY KEY (id)
);

CREATE TABLE public.d_workshop_user (
	id bigserial NOT NULL,
	thirdpart_id varchar(255) NULL,
	username varchar(255) NULL,
	workshop_id varchar(255) NULL,
	real_name varchar(255) NULL,
	email varchar(255) NULL,
	mobile varchar(255) NULL,
	"password" varchar(255) NULL,
	last_login_time timestamp NULL,
	last_login_ip varchar(50) NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	address varchar NULL,
	max_license_number int4 NULL,
	save_license_id_as_bytes _bool NULL,
	telephone varchar NULL,
	b_limit_license_number _bool NULL,
	license_expire_duration int8 NULL,
	group_ref varchar NULL,
	license_id varchar NULL,
	license_hex_id varchar NULL,
	active bool NULL,
	CONSTRAINT d_workshop_user_pkey PRIMARY KEY (id),
	CONSTRAINT d_workshop_user_username_key UNIQUE (username)
);

CREATE TABLE public.d_workshop_user_group (
	id bigserial NOT NULL,
	parent_id int8 NULL,
	display_name varchar(50) NOT NULL,
	"name" varchar(50) NOT NULL,
	description varchar(255) NULL,
	ordered int4 NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	CONSTRAINT d_workshop_user_group_name_key UNIQUE (name),
	CONSTRAINT d_workshop_user_group_pkey PRIMARY KEY (id)
);

CREATE TABLE public.d_workshop_user_groups (
	id bigserial NOT NULL,
	group_id int8 NULL,
	group_name varchar(50) NOT NULL,
	user_id int8 NULL,
	username varchar(255) NOT NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	CONSTRAINT d_workshop_user_groups_pkey PRIMARY KEY (id)
);

----------- workshop ------------


----------- flash ------------
CREATE TABLE public.vehicle_flash_record (
	id bigserial NOT NULL,
	"version" varchar(255) NULL,
	vin varchar(255) NOT NULL,
	workshop varchar(255) NULL,
	pcid varchar(255) NULL,
	model varchar(255) NULL,
	prev_version varchar(255) NULL,
	vehicle_version varchar(255) NULL,
	username varchar(255) NULL,
	session_id varchar(255) NULL,
	spend_time int4 NULL,
	feature text NULL,
	start_time timestamp NULL,
	end_time timestamp NULL,
	"result" int4 NULL,
	reason text NULL,
	tag varchar(255) NULL,
	"mode" int4 NULL,
	collect_time timestamp NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	CONSTRAINT vehicle_flash_record_pkey PRIMARY KEY (id)
);


CREATE TABLE public.vehicle_flash_detail (
	id bigserial NOT NULL,
	flash_id int8 NOT NULL,
	vin varchar(255) NOT NULL,
	"version" varchar(255) NOT NULL,
	ecu_name varchar(255) NULL,
	did varchar(255) NULL,
	version_name varchar(255) NULL,
	version_value varchar(255) NULL,
	fid varchar(255) NULL,
	resource_id varchar(255) NULL,
	flash_timestamp timestamp NULL,
	"result" int4 NOT NULL,
	reason varchar(255) NULL,
	tag varchar(255) NULL,
	target int4 NULL,
	collect_time timestamp NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	CONSTRAINT vehicle_flash_detail_pkey PRIMARY KEY (id)
);

CREATE TABLE public.ecu_version_history (
	id bigserial NOT NULL,
	vin varchar(255) NOT NULL,
	vehicle_version varchar(255) NULL,
	ecu_name varchar(255) NULL,
	version_name varchar(255) NULL,
	version_value varchar(255) NULL,
	ti varchar(255) NULL,
	session_id varchar(255) NULL,
	tag varchar(255) NULL,
	"source" int4 NULL,
	collect_time timestamp NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	CONSTRAINT ecu_version_history_pkey PRIMARY KEY (id)
);

COMMENT ON COLUMN public.vehicle_version_history."source" IS '1 as maintained 0 as build';


CREATE TABLE public.ecu_version_overview (
	id bigserial NOT NULL,
	vin varchar(255) NOT NULL,
    vehicle_version varchar(255) NULL,
	ecu_name varchar(255) NULL,
	version_name varchar(255) NULL,
	version_value varchar(255) NULL,
	ti varchar(255) NULL,
	session_id varchar(255) NULL,
	tag varchar(255) NULL,
	collect_time timestamp NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	"source" int4 NULL,
	CONSTRAINT ecu_version_overview_pkey PRIMARY KEY (id)
);

----------- flash ------------

-------- ecu --------

CREATE TABLE public.d_ecu_version (
	id bigserial NOT NULL,
	vin_code varchar(255) NOT NULL,
	model varchar(255) NOT NULL,
	ecu_name varchar(255) NULL,
	version_name varchar(255) NULL,
	ti varchar(255) NULL,
	version_value varchar(255) NULL,
	source_workshop_id varchar(255) NULL,
	source_session_id varchar(255) NOT NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	CONSTRAINT d_ecu_version_pkey PRIMARY KEY (id)
);

CREATE TABLE public.ecu_version_history (
	id bigserial NOT NULL,
	vin varchar(255) NOT NULL,
	vehicle_version varchar(255) NULL,
	ecu_name varchar(255) NULL,
	version_name varchar(255) NULL,
	version_value varchar(255) NULL,
	ti varchar(255) NULL,
	session_id varchar(255) NULL,
	tag varchar(255) NULL,
	"source" int4 NULL,
	collect_time timestamp NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	CONSTRAINT ecu_version_history_pkey PRIMARY KEY (id)
);

CREATE TABLE public.ecu_version_overview (
	id bigserial NOT NULL,
	vin varchar(255) NOT NULL,
	ecu_name varchar(255) NULL,
	version_name varchar(255) NULL,
	version_value varchar(255) NULL,
	ti varchar(255) NULL,
	session_id varchar(255) NULL,
	tag varchar(255) NULL,
	collect_time timestamp NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	"source" int4 NULL,
	CONSTRAINT ecu_version_overview_pkey PRIMARY KEY (id)
);

-------- ecu --------

CREATE TABLE public.file_log (
	id bigserial NOT NULL,
	file_name varchar(512) NOT NULL,
	file_path varchar(512) NOT NULL,
	tag varchar(512) NULL,
	hash_file varchar(255) NULL,
	status int4 NOT NULL,
	note text NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now(),
	session_id varchar(100) NULL,
	file_type varchar NULL,
	CONSTRAINT schedule_file_log_pkey PRIMARY KEY (id)
);
CREATE INDEX idx_file_log_name ON public.file_log USING btree (file_name);
CREATE INDEX idx_file_log_tag ON public.file_log USING btree (tag);


CREATE TABLE public.d_dtc_daily (
	id bigserial NOT NULL,
	vin varchar(17) NOT NULL,
	vehicle_type_id int8 NOT NULL,
	ecu_name varchar NOT NULL,
	ti varchar NULL,
	dtc_code varchar NOT NULL,
	dtc_code_int varchar NULL,
	dtc_code_hex varchar NULL,
	description varchar NOT NULL,
	dtc_num int8 NOT NULL,
	dtc_day varchar NOT NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	CONSTRAINT d_dtc_daily_pkey PRIMARY KEY (id),
	CONSTRAINT d_dtc_daily_dtc_key UNIQUE (vin,ecu_name,dtc_code,dtc_day)
);

CREATE TABLE public.d_cts_daily (
	id bigserial NOT NULL,
	source varchar(3) NOT NULL,
	vin varchar(17) NULL,
	vehicle_type_id int8 NOT NULL,
	cts_name varchar NOT NULL,
	function_code varchar NOT NULL,
	action_code varchar NULL,
	cts_num int8 NOT NULL,
	cts_day varchar NOT NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL DEFAULT now(),
	CONSTRAINT d_cts_daily_pkey PRIMARY KEY (id),
	CONSTRAINT d_cts_daily_cts_key UNIQUE (source,vin,cts_name,cts_day)
);