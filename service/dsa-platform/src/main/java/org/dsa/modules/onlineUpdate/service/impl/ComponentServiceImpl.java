package org.dsa.modules.onlineUpdate.service.impl;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.ReUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.Constant;
import org.dsa.modules.onlineUpdate.dao.CompVGroupDao;
import org.dsa.modules.onlineUpdate.dao.ComponentDao;
import org.dsa.modules.onlineUpdate.dao.ComponentVersionDao;
import org.dsa.modules.onlineUpdate.entity.CompVGroupEntity;
import org.dsa.modules.onlineUpdate.entity.ComponentEntity;
import org.dsa.modules.onlineUpdate.service.ComponentService;
import org.dsa.modules.onlineUpdate.utils.PageUtils;
import org.dsa.modules.onlineUpdate.utils.Query;
import org.dsa.modules.onlineUpdate.vo.CascadeOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;


@Service("ComponentService")
public class ComponentServiceImpl extends ServiceImpl<ComponentDao, ComponentEntity> implements ComponentService {

    @Autowired
    private CompVGroupDao compVGroupDao;

    @Autowired
    private ComponentVersionDao componentVersionDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String name = MapUtil.getStr(params, "name").trim();
        Integer status = MapUtil.getInt(params, "status");
        IPage<ComponentEntity> page = this.page(
                new Query<ComponentEntity>().getPage(params),
                new QueryWrapper<ComponentEntity>()
                        .eq(ObjectUtil.isNotNull(status), "status", status)
                        .apply(StringUtils.isNotBlank(name),"lower(name) like CONCAT(CONCAT('%',{0},'%')) ",name.toLowerCase())
                        .orderByAsc("created_at")
        );
        //	private String isRelease;
        page.getRecords().forEach(componentEntity -> {
            componentEntity.setIsRelease(componentVersionDao.isRelease(componentEntity.getId()));
        });
        return new PageUtils(page);
    }

    @Override
    public void saveOrUpdate2(ComponentEntity entity) {
        checkParam(entity);
        if (ObjectUtil.isNull(entity.getId())) {
            //设置备注key name
//            String key ="cname_"+ entity.getName() + "code_" + entity.getCode();
//            entity.setDescriptionTi(key);
            this.baseMapper.insert(entity);
        } else {
            // 校验该组件是否被其他组件依赖
            List<CompVGroupEntity> groupEntities = compVGroupDao.selectList(new QueryWrapper<CompVGroupEntity>().eq("component_id", entity.getId()));
            // 被依赖，要禁用
            if (groupEntities.size() > 0 && Constant.STATUS.DISABLED.getCode().equals(entity.getStatus())) {
                throw new RRException(Constant.Msg.COMPONENT_DISABLED_ERROR);
            }
            this.baseMapper.updateById(entity);
        }
    }

    private void checkParam(ComponentEntity entity) {
        // name为空校验
        if (StrUtil.isBlank(entity.getName().trim())) {
            throw new RRException(Constant.Msg.CHECK_NAME_ERROR);
        }
        // status为空校验
        if (ObjectUtil.isNull(entity.getStatus())) {
            throw new RRException(Constant.Msg.CHECK_STATUS_ERROR);
        }
        // code为空校验
        if (ObjectUtil.isNull(entity.getCode())) {
            throw new RRException(Constant.Msg.CHECK_CODE_ERROR);
        }
        // code 范围校验

        boolean match = ReUtil.isMatch("^[1-9]\\d{2}$", String.valueOf(entity.getCode()));
        if (!match){
            throw new RRException(Constant.Msg.CHECK_CODE_ERROR);
        }
        // 名称重复性校验
        ComponentEntity component = this.baseMapper.selectOne(new QueryWrapper<ComponentEntity>()
                .eq("name", entity.getName()));
        if (component!=null && !component.getId().equals(entity.getId())) {
            throw new RRException(Constant.Msg.NAME_DUPLICATION_ERROR);
        }
        // code重复性校验
        ComponentEntity component2 = this.baseMapper.selectOne(new QueryWrapper<ComponentEntity>()
                .eq("code", entity.getCode()));
        if (component2!=null && !component2.getId().equals(entity.getId())) {
            throw new RRException(Constant.Msg.CODE_DUPLICATION_ERROR);
        }
        if (ObjectUtil.isNotNull(entity.getId())) {
            ComponentEntity componentEntity = this.baseMapper.selectById(entity.getId());
            if (componentEntity == null) {
                throw new RRException(Constant.Msg.DATA_INFO_NULL_ERROR);
            }
        }
    }

    @Override
    public void ableOrDisabledById(Long id) {
        if (ObjectUtil.isNull(id)) {
            throw new RRException(Constant.Msg.CHECK_ID_ERROR);
        }
        ComponentEntity componentEntity = this.baseMapper.selectById(id);
        if (ObjectUtil.isNull(componentEntity)) {
            throw new RRException(Constant.Msg.DATA_INFO_NULL_ERROR);
        }
        // 校验该组件是否被其他组件依赖
        List<CompVGroupEntity> groupEntities = compVGroupDao.selectList(new QueryWrapper<CompVGroupEntity>().eq("component_id", id));
        // 被依赖，且是启用状态
        if (groupEntities.size() > 0 && Constant.STATUS.NORMAL.getCode().equals(componentEntity.getStatus())) {
            throw new RRException(Constant.Msg.COMPONENT_DISABLED_ERROR);
        }
        componentEntity.setStatus(Objects.equals(componentEntity.getStatus(), 0) ? 1 : 0);
        this.baseMapper.updateById(componentEntity);
    }

    @Override
    public List<HashMap> queryAll() {
        return this.baseMapper.selectList(new QueryWrapper<ComponentEntity>().eq("status", 0)).stream().map(c -> new HashMap() {{
            put("label", c.getName());
            put("value", c.getId());
        }}).collect(Collectors.toList());
    }

    @Override
    public List<HashMap> queryUpdateTypeList() {
        return Arrays.stream(Constant.COMPONENT_UPDATE_TYPE.values()).map(c -> new HashMap() {{
            put("label", c.getContent());
            put("value", c.getCode());
        }}).collect(Collectors.toList());
    }

    @Override
    public List<CascadeOptions> getComponentOptions() {
        LambdaQueryWrapper<ComponentEntity> queryWrapper = Wrappers.<ComponentEntity>lambdaQuery()
                .select(ComponentEntity::getName,ComponentEntity::getId)
                .eq(ComponentEntity::getStatus, Constant.STATUS.NORMAL.getCode());
        List<CascadeOptions> options = this.baseMapper.selectList(queryWrapper)
                .stream()
                .map(o->CascadeOptions.builder().label(o.getName()).value(o.getId()).build())
                .collect(Collectors.toList());

       return options;
    }
}