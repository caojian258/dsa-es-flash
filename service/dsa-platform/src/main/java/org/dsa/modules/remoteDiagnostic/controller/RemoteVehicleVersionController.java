package org.dsa.modules.remoteDiagnostic.controller;

import lombok.extern.slf4j.Slf4j;
import org.dsa.common.annotation.SysLog;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.R;
import org.dsa.common.validator.ValidatorUtils;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.common.validator.group.UpdateGroup;
import org.dsa.modules.onlineUpdate.entity.SoftwareVersionEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleVersionEntity;
import org.dsa.modules.remoteDiagnostic.enums.RemoteVersionStatusEnum;
import org.dsa.modules.remoteDiagnostic.service.*;
import org.dsa.modules.remoteDiagnostic.vo.CheckNameReqVo;
import org.dsa.modules.remoteDiagnostic.vo.Vehicle.VehicleVersionIdentificationReqVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Slf4j
@RestController
@RequestMapping("/remoteVehicleVersion")
public class RemoteVehicleVersionController {

    @Autowired
    private RemoteVehicleEcuService vehicleEcuService;


    @Autowired
    private RemoteVehiclePoolService vehiclePoolService;

    @Autowired
    private VinCodeFileService vinCodeFileService;

    @Autowired
    private RemoteVehicleTagService vehicleTagService;

    @Autowired
    private RemoteEcuVersionService ecuVersionService;
    @Autowired
    private RemoteVehicleVersionService vehicleVersionService;
    @Autowired
    private RemoteEcuDependencyService remoteEcuDependencyService;

    @RequestMapping("/checkName")
    public R checkName(@RequestBody CheckNameReqVo vo) {

        ValidatorUtils.validateEntity(513, vo, AddGroup.class);

        return R.ok().put("flag", vehicleVersionService.check(vo));
    }

    @GetMapping("/getVehicleVersion/{id}")
    public R getVehicleVersion(@PathVariable("id") Long id) {
        if (id == null) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        return R.ok().put("data", vehicleVersionService.getVersions(id));
    }

    @RequestMapping("/getInfo/{id}")
    public R getInfo(@PathVariable("id") Long id) {
        if (id == null) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        return R.ok().put("data", vehicleVersionService.getInfo(id));
    }

    @GetMapping("/getVehicleVersionInfo/{id}")
    public R getVehicleVersionInfo(@PathVariable("id") Long id) {
        if (id == null) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        return R.ok().put("data", vehicleVersionService.getVehicleVersionInfo(id));
    }

    @RequestMapping("/getVehicleVersionList/{id}")
    public R getVehicleVersionList(@PathVariable("id") Long typeId) {
        if (typeId == null) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        return R.ok().put("data", vehicleVersionService.getVersions(typeId));
    }

    @RequestMapping("/getVehicleVersionList2/{id}")
    public R getVehicleVersionList2(@PathVariable("id") Long typeId) {
        if (typeId == null) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        List<RemoteVehicleVersionEntity> versions2 = vehicleVersionService.getVersions2(typeId);

        return R.ok().put("data", versions2 == null || versions2.isEmpty() ? null : versions2.get(0));
    }

    @SysLog("新增车辆版本")
    @PostMapping("/saveVehicleVersion")
    public R saveVehicleVersion(@RequestBody RemoteVehicleVersionEntity version) {

        ValidatorUtils.validateEntity(513, version, AddGroup.class);

        vehicleVersionService.saveVersion(version);
        return R.ok();
    }

    @SysLog("修改车辆版本")
    @PostMapping("/updateVehicleVersion")
    public R updateVehicleVersion(@RequestBody RemoteVehicleVersionEntity version) {
        ValidatorUtils.validateEntity(513, version, UpdateGroup.class);

        //开发->测试, 测试->开发,测试->释放,测试->拒绝,释放->拒绝
        RemoteVehicleVersionEntity info = vehicleVersionService.getInfo(version.getId());
        if (RemoteVersionStatusEnum.DEV.getValue().equals(info.getStatus()) && !RemoteVersionStatusEnum.DEV.getValue().equals(version.getStatus())) {
            if (!RemoteVersionStatusEnum.TEST.getValue().equals(version.getStatus())) {
                throw new RRException(Constant.Msg.VERSION_DEV_STATUS_ERROR);
            }
        }

        if (RemoteVersionStatusEnum.OPEN.getValue().equals(info.getStatus())) {
            if (!RemoteVersionStatusEnum.CLOSE.getValue().equals(version.getStatus()) && !RemoteVersionStatusEnum.OPEN.getValue().equals(version.getStatus())) {
                throw new RRException(Constant.Msg.VERSION_RELEASE_STATUS_ERROR);
            }
        }
        vehicleVersionService.updateVersion(version);
        return R.ok();
    }

    @RequestMapping("/getVehicleTypeInfo/{id}")
    public R getVehicleTypeInfo(@PathVariable("id") Long typeId) {
//        ValidatorUtils.validateEntity(version, UpdateGroup.class);
        if (typeId == null) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }

        return R.ok().put("data", vehicleVersionService.getVehicleTypeInfo(typeId));
    }

    @RequestMapping("/getVersionListByType")
    public R getVersionListByType(@RequestBody List<Long> ids) {
        if (ids == null || ids.size() == 0) {
            return R.ok();
        }
        return R.ok().put("data", vehicleVersionService.getVersionListByType(ids));
    }

    @RequestMapping("/getAllVehicleVersion")
    public R getAllVehicleVersion(Integer status) {
        return R.ok().put("data", vehicleVersionService.getAllVehicleVersion(status));
    }

    @PostMapping("/copyVersion/{id}/{type}")
    public R copyVersion(@PathVariable("id") Long id, @PathVariable("type") Long type) {
        try {
            vehicleVersionService.copyVersionById(id,type);
            return R.ok();
        } catch (Exception e) {
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    @PostMapping("/getIdentificationByVersionAndEcu")
    public R getIdentificationByVersionAndEcu(@RequestBody VehicleVersionIdentificationReqVo vo) {
        try {
            return R.ok().put("data", vehicleVersionService.getIdentificationByVersionAndEcu(vo));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    @RequestMapping("/getEcuDependencyByVehicleVersion/{id}")
    public R getEcuDependencyByVehicleVersion(@PathVariable("id") Long id) {
        try {
            return R.ok().put("data", remoteEcuDependencyService.getEcuDependencyByVehicleVersion(id));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }
}
