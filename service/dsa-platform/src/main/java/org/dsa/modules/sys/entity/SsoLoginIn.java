package org.dsa.modules.sys.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SsoLoginIn implements Serializable {

    private String username;
    private String password;
    private String captcha;
    private String uuid;
    private String token;


}
