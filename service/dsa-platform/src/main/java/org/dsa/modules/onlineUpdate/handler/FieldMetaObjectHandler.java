package org.dsa.modules.onlineUpdate.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.dsa.common.utils.ShiroUtils;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 公共字段，自动填充值
 */
@Component
public class FieldMetaObjectHandler implements MetaObjectHandler {
    private final static String CREATED_DATE = "createdAt";
    private final static String UPDATED_DATE = "updatedAt";
    private final static String CREATED_USER_ID = "createdUserId";
    private final static String UPDATED_USER_ID = "updatedUserId";

    @Override
    public MetaObjectHandler fillStrategy(MetaObject metaObject, String fieldName, Object fieldVal) {

        return MetaObjectHandler.super.fillStrategy(metaObject, fieldName, fieldVal);
    }

    @Override
    public void insertFill(MetaObject metaObject) {
        Date date = new Date();
        Long userId = ShiroUtils.getUserId();
        //创建时间
        strictInsertFill(metaObject, CREATED_DATE, Date.class, date);
        strictInsertFill(metaObject, CREATED_USER_ID, Long.class, userId);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        Long userId = ShiroUtils.getUserId();
        //更新时间
        strictUpdateFill(metaObject, UPDATED_DATE, Date.class, new Date());
        strictUpdateFill(metaObject, UPDATED_USER_ID, Long.class, userId);
    }


}

