package org.dsa.modules.onlineUpdate.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.dsa.modules.onlineUpdate.entity.RuleMatchedRecordEntity;

/**
 * 规则
 * 
 */
@Mapper
public interface RuleMatchedRecordDao extends BaseMapper<RuleMatchedRecordEntity> {

}
