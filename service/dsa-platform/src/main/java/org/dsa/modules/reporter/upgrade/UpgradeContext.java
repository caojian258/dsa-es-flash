package org.dsa.modules.reporter.upgrade;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Component
public class UpgradeContext {

    private final Map<String, UpgradeStrategy> strategyMap = new ConcurrentHashMap<>();

    @Autowired
    private ApplicationContext applicationContext;

    @PostConstruct
    private void init() {
        strategyMap.putAll(applicationContext.getBeansOfType(UpgradeStrategy.class));
    }

    public UpgradeStrategy getInstance(Integer code) {
        String beanName = UpgradeStrategyEnum.getByCode(code).getName();
        return this.getInstanceByBeanName(beanName);
    }

    private UpgradeStrategy getInstanceByBeanName(String beanName) {
        if (!StringUtils.isEmpty(beanName)) {
            return strategyMap.get(beanName);
        }
        return null;
    }

}
