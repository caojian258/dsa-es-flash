package org.dsa.modules.remoteDiagnostic.service;

import org.dsa.modules.remoteDiagnostic.entity.RemoteEcuIdentificationInfoEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleVersionEntity;
import org.dsa.modules.remoteDiagnostic.vo.CheckNameReqVo;
import org.dsa.modules.remoteDiagnostic.vo.Vehicle.VehicleVersionIdentificationReqVo;
import org.dsa.modules.vehicle.entity.DVehicleTypeEntity;

import java.util.List;
import java.util.Map;

public interface RemoteVehicleVersionService {


    RemoteVehicleVersionEntity getInfo(Long id);

    void saveVersion(RemoteVehicleVersionEntity version);

    void updateVersion(RemoteVehicleVersionEntity version);

    List<RemoteVehicleVersionEntity> getVersions();

    List<RemoteVehicleVersionEntity> getVersions(Long typeId);

    List<RemoteVehicleVersionEntity> getVersions2(Long typeId);

    Boolean check(CheckNameReqVo vo);

    DVehicleTypeEntity getVehicleTypeInfo(Long typeId);

    Object getVersionListByType(List<Long> ids);

    List<RemoteVehicleVersionEntity> getAllVehicleVersion(Integer status);

    RemoteVehicleVersionEntity getVehicleVersionInfo(Long id);

    void copyVersionById(Long id, Long type);

    Map<String, Object> getIdentificationByVersionAndEcu(VehicleVersionIdentificationReqVo vo);
}
