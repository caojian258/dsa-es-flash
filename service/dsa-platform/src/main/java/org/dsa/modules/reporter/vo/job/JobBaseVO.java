package org.dsa.modules.reporter.vo.job;

import lombok.Data;

import jakarta.validation.constraints.NotNull;

/**
* 定时任务 Base VO，提供给添加、修改、详细的子 VO 使用
* 如果子 VO 存在差异的字段，请不要添加到这里，影响 Swagger 文档生成
*/
@Data
public class JobBaseVO {

    @NotNull(message = "任务名称不能为空")
    private String jobName;

    @NotNull(message = "任务分组不能为空")
    private String jobGroup;

    @NotNull(message = "任务类型不能为空")
    private Boolean cronJob;

    private Long jobId;

    private String cronExpression;

    private Integer retryCount;

    private Integer retryInterval;

}
