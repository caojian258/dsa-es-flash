package org.dsa.modules.diagnostic.entity.mqtt.send;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.io.Serializable;

/**
 * mq body 实体类
 * 
 */
@Data
public class MqRequestBodyEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * ECU名称数组
	 */
	private String[] ecuName;
	/**
	 * 发送数据
	 */
	private Object data =new Object();

}
