package org.dsa.modules.oss.service;


import lombok.extern.slf4j.Slf4j;
import org.dsa.common.utils.SpringContextUtils;
import org.dsa.modules.oss.config.StorageConfig;
import org.dsa.modules.oss.constant.StorageConstant;
import org.dsa.modules.sys.service.SysConfigService;

@Slf4j
public final class DiskStorageFactory {

    private static SysConfigService sysConfigService;

    static {
        DiskStorageFactory.sysConfigService = (SysConfigService) SpringContextUtils.getBean("sysConfigService");
    }

    /***
     * 上传文件的根目录与目录格式
     * {"basePath":"/data","dirFormat":"yyyy/MM/dd","hasRecord":true,"type":"local","workDir":"/upload"}
     * @return
     */
    public static DiskStorageService build(){
        StorageConfig config = sysConfigService.getConfigObject(StorageConstant.LOCAL_CONFIG_KEY,  StorageConfig.class);
        if(null != config && config.getType().equals(StorageConstant.LOCAL)){
            return new LocalStoreService(config);
        }
        return null;
    }
}
