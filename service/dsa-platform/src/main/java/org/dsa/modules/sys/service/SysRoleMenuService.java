package org.dsa.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dsa.modules.sys.entity.SysMenuEntity;
import org.dsa.modules.sys.entity.SysRoleMenuEntity;

import java.util.List;



/**
 * 角色与菜单对应关系
 *
 */
public interface SysRoleMenuService extends IService<SysRoleMenuEntity> {
	
	void saveOrUpdate(Long roleId, List<Long> menuIdList);
	
	/**
	 * 根据角色ID，获取菜单ID列表
	 */
	List<Long> queryMenuIdList(Long roleId);

	/**
	 * 根据角色ID数组，批量删除
	 */
	int deleteBatch(Long[] roleIds);

	/**
	 * 根据角色ID，获取菜单ID列表
	 */
    List<Long> queryRoleMenu(List<String> roleList, Integer owningSystem);

	/**
	 * 根据角色ID，获取菜单列表
	 */
	List<SysMenuEntity> queryRoleMenuEntity(List<String> roleList,Integer owningSystem);

	/**
	 * 获取菜单信息列表
	 */
    List<SysMenuEntity> queryAllMenuEntity();

    List<String> queryMenuNameList(String rName);
}
