package org.dsa.modules.oss.config;

import lombok.Data;

import java.io.Serializable;

@Data
public class StorageConfig implements Serializable {

    /***
     * 类型 local remote
     */
    private String type;

    /***
     * 分类
     */
    private String category;


    /***
     * 备份主目录
     */
    private String basePath;


    /***
     * 备份子目录格式
     */
    private String dirFormat;

    /***
     * 工作目录
     */
    private String workDir;

    /***
     * 地址host
     */
    private String address;

    /***
     * 端口
     */
    private Integer port;

    private Boolean hasRecord;

    /***
     * 用户名密码
     */
    private String username;

    private String password;
}
