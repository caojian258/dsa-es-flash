package org.dsa.modules.reporter.vo.workshop;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.modules.reporter.pojo.PageParam;

import java.util.ArrayList;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class WorkshopPageReVo extends PageParam {

    private String id;

    private String name;

    private ArrayList<String> group;

    private String tag;

    private String state;

    private String country;
}
