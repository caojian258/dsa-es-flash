package org.dsa.modules.remoteDiagnostic.controller;


import cn.hutool.core.bean.BeanUtil;
import com.google.common.base.Joiner;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.annotation.SysLog;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.R;
import org.dsa.common.validator.ValidatorUtils;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.common.validator.group.UpdateGroup;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehiclePoolEntity;
import org.dsa.modules.remoteDiagnostic.service.RemoteVehiclePoolService;
import org.dsa.modules.remoteDiagnostic.service.RemoteVehicleTagService;
import org.dsa.modules.remoteDiagnostic.vo.CheckNameReqVo;
import org.dsa.modules.remoteDiagnostic.vo.CheckStatusReqVo;
import org.dsa.modules.remoteDiagnostic.vo.Vehicle.VehiclePoolReqVo;
import org.dsa.modules.remoteDiagnostic.vo.VehiclePoolPageReVo;
import org.dsa.modules.remoteDiagnostic.vo.VinCode.VinCodeReqPageVo;
import org.dsa.modules.remoteDiagnostic.vo.VinCode.VinCodeReqVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/remoteVehiclePool")
public class RemoteVehiclePoolController {

    @Autowired
    private RemoteVehicleTagService vehicleTagService;

    @Autowired
    private RemoteVehiclePoolService vehiclePoolService;


    @RequestMapping("/checkName")
    public R checkName(@RequestBody CheckNameReqVo vo) {

        ValidatorUtils.validateEntity(513, vo, AddGroup.class);

        return R.ok().put("flag", vehiclePoolService.check(vo));
    }

    @RequestMapping("/checkStatus")
    public R checkStatus(@RequestBody CheckStatusReqVo vo) {

        ValidatorUtils.validateEntity(513, vo, AddGroup.class);

        return R.ok().put("flag", vehiclePoolService.checkStatus(vo));
    }

    // 车辆池---------------------------------------------------
    @RequestMapping("/getPoolPage")
    public R getPoolPage(@RequestBody VehiclePoolPageReVo vo) {
        return R.ok().put("data", vehiclePoolService.selectPage(vo));
    }

    @GetMapping("/getPoolInfo/{id}")
    public R getPoolInfo(@PathVariable("id") Long id) {
        if (id == null) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        return R.ok().put("data", vehiclePoolService.getInfo(id));
    }

    @RequestMapping("/getPoolList")
    public R getPoolList() {
        return R.ok().put("data", vehiclePoolService.getList());
    }

    @SysLog("新增车辆池")
    @PostMapping("/savePool")
//    @RequiresPermissions("sys:user:save")
    public R savePool(@RequestBody RemoteVehiclePoolEntity pool) {

        ValidatorUtils.validateEntity(513, pool, AddGroup.class);

//        RemoteVehiclePoolEntity pool = new RemoteVehiclePoolEntity();

//        BeanUtil.copyProperties(poolReqVo, pool);

//        if (pool.getVehicleTypeIds() != null && pool.getVehicleTypeIds().size() > 0 && pool.getVehicleTypeIds().get(0) != -1) {
//            pool.setVehicleTypeId(Joiner.on(",").join(pool.getVehicleTypeIds()));
//        }

        vehiclePoolService.savePool(pool);
        return R.ok();
    }

    @SysLog("修改车辆池")
    @PostMapping("/updatePool")
//    @RequiresPermissions("sys:user:update")
    public R updatePool(@RequestBody RemoteVehiclePoolEntity pool) {

        ValidatorUtils.validateEntity(513, pool, UpdateGroup.class);

//        RemoteVehiclePoolEntity pool = new RemoteVehiclePoolEntity();

//        BeanUtil.copyProperties(poolReqVo, pool);

//        if (pool.getVehicleTypeIds() != null && pool.getVehicleTypeIds().size() > 0) {
//            if (pool.getVehicleTypeIds().get(0) == -1) {
//                pool.setVehicleTypeId("");
//            } else {
//                pool.setVehicleTypeId(Joiner.on(",").join(pool.getVehicleTypeIds()));
//            }
//        }
        vehiclePoolService.updatePool(pool);
        return R.ok();
    }

    // 3-27 弃用(暂定
    @RequestMapping("/getVinCode")
    public R getVinCode(@RequestBody VinCodeReqPageVo vo) {
        return R.ok().put("data", vehiclePoolService.getVinCode(vo));
    }

    // 3-27 弃用(暂定
    @PostMapping("/saveVin")
    public R saveVin(@RequestBody VinCodeReqVo vo) {
        if (vo.getId() == null || vo.getVin() == null || vo.getVin().size() == 0) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        vehiclePoolService.saveVin(vo);
        return R.ok();
    }

    // 3-27 弃用(暂定
    @PostMapping("/deleteVin")
    public R deleteVin(@RequestBody VinCodeReqVo vo) {
        if (vo.getId() == null || vo.getVin() == null || vo.getVin().size() == 0) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        vehiclePoolService.deleteVin(vo);
        return R.ok();
    }

    // 3-27 弃用(暂定
    @RequestMapping("/getPoolVinCode")
    public R getPoolVinCode(@RequestBody VinCodeReqPageVo vo) {
        return R.ok().put("data", vehiclePoolService.getPoolVinCode(vo));
    }

    // 3-27 弃用(暂定
    @PostMapping("/batchSaveVin")
    public R batchSaveVin(@RequestBody VinCodeReqPageVo vo) {
        if (vo.getId() == null) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        vehiclePoolService.batchSaveVin(vo);
        return R.ok();
    }

    @PostMapping("/getPoolVinCodePage")
    public R getPoolVinCodePage(@RequestBody VinCodeReqPageVo vo) {
//        if (vo.getId() == null) {
//            return R.error(Constant.Msg.CHECK_INPUT);
//        }
        return R.ok().put("data", vehiclePoolService.getPoolVinCodePage(vo));
    }

//    @PostMapping("/delPoolVinCode")
//    public R getPoolVinCode(@RequestBody VinCodeReqVo vo) {
//        return R.ok().put("data", vehiclePoolService.getPoolVinCode(vo));
//    }
}
