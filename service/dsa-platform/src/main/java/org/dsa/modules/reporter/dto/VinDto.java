package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class VinDto implements Serializable {
    /***
     * 车架号 17位
     */
    private String vinCode;

    /***
     * World Manufacturer Identifier 三位字符（字母和数字）
     */
    private String wmi;

    /***
     * Vehicle Descriptive Section 车辆说明部分(VDS)是车辆识别代号( VIN)的第二部分。由六位字母或数字组成。
     */
    private String vds;

}
