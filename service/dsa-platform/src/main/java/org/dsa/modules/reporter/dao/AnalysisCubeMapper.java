package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.entity.AnalysisCube;

import java.util.List;
import java.util.Map;

@Mapper
public interface AnalysisCubeMapper extends BaseMapper<AnalysisCube> {
    List<Map<String, Object>> getData(@Param("sql") String sql);

    AnalysisCube getOneCube(@Param("table") String table, @Param("dimension") String dimension, @Param("visible") String visible);

    AnalysisCube getCubeByCode(@Param("code") String table);

    /***
     * 根据月份进行分表
     * @param partitionTable
     * @param column
     * @param month
     * @param inheritTable
     */
    public void createPartitionTable(@Param("partitionTable") String partitionTable, @Param("column") String column, @Param("value") String month, @Param("inheritTable") String inheritTable);

    /***
     * 根据时间段进行分表
     * @param partitionTable
     * @param minDate
     * @param maxDate
     * @param inheritTable
     */
    public void createPartitionTable2(@Param("partitionTable") String partitionTable, @Param("minDate") String minDate, @Param("maxDate") String maxDate, @Param("inheritTable") String inheritTable);

    /***
     * 根据SQL创建表
     * @param sql
     */
    public void createPartitionTableSql(@Param("sql") String sql);



}
