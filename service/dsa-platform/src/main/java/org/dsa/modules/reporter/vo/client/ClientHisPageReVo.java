package org.dsa.modules.reporter.vo.client;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.modules.reporter.pojo.PageParam;

import java.util.List;


@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ClientHisPageReVo extends PageParam {

    private String pcid;

    private String workshop;

    private String username;

    List<String> taskTime;

    /***
     * 操作类型
     */
    private Integer statusType;

    /***
     * 状态
     */
    private Integer status;

    /***
     * 应用类型
     */
    private String appType;
}
