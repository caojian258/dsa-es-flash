package org.dsa.modules.diagnostic.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 故障码统计 - 柱状图
 * 
 */
@Data
public class AnalysisDtcBarEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 故障码
	 */
	private String dtcCodeHex;

	/**
	 * 故障月份
	 */
	private String dtcMonth;

	/**
	 * 故障数
	 */
	private int dtcCount;
}
