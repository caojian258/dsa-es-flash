package org.dsa.modules.reporter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
@TableName("vehicle_flash_record")
public class VehicleFlashRecord extends BaseEntity{
    @TableId(type = IdType.AUTO)
    private Long id;

    private Integer mode;
    private String version;
    private String vin;
    private String workshop;
    private String pcid;
    private String model;
    private String prevVersion;
    private String vehicleVersion;
    private String username;
    private String sessionId;
    private Long spendTime;
    private String feature;
    private Date startTime;
    private Date endTime;
    private Integer result;
    private String tag;
    private String reason;

    private Date collectTime;

    @TableField(exist = false)
    private Map<String,Object> results;

    @TableField(exist = false)
    private List<Map<String,Object>> ecus;
}
