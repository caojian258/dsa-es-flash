package org.dsa.modules.remoteDiagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskDetailEntity;

import java.util.List;

@Mapper
public interface DiagnosticTaskDetailDao extends BaseMapper<DiagnosticTaskDetailEntity> {


    @Select("select * from r_diag_task_detail i where i.campaign_id = #{taskId} ")
    public List<DiagnosticTaskDetailEntity> getInfoByTask(@Param("taskId") Long taskId);

    @Select("select * from r_diag_task_detail i where i.session_id = #{sessionId} order by i.id desc")
    public List<DiagnosticTaskDetailEntity> getListBySession(@Param("sessionId") String sessionId);

    @Select("select * from r_diag_task_detail i where i.session_id = #{sessionId} order by i.id desc limit 1")
    public DiagnosticTaskDetailEntity getOneBySession(@Param("sessionId") String sessionId);

    @Insert("<script>"
            + " insert into r_diag_task_detail (campaign_id,session_id,vin,code,message,type) values "
            + " <foreach collection='list' item='item' separator=','> "
            + " (#{item.campaignId},#{item.sessionId},#{item.vin},#{item.code},#{item.message},#{item.type}) "
            + " </foreach>"
            + " </script>")
    void inserts(@Param("list") List<DiagnosticTaskDetailEntity> details);

    @Select("select * from r_diag_task_detail i where i.session_id = #{sessionId} and i.vin = #{vin} and i.type = #{type} and i.code = '0' limit 1")
    DiagnosticTaskDetailEntity getOneByMatch(@Param("type") String type,@Param("vin") String vin,@Param("sessionId") String sessionId);

}
