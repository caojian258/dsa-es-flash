package org.dsa.modules.onlineUpdate.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dsa.modules.onlineUpdate.entity.VersionRecordEntity;

/**
 * 规则
 *
 */
public interface VersionRecordService extends IService<VersionRecordEntity> {
}

