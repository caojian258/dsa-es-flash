package org.dsa.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.sys.dto.SysActiveUserDto;
import org.dsa.modules.sys.entity.SysUserTokenEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统用户Token
 *
 */
@Mapper
public interface SysUserTokenDao extends BaseMapper<SysUserTokenEntity> {

    SysUserTokenEntity queryByToken(String token);

    Page<SysActiveUserDto> activeUserPage(Page<SysActiveUserDto> page, @Param("workshopName") String workshopName, @Param("userName") String userName);
}
