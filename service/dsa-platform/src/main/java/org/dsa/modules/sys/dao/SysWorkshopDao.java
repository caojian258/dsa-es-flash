package org.dsa.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.dsa.modules.sys.entity.SysWorkshopEntity;

import java.util.List;
import java.util.Map;

/**
 * 部门管理
 *
 */
@Mapper
public interface SysWorkshopDao extends BaseMapper<SysWorkshopEntity> {

    List<SysWorkshopEntity> queryList(Map<String, Object> params);

    /**
     * 查询子部门ID列表
     * @param parentId  上级部门ID
     */
    List<Long> queryDetpIdList(Long parentId);

    /**
     * 通过部门id查询部门名
     * @param workshopId
     * @return
     */
    String queryWorkshopNameById(Long workshopId);

    SysWorkshopEntity getSysWorkshopEntity(Long workshopId);

    int querySysWorkshopEntityCount();

    void removeNotInIds(List<Long> organList);

    List<String> queryListString();

    void saveOrg(SysWorkshopEntity sysWorkshopEntity);

    String saveWorkshop(SysWorkshopEntity workshop);

    void disableById(long workshopId);
}
