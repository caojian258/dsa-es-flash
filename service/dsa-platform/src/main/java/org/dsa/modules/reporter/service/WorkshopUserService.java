package org.dsa.modules.reporter.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.reporter.entity.WorkshopUser;
import org.dsa.modules.reporter.entity.WorkshopUserGroup;
import org.dsa.modules.reporter.vo.client.ClientChartResVo;
import org.dsa.modules.reporter.vo.workshop.WorkshopUserPageReVo;
import org.dsa.modules.reporter.vo.workshopUser.ActiveUserVo;
import org.dsa.modules.reporter.vo.workshopUser.UserChartReVo;
import org.dsa.modules.reporter.vo.workshopUser.UserGroupChartReVo;
import org.dsa.modules.sys.dto.SysActiveUserDto;

import java.util.Date;
import java.util.List;

public interface WorkshopUserService {
    public Page<WorkshopUser> selectPage(WorkshopUserPageReVo vo);

    public List<WorkshopUserGroup> getGroup();

    public ClientChartResVo userGroups(UserGroupChartReVo vo);

    public ClientChartResVo workshopGroup(UserChartReVo vo);

    public void updateLoginTime(String username, Date loginTime);

    public Page<SysActiveUserDto> activeUserPage(ActiveUserVo vo);
}
