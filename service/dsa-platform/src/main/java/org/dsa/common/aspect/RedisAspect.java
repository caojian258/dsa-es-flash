package org.dsa.common.aspect;

import org.dsa.common.exception.RRException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.dsa.common.utils.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Redis切面处理类
 *@Aspect
 * @Configuration
 */

public class RedisAspect {
    private Logger logger = LoggerFactory.getLogger(getClass());
    //是否开启redis缓存  true开启   false关闭
    /*@Value("${spring.redis.open: false}")*/
    private boolean open;

    /*@Around("execution(* org.dsa.common.utils.RedisUtils.*(..))")*/
    public Object around(ProceedingJoinPoint point) throws Throwable {
        Object result = null;
        if(open){
            try{
                result = point.proceed();
            }catch (Exception e){
                logger.error("redis error", e);
                throw new RRException(Constant.Msg.SERVER_REDIS_FAIL);
            }
        }
        return result;
    }
}
