package org.dsa.modules.diagnostic.entity.mqtt.callback;

import lombok.Data;

import java.io.Serializable;
@Data
public class MqBodyDataDTCDataGridVO implements Serializable {

     private static final long serialVersionUID = 1L;
    /**
     *Ecu 名称
     */
    private String ecuName;
    /**
     * 故障码code
     */
    private String dtcCode;
    /**
     * 故障码code
     */
    private String dtcCode_Int;
    /**
     * 故障码code
     */
    private String dtcCodeHex;
    /**
     * 冻结帧名称
     */
    private String name;
    /**
     * 冻结帧名称 ti
     */
    private String ti;
    /**
     * 冻结帧值
     */
     private String value;
    /**
     * 冻结帧值ti
     */
    private String tiValue;
    /**
     * 冻结帧你单位
     */
     private String unit;

}
