package org.dsa.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.sys.entity.SysWorkshopsEntity;

import java.util.List;


@Mapper
public interface SysWorkshopsDao extends BaseMapper<SysWorkshopsEntity> {

    String queryWorkshopNameById(Long id);

    SysWorkshopsEntity findById(Long id);

    SysWorkshopsEntity findByName(String name);

    void disabledById(Long id);

    List<SysWorkshopsEntity> selectEnableWorkshops(@Param("country") Long country, @Param("province") Long province, @Param("city") Long city, @Param("region") String region, @Param("name") String name, @Param("status") Integer status);

    SysWorkshopsEntity findByCode(@Param("code") String code);

    List<String> queryRegion();

    List<Long> selectWorkshopByLocation(@Param("countryName") String countryName, @Param("region") String region,
                                        @Param("provinceName") String provinceName, @Param("cityName") String cityName);
}
