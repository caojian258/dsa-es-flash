package org.dsa.modules.onlineUpdate.controller;

import lombok.extern.slf4j.Slf4j;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.PageUtils;
import org.dsa.common.utils.R;
import org.dsa.modules.onlineUpdate.dto.RuleDto;
import org.dsa.modules.onlineUpdate.entity.RuleEntity;
import org.dsa.modules.onlineUpdate.rule.RuleComponent;
import org.dsa.modules.onlineUpdate.service.RuleService;
import org.dsa.modules.sys.entity.SysUserEntity;
import org.dsa.modules.sys.service.SysUserService;
import org.dsa.modules.sys.service.SysWorkshopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import jakarta.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * 规则
 */
@Slf4j
@RestController
@RequestMapping("/rule")
public class RuleController {
    @Autowired
    private RuleService ruleService;

    @Autowired
    private RuleComponent ruleComponent;

    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private SysWorkshopService sysWorkshopService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestBody Map<String, Object> params) {
        try {
            PageUtils page = ruleService.queryPage(params);
            return R.ok().put("page", page);
        } catch (RRException rrException) {
            log.error("规则列表 error: " + rrException.getMessage());
            return R.error(rrException.getCode(), "查询失败： " + rrException.getMsg());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("规则列表 error: " + e.getMessage());
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Integer id) {
        try {
            RuleEntity rule = ruleService.getById(id);
            return R.ok().put("rule", rule);
        } catch (RRException rrException) {
            log.error("规则详情 error: " + rrException.getMessage());
            return R.error(rrException.getCode(), "查询失败： " + rrException.getMsg());
        } catch (Exception e) {
            log.error("规则详情 error: " + e.getMessage());
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody RuleDto rule) {
        try {
            ruleService.saveOrUpdate(rule);
            return R.ok();
        } catch (RRException rrException) {
            log.error("规则保存 error: " + rrException.getMessage());
            return R.error(rrException.getCode(), "保存失败： " + rrException.getMsg());
        } catch (Exception e) {
            log.error("规则保存 error: " + e.getMessage());
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody RuleDto rule) {
        try {
            ruleService.saveOrUpdate(rule);
            return R.ok();
        } catch (RRException rrException) {
            log.error("规则修改 error: " + rrException.getMessage());
            return R.error(rrException.getCode(), "保存失败： " + rrException.getMsg());
        } catch (Exception e) {
            log.error("规则修改 error: " + e.getMessage());
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Integer[] ids) {
        try {
            ruleService.removeByIds(Arrays.asList(ids));
            return R.ok();
        } catch (RRException rrException) {
            log.error("规则修改 error: " + rrException.getMessage());
            return R.error(rrException.getCode(), "删除失败： " + rrException.getMsg());
        } catch (Exception e) {
            log.error("规则修改 error: " + e.getMessage());
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }
    /**
     * 规则禁用
     */
    @RequestMapping("/disabled")
    public R disabled(@RequestParam("id") Long id) {
        try {
            ruleService.disabled(id);
            return R.ok();
        } catch (RRException rrException) {
            log.error("规则禁用 error: " + rrException.getMessage());
            return R.error(rrException.getCode(), "禁用失败： " + rrException.getMsg());
        } catch (Exception e) {
            log.error("禁用修改 error: " + e.getMessage());
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }
    /**
     * 规则匹配
     */
    @RequestMapping("/match")
    public R match(@RequestBody Map<String,Object> param, HttpServletRequest request){
        try {
            param.put("token", request.getHeader("token"));
            return R.ok().put("data",ruleComponent.match(param));
        } catch (RRException rrException) {
            log.error("规则匹配 error: " + rrException.getMessage());
            return R.error(rrException.getCode(), "匹配失败： " + rrException.getMsg());
        } catch (Exception e) {
            log.error("规则匹配 error: " + e.getMessage());
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    /**
     * 查询用户
     */
    @RequestMapping("/queryUserList")
    public R queryUserList(@RequestBody Map<String,Object> params){
        try {
            return R.ok().put("users", sysUserService.queryUserList(params));
        }catch (Exception e) {
            log.error("规则匹配 error: " + e.getMessage());
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    /**
     * 查询用户
     */
    @RequestMapping("/querySelectedUserList")
    public R querySelectedUserList(@RequestBody Map<String,Object> params){
        try {
            List<SysUserEntity> list = sysUserService.querySelectedPage(params);
            return R.ok().put("list", list);
        }catch (Exception e) {
            log.error("查询用户 error: " + e.getMessage());
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

}
