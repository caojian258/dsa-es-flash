package org.dsa.modules.reporter.vo.doc;


import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.dsa.modules.reporter.document.ElasticEntity;

import java.util.Date;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper=false)
public class DtcDocVo extends ElasticEntity {

    private String sessionInfoId;

    private Integer recordFlag;

    private String sessionId;

    private String vin;

    private String diagSource;

    private Integer diagCategory;

    private String ecuName;

    private String workshopName;

    private String dtcCode;

    private String dtcCodeHex;

    private Integer dtcCodeInt;

    private String description;

    private String descriptionTi;

    private String statusByteHex;

    private String status;

    private String statusTi;

    private String nrcHex;

    private String nrcDescription;

    private String nrcDescriptionTi;

    private Integer snapshotEnable;

    private Integer extendedDataEnable;

    private String errorCode;

    private String errorMessage;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date lastUpdateTime;

    private List<DtcSnapshotDocVo> dtcSnapshotList;

    private List<DtcExtendDataDocVo> dtcExtendDataList;
}
