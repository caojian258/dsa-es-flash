package org.dsa.modules.reporter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
@TableName("client_application_version")
public class AppVersion extends BaseEntity{

    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 经销商ID
     */
    private String workshop;

    /***
     * 电脑ID
     */
    private String pcid;

    /***
     * 软件类型 smartstart jre application
     */
    private String type;

    /***
     * 应用名称
     */
    private String name;

    /***
     * 应用版本
     */
    private String version;

    /***
     * 信息收集日期
     */
    private String collectDate;

    /***
     * 信息收集时间
     */
    private Date collectTime;


    /***
     * 批处理
     */
    private String tag;

    private Integer appId;
}
