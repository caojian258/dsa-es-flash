package org.dsa.modules.remoteDiagnostic.vo.Flash;

import lombok.Data;

import java.io.Serializable;

@Data
public class FlashVehicleCheckVo implements Serializable {

    private String vin;

    private Long vehicleVersionId;

}
