package org.dsa.modules.remoteDiagnostic.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EcuIdentificationDto {

    private String did;

    private String didVal;

    private String didName;

    private String description;

}
