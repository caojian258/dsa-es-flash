package org.dsa.modules.remoteDiagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;
import org.dsa.modules.remoteDiagnostic.pojo.BaseEntity;

import java.io.Serializable;

@Data
@ToString
@TableName("r_diag_task_active")
public class DiagnosticTaskActiveEntity extends BaseEntity implements Serializable {

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long campaignId;

    private String sessionId;

    private String vin;

    private Integer status;

    private String mes;

    public DiagnosticTaskActiveEntity() {
    }

    public DiagnosticTaskActiveEntity(Long taskId, String sessionId, String vin) {
        this.campaignId = taskId;
        this.sessionId = sessionId;
        this.vin = vin;
    }
}
