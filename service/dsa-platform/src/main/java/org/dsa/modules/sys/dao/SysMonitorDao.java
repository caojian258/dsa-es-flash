package org.dsa.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.dsa.modules.sys.vo.monitor.MonitorVo;

import java.util.List;
import java.util.Map;

@Mapper
public interface SysMonitorDao extends BaseMapper<Object> {

    List<Map<String, String>> countTable(MonitorVo monitor);
}
