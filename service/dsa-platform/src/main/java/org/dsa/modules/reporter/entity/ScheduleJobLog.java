package org.dsa.modules.reporter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("schedule_job_log")
public class ScheduleJobLog implements Serializable {

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long jobId;

    private String beanName;

    private String params;

    private String status;

    private String error;

    private Integer times;

    private Date createTime;
}
