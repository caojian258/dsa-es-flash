package org.dsa.modules.reporter.event;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.RedisUtils;
import org.dsa.common.utils.ShellUtil;
import org.dsa.config.SyncConfig;
import org.dsa.modules.diagnostic.service.SessionLogService;
import org.dsa.modules.reporter.config.ReportConfig;
import org.dsa.modules.reporter.constant.RedisConstant;
import org.dsa.modules.reporter.dto.*;
import org.dsa.modules.reporter.entity.FileLog;
import org.dsa.modules.reporter.enums.FileCateEnum;
import org.dsa.modules.reporter.handler.FileHandler;
import org.dsa.modules.reporter.handler.SessionHandler;
import org.dsa.modules.reporter.handler.SessionLogHandler;
import org.dsa.modules.reporter.service.FileLogService;
import org.dsa.modules.reporter.util.FilesUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;
import org.springframework.util.StringUtils;

import jakarta.annotation.Resource;
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.nio.file.StandardCopyOption.ATOMIC_MOVE;

/***
 * 解压文件 解析文件
 */
@Slf4j
@Component
public class SessionListener implements ApplicationListener<SessionEvent> {

    @Autowired
    ReportConfig reportConfig;

    @Autowired
    SyncConfig syncConfig;

    @Autowired
    RedisUtils redisUtils;

    @Autowired
    SessionLogHandler sessionLogHandler;

    @Autowired
    FileHandler fileHandler;

    @Autowired
    FileLogService logService;

    @Override
    public void onApplicationEvent(SessionEvent sessionEvent) {
        log.info("Session Listener Start................");

        SessionFileDto fileDto = new SessionFileDto();
        String path = sessionEvent.getPath();
        String name = sessionEvent.getName();
        String tag = sessionEvent.getTag();

        String key = RedisConstant.FILE_KEY+path;

        String flag = redisUtils.get(key);
        if(!StringUtils.isEmpty(flag)){
            log.info("Session Listener file {} by the {} thread is processing", path, Thread.currentThread().getName());
            return;
        }

        SimpleDateFormat sdf = new SimpleDateFormat(syncConfig.getDirFormat());
        String targetFileDir = syncConfig.getSessionWorkDir() + File.separator+ sdf.format(new Date());
        String archiveDir = syncConfig.getSessionArchiveDir() + File.separator+ sdf.format(new Date());
        String faultDir = syncConfig.getSessionFaultDir() + File.separator+ sdf.format(new Date());

        redisUtils.set(key, 1, 180L);


        String fileName = logService.pathToFileName(path);

        log.info("Session Listener file name: {}", fileName);

        Boolean successFlag = logService.checkSuccessed( FileCateEnum.EVENT.getValue(), fileName);
        log.info("Session Listener file {} : {}", fileName, successFlag);
        if(successFlag){
            moveFile(path, faultDir);
//            logService.failedLog(FileCateEnum.EVENT.getValue(), getZipPath(name, faultDir), tag, fileName + " has been processed successfully.");
            redisUtils.delete(key);
            return;
        }

        //1. 解压文件 找到目标文件
        try {
            fileDto = fileHandler.decompressSwtsSession(path, targetFileDir);

            logService.succeedLog(FileCateEnum.EVENT.getValue(), getZipPath(name, archiveDir), tag,
                    fileDto.getFileMD5(), fileName);

            log.info("Session Listener {} file info: {}", fileName, JSONObject.toJSONString(fileDto));

        } catch (Exception e) {
            log.error("Session Listener decompressSession error: {}", JSONObject.toJSONString(e));
            moveFile(path, faultDir);
            logService.failedLog(FileCateEnum.EVENT.getValue(), getZipPath(name, faultDir), tag, e.getMessage());
            redisUtils.delete(key);
            return;
        }

        if(null == fileDto){
            moveFile(path, faultDir);
            logService.failedLog(FileCateEnum.EVENT.getValue(), getZipPath(name, faultDir), tag, "fileDto is null");
            redisUtils.delete(key);
            return;
        }

        if(null == fileDto.getVinCode() || fileDto.getVinCode().length() == 0){
            moveFile(path, faultDir);
            logService.failedLog(FileCateEnum.EVENT.getValue(), getZipPath(name, faultDir), tag, "vinCode is null");
            redisUtils.delete(key);
            return;
        }

        if("UNDEFINED".equals(fileDto.getVinCode())){
            moveFile(path, faultDir);
            logService.failedLog(FileCateEnum.EVENT.getValue(), getZipPath(name, faultDir), tag, "vinCode is UNDEFINED");
            redisUtils.delete(key);
            return;
        }

        if(StringUtils.isEmpty(fileDto.getSqlitePath())){
            moveFile(path, faultDir);
            String note = "sqlite file not exist";
            log.error("Session Listener decompressSession error: {}", note);
            logService.failedLog(FileCateEnum.EVENT.getValue(), getZipPath(name, faultDir), tag, note);
            redisUtils.delete(key);
            return;
        }

        //2. 解密sqlite文件
        SessionDto sessionDto = null;
        ShellUtil shellUtil = null;

        String basePath = fileDto.getSessionDir();
        String cipherPath = fileDto.getSqlitePath();
        String sqlitePath = cipherPath.replace("slog", "sqlite");

        String dbFileName = fileDto.getFileName().replace("slog", "sqlite");
        List<String> cmdList = new ArrayList<>();

        if(ShellUtil.isWindows()) {
            cmdList.add(reportConfig.getSqlcipherWindowsScript());
            cmdList.add(cipherPath);
            cmdList.add(sqlitePath);
            log.info("path: {} ", JSONObject.toJSONString(cmdList));

            shellUtil = new ShellUtil(basePath, cmdList);
            shellUtil.start();
            log.info("sqlite sqlcipher result:{}", JSONObject.toJSONString(shellUtil.getMessages()));

        }else{
            cmdList.add(reportConfig.getSqlcipherLinuxScript());
            cmdList.add(cipherPath);
            cmdList.add(sqlitePath);
            shellUtil = new ShellUtil(basePath, cmdList);
            shellUtil.start();
            log.info("sqlite sqlcipher linux result:{}", JSONObject.toJSONString(shellUtil.getMessages()));
        }

        //3. 解析sqlite文件
        try {
            log.info("sqlitePath:{}", sqlitePath);
            log.info("dbFileName:{}", dbFileName);

            SessionDto result = sessionLogHandler.saveData(sqlitePath, tag,  dbFileName, fileDto.getPcid());

            logService.insertSessionLog(FileCateEnum.SQLITE.getValue(), getZipPath(name, archiveDir), tag,
                    fileDto.getSessionId(), 1, result.getMsg());

        }catch (Exception ex){
            moveFile(path, faultDir);
            log.error("Session Listener sqlite error: {}", JSONObject.toJSONString(ex));
            logService.insertSessionLog(FileCateEnum.SQLITE.getValue(), getZipPath(name, faultDir), tag,
                    fileDto.getSessionId(), 0, ex.getMessage());
        }


        //4. 归档文件 移动的都是zip文件
        moveFile(path, archiveDir);

        //5. 从工作目录删除当前文件夹
        //delFileFormWorkDir(fileDto.getPath());

        redisUtils.delete(key);
        redisUtils.lSet(RedisConstant.PROCESSED_SESSION_KEY, fileDto.getVinCode()+":"+fileDto.getSessionId());
        redisUtils.lSet(RedisConstant.TAG_BASE_KEY, tag);
        redisUtils.lSet(RedisConstant.TAG_DIAG_KEY, tag);
    }

    protected void moveFile(String path, String dir){
        try {
            FilesUtil.mkdir(new File(dir));
            File sessionLogFile = new File(path);
            String archiveFile = dir+File.separator+ sessionLogFile.getName();
            Files.move(Paths.get(sessionLogFile.getPath()), Paths.get(archiveFile), ATOMIC_MOVE);
        }catch (Exception exception){
            log.error("Session listener move file {} error: {}", path, exception.getMessage());
        }
    }

    protected void delFileFormWorkDir(String dir){
        File file = new File(dir);
        if(file.exists()){
            try {
                FileUtils.deleteDirectory(file);
            }catch (Exception ex){
                log.error("Session listener delFileFormWorkDir error: {}", ex.getMessage());
            }
        }
    }

    protected String getZipPath(String name, String dir){
        return dir+File.separator+name;
    }
}
