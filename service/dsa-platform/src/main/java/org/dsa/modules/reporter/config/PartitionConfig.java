package org.dsa.modules.reporter.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Data
@Configuration
@ConfigurationProperties(prefix = "report.partition")
public class PartitionConfig {

    private Map<String, String> raw;

    private Map<String, String> diag;

    private Map<String, String> client;
}
