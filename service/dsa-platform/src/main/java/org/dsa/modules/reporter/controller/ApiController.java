package org.dsa.modules.reporter.controller;

import cn.hutool.core.util.StrUtil;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.apache.shiro.SecurityUtils;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.R;
import org.dsa.common.utils.RedisUtils;
import org.dsa.common.utils.ShiroUtils;
import org.dsa.modules.reporter.constant.RedisConstant;
import org.dsa.modules.reporter.entity.SystemHistory;
import org.dsa.modules.reporter.service.FlashLogService;
import org.dsa.modules.reporter.service.SystemService;
import org.dsa.modules.reporter.util.FilesUtil;
import org.dsa.modules.reporter.vo.api.ClientInfoReVo;
import org.dsa.modules.reporter.vo.api.FlashLogReVo;
import org.dsa.modules.reporter.vo.api.SyncFlashLogRevo;
import org.dsa.modules.reporter.vo.api.UpgradeLogReVo;
import org.dsa.modules.sys.entity.DisplayUserEntity;
import org.dsa.modules.sys.entity.SysUserEntity;
import org.dsa.modules.sys.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import jakarta.annotation.Resource;
import java.util.*;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
public class ApiController {

    @Resource
    FlashLogService flashLogService;

    @Autowired
    SystemService systemService;

    @Resource
    RedisUtils redisUtils;

    @Resource
    SysUserService sysUserService;

    /***
     * 收集客户端信息
     * @param clientInfo
     * @return
     */
    @RequestMapping(value = "/clientInfo", method = {RequestMethod.POST})
    public R clientInfo(@RequestBody ClientInfoReVo clientInfo){
        try {
            String  tag = FilesUtil.makeTag();

            clientInfo.setUsername(ShiroUtils.getUserName());
            clientInfo.setWorkshop(ShiroUtils.getWorkShop());

            if(StrUtil.isBlank(clientInfo.getWorkshop())){
                clientInfo.setWorkshop("");
            }

            if(StrUtil.isBlank(clientInfo.getUsername())){
                clientInfo.setUsername("");
            }

            if(StrUtil.isNotBlank(clientInfo.getUsername())){
                SysUserEntity sysUserEntity = sysUserService.queryByUserName(clientInfo.getUsername());
                if(null != sysUserEntity){
                    clientInfo.setWorkshopId(sysUserEntity.getWorkshopId());
                }
            }

            if(StrUtil.isNotBlank(clientInfo.getSystemLocale())){
                if(clientInfo.getSystemLocale().startsWith("{")  && clientInfo.getSystemLocale().endsWith("}")){
                    String[] systemLocale = clientInfo.getSystemLocale().replace("{", "").replace("}", "").split(",");
                    if(systemLocale.length>0 && StrUtil.isNotBlank(systemLocale[0])){
                        clientInfo.setSystemLocale(systemLocale[0].replace("\"",""));
                    }
                }
            }

            systemService.addClientInfo(clientInfo, tag);
            SystemHistory item = systemService.getOneHistory(tag);
            if(null != item){
                systemService.saveOverview(item);
            }
        }catch (Exception ex){
            log.error("Api clientInfo Error: {}", ex.getMessage());
            return  R.error(500,ex.getMessage());
        }

        return R.ok();
    }

    @RequestMapping(value = "/upgradeLog", method = {RequestMethod.POST})
    public R upgradeLog(@Valid @RequestBody UpgradeLogReVo vo){

        try {
            String  tag = FilesUtil.makeTag();
            systemService.addUpgradeLog(vo, tag);
            redisUtils.lSet(RedisConstant.TAG_BASE_KEY, tag);
        }catch (Exception ex){
         log.error("Api upgradeLog Error: {}", ex.getMessage());
         return  R.error(500,ex.getMessage());
        }
        return R.ok();
    }

    @RequestMapping(value = "/flashLog", method = {RequestMethod.POST })
    public R flashLog(@Valid @RequestBody FlashLogReVo vo){

        if(null != redisUtils.get(vo.getSign())){
            return R.error(302,"to many requests");
        }

        redisUtils.set(vo.getSign(), vo.getSessionId(),10);

        Boolean flag = flashLogService.validateSign(vo);
        if(!flag){
            return R.error(301,"sign validate failed");
        }

        try {
            SyncFlashLogRevo syncVo = flashLogService.formatSyncData(vo);
            flashLogService.syncData(syncVo);
        }catch (Exception ex){
            log.error(ex.getMessage());
        }

        flashLogService.collectData(vo);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 200);
        map.put("msg", "ok");
        return R.ok(map);
    }

}
