package org.dsa.modules.remoteDiagnostic.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "remote")
public class RemoteDiagnosticConfig {

    // 文件路径或文件夹路径 -----------------------------------------------------------------------------
    @Value("${remote.dir.otx}")
    private String otx;

    @Value("${remote.dir.ecuVersion}")
    private String ecuVersion;

    @Value("${remote.dir.tag}")
    private String tag;

    @Value("${remote.dir.taskLog}")
    private String taskLog;

    @Value("${remote.dir.ecuImage}")
    private String ecuImage;

    @Value("${remote.dir.template}")
    private String template;

    @Value("${remote.dir.vehicle}")
    private String vehicle;
    // 文件名或文件夹名 -----------------------------------------------------------------------------
    @Value("${remote.file.flashFileName}")
    private String flashFileName;

    @Value("${remote.file.vehicleFileName}")
    private String vehicleInfo;
    // 车辆池参数 -----------------------------------------------------------------------------
    @Value("${remote.config.pool.salesArea}")
    private String salesArea;

    @Value("${remote.config.pool.placeOfProduction}")
    private String placeOfProduction;

    @Value("${remote.config.pool.productionDate}")
    private String productionDate;

    // ECU 罐装证书
    @Value("${remote.cert.ecuFillingCertificateUrl}")
    private String ecuFillingCertificateUrl;
}
