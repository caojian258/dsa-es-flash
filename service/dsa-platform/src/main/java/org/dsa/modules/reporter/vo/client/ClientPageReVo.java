package org.dsa.modules.reporter.vo.client;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.modules.reporter.pojo.PageParam;

import java.util.List;


@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ClientPageReVo extends PageParam {

    private String pcid;

    private String workshop;

    private String smartstartVersion;

    private String username;

    // 系统语言
    private String systemLocale;
    // 应用语言
    private String applicationLocale;
    // 杀毒软件
    private String antivir;
    // 操作系统
    private String windowsVersion;

    private List<String> group;


}
