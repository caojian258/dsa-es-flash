package org.dsa.modules.reporter.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.dsa.modules.reporter.dao.AppHistoryMapper;
import org.dsa.modules.reporter.dao.AppStatsMapper;
import org.dsa.modules.reporter.dto.AppLogStatDto;
import org.dsa.modules.reporter.entity.*;
import org.dsa.modules.reporter.enums.AppTypeEnum;
import org.dsa.modules.reporter.service.ApplicationHistoryService;
import org.dsa.modules.reporter.service.LocaleService;
import org.dsa.modules.reporter.vo.client.ClientHisPageReVo;
import org.dsa.modules.reporter.vo.client.UpgradeLogPageReVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class ApplicationHistoryServiceImpl implements ApplicationHistoryService {
    @Autowired
    AppStatsMapper historyMapper;

    @Autowired
    AppHistoryMapper detailMapper;

    @Override
    public AppHistory queryAppHisByDeviceId(String deviceId, Integer updateId) {
        return detailMapper.queryAppHisByDeviceId(deviceId, updateId);
    }

    @Override
    public void statsHistory(String startTime, String endTime) {
        detailMapper.insertAppHisStat(startTime, endTime);
    }

    @Override
    public List<AppLogStatDto> queryData(String startTime, String endTime) {
        return detailMapper.queryAppLogStat(startTime, endTime);
    }

    @Override
    public void maintainData(List<AppLogStatDto> dtos) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        for (AppLogStatDto dto: dtos) {

            QueryWrapper<AppStats> qw = new QueryWrapper<>();
            qw.eq("app_type", dto.getAppType());
            qw.eq("app_id", dto.getAppId());
            qw.eq("version_number", dto.getVersionNumber());
            qw.eq("status_type", dto.getStatusType());
            qw.eq("status", dto.getStatus());
            qw.apply(true, "collect_date = CAST('"+dto.getDay()+"' as date)");

            AppStats his = historyMapper.selectOne(qw);
            if(null == his){
                AppStats entity = new AppStats();
                BeanUtil.copyProperties(dto,entity);
                try {
                    Date day = sdf.parse(dto.getDay());
                    entity.setCollectDate(day );
                    entity.setAppVersion(dto.getAppVersion());

                    historyMapper.insert(entity);
                }catch (Exception ex){
                    log.error("maintainData error: {}", ex.getMessage());
                }
            }else{
                his.setTotal(Long.valueOf(dto.getTotal()));
                historyMapper.updateById(his);
            }

        }
    }

    @Override
    public List<AppHistory> queryHistoryByTag(String tag) {
        return detailMapper.queryByTag(tag);
    }

    @Override
    public Page<AppHistory> queryClientHis(UpgradeLogPageReVo vo) {
        Page<AppHistory> page = new Page<>(vo.getPageNo(), vo.getPageSize());
        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);
        LambdaQueryWrapper<AppHistory> qw = Wrappers.<AppHistory>lambdaQuery();
        if(StrUtil.isNotBlank(vo.getPcid())){
            qw.eq(AppHistory::getDeviceId, vo.getPcid());
        }
        if(StrUtil.isNotBlank(vo.getMessage()) ){

            qw.apply("workshop_name"+" ilike {0}", "%"+vo.getMessage().trim()+"%")
                    .or()
                    .apply("username"+" ilike {0}", "%"+vo.getMessage().trim()+"%")
                    .or()
                    .apply("app_type"+" ilike {0}", "%"+vo.getMessage().trim()+"%")
                    .or()
                    .apply("app_name"+" ilike {0}", "%"+vo.getMessage().trim()+"%")
                    .or()
                    .apply("app_version"+" ilike {0}", "%"+vo.getMessage().trim()+"%");
        }
        Page<AppHistory> result =  detailMapper.selectPage(page, qw);
        return result;
    }

    @Override
    public Page<AppHistory> queryHis(ClientHisPageReVo vo) {
        Page<AppHistory> page = new Page<>(vo.getPageNo(), vo.getPageSize());
        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);
        LambdaQueryWrapper<AppHistory> qw = Wrappers.<AppHistory>lambdaQuery();

        if (vo.getTaskTime() != null && vo.getTaskTime().size() == 2) {
            qw.between(AppHistory::getStartTime, Timestamp.valueOf(vo.getTaskTime().get(0)+" 00:00:00.000"),Timestamp.valueOf(vo.getTaskTime().get(1)+" 23:59:59.999"));
        }

        if(StrUtil.isNotBlank(vo.getPcid())){
            qw.apply("device_id"+" ilike {0}", "%"+vo.getPcid().trim()+"%");
        }

        //支持用户名和显示名搜索
        if(StrUtil.isNotBlank(vo.getUsername())){
            qw.apply("username"+" ilike {0}", "%"+vo.getUsername().trim()+"%");
        }

        if(StrUtil.isNotBlank(vo.getWorkshop())){
            qw.apply("workshop_name"+" ilike {0}", "%"+vo.getWorkshop().trim()+"%");
        }
        if(StrUtil.isNotBlank(vo.getAppType())){
            qw.eq(AppHistory::getAppType, vo.getAppType());
        }
        if(null != vo.getStatusType()){
            qw.eq(AppHistory::getStatusType, vo.getStatusType());
        }

        //多语言处理
        Page<AppHistory> result =  detailMapper.selectPage(page, qw);

        return result;
    }

    @Override
    public void insertHisData(List<ClientUpgrade> records, Map<Integer, CurrentSoftwareInfo> currentSoftwareInfoMap, Map<Integer, FutureSoftwareInfo> futureSoftwareInfoMap, Map<Integer, CurrentComponentInfo> currentComponentInfoMap, Map<Integer, FutureComponentInfo> futureComponentInfoMap) {

        for (ClientUpgrade upgrade: records) {

            AppHistory check = detailMapper.queryAppHisBySessionId(upgrade.getSessionId(), upgrade.getUpdateId());
            if(null != check){
                continue;
            }

            AppHistory his = new AppHistory();

            his.setTag(upgrade.getTag());
            his.setUserId(0L);
            his.setUsername(upgrade.getUsername());
            his.setUserDisplayName(upgrade.getUserDisplayName());
            his.setWorkshopId(upgrade.getWorkshopId());
            his.setWorkshopName(upgrade.getWorkshopName());
            his.setDeviceId(upgrade.getDeviceId());
            his.setUpdateId(upgrade.getUpdateId());
            his.setStartTime(upgrade.getStartTime());
            his.setEndTime(upgrade.getEndTime());
            his.setCollectDate(upgrade.getCtime());
            his.setStatus(upgrade.getUpdateResult());
            his.setStatusType(upgrade.getInstallAction());
            his.setSessionId(upgrade.getSessionId());

            if(upgrade.getUpdateTarget()  == 0 ){
                his.setAppType(AppTypeEnum.SOFTWARE.getValue());
                his.setAppId(upgrade.getSoftwareId());

                his.setAppVersion(upgrade.getSoftwareTargetVersion());
                his.setAppPrevVersion(upgrade.getSoftwareCurrentVersion());

                his.setVersionNumber(upgrade.getSoftwareCurrentVersionNumber());
                his.setPrevVersionNumber(upgrade.getSoftwareTargetVersionNumber());

                CurrentSoftwareInfo csi = currentSoftwareInfoMap.get(upgrade.getSoftwareId());
                if(null != csi){
                    his.setAppName(csi.getSoftwareName());
                }

                FutureSoftwareInfo fsi = futureSoftwareInfoMap.get(upgrade.getSoftwareId());
                if(null != fsi){
                    his.setExpireDate(fsi.getExpireDate());
                    his.setExpireWarningDays(fsi.getExpireWarningDays());
                    his.setReleaseNotes(fsi.getReleaseNotes());
                }

            }else{
                his.setAppType(AppTypeEnum.COMPONENT.getValue());
                his.setAppId(upgrade.getComponentId());
                his.setAppVersion(upgrade.getComponentTargetVersion());
                his.setAppPrevVersion(upgrade.getComponentCurrentVersion());
                his.setVersionNumber(upgrade.getComponentTargetVersionNumber());
                his.setPrevVersionNumber(upgrade.getComponentCurrentVersionNumber());

                CurrentComponentInfo cci = currentComponentInfoMap.get(upgrade.getComponentId());

                if(null != cci){
                    his.setAppName(cci.getComponentName());
                }
                FutureComponentInfo fci = futureComponentInfoMap.get(upgrade.getComponentId());
                if(null != fci){
                    his.setReleaseNotes(fci.getReleaseNotes());
                }
            }

            detailMapper.insert(his);
        }
    }

}
