package org.dsa.modules.diagnostic.entity.mqtt.callback;


import lombok.Data;

import java.io.Serializable;
@Data
public class MqBodyDataIDENTVO implements Serializable {
    //定义序列化 serialVersionUID
    private static final long serialVersionUID=1l;
    /**
     * Ecu名称
     */
    private String  ecuName;
    /**
     * 多语言标识
     */
    private String ti;

    /**
     * 标识名称
     */
    private String name;

    /**
     * 标识值
     */
    private String value;
    /**
     * 标识值
     */
    private String tiValue;

}
