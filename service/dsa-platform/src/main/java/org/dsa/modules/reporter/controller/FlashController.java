package org.dsa.modules.reporter.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.Query;
import org.dsa.common.utils.R;
import org.dsa.modules.reporter.dto.ChartItemDto;
import org.dsa.modules.reporter.dto.EcuVersionHistoryDto;
import org.dsa.modules.reporter.dto.FlashVehicleStatsDto;
import org.dsa.modules.reporter.dto.SqlBuilderDto;
import org.dsa.modules.reporter.entity.AnalysisCube;
import org.dsa.modules.reporter.service.AnalysisCubeService;
import org.dsa.modules.reporter.service.SqlBuilderService;
import org.dsa.modules.reporter.service.VehicleVersionService;
import org.dsa.modules.reporter.util.ArrayUtils;
import org.dsa.modules.reporter.vo.ChartReVo;
import org.dsa.modules.reporter.vo.client.ClientChartResVo;
import org.dsa.modules.reporter.service.FlashService;
import org.dsa.modules.reporter.vo.flash.FlashReqVo;
import org.dsa.modules.reporter.vo.flash.VehicleStatPageReqVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import java.util.*;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/flash")
public class FlashController {

    @Autowired
    FlashService flashService;

    @Resource
    SqlBuilderService sqlBuilderService;

    @Autowired
    AnalysisCubeService cubeService;

    @Autowired
    VehicleVersionService vehicleVersionService;


    @RequestMapping(value = "/page", method = {RequestMethod.POST })
    public R page(@RequestBody FlashReqVo flashReqVo){
        return R.ok().put("data", flashService.getRecordPage(flashReqVo));
    }

    @RequestMapping(value = "/getByVin", method = {RequestMethod.POST })
    public R getByVin(@RequestBody FlashReqVo flashReqVo){
        return R.ok().put("data", flashService.getByVin(flashReqVo));
    }

    @RequestMapping(value = "/vehicleTypeChart", method = {RequestMethod.POST })
    public R vehicleTypeChart(@Valid @RequestBody ChartReVo vo){
        AnalysisCube cube = null;
        cube = cubeService.getCubeByCode(vo.getCode());

        if(null == cube){
            return R.error();
        }

        Map<String, Object> map = null;
        if(!CollectionUtil.isEmpty(vo.getCondition())){
            map = vo.getCondition();
            if (null == map.get("vehicletypeid") || "[]".equals(map.get("vehicletypeid").toString())) {
                map.put("vehicletypeid", null);
            }
        }

        if(StrUtil.isNotBlank(vo.getStartDate())){
            if(null == map){
                map = new HashMap<>();
            }
            map.put("startDate", vo.getStartDate());
        }

        SqlBuilderDto sqlBuilderDto = new SqlBuilderDto();
        BeanUtil.copyProperties(cube, sqlBuilderDto);

        String sql = sqlBuilderService.render(sqlBuilderDto, map, vo.getTop());
        log.info("flash chart {} sql: {}", cube.getCode() ,sql);

        List<Map<String, Object>> items =  cubeService.getData(sql);

        log.info("flash chart result:{}", JSONObject.toJSONString(items));
        Set<String> legendData = new HashSet<>();
        Map<String,Map<String, Object>> subBurstStore = new HashMap<>();
        Map<String,Long> stat = new HashMap<>();
        List<ChartItemDto> dtoList = new ArrayList<>();
        ClientChartResVo resVo = new ClientChartResVo();
        Long total = 0L;

        for (Map<String, Object> dto: items){
            String parent = dto.get("parent").toString();
            legendData.add(parent);
            Long value = Long.valueOf(dto.get("value").toString());

            if(null != stat.get(parent)){
                stat.put(parent, value + stat.get(parent));
            }else{
                stat.put(parent, value);
            }

            Map<String, Object> rmap = null;
            rmap = subBurstStore.get(parent);
            if(null == rmap){
                rmap = new HashMap<>();
            }
            rmap.put(dto.get("name").toString(), value);
            subBurstStore.put(parent, rmap);
        }

        Iterator<Map.Entry<String, Map<String, Object>>> entries = subBurstStore.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry<String, Map<String, Object>> entry = entries.next();
            String key = entry.getKey();
            Map<String, Object> value = entry.getValue();
            ChartItemDto dto = new ChartItemDto();
            dto.setName(key);
            dto.setValue(stat.get(key));
            List<Map<String,Object>> children = new ArrayList<>();

            for (String k: value.keySet()){
                Map<String,Object> m = new HashMap<>();
                m.put("name", k);
                m.put("value", value.get(k));
                total += Long.parseLong(value.get(k).toString());
                children.add(m);
            }
            dto.setChildren(children);
            dtoList.add(dto);
        }
        resVo.setItemTotal(total);
        resVo.setSeriesData(dtoList);
        resVo.setLegendData(legendData);

        return R.ok().put("data", resVo);
    }

    @RequestMapping(value = "/chart", method = {RequestMethod.POST })
    public R chart(@Valid @RequestBody ChartReVo vo){
        AnalysisCube cube = null;
        cube = cubeService.getCubeByCode(vo.getCode());

        if(null == cube){
            return R.error();
        }

        Map<String, Object> map = null;
        if(!CollectionUtil.isEmpty(vo.getCondition())) {
            map = ArrayUtils.keyToLowerCase(vo.getCondition());
            if (null == map.get("vehicletypeids") || "[]".equals(map.get("vehicletypeids").toString())) {
                map.put("vehicletypeid", null);
            } else {
                map.put("vehicletypeid", map.get("vehicletypeids"));
            }

            map.put("startDate", map.get("startdate"));
            map.put("endDate", map.get("enddate"));
        }
//        if(!CollectionUtil.isEmpty(vo.getCondition())){
//            map = vo.getCondition();
//        }
//
//        if(StrUtil.isNotBlank(vo.getStartDate())){
//            if(null == map){
//                map = new HashMap<>();
//            }
//            map.put("startDate", vo.getStartDate());
//        }

        SqlBuilderDto sqlBuilderDto = new SqlBuilderDto();
        BeanUtil.copyProperties(cube, sqlBuilderDto);

        String sql = sqlBuilderService.render(sqlBuilderDto, map, vo.getTop());
        log.info("flash chart {} sql: {}", cube.getCode() ,sql);

        List<Map<String, Object>> items =  cubeService.getData(sql);

        log.info("flash chart result:{}", JSONObject.toJSONString(items));

        ClientChartResVo resVo = cubeService.formatChartData(items, vo.getDimension(), vo.getChart());

        return R.ok().put("data", resVo);
    }


    @RequestMapping(value = "/vehicleStats", method = {RequestMethod.POST })
    public R vehicleStats(@RequestBody ChartReVo vo){

        AnalysisCube cube = null;
        cube = cubeService.getCubeByCode(vo.getCode());

        if(null == cube){
            return R.error();
        }

        Map<String, Object> map = null;
        if(!CollectionUtil.isEmpty(vo.getCondition())){
            map = vo.getCondition();
        }

        if(StrUtil.isNotBlank(vo.getStartDate())){
            if(null == map){
                map = new HashMap<>();
            }
            map.put("startDate", vo.getStartDate());
        }

        SqlBuilderDto sqlBuilderDto = new SqlBuilderDto();
        BeanUtil.copyProperties(cube, sqlBuilderDto);

        String sql = sqlBuilderService.render(sqlBuilderDto, map, vo.getTop());
        log.info("flash chart {} sql: {}", cube.getCode() ,sql);

        List<Map<String, Object>> items =  cubeService.getData(sql);

        return R.ok().put("data", items);
    }


    @RequestMapping(value = "/vehicleStatsPage", method = {RequestMethod.POST })
    public R vehicleStatsPage(@RequestBody VehicleStatPageReqVo vo){
        Map<String, Object> map = new HashMap<>();
        map.put("page", vo.getPageNo().toString());
        map.put("limit", vo.getPageSize().toString());

        if(null != vo.getSortColumn()){
            if("id".equals(vo.getSortColumn())){
                vo.setSortColumn("dvt.value");
            }
            String column = StrUtil.toUnderlineCase(vo.getSortColumn());
            map.put("sidx", column);
        }else{
            map.put("sidx", "dvt.value");
        }

        if(vo.getSortAsc()){
            map.put("order", "asc");
        }else{
            map.put("order", "desc");
        }

        IPage<FlashVehicleStatsDto> iPage = new Query<FlashVehicleStatsDto>().getPage(map);
        if(!CollectionUtil.isEmpty(vo.getDates())){
            if(StrUtil.isNotBlank(vo.getDates().get(0))){
                vo.setStartDate(vo.getDates().get(0));
            }
            if(StrUtil.isNotBlank(vo.getDates().get(1))){
                vo.setEndDate(vo.getDates().get(1));
            }
        }

        IPage<FlashVehicleStatsDto> resultPage =  flashService.selectVehicleStats(iPage, vo);
        return R.ok().put("data", resultPage);
    }

    @RequestMapping(value = "/getDetail", method = {RequestMethod.POST })
    public R getDetail(@RequestBody FlashReqVo flashReqVo){
        return R.ok().put("data", flashService.getDetail(flashReqVo));
    }

    @RequestMapping(value = "/getEcu", method = {RequestMethod.POST })
    public R getEcu(@RequestBody FlashReqVo flashReqVo){
        return R.ok().put("data", flashService.getEcu(flashReqVo));
    }

    @RequestMapping(value = "/getEcuHis", method = {RequestMethod.POST })
    public R getEcuHis(@RequestBody FlashReqVo flashReqVo){
        return R.ok().put("data", flashService.getEcuHis(flashReqVo));
    }

    @RequestMapping(value = "/getVehicleHis", method = {RequestMethod.POST })
    public R getVehicleHis(@RequestBody FlashReqVo flashReqVo){
        return R.ok().put("data", flashService.getVehicleHis(flashReqVo));
    }

    @RequestMapping(value = "/getEcuNames", method = {RequestMethod.GET })
    public R getEcuNames(){
        return R.ok().put("data", flashService.getEcuNames());
    }


    @RequestMapping(value = "/getEcuHistory", method = {RequestMethod.POST })
    public R getEcuHistory(@RequestBody FlashReqVo flashReqVo){

        List<EcuVersionHistoryDto> dtoList  =  flashService.getEcuVersionHistory(flashReqVo.getVin(), flashReqVo.getVersion());

        Map<String, List<EcuVersionHistoryDto>> data = new HashMap<>();

        for (EcuVersionHistoryDto dto: dtoList){
            List<EcuVersionHistoryDto> ecuVerions = null;
            if(null == data.get(dto.getEcuName())){
                ecuVerions = new ArrayList<>();
                ecuVerions.add(dto);
                data.put(dto.getEcuName(), ecuVerions);
            }else{
                ecuVerions = data.get(dto.getEcuName());
                ecuVerions.add(dto);
                data.put(dto.getEcuName(), ecuVerions);
            }
        }

        Iterator<String> iterator = data.keySet().iterator();
        Map<String, Map<String, Set<String>>>  ecuVersionHistoryMap = new HashMap<>();

        while (iterator.hasNext()) {
            String ecu = iterator.next();
            List<EcuVersionHistoryDto> versionList = data.get(ecu);
            Map<String, Set<String>> ecuMap = ecuVersionHistoryMap.get(ecu);
            if(null == ecuMap){
                ecuMap = new HashMap<>();
            }

            for (EcuVersionHistoryDto evd: versionList){
                Set<String> versionSet = null;
                versionSet = ecuMap.get(evd.getVersionName());
                if(null == versionSet){
                    versionSet = new HashSet<>();
                }
                versionSet.add( evd.getVersionValue());
                ecuMap.put(evd.getVersionName(), versionSet);
            }

            ecuVersionHistoryMap.put(ecu, ecuMap);
        }
        return R.ok().put("data", ecuVersionHistoryMap);
    }

    @RequestMapping(value = "/ecuPi", method = {RequestMethod.POST })
    public R ecuPi(@Valid @RequestBody ChartReVo vo){
        ClientChartResVo resVo =  flashService.statVehicleEcuPi(vo);
        return R.ok().put("data", resVo);
    }

    @RequestMapping(value = "/ecuBar", method = {RequestMethod.POST })
    public R ecuBar(@Valid @RequestBody ChartReVo vo){
        ClientChartResVo resVo =  flashService.statVehicleEcuBar(vo);
        return R.ok().put("data", resVo);
    }


    @RequestMapping("/getVehicleVersion/{vin}")
    public R getVehicleVersion(@PathVariable("vin") String vin){
        if (StrUtil.isBlank(vin)) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        return R.ok().put("data", vehicleVersionService.getVehicleVersion(vin));
    }
}
