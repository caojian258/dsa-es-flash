//package org.dsa.modules.remoteDiagnostic.entity;
//
//import com.baomidou.mybatisplus.annotation.IdType;
//import com.baomidou.mybatisplus.annotation.TableId;
//import com.baomidou.mybatisplus.annotation.TableName;
//import org.dsa.modules.remoteDiagnostic.pojo.BaseEntity;
//
//import lombok.Data;
//
//import java.io.Serializable;
//
//@Data
//@TableName("r_platform_ecu")
//public class RemoteVehiclePlatformEcuEntity extends BaseEntity implements Serializable {
//
//
//    private static final long serialVersionUID = 1L;
//
//    @TableId(type = IdType.AUTO)
//    private Long id;
//
//    private Long platformId;
//
//    private Long ecuId;
//}
