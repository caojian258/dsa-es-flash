package org.dsa.modules.reporter.event;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.watch.WatchMonitor;
import cn.hutool.core.io.watch.Watcher;
import lombok.extern.slf4j.Slf4j;
import org.dsa.config.SyncConfig;
import org.dsa.modules.reporter.util.FilesUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.WatchEvent;

@Slf4j
@Component
public class FileWatchMonitor implements ApplicationListener<ApplicationReadyEvent> {

    @Autowired
    SyncConfig syncConfig;

    @Autowired
    OnlineUpdatePublisher onlineUpdatePublisher;

    @Autowired
    SessionPublisher sessionPublisher;

    /***
     * @todo 这里要针对不同的工作目录进行监控 其中包含 session online_update flash
     * 这里只监控一个主目录的根据子目录名称，和配置走不同的处理逻辑。
     * @param applicationReadyEvent
     */
    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        //同步主目录
        String basePath = syncConfig.getBaseDir();

        File file = FileUtil.file(basePath);

        Watcher onlineUpdateWatcher = new Watcher(){

            @Override
            public void onCreate(WatchEvent<?> event, Path currentPath) {
                Object obj = event.context();
                log.info("创建：{}-> {}", currentPath, obj);
                String path = currentPath.toAbsolutePath() + File.separator + obj.toString();
                String tag = FilesUtil.makeTag();
                log.info("创建 file dir {} path: {}", currentPath.getFileName(),path);

                //目录和类型匹配
                log.info("online update upload dir {}, current dir: {}", syncConfig.getOnlineUpdateUploadDir() ,currentPath.getFileName());
//                System.out.println(syncConfig.getOnlineUpdateUploadDir().endsWith(currentPath.getFileName().toString()));
                //防止bug先注释掉 文件处理
//                if(syncConfig.getOnlineUpdateUploadDir().endsWith(currentPath.getFileName().toString())){
//                    if(obj.toString().endsWith(syncConfig.getOnlineUpdateFormat())){
//                        onlineUpdatePublisher.handlerFile(path, tag, obj.toString());
//                    }
//                }else if(syncConfig.getSessionUploadDir().endsWith(currentPath.getFileName().toString())){
//                    if(obj.toString().endsWith(syncConfig.getSessionFormat())){
//                        sessionPublisher.handlerFile(path, tag, obj.toString());
//                    }
//                }
            }

            @Override
            public void onModify(WatchEvent<?> watchEvent, Path path) {

            }

            @Override
            public void onDelete(WatchEvent<?> watchEvent, Path path) {

            }

            @Override
            public void onOverflow(WatchEvent<?> watchEvent, Path path) {

            }
        };

        WatchMonitor watchMonitor = WatchMonitor.create(file, WatchMonitor.ENTRY_CREATE);
        watchMonitor.setWatcher(onlineUpdateWatcher);
        watchMonitor.setMaxDepth(3);
        watchMonitor.start();

    }
}
