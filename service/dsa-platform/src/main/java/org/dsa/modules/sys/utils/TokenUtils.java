package org.dsa.modules.sys.utils;

import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.RestTemplateUtil;

import java.util.Map;

public final class TokenUtils {

    /**
     * 获取token
     */
    public static String getToken(String getTokenUrl) {
        String s = RestTemplateUtil.getForEntity(getTokenUrl, String.class);
        if(StringUtils.isNotEmpty(s)){
            Map<String, Object> ssoMessage = JSON.parseObject(s);
            if(ssoMessage!= null && "true".equals(ssoMessage.get("Success").toString())
                    && ssoMessage.get("ResData") != null && !"".equals(ssoMessage.get("ResData").toString())) {
                Map<String, Object> m = JSON.parseObject(ssoMessage.get("ResData").toString());
                if (m.get("AccessToken") != null && !"".equals(m.get("AccessToken"))) {
                    return "Bearer "+String.valueOf(m.get("AccessToken"));
                } else {
                    throw new RRException("get token faiel ", 500);
                }
            }else{
                throw new RRException("get token faiel ", 500);
            }
        }else{
            throw new RRException("get token faiel ", 500);
        }
    }
}
