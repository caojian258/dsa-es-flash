package org.dsa.modules.reporter.document;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class DiagnosticDailyDoc implements Serializable {

    private Long id;

     private String vin;

     private List<Long> vehicleTypeId;

     private String functionCode;

     private String action;

    private String  ctsName;

    private Long ctsNameCount;

    private String ctsDay;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date createdAt;
}
