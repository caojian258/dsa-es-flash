package org.dsa.modules.reporter.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ChartVisibleEnum {
    PI ("pi"),
    BAR ("bar"),
    LINE ("line"),
    SUNBURST ("sunburst"),
    ;

    @EnumValue
    private final String value;
}
