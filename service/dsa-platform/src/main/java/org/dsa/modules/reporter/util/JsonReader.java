package org.dsa.modules.reporter.util;

import java.io.*;

/***
 * JSON 文件解析
 */
public class JsonReader {

    /***
     * 文件路径
     */
    private String path;

    /***
     * 所用时间
     */
    private long spendTime;


    public JsonReader(){

    }

    public JsonReader(String path){
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path){
        this.path = path;
    }

    public Long getSpendTime(){
        return spendTime;
    }

    /***
     * 读取文件里面的内容得到json字符串
     * @return
     */
    public String getJsonString(){
        long startTime = System.currentTimeMillis();

        String jsonStr = "";
        try {
            File jsonFile = new File(this.path);
            FileReader fileReader = new FileReader(jsonFile);
            Reader reader = new InputStreamReader(new FileInputStream(jsonFile),"utf-8");
            int ch = 0;
            StringBuffer sb = new StringBuffer();
            while ((ch = reader.read()) != -1) {
                sb.append((char) ch);
            }
            fileReader.close();
            reader.close();
            jsonStr = sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        long endTime = System.currentTimeMillis();
        spendTime = endTime - startTime;

        return jsonStr;
    }
}
