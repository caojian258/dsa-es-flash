package org.dsa.modules.reporter.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.utils.R;
import org.dsa.modules.reporter.constant.ConfigConstant;
import org.dsa.modules.reporter.dto.AppOptionDto;
import org.dsa.modules.reporter.dto.ClientBaseInfoDto;
import org.dsa.modules.reporter.dto.ClientDetailDto;
import org.dsa.modules.reporter.dto.SqlBuilderDto;
import org.dsa.modules.reporter.entity.*;
import org.dsa.modules.reporter.service.*;
import org.dsa.modules.reporter.util.ArrayUtils;
import org.dsa.modules.reporter.util.LocalUtil;
import org.dsa.modules.reporter.vo.api.DeviceInfoVo;
import org.dsa.modules.reporter.vo.client.*;
import org.dsa.modules.sys.entity.SysConfigEntity;
import org.dsa.modules.sys.service.SysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/client")
public class ClientController {

    @Autowired
    ClientService clientService;

    @Autowired
    AnalysisCubeService cubeService;

    @Autowired
    SqlBuilderService sqlBuilderService;

    @Autowired
    ClientUserService clientUserService;

    @Autowired
    ApplicationHistoryService historyService;

    @Autowired
    ClientDependenceTreeService dependenceService;

    @Autowired
    SysConfigService sysConfigService;

    @Autowired
    LocaleService localeService;

    /***
     * 客户端列表
     * @param vo
     * @return
     */
    @RequestMapping(value = "/page", method = {RequestMethod.POST })
    public R selectPage(@Valid @RequestBody ClientPageReVo vo){
        Page<SystemOverview>  result = clientService.selectPage(vo);
        return R.ok().put("data", result);
    }

    /**
     * 应用版本概览列表
     * @param vo
     * @return
     */
    @RequestMapping(value = "/appPage", method = {RequestMethod.POST })
    public R appPage(@RequestBody AppPageReVo vo){
        Page<AppOverview>  result = clientService.selectAppPage(vo);
        return R.ok().put("data", result);
    }

    /***
     * 应用历史版本列表
     * @param vo
     * @return
     */
    @RequestMapping(value = "/appHisPage", method = {RequestMethod.POST })
    public R appHisPage(@RequestBody AppPageReVo vo){
        Page<AppVersion>  result = clientService.selectAppHisPage(vo);
        return R.ok().put("data", result);
    }


    /***
     * 应用列表
     * @return
     */
    @RequestMapping(value = "/appList", method = {RequestMethod.GET })
    public R appList(){
        List<AppOptionDto> apps =  clientService.queryAppOption();
        return R.ok().put("data", apps);
    }

    /***
     * 客户端基本信息
     * @param vo
     * @return
     */
    @RequestMapping(value = "/detail", method = {RequestMethod.POST })
    public R detail(@Valid @RequestBody ClientDetailReVo vo){
        ClientDetailDto detailDto =  clientService.detail(vo);
        return R.ok().put("data", detailDto);
    }

    @RequestMapping(value = "/historyPage", method = {RequestMethod.POST })
    public R historyPage(@Valid @RequestBody ClientHisPageReVo vo){
        Page<SystemHistory> result =  clientService.clientHistoryPage(vo);

        return R.ok().put("data", result);
    }

    @RequestMapping(value = "/protocolItemPage", method = {RequestMethod.POST })
    public R protocolItemItemPage(@Valid @RequestBody ProtocolItemPageReVo vo){
        Page<SystemProtocolItem> re = clientService.protocolItemPage(vo);

        return R.ok().put("data", re);
    }

    /***
     * 应用历史版本 client app his version for client detail
     * @param vo
     * @return
     */
    @RequestMapping(value = "/appVersionPage", method = {RequestMethod.POST })
    public R appVersionHistoryPage(@Valid @RequestBody AppVersionPageReVo vo){
        Page<AppVersion> re = clientService.appVersionHistoryPage(vo);
        Map<String, List<AppVersion>> recordsByDate = null;
        if(null != vo.getGroupBy() && vo.getGroupBy()){
            List<AppVersion> records =  re.getRecords();
            recordsByDate = records.stream().collect(groupingBy(AppVersion::getCollectDate));
        }
        return R.ok().put("data", re).put("groups", recordsByDate);
    }

    @RequestMapping(value = "/appInstallList", method = {RequestMethod.POST })
    public R appInstallList(@Valid @RequestBody ClientDetailReVo vo){
        List<AppInstallResVo> vos =  clientService.appInstallList(vo);
        return R.ok().put("data", vos);
    }

    @RequestMapping(value = "/appInstallPage", method = {RequestMethod.POST})
    public R appInstallPage(@Valid @RequestBody AppInstallPageReVo vo){
        Page<AppInstall> list = clientService.selectInstallPage(vo);
        return R.ok().put("data", list);
    }

    @RequestMapping(value = "/chart", method = {RequestMethod.POST })
    public R chart(@Valid  @RequestBody ClientChartReVo vo){
        Map<String, String> tableMap = new HashMap<>();
        tableMap.put("client", "client_os_overview");
        tableMap.put("client_application", "client_application_overview");
        tableMap.put("client_version", "client_application_version");

        AnalysisCube cube = null;
        if(null != vo.getCode()){
            cube = cubeService.getCubeByCode(vo.getCode());
        }else{
            cube = cubeService.getCube(tableMap.get(vo.getCategory()), vo.getDimension(), vo.getChart());
        }

        Map<String, Object> map = null;

        Integer top = null;
        if(!CollectionUtil.isEmpty(vo.getCondition())) {
            map = ArrayUtils.keyToLowerCase(vo.getCondition());
            if (null == map.get("workshopgroup") || "[]".equals(map.get("workshopgroup").toString())) {
                map.put("workshopgroup", null);
            }
            if (map.get("top") != null) {
                top = Integer.parseInt(map.get("top").toString());
            }
            map.put("startDate", map.get("startdate"));
            map.put("endDate", map.get("enddate"));
        }
        SqlBuilderDto sqlBuilderDto = new SqlBuilderDto();
        BeanUtil.copyProperties(cube,sqlBuilderDto);

        String sql = sqlBuilderService.render(sqlBuilderDto, map, top);

        log.info("client chart sql:{}", sql);
        List<Map<String, Object>> items =  cubeService.getData(sql);

        log.info("client chart result:{}", JSONObject.toJSONString(items));
        ClientChartResVo resVo = cubeService.formatChartData(items, vo.getDimension(), vo.getChart());
        return R.ok().put("data", resVo);
    }

    @RequestMapping(value = "/getInputs")
    public R getInputs(){
        Map<String,Object> vos =  clientService.getInputs();
        return R.ok().put("data", vos);
    }

    @RequestMapping(value = "/getHisAntivirus")
    public R getHisAntivirus(){
        List<String> vos =  clientService.getAllAnivirHistory();
        return R.ok().put("data", vos);
    }

    /***
     * 某个客户端的操作日志记录
     * @return
     */
    @RequestMapping(value = "/upgradeLogPage", method = {RequestMethod.POST })
    public R upgradeLogPage(@Valid @RequestBody UpgradeLogPageReVo vo){
        String lang = LocaleContextHolder.getLocale().toLanguageTag();

        Page<AppHistory> pageResult = historyService.queryClientHis(vo);

        List<AppHistory> items = new ArrayList<>(pageResult.getRecords().size());

        for (AppHistory his: pageResult.getRecords()) {
            his.setStatusMsg(localeService.translateDescription("ec"+his.getStatus(), lang, "ec"+his.getStatus()));
            items.add(his);
        }

        return R.ok().put("data", pageResult);
    }

    /***
     * 操作记录 application_history
     * @param vo
     * @return
     */
    @RequestMapping(value = "/hisPage", method = {RequestMethod.POST })
    public R hisPage(@Valid @RequestBody ClientHisPageReVo vo){
        String lang = LocaleContextHolder.getLocale().toLanguageTag();
        Page<AppHistory> pageResult = historyService.queryHis(vo);

        List<AppHistory> items = pageResult.getRecords();
        for (AppHistory item: items) {
            item.setStatusMsg(localeService.translateDescription("ec"+item.getStatus(), lang, "ec"+item.getStatus()));
        }
        pageResult.setRecords(items);
        return R.ok().put("data", pageResult);
    }

    /***
     * 设备终端详情 这里要展示的数据较少所以手动组装
     * @return
     */
    @RequestMapping(value = "/device", method = {RequestMethod.POST })
    public R device(@Valid @RequestBody DeviceReVo vo){
        Map<String, Object> deviceDto = clientUserService.clientDevice(vo.getPcid());
        DeviceInfoVo infoVo = new DeviceInfoVo();
        if(null == deviceDto || null == deviceDto.get("pcid")){
            return R.ok().put("data", infoVo);
        }

        List<ClientBaseInfoDto> baseList = new ArrayList<>();
        ClientBaseInfoDto dto = new ClientBaseInfoDto();
        dto.setField("pcid");
        dto.setTitle(LocalUtil.get("client.pcid"));
        dto.setValue(deviceDto.get("pcid").toString());
        dto.setSorted(0);
        baseList.add(dto);

        ClientBaseInfoDto dtoUsername = new ClientBaseInfoDto();
        dtoUsername.setField("last_username");
        dtoUsername.setTitle(LocalUtil.get("client.lastUsername"));
        dtoUsername.setValue(deviceDto.get("last_username").toString());
        dtoUsername.setSorted(10);

        baseList.add(dtoUsername);

        infoVo.setBaseList(baseList);

        List<ClientBaseInfoDto> workshopList = new ArrayList<>();

        ClientBaseInfoDto dtoWorkshop = new ClientBaseInfoDto();
        dtoWorkshop.setField("workshop_name");
        dtoWorkshop.setTitle(LocalUtil.get("client.workshopName"));
        dtoWorkshop.setValue(deviceDto.get("workshop_name").toString());
        dtoWorkshop.setSorted(0);
        workshopList.add(dtoWorkshop);

        ClientBaseInfoDto dtoWorkshopAddress = new ClientBaseInfoDto();
        dtoWorkshopAddress.setField("workshop_address");
        dtoWorkshopAddress.setTitle(LocalUtil.get("client.workshopAddress"));
        if(null != deviceDto.get("workshop_address")){
            dtoWorkshopAddress.setValue(deviceDto.get("workshop_address").toString());
        }

        dtoWorkshopAddress.setSorted(10);
        workshopList.add(dtoWorkshopAddress);

        infoVo.setWorkshopList(workshopList);

        List<ClientBaseInfoDto> LoginList = new ArrayList<>();

        ClientBaseInfoDto dtoFirst = new ClientBaseInfoDto();
        dtoFirst.setField("first_online_time");
        dtoFirst.setTitle(LocalUtil.get("client.firstOnlineTime"));
        dtoFirst.setValue(deviceDto.get("first_online_time").toString());
        dtoFirst.setSorted(0);

        LoginList.add(dtoFirst);

        ClientBaseInfoDto dtoLast = new ClientBaseInfoDto();
        dtoLast.setField("last_online_time");
        dtoLast.setTitle(LocalUtil.get("client.lastOnlineTime"));
        dtoLast.setValue(deviceDto.get("last_online_time").toString());
        dtoLast.setSorted(10);

        LoginList.add(dtoLast);

        infoVo.setLoginList(LoginList);

        return R.ok().put("data", infoVo);
    }

    /***
     * 根据软件查询组件版本信息
     * @param vo
     * @return
     */
    @RequestMapping(value = "/componentVersion", method = {RequestMethod.POST })
    public R componentVersion(@Valid @RequestBody ComponentVersionReVo vo){
//       List<AppOverview> list =  upgradeResultService.queryComponentOverview(vo);
        return R.ok();
    }

    /***
     * 根据软件查询组件历史版本信息
     * @param vo
     * @return
     */
    @RequestMapping(value = "/componentHisVersion", method = {RequestMethod.POST })
    public R componentHisVersion(@Valid @RequestBody ComponentVersionReVo vo){
//        List<AppVersion> list = upgradeResultService.queryComponentVersion(vo);
        return R.ok();
    }

    /***
     * 客户端的活跃度
     * @param vo
     * @return
     */
    @RequestMapping(value = "/devicePage", method = {RequestMethod.POST })
    public R deviceList(@Valid @RequestBody ClientPageReVo vo){
        Page<ClientUser> page = clientUserService.selectClientPage(vo);
        return R.ok().put("data", page);
    }

    @RequestMapping(value = "/tree", method = {RequestMethod.GET })
    public R deviceTree(@Valid @RequestParam(name = "id") String id){

        List<ClientDependenceTree> treeList = dependenceService.getDependenceListByDeviceId(id);

        System.out.println("treeList:");
        System.out.println(JSONObject.toJSONString(treeList));

        List<Map<String, Object>> treeMap = new ArrayList<>();

        treeMap = dependenceService.formatTreeData(treeList);

        return R.ok().put("data", treeMap) ;
    }

    @RequestMapping(value = "/chartApps", method = {RequestMethod.GET })
    public R chartApps(){
        List<SysConfigEntity> list = sysConfigService.getLikeGroup(ConfigConstant.REPORTING_CHART_APPS);
        List<Map<String, Object>> maps = new ArrayList<>();
        if(CollectionUtil.isNotEmpty(list)){
            for (SysConfigEntity config: list) {
                Map<String, Object> m = new HashMap<>();
                m.put("name",config.getParamKey());
                m.put("id",Integer.valueOf(config.getParamValue()));
                maps.add(m);
            }
        }

        return R.ok().put("data", maps);
    }
}
