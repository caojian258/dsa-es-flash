package org.dsa.modules.onlineUpdate.controller;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.annotation.SysLog;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.PageUtils;
import org.dsa.common.utils.R;
import org.dsa.modules.onlineUpdate.dao.SwVCompVDao;
import org.dsa.modules.onlineUpdate.dto.SoftwareVersionDto;
import org.dsa.modules.onlineUpdate.entity.SoftwareVersionEntity;
import org.dsa.modules.onlineUpdate.service.SoftwareVersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * 软件版本
 *
 */
@Slf4j
@RestController
@RequestMapping("onlineUpdate/softwareVersion")
public class SoftwareVersionController {
    @Autowired
    private SoftwareVersionService softwareVersionService;

    @Autowired
    private SwVCompVDao dependencyDao;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestBody Map<String, Object> params){
        try{
            PageUtils page = softwareVersionService.queryPage(params);
            return R.ok().put("page", page);
        }catch (Exception e) {
            log.error("software list error:"+ e.getMessage());
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Integer id){
     try {
         return R.ok().put("softwareVersion", softwareVersionService.info(id));
     }catch (RRException rre) {
            log.error(String.format("softwareVersion info error: id: %s msg: %s",id,rre.getMessage()));
            return R.error(rre.getCode(),rre.getMsg());
         } catch (Exception e) {
            log.error("softwareVersion info error:"+ e.getMessage());
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
         }
    }

    /**
     * 保存
     */
    @SysLog("保存软件版本")
    @RequestMapping("/save")
    public R save(@RequestBody SoftwareVersionDto softwareVersion){
        try {
            log.info("save software param {} ", JSON.toJSONString(softwareVersion));
		    softwareVersionService.saveOrUpdate2(softwareVersion);
            return R.ok();
        } catch (RRException rre) {
            log.error("softwareVersion save error:"+ rre.getMessage());
            return R.error(rre.getCode(),rre.getMsg());
        } catch (Exception e) {
            log.error("softwareVersion save error:"+ e.getMessage());
            if (e.getMessage().contains("ERROR: duplicate key value violates unique")) {
                return R.error(Constant.Msg.NAME_DUPLICATION_ERROR);
            }
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    /**
     * 修改
     */
    @SysLog("修改软件版本")
    @RequestMapping("/update")
    public R update(@RequestBody SoftwareVersionDto softwareVersion){
        try {
            log.info("update software {}", JSON.toJSONString(softwareVersion));
            if (ObjectUtil.isNull(softwareVersion.getId())) {
                throw new RRException(Constant.Msg.CHECK_ID_ERROR);
            }
            softwareVersionService.saveOrUpdate2(softwareVersion);
            return R.ok();
        } catch (RRException rre) {
            log.error("softwareVersion update error:"+ rre.getMessage());
            return R.error(rre.getCode(),rre.getMsg());
        } catch (Exception e) {
            log.error("softwareVersion update error:"+ e.getMessage());
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    /**
     * 删除
     */
    @SysLog("删除软件版本")
    @RequestMapping("/delete")
    public R delete(@RequestBody Integer[] ids){
         try {
             log.info("disabled softwareVersion param {}", JSON.toJSONString(ids));
		     softwareVersionService.removeByIds(Arrays.asList(ids));
             return R.ok();
         } catch (RRException rre) {
             log.error("delete softwareVersion param {}" , rre.getMessage());
             return R.error(rre.getCode(),rre.getMsg());
         } catch (Exception e) {
             log.error("delete softwareVersion error:"+ e.getMessage());
             return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
         }
    }

    /**
     * 查询软件启动入口
     */
    @PostMapping("/entry")
    public R queryEntryAndHistory(@RequestBody Map<String,Long> param){
        try {
            log.info("query software entry {}", param);
            Long softwareId = param.get("softwareId");
            Map<String, Object> res = softwareVersionService.querySoftwareEntry(softwareId);
            return R.ok().put("data", res);
        } catch (RRException rre) {
            log.error("query software entry error: {}" , rre.getMessage());
            return R.error(rre.getCode(),rre.getMsg());
        } catch (Exception e) {
            log.error("query software entry error:{}", e.getMessage());
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    /**
     * 根据软件id获取所有版本
     * @param sId
     * @return
     */
    @RequestMapping("/listByCompId/{sId}")
    public R listByCompId(@PathVariable("sId") Long sId){
        try {
            LambdaQueryWrapper<SoftwareVersionEntity> queryWrapper = Wrappers.<SoftwareVersionEntity>lambdaQuery()
                    .select(SoftwareVersionEntity::getVersionName,
                            SoftwareVersionEntity::getId,
                            SoftwareVersionEntity::getStatus,
                            SoftwareVersionEntity::getCreatedAt)
                    .eq(SoftwareVersionEntity::getSoftwareId, sId)
                    .orderByAsc(SoftwareVersionEntity::getVersionNumber);
            List<SoftwareVersionEntity> list = softwareVersionService.getBaseMapper().selectList(queryWrapper);
            return R.ok().put("versions", list);
        }catch (Exception e){
            log.error(e.getMessage());
            e.printStackTrace();
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }
    /**
     * 根据软件版本id获取详情，软件版本信息，规则信息，软件关联组件信息，操作记录
     */
    @RequestMapping("/infoBySoftwareVersionId/{versionId}")
    public R infoByCompVersionId(@PathVariable("versionId") Long versionId){
        try {
            Map<String,Object> info = softwareVersionService.queryInfoBySoftwareVersionId(versionId);
            return R.ok().put("info", info);
        }catch (Exception e){
            log.error(e.getMessage());
            e.printStackTrace();
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    /**
     * 版本复制
     */
    @RequestMapping("/copy/{versionId}")
    public R copy(@PathVariable() Long versionId){
        try {
            softwareVersionService.copyVersionByVersionId(versionId);
            return R.ok();
        }catch (Exception e){
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

}
