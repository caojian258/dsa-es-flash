package org.dsa.modules.remoteDiagnostic.vo.Task;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.modules.remoteDiagnostic.pojo.PageParam;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class DiagnosticTaskOverviewPageReqVo extends PageParam {

    private String id;

    private String name;

    private Integer status;

    private String validityPeriod;

}