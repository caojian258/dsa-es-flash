package org.dsa.modules.reporter.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.utils.R;
import org.dsa.modules.reporter.job.SessionFlashJob;
import org.dsa.modules.reporter.job.SessionLogJob;
import org.dsa.modules.reporter.job.SystemDataMinerJob;
import org.dsa.modules.reporter.job.WorkshopJob;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static org.quartz.DateBuilder.futureDate;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/init")
public class InitController {

    @Autowired
    private Scheduler scheduler;

    @Autowired
    private SchedulerFactoryBean schedulerFactoryBean;

    @RequestMapping(value = "/createTable", method = {RequestMethod.GET })
    public R createTable(){

        return R.ok();
    }

    @RequestMapping(value = "/clearBizTable", method = {RequestMethod.GET })
    public R clearBizTable(){

        return R.ok();
    }

    @RequestMapping(value = "/demoRun", method = {RequestMethod.GET })
    public R demoDataRun() throws SchedulerException {

        Scheduler sched = schedulerFactoryBean.getScheduler();

        //workshop
        JobDetail workshopJob = newJob(WorkshopJob.class).withIdentity("workshopJobDemo").build();
        Trigger workshopTrigger = newTrigger().withIdentity("workshopTrigger", "demo").startNow().build();
        sched.scheduleJob(workshopJob, workshopTrigger);

        //session
        JobDetail sessionLogJob = newJob(SessionLogJob.class).withIdentity("sessionLogJobDemo").build();
        Trigger sessionLogJobTrigger = newTrigger().withIdentity("sessionLogJobTrigger", "demo")
                .startAt(futureDate((10000 + (2 * 100)), DateBuilder.IntervalUnit.MILLISECOND))
                .build();
        sched.scheduleJob(sessionLogJob, sessionLogJobTrigger);


        JobDetail systemJob = newJob(SystemDataMinerJob.class).withIdentity("systemJobDemo").build();
        Trigger systemJobTrigger = newTrigger().withIdentity("systemJobTrigger", "demo")
                .startAt(futureDate((10000 + (3 * 100)), DateBuilder.IntervalUnit.MILLISECOND))
                .build();

        sched.scheduleJob(systemJob, systemJobTrigger);

        //flash
        JobDetail sessionFlashJob = newJob(SessionFlashJob.class).withIdentity("sessionFlashJobDemo").build();
        Trigger sessionFlashJobTrigger = newTrigger().withIdentity("sessionFlashJobTrigger", "demo")
                .startAt(futureDate((10000 + (4 * 100)), DateBuilder.IntervalUnit.MILLISECOND))
                .build();

        sched.scheduleJob(sessionFlashJob, sessionFlashJobTrigger);

        return R.ok();
    }


}
