package org.dsa.modules.onlineUpdate.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 软件入口表
 * 
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@TableName("o_software_entry")
public class SoftwareEntryEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 自增id
	 */
	@TableId(type = IdType.AUTO)
	private Long id;
	/**
	 * 软件id
	 */
	private Long softwareId;
	/**
	 * 组件id
	 */
	private Long componentId;
}
