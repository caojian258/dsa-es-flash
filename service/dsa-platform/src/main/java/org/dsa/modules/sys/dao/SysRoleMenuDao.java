package org.dsa.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.sys.entity.SysMenuEntity;
import org.dsa.modules.sys.entity.SysRoleMenuEntity;

import java.util.List;

/**
 * 角色与菜单对应关系
 *
 */
@Mapper
public interface SysRoleMenuDao extends BaseMapper<SysRoleMenuEntity> {
	
	/**
	 * 根据角色ID，获取菜单ID列表
	 */
	List<Long> queryMenuIdList(@Param("value") Long roleId);

	/**
	 * 根据角色ID数组，批量删除
	 */
	int deleteBatch(Long[] roleIds);

	/**
	 * 根据角色ID，获取菜单ID列表
	 */
    List<Long> queryRoleMenu(@Param("roleList") List<String> roleList, @Param("owningSystem") Integer owningSystem);

	/**
	 * 根据角色ID，获取菜单信息列表
	 */
    List<SysMenuEntity> queryRoleMenuEntity(@Param("roleList") List<String> roleList, @Param("owningSystem") Integer owningSystem);
	/**
	 * 获取菜单信息列表
	 */
    List<SysMenuEntity> queryAllMenuEntity();

    List<String> queryMenuNameList(@Param("roleName") String roleName);
}
