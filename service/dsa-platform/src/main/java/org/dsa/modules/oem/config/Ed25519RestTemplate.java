package org.dsa.modules.oem.config;

import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManagerBuilder;
import org.apache.hc.client5.http.io.HttpClientConnectionManager;
import org.apache.hc.client5.http.ssl.SSLConnectionSocketFactory;
import org.apache.hc.client5.http.ssl.SSLConnectionSocketFactoryBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.dsa.common.utils.SSLTrustManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;


import javax.net.ssl.*;
import java.io.FileInputStream;
import java.net.Socket;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.io.IOException;
import java.security.cert.X509Certificate;

//@Configuration
public class Ed25519RestTemplate {

    private static final String KEYSTORE_PATH = "/usr/file/ssl/client.p12";
    private static final String KEYSTORE_PASSWORD = "XW50#N6s";
    private static final String KEY_ALIAS = "1";

    public static RestTemplate createEd25519RestTemplate() throws Exception {
        System.setProperty("jdk.tls.client.protocols", "TLSv1.2");

        KeyStore keyStore = KeyStore.getInstance("PKCS12");
        try (FileInputStream fis = new FileInputStream(KEYSTORE_PATH)) {
            keyStore.load(fis, KEYSTORE_PASSWORD.toCharArray());
        }

        KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
        kmf.init(keyStore, KEYSTORE_PASSWORD.toCharArray()); // 使用密码
        KeyManager[] keyManagers = kmf.getKeyManagers();

        TrustManager tm = new SSLTrustManager();
        TrustManager[] trustAllCerts = new TrustManager[]{tm};


        // Create and configure SSL context with custom KeyManager
        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(keyManagers, trustAllCerts, new SecureRandom());
        // 从上述SSLContext对象中得到SSLSocketFactory对象
        SSLSocketFactory ssf = sslContext.getSocketFactory();
        // Create a custom ClientHttpRequestFactory with the SSL context
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
//        CloseableHttpClient build = HttpClients.custom().setSslcontext(sslContext).build();

        SSLConnectionSocketFactory sslSocketFactory = SSLConnectionSocketFactoryBuilder.create()
                .setSslContext(sslContext).build();
        HttpClientConnectionManager cm = PoolingHttpClientConnectionManagerBuilder.create()
                .setSSLSocketFactory(sslSocketFactory).build();

        requestFactory.setHttpClient(HttpClients.custom().setConnectionManager(cm).build());

        // Create the RestTemplate and set the custom request factory
        RestTemplate restTemplate = new RestTemplate(requestFactory);

        return restTemplate;
    }
//
//    public static KeyPair loadKeyPairFromPKCS12(String keystorePath, char[] keystorePassword, String keyAlias)
//            throws KeyStoreException, CertificateException, NoSuchAlgorithmException, UnrecoverableKeyException, IOException, InvalidKeyException {
//        KeyStore keyStore = KeyStore.getInstance("PKCS12");
//        try (FileInputStream fis = new FileInputStream(keystorePath)) {
//            keyStore.load(fis, keystorePassword);
//        }
//        kmf = KeyManagerFactory.getInstance("SunX509");
//        kmf.init(keyStore, keystorePassword); // 使用密码
//
//        Certificate cert = keyStore.getCertificate(keyAlias);
//        PrivateKey privateKey = (PrivateKey) keyStore.getKey(keyAlias, keystorePassword);
//        System.out.println("private key ----> " + privateKey);
//        PublicKey publicKey = cert.getPublicKey();
//        System.out.println("private key ----> " + publicKey);
//
//        return new KeyPair(publicKey, privateKey);
//    }
//
//    public static SSLContext createSSLContext(KeyPair keyPair) throws NoSuchAlgorithmException, KeyManagementException {
//        // Set up a custom TrustManager that trusts any certificate (only for demo purposes)
//        TrustManager[] trustAllCertificates = new TrustManager[]{
//                new X509TrustManager() {
//                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
//                        return new java.security.cert.X509Certificate[0];
//                    }
//
//                    public void checkClientTrusted(
//                            java.security.cert.X509Certificate[] certs, String authType) {
//                    }
//
//                    public void checkServerTrusted(
//                            java.security.cert.X509Certificate[] certs, String authType) {
//                    }
//                }
//        };
//
//
//        // Create and initialize SSL context
//        SSLContext sslContext = SSLContext.getInstance("TLS");
//        sslContext.init(null, trustAllCertificates, new SecureRandom());
//
//        return sslContext;
//    }
}
