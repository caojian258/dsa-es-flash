package org.dsa.modules.diagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.diagnostic.dto.FlashActionDto;
import org.dsa.modules.diagnostic.entity.*;

import java.util.List;
import java.util.Set;

/**
 * 车辆诊断命令表
 * 
 */
@Mapper
public interface DActionInfoDao extends BaseMapper<DActionInfoEntity> {

	/**
	 * 根据actionId修改状态
	 * @param actionId,results
	 */
    void updateByActionId(@Param("actionId") String actionId, @Param("results")String results,@Param("timeout")int timeout);

	/**
	 * 根据 actionId , functionCode,actionCode 获取 actionInfo
	 * @param actionId,functionCode,actionCode
	 */
    DActionInfoEntity selectByActionIdAndCode(@Param("actionId") String actionId, @Param("functionCode") String functionCode, @Param("actionCode") String actionCode);

	/**
	 * 根据 sessionId查询操作记录
	 * @param sessionId
	 */
    List<DActionInfoEntity> selectListBySessionId(@Param("sessionId") String sessionId);

	/**
	 * 诊断功能分析 - 分页
	 */
	IPage<AnalysisReqDataEntity> queryReqAnalysisPage(IPage<AnalysisDtcDataEntity> page
			,@Param("sources") List<String> sources
			,@Param("vehicleTypeIds")  List<String> vehicleTypeIds
			,@Param("noDtcSet") Set<String> noDtcSet
			,@Param("dtcSet") Set<String> dtcSet
			,@Param("actionCodes")  Set<String> actionCodes
			,@Param("vin")  String vin
			,@Param("startTime")  String startTime
			,@Param("endTime")  String endTime);
	/**
	 * 诊断功能分析 - 饼图
	 */
	List<AnalysisReqPirEntity> selectReqAnalysisPir(@Param("analysisEntity") AnalysisEntity analysisEntity,@Param("functions") String[] funtions);
	/**
	 * 诊断功能分析 - 柱图
	 */
	List<AnalysisReqBarEntity> selectReqAnalysisBar(AnalysisEntity analysisEntity);


	List<FlashActionDto> selectFlashListBySessionId(@Param("sessionId") String sessionId, @Param("functionCode") List<String>functions);
}
