package org.dsa.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.dsa.modules.sys.dao.SysLocalesDao;
import org.dsa.modules.sys.dao.SysTranslationsDao;
import org.dsa.modules.sys.dto.SysTranslationsDto;
import org.dsa.modules.sys.entity.SysLocales;
import org.dsa.modules.sys.service.SysTranslationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SysTranslationsServiceImpl implements SysTranslationsService {

    @Autowired
    SysTranslationsDao translationsDao;

    @Autowired
    SysLocalesDao localesDao;

    @Override
    public Map<String, String> translationTis(List<String> tis, String locale) {
        QueryWrapper<SysLocales> qw = new QueryWrapper<>();
        qw.eq("locale", locale);
        qw.last("limit 1");
        Integer localeInt = 9;
        SysLocales sysLocale = localesDao.selectOne(qw);
        if(null != sysLocale){
            localeInt = sysLocale.getId();
        }
        List<SysTranslationsDto> tsList =  translationsDao.queryTranslationsByLocale(localeInt, tis);
        Map<String, String> trsMap = new HashMap<>(tsList.size());
        for (SysTranslationsDto dto: tsList ) {
            trsMap.put(dto.getTi(),dto.getText());
        }
        return trsMap;
    }

    @Override
    public String translationTi(String ti, String locale) {
        QueryWrapper<SysLocales> qw = new QueryWrapper<>();
        qw.eq("locale", locale);
        qw.last("limit 1");
        Integer localeInt = 9;
        SysLocales sysLocale = localesDao.selectOne(qw);
        if(null != sysLocale){
            localeInt = sysLocale.getId();
        }
        return  translationsDao.queryTranslationByLocale(localeInt, ti);
    }
}
