package org.dsa.modules.reporter.vo.doc;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import org.dsa.modules.reporter.document.ElasticEntity;

import java.util.Date;

@Data
public class DiagnosticEcuDocVo extends ElasticEntity {

    private String sessionInfoId;

    private String sessionId;

    private String vin;

    private String ecuName;

    private Integer status;

    private Integer dtcCount;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}
