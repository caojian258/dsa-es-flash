package org.dsa.modules.diagnostic.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 故障码统计 - 饼状图
 * 
 */
@Data
public class AnalysisDtcDataEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 故障码
	 */
	private String dtcCodeHex;

	/**
	 * 描述信息
	 */
	private String description;

	/**
	 * 故障数
	 */
	private String dtcDate;

	/**
	 * 故障数
	 */
	private int dtcCount;

	/**
	 * 控制器名称
	 */
	private String ecuName;

	/**
	 * ti,翻译库key
	 */
	private String ti;

	/**
	 * actionId
	 */
	private String actionId;

}
