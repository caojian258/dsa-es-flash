package org.dsa.modules.reporter.service.impl;

import org.dsa.modules.reporter.dao.EcuVersionHistoryMapper;
import org.dsa.modules.reporter.dao.EcuVersionOverviewMapper;
import org.dsa.modules.reporter.dao.VehicleVersionHistoryMapper;
import org.dsa.modules.reporter.dao.VehicleVersionOverviewMapper;
import org.dsa.modules.reporter.entity.EcuVersionHistory;
import org.dsa.modules.reporter.entity.EcuVersionOverview;
import org.dsa.modules.reporter.entity.VehicleVersionHistory;
import org.dsa.modules.reporter.entity.VehicleVersionOverview;
import org.dsa.modules.reporter.service.VehicleEcuVersionService;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;

@Service
public class VehicleEcuVersionServiceImpl implements VehicleEcuVersionService {

    @Resource
    EcuVersionHistoryMapper ecuVersionHistoryMapper;

    @Resource
    EcuVersionOverviewMapper ecuVersionOverviewMapper;

    @Resource
    VehicleVersionOverviewMapper vehicleVersionOverviewMapper;

    @Resource
    VehicleVersionHistoryMapper vehicleVersionHistoryMapper;


    @Override
    public void addEcuOverview(EcuVersionOverview version) {
        ecuVersionOverviewMapper.insert(version);
    }

    @Override
    public void addEcuHistory(EcuVersionHistory version) {
        ecuVersionHistoryMapper.insert(version);
    }

    @Override
    public void addVehicleOverview(VehicleVersionOverview vehicle) {
        vehicleVersionOverviewMapper.insert(vehicle);
    }

    @Override
    public void addVehicleHistory(VehicleVersionHistory version) {
        vehicleVersionHistoryMapper.insert(version);
    }

    @Override
    public void syncEcuHistoryDataFromSession(String sessionId) {
        ecuVersionHistoryMapper.syncSessionEcuVersion(sessionId);
    }

    @Override
    public Boolean hasEcuHistoryDataFromSession(String sessionId) {
        Boolean flag = false;
        EcuVersionHistory version = ecuVersionHistoryMapper.checkSessionEcuData(sessionId);
        if(null != version){
            flag = true;
        }
        return flag;
    }


    @Override
    public void syncEcuOverviewDataFromSession(String sessionId) {
        ecuVersionOverviewMapper.syncSessionEcuVersion(sessionId);
    }

    @Override
    public Boolean hasEcuOverviewDataFromSession(String sessionId) {
        boolean flag = false;
        EcuVersionOverview v = ecuVersionOverviewMapper.checkEcuVersionOverviewBySession(sessionId);
        if(null == v){
            flag = true;
        }
        return flag;
    }

    @Override
    public Boolean hasVehicleEcuOverviewData(String vin) {
        boolean flag = false;
        EcuVersionOverview v = ecuVersionOverviewMapper.checkEcuVersionOverviewByVin(vin);
        if(null == v){
            flag = true;
        }
        return flag;
    }


    @Override
    public Boolean hasEcuOverviewData(String vin, String ecuName, String versionName) {
        boolean flag = false;
        EcuVersionOverview v = ecuVersionOverviewMapper.checkEcuVersionOverview(vin, ecuName, versionName);
        if(null == v){
            flag = true;
        }
        return flag;
    }


}
