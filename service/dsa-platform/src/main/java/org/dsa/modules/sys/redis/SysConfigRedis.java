package org.dsa.modules.sys.redis;


import org.dsa.common.utils.RedisKeys;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.sys.entity.SysConfigEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 系统配置Redis
 */
@Component
public class SysConfigRedis {
    @Autowired
    private RedisUtils redisUtils;

    public void saveOrUpdate(SysConfigEntity config) {
        if (config == null) {
            return;
        }
        String key = RedisKeys.getSysConfigKey(config.getParamKey());
        redisUtils.set(key, config);
    }

    public void delete(String configKey) {
        String key = RedisKeys.getSysConfigKey(configKey);
        redisUtils.delete(key);
    }

    public SysConfigEntity get(String configKey) {
        String key = RedisKeys.getSysConfigKey(configKey);
        return redisUtils.get(key, SysConfigEntity.class);
    }


    public void saveGroup(String list, String group) {

        String key = RedisKeys.getSysConfigGroupKey(group);
        redisUtils.set(key, list);
    }

    public String getGroup(String configKey) {
        String key = RedisKeys.getSysConfigGroupKey(configKey);
        return redisUtils.get(key);
    }

    public void deleteGroup(String configKey) {
        String key = RedisKeys.getSysConfigGroupKey(configKey);
        redisUtils.delete(key);
    }

    public boolean hasGroup(String configKey) {
        String key = RedisKeys.getSysConfigGroupKey(configKey);
        return redisUtils.hasKey(key);
    }

    public void deleteByKey(String key) {
        redisUtils.delete(key);
    }
}
