package org.dsa.modules.diagnostic.controller;

import org.dsa.common.utils.R;
import org.dsa.modules.diagnostic.service.SynchronizedService;
import org.dsa.modules.sys.controller.AbstractController;
import org.dsa.modules.vehicle.entity.VehicleInfoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 诊断功能
 *
 */
@RestController
@RequestMapping("syn")
public class SynController extends AbstractController {
	@Autowired
	private SynchronizedService synchronizedService;

	/**
	 * TSP向远程诊断仪同步车辆信息
	 */
	@RequestMapping("/vehicleInfo")
	public R vehicleInfo(@RequestBody List<VehicleInfoEntity> list){
		synchronizedService.save(list);
		return R.ok();
	}


	/**
	 * 同步redis信息
	 */
	@RequestMapping("/synRedis")
	public R synRedis(){
		synchronizedService.synRedis();
		return R.ok();
	}

}
