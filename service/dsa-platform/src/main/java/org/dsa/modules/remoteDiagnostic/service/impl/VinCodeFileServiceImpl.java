package org.dsa.modules.remoteDiagnostic.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import com.alibaba.excel.EasyExcel;
import lombok.extern.slf4j.Slf4j;
import org.dsa.modules.remoteDiagnostic.config.RemoteDiagnosticConfig;
import org.dsa.modules.remoteDiagnostic.listener.VinCodeExcelImportListener;
import org.dsa.modules.remoteDiagnostic.service.VinCodeFileService;
import org.dsa.modules.remoteDiagnostic.vo.VinCodePageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.scripting.support.ResourceScriptSource;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class VinCodeFileServiceImpl implements VinCodeFileService {
    @Autowired
    RedisTemplate<String, Object> redisTemplate;

    @Override
    public void readExcel(File file,String key) {
        TimeInterval timer = DateUtil.timer();

        EasyExcel.read(file, new VinCodeExcelImportListener("/usr/file",key)).sheet().doRead();

        if (file.exists()) {
            boolean delete = file.delete();
        }
        log.info("导入数据用时 {}",timer.intervalRestart());
    }

    @Override
    public Boolean clearKey(String key) {
//        return redisUtils.deleteKey(key);
        return true;
    }

    @Override
    public List<Object> getDataByKey(VinCodePageVo vo) {
//        DefaultRedisScript<List> redisScript = new DefaultRedisScript<>();
//        redisScript.setResultType(List.class);
//        redisScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("redis/vin_code.lua")));
//        List<String> list1 = new ArrayList<>();
//        list1.add(vo.getKey());
//        List<String> list = redisTemplate.opsForZSet().getOperations().execute(redisScript, list1,vo.getVin());
//
//        System.out.println(list);

        return null;
    }
}
