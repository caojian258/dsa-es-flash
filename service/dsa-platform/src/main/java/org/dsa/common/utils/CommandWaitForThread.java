package org.dsa.common.utils;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CommandWaitForThread extends Thread{

    private Process process;

    private boolean finish = false;

    private int exitValue = -1;

    public CommandWaitForThread(Process process) {
        this.process = process;
    }

    public void run() {
        try {
            this.exitValue = process.waitFor();
        } catch (InterruptedException e) {
            log.error("正式执行命令异常：{}", JSONObject.toJSONString(e));
        } finally {
            finish = true;
        }
    }

    public boolean isFinish() {
        return finish;
    }

    public void setFinish(boolean finish) {
        this.finish = finish;
    }

    public int getExitValue() {
        return exitValue;
    }
}
