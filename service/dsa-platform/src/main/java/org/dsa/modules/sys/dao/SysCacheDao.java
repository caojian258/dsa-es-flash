package org.dsa.modules.sys.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.dsa.modules.sys.entity.SysCacheEntity;
import org.dsa.modules.sys.entity.SysLogEntity;

/**
 * 系统日志
 *
 */
@Mapper
public interface SysCacheDao extends BaseMapper<SysCacheEntity> {

    @Select("select value from sys_cache where key = #{key} ")
    String getValueByKey(@Param("key") String key);

    @Select("insert into sys_cache values(#{key},#{value}) ON conflict(key) " +
            "DO UPDATE SET value = EXCLUDED.value ")
    String saveOrUpdate(@Param("key") String key,@Param("value") String value);
}
