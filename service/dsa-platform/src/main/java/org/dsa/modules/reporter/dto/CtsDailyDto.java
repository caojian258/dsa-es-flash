package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CtsDailyDto implements Serializable {
    /*
     "ctsName": "IDENTIFICATION-read",
             "functionCode": "IDENTIFICATION",
             "actionCode": "read",
             "ctsDay": "2023-06-20",
             "ctsNum": 1
    */

    private String ctsName;

    private String functionCode;

    private String actionCode;

    private String ctsDay;

    private Long ctsTotal;
}
