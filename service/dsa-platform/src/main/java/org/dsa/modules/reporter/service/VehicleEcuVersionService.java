package org.dsa.modules.reporter.service;

import org.dsa.modules.reporter.entity.EcuVersionHistory;
import org.dsa.modules.reporter.entity.EcuVersionOverview;
import org.dsa.modules.reporter.entity.VehicleVersionHistory;
import org.dsa.modules.reporter.entity.VehicleVersionOverview;

public interface VehicleEcuVersionService {

    public void addEcuOverview(EcuVersionOverview version);

    public void addEcuHistory(EcuVersionHistory version);

    public void addVehicleOverview(VehicleVersionOverview vehicle);

    public void addVehicleHistory(VehicleVersionHistory version);

    public void syncEcuHistoryDataFromSession(String sessionId);

    public Boolean hasEcuHistoryDataFromSession(String sessionId);

    public void syncEcuOverviewDataFromSession(String sessionId);

    public Boolean hasEcuOverviewDataFromSession(String sessionId);

    public Boolean hasVehicleEcuOverviewData(String vin);

    public Boolean hasEcuOverviewData(String vin, String ecuName, String versionName);


}
