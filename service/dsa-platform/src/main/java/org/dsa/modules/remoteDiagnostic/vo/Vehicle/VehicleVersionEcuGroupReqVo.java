package org.dsa.modules.remoteDiagnostic.vo.Vehicle;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class VehicleVersionEcuGroupReqVo {

    private List<Long> ids;

    private Long versionId;

    private Long typeId;
}
