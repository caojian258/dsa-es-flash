package org.dsa.modules.reporter.vo.diag;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.modules.reporter.pojo.PageParam;

import java.util.List;


@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class DiagnosticPageReVo extends PageParam {

    private List<String> dates;

    private String vin;

    private List<Long> vehicleTypeIds;

    private String dtc;

    private Integer top;

    private String ctsName;

    private String ecu;

    private Integer userId;

    private String startTime;

    private String endTime;

    private List<String> sources;
}
