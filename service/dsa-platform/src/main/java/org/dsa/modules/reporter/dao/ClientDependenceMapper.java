package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.dsa.modules.reporter.dto.ClientDependencyDto;
import org.dsa.modules.reporter.entity.ClientDependence;

import java.util.List;

@Mapper
public interface ClientDependenceMapper extends BaseMapper<ClientDependence> {

    public List<ClientDependencyDto> queryDependencyData(String tag);

    public List<ClientDependence> queryByTag(String tag);
}
