package org.dsa.modules.remoteDiagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;
import org.dsa.modules.remoteDiagnostic.dto.api.DiagnosticTaskDto;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskCampaignEntity;

import java.util.List;

@Mapper
public interface DiagnosticTaskCampaignDao extends BaseMapper<DiagnosticTaskCampaignEntity> {


    @Select("select i.*,i.task_id as taskId from" +
            " r_diag_task_campaign i left join r_diag_task j on i.task_id = j.id  where i.status = 0 and (now() between i.start_time and i.end_time) and i.pool_id is not null")
    @Results({
            @Result(property = "conditionList", column = "taskId",
                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.DiagnosticConditionCopyDao.getConditionList")),
            @Result(property = "funList", column = "taskId",
                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.DiagnosticTaskEcuDao.selectFunListByApi")),
            @Result(property = "file", column = "taskId",
                    one = @One(select = "org.dsa.modules.remoteDiagnostic.dao.DiagnosticFileDao.getDtoByTask")),
    })
    public List<DiagnosticTaskDto> getDiagnosticTaskList();

    @Select("<script>" +
            "  select i.*,i.task_id as taskId from" +
            "        r_diag_task_campaign i left join r_diag_task j on i.task_id = j.id" +
            "        where i.status = 0 and (now() between i.start_time and i.end_time) and i.pool_id is not null" +
            "        <if test='list!=null and list.size!=0 '>" +
            "            and i.id in" +
            "            <foreach collection='list' item='item' open=\"(\" separator=\",\" close=\")\">" +
            "                #{item.id}" +
            "            </foreach>" +
            "        </if>"+
            "</script>")
   @Results({
            @Result(property = "conditionList", column = "taskId",
                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.DiagnosticConditionCopyDao.getConditionList")),
            @Result(property = "funList", column = "taskId",
                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.DiagnosticTaskEcuDao.selectFunListByApi")),
            @Result(property = "file", column = "taskId",
                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.DiagnosticFileDao.getDtoByTask")),
    })
    // 查询任务记录 并进行封装
    public List<DiagnosticTaskDto> getDiagnosticTasksByIds(@Param("list") List<DiagnosticTaskCampaignEntity> list);

    @Select("select * from r_diag_task_campaign i where i.id = #{campaignId} ")
    public DiagnosticTaskCampaignEntity getInfo(@Param("campaignId") Long campaignId);

}
