package org.dsa.modules.sys.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.sys.entity.SysLocaleEntity;

import java.util.List;

/**
 * 数据字典
 *
 */
@Mapper
public interface SysLocaleDao{
    /**
     * 根据语言类型获取语种id
     */
    Integer getIdByLocale(String locale);

    /**
     * 根据语种id获取语言类型
     */
    String getLocaleById(Integer id);

    /**
     * 清空表
     * @return
     */
    Integer truncateTable();

    /**
     * 插入数据
     */
    Integer insert(SysLocaleEntity entity);

    List<SysLocaleEntity> listAll();

    String getContentByKeyAndLocale(@Param("key") String key,@Param("locale") String locale);
}
