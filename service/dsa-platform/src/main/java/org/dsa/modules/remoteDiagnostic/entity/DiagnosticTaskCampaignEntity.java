package org.dsa.modules.remoteDiagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;
import org.dsa.common.handler.JsonTypeHandler;
import org.dsa.modules.remoteDiagnostic.dto.TaskGradientDto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@ToString
@TableName(value = "r_diag_task_campaign", autoResultMap = true)
public class DiagnosticTaskCampaignEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;

    private String campaignName;

    private Long poolId;

    private Long taskId;

    private Date startTime;

    private Date endTime;

    private String model;

    private Integer failedAttemptsNum;

    private Integer failedNum;

    private Integer executionCycle;

    private Long executionInterval;

    private String priority;

    @TableField(typeHandler = JsonTypeHandler.class)
    private List<TaskGradientDto> gradient;


//    private Integer gradient;
//
//    private Integer gradientCondition;
//
//    private Integer gradientValue;

    private String descriptionTi;

    private Integer status;

    private Date createdAt;

    private Date updatedAt;

    @TableField(exist = false)
    List<String> taskTime;

    @TableField(exist = false)
    private RemoteVehiclePoolEntity pool;


}
