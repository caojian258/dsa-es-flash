package org.dsa.modules.sys.vo;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.io.Serializable;

@Data
public class LocaleCheckVo implements Serializable {
    /**
     * 条目数量
     */
    @NotNull(message = "大小不能为空")
    private Integer size;

    /***
     * 语言
     */
    @NotNull(message = "语言不能为空")
    private String locale;
}
