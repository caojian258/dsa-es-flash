package org.dsa.modules.reporter.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@TableName("ecu_version_history")
public class EcuVersionHistory extends BaseEntity {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String vin;

    private String vehicleVersion;

    private String ti;

    private String ecuName;

    private String versionName;

    private String versionValue;

    private String sessionId;

    private String tag;

    private Date collectTime;

    private Integer source;

    @TableField(exist = false)
    List<EcuVersionHistory> historyHardwareList;

    @TableField(exist = false)
    List<EcuVersionHistory> historySoftwareList;


}
