package org.dsa.modules.sys.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class FileEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 文件名
     */
    private String name;

    private String realName;

    /**
     * 文件路径
     */
    private String url;

    /**
     * 文件大小 单位:k
     */
    private Long fileSize;

    /**
     * 文件Md5
     */
    private String fileMd5;

}
