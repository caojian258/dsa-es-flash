package org.dsa.modules.sys.vo.workshop;

import lombok.Data;

import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
public class WorkshopVo implements Serializable {

    private String id;

    private String code;

    private String shortName;

    @NotNull(message = "经销商名称不能为空")
    private String name;

    private String address;

    @NotNull(message = "经销商地区不能为空")
    private List<Long> location;

    private String region;

    private String note;
}
