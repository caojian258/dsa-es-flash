package org.dsa.config;

import ch.qos.logback.classic.sift.SiftingAppender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.LocalDateTime;

@Configuration
@EnableScheduling
public class ScheduleTask {
    Logger logger = LoggerFactory.getLogger(this.getClass());
/*

    @Scheduled(cron = "0 1 0 * * ?") // 每天凌晨0点 1分执行一次
    private void restLogAppenderMapTasks() {
        logger.info("执行logback 动态日志 appentder Map 重置 任务时间: " + LocalDateTime.now());
    }
*/

}
