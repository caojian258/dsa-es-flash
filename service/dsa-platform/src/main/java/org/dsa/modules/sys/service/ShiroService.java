package org.dsa.modules.sys.service;

import org.dsa.modules.sys.entity.SysUserEntity;
import org.dsa.modules.sys.entity.SysUserTokenEntity;

import java.util.List;
import java.util.Set;

/**
 * shiro相关接口
 *
 */
public interface ShiroService {
    /**
     * 获取用户权限列表
     */
    Set<String> getUserPermissions(long userId);

    SysUserTokenEntity queryByToken(String token);

    /**
     * 根据用户ID，查询用户
     * @param userId
     */
    SysUserEntity queryUser(Long userId);

    /**
     * 获取角色权限列表
     */
    Set<String> getPermissionsByRole(List<String> roles, Integer owningSystem);
}
