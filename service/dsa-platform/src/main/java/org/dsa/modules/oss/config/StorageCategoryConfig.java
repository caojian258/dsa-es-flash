package org.dsa.modules.oss.config;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
public class StorageCategoryConfig implements Serializable {

    /***
     * 类型 local remote
     */
    private String type;

    /***
     * 不同类型的工作目录
     */
    private Map<String, String> workDirs;

}
