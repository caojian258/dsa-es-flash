package org.dsa.modules.reporter.document;


import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;


@Data
public class ElasticEntity implements Serializable {

    private String id;

    private String tag;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date createdAt;


    public String genIndexName(String pattern, Date time){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        return pattern+"-"+sdf.format(time);
    }
}
