package org.dsa.modules.reporter.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("vehicle_version_history")
public class VehicleVersionHistory extends BaseEntity{

    @TableId(type = IdType.AUTO)
    private Long id;

    private String vin;

    private String version;

    private String sessionId;

    private String tag;

    private Date collectTime;

    //1 as maintained 0 as build
    private Integer source;
}
