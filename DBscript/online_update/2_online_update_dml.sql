--初始化在线升级

--初始化菜单
--INSERT INTO public.sys_menu (parent_id, "name", url, perms, "type", icon, order_num, query) VALUES(0, 'OnlineUpdate', ' ', '', 0, 'zhedie', 0, NULL);
--INSERT INTO public.sys_menu (parent_id, "name", url, perms, "type", icon, order_num, query)
--VALUES((select menu_id from sys_menu sm where sm."name"='OnlineUpdate'), 'Component', 'software/component', '', 1, 'zhedie', 0, NULL);
--INSERT INTO public.sys_menu (parent_id, "name", url, perms, "type", icon, order_num, query)
--VALUES((select menu_id from sys_menu sm where sm."name"='OnlineUpdate'), 'Software', 'software/software', NULL, 1, 'zhedie', 0, NULL);
--INSERT INTO public.sys_menu (parent_id, "name", url, perms, "type", icon, order_num, query)
--VALUES((select menu_id from sys_menu sm where sm."name"='OnlineUpdate'), 'OemSoftwareVersion', 'software/oemSoftwareList', '', 1, 'zhedie', 0, NULL);
--INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query) VALUES(95, 84, 'rules', '/software/rule', '', 1, 'suoding', 0, NULL);
----初始化角色
--INSERT INTO public.sys_role (role_name, remark, create_user_id, create_time) VALUES('OnlineUpdateRole', '在线升级', 0, '2023-03-13 17:21:51.161');
----初始化用户
--INSERT INTO public.sys_user
--(username, chinese_name, english_name, emp_no, workshop_id, workshop_name, "password", salt, email, mobile, status, create_user_id, create_time, update_time)
--VALUES('qzq', 'qzq', 'qzq', '10000', 6, '陆家嘴4S店', 'efbcb5908b50dd7ee25d47b09b5add10cbd9a8ee3873f8cb8e3945194cb39e28', 'ffWMh3prwQvdD5Cq53xi', 'shanghaidsa@dsa.com', '12345678911', 1, 0, '2023-03-02 11:14:29.884', NULL)
--on conflict(username) do update set user_id=((select max(user_id) from sys_user)+1);
--用户关联角色
--insert into sys_user_role (user_id,role_id)values(select user_id from sys_user where "name" = 'qzq',select role_id from sys_role where role_name = 'OnlineUpdateRole');

--初始化基础数据
--组件数据

INSERT INTO public.o_component (code,"name",status,description_ti,created_at,updated_at,created_user_id,updated_user_id) VALUES
	 (201,'Mcd-Kernel',0,NULL,'2023-08-18 10:03:23.113787',NULL,44,NULL),
	 (202,'SWTS-CORE',0,NULL,'2023-08-18 10:03:23.124768',NULL,44,NULL),
	 (203,'SWTS-HMI',0,NULL,'2023-08-18 10:03:23.138776',NULL,44,NULL),
	 (200,'Pdu-Api',0,'','2023-08-18 10:03:23.099815','2023-08-18 10:04:29.266',44,0);
	 	 
INSERT INTO public.o_component_version (component_id,version_name,update_type,version_type,base_version_id,status,url,file_size,file_md5,release_note_ti,description_ti,created_at,updated_at,created_user_id,updated_user_id) VALUES
	 (2,'V1.0',1,0,NULL,2,'/usr/file/componentVersion/20230818101616_MCDKernel_3.18.0_setup.exe',11170750,'57ab959c0c54ba95e6176f49296b91c8','基础诊断功能组件','','2023-08-18 10:16:20.967','2023-08-18 10:16:48.755',44,44),
	 (3,'V1.0',1,0,NULL,2,'/usr/file/componentVersion/20230818101726_SWTS_CORE_1.0_setup.exe',113326626,'6edf3d7984a48ce2cd70eda4bc005027','基础诊断功能组件','','2023-08-18 10:17:59.981',NULL,44,NULL),
	 (4,'V1.0',1,0,NULL,2,'/usr/file/componentVersion/20230818101929_SWTS_HMI_V1.0_setup.exe',67209527,'45d05c17ca3a62595c233b9fdd697a3b','基础诊断功能组件','','2023-08-18 10:19:34.082',NULL,44,NULL),
	 (1,'V1.0',1,0,NULL,2,'/usr/file/componentVersion/20230818101427_pduapi_1.18_setup.exe',2959341,'a5e4c986aa5ef01c698abb7791c8b824','基础诊断功能组件','','2023-08-18 10:17:12.04','2023-08-18 10:26:04.279',0,0);
	 
INSERT INTO public.o_comp_v_group (group_id,component_id,component_version_id) VALUES
	 ('65B8C356-6AFA-41B6-9EE5-B740FFDD7A41',4,4),
	 ('65B8C356-6AFA-41B6-9EE5-B740FFDD7A41',3,3),
	 ('65B8C356-6AFA-41B6-9EE5-B740FFDD7A41',2,1),
	 ('65B8C356-6AFA-41B6-9EE5-B740FFDD7A41',1,2);

INSERT INTO public.o_software (code,"name",status,description_ti,created_at,updated_at,created_user_id,updated_user_id) VALUES
	 (100,'PROD.SWTS',0,'','2023-08-18 10:15:00.355','2023-08-18 10:23:46.245',44,0);

INSERT INTO public.o_software_version (software_id,version_name,version_type,base_version_id,update_type,status,last_update_time,last_update_tip_days,is_cross,release_note_ti,created_at,updated_at,created_user_id,updated_user_id) VALUES
	 (1,'V1.0',0,NULL,1,2,'2023-08-31 00:00:00',14,0,'First Version：base diagnostic version','2023-08-18 10:26:44.889','2023-08-18 10:27:27.243',0,0);
	 
INSERT INTO public.o_sw_v_comp_v (component_id,component_version_id,software_id,software_version_id) VALUES
	 (4,4,1,1),
	 (3,3,1,1),
	 (2,1,1,1),
	 (1,2,1,1);
	 
INSERT INTO public.o_software_entry (software_id,component_id) VALUES
	 (1,4);
 
INSERT INTO public.o_rule (software_id,software_version_id,"name",start_time,end_time,status,created_user_id,created_at,updated_user_id,updated_at) VALUES
	 (1,1,'PROD.SWTS-08181028-V1.0','2023-08-18 00:00:00','2023-09-30 23:59:59',0,0,'2023-08-18 10:29:03.555',NULL,NULL);

INSERT INTO public.o_rule_user (rule_id,user_id) VALUES
	 (1,44),
	 (1,119),
	 (1,120);






-- 插入user_license信息
INSERT INTO public.t_user_license
(user_id, app_code, app_name, app_name_ti, oem_code, oem_name, oem_name_ti, product_code, product_name, product_name_ti, license_type_code, license_type_name, license_type_name_ti, user_name)
VALUES((select user_id from sys_user where username = 'qzq'), 'szo7v4ury070', '离线诊断仪', NULL, '5k9wckuqz36u', '集度', NULL, '06jwg71vr2t2', 'JTester', NULL, '1mkrbelgsfbv', '网络狗', NULL, 'qzq');




-- 更新序列
select setval('o_component_id_seq', (select max(i.id)+1 from o_component i));
select setval('o_component_version_id_seq', (select max(i.id)+1 from o_component_version i));
select setval('o_comp_v_group_id_seq', (select max(i.id)+1 from o_comp_v_group i));
select setval('o_software_id_seq', (select max(i.id)+1 from o_software i));
select setval('o_software_version_id_seq', (select max(i.id)+1 from o_software_version i));
select setval('o_sw_v_comp_v_id_seq', (select max(i.id)+1 from o_sw_v_comp_v i));
select setval('o_software_entry_id_seq', (select max(i.id)+1 from o_software_entry i));
select setval('o_rule_id_seq', (select max(i.id)+1 from o_rule i));
select setval('o_rule_user_id_seq', (select max(i.id)+1 from o_rule_user i));
select setval('o_rule_workshop_id_seq', (select max(i.id)+1 from o_rule_workshop i));
select setval('t_user_license_id_seq', (select max(i.id)+1 from t_user_license i));





