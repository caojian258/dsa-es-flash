package org.dsa.modules.reporter.util;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.dsa.modules.reporter.constant.VehicleConstant;
import org.dsa.modules.reporter.dto.VinDto;
import org.dsa.modules.reporter.enums.FlagEnum;


public class ReportUtil {

    public static String sysFlagCheck(Object item) {
        String flag = FlagEnum.UNKNOWN.getValue();
        if(null == item){
            return flag;
        }
        if(item instanceof String){
            if(StrUtil.isNotBlank(item.toString())){
                flag =  FlagEnum.YES.getValue();
            }else{
                flag = FlagEnum.NO.getValue();
            }
        }else if(item instanceof JSONObject){
            if(  ((JSONObject) item).size()>0){
                flag =  FlagEnum.YES.getValue();
            }else{
                flag = FlagEnum.NO.getValue();
            }
        }else if(item instanceof JSONArray){
            if(  ((JSONArray) item).size()>0){
                flag =  FlagEnum.YES.getValue();
            }else{
                flag = FlagEnum.NO.getValue();
            }
        }
        return flag;
    }

    public static String sysWlanCheck(JSONArray wifiJson) {
        String flag = FlagEnum.UNKNOWN.getValue();

        if(null == wifiJson){
            return flag;
        }

        if(wifiJson.size()== 0){
            return flag;
        }

        flag = FlagEnum.NO.getValue();

        for(Object item: wifiJson){
            JSONObject json = (JSONObject)item;
            if(null != json.get("CONNECTED") &&
                    json.get("CONNECTED").equals("true") ){
                flag = FlagEnum.YES.getValue();
            }
        }
        return flag;
    }

    public static String sysFirewallCheck(JSONObject firewallJson) {
        String flag = FlagEnum.UNKNOWN.getValue();
        if(null == firewallJson){
            return flag;
        }

        if(firewallJson.size()== 0){
            flag = FlagEnum.NO.getValue();
            return flag;
        }

        flag = FlagEnum.NO.getValue();
        if((null != firewallJson.get("DOMAIN") &&
                (firewallJson.get("DOMAIN").equals("ON") || firewallJson.get("DOMAIN").equals("开启"))) &&
                (null !=firewallJson.get("PUBLIC") &&
                        (firewallJson.get("PUBLIC").equals("ON") || firewallJson.get("PUBLIC").equals("开启"))) &&
                (null !=firewallJson.get("PRIVATE") &&
                        (firewallJson.get("PRIVATE").equals("ON") || firewallJson.get("PRIVATE").equals("开启")))){
            flag = FlagEnum.YES.getValue();
        }

        return flag;
    }

    public static VinDto splitVinCode(String vinCode){
        VinDto dto = new VinDto();
        dto.setVinCode(vinCode);
        dto.setWmi(vinCode.substring(0, 3));
        dto.setVds(vinCode.substring(3, 9));
        return dto;
    }

    public static String getModelByVinCode(String vinCode){
        String model = VehicleConstant.NV11;
        String tmp = vinCode.substring(5, 7);

        String[] tmps =  VehicleConstant.TYPES.split(",");

        for(String s: tmps){
            if(s.equals(tmp)) {
                model =  VehicleConstant.NV12;
                break;
            }
        }

        return model;
    }
}
