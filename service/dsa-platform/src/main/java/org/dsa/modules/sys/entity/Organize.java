package org.dsa.modules.sys.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Organize implements Serializable {

    protected String id;// 主键 字符串 18
    protected String fullName;// 组织名称 字符串 50
    protected String enCode;// 组织编码 字符串 50
    protected String enFullName;// 组织名称（英文） 字符串 50
    protected String shortName;// 组织简称 字符串 20
    protected String parentId;// 父组织主键 字符串 18
    protected String parentName;// 父组织 字符串 50
    protected String layers;// 组织层级 数字
    protected String categoryId;// 组织类型 字符串 Group 集团
    protected String company;// 公司
    protected String department;// 部门
    protected String workGroup;// 工作组
    protected String nonHR;// 是否虚拟部门 字符串 Y=是，N=否
    protected String managerId;// 部门负责人主键 字符串 18
    protected String manager;// 部门负责人 字符串 50
    protected String chargeVPresident;// 部门分管领导主键 字符串 18
    protected String chargeVPresidentName;// 部门分管领导 字符串 50
    protected Boolean enabledMark;// 有效标志 Bool true:有效，false：无效
    protected Boolean deleteMark;// 删除标志 Bool true:删除，false:未删除
    protected List<Organize> children;// 子级组织 json 数组 为空就表示没有子级


}
