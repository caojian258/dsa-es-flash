package org.dsa.modules.sys.utils;

import java.util.HashMap;
import java.util.Map;

public class InternationalizationContext {

    public static Map<String, String> key = new HashMap<>();
    static {
        key.put("descriptionTi", "description");
//        key.put("short_name_ti", "short_name");
        key.put("releaseNodeTi", "releaseNode");
    }

}
