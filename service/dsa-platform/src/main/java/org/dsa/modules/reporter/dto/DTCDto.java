package org.dsa.modules.reporter.dto;

import cn.hutool.db.DaoTemplate;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class DTCDto implements Serializable {

    private String dtcCode;

    private String ti;

    private String dtcCodeInt;

    private String ecuName;

    private String dtcCodeHex;

    private String description;

    private Date dtcTime;

    private Integer errorCode;

    private String status;
}
