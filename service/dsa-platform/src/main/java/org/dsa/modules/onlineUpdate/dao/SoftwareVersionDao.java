package org.dsa.modules.onlineUpdate.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.onlineUpdate.entity.SoftwareVersionEntity;
import org.dsa.modules.onlineUpdate.vo.SoftwareVersionVo;

import java.util.List;

/**
 * 软件版本
 * 
 */
@Mapper
public interface SoftwareVersionDao extends BaseMapper<SoftwareVersionEntity> {

    List<SoftwareVersionVo> selectListBySoftwareId(Integer softwareId);

    SoftwareVersionEntity getOldestVersionBySoftwareId(@Param("softwareId") Long id);

    Boolean isRelease(@Param("softwareId") Long softwareId);
}
