package org.dsa.modules.reporter.job;

import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.springframework.stereotype.Component;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

/***
 * 监听sessionJob做 DataMinerJob
 */
@Slf4j
@Component
public class SessionLogJobListener implements JobListener {

    @Override
    public String getName() {
        return "SessionJobListener";
    }

    @Override
    public void jobToBeExecuted(JobExecutionContext jobExecutionContext) {

    }

    @Override
    public void jobExecutionVetoed(JobExecutionContext jobExecutionContext) {

    }

    @Override
    public void jobWasExecuted(JobExecutionContext jobExecutionContext, JobExecutionException e) {

        try{
            JobKey jobKey = jobExecutionContext.getJobDetail().getKey();

            String minerJobId = "dataMinerJob" + jobKey.getName();
            String minerTriggerId = "dataMinerTrigger" + jobKey.getName();

            //客户端系统数据处理
            JobDetail dataMinerJob = newJob(SystemDataMinerJob.class).withIdentity(minerJobId).build();
            Trigger dataMinerTrigger = newTrigger().withIdentity(minerTriggerId).startNow().build();

            try {
                jobExecutionContext.getScheduler().scheduleJob(dataMinerJob, dataMinerTrigger);
            }catch (Exception ex){
                log.warn("Unable to schedule DataMinerJob: {}", ex.getMessage());
            }
        }catch (Exception ee){
            log.warn("jobWasExecuted Unable to schedule DataMinerJob: {}", ee.getMessage());
        }

    }
}
