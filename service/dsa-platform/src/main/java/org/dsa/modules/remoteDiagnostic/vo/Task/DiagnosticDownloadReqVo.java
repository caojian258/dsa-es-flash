package org.dsa.modules.remoteDiagnostic.vo.Task;

import lombok.Data;
import lombok.ToString;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.common.validator.group.ReqGroup;
import org.dsa.common.validator.group.UpdateGroup;
import org.hibernate.validator.constraints.Length;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.util.Date;

@Data
@ToString()
public class DiagnosticDownloadReqVo {

    //    @NotBlank(message = "vin is null", groups = {AddGroup.class, UpdateGroup.class, ReqGroup.class})
//    @Length(message = "vin length error", min = 17, max = 17, groups = {AddGroup.class, UpdateGroup.class, ReqGroup.class} )
    private String vin;


    //    @NotBlank(message = "sessionId is null", groups = {UpdateGroup.class, ReqGroup.class})
    private String sessionId;

    private Integer begin;

    private Integer end;

    @NotBlank(message = "md5 is null", groups = {UpdateGroup.class, ReqGroup.class})
    @Length(message = "md5 length error", min = 32, max = 32, groups = {AddGroup.class, UpdateGroup.class, ReqGroup.class})
    private String md5;


}
