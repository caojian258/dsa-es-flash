package org.dsa.modules.reporter.service;

import org.dsa.modules.reporter.dto.AppInstallDto;
import org.dsa.modules.reporter.dto.AppUnInstallDto;
import org.dsa.modules.reporter.dto.AppVersionDto;
import org.dsa.modules.reporter.entity.AppInstall;
import org.dsa.modules.reporter.entity.AppOverview;
import org.dsa.modules.reporter.entity.AppVersion;

import java.util.Date;
import java.util.List;
import java.util.Set;

public interface AppService {

    /***
     * 添加到应用安装记录
     */
    public void addInstallation(AppInstall installation);

    /***
     * 更新应用预览
     * @param overview
     */
    public void saveOverview(AppOverview overview);

    public AppOverview getAppOverview(String workshop, String pcid, String name);

    public AppOverview getAppOverview(String pcid, Integer appId);


    public boolean batchAddInstall(List<AppInstallDto> dtos, String workshop, String pcid, Date timestamp, String tag);

    public boolean batchAddInstall(Set<AppInstallDto> dtos, String workshop, String pcid, Date timestamp, String tag);


    /***
     * 对于已经存在的应用版本更新 不存在就新增
     * @param dtos
     * @param workshop
     * @param pcid
     * @param timestamp
     * @param tag
     */
    public void batchSaveOverview(List<AppVersionDto> dtos, String workshop, String pcid, Date timestamp,String tag);

    public void batchSaveOverview(Set<AppVersionDto> dtos, String workshop, String pcid, Date timestamp,String tag);


    /***
     * 这里要过滤掉 相同批次的设备的重复的版本信息
     * @param dtos
     * @param workshop
     * @param pcid
     * @param timestamp
     * @param tag
     */
    public void batchAddVersion(List<AppVersionDto> dtos, String workshop, String pcid, Date timestamp, String tag);


    public void batchAddVersion(Set<AppVersionDto> dtos, String workshop, String pcid, Date timestamp, String tag);

    public AppOverview getAppOverviewByName(String workshop, String pcid, String name);

    public void addUnInstall(List<AppUnInstallDto> dtos, String workshop, String pcid, Date timestamp, String tag);


    public void addUnInstall(Set<AppUnInstallDto> dtos, String workshop, String pcid, Date timestamp, String tag);


    public AppVersion queryDeviceAppVersionByTag(String pcid, Integer appId, String version, String tag);

}
