package org.dsa.modules.reporter.job;

import com.alibaba.fastjson.JSONObject;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.dsa.common.utils.RedisUtils;
import org.dsa.common.utils.ShellUtil;
import org.dsa.config.SyncConfig;
import org.dsa.modules.reporter.config.ReportConfig;
import org.dsa.modules.reporter.constant.RedisConstant;
import org.dsa.modules.reporter.dto.SessionDto;
import org.dsa.modules.reporter.dto.SessionFileDto;
import org.dsa.modules.reporter.entity.FileLog;
import org.dsa.modules.reporter.enums.FileCateEnum;
import org.dsa.modules.reporter.handler.FileHandler;
import org.dsa.modules.reporter.handler.SessionLogHandler;
import org.dsa.modules.reporter.service.FileLogService;
import org.dsa.modules.reporter.util.FilesUtil;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.nio.file.StandardCopyOption.ATOMIC_MOVE;

/***
 * 诊断会话 处理session log zip 文件
 */
@Slf4j
@Component
@DisallowConcurrentExecution
public class SessionLogJob extends QuartzJobBean {

    @Autowired
    ReportConfig reportConfig;

    @Autowired
    SyncConfig syncConfig;

    @Autowired
    RedisUtils redisUtils;

    @Autowired
    FileHandler fileHandler;

    @Autowired
    FileLogService logService;

    @Resource
    SessionLogHandler sessionLogHandler;


    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        log.info("SessionJob Start................");

        SimpleDateFormat sdf = new SimpleDateFormat(syncConfig.getDirFormat());

        String targetFileDir = syncConfig.getSessionWorkDir() + File.separator+ sdf.format(new Date());

        String archiveDir = syncConfig.getSessionArchiveDir() + File.separator+ sdf.format(new Date());

        List<String> targetFiles = fileHandler.findReadyFiles();

        List<String> archiveFiles = new ArrayList<>();

        if(null !=targetFiles && targetFiles.size()>0){

            for (String path: targetFiles){
                SessionFileDto fileDto = null;

                String key = RedisConstant.FILE_KEY+path;

                String flag = redisUtils.get(key);

                if(!StringUtils.isEmpty(flag)){
                    log.info("SessionJob file {} by the {} thread is processing", path, Thread.currentThread().getName());
                    continue;
                }

                redisUtils.set(key, 1, 180L);
                String tag = FilesUtil.makeTag();

                String fileName = logService.pathToFileName(path);

                Boolean successFlag = logService.checkSuccessed( FileCateEnum.EVENT.getValue(), fileName);
                log.info("Session Job file {} : {}", fileName, successFlag);
                if(successFlag){
//                    logService.failedLog(FileCateEnum.EVENT.getValue(), path, tag, fileName + " has been processed successfully.");
                    redisUtils.delete(key);
                    continue;
                }

                try {
                    fileDto = fileHandler.decompressSwtsSession(path, targetFileDir);

                    logService.succeedLog(FileCateEnum.EVENT.getValue(), path, tag, fileDto.getFileMD5(), fileDto.getFileName());

                    log.info("SessionJob {} file info: {}", fileName, JSONObject.toJSONString(fileDto));

                } catch (Exception e) {
                    log.error("SessionJob decompressSession error: {}", e.getMessage());
                    logService.failedLog(FileCateEnum.EVENT.getValue(), path, tag, e.getMessage());
                    redisUtils.delete(key);
                    continue;
                }

                if(null == fileDto){
                    logService.failedLog(FileCateEnum.EVENT.getValue(), path, tag, "fileDto is null");
                    redisUtils.delete(key);
                    continue;
                }

                if(null == fileDto.getVinCode() || fileDto.getVinCode().length() == 0){
                    logService.failedLog(FileCateEnum.EVENT.getValue(), path, tag, "vinCode is null");
                    redisUtils.delete(key);
                    continue;
                }

                if("UNDEFINED".equals(fileDto.getVinCode())){
                    logService.failedLog(FileCateEnum.EVENT.getValue(), path, tag, "vinCode is UNDEFINED");
                    redisUtils.delete(key);
                    continue;
                }

                if(StringUtils.isEmpty(fileDto.getSqlitePath())){
                    String note = "sqlite file not exist";
                    log.error("SessionJob decompressSession error: {}", note);
                    logService.failedLog(FileCateEnum.EVENT.getValue(), path, tag, note);
                    redisUtils.delete(key);
                    continue;
                }

                ShellUtil shellUtil = null;
                String basePath = fileDto.getSessionDir();
                String cipherPath = fileDto.getSqlitePath();
                String sqlitePath = cipherPath.replace("slog", "sqlite");
                String dbFileName = fileDto.getFileName().replace("slog", "sqlite");
                List<String> cmdList = new ArrayList<>();
                if(ShellUtil.isWindows()) {
                    cmdList.add(reportConfig.getSqlcipherWindowsScript());
                    cmdList.add(cipherPath);
                    cmdList.add(sqlitePath);
                    log.info("path: {} ", JSONObject.toJSONString(cmdList));

                    shellUtil = new ShellUtil(basePath, cmdList);
                    shellUtil.start();
                    log.info("sqlite sqlcipher result:{}", JSONObject.toJSONString(shellUtil.getMessages()));

                }else{
                    cmdList.add(reportConfig.getSqlcipherLinuxScript());
                    cmdList.add(cipherPath);
                    cmdList.add(sqlitePath);
                    shellUtil = new ShellUtil(basePath, cmdList);
                    shellUtil.start();
                    log.info("sqlite sqlcipher linux result:{}", JSONObject.toJSONString(shellUtil.getMessages()));
                }

                //最后处理sqlite文件
                try {

                    log.info("sqlitePath:{}", sqlitePath);
                    log.info("dbFileName:{}", dbFileName);

                    SessionDto result = sessionLogHandler.saveData(sqlitePath, tag,  dbFileName, fileDto.getPcid());

                    logService.insertSessionLog(FileCateEnum.SQLITE.getValue(), fileDto.getSqlitePath(), tag,
                            fileDto.getSessionId(), 1, result.getMsg());

                }catch (Exception ex){
                    log.error("SessionLogJob sqlite error: {}", JSONObject.toJSONString(ex));
                    logService.insertSessionLog(FileCateEnum.SQLITE.getValue(), fileDto.getSqlitePath(), tag,
                            fileDto.getSessionId(), 0, ex.getMessage());
                }

               // delFileFormWorkDir(sqlitePath);

                archiveFiles.add(path);
                redisUtils.delete(key);
                redisUtils.lSet(RedisConstant.PROCESSED_SESSION_KEY, fileDto.getVinCode()+":"+fileDto.getSessionId());
                redisUtils.lSet(RedisConstant.TAG_BASE_KEY, tag);
                redisUtils.lSet(RedisConstant.TAG_DIAG_KEY, tag);

            }
            //归档正常处理的
            if(archiveFiles.size()>0){
                for (String targetPath: archiveFiles){
                    try {
                        FilesUtil.mkdir(new File(archiveDir));
                        File sessionLogFile = new File(targetPath);
                        String archiveFile = archiveDir+File.separator+ sessionLogFile.getName();
                        Files.move(Paths.get(sessionLogFile.getPath()), Paths.get(archiveFile), ATOMIC_MOVE);
                    }catch (Exception exception){
                        log.error("SessionJob move file {} error: {}", targetPath, JSONObject.toJSONString(exception));
                    }
                }
            }

        }
        log.info("SessionJob End................");
    }


    protected void delFileFormWorkDir(String dir){
        File file = new File(dir);
        if(file.exists()){
            try {
                FileUtils.deleteDirectory(file);
            }catch (Exception ex){
                log.error("Session SessionLogJob delFileFormWorkDir error: {}", ex.getMessage());
            }
        }
    }
}
