package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;


@Data
public class DIDDto implements Serializable {

    String ecuName;

    Integer onlineStatus;

    Integer dtcNumber;

    List<EcuVersionDto> didList;
}
