package org.dsa.modules.diagnostic.util;

import ch.qos.logback.classic.Logger;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.DateUtils;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.diagnostic.dao.DActionInfoDao;
import org.dsa.modules.diagnostic.dao.DSessionActionDao;
import org.dsa.modules.diagnostic.entity.mqtt.callback.MqEntity;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

import jakarta.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class MqAsynchronousData<T> {
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass());

    @Resource
    private RedisUtils redisUtils;
    @Resource
    private DSessionActionDao dSessionActionDao;
    @Resource
    private DActionInfoDao dActionInfoDao;

    public Constant.RESULTS handle(String function,MqEntity mqEntity,T t){
        String vin="";
        //消息状态 ACK
        int ack =mqEntity.getHeader().getAck();
        //消息状态 emm
        Constant.RESULTS results =null;
        //判断是否超时
        int timeout = 0;

        //判断命令是否有缓存   命令执行超时
        if(redisUtils.hasKey(Constant.REDIS_ACTION+mqEntity.getHeader().getRid())){
            //接受到读取的标识的信息回调
            vin =redisUtils.get(Constant.REDIS_ACTION+mqEntity.getHeader().getRid(), Constant.Function.getExpireByCode(mqEntity.getHeader().getFunctionCode()));
            logger.info("{} MQ Call back Redis vin:{},actionId:{}",function,vin,mqEntity.getHeader().getRid());
        }else{
            logger.info("{} MQ call ----Redis Action is null----- message command run timeout",function);
            vin=dSessionActionDao.selectVinIdByActionId(mqEntity.getHeader().getRid());
            timeout = 1;
        }
        //创建车辆日志
        String loggerKey= DateUtils.format(new Date(),"yyyyMMdd")+"/"+vin;
        MDC.put("vin",loggerKey);
        logger.info("{} MQ call ----VIN:{}",function,vin);

        //诊断命令响应
        if((ack & Constant.ACK.ACK.getValue()) !=0 ){
            logger.info("{} MQ Call ---- ACK " ,function );
            //接受响应 -- 延时命令执行超时
            redisUtils.get(Constant.REDIS_ACTION_RESULT + mqEntity.getHeader().getRid(), Constant.Function.getExpireByCode(mqEntity.getHeader().getFunctionCode()));
            redisUtils.get(Constant.REDIS_ACTION_STATUS +mqEntity.getHeader().getRid(), Constant.Function.getExpireByCode(mqEntity.getHeader().getFunctionCode()));
        }

        //诊断数据
        if((ack & Constant.ACK.DATA.getValue()) !=0 ){
            logger.info("{} MQ Call ----add Data to redis result ",function );
            logger.info("{} MQ Call ---- Data {} ",function , mqEntity.getBody().getData());
            if(mqEntity.getBody().getData() != null ){
                List<JSONObject> mqBodyDataList ;
                if(function.equals(Constant.Function.VHSS.getFunctionCode())){
                    logger.info("{} MQ Call ----VHHS Data  ",function );
                    JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSONString(mqEntity.getBody().getData()));
                    mqBodyDataList = new ArrayList<JSONObject>();
                    mqBodyDataList.add(jsonObject);
                    logger.info("{} MQ Call ----VHHS Data add ",function );
                }else{
                    mqBodyDataList = (List<JSONObject>) mqEntity.getBody().getData();
                }

                if(mqBodyDataList != null && mqBodyDataList.size()>0){
                    //缓存数据
                    String redisListStr =  redisUtils.get(Constant.REDIS_ACTION_RESULT + mqEntity.getHeader().getRid(), Constant.Function.getExpireByCode(mqEntity.getHeader().getFunctionCode()));
                    logger.info("{} MQ Call ----get redis Data : {}",function ,redisListStr );
                    JSONArray redisArray;
                    if(StringUtils.isNotEmpty(redisListStr)) {
                        redisArray = JSONArray.parseArray(redisListStr);
                    }else{
                        redisArray= new JSONArray();
                    }
                    logger.info("{} MQ Call ----redis Data size:{} ,Data : {}",function ,redisArray.size(),redisArray );
                    logger.info("{} MQ Call ----get redis Add Data size:{} ,Data :{} ",function,mqBodyDataList.size() ,mqBodyDataList );
                    for (int i = 0 ; i < mqBodyDataList.size() ; i++) {
                        redisArray.add(mqBodyDataList.get(i));
                    }
                    logger.info("{} MQ Call ----set redis Data : {}",function ,redisArray );
                    redisUtils.set(Constant.REDIS_ACTION_RESULT + mqEntity.getHeader().getRid(), JSONObject.toJSONString(redisArray), Constant.Function.getExpireByCode(mqEntity.getHeader().getFunctionCode()));
                }
            }
        }

        //诊断命令执行失败(异常)   保存状态值:失败
        if((ack & Constant.ACK.NAK.getValue()) !=0 ) {
            results = Constant.RESULTS.FAIL;
        }
        //诊断命令执行超时  保存状态值:超时
        if((ack & Constant.ACK.TIMEOUT.getValue()) !=0 ) {
            results = Constant.RESULTS.TIMEOUT;
        }

        //诊断命令执行完成 , 根据body中数据判断,业务数据成功/失败
        if((ack & Constant.ACK.FIN.getValue()) !=0 ) {
            //状态 == 0成功  !=0 失败
            if(mqEntity.getBody().getStatus()==0) {
                //诊断命令执行 成功
                results = Constant.RESULTS.SUCCESS;

            }else{
                //诊断命令执行 失败
                results = Constant.RESULTS.FAIL;
            }
        }


        if(results != null) {
            logger.info("{} MQ --- actionId:{},RESULTS:{}",function, mqEntity.getHeader().getRid(),results.getName());
            //诊断执行
            //缓存诊断命令执行状态
            redisUtils.set(Constant.REDIS_ACTION_STATUS + mqEntity.getHeader().getRid(), results.getValue(), Constant.Function.getExpireByCode(mqEntity.getHeader().getFunctionCode()));
            redisUtils.get(Constant.REDIS_ACTION_RESULT + mqEntity.getHeader().getRid(),Constant.Function.getExpireByCode(mqEntity.getHeader().getFunctionCode()));

            //清空 vin码阻塞
            logger.info("{} ----delete redis key :{}{}",function,Constant.REDIS_RUN_VIN, vin);
            redisUtils.delete(Constant.REDIS_RUN_VIN + vin);
            //清除 诊断action
            logger.info("{} ----delete redis key:{}{}",function,Constant.REDIS_ACTION, mqEntity.getHeader().getRid());
            redisUtils.delete(Constant.REDIS_ACTION + mqEntity.getHeader().getRid());

            //更新诊断命令执行状态
            dActionInfoDao.updateByActionId(mqEntity.getHeader().getRid(), String.valueOf(results.getValue()), timeout);
        }
        MDC.clear();

        return results;
    }
}
