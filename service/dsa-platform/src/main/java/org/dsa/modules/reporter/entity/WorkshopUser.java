package org.dsa.modules.reporter.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@TableName("d_workshop_user")
public class WorkshopUser extends BaseEntity {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String thirdpartId;

    private String username;

    private String workshopId;

    private String realName;

    private String email;

    private String mobile;

    private String password;

    private Boolean active;

    private String lastLoginTime;

    private String lastLoginIp;

    private String address;

    private Long maxLicenseNumber;

    private Boolean saveLicenseIdAsBytes;

    private String telephone;

    private Long bLimitLicenseNumber;

    private Long licenseExpireDuration;

    private String groupRef;

    private String licenseId;

    @TableField(exist = false)
    private String workshopGroup;
}
