package org.dsa.modules.remoteDiagnostic.controller;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.ibatis.annotations.Param;
import org.dsa.common.annotation.SysLog;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.R;
import org.dsa.common.validator.ValidatorUtils;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.common.validator.group.ReqGroup;
import org.dsa.common.validator.group.UpdateGroup;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuGroupEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuVersionEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleVersionEntity;
import org.dsa.modules.remoteDiagnostic.enums.RemoteVersionStatusEnum;
import org.dsa.modules.remoteDiagnostic.service.RemoteEcuVersionService;
import org.dsa.modules.remoteDiagnostic.service.RemoteVehicleEcuGroupService;
import org.dsa.modules.remoteDiagnostic.service.RemoteVehicleEcuService;
import org.dsa.modules.remoteDiagnostic.vo.*;
import org.dsa.modules.remoteDiagnostic.vo.EcuVersion.EcuVersionEcuReqVo;
import org.dsa.modules.remoteDiagnostic.vo.EcuVersion.FlashFileReqVo;
import org.dsa.modules.remoteDiagnostic.vo.Vehicle.VehicleTypeEcuGroupReqVo;
import org.dsa.modules.remoteDiagnostic.vo.Vehicle.VehicleVersionEcuGroupReqVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.*;

@Slf4j
@RestController
@RequestMapping("/remoteVehicleEcu")
public class RemoteVehicleEcuController {


    @Autowired
    private RemoteVehicleEcuService vehicleEcuService;
    @Autowired
    private RemoteVehicleEcuGroupService vehicleEcuGroupService;
    @Autowired
    private RemoteEcuVersionService versionService;


    @RequestMapping("/checkName")
    public R checkName(@RequestBody CheckNameReqVo vo) {

        ValidatorUtils.validateEntity(513, vo, AddGroup.class);

        return R.ok().put("flag", vehicleEcuService.check(vo));
    }
    // 车辆ecu---------------------------------------------------

    @RequestMapping("/getEcuPage")
    public R getEcuPage(@RequestBody VehicleEcuPageReVo vo) {
        return R.ok().put("data", vehicleEcuService.selectPage(vo));
    }

    @GetMapping("/getEcus")
    public R getEcus() {
        return R.ok().put("data", vehicleEcuService.getEcus());
    }


    @RequestMapping("/getNoIdEcus")
    public R getNoIdEcus(@RequestBody EcuVersionEcuReqVo vo) {
        return R.ok().put("data", vehicleEcuService.getNoIdEcus(vo.getId(), vo.getVersionId()));
    }

    @GetMapping("/getEcuInfo/{id}")
    public R getEcuInfo(@PathVariable("id") Long id) {
        if (id == null) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        return R.ok().put("data", vehicleEcuService.getInfo(id));
    }

    @RequestMapping("/getEcusById")
    public R getEcusById(@RequestBody EcuVersionEcuReqVo vo) {

        if (vo.getIds() == null || vo.getIds().size() == 0) {
            return R.ok().put("data", new ArrayList<RemoteVehicleEcuEntity>());
        }
        return R.ok().put("data", vehicleEcuService.getEcus(vo));
    }


    @SysLog("新增车辆ECU")
    @PostMapping("/saveEcu")
    public R save(@RequestBody RemoteVehicleEcuEntity ecu) {

        ValidatorUtils.validateEntity(513, ecu, AddGroup.class);
        try {
            vehicleEcuService.saveEcu(ecu);
            return R.ok();
        } catch (Exception e) {
            return R.error();
        }
    }

    @SysLog("修改车辆ECU")
    @PostMapping("/updateEcu")
    public R update(@RequestBody RemoteVehicleEcuEntity ecu) {
        ValidatorUtils.validateEntity(513, ecu, UpdateGroup.class);
        try {
            vehicleEcuService.updateEcu(ecu);
            return R.ok();
        } catch (Exception e) {
            return R.error();
        }
    }

    @PostMapping("/getEcusByGroup")
    public R getEcusByGroup(@RequestBody PublicPageReqVo vo) {


        return R.ok().put("data", vehicleEcuService.getEcus(vo));
    }

    // 车辆ecu 分组---------------------------------------------------

    @RequestMapping("/getGroupPage")
    public R getGroupPage(@RequestBody PublicPageReqVo vo) {
        return R.ok().put("data", vehicleEcuGroupService.selectPage(vo));
    }

    @GetMapping("/getGroups")
    public R getGroups() {
        return R.ok().put("data", vehicleEcuGroupService.getGroups());
    }

    @GetMapping("/getAllGroups")
    public R getAllGroups(Integer status) {
        return R.ok().put("data", vehicleEcuGroupService.getGroups(status));
    }


    @GetMapping("/getGroupInfo/{id}")
    public R getGroupInfo(@PathVariable("id") Long id) {
        if (id == null) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        return R.ok().put("data", vehicleEcuGroupService.getInfo(id));
    }

    @SysLog("新增车辆ECU分组")
    @PostMapping("/saveGroup")
    public R saveGroup(@RequestBody RemoteVehicleEcuGroupEntity group) {

        ValidatorUtils.validateEntity(513, group, AddGroup.class);
        try {
            vehicleEcuGroupService.saveGroup(group);
            return R.ok();
        } catch (Exception e) {
            return R.error();
        }
    }

    @SysLog("修改车辆ECU分组")
    @PostMapping("/updateGroup")
    public R updateGroup(@RequestBody RemoteVehicleEcuGroupEntity group) {

        ValidatorUtils.validateEntity(513, group, UpdateGroup.class);
        try {
            vehicleEcuGroupService.updateGroup(group);
            return R.ok();
        } catch (Exception e) {
            return R.error();
        }
    }

    @RequestMapping("/getEcuGroupPage")
    public R getEcuGroupPage(@RequestBody PublicPageReqVo vo) {
        return R.ok().put("data", vehicleEcuGroupService.selectEcuPage(vo));
    }

    @RequestMapping("/selectGroupByType/{typeId}")
    public R selectGroupByType(@PathVariable("typeId") Long typeId) {
        return R.ok().put("data", vehicleEcuGroupService.selectGroupByType(typeId));
    }

//    @RequestMapping("/getEcuGroupAndEcu")
//    public R getEcuGroupPage(@RequestBody PublicPageReqVo vo) {
//        return R.ok().put("data", vehicleEcuGroupService.selectEcuPage(vo));
//    }

//    @RequestMapping("/getGroupsByPlatform/{id}")
//    public R getEcuGroupsByPlatform(@PathVariable("id") Long id) {
//        if (id == null) {
//            return R.error(Constant.Msg.CHECK_INPUT);
//        }
//        return R.ok().put("data", vehicleEcuGroupService.getGroups(id));
//    }


    // ECU版本 分组---------------------------------------------------

    @RequestMapping("/getEcuVersionPage")
    public R getEcuVersionPage(@RequestBody PublicPageReqVo vo) {
        return R.ok().put("data", versionService.selectPage(vo));
    }

    @GetMapping("/getEcuVersions/{id}")
    public R getEcuVersion(@PathVariable("id") Long id) {
        if (id == null) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        return R.ok().put("data", versionService.getVersions(id));
    }

    @GetMapping("/getEcuVersionInfo/{id}")
    public R getEcuVersionInfo(@PathVariable("id") Long id) {
        if (id == null) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        return R.ok().put("data", versionService.getInfo(id));
    }

    @GetMapping("/getOneEcuVersionInfo/{id}")
    public R getOneEcuVersionInfo(@PathVariable("id") Long id) {
        if (id == null) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        return R.ok().put("data", versionService.getOneInfo(id));
    }

    @RequestMapping("/getEcuVersionByEcuId/{id}")
    public R getEcuVersionByEcuId(@PathVariable("id") Long id) {
        if (id == null) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        return R.ok().put("data", versionService.getEcuVersionByEcuId(id));
    }


    @SysLog("新增ECU版本")
    @PostMapping("/saveEcuVersion")
    public R saveEcuVersion(@RequestBody RemoteVehicleEcuVersionEntity version) {

        ValidatorUtils.validateEntity(513, version, AddGroup.class);

        versionService.saveVersion(version);
        return R.ok();
    }

    @SysLog("修改ECU版本")
    @PostMapping("/updateEcuVersion")
    public R updateEcuVersion(@RequestBody RemoteVehicleEcuVersionEntity version) {
        ValidatorUtils.validateEntity(513, version, UpdateGroup.class);

        //开发->测试, 测试->开发,测试->释放,测试->拒绝,释放->拒绝
        RemoteVehicleEcuVersionEntity info = versionService.getOneInfo(version.getId());
        if (RemoteVersionStatusEnum.DEV.getValue().equals(info.getStatus()) && !RemoteVersionStatusEnum.DEV.getValue().equals(version.getStatus())) {
            if (!RemoteVersionStatusEnum.TEST.getValue().equals(version.getStatus())) {
                throw new RRException(Constant.Msg.VERSION_DEV_STATUS_ERROR);
            }
        }

        if (RemoteVersionStatusEnum.OPEN.getValue().equals(info.getStatus())) {
            if (!RemoteVersionStatusEnum.CLOSE.getValue().equals(version.getStatus()) && !RemoteVersionStatusEnum.OPEN.getValue().equals(version.getStatus())) {
                throw new RRException(Constant.Msg.VERSION_RELEASE_STATUS_ERROR);
            }
        }

        versionService.updateVersion(version);
        return R.ok();
    }

    @RequestMapping("/getVersionFile/{type}/{id}")
    public R getVersionFile(@PathVariable("type") String type, @PathVariable("id") Long id) {
        if (id == null) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        if (StringUtils.isBlank("type")) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        return R.ok().put("data", versionService.getVersionFile(type, id));
    }

    /**
     * ecu版本文件
     */
    @PostMapping("/ecuVersionFileUpload/{type}")
    public R diagnosticFileUpload(@PathVariable("type") String type, @RequestParam("file") MultipartFile mFile) {
        if (StringUtils.isBlank(type)) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }

        try {
            //MultipartFile 转化为file easyexcel接受file类型的文件
            File file = new File(Objects.requireNonNull(mFile.getOriginalFilename()));
//            if (!file.getName().endsWith(".xlsx")) {
//                return R.error("请导入excel类型的文件");
//            }
//
            FileUtils.copyInputStreamToFile(mFile.getInputStream(), file);

            return R.ok().put("file", versionService.upload(file, type));
        } catch (Exception e) {
            log.error("文件导入出错：", e);
            return R.error(600, "文件导入出错：" + e);
        }
    }

    @PostMapping("/copyVersion/{id}")
    public R copyVersion(@PathVariable("id") Long id) {
        try {
            versionService.copyVersionById(id);
            return R.ok();
        }catch (Exception e){
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    /**
     * ecu版本文件
     */
    @RequestMapping("/ecuVersionFileDownload/")
    public void diagnosticFileDownload(@RequestBody PublicFileReqVo vo, HttpServletRequest req, HttpServletResponse response) {
//        VehicleEcuEntity info = vehicleEcuService.getInfo(id);
        if (StringUtils.isBlank(vo.getPath())) {
            return;
        }
//        File downloadFile = new File(file.getFilePath());
        File downloadFile = new File(vo.getPath());
        InputStream ips = null;
        OutputStream ops = null;
        try {
            ips = new FileInputStream(downloadFile);
            ops = response.getOutputStream();
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(downloadFile.getName(), "UTF-8"));
            IOUtils.copy(ips, ops);
            ips.close();
            ops.flush();

        } catch (IOException e) {

        } finally {
            try {
                if (ips != null) {
                    ips.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (ops != null) {
                    ops.flush();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * ecu版本文件
     */
    @PostMapping("/ecu_image_upload")
    public R ecuImageUpload(@RequestParam("file") MultipartFile mFile) {
        try {
            //MultipartFile 转化为file easyexcel接受file类型的文件
            File file = new File(Objects.requireNonNull(mFile.getOriginalFilename()));
//            if (!file.getName().endsWith(".xlsx")) {
//                return R.error("请导入excel类型的文件");
//            }
//
            FileUtils.copyInputStreamToFile(mFile.getInputStream(), file);

            return R.ok().put("file", versionService.uploadImage(file));
        } catch (Exception e) {
            log.error("文件导入出错：", e);
            return R.error(600, "文件导入出错：" + e);
        }
    }

    @GetMapping("/selectEcuVersionDepend")
    public R selectEcuVersionDepend(@RequestParam("versionId") Long versionId, @RequestParam("ecuId") Long ecuId) {

        if (versionId == null) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        List<RemoteVehicleEcuEntity> ecus = versionService.selectEcuVersionDepend(versionId, ecuId);
        return R.ok().put("data", ecus);
    }

    @GetMapping("/getAllEcuVersions")
    public R getAllEcuVersions(Integer status) {
        return R.ok().put("data", versionService.getAllVersions(status));
    }


    // 车辆类型  ecu分组关联---------------------------------------------------

    @RequestMapping("/getVehicleTypeEcuGroup")
    public R getVehicleTypeEcuGroup(@Validated @RequestBody VehicleTypeEcuGroupReqVo vo) {

        return R.ok().put("data", vehicleEcuGroupService.getVehicleTypeEcuGroup(vo));
    }

    @PostMapping("/saveVehicleTypeEcuGroup")
    public R saveVehicleTypeEcuGroup(@Validated @RequestBody VehicleTypeEcuGroupReqVo vo) {

        vehicleEcuGroupService.saveVehicleTypeEcuGroup(vo);

        return R.ok();
    }

    @PostMapping("/updateVehicleTypeEcuGroup")
    public R updateVehicleTypeEcuGroup(@Validated @RequestBody VehicleTypeEcuGroupReqVo vo) {
        if (vo.getDelGroupEntities() == null || vo.getDelGroupEntities().size() == 0) {
            return R.ok();
        }
        vehicleEcuGroupService.updateVehicleTypeEcuGroup(vo);

        return R.ok();
    }

    @PostMapping("/deleteVehicleTypeEcuGroup")
    public R deleteVehicleTypeEcuGroup(@Validated @RequestBody VehicleTypeEcuGroupReqVo vo) {
        if (vo.getGroupEntities() == null || vo.getGroupEntities().size() == 0) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        vehicleEcuGroupService.deleteVehicleTypeEcuGroup(vo);

        return R.ok();
    }

    @RequestMapping("/getGroupsById")
    public R getGroupsById(@RequestBody VehicleVersionEcuGroupReqVo vo) {
        if (vo.getIds() == null || vo.getIds().size() == 0) {
            return R.ok().put("data", new ArrayList<RemoteVehicleEcuEntity>());
        }
        return R.ok().put("data", vehicleEcuGroupService.getGroups(vo.getIds(), vo.getVersionId(), vo.getTypeId()));
    }

    // 获取灌装证书
    @PostMapping("/getECUFillingCertificate")
    public Map<String, Object> getECUFillingCertificate(@RequestBody ECUFillingCertificateReqVo vo) {

        HashMap<String, Object> result = new HashMap<>();
//            result.put("key", "");
//            result.put("ca", "");
        result.put("cert", "");
//        if (StringUtils.isEmpty(vo.getPcid())) {
//
//            return R.error(Constant.Msg.CHECK_INPUT).put("data", result);
//        }
        ValidatorUtils.validateEntityByRemote(1,result, vo, ReqGroup.class);
        try {
            return vehicleEcuService.getECUFillingCertificate(vo);
        } catch (Exception e) {
            return R.error().put("data", result);
        }
    }
}
