package org.dsa.modules.reporter.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.dsa.config.SyncConfig;
import org.dsa.modules.reporter.dao.FileLogMapper;
import org.dsa.modules.reporter.entity.FileLog;
import org.dsa.modules.reporter.entity.ScheduleJobLog;
import org.dsa.modules.reporter.enums.FileCateEnum;
import org.dsa.modules.reporter.enums.FileStatusEnum;
import org.dsa.modules.reporter.service.FileLogService;
import org.dsa.modules.reporter.vo.file.FileLogPageReqVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import jakarta.annotation.Resource;
import java.io.File;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;

@Slf4j
@Service
public class FileLogServiceImpl implements FileLogService {

    @Autowired
    FileLogMapper logMapper;

    @Resource
    private SyncConfig syncConfig;

    @Override
    public Boolean checkSuccessed(String fileType, String fileName) {
        Boolean flag = false;
        FileLog successFile =  logMapper.checkFile(fileType, fileName);
        FileLog failedFile = logMapper.checkFailedFile(fileType, fileName);
        if(null != successFile){
            flag = true;
        }
        if(null != failedFile){
            flag = false;
        }
        return flag;
    }

    @Override
    public void insertSessionLog(String name, String path, String tag, String sessionId, Integer status, String note) {
        FileLog fileLog = new FileLog();
        fileLog.setFileType(name);
        fileLog.setFilePath(path);

        String[] paths = path.split(Matcher.quoteReplacement(File.separator));
        if(null != paths && paths.length>0){
            fileLog.setFileName(paths[paths.length-1]);
        }

        if(null != sessionId){
            fileLog.setSessionId(sessionId);
        }
        if(null != tag){
            fileLog.setTag(tag);
        }
        fileLog.setNote(note);
        fileLog.setStatus(status);
        logMapper.insert(fileLog);
    }

    @Override
    public void insertLog(String name, String path, String tag, String hash, Integer status, String note) {
        FileLog fileLog = new FileLog();
        fileLog.setFileType(name);
        if(StrUtil.isNotBlank(path)){
            fileLog.setFilePath(path);
            String[] paths = path.split(Matcher.quoteReplacement(File.separator));
            if(null != paths && paths.length>0){
                fileLog.setFileName(paths[paths.length-1]);
            }
        }else{
            fileLog.setFileName(note);
        }
        if(null !=hash){
            fileLog.setHashFile(hash);
        }
        if(null != tag){
            fileLog.setTag(tag);
        }
        fileLog.setNote(note);
        fileLog.setStatus(status);
        logMapper.insert(fileLog);
    }

    @Override
    public void failedLog(String name, String path, String tag, String note) {
        insertLog(name, path, tag, null, 0, note);
    }

    @Override
    public void succeedLog(String name, String path, String tag, String hash, String note) {
        insertLog(name, path, tag, hash, 1, note);
    }

    @Override
    public List<String> failedFiles(Date handlerTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return logMapper.filePathListByDate(sdf.format(handlerTime), null, FileStatusEnum.FAILED.getValue());
    }

    @Override
    public List<String> succeedFiles(Date handlerTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return logMapper.filePathListByDate(sdf.format(handlerTime), null, FileStatusEnum.SUCCEED.getValue());
    }

    @Override
    public Page<FileLog> selectPage(FileLogPageReqVo vo) {
        Page<FileLog> pg = new Page<>(vo.getPageNo(), vo.getPageSize());
        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        pg.addOrder(oi);
        LambdaQueryWrapper<FileLog>  qw = Wrappers.<FileLog>lambdaQuery();
        if(!StringUtils.isEmpty(vo.getName())){
            qw.apply("file_name"+" ilike {0}", "%"+vo.getName().trim()+"%");
        }

        if(!StringUtils.isEmpty(vo.getStartDate()) && !StringUtils.isEmpty(vo.getEndDate()) ){
            qw.between(FileLog::getCreatedAt,
                    Timestamp.valueOf(vo.getStartDate()+" 00:00:00.000"),
                    Timestamp.valueOf(vo.getEndDate()+" 23:59:59.999"));
        }

        if(!StringUtils.isEmpty(vo.getStatus())){
            qw.eq(FileLog::getStatus, vo.getStatus());
        }

        if(!StringUtils.isEmpty(vo.getFileType())){
            qw.eq(FileLog::getFileType, vo.getFileType());
        }

        if(!StringUtils.isEmpty(vo.getError())){
            qw.and((wrapper)->{
                wrapper.apply("note"+" ilike {0}", "%"+vo.getError().trim()+"%")
                        .or()
                        .apply("file_name"+" ilike {0}", "%"+vo.getError().trim()+"%")
                        .or().like(FileLog::getTag, vo.getError());
            });
        }

        if(!StringUtils.isEmpty(vo.getTag())){
            qw.eq(FileLog::getTag, vo.getTag());
        }

        Page<FileLog> logPage = logMapper.selectPage(pg, qw);

        return logPage;
    }

    @Override
    public FileLog findOneFile(Long id) {
        return logMapper.selectById(id);
    }

    @Override
    public FileLog checkFile(String fileType, String fileName) {
        return logMapper.checkFile(fileType, fileName);
    }

    @Override
    public String pathToFileName(String path) {
        String[] paths = path.split(Matcher.quoteReplacement(File.separator));
        return paths[paths.length-1];
    }

    @Override
    public String getArchiveFile(String fileType, String fileName) {
        FileLog log = checkFile(FileCateEnum.EVENT.getValue(), fileName);
        if (null == log) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(syncConfig.getDirFormat());
        String archiveDir = syncConfig.getSessionArchiveDir() + File.separator + sdf.format(log.getCreatedAt());
        return archiveDir + File.separator + log.getFileName();
    }
    public Integer insertLog(String name, String fileName, String path, String tag, String hash, Integer status, String note) {
        FileLog fileLog = new FileLog();
        fileLog.setFileType(name);
        fileLog.setFilePath(path);
        fileLog.setFileName(fileName);
        if(null !=hash){
            fileLog.setHashFile(hash);
        }
        if(null != tag){
            fileLog.setTag(tag);
        }
        fileLog.setNote(note);
        fileLog.setStatus(status);
        return logMapper.insert(fileLog);
    }

    @Override
    public Integer failedLog(String name, String fileName, String path, String tag, String note) {
        return insertLog(name,fileName, path, tag, null, 0, note);
    }

    @Override
    public Integer succeedLog(String name, String fileName, String path, String tag, String hash, String note) {
        return insertLog(name, fileName, path, tag, hash, 1, note);
    }

    public void updateLog(Integer fileId,String filePath) {
        logMapper.updatePath(fileId,filePath);
    }
}
