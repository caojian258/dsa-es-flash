package org.dsa.common.utils;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/***
 * https://www.cnblogs.com/hyl8218/p/8302552.html
 */
@Slf4j
public class ShellUtil {

    /***
     * 目录
     */
    private String directory;

    /***
     * 执行结果 默认-1 成功0 失败1
     */
    private int exitCode = -1;

    /***
     * 执行过程中的消息
     */
    private List<String> messages = new ArrayList<>();

    /***
     * 指令集合
     */
    private List<String> commandList = new ArrayList<>();

    /***
     * 构造函数
     * @param path shell目录
     * @param cmdList 要执行的命令
     */
    public ShellUtil(String path, List<String> cmdList){
        this.directory = path;
        this.commandList = cmdList;
    }


    /***
     * 执行命令
     */
    public int start(){
        Process process = null;
        ProcessBuilder pb = new ProcessBuilder(commandList);
        pb.directory(new File(directory));

        try {
            String command =  String.join(StringUtils.SPACE, pb.command());
            log.info("命令：{}", command);
            log.info("目录：{}", pb.directory().getAbsolutePath());

            process = pb.start();
            CommandStreamGobbler errorGobbler = new CommandStreamGobbler(process.getErrorStream(), command, "ERR");
            CommandStreamGobbler outputGobbler = new CommandStreamGobbler(process.getInputStream(), command, "STD");

            errorGobbler.start();
            // 必须先等待错误输出ready再建立标准输出
            while (!errorGobbler.isReady()) {
                Thread.sleep(10);
            }

            outputGobbler.start();
            while (!outputGobbler.isReady()) {
                Thread.sleep(10);
            }

            CommandWaitForThread commandThread = new CommandWaitForThread(process);
            commandThread.start();

            long timeout = 2 * 1000;

            long commandTime = new Date().getTime();
            long nowTime = new Date().getTime();
            boolean timeoutFlag = false;
            while (!commandIsFinish(commandThread, errorGobbler, outputGobbler)) {
                if (nowTime - commandTime > timeout) {
                    timeoutFlag = true;
                    break;
                } else {
                    Thread.sleep(100);
                    nowTime = new Date().getTime();
                }
            }

            exitCode =  commandThread.getExitValue();

            if (timeoutFlag) {
                // 命令超时
                errorGobbler.setTimeout(1);
                outputGobbler.setTimeout(1);
                log.error("执行命令：" + command + " 超时");
            }else {
                // 命令执行完成
                errorGobbler.setTimeout(2);
                outputGobbler.setTimeout(2);
            }

            while (true) {
                if (errorGobbler.isReadFinish() && outputGobbler.isReadFinish()) {
                    break;
                }
                Thread.sleep(10);
            }

            if(outputGobbler.getInfoList()!= null){
                this.messages.addAll(outputGobbler.getInfoList());
            }

            if(errorGobbler.getInfoList() != null){
                this.messages.addAll(errorGobbler.getInfoList());
            }

        }catch (Exception ex){
            log.error("启动任务失败: {} ", JSONObject.toJSONString(ex));
            messages.add(JSONObject.toJSONString(ex));
        }finally {
            if (process != null) {
                process.destroy();
            }
        }

        return exitCode;
    }

    private boolean commandIsFinish(CommandWaitForThread commandThread, CommandStreamGobbler errorGobbler, CommandStreamGobbler outputGobbler) {
        if (commandThread != null) {
            return commandThread.isFinish();
        } else {
            return (errorGobbler.isReadFinish() && outputGobbler.isReadFinish());
        }
    }

    public String getDirectory(){
        return directory;
    }

    public List<String> getMessages(){
        return messages;
    }

    public int getExitCode(){
        return exitCode;
    }

    public static boolean isWindows(){
        return System.getProperty("os.name").toLowerCase().contains("windows");
    }

    public static boolean isLinux(){
        return System.getProperty("os.name").toLowerCase().contains("linux");
    }
}
