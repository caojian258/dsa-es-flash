package org.dsa.modules.vehicle.vo;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class VehicleInfoUpdateReqVo {

    // 产地
    private String placeOfProduction;

    // 生产日期
    private String productionDate;

    // VIN码
    private List<String> vinList;

    // 车型 ID
    private Long vehicleTypeId;

    // 标签
    private List<Long> tags;

    //是否含有tsp
    private Boolean hasTsp;


}
