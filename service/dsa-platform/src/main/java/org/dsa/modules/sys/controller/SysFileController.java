package org.dsa.modules.sys.controller;

import ch.qos.logback.classic.Logger;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import jakarta.annotation.Resource;
import jakarta.mail.internet.MimeUtility;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.annotation.SysLog;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.*;
import org.dsa.modules.reporter.enums.FileCateEnum;
import org.dsa.modules.reporter.service.FileLogService;
import org.dsa.modules.sys.dto.SysTranslationsDto;
import org.dsa.modules.sys.entity.FileEntity;
import org.dsa.modules.sys.service.SysFileService;
import org.dsa.modules.sys.service.SysLocaleService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping("sys/file")
public class SysFileController {

    private final Logger logger = (Logger) LoggerFactory.getLogger(getClass());

    @Autowired
    private SysFileService sysFileService;

    @Resource
    private RedisUtils redisUtils;

    @Resource
    FileLogService logService;

    @Autowired
    SysLocaleService SysLocaleServiceImpl;

    @Value("${file.language.sqlite_path}")
    private String sqlitePath;

    @Value("${file.component.version_path}")
    private String versionPath;

    @Value("${file.launcher.path}")
    private String launcherPath;

    //    @Value("${}")
    private static final Long languageFileExpire = 3*60*60L;
    /**
     * 上传文件
     * @param file
     * @param req
     * @return
     */
    @RequestMapping("/upload")
//    @RequiresPermissions("sys:file:upload")
    public R upload(@RequestParam("file") MultipartFile file, @RequestParam("versionType") String versionType,
                    HttpServletRequest req){
        logger.info("文件上传中...");
        if(null!=file){
            FileEntity fileEntity=sysFileService.uploadFiles(file,versionType,req);
            logger.info("文件上传成功...");
            return R.ok().put("data",fileEntity);
        }else {
            return R.error("上传的文件信息不能为空");
        }
    }
    /**
     * checkFile
     */
    @PostMapping("/checkFile")
    public R checkFile(@RequestBody FileEntity fileEntity){
        File file = new File(fileEntity.getUrl());
        String fileName = file.getName();
        Boolean sessionFlag = StrUtil.startWith(fileName, "Session_");
        if(sessionFlag){
            String archivePath = logService.getArchiveFile(FileCateEnum.EVENT.getValue(), fileName);
            if(StrUtil.isNotBlank(archivePath)){
                file = new File(archivePath);
            }
        }
        if (!FileUtil.exist(file)){
            return R.error(Constant.Msg.NO_LOG_FILE);
        }
        return R.ok();
    }
    /**
     * 文件下载
     * @param fileEntity
     * @param req
     * @param response
     * @return
     */
    @RequestMapping(value="/download")
//    @RequiresPermissions("sys:file:download")
    public void download(@RequestBody FileEntity fileEntity, HttpServletRequest req, HttpServletResponse response) {
        File file = new File(fileEntity.getUrl());
        if (!file.exists()) {
            throw new RRException(Constant.Msg.FILE_NOT_EXIST);
        }

        InputStream ips = null;
        OutputStream ops = null;
        try {
            String fileName = file.getName();
            logger.info("session file name: {}",fileName);
            Boolean sessionFlag = StrUtil.startWith(fileName, "Session_");
            if(sessionFlag){
                String archivePath = logService.getArchiveFile(FileCateEnum.EVENT.getValue(), fileName);
                logger.info("session path: {}",archivePath);
                if(StrUtil.isNotBlank(archivePath)){
                    file = new File(archivePath);
                    fileEntity.setName(fileName);
                    fileEntity.setRealName(fileName);
                }
            }

            ips =  new FileInputStream(file);;//new FileInputStream(new File(fileName));
            ops = response.getOutputStream();
            response.setContentType("application/x-download");
            response.setHeader("Content-Disposition", "attachment;filename=utf-8'zh_cn'" + URLEncoder.encode(fileEntity.getName(), "UTF-8"));
            IOUtils.copy(ips, ops);
            ips.close();
            ops.flush();

        } catch (IOException e) {
            logger.error("下载文件出错："+e);
        } finally {
            try {
                if(ips != null ) {
                    ips.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if(ops != null) {
                    ops.flush();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @RequestMapping(value = "/security-download", method = {RequestMethod.GET, RequestMethod.POST})
    public void securityDownload(@RequestParam("params") String params, HttpServletRequest request, HttpServletResponse response) {
        if (StringUtils.isEmpty(params)) {
            logger.error("security download params is empty.");
            throw new RRException(Constant.Msg.CHECK_INPUT);
        }

        logger.info("security-download params={}", params);

        String filePath = AESUtils.decrypt(params, AESUtils.AES_KEY);

        if (StringUtils.isEmpty(filePath)) {
            logger.error("security download file path is empty.");
            throw new RRException(Constant.Msg.CHECK_INPUT);
        }

        File file = new File(filePath);
        if (!file.exists()) {
            logger.error("security download path = [{}] is not exist.", filePath);
            throw new RRException(Constant.Msg.FILE_NOT_EXIST);
        }
        long fileLength = file.length();

        String fileName = FileHelper.getFileName(filePath);

        String range = request.getHeader("Range");
        long pos = 0;
        if (StringUtils.isNotEmpty(range)) {
            response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
            try {
                pos = Long.parseLong(range.replaceAll("bytes=", "").replaceAll("-", ""));
            } catch (NumberFormatException e) {
                logger.error(range + " is not Number!");
            }
        }

        String contentRange = "bytes=" + pos + "-" + (fileLength - 1) + "/" + fileLength;
        logger.info("Content-Range={}", contentRange);

        response.setContentType("application/x-download");
        response.setHeader("Accept-Ranges", "bytes");
        response.setHeader("Content-Range", contentRange);
        response.setHeader("Content-Length", String.valueOf(fileLength - pos));
        try {
            response.setHeader("Content-Disposition", "attachment;" + getName(fileName, request));
        } catch (Exception e) {
            logger.error("getName occurred exception.", e);
            throw new RRException(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }

        try(BufferedInputStream bis = new BufferedInputStream(Files.newInputStream(file.toPath()));
            BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream())) {

            bis.skip(pos);
            byte[] buff = new byte[4096];
            int len;
            while (-1 != (len = bis.read(buff, 0, buff.length))) {
                bos.write(buff, 0, len);
            }
        } catch (Exception e) {
            logger.error("下载文件出错", e);
            throw new RRException(Constant.Msg.FILE_DOWNLOAD_ERROR);
        }
    }

    private String getName(String filename, HttpServletRequest request) throws Exception {
        String userAgent = request.getHeader("userAgent");
        String new_filename = URLEncoder.encode(filename, "UTF8");
        // 如果没有UA，则默认使用IE的方式进行编码，因为毕竟IE还是占多数的
        String rtn = "filename=\"" + new_filename + "\"";
        if (userAgent != null) {
            userAgent = userAgent.toLowerCase();
            // IE浏览器，只能采用URLEncoder编码
            if (userAgent.indexOf("msie") != -1) {
                rtn = "filename=\"" + new_filename + "\"";
            }
            // Opera浏览器只能采用filename*
            else if (userAgent.indexOf("opera") != -1) {
                rtn = "filename*=UTF-8''" + new_filename;
            }
            // Safari浏览器，只能采用ISO编码的中文输出
            else if (userAgent.indexOf("safari") != -1) {
                rtn = "filename=\"" + new String(filename.getBytes("UTF-8"), "ISO8859-1") + "\"";
            }
            // Chrome浏览器，只能采用MimeUtility编码或ISO编码的中文输出
            else if (userAgent.indexOf("applewebkit") != -1) {
                new_filename = MimeUtility.encodeText(filename, "UTF8", "B");
                rtn = "filename=\"" + new_filename + "\"";
            }
            // FireFox浏览器，可以使用MimeUtility或filename*或ISO编码的中文输出
            else if (userAgent.indexOf("mozilla") != -1) {
//    	          rtn = "filename*=UTF-8''" + new_filename;
//    	    	   new_filename = MimeUtility.encodeText(filename, "UTF8", "B");
                rtn = "filename*=UTF-8''" + new String(filename.getBytes("UTF-8"), "ISO8859-1") + "\"";
            }
        }
        return rtn;

    }

    /**
     * 删除文件
     * @param fileEntity
     * @return
     */
    @PostMapping("/delete")
//    @RequiresPermissions("sys:file:delete")
    public R deleteFile(@RequestBody FileEntity fileEntity) {
        logger.info("文件删除中...,文件路径为：" + fileEntity.getUrl());
        try {
            if (StringUtils.isNotBlank(fileEntity.getUrl())) {
                sysFileService.deletefile(fileEntity.getUrl());
            }
        } catch (Exception e) {
            logger.error("删除文件出错：" + e);
            return R.error("删除文件出错：" + e);
        }
        return R.ok();
    }

    /**
     * 多语言excel导入
     */
    @PostMapping("/multi_language_excel_import")
//    @RequiresPermissions("sys:file:ExcelImport")
    public R multiLanguageExcelImport(@RequestParam("file") MultipartFile mFile) {
        try {
            //MultipartFile 转化为file easyexcel接受file类型的文件
            File file = new File(Objects.requireNonNull(mFile.getOriginalFilename()));
            if (!file.getName().endsWith(".xlsx")) {
                return R.error("请导入excel类型的文件");
            }
            FileUtils.copyInputStreamToFile(mFile.getInputStream(), file);
            sysFileService.multiLanguageExcelImport(file);
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            logger.error("excel导入出错：", e);
            return R.error(600,"excel出错：" + e);
        }
        return R.ok();
    }

    /**
     * 多语言excel导出
     */
    @GetMapping("/multi_language_excel_export")
//    @RequiresPermissions("sys:file:multiLanguageExcelExport")
    public R multiLanguageExcelExport() {
        String url = "";
        try {
            url = sysFileService.multiLanguageExcelExport();
        } catch (Exception e) {
            logger.error("excel导出有误：",e);
            return R.error("excel出错：" + e);
        }
        return R.ok().put("url", url);
    }

    /**
     * 多语言配置zip包下载
     */
    @GetMapping("/multi_language_zip")
//    @RequiresPermissions("sys:file:multiLanguageZip")
    public R multiLanguageZip() {
        TimeInterval timer = DateUtil.timer();
        String url = "";
        try {
            url = sysFileService.multiLanguageZip();
        }catch (FileNotFoundException fNNE){
            logger.info("文件下载异常"+fNNE);
        }catch (Exception e){
            logger.info("服务器内部异常"+e);
            return R.error(500,"服务内部异常");
        }
        logger.info("下载zip用时{}",timer.intervalRestart());
        return R.ok().put("zipUrl",url);
    }

    /**
     * sqlite诊断库文件上传
     */
    @RequestMapping("/sqlite_upload")
//    @RequiresPermissions("sys:file:sqliteUpload")
    public R sqliteUpload(@RequestParam("file") MultipartFile file){
        logger.info("文件上传中...");
        if(null!=file){
            FileEntity fileEntity = sysFileService.uploadFile(file,sqlitePath);
            if(StrUtil.isNotBlank(fileEntity.getUrl())){
                SysLocaleServiceImpl.handleSqlite(fileEntity.getUrl());
            }
            logger.info("文件上传成功...");
            return R.ok().put("data",fileEntity);
        }else {
            return R.error("上传的文件信息不能为空");
        }
    }

    /**
     * 组件/软件版本文件上传
     */
    @RequestMapping("/upload_version_file")
//    @RequiresPermissions("sys:file:upload")
    public R uploadVersionFile(@RequestParam("file") MultipartFile file, HttpServletRequest req){
        logger.info("文件上传中...");
        if(null!=file){
            FileEntity fileEntity=sysFileService.uploadVersionFile(file,versionPath);
            logger.info("文件上传成功...");
            return R.ok().put("data",fileEntity);
        }else {
            return R.error("上传的文件信息不能为空");
        }
    }

    /**
     * 车辆导入模板下载
     */
    @GetMapping("/vehicle_template")
    public R downloadVehicleTemplate(@RequestParam("languages") String languages) {
        Map<String,String> map;
        try {
            map = sysFileService.downloadVehicleTemplate(languages);
        }catch (RRException rre){
            logger.info(rre.getMsg());
            return R.error(Constant.Msg.TEMPLATE_FILE_EMPTY.getCode(),rre.getMsg());
        }catch (Exception e){
            logger.info("服务器内部异常"+e);
            return R.error(Constant.Msg.EXCEL_EXPORT_ERROR);
        }
        return R.ok().put("data",map);
    }

    /**
     * 车辆信息excel导入
     */
    @PostMapping("/vehicle_excel_import")
    public R vehicleExcelImport(@RequestParam("file") MultipartFile mFile) {
        try {
            //MultipartFile 转化为file easyexcel接受file类型的文件
            File file = new File(Objects.requireNonNull(mFile.getOriginalFilename()));
            if (!file.getName().endsWith(".xlsx")) {
                return R.error("请导入excel类型的文件");
            }
            FileUtils.copyInputStreamToFile(mFile.getInputStream(), file);
            sysFileService.vehicleExcelImport(file);
            if (file.exists()) {
                file.delete();
            }
        }catch (RRException re){
            logger.error("excel导入出错123：" + re);
            return R.error(Constant.Msg.EXCEL_IMPORT_ERROR.getCode(),re.getMsg());
        }catch (Exception e) {
            logger.error("excel导入出错：" + e);
            return R.error(Constant.Msg.EXCEL_IMPORT_ERROR);
        }
        return R.ok();
    }

    @PostMapping("/upload-launcher")
    public R uploadLauncher(@RequestParam("file") MultipartFile file) {
        if (file == null || StringUtils.isEmpty(file.getOriginalFilename())) {
            return R.error("上传的文件信息不能为空");
        }

        if (!file.getOriginalFilename().endsWith(".exe")) {
            return R.error("请导入exe类型的文件");
        }

        String fileName = "SmartLauncher_setup.exe";
        File launcherFile = new File(launcherPath + fileName);
        try {
            FileUtils.copyInputStreamToFile(file.getInputStream(), launcherFile);
        } catch (IOException e) {
            logger.error("upload launcher error：" + e);
            return R.error(Constant.Msg.FILE_UPLOAD_ERROR);
        }
        logger.info("userId=[{}]上传了Launcher安装包", ShiroUtils.getUserId());
        return R.ok();
    }

    @GetMapping(value="/download-launcher")
    public R downloadLauncher(){
        String fileName = "SmartLauncher_setup.exe";
        File launcherFile = new File(launcherPath + fileName);
        if (!launcherFile.exists()) {
            return R.error(Constant.Msg.FILE_NOT_EXIST);
        }
        Map<String, Object> file = new HashMap<>();
        file.put("url",launcherPath + fileName);
        file.put("len",launcherFile.length());
        return R.ok().put("file", file);
    }

    /**
     * 用户导入模板下载
     */
    @GetMapping("/user_template")
    public R downloadUserTemplate() {
        Map<String,String> map;
        try {
            map = sysFileService.downloadUserTemplate();
        }catch (RRException rre){
            logger.info(rre.getMsg());
            return R.error(Constant.Msg.TEMPLATE_FILE_EMPTY.getCode(),rre.getMsg());
        }catch (Exception e){
            logger.info("服务器内部异常"+e);
            return R.error(Constant.Msg.EXCEL_EXPORT_ERROR);
        }
        return R.ok().put("data",map);
    }

    /**
     * 用户excel导入
     */
    @SysLog("导入用户")
    @PostMapping("/import_user")
    public R importUser(@RequestParam("file") MultipartFile mFile) {
        try {
            //MultipartFile 转化为file easy-excel接受file类型的文件
            File file = new File(Objects.requireNonNull(mFile.getOriginalFilename()));
            if (!file.getName().endsWith(".xlsx")) {
                return R.error("请导入excel类型的文件");
            }
            FileUtils.copyInputStreamToFile(mFile.getInputStream(), file);
            sysFileService.importUser(file);
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            logger.error("excel导入出错：", e);
            return R.error(600,"excel出错：" + e);
        }
        return R.ok();
    }
}
