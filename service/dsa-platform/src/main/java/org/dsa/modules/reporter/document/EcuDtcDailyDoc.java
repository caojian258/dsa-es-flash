package org.dsa.modules.reporter.document;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class EcuDtcDailyDoc implements Serializable{

    private Long id;

    private String vin;

    private List<Long> vehicleTypeId;

    private String ecuName;

    private String dtcCode;

    private Long dtcCodeCount;

    private String description;

    private String descriptionTi;

    private String createDay;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date createdAt;
}
