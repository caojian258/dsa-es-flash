package org.dsa.modules.onlineUpdate.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dsa.modules.onlineUpdate.entity.ComponentEntity;
import org.dsa.modules.onlineUpdate.utils.PageUtils;
import org.dsa.modules.onlineUpdate.vo.CascadeOptions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 组件service
 *
 */
public interface ComponentService extends IService<ComponentEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveOrUpdate2(ComponentEntity entity);

    void ableOrDisabledById(Long id);

    List<HashMap> queryAll();

    List<HashMap> queryUpdateTypeList();

    List<CascadeOptions> getComponentOptions();
}

