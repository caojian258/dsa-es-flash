package org.dsa.modules.reporter.document;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
public class DiagnosticEcuDoc extends ElasticEntity {

    private String sessionInfoId;

    private String sessionId;

    private String vin;

    private List<Long> vehicleTypeId;

    private String ecuName;

    private Map<String, Object> ecuNameSuggest;

    //0 在线 非零不在线异常
    private Integer status;

    private Integer dtcCount;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

}
