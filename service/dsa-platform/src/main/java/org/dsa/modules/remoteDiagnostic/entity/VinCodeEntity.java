package org.dsa.modules.remoteDiagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("r_pool_vin")
public class VinCodeEntity {

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long pId;

    private String vin;

    private Long tId;

}
