package org.dsa.modules.sys.enums;


public enum RoleEnum {
    SUPER_ADMIN("超级管理员","SuperAdmin",0),
    DIAGNOSTIC_ADMIN("诊断数据中心管理员","DiagnosticAdmin",1),
    REPORTING_ADMIN("统计分析管理员","ReportingAdmin",2),
    LICENSE_ADMIN("诊断数据中心管理员","LicenseAdmin",3);


    RoleEnum(String roleName, String roleCode,Integer level) {
        this.roleName = roleName;
        this.roleCode = roleCode;
        this.level = level;
    }

    private String roleName;
    private String roleCode;
    private Integer level;

    public String getRoleName() {
        return roleName;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public Integer getLevel() {
        return level;
    }
}
