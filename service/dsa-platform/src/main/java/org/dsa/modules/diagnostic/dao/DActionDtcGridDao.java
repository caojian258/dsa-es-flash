package org.dsa.modules.diagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.dsa.modules.diagnostic.entity.DActionDtcGridEntity;


@Mapper
public interface DActionDtcGridDao extends BaseMapper<DActionDtcGridEntity> {
}
