package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class OnlineUpdateFileDto implements Serializable {

    private String path;

    private String name;

    private String dir;

    private String sessionId;

    private String deviceId;
}
