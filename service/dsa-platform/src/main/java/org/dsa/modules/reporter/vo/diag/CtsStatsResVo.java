package org.dsa.modules.reporter.vo.diag;

import lombok.Data;

@Data
public class CtsStatsResVo {

    private String collectDate;

    private String source;

    private String ctsName;

    private String ctsTitle;

    private Long total;

    private Long time;


}
