package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class VehicleDto implements Serializable {

    private Long id;

    private String vin;

    private Long vehicleTypeId;

    private String vehicleType;

    private Long vehicleTypeParentId;

    private String  productionDate;

    private String  placeOfProduction;

    private String brand;

    private String factory;

    private String model;
}
