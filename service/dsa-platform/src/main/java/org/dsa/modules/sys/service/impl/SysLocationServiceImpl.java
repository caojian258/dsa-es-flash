package org.dsa.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.dsa.modules.sys.dao.SysLocationDao;
import org.dsa.modules.sys.entity.SysLocationEntity;
import org.dsa.modules.sys.service.SysLocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("sysLocationService")
public class SysLocationServiceImpl implements SysLocationService {

    @Autowired
    SysLocationDao sysLocationDao;


    @Override
    public SysLocationEntity findByCode(Long code) {
        return sysLocationDao.findByCode(code);
    }

    @Override
    public SysLocationEntity findByNameAndType(String name, String type) {
        return sysLocationDao.findByNameAndType(name, type);
    }

    @Override
    public List<SysLocationEntity> selectByPid(Long pid) {
        return sysLocationDao.selectByPid(pid);
    }

    @Override
    public Map<Long, SysLocationEntity> getMapById(List<Long> ids) {

        List<SysLocationEntity> list = sysLocationDao.selectById(ids);
        Map<Long, SysLocationEntity> maps = new HashMap<>();

        for (SysLocationEntity entity: list) {
            maps.put(entity.getId(), entity);
        }
        return maps;
    }

    @Override
    public Map<Long, SysLocationEntity> getMapByCode(List<Long> codes) {

        List<SysLocationEntity> list = sysLocationDao.selectByCode(codes);
        Map<Long, SysLocationEntity> maps = new HashMap<>();

        for (SysLocationEntity entity: list) {
            maps.put(entity.getCode(), entity);
        }

        return maps;
    }

    @Override
    public SysLocationEntity findById(Long id) {
        QueryWrapper<SysLocationEntity > qw = new QueryWrapper<SysLocationEntity >();
        qw.eq("id", id);
        return sysLocationDao.selectOne(qw);
    }

}
