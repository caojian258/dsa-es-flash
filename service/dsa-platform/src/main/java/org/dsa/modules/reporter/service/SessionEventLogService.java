package org.dsa.modules.reporter.service;

import org.dsa.modules.reporter.entity.SessionEventLog;

public interface SessionEventLogService {

    public SessionEventLog selectOneBySessionId(String vin, String sessionId);
}
