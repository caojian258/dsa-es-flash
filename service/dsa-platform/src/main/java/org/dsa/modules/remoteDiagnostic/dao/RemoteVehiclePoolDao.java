package org.dsa.modules.remoteDiagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.*;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehiclePoolEntity;

@Mapper
public interface RemoteVehiclePoolDao extends BaseMapper<RemoteVehiclePoolEntity> {
    Page<RemoteVehiclePoolEntity> queryPage(Page<RemoteVehiclePoolEntity> page, @Param("name") String name, @Param("status") Integer status);

    @Select("select *,i.id as poolId from  r_vehicle_pool i where i.id = #{id} ")
    @Results({
//            @Result(property = "tags", column = "poolId"
//                    , many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.RemotePoolVinTagDao.getListByPool")),
            @Result(property = "tags", column = "poolId"
                    , many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.RemotePoolDependDao.selectTag")),
            @Result(property = "vehicleTypeIds", column = "poolId"
                    , many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.RemotePoolDependDao.selectType")),
            @Result(property = "versions", column = "poolId"
                    , many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.RemotePoolDependDao.selectVersion")),
            @Result(property = "ecuVersions", column = "poolId"
                    , many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.RemotePoolDependDao.selectEcu")),
    })
    RemoteVehiclePoolEntity getInfo(Long id);
}
