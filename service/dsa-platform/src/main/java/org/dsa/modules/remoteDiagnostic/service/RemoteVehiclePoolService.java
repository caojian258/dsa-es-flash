package org.dsa.modules.remoteDiagnostic.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.remoteDiagnostic.entity.RVehicleTagEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehiclePoolEntity;
import org.dsa.modules.remoteDiagnostic.entity.VinCodeEntity;
import org.dsa.modules.remoteDiagnostic.vo.CheckNameReqVo;
import org.dsa.modules.remoteDiagnostic.vo.CheckStatusReqVo;
import org.dsa.modules.remoteDiagnostic.vo.Vehicle.VehiclePoolResVo;
import org.dsa.modules.remoteDiagnostic.vo.VehiclePoolPageReVo;
import org.dsa.modules.remoteDiagnostic.vo.VinCode.VinCodeReqPageVo;
import org.dsa.modules.remoteDiagnostic.vo.VinCode.VinCodeReqVo;

import java.util.List;

public interface RemoteVehiclePoolService {

    Page<RemoteVehiclePoolEntity> selectPage(VehiclePoolPageReVo vo);

    RemoteVehiclePoolEntity getInfo(Long id);

    void savePool(RemoteVehiclePoolEntity pool);

    void updatePool(RemoteVehiclePoolEntity pool);

    List<RemoteVehiclePoolEntity> getList();

    // 检查平台是否重复
    Boolean check(CheckNameReqVo vo);

    Page<String> getVinCode(VinCodeReqPageVo vo);

    void saveVin(VinCodeReqVo vo);

    void deleteVin(VinCodeReqVo vo);

    Page<VinCodeEntity> getPoolVinCode(VinCodeReqPageVo vo);

    void batchSaveVin(VinCodeReqPageVo vo);

    Page<RVehicleTagEntity> getPoolVinCodePage(VinCodeReqPageVo vo);

    Boolean checkStatus(CheckStatusReqVo vo);
}
