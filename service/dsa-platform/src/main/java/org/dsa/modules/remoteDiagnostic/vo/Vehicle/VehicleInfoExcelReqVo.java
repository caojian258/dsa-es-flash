package org.dsa.modules.remoteDiagnostic.vo.Vehicle;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class VehicleInfoExcelReqVo {

    private String vin;

    private List<Long> tag;

    private String regular;

    private List<Long> vehicleTypeIds;

    private List<Long> vehicleIds;

}
