package org.dsa.common.validator;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.constraints.NotNull;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.exception.RRException;
import org.dsa.common.exception.RemoteApiException;
import org.dsa.common.utils.R;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.sys.service.SysCaptchaService;
import org.dsa.modules.sys.utils.SpringContextHolder;

import jakarta.servlet.http.HttpServletRequest;

import java.util.ArrayList;
import java.util.Set;

/**
 * hibernate-validator校验工具类
 * <p>
 * 参考文档：http://docs.jboss.org/hibernate/validator/5.4/reference/en-US/html_single/
 */
public class ValidatorUtils {
    private static Validator validator;

    static {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    /**
     * 校验对象
     *
     * @param object 待校验对象
     * @param groups 待校验的组
     * @throws RRException 校验不通过，则报RRException异常
     */
    public static void validateEntity(Object object, Class<?>... groups)
            throws RRException {
        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(object, groups);
        if (!constraintViolations.isEmpty()) {
            StringBuilder msg = new StringBuilder();
            for (ConstraintViolation<Object> constraint : constraintViolations) {
                msg.append(constraint.getMessage()).append("; ");
            }
            throw new RRException(msg.toString());
        }
    }

    public static void validateEntity(Integer code, Object object, Class<?>... groups)
            throws RRException {
        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(object, groups);
        if (!constraintViolations.isEmpty()) {
            StringBuilder msg = new StringBuilder();
            for (ConstraintViolation<Object> constraint : constraintViolations) {
                msg.append(constraint.getMessage()).append("; ");
            }
            throw new RRException(msg.toString(), code);
        }
    }

    public static void validateEntityByRemote(Integer code,Object data, Object object, Class<?>... groups)
            throws RRException {
        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(object, groups);
        if (!constraintViolations.isEmpty()) {
            StringBuilder msg = new StringBuilder();
            for (ConstraintViolation<Object> constraint : constraintViolations) {
                msg.append(constraint.getMessage()).append("; ");
            }
            throw new RemoteApiException(msg.toString(), code, data);
        }
    }


    /**
     * 验证码校验
     * @param request 获取HttpServletRequest 验证码的key
     * @param captcha 验证码
     * @return Boolean
     */
    public static boolean validateCaptcha(@NotNull HttpServletRequest request, @NotNull String captcha){
        RedisUtils redisUtils = SpringContextHolder.getBean(RedisUtils.class);
        SysCaptchaService sysCaptchaService = SpringContextHolder.getBean(SysCaptchaService.class);
        String key = request.getSession().getId() + "captcha";
        String uuid = redisUtils.get(key);
        if (StringUtils.isBlank(captcha)) {
            return false;
        }
        return sysCaptchaService.validate(uuid, captcha);
    }
}
