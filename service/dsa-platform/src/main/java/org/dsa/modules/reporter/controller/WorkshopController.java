package org.dsa.modules.reporter.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.utils.R;
import org.dsa.modules.reporter.dto.SqlBuilderDto;
import org.dsa.modules.reporter.entity.AnalysisCube;
import org.dsa.modules.reporter.service.AnalysisCubeService;
import org.dsa.modules.reporter.service.ClientService;
import org.dsa.modules.reporter.service.SqlBuilderService;
import org.dsa.modules.reporter.util.ArrayUtils;
import org.dsa.modules.reporter.vo.ChartReVo;
import org.dsa.modules.reporter.vo.client.ClientChartResVo;
import org.dsa.modules.sys.entity.SysWorkshopsEntity;
import org.dsa.modules.sys.service.SysWorkshopsIService;
import org.dsa.modules.sys.vo.workshop.WorkshopsPageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/workshop")
public class WorkshopController {

    @Autowired
    AnalysisCubeService cubeService;

    @Autowired
    SqlBuilderService sqlBuilderService;

    @Resource
    private SysWorkshopsIService sysWorkshopsIService;

    @Resource
    private ClientService clientService;


    @RequestMapping("/page")
    public R page(@RequestBody WorkshopsPageVo vo){

        Page<SysWorkshopsEntity> page = sysWorkshopsIService.queryWorkshopsPage(vo);
        return R.ok().put("data", page);
    }

    @RequestMapping(value = "/chart", method = {RequestMethod.POST })
    public R chart(@Valid @RequestBody ChartReVo vo){

        Map<String, String> tableMap = new HashMap<>();
        tableMap.put("workshop", "d_workshop");
        tableMap.put("workshop_group", "d_workshop_group");
        tableMap.put("workshop_user", "d_workshop_user");
        tableMap.put("workshop_user_group", "d_workshop_user_group");
        tableMap.put("client", "client_os_overview");

        AnalysisCube cube = null;

        if(null != vo.getCode()){
            cube = cubeService.getCubeByCode(vo.getCode());
        }else{
            cube = cubeService.getCube(tableMap.get(vo.getCategory()), vo.getDimension(), vo.getChart());
        }
        Map<String, Object> map = null;
        if(!CollectionUtil.isEmpty(vo.getCondition())) {
            map = ArrayUtils.keyToLowerCase(vo.getCondition());
            if (null == map.get("workshopgroup") || "[]".equals(map.get("workshopgroup").toString())) {
                map.put("workshopgroup", null);
            }
        }
        if(StrUtil.isNotBlank(vo.getStartDate())){
            if(null == map){
                map = new HashMap<>();
            }
            map.put("startDate", vo.getStartDate());
        }
        SqlBuilderDto sqlBuilderDto = new SqlBuilderDto();
        BeanUtil.copyProperties(cube,sqlBuilderDto);

        String sql = sqlBuilderService.render(sqlBuilderDto, map, vo.getTop());
        log.info("workshop chart sql: {}", sql);
        List<Map<String, Object>> items =  cubeService.getData(sql);
        ClientChartResVo resVo = cubeService.formatChartData(items, vo.getDimension(), vo.getChart());
        return R.ok().put("data", resVo);
    }

    @RequestMapping(value = "/clientChart", method = {RequestMethod.POST })
    public R clientChart(@Valid @RequestBody ChartReVo vo){

        List<Map<String, Object>> items =  clientService.clientWorkshopStatics(vo);
        ClientChartResVo resVo = cubeService.formatChartData(items, vo.getDimension(), vo.getChart());
        return R.ok().put("data", resVo);
    }
}
