package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.entity.VehicleVersionOverview;

import java.util.List;

@Mapper
public interface VehicleVersionOverviewMapper extends BaseMapper<VehicleVersionOverview> {

    void inserts(@Param("overviews") List<VehicleVersionOverview> overviews);

}
