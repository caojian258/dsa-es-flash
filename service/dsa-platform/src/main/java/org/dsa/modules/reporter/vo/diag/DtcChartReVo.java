package org.dsa.modules.reporter.vo.diag;


import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString(callSuper = true)
public class DtcChartReVo {

    private List<String> dates;

    private String startTime;

    private String endTime;

    private String vin;

    private List<Long> vehType;

    private Integer top;

    private List<String> ecuNames;

    private Integer pageNo;

    private Integer pageSize;
}
