package org.dsa.modules.reporter.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RecordFlagEnum {

    first(1),
    last(2),
    both(3);

    @EnumValue
    private final Integer value;
}
