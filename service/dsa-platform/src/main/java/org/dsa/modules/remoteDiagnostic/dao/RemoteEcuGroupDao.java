package org.dsa.modules.remoteDiagnostic.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;
import org.dsa.modules.remoteDiagnostic.entity.RemoteEcuGroupEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuEntity;

import java.util.List;

@Mapper
public interface RemoteEcuGroupDao extends BaseMapper<RemoteEcuGroupEntity> {

    @Insert("<script>"
            + " insert into r_ecu_group (group_id,ecu_id) values "
            + " <foreach collection='ecus' item='item' separator=','> "
            + " (#{groupId},#{item.id}) "
            + " </foreach>"
            + " ON conflict(ecu_id,group_id)  DO NOTHING "
            + " </script>")
    void inserts(@Param("groupId") Long id, @Param("ecus") List<RemoteVehicleEcuEntity> ecus);

    @Delete("delete from r_ecu_group i where i.group_id = #{groupId} and i.ecu_id in (select j.id from r_vehicle_ecu j where j.ecu_name ilike concat('%',#{name},'%')) ")
    void deleteByName(@Param("groupId") Long id,@Param("name") String ecuName);
}
