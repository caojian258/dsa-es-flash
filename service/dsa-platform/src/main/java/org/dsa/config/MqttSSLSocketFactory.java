package org.dsa.config;

import lombok.Data;
import org.dsa.common.utils.SSLTrustManager;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.net.ssl.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;

/**
 *
 * @Title: MqttSSLSocketFactory.java
 * @description:MQTTSSL证书工厂类
 */
@Component
@ConfigurationProperties(prefix = "mqtt-properties")
@Data
public class MqttSSLSocketFactory {

    private String keyStore;
    private String keyStorePassword;

    /**
     * 创建SSLSocketFactory
     * @return
     * @throws Exception
     */
    public SSLSocketFactory getSSLSocketFactory() throws Exception {
        KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
        kmf.init(getkeyStore(),keyStorePassword.toCharArray());
        KeyManager[] keyManagers = kmf.getKeyManagers();

        SSLTrustManager tm = new SSLTrustManager();
        TrustManager[] trustAllCerts = new TrustManager[]{tm};

        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(keyManagers, trustAllCerts, new SecureRandom());
        return sslContext.getSocketFactory();

    }

    public KeyStore getkeyStore(){
        KeyStore keySotre=null;
        try {
            keySotre = KeyStore.getInstance("PKCS12");
            File file = new File(keyStore);
            FileInputStream fis = new FileInputStream(file);
            keySotre.load(fis, keyStorePassword.toCharArray());
            fis.close();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return keySotre;
    }
}
