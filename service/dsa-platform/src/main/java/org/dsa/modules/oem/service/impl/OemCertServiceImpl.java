package org.dsa.modules.oem.service.impl;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.utils.R;
import org.dsa.modules.oem.config.OemCertComponent;
import org.dsa.modules.oem.service.OemCertService;
import org.dsa.modules.oem.vo.SWTSGetCertReqVo;
import org.dsa.modules.oem.vo.WhiteSyncReqVo;
import org.dsa.modules.reporter.util.LocalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class OemCertServiceImpl implements OemCertService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private OemCertComponent oemCertComponent;


    @Override
    public Map<String, Object> getCert(SWTSGetCertReqVo vo) {
        Gson gson = new Gson();

        HashMap<String, String> req = new HashMap<>();
        req.put("vehicle_project_id", ""); //车系id  @TODO 待确认值
        req.put("device_id", vo.getPcid());
        HashMap<String, Object> rps = restTemplate.postForObject(oemCertComponent.getCert().getUrl(), gson.toJson(req), HashMap.class);
        HashMap<String, Object> result;
        if (rps != null) {
            if (rps.containsKey("code")) {
                result = getResult(rps.get("code"), LocalUtil.get("message.699") + ":" + rps.get("msg"), "");
            } else {
                result = getResult(0, LocalUtil.get("message.699") + ":" + rps.get("msg"), rps.get("cert"));
            }
        } else {
            result = R.error(LocalUtil.get("message.698"));
            result.put("key", "");
            result.put("ca", "");
            result.put("cert", "");
        }
        return result;
    }

    private HashMap<String, Object> getResult(Object code, Object msg, Object data) {
        HashMap<String, Object> result = new HashMap<>();
        result.put("key", "");
        result.put("ca", "");
        result.put("cert", data);

        HashMap<String, Object> map = new HashMap<>();
        map.put("code", code);
        map.put("msg", msg);
        map.put("data", result);
        return map;
    }

    private HashMap<String, Object> getResult2(Object code, Object msg) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("code", code);
        map.put("msg", msg);
        return map;
    }

    @Override
    public Map<String, Object> CSRCertification(SWTSGetCertReqVo vo) {
        Gson gson = new Gson();

        HashMap<String, String> req = new HashMap<>();
        req.put("vehicle_project_id", ""); //车系id  @TODO 待确认值
        req.put("device_id", vo.getPcid());
        HashMap<String, Object> rps = restTemplate.postForObject(oemCertComponent.getCert().getUrl(), gson.toJson(req), HashMap.class);
        HashMap<String, Object> result;
        if (rps != null) {
            if (rps.containsKey("code")) {
                result = getResult(rps.get("code"), LocalUtil.get("message.699") + ":" + rps.get("msg"), "");
            } else {
                result = getResult(0, LocalUtil.get("message.699") + ":" + rps.get("msg"), rps.get("cert"));
            }
        } else {
            result = R.error(LocalUtil.get("message.698"));
            result.put("key", "");
            result.put("ca", "");
            result.put("cert", "");
        }
        return result;
    }

    @Override
    public Map<String, Object> whiteListSync(WhiteSyncReqVo vo) {
        Gson gson = new Gson();

        ArrayList<Object> reqList = new ArrayList<>();
        HashMap<String, Object> req = new HashMap<>();
        HashMap<String, String> reqMap = new HashMap<>();
        reqMap.put("name", vo.getName());
        reqMap.put("type", vo.getType());
        reqMap.put("serialNum", vo.getPcid()); //设备序列号
        reqList.add(reqMap);

        req.put("operationType", "add");
        req.put("list", reqList);
//        HashMap<String, Object> rps = restTemplate.postForObject(oemCertComponent.getCert().getWhiteSyncUrl(), gson.toJson(req), HashMap.class);
        HashMap<String, Object> rps = null;
        HashMap<String, Object> result;
        if (rps != null) {
            result = getResult2(rps.get("resultCode"), rps.get("resultMsg"));
        } else {
            result = R.error(LocalUtil.get("message.698"));
        }
        return result;
    }
}
