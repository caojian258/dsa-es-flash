package org.dsa.modules.reporter.dto;

import lombok.Data;


@Data
public class SessionCts {

    private String ctsName;

    private String startTimestamp;

    private String endTimestamp;

    private String returnValue;

    private String returnMessage;

    private Integer failureCode;

}
