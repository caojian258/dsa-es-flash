package org.dsa.modules.diagnostic.dto;

import lombok.Getter;
import lombok.Setter;
import org.dsa.modules.diagnostic.entity.DDiagPackageVersionEntity;

import java.io.Serializable;

/**
 * @author weishunxin
 * @since 2023-06-08
 */
@Setter
@Getter
public class DDiagPackageVersionDTO extends DDiagPackageVersionEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    private String odxVersionNum;

    private String otxVersionNum;


}
