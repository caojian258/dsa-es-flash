package org.dsa.modules.reporter.service.impl;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.reporter.dao.CtsDailyMapper;
import org.dsa.modules.reporter.dto.CtsDailyDto;
import org.dsa.modules.reporter.entity.CtsDaily;
import org.dsa.modules.reporter.service.CtsDailyService;
import org.dsa.modules.reporter.vo.diag.DiagnosticPageReVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CtsDailyServiceImpl implements CtsDailyService {

    @Autowired
    CtsDailyMapper ctsDailyMapper;

    @Override
    public Page<CtsDailyDto> selectPage(DiagnosticPageReVo vo) {
        Page<CtsDaily>  page = new Page<>(vo.getPageNo(), vo.getPageSize());

        OrderItem oi = new OrderItem("cts_day", false);
        page.addOrder(oi);

        Page<CtsDailyDto> result = ctsDailyMapper.selectPageByDate(page,
                vo.getCtsName(),
                vo.getVin(),
                vo.getSources(),
                vo.getVehicleTypeIds(),
                vo.getStartTime(),
                vo.getEndTime());

        return result;
    }

    @Override
    public void insertData(CtsDaily entity) {
        ctsDailyMapper.oneInsert(entity);
    }

    @Override
    public void upsertData(CtsDaily entity) {
        ctsDailyMapper.oneUpsert(entity);
    }

    @Override
    public void deleteData(String vin, String day) {
        ctsDailyMapper.deleteData(vin, day);
    }

}
