package org.dsa.common.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.exception.RRException;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Scanner;

/**
 * @author weishunxin
 * @since 2023-06-06
 */
@Slf4j
@Service
public class LinuxService {

    public String runCommand(String command){
        log.info("run command {}",command);
        //命令执行返回数据流
        Scanner input;
        //命令执行接收结果
        Process process = null;
        //执行结果
        StringBuilder result = new StringBuilder();
        String res;
        try {
            //执行脚本
            process = Runtime.getRuntime().exec(command );
            //等命令执行结果
            process.waitFor();
            //取执行结果数据流
            InputStream is = process.getInputStream();
            //数据流转换
            input = new Scanner(is);
            while (input.hasNextLine()){
                String r = input.nextLine();
                log.info("run command result -r:{}",r);
                if(StringUtils.isNotEmpty(r) && !"null".equals(r)){
                    result.append(r);
                }
            }
            //关闭数据流
            is.close();
            //关闭转换
            input.close();

            res = result.toString();
            if(StringUtils.isEmpty(res) ){
                throw new RRException("run command failed.");
            }
            log.info("run command result:{}", res);
        } catch (IOException | InterruptedException e) {
            log.error("IOException:{}", e.getMessage(), e);
            throw new RRException("run command failed.");
        } finally {
            //关闭命令执行线程
            if (Objects.nonNull(process)) {
                process.destroy();
            }
        }
        return res;
    }

}
