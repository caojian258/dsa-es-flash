package org.dsa.modules.remoteDiagnostic.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.common.validator.group.UpdateGroup;
import org.dsa.modules.remoteDiagnostic.pojo.BaseEntity;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@ToString
@TableName("r_vehicle_ecu")
public class RemoteVehicleEcuEntity extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;

    @TableField(exist = false)
    private Long groupId;

    @TableField(exist = false)
    private Long ygroupId;

    @NotBlank(message = "ECU名称不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String ecuName;

    private String descriptionTi;

    private String shortNameTi;

    @NotNull(message = "状态不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private Integer status;

    @TableField(exist = false)
    private Long platformId;

    @TableField(exist = false)
    private List<RemoteEcuIdentificationInfoEntity> identifications;

    @TableField(exist = false)
    private List<RemoteVehicleEcuEntity> ecuVersions;

    @TableField(exist = false)
    private Long versionId;

    @TableField(exist = false)
    private Long flag;

    @TableField(exist = false)
    private RemoteVehicleEcuImageEntity image;

    public RemoteVehicleEcuEntity() {
    }

    public RemoteVehicleEcuEntity(Long id, Long versionId) {
        this.id = id;
        this.versionId = versionId;
    }
}
