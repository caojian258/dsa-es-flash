package org.dsa.modules.reporter.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum FileCateEnum {

    SQLITE("Sqlite"),
    STATUS("Status"),
    SharedData("ShareData"),
    WORKSHOP("Workshop"),
    WORKSHOPUSER("WorkshopUser"),

    EVENT("Event"),

    SessionState("SessionState"),

    AsMaintained("AsMaintained"),

    OnlineUpdate("OnlineUpdate"),

    OTHER("Other");


    @EnumValue
    private final String value;
}
