package org.dsa.modules.reporter.dto;

import lombok.Data;
import lombok.ToString;
import org.dsa.modules.reporter.entity.SystemProtocolItem;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/***
 * 安装应用的信息
 */
@Data
@ToString
public class AppsInfoDto implements Serializable {

    private List<AppInstallDto> installDtos;

    private List<AppVersionDto> versionDtos;

    private List<AppUnInstallDto> unInstallDtos;


    /**
     * 经销商ID
     */
    private String workshop;

    /***
     * 电脑ID
     */
    private String pcid;

    /***
     * 安装失败统计
     */
    private int failedInstallationCount;

    /***
     * 安装成功统计
     */
    private int okInstallationCount;

    /***
     * 异常统计
     */
    private int errorExceptionCount;


    /***
     * 收集时间
     */
    private String timestampText;

    /***
     * 收集时间
     */
    private Date timestamp;
}
