package org.dsa.modules.sys.enums;

/**
 * @author weishunxin
 * @since 2023-06-20
 */
public enum NoticeStatusEnum {

    NOT_EFFECTIVE(0, "未生效"),
    EFFECTIVE(1, "已生效"),
    CLOSE(2, "下架"),
    EXPIRED(3, "失效");

    private final Integer code;
    private final String name;

    NoticeStatusEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }


    public static boolean contains(Integer code) {
        if (code == null) {
            return false;
        }

        for (NoticeStatusEnum value : NoticeStatusEnum.values()) {
            if (code.equals(value.getCode())) {
                return true;
            }
        }

        return false;
    }

}
