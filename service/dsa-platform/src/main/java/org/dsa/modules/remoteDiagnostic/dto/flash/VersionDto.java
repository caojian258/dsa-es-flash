package org.dsa.modules.remoteDiagnostic.dto.flash;

import lombok.Data;

import java.io.Serializable;

@Data
public class VersionDto implements Serializable {

    private String softwareVersion;

    private String hardwareVersion;

    private Long versionId;

    private String partNumber;
}
