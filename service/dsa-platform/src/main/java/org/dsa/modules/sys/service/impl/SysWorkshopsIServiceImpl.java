package org.dsa.modules.sys.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.read.listener.PageReadListener;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.dsa.modules.reporter.util.LocalUtil;
import org.dsa.modules.sys.dao.SysWorkshopsDao;
import org.dsa.modules.sys.dto.WorkshopExcelDto;
import org.dsa.modules.sys.dto.WorkshopImportMsgDto;
import org.dsa.modules.sys.entity.SysLocationEntity;
import org.dsa.modules.sys.entity.SysWorkshopsEntity;
import org.dsa.modules.sys.service.SysLocationService;
import org.dsa.modules.sys.service.SysUserService;
import org.dsa.modules.sys.service.SysWorkshopsIService;
import org.dsa.modules.sys.vo.workshop.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jakarta.annotation.Resource;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Slf4j
@Service("sysWorkshopsIService")
public class SysWorkshopsIServiceImpl extends ServiceImpl<SysWorkshopsDao, SysWorkshopsEntity> implements SysWorkshopsIService {

    @Resource
    SysLocationService sysLocationService;

    @Resource
    SysUserService sysUserService;

    @Value("${file.template.path}")
    private String templatePath;

    private static final String countryName = "中国";

    private static final Long countryCode = 100000L;

    @Override
    public Map<String, String> excelTemplate() {
        String templateName = "workshops_template.xlsx";
        String template = templatePath + templateName;
        List<WorkshopFillVo> voList = new ArrayList<>(1);
        WorkshopFillVo fillVo = new WorkshopFillVo();
        fillVo.setNo("1");
        fillVo.setCode("SHOP123");
        fillVo.setShortName("经销商简称");
        fillVo.setName("经销商名称");
        fillVo.setCountryName("中国");
        fillVo.setRegion("北区");
        fillVo.setProvinceName("吉林省");
        fillVo.setCityName("长春市");
        voList.add(fillVo);
        String name = "workshop_template.xlsx";
        String path = templatePath + "workshop_template.xlsx";
        EasyExcel.write(path).withTemplate(template).sheet().doFill(voList);
        Map<String, String> map = new HashMap<>();
        map.put("name", name);
        map.put("path", path);

        return map;
    }

    @Override
    public Map<String, String> exportToExcel(WorkshopClientVo vo) {
        String templateName = "workshops_template.xlsx";
        String template = templatePath + templateName;
        Date today = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        String name = "workshops_"+sdf.format(today)+".xlsx";
        String path = templatePath + name;
        Map<String, String> map = new HashMap<>();
        map.put("name", name);
        map.put("path", path);

        List<SysWorkshopsEntity> entityList = selectWorkshop(vo);
        List<WorkshopFillVo> voList = new ArrayList<>(entityList.size());
        int i = 1;
        for (SysWorkshopsEntity entity: entityList) {
            WorkshopFillVo fillVo = new WorkshopFillVo();
            BeanUtil.copyProperties(entity, fillVo);
            fillVo.setNo(String.valueOf(i++));
            voList.add(fillVo);
        }
        EasyExcel.write(path).withTemplate(template).sheet().doFill(voList);
        return map;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public WorkshopImportMsgDto importFromExcel(String path) {

        String lang = LocaleContextHolder.getLocale().toLanguageTag();
        WorkshopImportMsgDto dto = new WorkshopImportMsgDto();
        dto.setTotal(0);
        dto.setSuccessTotal(0);
        dto.setFailedTotal(0);
        AtomicInteger total = new AtomicInteger();
        AtomicInteger success = new AtomicInteger();
        AtomicInteger failed = new AtomicInteger();

        EasyExcel.read(path, WorkshopExcelDto.class, new PageReadListener<WorkshopExcelDto>(dataList -> {

            for (WorkshopExcelDto demoData : dataList) {

                if(null != demoData.getNo()){
                    total.addAndGet(1);
                }
                if(null != demoData && StringUtils.isNotBlank(demoData.getName())){
                    SysWorkshopsEntity entity = baseMapper.findByName(demoData.getName());
                    if(entity != null){
                        failed.addAndGet(1);
                        continue;
                    }

                    if(null == demoData.getProvince() || StringUtils.isBlank(demoData.getProvince()) ||
                       null == demoData.getCity() || StringUtils.isBlank(demoData.getCity())){
                        failed.addAndGet(1);
                        continue;
                    }

                    Long code = 100000L;
                    String p = demoData.getProvince().trim().replace("自治区", "");
                    SysLocationEntity province = sysLocationService.findByNameAndType(p, "1");
                    SysLocationEntity city =  sysLocationService.findByNameAndType(demoData.getCity().trim(), "2");
                    if(null == city){
                        SysLocationEntity  district =  sysLocationService.findByNameAndType(demoData.getCity().trim(), "3");
                        if(null != district){
                            city =  sysLocationService.findById(district.getPid());
                        }
                    }
                    SysWorkshopsEntity workshopsEntity = new SysWorkshopsEntity();
                    workshopsEntity.setCode(demoData.getCode());
                    workshopsEntity.setName(demoData.getName());
                    workshopsEntity.setShortName(demoData.getShortName());
                    workshopsEntity.setRegion(demoData.getRegion());
                    if(null != city){
                        workshopsEntity.setCityCode(city.getCode());
                        workshopsEntity.setCityName(city.getName());
                    }else{
                        workshopsEntity.setCityName(demoData.getCity().trim());
                    }

                    if(null != province){
                        workshopsEntity.setProvinceCode(province.getCode());
                        workshopsEntity.setProvinceName(province.getName());
                    }else{
                        workshopsEntity.setProvinceName(p);
                    }

                    if(null != demoData.getNote()){
                        workshopsEntity.setNote(demoData.getNote().trim());
                    }
                    workshopsEntity.setCountryCode(code);
                    workshopsEntity.setCountryName(demoData.getCountry());
                    workshopsEntity.setNote(demoData.getNote());
                    workshopsEntity.setCreateTime(new Date());
                    workshopsEntity.setUpdateTime(new Date());
                    baseMapper.insert(workshopsEntity);
                    success.addAndGet(1);
                }
            }
        })).headRowNumber(3).sheet().doRead();

        String msg = LocalUtil.get("workshop.msg");
        dto.setTotal(total.intValue());
        dto.setFailedTotal(failed.intValue());
        dto.setSuccessTotal(success.intValue());
        dto.setMsg(String.format(msg, dto.getTotal(), dto.getSuccessTotal(), dto.getFailedTotal()));
        return dto;
    }

    @Override
    public SysWorkshopsEntity findById(Long id) {
        SysWorkshopsEntity entity = new SysWorkshopsEntity();
        entity =  baseMapper.findById(id);
        if(null != entity){
            List<Long> codes = new ArrayList<>();
            if(null != entity.getProvinceCode()){
                codes.add(entity.getProvinceCode());
            }
            if(null != entity.getCityCode()){
                codes.add(entity.getCityCode());
            }
            Map<Long, SysLocationEntity> maps = sysLocationService.getMapByCode(codes);
            if(CollectionUtil.isNotEmpty(maps)){
                entity.setProvinceId(maps.get(entity.getProvinceCode()).getId());
                entity.setCityId(maps.get(entity.getCityCode()).getId());
            }
        }
        return entity;
    }

    @Override
    public SysWorkshopsEntity findByName(String name) {
        return baseMapper.findByName(name);
    }

    @Override
    public Page<SysWorkshopsEntity> queryPage(WorkshopPageVo vo) {

        Page<SysWorkshopsEntity> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);

        LambdaQueryWrapper<SysWorkshopsEntity> qw = Wrappers.<SysWorkshopsEntity>lambdaQuery();

        if(StringUtils.isNotEmpty(vo.getName())){
            qw.and(( wrapper ) -> {
                wrapper.apply("short_name"+" ilike {0}", "%"+vo.getName().trim()+"%")
                        .or()
                        .apply("name"+" ilike {0}", "%"+vo.getName().trim()+"%");
            });
        }

        if(StringUtils.isNotEmpty(vo.getCode())){
            if(vo.getCode().equals("N/A")){
                qw.apply("(code is null or code = '' or code= 'N/A')");
            }else{
                qw.apply("code"+" ilike {0}", "%"+vo.getCode().trim()+"%");
            }
        }

        if(null != vo.getRegion() && StrUtil.isNotBlank(vo.getRegion())){
            if(vo.getRegion().equals("N/A")){
                qw.apply("(region is null or region = '' or region= 'N/A')");
            }else{
                qw.eq(SysWorkshopsEntity::getRegion, vo.getRegion());
            }
        }

        if(null != vo.getCity()){
            qw.eq(SysWorkshopsEntity::getCityCode, vo.getCity());
        }

        if(null != vo.getProvince()){
            qw.eq(SysWorkshopsEntity::getProvinceCode, vo.getProvince());
        }

        if(null != vo.getDelFlag()){
            qw.eq(SysWorkshopsEntity::getDelFlag, vo.getDelFlag());
        }

        Page<SysWorkshopsEntity> result =  baseMapper.selectPage(page, qw);
        return result;
    }

    @Override
    public Page<SysWorkshopsEntity> queryWorkshopsPage(WorkshopsPageVo vo) {
        Page<SysWorkshopsEntity> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);

        LambdaQueryWrapper<SysWorkshopsEntity> qw = Wrappers.<SysWorkshopsEntity>lambdaQuery();

        if(null != vo.getCountry() && StrUtil.isNotBlank(vo.getCountry())){
            if(vo.getCountry().equals("N/A")){
                qw.apply("(country_name is null or country_name = '' or country_name = 'N/A')");
            }else{
                qw.eq(SysWorkshopsEntity::getCountryName, vo.getCountry());
            }
        }

        if(StringUtils.isNotEmpty(vo.getName())){
            qw.and(( wrapper ) -> {
                wrapper.apply("short_name"+" ilike {0}", "%"+vo.getName().trim()+"%")
                        .or()
                        .apply("name"+" ilike {0}", "%"+vo.getName().trim()+"%");
            });
        }

        if(StringUtils.isNotEmpty(vo.getCode())){
            if(vo.getCode().equals("N/A")){
                qw.apply("(code is null or code = '' or code= 'N/A')");
            }else{
                qw.apply("code"+" ilike {0}", "%"+vo.getCode().trim()+"%");
            }
        }

        if(null != vo.getRegion() && StrUtil.isNotBlank(vo.getRegion())){
            if(vo.getRegion().equals("N/A")){
                qw.apply("(region is null or region = '' or region = 'N/A')");
            }else{
                qw.eq(SysWorkshopsEntity::getRegion, vo.getRegion());
            }
        }

        if(null != vo.getCity() && StrUtil.isNotBlank(vo.getCity())){
            if(vo.getCity().equals("N/A")){
                qw.apply("(city_name is null or city_name = '' or city_name = 'N/A')");
            }else{
                qw.eq(SysWorkshopsEntity::getCityName, vo.getCity());
            }
        }

        if(null != vo.getProvince() && StrUtil.isNotBlank(vo.getProvince())){
            if(vo.getProvince().equals("N/A")){
                qw.apply("(province_name is null or province_name = '' or province_name = 'N/A')");
            }else{
                qw.eq(SysWorkshopsEntity::getProvinceName, vo.getProvince());
            }
        }

        if(null != vo.getDelFlag()){
            qw.eq(SysWorkshopsEntity::getDelFlag, vo.getDelFlag());
        }

        Page<SysWorkshopsEntity> result =  baseMapper.selectPage(page, qw);
        return result;
    }

    @Override
    public void disabledById(Long id) {
        SysWorkshopsEntity workshop = baseMapper.findById(id);
        if(null != workshop && workshop.getDelFlag() == 0){
            workshop.setDelFlag(1);
            baseMapper.updateById(workshop);
            List<Long> userIds = sysUserService.queryUserIdByWorkshopId(id);
            if(userIds != null && userIds.size() > 0) {
                Long[] userIdArray= new Long[userIds.size()];
                sysUserService.deleteBatch(userIds.toArray(userIdArray));
            }

        }

    }

    @Override
    public void saveWorkshop(WorkshopVo vo) {
        SysWorkshopsEntity entity = new SysWorkshopsEntity();
        BeanUtil.copyProperties(vo, entity);

        entity.setCountryCode(countryCode);
        entity.setCountryName(countryName);

        if(CollectionUtil.isNotEmpty(vo.getLocation())){
            Map<Long, SysLocationEntity> maps =  sysLocationService.getMapById(vo.getLocation());
            entity.setProvinceCode(maps.get(vo.getLocation().get(0)).getCode());
            entity.setProvinceName(maps.get(vo.getLocation().get(0)).getName());
            entity.setCityCode(maps.get(vo.getLocation().get(1)).getCode());
            entity.setCityName(maps.get(vo.getLocation().get(1)).getName());
        }

        if(null != vo.getNote()){
            entity.setNote(vo.getNote());
        }

        if(null != entity.getId()){
            entity.setUpdateTime(new Date());

            baseMapper.updateById(entity);
        }else{
            entity.setCreateTime(new Date());
            entity.setUpdateTime(new Date());
            baseMapper.insert(entity);
        }

    }

    @Override
    public List<SysWorkshopsEntity> selectWorkshop(WorkshopClientVo vo) {
        return baseMapper.selectEnableWorkshops(vo.getCountry(), vo.getProvince(), vo.getCity(), vo.getRegion(), vo.getName(), vo.getStatus());
    }

    @Override
    public String queryWorkshopNameById(Long id) {
        SysWorkshopsEntity entity = baseMapper.findById(id);
        if(null != entity){
            return entity.getName();
        }
        return null;
    }

    @Override
    public boolean checkWorkshopCode(WorkshopCodeValidatorVo code) {
        SysWorkshopsEntity entity =  baseMapper.findByCode(code.getCode());
        if(null != entity && !entity.getId().equals(code.getWorkshopId())){
            return true;
        }
        return false;
    }

    @Override
    public void enabledById(Long id) {
        SysWorkshopsEntity workshop = baseMapper.findById(id);
        if(null != workshop && workshop.getDelFlag() == 1){
            workshop.setDelFlag(0);
            baseMapper.updateById(workshop);
            List<Long> userIds = sysUserService.queryUserIdByWorkshopId(id);
            if(userIds != null && userIds.size() > 0) {
                Long[] userIdArray= new Long[userIds.size()];
                sysUserService.enableBatch(userIds.toArray(userIdArray));
            }
        }
    }

    @Override
    public List<String> regions() {
        List<String> regions = baseMapper.queryRegion();
        List<String> myList = regions.stream().distinct().collect(Collectors.toList());
        return myList;
    }
}
