package org.dsa.modules.reporter.vo.client;

import lombok.Data;
import org.apache.poi.ss.formula.functions.T;
import org.dsa.modules.reporter.dto.ChartItemDto;

import java.util.List;
import java.util.Set;

@Data
public class ClientChartResVo {

    /***
     * X轴数据列表
     */
    private Set<String> xAxisData;

    /***
     * 分类数据列表
     */
    private Set<String> legendData;

    /***
     * 实际的数据列表
     */
    private List<?> seriesData;

    /***
     * 数据合计
     */
    private Long total;

    private Long itemTotal;
}
