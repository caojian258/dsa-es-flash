package org.dsa.modules.reporter.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Objects;

@Data
@Accessors(chain = true)
@TableName("client_dependence_tree")
public class ClientDependenceTree extends BaseEntity{

    @TableId(type = IdType.AUTO)
    private Long id;

    private String pcid;

    private Integer appId;

    private String appType;

    private String appName;

    private String appVersion;

    private Integer appVersionNumber;

    private Integer dependencyId;

    private String dependencyType;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientDependenceTree dependenceTree = (ClientDependenceTree) o;
        return Objects.equals(appId, dependenceTree.appId) &&
                Objects.equals(appVersion, dependenceTree.appVersion) &&
                Objects.equals(dependencyId, dependenceTree.dependencyId) &&
                Objects.equals(pcid, dependenceTree.pcid) &&
                Objects.equals(dependencyType, dependenceTree.dependencyType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pcid, appId, appVersion, dependencyId, dependencyType);
    }

}
