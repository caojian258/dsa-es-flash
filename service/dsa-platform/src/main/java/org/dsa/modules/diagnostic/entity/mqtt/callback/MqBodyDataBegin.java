package org.dsa.modules.diagnostic.entity.mqtt.callback;

import lombok.Data;

import java.io.Serializable;

/**
 * mq body data Begin
 *
 */
@Data
public class MqBodyDataBegin implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * vin码
     */
    private String vin;
    /**
     * 会话id
     */
    private String sessionId;


}
