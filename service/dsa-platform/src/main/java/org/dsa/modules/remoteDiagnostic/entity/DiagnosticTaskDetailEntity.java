package org.dsa.modules.remoteDiagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;
import org.dsa.modules.remoteDiagnostic.pojo.BaseEntity;

import java.io.Serializable;
import java.util.Date;

@Data
@ToString
@TableName("r_diag_task_detail")
public class DiagnosticTaskDetailEntity extends BaseEntity implements Serializable {

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long campaignId;

    private String sessionId;

    private String type;

    private Date startTime;

    private Date endTime;

    private Integer action;

    private String code;

    private String message;

    private String vin;

    public DiagnosticTaskDetailEntity() {
    }

    public DiagnosticTaskDetailEntity(Long campaignId, String sessionId, String type,String code, String message, String vin) {
        this.campaignId = campaignId;
        this.sessionId = sessionId;
        this.type = type;
        this.message = message;
        this.code = code;
        this.vin = vin;
    }

    public DiagnosticTaskDetailEntity(Long campaignId, String sessionId, String type,String code, String message, String vin,Date startTime,Date endTime) {
        this.campaignId = campaignId;
        this.sessionId = sessionId;
        this.type = type;
        this.message = message;
        this.code = code;
        this.vin = vin;
        this.startTime = startTime;
        this.endTime = endTime;
    }

}
