package org.dsa.common.filter;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

/**
 * Author: lyf
 * Date: 2023/9/20
 * Description:
 */
@WebFilter(urlPatterns = "/*", filterName = "responseHeadFilter")
public class ResponseHeadFilter  implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Filter.super.init(filterConfig);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        //增加响应头缺失代码
        HttpServletRequest req=(HttpServletRequest)servletRequest;
        HttpServletResponse res=(HttpServletResponse)servletResponse;
        res.addHeader("X-Frame-Options","SAMEORIGIN");
        res.addHeader("Referrer-Policy","origin");
        res.addHeader("Content-Security-Policy","object-src 'self'");
        res.addHeader("X-Permitted-Cross-Domain-Policies","master-only");
        res.addHeader("X-Content-Type-Options","nosniff");
        res.addHeader("X-XSS-Protection","1; mode=block");
        res.addHeader("X-Download-Options","noopen");
        res.addHeader("Strict-Transport-Security","max-age=63072000; includeSubdomains; preload");
        //处理cookie问题
        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                String value = cookie.getValue();
                StringBuilder builder = new StringBuilder();
                builder.append(cookie.getName()+"="+value+";");
                builder.append("Secure;");//Cookie设置Secure标识
                builder.append("HttpOnly;");//Cookie设置HttpOnly
                res.addHeader("Set-Cookie", builder.toString());
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
        Filter.super.destroy();
    }
}
