package org.dsa.modules.onlineUpdate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.dsa.modules.onlineUpdate.entity.SoftwareVersionEntity;
import org.dsa.modules.onlineUpdate.entity.SwVCompVEntity;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class SoftwareVersionDto extends SoftwareVersionEntity {

    /**
     * 关联组件版本
     */
    private List<SwVCompVEntity> groups;
    /**
     * 软件启动入口 componentId;
     */
    public Long componentId;


}
