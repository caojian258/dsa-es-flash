package org.dsa.modules.reporter.service.impl;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.reporter.dao.DtcDailyMapper;
import org.dsa.modules.reporter.dto.DtcDailyDto;
import org.dsa.modules.reporter.entity.DtcDaily;
import org.dsa.modules.reporter.service.DtcDailyService;
import org.dsa.modules.reporter.vo.diag.DtcChartReVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DtcDailyServiceImpl implements DtcDailyService {

    @Autowired
    DtcDailyMapper dtcDailyMapper;


    @Override
    public Page<DtcDailyDto> selectPage(DtcChartReVo vo) {
        Page<DtcDaily> page = new Page<>(vo.getPageNo(), vo.getPageSize());
        OrderItem oi = new OrderItem("dtc_day", false);
        page.addOrder(oi);

        Page<DtcDailyDto> result = dtcDailyMapper.selectPageByDate(page, vo.getVin(), vo.getEcuNames(), vo.getStartTime(), vo.getEndTime(), vo.getVehType());
        return result;
    }

    @Override
    public void insertData(DtcDaily entity) {
        dtcDailyMapper.oneInsert(entity);
    }

    @Override
    public void upsertData(DtcDaily entity) {
        dtcDailyMapper.oneUpsert(entity);
    }

    @Override
    public void deleteData(String vin, String day) {

    }

    @Override
    public List<String> findAllEcu() {
        return dtcDailyMapper.findAllEcu();
    }
}
