package org.dsa.modules.reporter.handler;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.reporter.constant.RedisConstant;
import org.dsa.modules.reporter.constant.VehicleConstant;
import org.dsa.modules.reporter.dao.FileLogMapper;
import org.dsa.modules.reporter.dao.VehicleFlashDetailMapper;
import org.dsa.modules.reporter.dao.VehicleFlashRecordMapper;
import org.dsa.modules.reporter.dto.AsMaintainedJsonDto;
import org.dsa.modules.reporter.entity.FileLog;
import org.dsa.modules.reporter.entity.VehicleFlashDetail;
import org.dsa.modules.reporter.entity.VehicleFlashRecord;
import org.dsa.modules.reporter.enums.FileCateEnum;
import org.dsa.modules.reporter.service.FileLogService;
import org.dsa.modules.vehicle.dao.DVehicleInfoDao;
import org.dsa.modules.vehicle.dao.DVehicleTypeDao;
import org.dsa.modules.vehicle.entity.DVehicleInfoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jakarta.annotation.Resource;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Map;

@Slf4j
@Service
public class AsMaintainedJsonHandler {
    @Autowired
    VehicleFlashDetailMapper detailMapper;
    @Autowired
    VehicleFlashRecordMapper recordMapper;
    @Autowired
    DVehicleInfoDao dVehicleInfoDao;
    @Autowired
    FileLogMapper fileLogMapper;
    @Autowired
    FileLogService logService;
    @Autowired
    DVehicleTypeDao dVehicleTypeDao;

    @Resource
    RedisUtils redisUtils;

    public void readFile(String path) {
        StringBuilder sb = new StringBuilder();
        File file = new File(path);
        File[] files = file.listFiles();
        if (files == null) {
            log.error("not find file by asmaintained");
            logService.failedLog(FileCateEnum.AsMaintained.getValue(), path, null, "not find file by asmaintained");
            return;
        }
        Integer size = 0;
        // 开始读取文件
        for (File fileAs : files) {
            if (!fileAs.getName().endsWith("json")) {
                continue;
            }
            Long startTime = System.currentTimeMillis();
            sb = new StringBuilder();
            try {
                QueryWrapper<FileLog> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("file_name", fileAs.getName());

                FileLog fileLog = fileLogMapper.selectOne(queryWrapper);
                if (fileLog != null) {
                    continue;
                }
                readJson(sb, fileAs, startTime);
            } catch (Exception e) {
                log.error("read asmaintained json error " + e.getMessage(), e);
                logService.failedLog(FileCateEnum.AsMaintained.getValue(), fileAs.getPath(),null, e.getMessage());
            }
        }

    }
    @Transactional
    private void readJson(StringBuilder sb, File fileAs, Long startTime) throws Exception {
        VehicleFlashRecord vehicleFlashRecord;
        String jsonString = new String(Files.readAllBytes(Paths.get(fileAs.getPath())));
        if (StringUtils.isNotBlank(jsonString)) {
            AsMaintainedJsonDto asMaintainedJsonDto = JSONObject.parseObject(jsonString, AsMaintainedJsonDto.class);
            if (asMaintainedJsonDto != null) {
                String vin = asMaintainedJsonDto.getVin();

                vehicleFlashRecord = new VehicleFlashRecord();
                DVehicleInfoEntity dVehicleInfoEntity = dVehicleInfoDao.selectOneByVin(vin);

                String model = asMaintainedJsonDto.getModel();
                if (dVehicleInfoEntity == null) {
                    Long vehicleTypeId = dVehicleTypeDao.getVehicleTypeId(model);

                    dVehicleInfoEntity = new DVehicleInfoEntity();
                    dVehicleInfoEntity.setVin(vin);
                    dVehicleInfoEntity.setVehicleTypeId(vehicleTypeId);
                    dVehicleInfoEntity.setSource(VehicleConstant.AsMaintained);
                    dVehicleInfoDao.insert(dVehicleInfoEntity);
                }

                vehicleFlashRecord.setVin(vin);
                vehicleFlashRecord.setTag(asMaintainedJsonDto.getUpdateTime());
                vehicleFlashRecord.setModel(asMaintainedJsonDto.getModel());
                vehicleFlashRecord.setVehicleVersion(asMaintainedJsonDto.getTargetVersion());
                vehicleFlashRecord.setPrevVersion(asMaintainedJsonDto.getSourceVersion());
                vehicleFlashRecord.setCollectTime(new Date(Long.parseLong(asMaintainedJsonDto.getUpdateTime())));
                vehicleFlashRecord.setFeature(jsonString);
                vehicleFlashRecord.setSessionId(asMaintainedJsonDto.getSessionId());
                recordMapper.insert(vehicleFlashRecord);
                VehicleFlashDetail vehicleFlashDetail = new VehicleFlashDetail();
                vehicleFlashDetail.setVin(vin);
                vehicleFlashDetail.setFlashId(vehicleFlashRecord.getId().intValue());
                vehicleFlashDetail.setCollectTime(vehicleFlashRecord.getCollectTime());
                vehicleFlashDetail.setFlashTimestamp(vehicleFlashRecord.getCollectTime());
                vehicleFlashDetail.setVersion(vehicleFlashRecord.getVehicleVersion());

                redisUtils.lSet(RedisConstant.FLASH_VIN_KEY, vin+":"+asMaintainedJsonDto.getSessionId()+":"+asMaintainedJsonDto.getUpdateTime());

                Integer sum = 0;
                StringBuilder reason = new StringBuilder();
                for (Map<String, String> ecus : asMaintainedJsonDto.getEcus()) {
                    vehicleFlashDetail.setEcuName(ecus.get("name"));
                    int result = Integer.parseInt(ecus.get("result"));
                    if (result == 1 || result == 2) {
                        sum += result;
                    }else{
                        sum += 3;
                    }
                    reason.append(ecus.get("reason")).append(";");
                    JSONObject expectedIdents = JSONObject.parseObject(ecus.get("expectedIdents"));
                    for (String s : expectedIdents.keySet()) {
                        vehicleFlashDetail.setId(null);
                        vehicleFlashDetail.setVersion(asMaintainedJsonDto.getSourceVersion());
                        vehicleFlashDetail.setVersionName(s);
                        vehicleFlashDetail.setVersionValue(expectedIdents.get(s).toString());
                        // 上一版本默认成功
                        vehicleFlashDetail.setResult(1);
                        detailMapper.insert(vehicleFlashDetail);
                    }
                    vehicleFlashDetail.setResult(Integer.parseInt(ecus.get("result")));
                    vehicleFlashDetail.setReason(ecus.get("reason"));
                    JSONObject targetIdents = JSONObject.parseObject(ecus.get("targetIdents"));
                    for (String s : targetIdents.keySet()) {
                        vehicleFlashDetail.setId(null);
                        vehicleFlashDetail.setVersion(asMaintainedJsonDto.getTargetVersion());
                        vehicleFlashDetail.setVersionName(s);
                        vehicleFlashDetail.setVersionValue(targetIdents.get(s).toString());
                        detailMapper.insert(vehicleFlashDetail);
                    }

//                    for (String s : ecus.keySet()) {
//                        if ("result,reason,name".contains(s)){
//                            continue;
//                        }
//                        vehicleFlashDetail.setId(null);
//                        vehicleFlashDetail.setVersionName(s);
//                        vehicleFlashDetail.setVersionValue(ecus.get(s));
//                        detailMapper.insert(vehicleFlashDetail);
//                    }
                }
                Integer size = asMaintainedJsonDto.getEcus().size();
                vehicleFlashRecord.setResult(sum % size == 0 ? sum / size : sum / size + 1);
                vehicleFlashRecord.setReason(reason.toString());
                recordMapper.updateById(vehicleFlashRecord);

                sb.append("AsMaintained size:").append(asMaintainedJsonDto.getEcus().size()).append(", spend time: ").append(System.currentTimeMillis() - startTime).append(",File name :").append(fileAs.getName()).append(";");
                logService.succeedLog(FileCateEnum.AsMaintained.getValue(), fileAs.getPath(), null, null, sb.toString());
            }else {
                logService.failedLog(FileCateEnum.AsMaintained.getValue(), fileAs.getPath(),null, "ecus is null");
            }
        }else {
            logService.failedLog(FileCateEnum.AsMaintained.getValue(), fileAs.getPath(),null, "file is null");
        }
    }
}
