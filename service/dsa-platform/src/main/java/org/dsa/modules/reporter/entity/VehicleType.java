package org.dsa.modules.reporter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@TableName("d_vehicle_type")
public class VehicleType extends BaseEntity{

    @TableId(type = IdType.AUTO)
    private Long id;

    private String titleZh;

    private String titleEn;

    private String maker;

    private String platform;

    private String model;

    private String trimLevel;

}
