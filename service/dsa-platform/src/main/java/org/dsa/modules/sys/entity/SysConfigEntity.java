package org.dsa.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;


/**
 * 系统配置信息
 *
 */
@Data
@TableName("sys_config")
public class SysConfigEntity {

	@TableId(type = IdType.AUTO)
	private Long id;
	@NotBlank(message="参数名不能为空")
	private String paramKey;
	@NotBlank(message="参数值不能为空")
	private String paramValue;
	private String remark;

	// 分组
	private String paramGroup;
	// 多语言
	private String languageKey;
	//排序
	private Integer orderNum;
	//数据类型 0 系统参数 1 业务参数
	private Integer type;
}
