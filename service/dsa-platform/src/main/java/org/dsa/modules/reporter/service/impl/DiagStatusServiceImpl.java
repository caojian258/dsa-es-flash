package org.dsa.modules.reporter.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.dsa.modules.reporter.dao.SessionEventMapper;
import org.dsa.modules.reporter.dao.SystemHistoryMapper;
import org.dsa.modules.reporter.dto.SessionStateDto;
import org.dsa.modules.reporter.dto.SharedDataDto;
import org.dsa.modules.reporter.dto.StatusDto;
import org.dsa.modules.reporter.entity.SessionEventLog;
import org.dsa.modules.reporter.entity.SystemHistory;
import org.dsa.modules.reporter.entity.VehicleInfo;
import org.dsa.modules.reporter.service.DiagStatusService;
import org.dsa.modules.reporter.service.VehicleInfoService;
import org.dsa.modules.reporter.util.JsonReader;
import org.dsa.modules.reporter.util.ReportUtil;
import org.dsa.modules.reporter.util.XmlReader;
import org.dsa.modules.vehicle.service.VehicleTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
@Service
public class DiagStatusServiceImpl implements DiagStatusService {

    @Autowired
    SystemHistoryMapper historyMapper;

    @Autowired
    VehicleTypeService vehicleTypeService;

    @Autowired
    VehicleInfoService vehicleInfoService;

    @Autowired
    SessionEventMapper sessionEventMapper;


    @Transactional
    @Override
    public StatusDto storeData(String path, String tag, String sessionId, String vinCode) {
        StatusDto result = new StatusDto();

        JsonReader reader = new JsonReader(path);
        String jsonStr = reader.getJsonString();
        JSONObject parse = (JSONObject)JSONObject.parse(jsonStr);

        SystemHistory en = new SystemHistory();
        Map<String, Object> jsonFile = new HashMap<>();
        Set<Map.Entry<String, Object>> entrySet = parse.entrySet();

        for (Map.Entry<String, Object> entry: entrySet) {
            jsonFile.put(entry.getKey().toLowerCase() , entry.getValue());
        }
        String[] infos = parse.get("SYSTEM_INFO_OUTPUT").toString().split(",");
        List<String> infoList = Arrays.asList(infos);
        List<String> osInfoList = new ArrayList<>();
        for (String ss : infoList){
            String item = ss.replace("\"", "").replace("/n","");
            osInfoList.add(item);
        }

        String[] osInfos = osInfoList.toArray(new String[osInfoList.size()]);

        BeanUtil.fillBeanWithMap(jsonFile, en, true, new CopyOptions());

        if(null != osInfos && osInfos.length>0){
            en.setWindowsId(osInfos[0]);
            for (int i=0;i< osInfos.length;i++){
                if(!StringUtils.isEmpty(osInfos[i]) && osInfos[i].endsWith("PC")){
                    en.setCpuArch(osInfos[i]);
                    break;
                }
            }
           if(null != en.getProcessors()){
               en.setCpuInfo(en.getProcessors());
           }
        }

        //@todo set default workshop
        if(null == en.getWorkshop()){
            en.setWorkshop("unknown");
        }
        en.setTag(tag);
        en.setSystemInfoOutput(String.join(",", osInfoList));
        en.setVinCode(vinCode);
        en.setAntivirFlag(ReportUtil.sysFlagCheck(en.getAntivirInstalled()));
        en.setSessionid(sessionId);
        en.setProxyFlag(ReportUtil.sysFlagCheck(en.getProxies()));
        en.setFirewallFlag(ReportUtil.sysFirewallCheck((JSONObject) en.getFirewalls()));
        en.setWlanFlag(ReportUtil.sysWlanCheck((JSONArray) en.getWlanNetworks()));

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
            Date dt = new Date(Long.valueOf(en.getTimestamp()));
            en.setCollectDate(sdf2.format(dt));
            en.setCollectTime(dt);
        }catch (Exception ex){
            log.error("Status File Parser Timestamp {} Exception: {}", en.getTimestamp(),ex.getMessage());
        }

        historyMapper.save(en);
        result.setUsername(en.getUsername());
        result.setPcid(en.getPcid());
        result.setWorkshop(en.getWorkshop());
        result.setSessionId(sessionId);
        result.setLocale(en.getApplicationLocale());
        result.setMsg("Workshop:"+en.getWorkshop()+", Pcid:"+en.getPcid()+", SessionId:"+sessionId);
        return result;
    }

    @Override
    public SharedDataDto storeVehicleInfo(String sharedDataPath, String vinCode) throws Exception {
        XmlReader reader = new XmlReader( sharedDataPath);
        List<Map<String, Object>> mapList =  reader.parse("entry");

        if(null !=mapList && mapList.size()>2){
            SharedDataDto dto = new SharedDataDto();
            dto.setPcId(formatVehicleType(mapList.get(0).toString()));
            dto.setMadMake(formatVehicleType(mapList.get(1).toString()));
            dto.setMadModel(formatVehicleType(mapList.get(2).toString()));

            Long vehTypeId = vehicleTypeService.getVehicleTypeId(dto.getMadModel());
            if(null != vehTypeId){
                dto.setVehTypeId(vehTypeId);
                VehicleInfo info = vehicleInfoService.getOneByVinCode(vinCode);
                if(null == info){
                    vehicleInfoService.addOne(vinCode, vehTypeId);
                }
            }
            return dto;
        }
        return null;
    }

    @Override
    public SessionStateDto storeSessionState(String path, String tag, String sessionId, String vinCode, String pcid, String workshop, String lang) throws Exception {
        XmlReader reader = new XmlReader( path);
        SessionStateDto dto = new SessionStateDto();

        List<Map<String, Object>> mapList = reader.parse("de.dsa.rcp.core.sessionlog.model.SessionStateLogEvent");
        if(null !=mapList && mapList.size()>0){
            for (Map<String, Object> map: mapList){
                SessionEventLog entity = new SessionEventLog();
                /***
                 * "mUserName": "default",
                 * "mUsedVciSerialNumber": "D0057205",
                 * "mState": "CREATED",
                 * "mUsedVciIdentification": "DSA",
                 * "timestamp": "2022-09-02 18:20:56.411 CST"
                 * */
                entity.setSessionId(sessionId);
                entity.setVin(vinCode);
                entity.setPcid(pcid);
                entity.setWorkshop(workshop);
                entity.setState(map.get("mState").toString());
                entity.setUserName(map.get("mUserName").toString());
                entity.setUsedVciSerialNumber(map.get("mUsedVciSerialNumber").toString());
                entity.setUsedVciIdentification(map.get("mUsedVciIdentification").toString());
                entity.setTimestampText(map.get("timestamp").toString());
                entity.setLanguage(lang);
                entity.setTag(tag);
                entity.setCollectTime(new Date());
                sessionEventMapper.insert(entity);

                dto.setLanguage(lang);
                dto.setDate(entity.getTimestampText().substring(0, 10));
                dto.setVciIdentification(entity.getUsedVciIdentification());
                dto.setVciSerialNumber(entity.getUsedVciSerialNumber());
                dto.setUserName(entity.getUserName());
            }
        }

        return dto;
    }

    private String formatVehicleType(String raw){
        return raw.trim().replace("{", "")
                .replace("}", "")
                .replace("string=", "");
    }

}
