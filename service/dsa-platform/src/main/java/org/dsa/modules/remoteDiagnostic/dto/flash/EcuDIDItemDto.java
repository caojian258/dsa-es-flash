package org.dsa.modules.remoteDiagnostic.dto.flash;

import lombok.Data;

import java.io.Serializable;

@Data
public class EcuDIDItemDto implements Serializable {

    private String did;

    private String didName;

    private String didValue;

    //是否可选 1可选 0强制
    private Integer optional;
}
