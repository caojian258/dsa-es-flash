package org.dsa.modules.reporter.vo.doc;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.io.Serializable;

@Data
public class CheckSessionVo implements Serializable {

    @NotNull(message = "诊断会话ID不能为空")
    private String sessionId;

    @NotNull(message = "VIN不能为空")
    private String vin;

    private String day;
}
