package org.dsa.modules.remoteDiagnostic.controller;


import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.dsa.common.annotation.SysLog;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.R;
import org.dsa.common.validator.ValidatorUtils;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.common.validator.group.UpdateGroup;
import org.dsa.modules.remoteDiagnostic.config.RemoteDiagnosticConfig;
import org.dsa.modules.remoteDiagnostic.constant.RemoteDiagnosticConstant;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleTagEntity;
import org.dsa.modules.remoteDiagnostic.service.*;
import org.dsa.modules.remoteDiagnostic.vo.Vehicle.VehicleInfoExcelReqVo;
import org.dsa.modules.remoteDiagnostic.vo.VehiclePlatformPageReVo;
import org.dsa.modules.remoteDiagnostic.vo.VehicleTag.VehicleTagReqVo;
import org.dsa.modules.sys.entity.FileEntity;
import org.dsa.modules.vehicle.entity.DVehicleInfoEntity;
import org.dsa.modules.vehicle.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Slf4j
@RestController
@RequestMapping("/remoteVehicle")
public class RemoteVehicleController {

    @Autowired
    RemoteVehicleEcuService vehicleEcuService;
//    @Autowired
//    private RemoteVehiclePlatformService vehiclePlatformService;
    @Autowired
    RemoteVehiclePoolService vehiclePoolService;
    @Autowired
    VinCodeFileService vinCodeFileService;
    @Autowired
    RemoteVehicleTagService vehicleTagService;
    @Autowired
    RemoteDiagnosticConfig remoteDiagnosticConfig;
    @Autowired
    RemoteVersionRecordService remoteVersionRecordService;
    @Autowired
    VehicleService vehicleService;
// 车辆平台---------------------------------------------------

//    @RequestMapping("/getPlatformPage")
//    public R getPlatformPage(@RequestBody VehiclePlatformPageReVo vo) {
//        return R.ok().put("data",vehiclePlatformService.selectPage(vo));
//    }
//
//    @GetMapping("/getPlatformInfo/{id}")
//    public R getPlatformInfo(@PathVariable("id") Long id) {
//        if (id == null) {
//            return R.error(Constant.Msg.CHECK_INPUT);
//        }
//        return R.ok().put("data",vehiclePlatformService.getInfo(id));
//    }
//
//    @SysLog("新增车辆平台")
//    @PostMapping("/savePlatform")
////    @RequiresPermissions("sys:user:save")
//    public R savePlatform(@RequestBody RemoteVehiclePlatformEntity platform){
//
//        ValidatorUtils.validateEntity(platform, AddGroup.class);
//        return R.ok().put("id",vehiclePlatformService.savePlatform(platform));
//    }
//
//    @SysLog("修改车辆平台")
//    @PostMapping("/updatePlatform")
////    @RequiresPermissions("sys:user:update")
//    public R updatePlatform(@RequestBody RemoteVehiclePlatformEntity platform){
//        ValidatorUtils.validateEntity(platform, UpdateGroup.class);
//
//        return R.ok().put("id",vehiclePlatformService.updatePlatform(platform));
//    }

// tag ---------------------------------------------------
    @RequestMapping("/getTagPage")
    public R getTagPage(@RequestBody VehiclePlatformPageReVo vo) {
        return R.ok().put("data",vehicleTagService.selectPage(vo));
    }

    @GetMapping("/getTags")
    public R getTags(Boolean flag) {
        return R.ok().put("data", vehicleTagService.getList(flag));
    }

    @GetMapping("/getTagInfo/{id}")
    public R getTagInfo(@PathVariable("id") Long id) {
        if (id == null) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        return R.ok().put("data",vehicleTagService.getInfo(id));
    }

    @SysLog("新增车辆TAG")
    @PostMapping("/saveTag")
//    @RequiresPermissions("sys:user:save")
    public R saveTag(@RequestBody RemoteVehicleTagEntity tag){

        ValidatorUtils.validateEntity(513, tag, AddGroup.class);

        vehicleTagService.saveTag(tag);
        return R.ok();
    }

    @SysLog("修改车辆TAG")
    @PostMapping("/updateTag")
//    @RequiresPermissions("sys:user:update")
    public R updateTag(@RequestBody RemoteVehicleTagEntity tag){
        ValidatorUtils.validateEntity(513, tag, UpdateGroup.class);

        vehicleTagService.updateTag(tag);
        return R.ok();
    }


    @SysLog("删除车辆TAG")
    @PostMapping("/deleteTag/{id}")
//    @RequiresPermissions("sys:user:update")
    public R deleteTag(@PathVariable("id") Long id) {
        if (id == null) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        vehicleTagService.deleteTag(id);
        return R.ok();
    }

    @PostMapping("/vehicleTagUpdate")
    @SysLog("给车辆分配TAG")
    public R vehicleTagUpdate(@RequestBody VehicleTagReqVo reqVo){
        if (reqVo.getId() == null && reqVo.getVinList() == null) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        vehicleTagService.updateVehicleTag(reqVo);
        return R.ok();
    }

    @RequestMapping("/getVehicleTagInfo/{vin}")
    public R getVehicleTagInfo(@PathVariable("vin") String vin) {
        if (StringUtils.isBlank(vin)) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }

        return R.ok().put("data", vehicleTagService.getVehicleTagInfo(vin));
    }

    @RequestMapping("/getVehicleTagList/{vin}")
    public R getVehicleTagList(@PathVariable("vin") String vin) {
        if (StringUtils.isBlank(vin)) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        return R.ok().put("data", vehicleTagService.getVehicleTagList(vin));
    }

// 文件操作 ---------------------------------------------------


    /**
     * vin码excel导入
     */
    @PostMapping("/vin_code_excel_import")
    public R vinCodeExcelImport(@RequestParam("file") MultipartFile mFile) {
        String key = RemoteDiagnosticConstant.VIN_CODE_FLAG + System.currentTimeMillis();
        try {
            //MultipartFile 转化为file easyexcel接受file类型的文件
            File file = new File(Objects.requireNonNull(mFile.getOriginalFilename()));
            if (!file.getName().endsWith(".xlsx")) {
                return R.error("请导入excel类型的文件");
            }
            FileUtils.copyInputStreamToFile(mFile.getInputStream(), file);
            key += ":" + file.getName();
            vinCodeFileService.readExcel(file, key);
        } catch (Exception e) {
            log.error("excel导入出错：", e);
            return R.error(600,"excel出错：" + e);
        }
        return R.ok().put("key", key);
    }

    /**
     * tag vin excel导入
     */
    @PostMapping("/tag_vin_excel_import")
    public R tagVinExcelImport(@RequestParam("file") MultipartFile mFile) {
//        String key = StaticConstant.VIN_CODE_FLAG + System.currentTimeMillis();
        try {
            //MultipartFile 转化为file easyexcel接受file类型的文件
            File file = new File(Objects.requireNonNull(mFile.getOriginalFilename()));
            if (!file.getName().endsWith(".xlsx")) {
                return R.error("请导入excel类型的文件");
            }
            FileUtils.copyInputStreamToFile(mFile.getInputStream(), file);
            vehicleTagService.readExcel(file);
        } catch (Exception e) {
            log.error("excel导入出错：", e);
            return R.error(600,"excel出错：" + e);
        }
        return R.ok();
    }

    // 下载模板 tag
    @GetMapping("/tag_template_excel_export")
    public R multiLanguageExcelExport() {
        Map<String, String> map = new HashMap<>();
        try {
            map = vehicleTagService.templateExcelExport();
        } catch (Exception e) {
            return R.error("excel出错：" + e);
        }
        return R.ok().put("data", map);
    }

    @RequestMapping(value="/download")
    public void download(@RequestBody FileEntity fileEntity, HttpServletRequest req, HttpServletResponse response){
        InputStream ips = null;
        OutputStream ops = null;

        File file = new File(fileEntity.getUrl());
        if (!file.exists()) {
            throw new RRException(Constant.Msg.FILE_NOT_EXIST);
        }

        try {
            ips =  new FileInputStream(file);;//new FileInputStream(new File(fileName));
            ops = response.getOutputStream();
            response.setContentType("application/x-download");
            response.setHeader("Content-Disposition", "attachment;filename=utf-8'zh_cn'" + URLEncoder.encode(fileEntity.getName(), "UTF-8"));
            IOUtils.copy(ips, ops);
            ips.close();
            ops.flush();

        } catch (IOException e) {
            log.error("下载文件出错："+e);
        } finally {
            try {
                if(ips != null ) {
                    ips.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if(ops != null) {
                    ops.flush();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // 下载车辆模板
    @RequestMapping("/vehicle_excel_template_export")
    public R multiVehicleTemplateExcelExport() {
        Map<String, String> map = new HashMap<>();
        try {
            map = vehicleService.vehicleExcelExport();
        } catch (Exception e) {
            return R.error("excel出错：" + e);
        }
        return R.ok().put("data", map);
    }

    // 下载车辆信息
    @RequestMapping("/batch_vehicle_excel_export")
    public R multiBatchVehicleExcelExport(@RequestBody VehicleInfoExcelReqVo vo) {
        Map<String, String> map = new HashMap<>();
        try {
            map = vehicleService.vehicleExcelExport(vo);
        } catch (Exception e) {
            return R.error("excel出错：" + e);
        }
        return R.ok().put("data", map);
    }

    // 下载车辆信息
    @RequestMapping("/vehicle_excel_export")
    public R multiVehicleExcelExport(@RequestBody List<DVehicleInfoEntity> ids) {
        if (ids == null || ids.size() == 0) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        Map<String, String> map = new HashMap<>();
        try {
            map = vehicleService.vehicleExcelExport(ids);
        } catch (Exception e) {
            return R.error("excel出错：" + e);
        }
        return R.ok().put("data", map);
    }


    // 获取版本记录 默认倒序
    @RequestMapping("/getVersionRecord/{type}/{id}")
    public R getVersionRecord(@PathVariable("type") String type, @PathVariable("id") Long id) {
        return R.ok().put("data", remoteVersionRecordService.getList(type, id));
    }

}
