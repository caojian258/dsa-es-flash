package org.dsa.modules.remoteDiagnostic.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONObject;
import junit.framework.TestCase;
import org.dsa.modules.remoteDiagnostic.dto.flash.EcuDIDDto;
import org.dsa.modules.remoteDiagnostic.dto.flash.EcuDIDItemDto;
import org.dsa.modules.remoteDiagnostic.dto.flash.EcuVersionDto;
import org.dsa.modules.remoteDiagnostic.dto.flash.EcuVersionFileDto;
import org.dsa.modules.remoteDiagnostic.entity.RemoteEcuIdentificationInfoEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuVersionEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVersionEcuRelyEntity;
import org.dsa.modules.remoteDiagnostic.service.EcuFlashService;
import org.dsa.modules.remoteDiagnostic.vo.Flash.FlashCheckVo;
import org.dsa.modules.reporter.util.FilesUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EcuFlashServiceImplTest extends TestCase {

    @Autowired
    EcuFlashService ecuFlashService;


    @Test
    public void testDependencyVersion(){
        List<EcuVersionDto> ss =  ecuFlashService.findVehicleEcuVersionByVehicleVersion(21L);

        System.out.println("ss:");
        System.out.println(JSONObject.toJSONString(ss));

        List<Long> ecuVersionIdList = new ArrayList<>();
        for (EcuVersionDto evd: ss) {
            ecuVersionIdList.add(evd.getId());
        }
        List<RemoteVersionEcuRelyEntity> ecuVersionDependencyList =  ecuFlashService.findEcuVersionDependency(ecuVersionIdList);
        List<EcuVersionDto> ecuVersionDtoList = null;

        List<Long> ecuVersionIds = ecuVersionDependencyList.stream().map(i -> i.getEcuVersionId()).collect(Collectors.toList());
        //这个数组的顺序不变 把 ss中存在在这里的item 删除掉 然后把 这里面的数据加入到ss的尾部
        List<EcuVersionDto> dependencyEcuVersion = ecuFlashService.findEcuVersionByDependencyEcuVersionId(ecuVersionIds);

        System.out.println("dependencyEcuVersion:");
        System.out.println(JSONObject.toJSONString(dependencyEcuVersion));

        ecuVersionDtoList = ss;
        for (EcuVersionDto versionDto: ss) {
            if(dependencyEcuVersion.contains(versionDto)){
                ecuVersionDtoList.remove(versionDto);
            }
        }
        ecuVersionDtoList.addAll(dependencyEcuVersion);

        System.out.println("ecuVersion:");
        System.out.println(JSONObject.toJSONString(ecuVersionDtoList));
    }
    @Test
    public void testCheckAlign() {
        FlashCheckVo vo = new FlashCheckVo();
        List<EcuDIDDto> ecus = new ArrayList<>();
        EcuDIDDto vcd = new EcuDIDDto();
        List<EcuDIDItemDto> dids = new ArrayList<>();
        EcuDIDItemDto did1  = new EcuDIDItemDto();
        did1.setDid("F189");
        did1.setDidName("Software Version Number");
        did1.setDidValue("205");
        dids.add(did1);


        EcuDIDItemDto did2  = new EcuDIDItemDto();
        did2.setDid("F191");
        did2.setDidName("Hardware Version Number");
        did2.setDidValue("1.0");
        dids.add(did2);

        vcd.setEcuName("VCD");
        vcd.setDidList(dids);

        ecus.add(vcd);
        vo.setEcuList(ecus);


        List<EcuVersionDto> ss =   ecuFlashService.findVehicleEcuVersionByVehicleVersion(20L);
        Map<String, Map<String, String>> eucDIDs = new HashMap<>();

        for (EcuVersionDto dto: ss) {
            Map<String, String> didMap =  ecuFlashService.findDidMapByEcuVersionId(dto.getId(), "target");
            eucDIDs.put(dto.getEcuName(), didMap);
        }
        Integer needAlign = 0;

        outer: for (EcuDIDDto ecuDIDDto: vo.getEcuList()){
            String ecu = ecuDIDDto.getEcuName();
            Map<String, String> recordDids = eucDIDs.get(ecu);
            if(CollectionUtil.isNotEmpty(recordDids) && CollectionUtil.isNotEmpty(ecuDIDDto.getDidList())){
                for (EcuDIDItemDto didItem: ecuDIDDto.getDidList()) {
                    if(null != recordDids.get(didItem.getDid()) &&
                            !didItem.getDidValue().equals(recordDids.get(didItem.getDid()))){
                        needAlign = 1;
                    }
                    break outer;
                }
            }

        }

        System.out.println(needAlign);
        System.out.println(JSONObject.toJSONString(eucDIDs));
    }

    @Test
    public void testFindVehicleEcuByVehicleVersion() {
        List<RemoteVehicleEcuEntity> ss =  ecuFlashService.findVehicleEcuByVehicleVersion(20L);

        System.out.println(JSONObject.toJSONString(ss));
    }

    @Test
    public void testFindVehicleEcuVersionByVehicleVersion() {
        List<EcuVersionDto> ss =   ecuFlashService.findVehicleEcuVersionByVehicleVersion(20L);
        Map<String, Map<String, RemoteEcuIdentificationInfoEntity>> eucDIDs = new HashMap<>();

        for (EcuVersionDto dto: ss) {
            Map<String, RemoteEcuIdentificationInfoEntity> did =  ecuFlashService.findDidByEcuVersionId(dto.getId(), "target");
          eucDIDs.put(dto.getEcuName(), did);
        }

        System.out.println(JSONObject.toJSONString(eucDIDs));

    }

    @Test
    public void testFindDidByEcuVersionId() {
        List<Long> ids = new ArrayList<>();
        ids.add(26L);
        List<EcuVersionFileDto> ss =  ecuFlashService.findEcuVersionFiles(ids);
        System.out.println(JSONObject.toJSONString(ss));
    }

    @Test
    public void testBuildEcuFiles(){
        List<String> filePaths = new ArrayList<>(4);
        filePaths.add("C:\\Users\\jian8\\logs\\chiner\\runtime-2023-04-14.log");
        filePaths.add("C:\\Users\\jian8\\logs\\chiner\\runtime-2023-04-20.log");
        filePaths.add("C:\\Users\\jian8\\logs\\chiner\\runtime-2023-05-06.log");
        filePaths.add("C:\\Users\\jian8\\logs\\chiner\\runtime-2023-05-08.log");

        String[] paths = filePaths.toArray(new String[filePaths.size()]);
        String path = "C:\\Users\\jian8\\logs\\chiner\\runtime.zip";

        FilesUtil.compressMultipleFiles(path, paths);
    }

    @Test
    public void testEcuDid(){
        List<EcuDIDDto> ecuList = new ArrayList<>(2);

        EcuDIDDto dto1 = new EcuDIDDto();

        dto1.setEcuName("VCD");

        List<EcuDIDItemDto> dids1 =  new ArrayList<>();
        EcuDIDItemDto didItemDto = new EcuDIDItemDto();

        didItemDto.setDid("F189");
        didItemDto.setDidName("Software Version Number");
        didItemDto.setDidValue("1.0");
        dids1.add(didItemDto);


        EcuDIDItemDto didItemDto2 = new EcuDIDItemDto();

        didItemDto2.setDid("F187");
        didItemDto2.setDidName("Part Number");
        didItemDto2.setDidValue("100");

        dids1.add(didItemDto2);

        EcuDIDItemDto didItemDtoX = new EcuDIDItemDto();

        didItemDtoX.setDid("F1AA");
        didItemDtoX.setDidName("Vehicle Version");
        didItemDtoX.setDidValue("100");

        dids1.add(didItemDtoX);

        dto1.setDidList(dids1);
        ecuList.add(dto1);


        EcuDIDDto dto2 = new EcuDIDDto();

        dto2.setEcuName("BMS_QR01");


        List<EcuDIDItemDto> dids2 =  new ArrayList<>();


        EcuDIDItemDto didItemDto3 = new EcuDIDItemDto();

        didItemDto3.setDid("F189");
        didItemDto3.setDidName("Software Version Number");
        didItemDto3.setDidValue("1.0");
        dids2.add(didItemDto3);

        EcuDIDItemDto didItemDto4 = new EcuDIDItemDto();

        didItemDto4.setDid("F187");
        didItemDto4.setDidName("Part Number");
        didItemDto4.setDidValue("9201010");

        dids2.add(didItemDto4);

        dids2.add(didItemDtoX);

        dto2.setDidList(dids2);

        ecuList.add(dto2);

        System.out.println(JSONObject.toJSONString(ecuList));
    }
}