package org.dsa.modules.reporter.document;


import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
public class SessionDoc extends ElasticEntity {

    private String sessionId;

    private String vin;

    private Map<String, Object> vinSuggest;

    private List<Long> vehicleTypeId;

    private String logPath;


    private String logName;

    //0=reserved；1=预诊断；2=实时诊断
    private Integer diagCategory;

    //“001”=车载诊断；“002”=“离线诊断”；“003”=远程诊断
    private String diagSource;


    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    private Long userId;

    private String userName;

    private Long workshopId;

    private String workshop;

    private String pcid;

    private Integer sourceVersion;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date lastUpdateTime;

    private String tag;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;

}
