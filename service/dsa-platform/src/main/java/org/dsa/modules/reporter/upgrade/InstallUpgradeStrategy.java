package org.dsa.modules.reporter.upgrade;

import lombok.extern.slf4j.Slf4j;
import org.dsa.modules.reporter.dto.AppInstallDto;
import org.dsa.modules.reporter.dto.AppVersionDto;
import org.dsa.modules.reporter.entity.AppHistory;
import org.dsa.modules.reporter.entity.ClientUpgrade;
import org.dsa.modules.reporter.enums.AppTypeEnum;
import org.dsa.modules.reporter.service.AppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/***
 * 安装记录 保存到安装表 安装成功更新 overview  version
 * @todo 版本名称和组件、软件名称从其他sqlite里面获取 等sqlite 数据结构确认
 */
@Slf4j
@Service
public class InstallUpgradeStrategy implements UpgradeStrategy{

    @Autowired
    AppService appService;

    @Override
    public void save(ClientUpgrade log) {
        List<AppInstallDto> installDtos = new ArrayList<>();

        List<AppVersionDto> versionDtos = new ArrayList<>();

        String type = "";
        Integer appId = 0;
        Integer versionId = 0;

        if(log.getUpdateTarget() == 0){
            type = AppTypeEnum.SOFTWARE.getValue();
            appId = log.getSoftwareId();
            versionId = log.getSoftwareCurrentVersionNumber();

        }else{
            type = AppTypeEnum.COMPONENT.getValue();
            appId = log.getComponentId();
            versionId = log.getComponentCurrentVersionNumber();
        }

        AppInstallDto installDto = new AppInstallDto();
        installDto.setInstallationResult(String.valueOf(log.getUpdateResult()));
        installDto.setInstallationMessage(log.getUpdateResult()==0? "success":"fail");
        installDto.setApplicationName("");
        installDto.setUpdateOid(log.getUpdateId());
        installDto.setUpdateVersion("");
        installDto.setAppId(appId);
        installDto.setType(type);

        installDtos.add(installDto);

        appService.batchAddInstall(installDtos, log.getWorkshopName(), log.getDeviceId(), log.getEndTime(), log.getTag());

        if(log.getUpdateResult() ==0 ){

            AppVersionDto versionDto = new AppVersionDto();

            versionDto.setVersion("");
            versionDto.setName("");
            versionDto.setAppId(appId);
            versionDto.setType(type);
            versionDto.setVersionId(versionId);
            versionDtos.add(versionDto);

            appService.batchSaveOverview(versionDtos, log.getWorkshopName(), log.getDeviceId(), log.getEndTime(), log.getTag());

            appService.batchAddVersion(versionDtos, log.getWorkshopName(), log.getDeviceId(), log.getEndTime(), log.getTag());
        }

    }

    @Override
    public void saveHis(AppHistory data) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Set<AppInstallDto> installDtos = new HashSet<>();

        Set<AppVersionDto> versionDtos = new HashSet<>();

        AppInstallDto installDto = new AppInstallDto();
        installDto.setInstallationTime(sdf.format(data.getStartTime()));
        installDto.setInstallationResult(String.valueOf(data.getStatus()));
        installDto.setInstallationMessage(data.getStatus()==0? "success":"fail");
        installDto.setApplicationName(data.getAppName());
        installDto.setUpdateOid(data.getUpdateId());
        installDto.setUpdateVersion(data.getAppVersion());
        installDto.setAppId(data.getAppId());
        installDto.setType(data.getAppType());

        installDtos.add(installDto);

        appService.batchAddInstall(installDtos, data.getWorkshopName(), data.getDeviceId(), data.getCollectDate(), data.getTag());

        if(data.getStatus() == 0 ){
            AppVersionDto versionDto = new AppVersionDto();
            versionDto.setVersion(data.getAppVersion());
            versionDto.setName(data.getAppName());
            versionDto.setAppId(data.getAppId());
            versionDto.setType(data.getAppType());
            versionDto.setTimestamp(sdf.format(data.getStartTime()));
            versionDto.setVersionId(data.getVersionNumber());

            if(null != data.getAppPrevVersion()){
                versionDto.setPrevVersion(data.getAppPrevVersion());
            }
            versionDtos.add(versionDto);

            appService.batchSaveOverview(versionDtos, data.getWorkshopName(), data.getDeviceId(), data.getCollectDate(), data.getTag());
            appService.batchAddVersion(versionDtos, data.getWorkshopName(), data.getDeviceId(), data.getCollectDate(), data.getTag());
        }

    }
}
