package org.dsa.modules.onlineUpdate.vo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.dsa.modules.onlineUpdate.entity.SoftwareEntryEntity;

/**
 * 软件入口表
 * 
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("o_software_entry")
public class SoftwareEntryVo extends SoftwareEntryEntity {

	/**
	 * 启动软件入口的组件名称
	 */
	private String componentName;


}
