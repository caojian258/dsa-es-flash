package org.dsa.modules.remoteDiagnostic.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.dsa.modules.onlineUpdate.dao.ComponentDao;
import org.dsa.modules.onlineUpdate.entity.ComponentEntity;
import org.dsa.modules.onlineUpdate.service.ComponentService;
import org.dsa.modules.remoteDiagnostic.dao.RemoteVersionRecordDao;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVersionRecordEntity;
import org.dsa.modules.remoteDiagnostic.service.RemoteVersionRecordService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class RemoteVersionRecordServiceImpl extends ServiceImpl<RemoteVersionRecordDao, RemoteVersionRecordEntity> implements RemoteVersionRecordService {


    @Override
    public List<RemoteVersionRecordEntity> getList(String type, Long id) {

        return this.getBaseMapper().selectList(Wrappers.<RemoteVersionRecordEntity>lambdaQuery().eq(RemoteVersionRecordEntity::getType, type).eq(RemoteVersionRecordEntity::getValueId, id).orderByDesc(RemoteVersionRecordEntity::getCreateBy));
    }
}
