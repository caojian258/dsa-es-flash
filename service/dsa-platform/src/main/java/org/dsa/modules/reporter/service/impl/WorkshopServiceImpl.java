package org.dsa.modules.reporter.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.reporter.dao.WorkshopGroupMapper;
import org.dsa.modules.reporter.dao.WorkshopMapper;
import org.dsa.modules.reporter.entity.Workshop;
import org.dsa.modules.reporter.entity.WorkshopGroup;
import org.dsa.modules.reporter.service.WorkshopService;
import org.dsa.modules.reporter.vo.workshop.WorkshopPageReVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.function.Consumer;


@Service
public class WorkshopServiceImpl implements WorkshopService {

    @Autowired
    WorkshopMapper mapper;

    @Autowired
    WorkshopGroupMapper groupMapper;


    @Override
    public Page<Workshop> selectPage(WorkshopPageReVo vo) {
        Page<Workshop> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);
        LambdaQueryWrapper<Workshop> qw = Wrappers.<Workshop>lambdaQuery();

        if(!StringUtils.isEmpty(vo.getId())){
            qw.like(Workshop::getId, vo.getId().trim());
        }
        if(null != vo.getGroup() && vo.getGroup().size() > 0){
            if (vo.getGroup().toString().contains("unknown")) {
                qw.and((wap) -> {
                    wap.isNull(Workshop::getWorkshopGroup).or().eq(Workshop::getWorkshopGroup, "").or().in(Workshop::getWorkshopGroup, vo.getGroup());});
//                vo.getGroup().add(null);
//                vo.getGroup().add("");
            }else {
                qw.in(Workshop::getWorkshopGroup, vo.getGroup());
            }
        }
        if(!StringUtils.isEmpty(vo.getName())){
//            qw.like(Workshop::getName, vo.getName().trim());
            qw.apply("name"+" ilike {0}", "%"+vo.getName().trim()+"%");
        }

        if(!StringUtils.isEmpty(vo.getState())){
            qw.eq(Workshop::getState, Integer.valueOf(vo.getState()));
        }
        if(!StringUtils.isEmpty(vo.getCountry())){
            qw.eq(Workshop::getCountry, vo.getCountry());
        }
        if(!StringUtils.isEmpty(vo.getTag())){
            if ("unknown".equals(vo.getTag())){
                qw.and((wap) -> {wap.isNull(Workshop::getTag).or().eq(Workshop::getTag,"");});
//                qw.comment(qw.isNull(Workshop::getTag).or().eq(Workshop::getTag,""));
            }else {
                qw.like(Workshop::getTag, vo.getTag().trim());
            }
        }
        Page<Workshop> resultPage = mapper.selectPage(page, qw);
        return resultPage;
    }

    @Override
    public Workshop getOne(String id) {
        LambdaQueryWrapper<Workshop> qw = Wrappers.<Workshop>lambdaQuery();
        qw.eq(Workshop::getId, id);
        return mapper.selectOne(qw);
    }

    @Override
    public List<WorkshopGroup> getGroup() {
        return groupMapper.selectAllGroup();
    }

    @Override
    public List<String> selectWorkshopIdByGroup(String name) {
        return mapper.selectWorkshopIdByGroup(name);
    }

    @Override
    public void updateLoginTime(String id, Date loginTime) {
        mapper.updateLoginTime(id, loginTime);
    }

    @Override
    public List<String> countryList() {
        return mapper.selectCountry();
    }

    @Override
    public List<String> tagList() {
        return mapper.selectTag();
    }


}
