package org.dsa.modules.remoteDiagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.*;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskOverviewEntity;

@Mapper
public interface DiagnosticTaskOverviewDao extends BaseMapper<DiagnosticTaskOverviewEntity> {


    @Select("select * from r_diag_task_overview i where i.campaign_id = #{taskId} ")
    public DiagnosticTaskOverviewEntity getInfoByTask(@Param("taskId") Long taskId);

    @Select("select *,i.campaign_id as taskId from r_diag_task_overview i where i.campaign_id = #{taskId} ")
    @Results({
            @Result(property = "task",column = "taskId",
            one = @One(select = "org.dsa.modules.remoteDiagnostic.dao.DiagnosticTaskCampaignDao.getInfo"))
    })
    DiagnosticTaskOverviewEntity getOverviewByTask(@Param("taskId") Long id);

    Page<DiagnosticTaskOverviewEntity> getOverviewPage(Page<DiagnosticTaskOverviewEntity> page, @Param("name") String name, @Param("validityPeriod") String validityPeriod);
}
