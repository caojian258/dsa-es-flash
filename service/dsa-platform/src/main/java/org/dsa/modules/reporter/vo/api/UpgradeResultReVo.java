package org.dsa.modules.reporter.vo.api;

import lombok.Data;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;


@Data
public class UpgradeResultReVo implements Serializable {

    @NotBlank(message = "升级标识不能为空")
    private String updateId;

    @NotBlank(message = "设备标识不能为空")
    private String deviceId;

    @NotNull(message = "开始时间不能为空")
    private Long startTime;

    @NotNull(message = "结束时间不能为空")
    private Long endTime;

    private Integer failUpdateCount;

    /***
     * 0,software软件, 1,component组件
     */
    @NotNull(message = "模式不能为空")
    private Integer operateType;

    /***
     * 软件/组件id
     */
    @NotNull(message = "软件/组件ID不能为空")
    private Integer itemId;

    @NotBlank(message = "软件/组件名称不能为空")
    private String itemName;

    @NotNull(message = "软件/组件版本不能为空")
    private String versionName;

    private Integer versionNumber;

    private String previousVersionName;

    /***
     * 0,download(下载)
     * 1,install(安装)
     * 2,repair(修复)
     * 3,rollback(回退)
     * 4,uninstall(卸载)
     */
    @NotNull(message = "状态类型不能为空")
    private Integer statusType;

    /***
     * 0,success成功
     * 1, fail失败
     */
    @NotNull(message = "状态不能为空")
    private Integer status;

    private String tag;
}
