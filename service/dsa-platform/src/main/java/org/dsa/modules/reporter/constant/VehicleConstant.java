package org.dsa.modules.reporter.constant;

public class VehicleConstant {

    public final static String NV11 = "NV11";

    public final static String NV12 = "NV12";

    public final static String TYPES = "RT,N1,9S,FS,3S,4S,4S,8S";

    public final static Integer TspSync = 1;

    public final static Integer AsMaintained = 2;

}
