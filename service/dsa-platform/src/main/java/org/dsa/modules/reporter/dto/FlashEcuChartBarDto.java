package org.dsa.modules.reporter.dto;

import lombok.Data;

@Data
public class FlashEcuChartBarDto {

    private Long value;

    private String ecuName;

    private String collectMonth;
}
