package org.dsa.modules.sys.service.impl;

import java.util.List;
import java.util.Map;

import org.dsa.common.utils.MapUtils;
import org.dsa.common.utils.PageUtils;
import org.dsa.common.utils.Query;
import org.dsa.modules.sys.dao.SysUserGroupDao;
import org.dsa.modules.sys.entity.SysUserGroupEntity;
import org.dsa.modules.sys.service.SysUserGroupService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


@Service("sysUserGroupService")
public class SysUserGroupServiceImpl extends ServiceImpl<SysUserGroupDao, SysUserGroupEntity> implements SysUserGroupService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SysUserGroupEntity> page = this.page(
                new Query<SysUserGroupEntity>().getPage(params),
                new QueryWrapper<SysUserGroupEntity>()
        );

        return new PageUtils(page);
    }

	@Override
	public void saveOrUpdate(Long userId, List<Long> groupIdList) {
		//先删除用户与角色关系
		this.removeByMap(new MapUtils().put("user_id", userId));
		if(groupIdList == null || groupIdList.size() == 0){
			return ;
		}
		//保存用户与组关系
		for(Long groupId : groupIdList){
			SysUserGroupEntity sysUserGroupEntity = new SysUserGroupEntity();
			sysUserGroupEntity.setUserId(userId);
			sysUserGroupEntity.setGroupId(groupId);
			this.save(sysUserGroupEntity);
		}
		
	}

	/**
	 * 根据用户ID，获取角色ID列表
	 */
	@Override
	public List<Long> queryGroupIdList(Long userId) {
		return this.baseMapper.queryGroupIdList(userId);
	}

}