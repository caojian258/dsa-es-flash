package org.dsa.modules.diagnostic.service.impl;

import ch.qos.logback.classic.Logger;
import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.exception.RRException;
import org.dsa.common.pojo.CodeMessage;
import org.dsa.common.utils.*;
import org.dsa.modules.diagnostic.dao.*;
import org.dsa.modules.diagnostic.dto.SessionEcuVersionDto;
import org.dsa.modules.diagnostic.entity.*;
import org.dsa.modules.diagnostic.entity.mqtt.send.MqRequestBodyEntity;
import org.dsa.modules.diagnostic.service.DiagnosticService;
import org.dsa.modules.diagnostic.util.DiagnosticUtils;
import org.dsa.modules.diagnostic.util.LogsUtils;
import org.dsa.modules.sys.dao.SysLogDao;
import org.dsa.modules.sys.dao.SysUserDao;
import org.dsa.modules.sys.entity.InceptioResponserEntity;
import org.dsa.modules.sys.entity.DisplayUserEntity;
import org.dsa.modules.sys.entity.UserInfoEntity;
import org.dsa.modules.vehicle.dao.DVehicleInfoDao;
import org.dsa.modules.vehicle.entity.DVehicleInfoEntity;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;
import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;

@Service("diagnosticService")
public class DiagnosticServiceImpl implements DiagnosticService {

    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass());

    @Resource
    private RedisUtils redisUtils;
    @Resource
    private ApplicationContext applicationContext;
    @Resource
    private DSessionInfoDao dSessionInfoDao;
    @Resource
    private DSessionActionDao dSessionActionDao;
    @Resource
    private DActionInfoDao dActionInfoDao;
    @Resource
    private DActionEcuDao dActionEcuDao;
    @Resource
    private DActionDtcDao dActionDtcDao;
    @Resource
    private DActionVersionDao dActionVersionDao;
    @Resource
    private DiagnosticUtils diagnosticUtils;
    @Resource
    private DVehicleInfoDao dVehicleInfoDao;

    @Resource
    LogsUtils logsUtils;


    /**
     * 诊断激活
     */
    @Override
    public void active(ActiveEntity activeEntity, String token) {
        //创建车辆日志
        String loggerKey = DateUtils.format(new Date(), "yyyyMMdd") + "/" + activeEntity.getVin();
        MDC.put("vin", loggerKey);

        logger.info("Active ---- begin");
        logger.info("Active in:" + JSONObject.toJSONString(activeEntity));
        //check token
        DisplayUserEntity displayUserEntity;
        if ("dev".equals(applicationContext.getEnvironment().getActiveProfiles()[0])) {
            displayUserEntity = new DisplayUserEntity();
            UserInfoEntity userInfoEntity = new UserInfoEntity();
            userInfoEntity.setUserId("1");
            userInfoEntity.setUsername("devtest");
            userInfoEntity.setWorkShop("金桥4S店");
            List<DisplayUserEntity.Role> roles = new ArrayList<>();
            roles.add(DisplayUserEntity.Role.builder().roleCode("Version").build());
            roles.add(DisplayUserEntity.Role.builder().roleCode("Vehicle").build());
            roles.add(DisplayUserEntity.Role.builder().roleCode("Diagnostic").build());
            roles.add(DisplayUserEntity.Role.builder().roleCode("History").build());
            displayUserEntity.setUserInfo(userInfoEntity);
            displayUserEntity.setRoles(roles);
        } else {
            displayUserEntity = checkToken(token);
        }
//        InceptioUserEntity inceptioUserEntity = checkToken(token);

        //获取用户信息
        logger.info("Active Login userId:" + displayUserEntity.getUserInfo().getUserId() + ",username:" + displayUserEntity.getUserInfo().getUsername());

        //诊断车辆信息	S+SessionId	"VersionActiveEntity"
        logger.info("Active Redis key:" + Constant.REDIS_VIN + activeEntity.getVin());
        logger.info("Active Redis value:" + redisUtils.get(Constant.REDIS_VIN + activeEntity.getVin()));

        //2.判断是否存在车辆诊断信息，redis缓存
        VehicleActiveEntity v = getVehicleActiveEntity(activeEntity, displayUserEntity);

        logger.info("Active ---- end");
        MDC.remove(loggerKey);
    }


    /**
     * 取消诊断激活
     */
    @Override
    public void cancelActive(ActiveEntity activeEntity) {
        //1创建车辆日志
        String loggerKey = DateUtils.format(new Date(), "yyyyMMdd") + "/" + activeEntity.getVin();
        MDC.put("vin", loggerKey);

        logger.info("cancelActive----begin");
        logger.info("cancelActive Entity is " + JSONObject.toJSONString(activeEntity));

        //2获取用户信息
        logger.info("cancelActive Redis key:" + Constant.REDIS_VIN + activeEntity.getVin());
        logger.info("cancelActive Redis value:" + redisUtils.get(Constant.REDIS_VIN + activeEntity.getVin()));

        //3.修改session信息 和新增被动结束诊断记录
        try {
            dSessionInfoDao.updateSessionEndTimeAndEndStatusByVin(activeEntity.getVin(), Constant.SESSION_END_STATUS.PASSIVE_END.getCode());
            String actionId = RandomUtil.randomString(4) + System.currentTimeMillis();
            DActionInfoEntity dActionInfoEntity = new DActionInfoEntity();
            dActionInfoEntity.setActionId(actionId);
            dActionInfoEntity.setFunctionCode("SESSION_END");
            dActionInfoEntity.setActionCode("stopRemote");
            dActionInfoEntity.setStartTime(new Date());
            dActionInfoEntity.setEndTime(DateUtils.addDateSeconds(new Date(), 1));
            dActionInfoEntity.setResults(String.valueOf(Constant.RESULTS.SUCCESS.getValue()));
            dActionInfoDao.insert(dActionInfoEntity);
        } catch (Exception e) {
            logger.error("update Session Exception");
            logger.error(e.getMessage());
        }

        //4.如果有运行的诊断,设置诊断命令失败.
        if(redisUtils.hasKey(Constant.REDIS_RUN_VIN + activeEntity.getVin())){
            logger.info("cancelActive ---- redis getKey{} value:{} ", Constant.REDIS_RUN_VIN + activeEntity.getVin(), redisUtils.get(Constant.REDIS_RUN_VIN + activeEntity.getVin()));
            String[] ridFunction = redisUtils.get(Constant.REDIS_RUN_VIN + activeEntity.getVin()).split(",");
            //如果诊断命令还在执行,除了结束诊断提示成功,其它都提示诊断命令执行失败.
            if(redisUtils.hasKey(Constant.REDIS_ACTION + ridFunction[0])){
                if(Constant.Function.END.getFunctionCode().equals(ridFunction[1])){
                    //清除缓存列表,并设置mq命令成功
                    List vehicleActiveEntityList = redisUtils.get(Constant.REDIS_LIST + ShiroUtils.getWorkShop(), (new ArrayList()).getClass());
                    if (vehicleActiveEntityList != null && vehicleActiveEntityList.size() > 0) {
                        vehicleActiveEntityList.removeIf(versionActiveEntity -> (versionActiveEntity.toString().indexOf(activeEntity.getVin()) > 0));
                        if (ObjectUtils.isNotNull(ShiroUtils.getWorkShop())) {
                            redisUtils.set(Constant.REDIS_LIST + ShiroUtils.getWorkShop(), vehicleActiveEntityList);
                        }
                        logger.info("cancelActive ---- redis getKey{} clear:{} ", Constant.REDIS_LIST + ShiroUtils.getWorkShop(), activeEntity.getVin());
                    }
                    redisUtils.set(Constant.REDIS_ACTION_STATUS + ridFunction[0], Constant.RESULTS.SUCCESS.getValue(), Constant.Function.END.getExpire());
                }else if(Constant.Function.BEGIN.getFunctionCode().equals(ridFunction[1])){
                    //清除缓存列表,并设置mq命令执行失败
                    List vehicleActiveEntityList = redisUtils.get(Constant.REDIS_LIST + ShiroUtils.getWorkShop(), (new ArrayList()).getClass());
                    if (vehicleActiveEntityList != null && vehicleActiveEntityList.size() > 0) {
                        vehicleActiveEntityList.removeIf(versionActiveEntity -> (versionActiveEntity.toString().indexOf(activeEntity.getVin()) > 0));
                        if (ObjectUtils.isNotNull(ShiroUtils.getWorkShop())) {
                            redisUtils.set(Constant.REDIS_LIST + ShiroUtils.getWorkShop(), vehicleActiveEntityList);
                        }
                        logger.info("cancelActive ---- redis getKey{} clear:{} ", Constant.REDIS_LIST + ShiroUtils.getWorkShop(), activeEntity.getVin());
                    }
                    redisUtils.set(Constant.REDIS_ACTION_STATUS + ridFunction[0], Constant.RESULTS.FAIL.getValue(), Constant.Function.END.getExpire());
                }else {
                    redisUtils.set(Constant.REDIS_ACTION_STATUS + ridFunction[0], Constant.RESULTS.FAIL.getValue(), Constant.Function.END.getExpire());
                }
            }
        }

        //5.删除redis里的缓存
        redisUtils.delete(Constant.REDIS_RUN_VIN + activeEntity.getVin());

        //6.删除redis里的缓存
        redisUtils.delete(Constant.REDIS_VIN + activeEntity.getVin());

//        List vehicleActiveEntityList = redisUtils.get(Constant.REDIS_LIST + ShiroUtils.getWorkShop(), (new ArrayList()).getClass());
//        if (vehicleActiveEntityList != null && vehicleActiveEntityList.size() > 0) {
//            vehicleActiveEntityList.removeIf(versionActiveEntity -> (versionActiveEntity.toString().indexOf(activeEntity.getVin()) > 0));
//            if (ObjectUtils.isNotNull(ShiroUtils.getWorkShop())) {
//                redisUtils.set(Constant.REDIS_LIST + ShiroUtils.getWorkShop(), vehicleActiveEntityList);
//            }
//            logger.info("cancelActive ---- redis getKey{} clear:{} ", Constant.REDIS_LIST + ShiroUtils.getWorkShop(), activeEntity.getVin());
//        }

        logger.info("cancelActive---end");
        MDC.remove(loggerKey);

    }


    /**
     * 诊断激活列表
     */
    @Override
    public List<VehicleActiveEntity> list(Map<String, Object> params) {
        //查询条件
        String vin = (String) params.get("vin");
        String diagnosticUserName = (String) params.get("diagnosticUserName");
        String workShopName = (String) params.get("workShopName");
        //根据登陆的用户，经销商id获取，已激活诊断列表
        List<VehicleActiveEntity> l = getActiveList();
        List<VehicleActiveEntity> res = new ArrayList<>();
        if (l.size() > 0) {
            if (StringUtils.isNotBlank(vin) || StringUtils.isNotBlank(diagnosticUserName) || StringUtils.isNotBlank(workShopName)) {
                res = l.stream()
                        .filter(v ->
                                StrUtils.containsNotBlank(v.getWorkShopName(), workShopName)
                                        && StrUtils.containsNotBlank(v.getVin(), (vin))
                                        && StrUtils.containsNotBlank(ObjectUtils.isNull(v.getDiagnosticUserName()) ? "null" : v.getDiagnosticUserName(), diagnosticUserName)
                        ).collect(Collectors.toList());
            } else {
                res = l;
            }
        }
        logger.info("List ---- end");
        return res;
    }

    private List<VehicleActiveEntity> getActiveList() {
        logger.info("List ---- begin");
        Type listType = new TypeToken<List<VehicleActiveEntity>>() {
        }.getType();
        //根据登陆的用户，经销商id获取，已激活诊断列表
        List<VehicleActiveEntity> l = new ArrayList<>(16);
        //如果当前用户的角色是Diagnostician 诊断专家，则查询所有经销商车辆
        boolean hasDiagnostician = false;
        if(ObjectUtils.isNotNull(ShiroUtils.getUserEntity())){
            List<String> roles = ShiroUtils.getUserEntity().getRoles().stream().map(DisplayUserEntity.Role::getRoleCode).collect(Collectors.toList());
            hasDiagnostician = roles.contains("Diagnostician");
        }
        if (hasDiagnostician) {
            logger.info("key List value:" + redisUtils.getKeys(Constant.REDIS_LIST + "*"));
            Set<String> keys = redisUtils.getKeys(Constant.REDIS_LIST + "*");
            keys.forEach(key -> {
                List<VehicleActiveEntity> vl = redisUtils.get(key, listType);
                if (ObjectUtils.isNotNull(vl)) {
                    List<VehicleActiveEntity> vs = redisUtils.get(key, listType);
                    if (ObjectUtils.isNotNull(vs)) {
                        if (StringUtils.isNotBlank(ShiroUtils.getWorkShop())){
                            l.addAll(vs);
                        }
                    }
                }
            });
            logger.info("List value:" + l);
        } else {
            logger.info("List value:" + redisUtils.get(Constant.REDIS_LIST + ShiroUtils.getWorkShop()));
            if (StringUtils.isNotBlank(ShiroUtils.getWorkShop())){
                List<VehicleActiveEntity> vs = redisUtils.get(Constant.REDIS_LIST + ShiroUtils.getWorkShop(), listType);
                if (ObjectUtils.isNotNull(vs)) {
                    l.addAll(vs);
                }
            }
        }
        if (l.size() > 0) {
            for (int i = 0; i < l.size(); i++) {
                VehicleActiveEntity v = l.get(i);
                //判断诊断车辆是否有redis缓存,没有的话清除列表.
                if (!redisUtils.hasKey(Constant.REDIS_VIN + v.getVin())) {
                    l.remove(i);
                    i--;
                }
                // 不知道干啥。。。废代码
                /*if (ObjectUtils.isNull(v.getWorkShopName())) {
                    v.setWorkShopName(v.getWorkShopName());
                }*/
            }
            /*if (StringUtils.isNotBlank(ShiroUtils.getWorkShop())) {
                //存经销商诊断车辆信息
                redisUtils.set(Constant.REDIS_LIST + ShiroUtils.getWorkShop(), l);
            }*/
        }
        return l;
    }

    /**
     * 开始诊断
     */
    @Override
    public String begin(String vin) {
        String loggerKey = DateUtils.format(new Date(), "yyyyMMdd") + "/" + vin;
        MDC.put("vin", loggerKey);
        logger.info("Begin ---- begin");
        logger.info("Begin ---- vin:{}", vin);

        //1.判断入参是否正确
        if (StringUtils.isEmpty(vin)) {
            throw new RRException(Constant.Msg.CHECK_INPUT);
        }
        VehicleActiveEntity v = null;

        //2.判断redis 是否缓存诊断车辆 获取action id
        v = checkBeginRedisValue(vin);
        String actionId = diagnosticUtils.get4Code() + System.currentTimeMillis();

        //3.发送mq消息通知车辆
        JSONObject bodyData = new JSONObject();
        bodyData.put("mode", -1);
        bodyData.put("maker", -1);
        bodyData.put("userId", "");
        bodyData.put("username", "");
        bodyData.put("token", "");

        MqRequestBodyEntity mqRequestBodyEntity = new MqRequestBodyEntity();
        mqRequestBodyEntity.setData(bodyData);
        diagnosticUtils.sendMqMessage(v.getVin(), actionId, Constant.Function.BEGIN, Constant.Action.BEGIN, mqRequestBodyEntity);

        //4.修改缓存redis
        saveBeginRedis(v);

        logger.info("Begin ---- end");
        MDC.remove(loggerKey);
        return actionId;
    }


    /**
     * 结束诊断
     */
    @Override
    public String end(String vin) {
        String loggerKey = DateUtils.format(new Date(), "yyyyMMdd") + "/" + vin;
        MDC.put("vin", loggerKey);
        logger.info("End ---- begin");
        logger.info("End ---- vin:{}", vin);

        //1.判断入参是否正确
        if (StringUtils.isEmpty(vin)) {
            throw new RRException(Constant.Msg.CHECK_INPUT);
        }
        VehicleActiveEntity v = null;

        //2.判断redis 是否缓存诊断车辆 获取action id
        v = checkEndRedisValue(vin);
        String actionId = diagnosticUtils.get4Code() + System.currentTimeMillis();

        //3.发送mq消息通知车辆
        diagnosticUtils.sendMqMessage(v.getVin(), actionId, Constant.Function.END, Constant.Action.END, null);

        //4.保存action 信息
        diagnosticUtils.saveAction(v.getSessionId(), actionId, new String[]{}, Constant.Function.END, Constant.Action.END);

        logger.info("End ---- end");
        MDC.remove(loggerKey);
        return actionId;
    }

    /**
     * 车辆健康检查
     */
    @Override
    public String healthy(String vin) {
        String actionId = "";
        String loggerKey = DateUtils.format(new Date(), "yyyyMMdd") + "/" + vin;
        MDC.put("vin", loggerKey);
        logger.info("VHHS ---- begin");
        logger.info("VHHS ---- vin:{}", vin);
        //1.判断入参是否正确
        if (StringUtils.isEmpty(vin)) {
            throw new RRException(Constant.Msg.CHECK_INPUT);
        }
        VehicleActiveEntity v = null;

        //2.判断redis 是否缓存诊断车辆 获取action id
        v = checkRunRedisValue(vin);
        actionId = diagnosticUtils.get4Code() + System.currentTimeMillis();

        //3.发送mq消息通知车辆
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("executionMode", "ECU_AND_DTCS");
        MqRequestBodyEntity mqRequestBodyEntity = new MqRequestBodyEntity();
        mqRequestBodyEntity.setEcuName(new String[]{});
        mqRequestBodyEntity.setData(jsonObject);
        diagnosticUtils.sendMqMessage(v.getVin(), actionId, Constant.Function.VHSS, Constant.Action.VHSS_INIT, mqRequestBodyEntity);

        //4.保存action 信息
        diagnosticUtils.saveAction(v.getSessionId(), actionId, Constant.ECU_ALL, Constant.Function.VHSS, Constant.Action.VHSS_INIT);

        logger.info("VHHS ---- end");
        MDC.remove(loggerKey);
        return actionId;
    }

    /**
     * 读取版本信息
     */
    @Override
    public String readIdentData(String vin, String[] ecuNames) {
        String loggerKey = DateUtils.format(new Date(), "yyyyMMdd") + "/" + vin;
        MDC.put("vin", loggerKey);
        logger.info("readIdentData-----readIdentData");
        logger.info("readIdentData-----vin:{}" + vin);
        //1.判断入参是否正确
        if (StringUtils.isEmpty(vin)) {
            throw new RRException(Constant.Msg.CHECK_INPUT);
        }
        VehicleActiveEntity v = null;
        //2.判断redis是否缓存诊断车辆，获取action id
        v = checkRunRedisValue(vin);
        String actionId = diagnosticUtils.get4Code() + System.currentTimeMillis();

        //3.发送mq消息通知车辆
        if (ecuNames != null) {
            if (ecuNames.length == 0) {
                ecuNames = Constant.ECU_ALL;
            }
        } else {
            ecuNames = Constant.ECU_ALL;
        }

        JSONArray jsonArray = new JSONArray();
        for (String ecuName : ecuNames) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("ecuName", ecuName);
            jsonObject.put("didNames", new String[]{});
            jsonArray.add(jsonObject);
        }
        MqRequestBodyEntity mqRequestBodyEntity = new MqRequestBodyEntity();
        mqRequestBodyEntity.setEcuName(ecuNames);
        mqRequestBodyEntity.setData(jsonArray);

        diagnosticUtils.sendMqMessage(v.getVin(), actionId, Constant.Function.IDENTIFICATION, Constant.Action.IDENTIFICATION_INIT, mqRequestBodyEntity);

        //4.保存action信息
        diagnosticUtils.saveAction(v.getSessionId(), actionId, ecuNames, Constant.Function.IDENTIFICATION, Constant.Action.IDENTIFICATION_INIT);

        logger.info("readIdentData ----end");
        MDC.remove(loggerKey);
        return actionId;
    }

    /**
     * 读取故障码
     */
    @Override
    public String readDtcCode(String vin, String[] ecuNames) {
        String actionId = "";
        String loggerKey = DateUtils.format(new Date(), "yyyyMMdd") + "/" + vin;
        MDC.put("vin", loggerKey);
        logger.info("readDtcCode---begin");
        logger.info("readDtcCode----vin;{}" + vin);
        //1.判断sessionId是否为空
        if (StringUtils.isEmpty(vin)) {
            throw new RRException(Constant.Msg.CHECK_INPUT);
        }
        //2.判断redis是否缓存诊断车辆，获取action id
        VehicleActiveEntity v = null;
        v = checkRunRedisValue(vin);
        actionId = diagnosticUtils.get4Code() + System.currentTimeMillis();

        MqRequestBodyEntity mqRequestBodyEntity = new MqRequestBodyEntity();
        if (ecuNames != null && ecuNames.length > 0) {
            mqRequestBodyEntity.setEcuName(ecuNames);
        } else {
            mqRequestBodyEntity.setEcuName(Constant.ECU_ALL);
        }
        //3.发送mq消息通知车辆
        diagnosticUtils.sendMqMessage(v.getVin(), actionId, Constant.Function.DTC, Constant.Action.DTC_READ, mqRequestBodyEntity);

        //4.保存action信息
        diagnosticUtils.saveAction(v.getSessionId(), actionId, ecuNames, Constant.Function.DTC, Constant.Action.DTC_READ);

        logger.info("readDtcCode----end");
        MDC.remove(loggerKey);
        return actionId;
    }

    /**
     * 读取故障码 - 冻结帧
     */
    @Override
    public String readDtcDataGrid(String vin, String ecuName, String dtcCode, String dtcCodeInt) {
        String actionId = "";
        String loggerKey = DateUtils.format(new Date(), "yyyyMMdd") + "/" + vin;
        MDC.put("vin", loggerKey);
        logger.info("readDtcDataGrid---begin");
        logger.info("readDtcDataGrid----vin;{}" + vin);
        //1.判断vin是否为空
        if (StringUtils.isEmpty(vin) || StringUtils.isEmpty(ecuName) || StringUtils.isEmpty(dtcCode) || StringUtils.isEmpty(dtcCodeInt)) {
            throw new RRException(Constant.Msg.CHECK_INPUT);
        }
        //2.判断redis是否缓存诊断车辆，获取action id
        VehicleActiveEntity v = null;
        v = checkRunRedisValue(vin);
        actionId = diagnosticUtils.get4Code() + System.currentTimeMillis();

        //把请求的参数放入到JSon 数据的 data字段里
        JSONObject bodyData = new JSONObject();
        bodyData.put("ecuName", ecuName);
        bodyData.put("dtcCode", dtcCode);
        bodyData.put("dtcCode_Int", dtcCodeInt);

        //3.发送mq消息通知车辆
        MqRequestBodyEntity mqRequestBodyEntity = new MqRequestBodyEntity();
        mqRequestBodyEntity.setEcuName(new String[]{ecuName});
        mqRequestBodyEntity.setData(bodyData);
        diagnosticUtils.sendMqMessage(v.getVin(), actionId, Constant.Function.DTC, Constant.Action.DTC_SNAPSHOT, mqRequestBodyEntity);

        //4.保存action信息
        diagnosticUtils.saveAction(v.getSessionId(), actionId, new String[]{ecuName}, Constant.Function.DTC, Constant.Action.DTC_SNAPSHOT);

        logger.info("readDtcDataGrid----end");
        MDC.remove(loggerKey);
        return actionId;
    }

    /**
     * 清除故障码
     */
    @Override
    public String clearDtc(String vin, String[] ecuNames) {
        String actionId = "";
        String loggerKey = DateUtils.format(new Date(), "yyyyMMdd") + "/" + vin;
        MDC.put("vin", loggerKey);
        logger.info("clearDtc---begin");
        logger.info("clearDtc----vin;{}" + vin);
        //1.判断sessionId是否为空
        if (StringUtils.isEmpty(vin)) {
            throw new RRException(Constant.Msg.CHECK_INPUT);
        }
        //2.判断redis是否缓存诊断车辆，获取action id
        VehicleActiveEntity v = null;
        v = checkRunRedisValue(vin);
        actionId = diagnosticUtils.get4Code() + System.currentTimeMillis();

        MqRequestBodyEntity mqRequestBodyEntity = new MqRequestBodyEntity();
        mqRequestBodyEntity.setEcuName(ecuNames);
        //3.发送mq消息通知车辆
        diagnosticUtils.sendMqMessage(v.getVin(), actionId, Constant.Function.DTC, Constant.Action.DTC_CLEARDTC, mqRequestBodyEntity);

        //4.保存action信息
        diagnosticUtils.saveAction(v.getSessionId(), actionId, ecuNames, Constant.Function.DTC, Constant.Action.DTC_CLEARDTC);

        logger.info("clearDtc----end");
        MDC.remove(loggerKey);
        return actionId;
    }

    //车辆报告
    @Override
    public String readVehicleReport(String vin) {
        String[] ecuNames = {};
        String actionId = "";
        String loggerKey = DateUtils.format(new Date(), "yyyyMMdd") + "/" + vin;
        MDC.put(vin, loggerKey);
        logger.info("readVehicleReport-----begin");
        logger.info("readCehicleReport------vin:{}" + vin);
        if (StringUtils.isEmpty(vin)) {
            throw new RRException(Constant.Msg.CHECK_INPUT);
        }
        VehicleActiveEntity v = null;
        v = checkRunRedisValue(vin);
        actionId = diagnosticUtils.get4Code() + System.currentTimeMillis();

        diagnosticUtils.saveAction(v.getSessionId(), actionId, ecuNames, Constant.Function.REPORT, Constant.Action.REPORT);

        logger.info("readVehicleReport-----end");
        MDC.remove(loggerKey);
        return null;
    }

    /**
     * 激活检查token是否生效
     */
    private DisplayUserEntity checkToken(String token) {
        if (StringUtils.isEmpty(token)) {
            throw new RRException(Constant.Msg.CHECK_INPUT);
        }
        return redisUtils.get(token,DisplayUserEntity.class);
    }

    /**
     * 判断redis 是否缓存诊断车辆
     */
    private VehicleActiveEntity checkBeginRedisValue(String vin) {
        //诊断车辆信息	H+VIN	"VersionActiveEntity"
        logger.info("Begin Redis key:" + Constant.REDIS_VIN + vin);
        logger.info("Begin Redis value:" + redisUtils.get(Constant.REDIS_VIN + vin));
        VehicleActiveEntity v = null;
        //车辆缓存信息存在 检查状态是否可执行， 缓存不存在，提示车辆连接已断
        if (redisUtils.hasKey(Constant.REDIS_VIN + vin)) {
            v = redisUtils.get(Constant.REDIS_VIN + vin, VehicleActiveEntity.class);
            ///如果已激状态 判断是否有诊断命令执行，如果非已激状态，判断状态：诊断中，返回提示，其它状态清缓存并提示
            if (Constant.SESSION_STATUS.ACTIVE.getValue() == v.getStatus()) {
                //是否有开始诊断在运行
                if (redisUtils.hasKey(Constant.REDIS_RUN_VIN + vin)) {
                    logger.info("Begin ---- have diagnostic run");
                    throw new RRException(Constant.Msg.DIA_RUN);
                }
            } else if (Constant.SESSION_STATUS.PROCES.getValue() == v.getStatus()) {
                logger.info("End ---- session status ={} not  PROCES-------end ", v.getStatus());
                if (!v.getDiagnosticUserId().equals(ShiroUtils.getUserId())) {
                    throw new RRException(Constant.Msg.CHECK_BEGIN_USER);
                } else {
                    throw new RRException(Constant.Msg.DIA_RUN);
                }
            }
        } else {
            //断开连接，清除经销商关联诊断车辆
            List vehicleActiveEntityList = redisUtils.get(Constant.REDIS_LIST + ShiroUtils.getWorkShop(), (new ArrayList()).getClass());
            if (vehicleActiveEntityList != null && vehicleActiveEntityList.size() > 0) {
                vehicleActiveEntityList.removeIf(versionActiveEntity -> (versionActiveEntity.toString().indexOf(vin) > 0));
            }
            if (ObjectUtils.isNotNull(ShiroUtils.getWorkShop())) {
                redisUtils.set(Constant.REDIS_LIST + ShiroUtils.getWorkShop(), vehicleActiveEntityList);
            }
            logger.info("Begin ---- redis getKey{} clear:{} ", Constant.REDIS_LIST + ShiroUtils.getWorkShop(), vin);
            logger.info("Begin ---- vehicle is null -------end ");
            throw new RRException(Constant.Msg.DIA_DEC);
        }

        //设置诊断用户
        v.setDiagnosticUserId(ShiroUtils.getUserId());
        v.setDiagnosticUserName(ShiroUtils.getUserDisplayName());
        //设置 车辆检测列表
        List vehicleActiveEntityList = redisUtils.get(Constant.REDIS_LIST + ShiroUtils.getWorkShop(), (new ArrayList()).getClass());
        if (vehicleActiveEntityList != null && vehicleActiveEntityList.size() > 0) {
            vehicleActiveEntityList.removeIf(versionActiveEntity -> (versionActiveEntity.toString().indexOf(vin) > 0));
        } else {
            vehicleActiveEntityList = new ArrayList<VehicleActiveEntity>();
        }
        vehicleActiveEntityList.add(v);

        return v;
    }

    /**
     * 判断redis 是否缓存诊断车辆
     */
    private VehicleActiveEntity checkEndRedisValue(String vin) {
        //诊断车辆信息	H+VIN	"VersionActiveEntity"
        logger.info("End Redis key:" + Constant.REDIS_VIN + vin);
        logger.info("End Redis value:" + redisUtils.get(Constant.REDIS_VIN + vin));
        VehicleActiveEntity v = null;
        //车辆缓存信息存在 检查状态是否可执行， 缓存不存在，提示车辆连接已断
        if (redisUtils.hasKey(Constant.REDIS_VIN + vin)) {
            v = redisUtils.get(Constant.REDIS_VIN + vin, VehicleActiveEntity.class);
            //判断状态：非诊断中，返回提示，诊断状态不正确   ,诊断中,判断是否有诊断命令执行,没有命令执行时,可结束诊断.
            if (Constant.SESSION_STATUS.PROCES.getValue() == v.getStatus()) {
                //判断结束诊断用户,是否是当衣谁有断用户
                if (ShiroUtils.getUserId().longValue() != v.getDiagnosticUserId().longValue()) {
                    logger.info("End ---- session is not null -------end ");
                    throw new RRException(Constant.Msg.CHECK_ACTIVE_USER);
                }
                if (redisUtils.hasKey(Constant.REDIS_RUN_VIN + vin)) {
                    logger.info("End ---- session is not null -------end ");
                    throw new RRException(Constant.Msg.DIA_RUN);
                }
            }
        } else {
            //断开连接，清除经销商关联诊断车辆
            List vehicleActiveEntityList = redisUtils.get(Constant.REDIS_LIST + ShiroUtils.getWorkShop(), (new ArrayList()).getClass());
            if (vehicleActiveEntityList != null && vehicleActiveEntityList.size() > 0) {
                vehicleActiveEntityList.removeIf(versionActiveEntity -> (versionActiveEntity.toString().indexOf(vin) > 0));
                if (ObjectUtils.isNotNull(ShiroUtils.getWorkShop())) {
                    redisUtils.set(Constant.REDIS_LIST + ShiroUtils.getWorkShop(), vehicleActiveEntityList);
                }
                logger.info("Begin ---- redis getKey{} clear:{} ", Constant.REDIS_LIST + ShiroUtils.getWorkShop(), vin);
                throw new RRException(Constant.Msg.DIA_DEC);
            }
        }
        return v;
    }

    /**
     * 判断redis 是否缓存诊断车辆
     */
    private void checkCancelRedisValue(String vin) {
        //诊断车辆信息	H+VIN	"VersionActiveEntity"
        logger.info("End Redis key:" + Constant.REDIS_VIN + vin);
        logger.info("End Redis value:" + redisUtils.get(Constant.REDIS_VIN + vin));
        //车辆缓存信息存在 检查状态是否可执行， 缓存不存在，提示车辆连接已断
        if (!redisUtils.hasKey(Constant.REDIS_VIN + vin)) {
            //断开连接，清除经销商关联诊断车辆
            List vehicleActiveEntityList = redisUtils.get(Constant.REDIS_LIST + ShiroUtils.getWorkShop(), (new ArrayList()).getClass());
            if (vehicleActiveEntityList != null && vehicleActiveEntityList.size() > 0) {
                vehicleActiveEntityList.removeIf(versionActiveEntity -> (versionActiveEntity.toString().indexOf(vin) > 0));
                if (ObjectUtils.isNotNull(ShiroUtils.getWorkShop())) {
                    redisUtils.set(Constant.REDIS_LIST + ShiroUtils.getWorkShop(), vehicleActiveEntityList);
                }
                logger.info("End ---- redis getKey{} clear:{} ", Constant.REDIS_LIST + ShiroUtils.getWorkShop(), vin);
                throw new RRException(Constant.Msg.DIA_CAN_ACTIVE);
            }
        }
    }

    /**
     * 判断redis 是否缓存诊断车辆
     */
    @Override
    public VehicleActiveEntity checkRunRedisValue(String vin) {
        //诊断车辆信息	H+VIN	"VersionActiveEntity"
        logger.info("End Redis key:" + Constant.REDIS_VIN + vin);
        logger.info("End Redis value:" + redisUtils.get(Constant.REDIS_VIN + vin));
        VehicleActiveEntity v = null;
        //车辆缓存信息存在 检查状态是否可执行， 缓存不存在，提示车辆连接已断
        if (redisUtils.hasKey(Constant.REDIS_VIN + vin)) {
            v = redisUtils.get(Constant.REDIS_VIN + vin, VehicleActiveEntity.class, Constant.VEHICLE_REDIS_EXPIRE);
            if (!v.getDiagnosticUserId().equals(ShiroUtils.getUserId())) {
                logger.info("End ---- session status ={} not  PROCES-------end ", v.getStatus());
                throw new RRException(Constant.Msg.CHECK_BEGIN_USER);
            }

            //判断状态：非诊断中，返回提示，诊断状态不正确   ,诊断中,判断是否有诊断命令执行,没有命令执行时,可结束诊断.
            if (Constant.SESSION_STATUS.PROCES.getValue() != v.getStatus()) {
                logger.info("End ---- session status ={} not  PROCES-------end ", v.getStatus());
                throw new RRException(Constant.Msg.DIA_BEING_STATUS_CONT_ACTIVE);
            } else {
                if (redisUtils.hasKey(Constant.REDIS_RUN_VIN + vin)) {
                    logger.info("End ---- session is not null -------end ");
                    throw new RRException(Constant.Msg.DIA_RUN);
                }
            }
        } else {
            //断开连接，清除经销商关联诊断车辆
            List vehicleActiveEntityList = redisUtils.get(Constant.REDIS_LIST + ShiroUtils.getWorkShop(), (new ArrayList()).getClass());
            if (vehicleActiveEntityList != null && vehicleActiveEntityList.size() > 0) {
                vehicleActiveEntityList.removeIf(versionActiveEntity -> (versionActiveEntity.toString().indexOf(vin) > 0));
            }
            if (ObjectUtils.isNotNull(ShiroUtils.getWorkShop())) {
                redisUtils.set(Constant.REDIS_LIST + ShiroUtils.getWorkShop(), vehicleActiveEntityList);
            }
            logger.info("End ---- redis getKey{} clear:{} ", Constant.REDIS_LIST + ShiroUtils.getWorkShop(), vin);
            throw new RRException(Constant.Msg.DIA_CAN_ACTIVE);
        }
        return v;
    }

    @Override
    public void checkOwner(String vin) {
        //诊断车辆信息	H+VIN	"VersionActiveEntity"
        logger.info("End Redis key:" + Constant.REDIS_VIN + vin);
        logger.info("End Redis value:" + redisUtils.get(Constant.REDIS_VIN + vin));
        VehicleActiveEntity v = null;
        //车辆缓存信息存在 检查状态是否可执行， 缓存不存在，提示车辆连接已断
        if (redisUtils.hasKey(Constant.REDIS_VIN + vin)) {
            v = redisUtils.get(Constant.REDIS_VIN + vin, VehicleActiveEntity.class);
            //判断状态：非诊断中，返回提示，诊断状态不正确   ,诊断中,判断是否有诊断命令执行,没有命令执行时,可结束诊断.
            if (Constant.SESSION_STATUS.PROCES.getValue() == v.getStatus()) {
                //判断结束诊断用户,是否是当衣谁有断用户
                if (ShiroUtils.getUserId().longValue() != v.getActiveUserId().longValue()) {
                    logger.info("End ---- session is not null -------end ");
                    throw new RRException(Constant.Msg.CHECK_ACTIVE_USER);
                }
                if (redisUtils.hasKey(Constant.REDIS_RUN_VIN + vin)) {
                    logger.info("End ---- session is not null -------end ");
                    throw new RRException(Constant.Msg.DIA_RUN);
                }
            }
        } else {
            //断开连接，清除经销商关联诊断车辆
            List vehicleActiveEntityList = redisUtils.get(Constant.REDIS_LIST + ShiroUtils.getWorkShop(), (new ArrayList()).getClass());
            if (vehicleActiveEntityList != null && vehicleActiveEntityList.size() > 0) {
                vehicleActiveEntityList.removeIf(versionActiveEntity -> (versionActiveEntity.toString().indexOf(vin) > 0));
                if (ObjectUtils.isNotNull(ShiroUtils.getWorkShop())) {
                    redisUtils.set(Constant.REDIS_LIST + ShiroUtils.getWorkShop(), vehicleActiveEntityList);
                }
                logger.info("Begin ---- redis getKey{} clear:{} ", Constant.REDIS_LIST + ShiroUtils.getWorkShop(), vin);
                throw new RRException(Constant.Msg.DIA_DEC);
            }
        }
    }

    /**
     * 解析日志
     */
    @Override
    public void synchronizeLogs() {
        Map<String, Object> map = new HashMap<>();
        map.put("source", Constant.SESSION_SOURCE.OFFLINE.getDbName());
        logsUtils.startLogging(map);
    }

    private void saveBeginRedis(VehicleActiveEntity v) {
        //设置 redis
        v.setStatus(Constant.SESSION_STATUS.ACTIVE.getValue());
        v.setDiagnosticUserId(ShiroUtils.getUserId());
        v.setDiagnosticUserName(ShiroUtils.getUserName());
        v.setDiagnosticTime(System.currentTimeMillis());
        redisUtils.set(Constant.REDIS_VIN + v.getVin(), v, Constant.Function.BEGIN.getExpire());

        logger.info("Begin ---- Redis key({}) setValue:{}", Constant.REDIS_VIN + v.getVin(), v);

        //设置 车辆检测列表
        List vehicleActiveEntityList = redisUtils.get(Constant.REDIS_LIST + ShiroUtils.getWorkShop(), (new ArrayList()).getClass());
        if (vehicleActiveEntityList != null && vehicleActiveEntityList.size() > 0) {
            vehicleActiveEntityList.removeIf(versionActiveEntity -> (versionActiveEntity.toString().indexOf(v.getVin()) > 0));
        } else {
            vehicleActiveEntityList = new ArrayList<VehicleActiveEntity>();
        }
        vehicleActiveEntityList.add(v);

        //除经销商id 关联 车辆诊断 缓存
        if (ObjectUtils.isNotNull(ShiroUtils.getWorkShop())) {
            redisUtils.set(Constant.REDIS_LIST + ShiroUtils.getWorkShop(), vehicleActiveEntityList);
        }
        logger.info("Begin ---- Redis key({}) setValue:{}", Constant.REDIS_LIST + ShiroUtils.getWorkShop(), vehicleActiveEntityList);
    }

    /**
     * 车辆激活缓存
     */
    private VehicleActiveEntity getVehicleActiveEntity(ActiveEntity activeEntity, DisplayUserEntity displayUserEntity) {
        logger.info("Active getVersionActiveEntity");
        //校验vin 码
        DVehicleInfoEntity dVehicleInfoEntity = dVehicleInfoDao.selectOneByVin(activeEntity.getVin());
        if (dVehicleInfoEntity == null) {
            logger.info("Active new VIN {} fald", activeEntity.getVin());
            throw new RRException(Constant.Msg.CHECK_VIN);
        }

        //如果车辆诊断信息在缓存中
        if (redisUtils.hasKey(Constant.REDIS_VIN + activeEntity.getVin())) {
            logger.info("Active new VIN {} fald", activeEntity.getVin());
            //throw new RRException( "诊断仪已激活,无需再次激活!" ,200 );
            //已激活 取消激活  再重新激活
            //已激活 修改session信息
            dSessionInfoDao.updateSessionEndTimeByVin(activeEntity.getVin());

            //已激活 删除redis里的缓存
            redisUtils.delete(Constant.REDIS_VIN + activeEntity.getVin());
        }


        List vehicleActiveEntityList = redisUtils.get(Constant.REDIS_LIST + displayUserEntity.getUserInfo().getWorkShop(), (new ArrayList()).getClass());

        VehicleActiveEntity v = new VehicleActiveEntity();
        v.setVin(activeEntity.getVin());
        v.setWorkShopName(displayUserEntity.getUserInfo().getWorkShop());
        v.setActiveUserId(Long.valueOf(displayUserEntity.getUserInfo().getUserId()));
        v.setActiveUserName(displayUserEntity.getUserInfo().getUsername());
        v.setActiveTime(System.currentTimeMillis());
        v.setStatus(Constant.SESSION_STATUS.ACTIVE.getValue());

        logger.info("Active VersionActiveEntity:{}", JSONObject.toJSONString(v));

        if (vehicleActiveEntityList != null) {
            vehicleActiveEntityList.removeIf(versionActiveEntity -> (versionActiveEntity.toString().indexOf(activeEntity.getVin()) > 0));
        } else {
            vehicleActiveEntityList = new ArrayList<VehicleActiveEntity>();
        }

        vehicleActiveEntityList.add(v);
        //车辆诊断信息 缓存
        redisUtils.set(Constant.REDIS_VIN + activeEntity.getVin(), v);
        //除经销商id 关联 车辆诊断 缓存
        if (StringUtils.isNotBlank(displayUserEntity.getUserInfo().getWorkShop())){
            redisUtils.set(Constant.REDIS_LIST + displayUserEntity.getUserInfo().getWorkShop(), vehicleActiveEntityList);
        }
        logger.info("Active redis add key:{},{}", Constant.REDIS_VIN + activeEntity.getVin(), Constant.REDIS_LIST + displayUserEntity.getUserInfo().getWorkShop());
        return v;
    }


    @Override
    public List<SessionEcuVersionDto> selectSessionEcuVersion(String sessionId) {
        return dActionVersionDao.selectSessionEcuVersion(sessionId);
    }

    @Override
    public List<DActionDtcEntity> sessionActionDtc(String sessionId) {
        return dActionDtcDao.sessionActionDtc(sessionId);
    }

    @Override
    public List<DActionInfoEntity> sessionActionInfo(String sessionId) {
        return dActionInfoDao.selectListBySessionId(sessionId);
    }

    @Override
    public DSessionInfoEntity findSessionInfo(String sessionId, String vin) {
        return dSessionInfoDao.findOneBySessionID(sessionId, vin);
    }

}