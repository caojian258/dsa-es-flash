package org.dsa.modules.sys.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class SysTranslationsDto implements Serializable {

    private String ti;

    /**
     * 词条内容
     */
    private String text;
}
