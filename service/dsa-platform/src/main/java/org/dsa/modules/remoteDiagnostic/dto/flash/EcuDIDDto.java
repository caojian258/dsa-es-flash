package org.dsa.modules.remoteDiagnostic.dto.flash;

import lombok.Data;
import org.dsa.modules.remoteDiagnostic.vo.Flash.FlashFileVo;

import java.io.Serializable;
import java.util.List;

@Data
public class EcuDIDDto implements Serializable {

    private String ecuName;

    private String ecuVersion;

    private String versionType;

    private Integer isCross;

    //文件下载地址
    private String fileDownloadUrl;

    //文件名
    private String fileDownloadName;

    //文件大小
    private Long fileDownloadSize;

    //文件hash值
    private String fileDownloadMd5;

    List<EcuDIDItemDto> didList;

    //ECU刷写列表
    List<FlashFileVo> flashFiles;
}
