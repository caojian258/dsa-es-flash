package org.dsa.modules.reporter.entity;

import lombok.Data;

@Data
public class LdapUser {
    public String cn;
    public String sn;
    public String uid;
    public String userPassword;
    public String displayName;
    public String mail;
    public String description;
    public String uidNumber;
    public String gidNumber;
    public String status;
}
