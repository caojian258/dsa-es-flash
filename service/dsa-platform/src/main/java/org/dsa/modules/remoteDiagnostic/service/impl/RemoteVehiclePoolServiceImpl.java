package org.dsa.modules.remoteDiagnostic.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.utils.ObjectUtils;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.remoteDiagnostic.config.RemoteDiagnosticConfig;
import org.dsa.modules.remoteDiagnostic.constant.RemoteDiagnosticConstant;
import org.dsa.modules.remoteDiagnostic.dao.*;
import org.dsa.modules.remoteDiagnostic.entity.*;
import org.dsa.modules.remoteDiagnostic.service.RemoteVehiclePoolService;
import org.dsa.modules.remoteDiagnostic.service.RemoteVehicleTagService;
import org.dsa.modules.remoteDiagnostic.vo.CheckNameReqVo;
import org.dsa.modules.remoteDiagnostic.vo.CheckStatusReqVo;
import org.dsa.modules.remoteDiagnostic.vo.VehiclePoolPageReVo;
import org.dsa.modules.remoteDiagnostic.vo.VinCode.VinCodeReqPageVo;
import org.dsa.modules.remoteDiagnostic.vo.VinCode.VinCodeReqVo;
import org.dsa.modules.vehicle.dao.DVehicleInfoDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class RemoteVehiclePoolServiceImpl implements RemoteVehiclePoolService {

    @Autowired
    RemoteVehiclePoolDao poolDao;

    @Autowired
    VinCodeDao vinCodeDao;

    @Autowired
    private DVehicleInfoDao dVehicleInfoDao;
    @Autowired
    private RemoteVehicleTagService remoteVehicleTagService;
    @Autowired
    private RVehicleTagDao vehicleTagDao;
    @Autowired
    private RemotePoolDependDao poolDependDao;
    @Autowired
    private RemotePoolVehicleDao poolVehicleDao;
    @Autowired
    private RemoteDiagnosticConfig config;
    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private DiagnosticTaskCampaignDao campaignDao;

    @Override
    public Page<RemoteVehiclePoolEntity> selectPage(VehiclePoolPageReVo vo) {
        Page<RemoteVehiclePoolEntity> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);

        return poolDao.queryPage(page, vo.getName() == null ? null : vo.getName().trim(), vo.getStatus());
    }

    @Override
    public RemoteVehiclePoolEntity getInfo(Long id) {
//        RemoteVehiclePoolEntity info = poolDao.getInfo(id);
//        info.setSalesArea(poolVehicleDao.selectListByGroup(id, config.getSalesArea()));
//        info.setPlaceOfProduction(poolVehicleDao.selectListByGroup(id, config.getPlaceOfProduction()));
//        info.setProductionDate(poolVehicleDao.selectListByGroup(id, config.getProductionDate()));

        Object jsonString = redisUtils.hget(RemoteDiagnosticConstant.VEHICLE_POOL_KEY, RemoteDiagnosticConstant.CAMPAIGN_ITEM_KEY + id);
        Gson gson = new Gson();
        RemoteVehiclePoolEntity entity;
        if (ObjectUtils.isEmpty(jsonString)) {
            entity = poolDao.getInfo(id);
            // 更新缓存
            redisUtils.hset(RemoteDiagnosticConstant.VEHICLE_POOL_KEY, RemoteDiagnosticConstant.VEHICLE_POOL_ITEM_KEY + entity.getId(), gson.toJson(entity));
        } else {
            entity = gson.fromJson(jsonString.toString(), RemoteVehiclePoolEntity.class);
        }

        Type type = new TypeToken<List<Long>>() {
        }.getType();
        if (redisUtils.hasKey(RemoteDiagnosticConstant.VEHICLE_POOL_CONFIG_KEY + id + ":" + config.getSalesArea())) {
            entity.setSalesArea(gson.fromJson(redisUtils.get(RemoteDiagnosticConstant.VEHICLE_POOL_CONFIG_KEY + id + ":" + config.getSalesArea()), type));
        } else {
            List<Long> sa = poolVehicleDao.selectListByGroup(id, config.getSalesArea());
            entity.setSalesArea(sa);
            redisUtils.set(RemoteDiagnosticConstant.VEHICLE_POOL_CONFIG_KEY + id + ":" + config.getSalesArea(), gson.toJson(sa), RemoteDiagnosticConstant.ONE_DAY_TIMEOUT);
        }

        if (redisUtils.hasKey(RemoteDiagnosticConstant.VEHICLE_POOL_CONFIG_KEY + id + ":" + config.getPlaceOfProduction())) {
            entity.setPlaceOfProduction(gson.fromJson(redisUtils.get(RemoteDiagnosticConstant.VEHICLE_POOL_CONFIG_KEY + id + ":" + config.getPlaceOfProduction()), type));
        } else {
            List<Long> pp = poolVehicleDao.selectListByGroup(id, config.getPlaceOfProduction());
            entity.setPlaceOfProduction(pp);
            redisUtils.set(RemoteDiagnosticConstant.VEHICLE_POOL_CONFIG_KEY + id + ":" + config.getPlaceOfProduction(), gson.toJson(pp), RemoteDiagnosticConstant.ONE_DAY_TIMEOUT);
        }

        if (redisUtils.hasKey(RemoteDiagnosticConstant.VEHICLE_POOL_CONFIG_KEY + id + ":" + config.getProductionDate())) {
            entity.setProductionDate(gson.fromJson(redisUtils.get(RemoteDiagnosticConstant.VEHICLE_POOL_CONFIG_KEY + id + ":" + config.getProductionDate()), type));
        } else {
            List<Long> pd = poolVehicleDao.selectListByGroup(id, config.getProductionDate());
            entity.setProductionDate(pd);
            redisUtils.set(RemoteDiagnosticConstant.VEHICLE_POOL_CONFIG_KEY + id + ":" + config.getProductionDate(), gson.toJson(pd), RemoteDiagnosticConstant.ONE_DAY_TIMEOUT);
        }

        return entity;
    }

    @Override
    @Transactional
    public void savePool(RemoteVehiclePoolEntity pool) {
        poolDao.insert(pool);
        Gson gson = new Gson();

        if (pool.getTags() != null && pool.getTags().size() != 0) {
//            poolVinTagDao.inserts(pool.getId(), pool.getTags());
            poolDependDao.insertsTag(pool.getId(), pool.getTags());
        }

        if (pool.getVehicleTypeIds() != null && pool.getVehicleTypeIds().size() != 0) {
            poolDependDao.insertsType(pool.getId(), pool.getVehicleTypeIds());
        }

        if (pool.getVersions() != null && pool.getVersions().size() != 0) {
            poolDependDao.insertsVersion(pool.getId(), pool.getVersions());
        }

        if (pool.getEcuVersions() != null && pool.getEcuVersions().size() != 0) {
            poolDependDao.insertsEcu(pool.getId(), pool.getEcuVersions());
        }

        if (pool.getSalesArea() != null && pool.getSalesArea().size() != 0) {
            poolVehicleDao.inserts(pool.getId(), pool.getSalesArea(), config.getSalesArea());
        }
        if (pool.getPlaceOfProduction() != null && pool.getPlaceOfProduction().size() != 0) {
            poolVehicleDao.inserts(pool.getId(), pool.getPlaceOfProduction(), config.getPlaceOfProduction());
        }

        if (pool.getProductionDate() != null && pool.getProductionDate().size() != 0) {
            poolVehicleDao.inserts(pool.getId(), pool.getProductionDate(), config.getProductionDate());
        }

        redisUtils.set(RemoteDiagnosticConstant.VEHICLE_POOL_CONFIG_KEY + pool.getId() + ":" + config.getPlaceOfProduction(), gson.toJson(pool.getPlaceOfProduction()), RemoteDiagnosticConstant.ONE_DAY_TIMEOUT);
        redisUtils.set(RemoteDiagnosticConstant.VEHICLE_POOL_CONFIG_KEY + pool.getId() + ":" + config.getSalesArea(), gson.toJson(pool.getSalesArea()), RemoteDiagnosticConstant.ONE_DAY_TIMEOUT);
        redisUtils.set(RemoteDiagnosticConstant.VEHICLE_POOL_CONFIG_KEY + pool.getId() + ":" + config.getProductionDate(), gson.toJson(pool.getProductionDate()), RemoteDiagnosticConstant.ONE_DAY_TIMEOUT);

        // 更新缓存
        pool.setSalesArea(null);
        pool.setProductionDate(null);
        pool.setPlaceOfProduction(null);
        redisUtils.hset(RemoteDiagnosticConstant.VEHICLE_POOL_KEY, RemoteDiagnosticConstant.VEHICLE_POOL_ITEM_KEY + pool.getId(), gson.toJson(pool));

        redisUtils.set(RemoteDiagnosticConstant.VEHICLE_POOL_LOCK, System.currentTimeMillis());

    }

    @Override
    @Transactional
    public void updatePool(RemoteVehiclePoolEntity pool) {
        pool.setUpdatedAt(new Date());
        poolDao.updateById(pool);
        Gson gson = new Gson();

        poolDependDao.delete(Wrappers.<RemotePoolDependEntity>lambdaQuery().eq(RemotePoolDependEntity::getPoolId, pool.getId()));
        poolVehicleDao.delete(Wrappers.<RemotePoolVehicleEntity>lambdaQuery().eq(RemotePoolVehicleEntity::getPoolId, pool.getId()));

        if (pool.getTags() != null && pool.getTags().size() != 0) {
            poolDependDao.insertsTag(pool.getId(), pool.getTags());
        }

        if (pool.getVehicleTypeIds() != null && pool.getVehicleTypeIds().size() != 0) {
            poolDependDao.insertsType(pool.getId(), pool.getVehicleTypeIds());
        }

        if (pool.getVersions() != null && pool.getVersions().size() != 0) {
            poolDependDao.insertsVersion(pool.getId(), pool.getVersions());
        }

        if (pool.getEcuVersions() != null && pool.getEcuVersions().size() != 0) {
            poolDependDao.insertsEcu(pool.getId(), pool.getEcuVersions());
        }

        if (pool.getSalesArea() != null && pool.getSalesArea().size() != 0) {
            poolVehicleDao.inserts(pool.getId(), pool.getSalesArea(), config.getSalesArea());
        }
        if (pool.getPlaceOfProduction() != null && pool.getPlaceOfProduction().size() != 0) {
            poolVehicleDao.inserts(pool.getId(), pool.getPlaceOfProduction(), config.getPlaceOfProduction());
        }
        if (pool.getProductionDate() != null && pool.getProductionDate().size() != 0) {
            poolVehicleDao.inserts(pool.getId(), pool.getProductionDate(), config.getProductionDate());
        }

        redisUtils.set(RemoteDiagnosticConstant.VEHICLE_POOL_CONFIG_KEY + pool.getId() + ":" + config.getSalesArea(), gson.toJson(pool.getSalesArea()), RemoteDiagnosticConstant.ONE_DAY_TIMEOUT);
        redisUtils.set(RemoteDiagnosticConstant.VEHICLE_POOL_CONFIG_KEY + pool.getId() + ":" + config.getPlaceOfProduction(), gson.toJson(pool.getPlaceOfProduction()), RemoteDiagnosticConstant.ONE_DAY_TIMEOUT);
        redisUtils.set(RemoteDiagnosticConstant.VEHICLE_POOL_CONFIG_KEY + pool.getId() + ":" + config.getProductionDate(), gson.toJson(pool.getProductionDate()), RemoteDiagnosticConstant.ONE_DAY_TIMEOUT);

        // 更新缓存
        pool.setSalesArea(null);
        pool.setProductionDate(null);
        pool.setPlaceOfProduction(null);
        redisUtils.hset(RemoteDiagnosticConstant.VEHICLE_POOL_KEY, RemoteDiagnosticConstant.VEHICLE_POOL_ITEM_KEY + pool.getId(), gson.toJson(pool));

        redisUtils.set(RemoteDiagnosticConstant.VEHICLE_POOL_LOCK, System.currentTimeMillis());

    }

    @Override
    public List<RemoteVehiclePoolEntity> getList() {
        return poolDao.selectList(Wrappers.<RemoteVehiclePoolEntity>lambdaQuery().eq(RemoteVehiclePoolEntity::getStatus, 0));
    }

    @Override
    public Boolean check(CheckNameReqVo vo) {
        switch (vo.getType()) {
            case "pool":
                LambdaQueryWrapper<RemoteVehiclePoolEntity> qw = Wrappers.<RemoteVehiclePoolEntity>lambdaQuery();
                qw.eq(RemoteVehiclePoolEntity::getName, vo.getName());
                if (vo.getId() != null) {
                    qw.ne(RemoteVehiclePoolEntity::getId, vo.getId());
                }
                qw.select(RemoteVehiclePoolEntity::getId);
                return poolDao.selectOne(qw) == null;
            case "tag":
                return remoteVehicleTagService.check(vo);
//            case "version":
//                versionService.check(vo.getName());
//                break;
            default:
                return true;
        }
    }

    @Override
    public Boolean checkStatus(CheckStatusReqVo vo) {
        switch (vo.getType()) {
            case "pool":
                LambdaQueryWrapper<DiagnosticTaskCampaignEntity> qw = Wrappers.<DiagnosticTaskCampaignEntity>lambdaQuery();
                qw.eq(DiagnosticTaskCampaignEntity::getPoolId, vo.getId());
                qw.select(DiagnosticTaskCampaignEntity::getId);
                return ObjectUtils.isEmpty(campaignDao.selectOne(qw));
            default:
                return true;
        }
    }

    @Override
    public Page<String> getVinCode(VinCodeReqPageVo vo) {
        Page<String> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        Page<String> result;
//        if (StringUtils.isBlank(vo.getVin()) && StringUtils.isNotBlank(vo.getKey())) {
//            result = dVehicleInfoDao.getVinCodeByKey(page, vo.getKey());
//        } else {
        result = dVehicleInfoDao.getVinCode(page, vo.getKey(), vo.getVin() == null ? null : vo.getVin().trim(), vo.getTag(), vo.getRegular() == null ? null : vo.getRegular().trim());
//        }

        return result;
    }

    @Override
    public void saveVin(VinCodeReqVo vo) {
        // 先删除
        deleteVin(vo);
        // 再插入
        vinCodeDao.inserts(vo.getId(), vo.getVin());
    }

    @Override
    public void deleteVin(VinCodeReqVo vo) {
        vinCodeDao.deletes(vo.getId(), vo.getVin());
    }

    @Override
    public Page<VinCodeEntity> getPoolVinCode(VinCodeReqPageVo vo) {
        Page<VinCodeEntity> page = new Page<>(vo.getPageNo(), vo.getPageSize());
        LambdaQueryWrapper<VinCodeEntity> qw = Wrappers.<VinCodeEntity>lambdaQuery();

        String column = StrUtil.toUnderlineCase("vin");
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);

//        qw.select(VinCodeEntity::getVin);
//        qw.eq(VinCodeEntity::getPId, vo.getId());
//        if (!StringUtils.isEmpty(vo.getVin())) {
////            qw.like(Workshop::getName, vo.getName().trim());
//            qw.apply("vin" + " ilike {0}", "%" + vo.getVin().trim() + "%");
//        }
//
//        if (vo.getTag() != null) {
//            qw.eq(VinCodeEntity::getTId, vo.getTag());
//        }

        return vinCodeDao.getPoolVinCodeList(page, vo.getId(), vo.getVin(), vo.getTag());
    }

    @Override
    public void batchSaveVin(VinCodeReqPageVo vo) {
        Page<String> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        List<String> result = dVehicleInfoDao.getVinCodeList(vo.getKey(), vo.getVin() == null ? null : vo.getVin().trim(), vo.getTag(), vo.getRegular() == null ? null : vo.getRegular().trim());
        if (result != null && result.size() != 0) {
            VinCodeReqVo reqVo = new VinCodeReqVo();
            reqVo.setId(vo.getId());
            reqVo.setTag(vo.getTag());
            reqVo.setVin(result);
            saveVin(reqVo);
        }
    }

    @Override
    public Page<RVehicleTagEntity> getPoolVinCodePage(VinCodeReqPageVo vo) {
        Page<RVehicleTagEntity> page = new Page<>(vo.getPageNo(), vo.getPageSize());

//        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem("vin", vo.getSortAsc());
        page.addOrder(oi);

        LambdaQueryWrapper<RVehicleTagEntity> qw = Wrappers.<RVehicleTagEntity>lambdaQuery();

        if (vo.getTags() != null && vo.getTags().size() != 0) {
            qw.in(RVehicleTagEntity::getTagId, vo.getTags());
        } else {
            return new Page<RVehicleTagEntity>();
        }
        if (StringUtils.isNotBlank(vo.getVin())) {
            qw.apply("vin" + " ilike {0}", "%" + vo.getVin().trim() + "%");
        }
        qw.select(RVehicleTagEntity::getVin);
        qw.groupBy(RVehicleTagEntity::getVin);

        Page<RVehicleTagEntity> resultPage = vehicleTagDao.selectPage(page, qw);

        return resultPage;
    }
}
