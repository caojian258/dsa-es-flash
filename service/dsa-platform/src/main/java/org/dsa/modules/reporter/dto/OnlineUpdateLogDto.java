package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class OnlineUpdateLogDto implements Serializable {

    private String msg;

    private String deviceId;

    private String updateId;

    private Integer installRecordTotal = 0;
    private Integer installTotal = 0 ;

    private Integer dependenceTotal = 0 ;

    private Integer currentComponentTotal = 0 ;

    private Integer currentSoftwareTotal = 0 ;

    private Integer featureComponentTotal = 0 ;

    private Integer featureSoftwareTotal = 0 ;

    private Integer softwareRollbackTotal = 0 ;

    private Integer componentRollbackTotal = 0 ;

    public String getSummary(){
        return  String.format("deviceId %s, " +
                        "installTotal %d," +
                        "installRecordTotal %d," +
                        "dependenceTotal %d," +
                        "currentComponentTotal %d, " +
                        "currentSoftwareTotal %d, " +
                        "featureComponentTotal %d," +
                        "featureSoftwareTotal %d," +
                        "softwareRollbackTotal %d," +
                        "componentRollbackTotal %d.",
                deviceId,
                installTotal,
                installRecordTotal,
                dependenceTotal,
                currentComponentTotal,
                currentSoftwareTotal,
                featureComponentTotal,
                featureSoftwareTotal,
                softwareRollbackTotal,
                componentRollbackTotal
                );
    }
}
