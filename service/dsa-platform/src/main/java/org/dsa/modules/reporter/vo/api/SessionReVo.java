package org.dsa.modules.reporter.vo.api;

import lombok.Data;

import java.io.Serializable;

@Data
public class SessionReVo implements Serializable {

    private String id;

    private Long startTime;

    private Long endTime;

    private String userId;
}
