package org.dsa.modules.diagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.diagnostic.entity.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 故障码- 诊断数据
 *
 */
@Mapper
public interface DActionDtcDao extends BaseMapper<DActionDtcEntity> {

    void updateStatusByActionId(String vin, int status);

    List<DActionDtcEntity> selectListBySessionId(@Param("sessionId") String sessionId
            ,@Param("functionCode") String functionCode,@Param("actionCode") String actionCode);

    List<AnalysisDtcPirEntity> selectAnalysisDtcPir(AnalysisEntity analysisEntity);

    List<AnalysisDtcBarEntity> selectAnalysisDtcBar(AnalysisEntity analysisEntity);

    IPage<AnalysisDtcDataEntity> selectAnalysisDtcData(IPage<AnalysisDtcDataEntity> page, @Param("ecuNames") ArrayList<String> ecuNames
            , @Param("titleList") List<String> titleList
            , @Param("startTime") String startTime, @Param("endTime") String endTime, @Param("vin") String vin
            , @Param("vehicleTypeIds") List<String> vehicleTypeIds);

    List<DActionDtcEntity> selectListByActionId(String actionId);
    List<DActionDtcEntity> selectListByBatchActionId(@Param(("actionIds")) List<String> actionIds);
    List<DActionDtcEntity> selectReportListByActionId(String actionId);

    List<String> getEcuNameList();

    @MapKey("id")
    List<Map<String,String>> selectDtcByEcu(@Param("ecuNames") ArrayList<String> ecuNames
            , @Param("startTime") String startTime, @Param("endTime") String endTime
            , @Param("vehicleTypeIds") List<String> vehicleTypeIds);


    List<DActionDtcEntity> sessionActionDtc(@Param("sessionId") String sessionId);
}
