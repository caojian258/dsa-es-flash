package org.dsa.modules.reporter.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.reporter.dto.EcuVersionHistoryDto;
import org.dsa.modules.reporter.dto.FlashVehicleStatsDto;
import org.dsa.modules.reporter.entity.EcuVersionHistory;
import org.dsa.modules.reporter.entity.VehicleFlashDetail;
import org.dsa.modules.reporter.entity.VehicleFlashRecord;
import org.dsa.modules.reporter.vo.ChartReVo;
import org.dsa.modules.reporter.vo.client.ClientChartResVo;
import org.dsa.modules.reporter.vo.flash.FlashReqVo;
import org.dsa.modules.reporter.vo.flash.VehicleStatPageReqVo;

import java.util.List;
import java.util.Map;

public interface FlashService {
    Page<VehicleFlashRecord> getRecordPage(FlashReqVo flashReqVo);


    List<Map<String, Object>> getByVin(FlashReqVo vo);

    IPage<FlashVehicleStatsDto> selectVehicleStats(IPage<FlashVehicleStatsDto> page, VehicleStatPageReqVo vo);

    Map<String,Object> getDetail(FlashReqVo flashReqVo);

    Page<VehicleFlashDetail> getEcu(FlashReqVo flashReqVo);

    List<EcuVersionHistory> getEcuHis(FlashReqVo flashReqVo);

    Object getVehicleHis(FlashReqVo flashReqVo);

    List<String> getEcuNames();

    List<EcuVersionHistoryDto> getEcuVersionHistory(String vin, String vehVersion);

    ClientChartResVo statVehicleEcuPi(ChartReVo vo);

    ClientChartResVo statVehicleEcuBar(ChartReVo vo);
}
