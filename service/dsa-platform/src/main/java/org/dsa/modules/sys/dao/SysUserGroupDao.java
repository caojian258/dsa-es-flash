package org.dsa.modules.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.dsa.modules.sys.entity.SysUserGroupEntity;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户与组对应关系
 * 
 */
@Mapper
public interface SysUserGroupDao extends BaseMapper<SysUserGroupEntity> {
	
	/**
	 * 根据用户ID，获取角色ID列表
	 */
	List<Long> queryGroupIdList(Long userId);

}
