package org.dsa.modules.reporter.vo.client;

import lombok.Data;

import jakarta.validation.constraints.NotNull;

@Data
public class DeviceReVo {

    @NotNull(message = "设备ID不能为空")
    private String pcid;
}
