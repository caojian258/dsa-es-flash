package org.dsa.modules.reporter.job;


import lombok.extern.slf4j.Slf4j;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.reporter.config.ReportConfig;
import org.dsa.modules.reporter.constant.RedisConstant;
import org.dsa.modules.reporter.enums.FileCateEnum;
import org.dsa.modules.reporter.handler.AsMaintainedHandler;
import org.dsa.modules.reporter.handler.AsMaintainedJsonHandler;
import org.dsa.modules.reporter.service.FileLogService;
import org.dsa.modules.reporter.util.FilesUtil;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/***
 * 解析asmaintained 文件 xml/json
 */
@Slf4j
@Component
@DisallowConcurrentExecution
public class AsMaintainedJob extends QuartzJobBean {
    @Autowired
    ReportConfig reportConfig;

    @Autowired
    RedisUtils redisUtils;

    @Autowired
    AsMaintainedHandler asMaintainedHandler;
    @Autowired
    AsMaintainedJsonHandler asMaintainedJsonHandler;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.info("AsMaintained Start................");
        SimpleDateFormat sdf = new SimpleDateFormat(reportConfig.getDirFormat());

        String path = reportConfig.getAsmaintainedDir()+ File.separator+ sdf.format(new Date());
        String key = RedisConstant.FILE_KEY+":"+path+":"+System.currentTimeMillis();
        String flag = redisUtils.get(key);
        log.info("AsMaintained file: {}", path);

        if(StringUtils.isEmpty(flag)) {
            redisUtils.set(key, 1, 1200L);
            if(FilesUtil.checkExist(path)){
                 asMaintainedHandler.readFile(path);
                log.info(" read asMaintained  complete");
            }
            redisUtils.delete(key);
        }

        String uKey = RedisConstant.FILE_KEY+":"+path+":"+System.currentTimeMillis();
        String uFlag = redisUtils.get(uKey);
        if(StringUtils.isEmpty(uFlag)) {
            redisUtils.set(uKey, 1, 1200L);
            if(FilesUtil.checkExist(path)){
                asMaintainedJsonHandler.readFile(path);
                log.info(" read asMaintained  complete");
            }
            redisUtils.delete(uKey);
        }
        log.info("AsMaintained End................");
    }
}
