package org.dsa.modules.diagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

/**
 * @author weishunxin
 * @since 2023-05-29
 */
@Setter
@Getter
@TableName("d_diag_package_version")
public class DDiagPackageVersionEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long vehicleTypeId;

    private String versionNum;

    private String parentVersionNum;

    private Long odxVersionId;

    private Long otxVersionId;

    private String status;

    private String description;

    private Date createdAt;

    private Date updatedAt;

    private Long createdUserId;

    private Long updatedUserId;

}
