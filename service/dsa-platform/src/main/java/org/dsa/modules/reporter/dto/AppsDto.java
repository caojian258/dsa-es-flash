package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@Data
public class AppsDto implements Serializable {

    /***
     * 应用列表
     */
    Object applications;

    /***
     * 安装信息列表
     */
    Object installationProtocol;

    /***
     * 经销商编码
     */
    private String workshop;

    /***
     * 电脑ID
     */
    private String pcid;

    /***
     * smartstart version
     */
    private String smartstartVersion;

    /***
     * 安装失败统计
     */
    private int failedInstallationCount;

    /***
     * 安装成功统计
     */
    private int okInstallationCount;

    /***
     * 异常统计
     */
    private int errorExceptionCount;

    /***
     * 收集时间
     */
    private String timestampText;

    /***
     * 收集时间
     */
    private Date timestamp;

}
