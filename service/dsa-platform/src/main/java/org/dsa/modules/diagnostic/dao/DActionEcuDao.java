package org.dsa.modules.diagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.diagnostic.entity.DActionEcuEntity;

import java.util.List;

/**
 * 车辆诊断命令-关联ecu表表
 * 
 */
@Mapper
public interface DActionEcuDao extends BaseMapper<DActionEcuEntity> {

	/**
	 * 根据ActionId和ecuName修改信息
	*/
	 void updateByActionAndEcuName(DActionEcuEntity dActionEcuEntity);

    List<DActionEcuEntity> selectBatchActionIds(@Param("actionIds") List<String> actionIds);

    List<DActionEcuEntity> selectListByActionId(String actionId);

	List<String> queryNams();

	/**
	 * 根据号码查
	 * @param id
	 * @return

	TEventAcceptEntity getAcceptInfoByEventNo(@Param("id") String id);

	 */
}
