package org.dsa.modules.reporter.vo.client;

import lombok.Data;

@Data
public class AppVersionResVo {

    private String collectDate;

    private String pcid;

    private String workshop;

}
