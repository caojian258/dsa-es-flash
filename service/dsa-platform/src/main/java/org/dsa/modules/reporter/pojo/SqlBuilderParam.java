package org.dsa.modules.reporter.pojo;

import lombok.Data;


@Data
public class SqlBuilderParam {
	
	private String key;

	private String operator;

	private String value;
}
