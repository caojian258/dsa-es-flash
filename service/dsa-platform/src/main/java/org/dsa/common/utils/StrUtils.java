package org.dsa.common.utils;

/**
 * 判断对象是否为空或null
 */
public class StrUtils {

    /**
     * 非空字符串比较
     * @param str
     * @param string
     * @return
     */
    public static boolean containsNotBlank(String str,String string){
        //空值不判断
        if (org.apache.commons.lang.StringUtils.isBlank(str)){
            return true;
        }
        //空值不判断
        if (org.apache.commons.lang.StringUtils.isBlank(string)){
            return true;
        }
        if(org.apache.commons.lang.StringUtils.contains(str,string)){
            return true;
        }
        return false;
    }
}
