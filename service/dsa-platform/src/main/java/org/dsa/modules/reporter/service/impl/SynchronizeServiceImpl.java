package org.dsa.modules.reporter.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;
import org.dsa.common.pojo.CodeMessage;
import org.dsa.common.utils.HttpUtil;
import org.dsa.modules.diagnostic.dto.SessionEcuVersionDto;
import org.dsa.modules.diagnostic.entity.DActionDtcEntity;
import org.dsa.modules.diagnostic.entity.DActionInfoEntity;
import org.dsa.modules.diagnostic.entity.DSessionInfoEntity;
import org.dsa.modules.diagnostic.service.DiagnosticService;
import org.dsa.modules.reporter.config.OffBoardConfig;
import org.dsa.modules.reporter.dto.*;
import org.dsa.modules.reporter.entity.AppOverview;
import org.dsa.modules.reporter.entity.SessionEventLog;
import org.dsa.modules.reporter.service.AppService;
import org.dsa.modules.reporter.service.SessionEventLogService;
import org.dsa.modules.reporter.service.SynchronizedService;
import org.dsa.modules.reporter.util.FilesUtil;
import org.dsa.modules.reporter.util.TspUtil;
import org.dsa.modules.sys.entity.SysLogEntity;
import org.dsa.modules.sys.service.SysConfigService;
import org.dsa.modules.sys.service.SysLogService;
import org.dsa.modules.vehicle.entity.VehicleInfoEntity;
import org.dsa.modules.vehicle.service.VehicleService;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;


@Slf4j
@Data
@Service
public class SynchronizeServiceImpl implements SynchronizedService {

    @Resource
    private SysConfigService sysConfigService;

    @Resource
    private VehicleService vehicleService;

    @Resource
    private DiagnosticService diagnosticService;

    @Resource
    private SessionEventLogService sessionEventLogService;

    @Resource
    private OffBoardConfig offBoardConfig;

    @Resource
    private SysLogService sysLogService;

    @Resource
    private AppService appService;


    @Override
    public SyncSessionLogDto sessionLogAssemble(String vin, String sessionId) {
        SyncSessionLogDto logDto = new SyncSessionLogDto();
        WtsInfoDto wtsInfoDto = wtsInfo(vin, sessionId);


        //@todo timestamp为诊断时间 d_session_info end_time

        DSessionInfoEntity sessionInfo = diagnosticService.findSessionInfo(sessionId, vin);

        AppOverview wtsOverview =  appService.getAppOverviewByName(sessionInfo.getWorkshop(), sessionInfo.getPcid(), "WTS");
        Date timestamp = new Date();
        if(null != sessionInfo.getEndTime()){
            timestamp = sessionInfo.getEndTime();
        }

        //1. WTS
        if(null != wtsInfoDto){
            if(null != wtsOverview){
                wtsInfoDto.setSoftVersion(wtsOverview.getVersion());
            }
            wtsInfoDto.setDate(new SimpleDateFormat("yyyy-MM-dd").format(timestamp));
            logDto.setWtsInfo(wtsInfoDto);
        }

        //2. Vehicle
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        VehicleInfoDto vehicleInfoDto = new VehicleInfoDto();
        vehicleInfoDto.setVin(vin);
        vehicleInfoDto.setTimestamp(sdf.format(timestamp));
        vehicleInfoDto.setBatteryType("");
        vehicleInfoDto.setMileage("");
        vehicleInfoDto.setBatteryVoltage("");
        vehicleInfoDto.setPowerType("");
        vehicleInfoDto.setTransmissionType("");
        try {
            VehicleInfoEntity dVehicleInfoEntity = vehicleService.getVehicleInfoByVin(vin);
            if(null != dVehicleInfoEntity){
                vehicleInfoDto.setProject(dVehicleInfoEntity.getMaker());
                vehicleInfoDto.setSalesName(dVehicleInfoEntity.getModel());
            }
        }catch (Exception ex){
            log.error("SynchronizeServiceImpl sessionLogAssemble:", ex.getMessage());
        }

        logDto.setVehicleInfo(vehicleInfoDto);

        //3. BasicDiagnostic
        BasicDiagnosticDto basicDiagDto = new BasicDiagnosticDto();

        //生成Identification 需要配置TI
        Map<String,Map<String, String>> vehicleIdentificationMap = sessionVehicleIdentification(sessionId);

        basicDiagDto.setVehicleIdentification(vehicleIdentificationMap);

        //生成DTCS
        Map<String, Map<String, String>>  dtcs = sessionDtc(sessionId);

        basicDiagDto.setDtcs(dtcs);

        logDto.setBasicDiagnostic(basicDiagDto);

        //4. 生成CTS
        ExecutedCtsDto executedCts = new ExecutedCtsDto();
        List<SessionCts> list = sessionCTS(sessionId);
        executedCts.setCts(list);

        logDto.setExecutedCts(executedCts);

        return logDto;
    }

    @Override
    public Boolean generateXML(SyncSessionLogDto dto, String path, String sessionId) {

        Document document = DocumentHelper.createDocument();
        Element root = document.addElement( "AsMaintenanceLog" );

        root.addAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        root.addAttribute("xsi:schemaLocation", "http://www.w3school.com.cn/DSA_AS_DataReport.xsd");

        WtsInfoDto wtsInfoDto = dto.getWtsInfo();

        Element WTSInfo = root.addElement( "WTSInfo" );
        if(null != wtsInfoDto){
            WTSInfo.addAttribute( "SoftVersion", wtsInfoDto.getSoftVersion() )
                        .addAttribute( "Account", wtsInfoDto.getAccount() )
                        .addAttribute( "Profile", wtsInfoDto.getProfile() )
                        .addAttribute( "Expiration", wtsInfoDto.getExpiration() )
                        .addAttribute( "Language", wtsInfoDto.getLanguage() )
                        .addAttribute( "Date", wtsInfoDto.getDate());
        }

        VehicleInfoDto vehicleInfoDto = dto.getVehicleInfo();

        Element VehicleInfo = root.addElement( "VehicleInfo" );
        if(null != vehicleInfoDto){
            VehicleInfo.addAttribute( "VIN", vehicleInfoDto.getVin() );
            Element Project = VehicleInfo.addElement("Project");
            Project.addAttribute("value", vehicleInfoDto.getProject());

            Element SalesName = VehicleInfo.addElement("SalesName");
            SalesName.addAttribute("value", vehicleInfoDto.getSalesName());

            Element Mileage = VehicleInfo.addElement("Mileage");
            Mileage.addAttribute("value", vehicleInfoDto.getMileage());

            Element PowerType = VehicleInfo.addElement("PowerType");
            PowerType.addAttribute("value", vehicleInfoDto.getPowerType());

            Element TransmissionType = VehicleInfo.addElement("TransmissionType");
            TransmissionType.addAttribute("value", vehicleInfoDto.getTransmissionType());

            Element BatteryVoltage = VehicleInfo.addElement("BatteryVoltage");
            BatteryVoltage.addAttribute("value", vehicleInfoDto.getBatteryVoltage());

            Element Timestamp = VehicleInfo.addElement("Timestamp");
            Timestamp.addAttribute("value", vehicleInfoDto.getTimestamp());
        }

        Element BasicDiagnostic = root.addElement( "BasicDiagnostic" );

        Element VehicleIdentification = BasicDiagnostic.addElement("VehicleIdentification");

        Element Identification = VehicleIdentification.addElement("Identification");
        Map<String, Map<String, String>> vehicleIdentificationDto =  dto.getBasicDiagnostic().getVehicleIdentification();
        if(null != vehicleIdentificationDto){
            Iterator<Map.Entry<String, Map<String, String>>> entries = vehicleIdentificationDto.entrySet().iterator();
            while (entries.hasNext()) {
                Map.Entry<String, Map<String, String>> entry = entries.next();
                Map<String, String> value = entry.getValue();
                Element items = Identification.addElement("Items");
                for (String k: value.keySet()){
                    items.addAttribute(k, value.get(k));
                }
            }
        }

        Element DTCs = BasicDiagnostic.addElement("DTCs");
        Map<String, Map<String, String>> dtcMap = dto.getBasicDiagnostic().getDtcs();
        if(null != dtcMap){
            Iterator<Map.Entry<String, Map<String, String>>> dtcList = dtcMap.entrySet().iterator();
            while (dtcList.hasNext()) {
                Map.Entry<String, Map<String, String>> entry = dtcList.next();
                Map<String, String> value = entry.getValue();
                Element DTC = DTCs.addElement("DTC");
                for (String k: value.keySet()){
                    DTC.addAttribute(k, value.get(k));
                }
            }
        }

        ExecutedCtsDto executedCtsDto =  dto.getExecutedCts();
        Element EXECUTED_CTS = root.addElement("EXECUTED_CTS");

        if(null != executedCtsDto){
            List<SessionCts> ctsList = executedCtsDto.getCts();
            if(null != ctsList){
                for (SessionCts cts: ctsList) {
                    Element CTS = EXECUTED_CTS.addElement("CTS");
                    CTS.addAttribute("CTSName",cts.getCtsName());
                    CTS.addAttribute("StartTimestamp",cts.getStartTimestamp());
                    CTS.addAttribute("EndTimestamp",cts.getEndTimestamp());
                    CTS.addAttribute("ReturnValue",cts.getReturnValue());
                    CTS.addAttribute("ReturnMessage",cts.getReturnMessage());
                    CTS.addAttribute("FailureCode", cts.getFailureCode().toString());
                }
            }
        }

        try {
            File file = new File(path);
            if(!file.exists()){
                FilesUtil.mkdir(file);
            }
            XMLWriter writer = new XMLWriter(new FileWriter( path+ File.separator+vehicleInfoDto.getVin()+"_"+sessionId+".xml" ));
            writer.write( document );
            writer.close();
        }catch (Exception ex){
            log.error("SynchronizeServiceImpl generateXML:{}", ex.getMessage());
            return false;
        }

        return true;
    }

    @Override
    public Boolean syncTsp(String path, Map<String,String> params) {
        String domain = offBoardConfig.getSyncDomain();
        String url =  domain + offBoardConfig.getUploadUrl();
        Boolean flag = false;
        Long timestamp = System.currentTimeMillis();

        try {
            File postFile = new File(path);
            HttpEntity reqEntity = TspUtil.buildMultipartForm(postFile, "log", params);
            String ak = offBoardConfig.getAccessKey();
            String sk = offBoardConfig.getSecretKey();

            ByteArrayOutputStream out = new ByteArrayOutputStream((int) reqEntity.getContentLength());
            reqEntity.writeTo(out);
            byte[] bodyBytes = out.toByteArray();

            String body = TspUtil.aesEncrypt(bodyBytes, sk);
            String sin = TspUtil.sign(body+timestamp.toString(), sk);

            Map<String, String> headers = new HashMap<>();
            headers.put("Content-Type", reqEntity.getContentType().getValue());
            headers.put("x-niutron-ak", ak);
            headers.put("x-niutron-sign", sin);
            headers.put("timestamp", timestamp.toString());

            CodeMessage result =  HttpUtil.post(url, body, headers);

            log.info("syncTsp result : {}", result.getMessage());

            Long endTime = System.currentTimeMillis();
            params.put("file", postFile.getName());
            params.put("time", String.valueOf(endTime-timestamp));
            params.put("ip", domain);
            logSyncResult(result, params);

            if(result.getCode().equals("200")){
                flag = true;
            }
        }catch (Exception ex){
            log.error("syncTsp: {}", JSONObject.toJSONString(ex));
            CodeMessage result = new CodeMessage();
            Long endTime = System.currentTimeMillis();
            params.put("ip", domain);
            params.put("file", path);
            params.put("time", String.valueOf(endTime-timestamp));
            result.setCode("500");
            result.setMessage(ex.getMessage());
            logSyncResult(result, params);
        }
        return flag;
    }

    @Override
    public Map<String, Map<String, String>> sessionVehicleIdentification(String sessionId) {

        List<Map<String, String>> vehicleIdentification = new ArrayList<>();
        Map<String,Map<String, String>> vehicleIdentificationMap = new HashMap<>();

        //生成ECU Version
        List<SessionEcuVersionDto> ecuVersionDtos =  diagnosticService.selectSessionEcuVersion(sessionId);

        if(CollectionUtil.isNotEmpty(ecuVersionDtos)){

            for (SessionEcuVersionDto dto: ecuVersionDtos) {
                Map<String, String> identMap = null;
                identMap = vehicleIdentificationMap.get(dto.getEcuName());
                if(null == identMap){
                    identMap = new HashMap<>();
                }
                identMap.put("Name", dto.getEcuName());
                identMap.put(StrUtil.cleanBlank(dto.getName()), dto.getValue());
                vehicleIdentificationMap.put(dto.getEcuName(), identMap);
            }
        }

        return vehicleIdentificationMap;
    }

    @Override
    public Map<String, Map<String, String>> sessionDtc(String sessionId) {
        List<DActionDtcEntity> dActionDtcEntities = diagnosticService.sessionActionDtc(sessionId);
        Map<String, Map<String, String>> result = new HashMap<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        if(CollectionUtil.isNotEmpty(dActionDtcEntities)){
            for (DActionDtcEntity entity: dActionDtcEntities) {
                Map<String, String> identMap =  new HashMap<>();
                identMap.put("DTCCode", entity.getDtcCode());
                identMap.put("SourceName", entity.getEcuName());
                identMap.put("DTCStatusCode", entity.getStatus());
                identMap.put("DTCDefinition", entity.getDescription());
                identMap.put("DTCTime", sdf.format(entity.getCreateTime()));
                result.put(entity.getDtcCode(), identMap);
            }
        }
        return result;
    }

    @Override
    public List<SessionCts> sessionCTS(String sessionId) {
        List<SessionCts> result = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        List<DActionInfoEntity>  entities = diagnosticService.sessionActionInfo(sessionId);
        for (DActionInfoEntity entity: entities) {
            SessionCts cts = new SessionCts();
            if(StrUtil.isNotBlank(entity.getTitleCn())){
                cts.setCtsName(entity.getTitleCn());
            }else{
                cts.setCtsName(entity.getFunctionCode()+ (StrUtil.isNotBlank(entity.getActionCode())?"_"+entity.getActionCode():"") );
            }
            cts.setStartTimestamp(sdf.format(entity.getStartTime()));
            cts.setEndTimestamp(sdf.format(entity.getEndTime()));
            cts.setReturnValue(entity.getDescription());
            cts.setReturnMessage(entity.getMessageCn());
            cts.setFailureCode( entity.getResults().equals("1") ?0:1);

            result.add(cts);
        }
        return result;
    }

    @Override
    public WtsInfoDto wtsInfo(String vin, String sessionId) {
        SessionEventLog eventLog =  sessionEventLogService.selectOneBySessionId(vin, sessionId);
        if(null == eventLog){
            return null;
        }

        WtsInfoDto info = new WtsInfoDto();
        info.setProfile("");
        info.setExpiration("");
        info.setSoftVersion(eventLog.getUsedVciIdentification());
        info.setDate(eventLog.getTimestampText().substring(0, 10));
        info.setAccount(eventLog.getUserName());
        info.setLanguage(eventLog.getLanguage());
        return info;
    }


    private void logSyncResult(CodeMessage res, Map<String,String> params){

        SysLogEntity logEntity = new SysLogEntity();
        logEntity.setCreateDate(new Date());
        logEntity.setResult(res.getMessage());
        logEntity.setMethod(offBoardConfig.getUploadUrl());
        logEntity.setDisplayUserName("System");
        logEntity.setOperation(offBoardConfig.getUploadUrl());
        logEntity.setParams(JSONObject.toJSONString(params));
        logEntity.setTime(Long.valueOf(params.get("time")));
        logEntity.setIp(params.get("ip"));
        sysLogService.save(logEntity);
    }
}
