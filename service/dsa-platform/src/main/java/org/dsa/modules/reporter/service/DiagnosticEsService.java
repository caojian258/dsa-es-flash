package org.dsa.modules.reporter.service;

import org.dsa.modules.reporter.document.*;
import org.dsa.modules.reporter.dto.VehicleReportDto;
import org.dsa.modules.reporter.entity.CtsDaily;
import org.dsa.modules.reporter.entity.DtcDaily;
import org.dsa.modules.reporter.vo.VehicleReportVo;
import org.dsa.modules.reporter.vo.diag.DiagnosticPageReVo;
import org.dsa.modules.reporter.vo.diag.DtcChartReVo;
import org.dsa.modules.reporter.vo.diag.DtcPageReVo;
import org.dsa.modules.reporter.vo.diag.SessionPageReVo;

import java.util.List;
import java.util.Map;

public interface DiagnosticEsService {

    /***
     * 诊断会话搜索
     * @param vo
     * @return
     */
    public Map<String, Object> sessionPage(SessionPageReVo vo);

    /***
     * 根据诊断会话ID获取诊断会话列表
     * @param infoIds
     * @return
     */
    public List<SessionDoc> sessionListById(List<String> infoIds, List<String> dates);

    /***
     * 诊断功能搜索
     * @param vo
     * @return
     */
    public Map<String, Object> diagnosticPage(DiagnosticPageReVo vo);

    /***
     * 诊断功能按天聚合
     * @param vo
     * @return
     */
    public List<Map<String, Object>> diagnosticDaily(DiagnosticPageReVo vo);


    /***
     * 诊断功能饼图
     * @param vo
     * @return
     */
    public List<Map<String, Object>> diagnosticPi(DiagnosticPageReVo vo);

    /***
     * 诊断功能柱状图
     * @param vo
     * @return
     */
    public List<Map<String, Object>> diagnosticMonthBar(DiagnosticPageReVo vo);


    /***
     * 诊断操作获取诊断会话
     * @param vo
     * @return
     */
    public Map<String, Object> diagnosticSessionPage(DiagnosticPageReVo vo);
    /***
     * 控制器故障码列表
     * @param vo
     * @return
     */
    public Map<String, Object> dtcPage(DtcPageReVo vo);

    /***
     * 根据DTC拿诊断会话信息
     * @param vo
     * @return
     */
    public Map<String, Object> dtcSessionPage(DtcPageReVo vo);

    /***
     * 控制器故障码PI图
     * @param vo
     * @return
     */
    public List<Map<String, Object>> dtcPi(DtcChartReVo vo);


    /***
     * 控制器故障码柱形图
     * @param vo
     * @return
     */
    public List<Map<String, Object>> dtcMonthBar(DtcChartReVo vo);

    /***
     * 控制器故障码按天统计
     * @param vo
     * @return
     */
    public List<Map<String, Object>> dtcDailyBar(DtcChartReVo vo);

    /***
     * 根据sessionInfoId 获取
     * @param vo
     * @return
     */
    public VehicleReportDto vehicleReport(VehicleReportVo vo);

    /***
     * 获取诊断会话信息
     * @param vo
     * @return
     */
    public SessionDoc vehicleReportSession(VehicleReportVo vo);

    /***
     * 获取Dtc文档
     * @param sessionInfoId
     * @param recordId
     * @param date
     * @return
     */
    public List<DtcDoc> queryDtcDocBySessionAndRecordId(String sessionInfoId, Integer recordId, String date);

    /***
     * 获取DID文档
     * @param sessionInfoId
     * @param recordId
     * @param date
     * @return
     */
    public List<DidDoc> queryDidDocBySessionAndRecordId(String sessionInfoId, Integer recordId, String date);


    /***
     * 获取VehicleReportDoc文档
     * @param sessionInfoId
     * @return
     */
    public VehicleReportDoc queryVehicleReportDoc(String sessionInfoId, String date);


    /***
     * 查询诊断功能VHSS ECU状态
     * @param sessionInfoId
     * @param date
     * @return
     */
    public List<DiagnosticEcuDoc> queryDiagnosticVhss(String sessionInfoId, String date);

    /***
     * 根据vin提示10条记录
     * @param vin
     * @param date
     * @return
     */
    public List<String> vinRecommend(String vin, String date, Integer size);

    /***
     * 根据ecu提示10条
     * @param ecu
     * @param date
     * @return
     */
    public List<String> ecuRecommend(String ecu, String date, Integer size);

    /***
     * 据dtc提示10条
     * @param dtc
     * @param date
     * @param size
     * @return
     */
    public List<String> dtcRecommend(String dtc, String date, Integer size);

    /***
     * 为诊断报告查询DTC
     * @param sessionInfoId
     * @param date
     * @param recordFlags 类型1,2,3
     * @return
     */
    public List<DtcDoc> sessionDtcForVehicleReport(String sessionInfoId, String date, List<Integer> recordFlags);

    /***
     * 为诊断报告查询DID
     * @param sessionInfoId
     * @param date
     * @param recordFlags
     * @return
     */
    public List<DidDoc> sessionDidForVehicleReport(String sessionInfoId, String date, List<Integer> recordFlags);


    /***
     * 诊断报告 ECU在线情况
     * @param sessionInfoId
     * @param date
     * @return
     */
    public List<DiagnosticEcuDoc> sessionEcuForVehicleReport(String sessionInfoId, String date);


    /***
     * 诊断报告 诊断操作
     * @param sessionInfoId
     * @param date
     * @return
     */
    public List<DiagnosticDoc> sessionDiagnosticVehicleReport(String sessionInfoId, String date);

    /***
     * 诊断报告 DTC冻结帧
     * @param sessionInfoId
     * @param date
     * @param ecuName
     * @param dtcCode
     * @return
     */
    public List<DtcSnapshotDoc> sessionDtcSnapshotVehicleReport(String sessionInfoId, String date, String ecuName, String dtcCode);


    /***
     * 诊断报告 DTC冻结帧
     * @param sessionInfoId
     * @param date
     * @return
     */
    public List<DtcSnapshotDoc> sessionDtcSnapshotVehicleReport(String sessionInfoId, String date);


    public List<DtcExtendedDoc> sessionDtcExtendedVehicleReport(String sessionInfoId, String date);

    public List<String> sessionEcuList();

    /***
     * 删除 诊断会话相关文档
     * @param sessionInfoId
     * @param date
     */
    public void deleteSessionDoc(String sessionInfoId, String date);

    public List<DtcDaily> dtcDailyByVinDay(String vin, String date);


    public List<CtsDaily> ctsDailyByVinDay(String vin, String date);

    public SessionDoc querySessionBySessionId(String sessionId, String date);

    public List<Map<String, Object>> vinDailyAggregations(String date);
}
