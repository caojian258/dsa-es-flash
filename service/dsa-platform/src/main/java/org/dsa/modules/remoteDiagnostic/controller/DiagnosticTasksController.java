package org.dsa.modules.remoteDiagnostic.controller;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.dsa.common.annotation.SysLog;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.R;
import org.dsa.common.validator.ValidatorUtils;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.common.validator.group.UpdateGroup;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticConditionEntity;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticFileEntity;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticFunctionEntity;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskEntity;
import org.dsa.modules.remoteDiagnostic.service.*;
import org.dsa.modules.remoteDiagnostic.vo.*;
import org.dsa.modules.remoteDiagnostic.vo.Task.DiagnosticTaskOverviewPageReqVo;
import org.dsa.modules.remoteDiagnostic.vo.Task.TaskConditionReqVo;
import org.dsa.modules.reporter.util.LocalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

@Slf4j
@RestController
@RequestMapping("/diagnosticTask")
public class DiagnosticTasksController {

    @Autowired
    DiagnosticFunctionService functionService;
    @Autowired
    DiagnosticConditionService conditionService;
    @Autowired
    DiagnosticFileService fileService;
    @Autowired
    DiagnosticTaskService taskService;
    @Autowired
    DiagnosticConditionCopyService copyService;


    @RequestMapping("/checkName")
    public R checkName(@RequestBody CheckNameReqVo vo) {

        ValidatorUtils.validateEntity(513, vo, AddGroup.class);

        return R.ok().put("flag", taskService.check(vo));
    }

    @RequestMapping("/checkStatus")
    public R checkStatus(@RequestBody CheckStatusReqVo vo) {

        ValidatorUtils.validateEntity(513, vo, AddGroup.class);

        return R.ok().put("flag", taskService.checkStatus(vo));
    }

    // ----诊断功能管理-----------------------------------------------
    @RequestMapping("/getFunPage")
    public R getFunPage(@RequestBody DiagnosticFunctionPageVo vo) {
        return R.ok().put("data", functionService.selectPage(vo));
    }

    @GetMapping("/getFunInfo/{id}")
    public R getFunInfo(@PathVariable("id") Long id) {
        if (id == null) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        return R.ok().put("data", functionService.getInfo(id));
    }

    @GetMapping("/getFunGroups")
    public R getFunGroups() {
        return R.ok().put("data", functionService.getFunGroups());
    }

    @GetMapping("/getFunList")
    public R getFunList() {
        return R.ok().put("data", functionService.getFunctions());
    }

    @SysLog("新增诊断功能")
    @PostMapping("/saveFun")
//    @RequiresPermissions("sys:user:save")
    public R saveFun(@RequestBody DiagnosticFunctionEntity fun) {

        ValidatorUtils.validateEntity(513, fun, AddGroup.class);

        functionService.saveFun(fun);
        return R.ok();
    }

    @SysLog("修改诊断功能")
    @PostMapping("/updateFun")
//    @RequiresPermissions("sys:user:update")
    public R updateFun(@RequestBody DiagnosticFunctionEntity fun) {

        ValidatorUtils.validateEntity(513, fun, UpdateGroup.class);

        functionService.updateFun(fun);
        return R.ok();
    }

    // ----诊断条件管理-----------------------------------------------
    @RequestMapping("/getCondPage")
    public R getCondPage(@RequestBody DiagnosticConditionPageVo vo) {
        return R.ok().put("data", conditionService.selectPage(vo));
    }

    @GetMapping("/getCondInfo/{id}")
    public R getCondInfo(@PathVariable("id") Long id) {
        if (id == null) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        return R.ok().put("data", conditionService.getInfo(id));
    }

    @PostMapping("/getConditions")
    public R getConditions(@RequestBody TaskConditionReqVo vo) {
        if (vo.getIds() == null || vo.getIds().size() == 0) {
            return R.ok();
        }
        return R.ok().put("data", conditionService.getConditions(vo.getIds(), vo.getTaskId()));
    }

//    @PostMapping("/setFileDefault")
//    public R setFileDefault(@RequestBody Long id) {
//        if (id == null) {
//            return R.error(Constant.Msg.CHECK_INPUT);
//        }
//        fileService.setFileDefault(id);
//        return R.ok();
//    }

    @RequestMapping("/getCondList")
//    @RequiresPermissions("sys:user:save")
    public R getCondList() {
        return R.ok().put("data", conditionService.getConditions());
    }

    @SysLog("新增诊断条件")
    @PostMapping("/saveCond")
//    @RequiresPermissions("sys:user:save")
    public R saveCond(@RequestBody DiagnosticConditionEntity cond) {

        ValidatorUtils.validateEntity(513, cond, AddGroup.class);

        conditionService.saveCond(cond);
        return R.ok();
    }

    @SysLog("修改诊断条件")
    @PostMapping("/updateCond")
//    @RequiresPermissions("sys:user:update")
    public R updateCond(@RequestBody DiagnosticConditionEntity cond) {

        ValidatorUtils.validateEntity(513, cond, UpdateGroup.class);
//        try {
//            conditionService.updateCond(cond);
//            return R.ok();
//        }catch (Exception e){
//            return R.error();
//        }
        conditionService.updateCond(cond);
        return R.ok();
    }

    // ---诊断文件管理------------------------------------------------
    @RequestMapping("/getFilePage")
    public R getFilePage(@RequestBody PublicPageReqVo vo) {
        return R.ok().put("data", fileService.selectPage(vo));
    }

    @GetMapping("/getFileInfo/{id}")
    public R getFileInfo(@PathVariable("id") Long id) {
        if (id == null) {
            return R.error();
        }
        return R.ok().put("data", fileService.getInfo(id));
    }

    @GetMapping("/getFileList")
    public R getFileList() {
        return R.ok().put("data", fileService.getFiles());
    }

    @SysLog("新增诊断文件")
    @PostMapping("/saveFile")
//    @RequiresPermissions("sys:user:save")
    public R saveFile(@RequestBody DiagnosticFileEntity file) {

        ValidatorUtils.validateEntity(513, file, AddGroup.class);

        try {
            fileService.saveFile(file);
        } catch (IOException e) {
            return R.error(Constant.Msg.MESSAGE_696);
        }

        return R.ok();
    }

    @SysLog("修改诊断文件")
    @PostMapping("/updateFile")
//    @RequiresPermissions("sys:user:update")
    public R updateFile(@RequestBody DiagnosticFileEntity file) {

        ValidatorUtils.validateEntity(513, file, UpdateGroup.class);

        try {
            fileService.updateFile(file);
        } catch (IOException e) {
            return R.error(Constant.Msg.MESSAGE_696);
        }
        return R.ok();
    }

    /**
     * otx脚本上传
     */
    @PostMapping("/diagnosticFileUpload")
    public R diagnosticFileUpload(@RequestParam("file") MultipartFile mFile) {
        try {
            //MultipartFile 转化为file easyexcel接受file类型的文件
            File file = new File(Objects.requireNonNull(mFile.getOriginalFilename()));
//            if (!file.getName().endsWith(".xlsx")) {
//                return R.error("请导入excel类型的文件");
//            }
//
            FileUtils.copyInputStreamToFile(mFile.getInputStream(), file);

            String path = fileService.upload(file);
            return R.ok().put("name", file.getName()).put("path", path);
        } catch (Exception e) {
            log.error("otx导入出错：", e);
            return R.error(606, "otx导入出错：" + e);
        }
    }

    /**
     * otx脚本下载
     */
    @RequestMapping("/diagnosticFileDownload/{id}")
    public void diagnosticFileDownload(@PathVariable("id") Long id, HttpServletRequest req, HttpServletResponse response) {
        DiagnosticFileEntity file = fileService.getInfo(id);

        if (null == file) {
            return;
        }
        File downloadFile = new File(file.getFilePath());

        InputStream ips = null;
        OutputStream ops = null;
        try {
            ips = new FileInputStream(downloadFile);
            ops = response.getOutputStream();
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=utf-8'zh_cn'" + URLEncoder.encode(file.getFileName(), "UTF-8"));
            IOUtils.copy(ips, ops);
            ips.close();
            ops.flush();

        } catch (IOException e) {

        } finally {
            try {
                if (ips != null) {
                    ips.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (ops != null) {
                    ops.flush();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    // ---诊断任务管理------------------------------------------------
    @RequestMapping("/getTaskPage")
    public R getTaskPage(@RequestBody PublicPageReqVo vo) {
        return R.ok().put("data", taskService.selectPage(vo));
    }

    @GetMapping("/getTaskInfo/{id}")
    public R getTaskInfo(@PathVariable("id") Long id) {
        if (id == null) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        return R.ok().put("data", taskService.getInfo(id));
    }

    @SysLog("新增诊断任务")
    @PostMapping("/saveTask")
//    @RequiresPermissions("sys:user:save")
    public R saveTask(@RequestBody DiagnosticTaskEntity task) {

        ValidatorUtils.validateEntity(513, task, AddGroup.class);

        taskService.saveTask(task);
        return R.ok();
    }

    @SysLog("修改诊断任务")
    @PostMapping("/updateTask")
//    @RequiresPermissions("sys:user:update")
    public R updateTask(@RequestBody DiagnosticTaskEntity task) {

        ValidatorUtils.validateEntity(513, task, UpdateGroup.class);

        taskService.updateTask(task);
        return R.ok();
    }

    @RequestMapping("/getTaskList")
    public R getTaskList() {
        return R.ok().put("data", taskService.getTasks());
    }

    @RequestMapping("/getCondByTask/{id}")
//    @RequiresPermissions("sys:user:update")
    public R getCondByTask(@PathVariable("id") Long id) {
        if (id == null) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        return R.ok().put(("data"), copyService.getConditions(id));
    }

    @RequestMapping("/getFunctionList")
//    @RequiresPermissions("sys:user:update")
    public R getCondByTask(@RequestBody TaskConditionReqVo vo) {
        if (vo.getIds() == null || vo.getIds().size() == 0) {
            return R.ok();
        }
        return R.ok().put(("data"), taskService.getFunctionByTest(vo));
    }
}

