package org.dsa.modules.reporter.upgrade;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class ClientUpgradeDto implements Serializable {
    protected String username;
    protected String userDisplayName;
    protected Long workshopId;
    protected String workshopName;
    protected String deviceId;
    private String updateId;
    protected Date startTime;
    protected Date endTime;
    protected Integer installAction;
    protected Integer updateResult;
    protected Integer updateTarget;
    protected String tag;
    protected Date ctime;

}
