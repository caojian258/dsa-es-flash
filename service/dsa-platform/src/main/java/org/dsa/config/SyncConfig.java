package org.dsa.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "sync")
public class SyncConfig {

    //主工作目录
    @Value("${sync.baseDir}")
    private String baseDir;

    @Value("${sync.dirFormat}")
    private String dirFormat;

    // workshop
    @Value("${sync.workshop.workshopFile}")
    private String workshopFileName;

    @Value("${sync.workshop.workshopUserFile}")
    private String workshopUserFileName;

    @Value("${sync.workshop.upload}")
    private String workshopUploadDir;

    @Value("${sync.workshop.work}")
    private String workshopWorkDir;

    @Value("${sync.workshop.archive}")
    private String workshopArchiveDir;

    @Value("${sync.workshop.format}")
    private String workshopFormat;

    //session log
    @Value("${sync.session.upload}")
    private String sessionUploadDir;

    @Value("${sync.session.work}")
    private String sessionWorkDir;

    @Value("${sync.session.archive}")
    private String sessionArchiveDir;

    @Value("${sync.session.fault}")
    private String sessionFaultDir;

    @Value("${sync.session.format}")
    private String sessionFormat;

    @Value("${sync.onlineUpdate.upload}")
    private String onlineUpdateUploadDir;

    @Value("${sync.onlineUpdate.work}")
    private String onlineUpdateWorkDir;

    @Value("${sync.onlineUpdate.archive}")
    private String onlineUpdateArchiveDir;

    @Value("${sync.onlineUpdate.fault}")
    private String onlineUpdateFaultDir;

    @Value("${sync.onlineUpdate.format}")
    private String onlineUpdateFormat;

    @Value("${sync.tsp.archive}")
    private String tspArchiveDir;

    @Value("${sync.tsp.upload}")
    private String tspUploadDir;

}
