//package org.dsa.modules.remoteDiagnostic.dao;
//
//import com.baomidou.mybatisplus.core.mapper.BaseMapper;
//import org.apache.ibatis.annotations.*;
//import org.dsa.modules.remoteDiagnostic.entity.RemoteEcuEcuVersionEntity;
//
//import java.util.List;
//
//@Mapper
//public interface RemoteEcuEcuVersionDao extends BaseMapper<RemoteEcuEcuVersionEntity> {
//
//
//    @Select("select * from r_ecu_ecu_version i where i.ecu_id = #{ecuId} order by i.id")
//    @Results({
//            @Result(property = "versions", column = "init_id",
//                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.RemoteEcuVersionDao.getList"))
//    })
//    public List<RemoteEcuEcuVersionEntity> getList(@Param("ecuId") Long id);
//
//    @Select("select * from r_ecu_ecu_version i where i.ecu_id = #{ecuId} order by i.id")
//    @Results({
//            @Result(property = "versions", column = "init_id",
//                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.RemoteEcuVersionDao.getVersionList"))
//    })
//    public List<RemoteEcuEcuVersionEntity> getAllList(@Param("ecuId") Long id);
//
//
//    // 2023/3/16 暂未有多个ecu版本分类，目前只会有一个
//    @Select("select *,#{versionId} as vId from r_ecu_ecu_version i where i.ecu_id = #{ecuId} order by i.id limit 1")
//    @Results({
//            @Result(property = "versions", column = "{initId=init_id,versionId=vId}",
//                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.RemoteEcuVersionDao.getListById"))
//    })
//    public List<RemoteEcuEcuVersionEntity> getListByEcu(@Param("ecuId") Long id, @Param("versionId") Long versionId);
//
//
//}
