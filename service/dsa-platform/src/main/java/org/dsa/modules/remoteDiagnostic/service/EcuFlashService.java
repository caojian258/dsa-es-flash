package org.dsa.modules.remoteDiagnostic.service;


import org.dsa.modules.remoteDiagnostic.dto.flash.EcuDIDDto;
import org.dsa.modules.remoteDiagnostic.dto.flash.EcuVersionDto;
import org.dsa.modules.remoteDiagnostic.dto.flash.EcuVersionFileDto;
import org.dsa.modules.remoteDiagnostic.dto.flash.VersionDto;
import org.dsa.modules.remoteDiagnostic.entity.RemoteEcuIdentificationInfoEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleVersionEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVersionEcuRelyEntity;

import java.util.List;
import java.util.Map;

public interface EcuFlashService {

    /***
     * 校验ecu版本 校验是否存在白件
     */
    public Integer checkEcuVersionList(List<EcuDIDDto> ecuList);

    /***
     *
     */
    public List<EcuVersionDto> buildEcuVersionListByVehicleVersion(Long versionId);

    /***
     * 当所有版本都有依赖的时候
     * @param versionList
     * @return
     */
    public List<EcuVersionDto> findEcuVersionByDependencyEcuVersionId(List<Long> versionList);

    /***
     * 获取 ECU版本依赖关系
     * @param versionList
     * @return
     */
    public List<RemoteVersionEcuRelyEntity> findEcuVersionDependency(List<Long> versionList);

    /***
     * 根据ECU DID 列表判断是否是换件
     * @param versionMap ecu版本集合
     * @return
     */
    public Boolean checkExchange(Map<String, VersionDto> versionMap, List<EcuDIDDto> ecuList);

    /***
     * 返回车辆软件版本号
     * @param ecuList
     * @return
     */
    public String searchVehicleVersion(List<EcuDIDDto> ecuList);

    /***
     * 返回ecu软件版本号
     * @param ecuList
     * @return
     */
    public Map<String, VersionDto> searchEcuVersion(List<EcuDIDDto> ecuList);

    /***
     * 查找待升级的车辆版本
     * @param vehicleTypeId
     * @param versionId
     * @return
     */
    public RemoteVehicleVersionEntity findVehicleTargetVersion(Long vehicleTypeId, Long versionId);

    /***
     * 根据当前车型的整车版本找到大于他的整车版本列表
     * @param vehicleTypeId
     * @param versionId
     * @return
     */
    public List<RemoteVehicleVersionEntity> findVehicleTargetVersions(Long vehicleTypeId, Long versionId);

    /***
     * 根据车辆池获取当前车辆需要升级的版本
     * @param versions
     * @param filters
     * @return
     */
    public RemoteVehicleVersionEntity findVehicleTargetVersionByPool(List<RemoteVehicleVersionEntity> versions,  Map<String, Object> filters);
    /***
     * 根据车辆版本找到ECU版本
     * @param version
     * @return
     */
    public List<EcuDIDDto> ecuUpdateData(RemoteVehicleVersionEntity version, String vin);

    /***
     * 根据车型和整车版本查找已经发布的整车版本信息
     * @param vehicleTypeId
     * @param version
     * @return
     */
    public RemoteVehicleVersionEntity findVehicleVersion(Long vehicleTypeId, String version);


    /***
     * 根据车型和版本检查是否存在最新待版本
     * @param vehicleTypeId
     * @param version
     * @return
     */
    public Integer checkTargetVersion(Long vehicleTypeId, String version);


    /***
     * 检查是否要版本对齐 0不需要更新和对齐 1对齐 2更新
     * @param version
     * @param ecuList
     * @return
     */
    public Integer checkAlign(RemoteVehicleVersionEntity version, List<EcuDIDDto> ecuList);

    /***
     * 根据整车版本ID 获取ECU列表
     * @param versionId
     * @return
     */
    public List<RemoteVehicleEcuEntity> findVehicleEcuByVehicleVersion(Long versionId);

    /***
     * 根据整车版本ID 获取ECU版本列表
     * @param versionId
     * @return
     */
    public List<EcuVersionDto> findVehicleEcuVersionByVehicleVersion(Long versionId);

    /***
     * 根据ECU版本ID获取 DID
     * @param ecuVersionId
     * @param group
     * @return
     */
    public Map<String, String> findDidMapByEcuVersionId(Long ecuVersionId, String group);


    /***
     * 根据ECU版本ID获取 DID
     * @param ecuId
     * @param group
     * @return
     */
    public Map<String, RemoteEcuIdentificationInfoEntity> findDidByEcuVersionId(Long ecuId, String group);

    /***
     * 获取需要对齐的ECU DID
     * @param version
     * @param ecuList
     * @return
     */
    public List<EcuDIDDto> ecuAlignData(RemoteVehicleVersionEntity version, List<EcuDIDDto> ecuList, String vin);


    /***
     * 根据ECU版本列表获取文件列表
     * @param ecuVersionId
     * @return
     */
    public List<EcuVersionFileDto> findEcuVersionFiles(List<Long> ecuVersionId);


    public Long getTimestamp();

    /***
     * 根据的相对路径获取文件绝对路径
     * @param url
     * @return
     */
    public String findEcuFilePath(String url);

    /***
     * ECU刷写文件打包
     * @param ecu
     * @param version
     * @param files
     * @return
     */
    public Map<String, Object> buildFlashDownloadFile(String ecu, String version, List<EcuVersionFileDto> files, String vin);

    public Long findFullVolumePreviousVersion(Long currentVehicleVersionId, Long currentEcuVersionId,
                                              Long targetVehicleVersionId, Long targetEcuVersionId,
                                              Long ecuId);


}
