package org.dsa.modules.reporter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;


import java.io.Serializable;

@Data
@TableName("schedule_job")
public class ScheduleJob extends BaseEntity {

    @TableId(type = IdType.AUTO)
    private Long jobId;

    private String jobName;

    private String jobGroup;

    private String jobStatus;

    private String jobClass;

    private String cronExpression;

    private String remark;

    private String interfaceName;

    private Long repeatTime;

    private Boolean cronJob;

    private Boolean hasListener;

    private String listenerClass;
}
