package org.dsa.modules.diagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("d_action_dtc")
public class DActionDtcAnalysisEntity implements Serializable
{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 功能执行id
     */
    private String actionId;
    /**
     * 排序
     */
    private int serial;
    /**
     * ecu 名称
     */
    private String ecuName;
    /**
     * 语言
     */
    private String ti;
    /**
     * 故障码
     */
    private String dtcCode;
    /**
     * 故障码
     */
    private String dtcCodeInt;


    /**
     * 备注
     */
    private String description;

    /**
     * 状态
     */
    private String status;

    /**
     * 创建时间
     */
    private Date createTime;

}
