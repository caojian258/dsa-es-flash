package org.dsa.modules.reporter.vo.diag;

import lombok.Data;

@Data
public class DtcStatsResVo {

    private String collectDate;

    private String dtcEcu;

    private String dtcCode;

    private String dtcDescription;

    private String ti;

    private Integer total;
}
