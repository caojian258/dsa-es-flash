package org.dsa.modules.remoteDiagnostic.dao;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.*;
import org.dsa.modules.remoteDiagnostic.dto.flash.EcuVersionDto;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuVersionEntity;
import org.dsa.modules.remoteDiagnostic.vo.PublicPageReqVo;

import java.util.List;

@Mapper
public interface RemoteEcuVersionDao extends BaseMapper<RemoteVehicleEcuVersionEntity> {


    @Select("select * from r_ecu_version i where i.ecu_id = #{ecuId} and (i.status = 3 or i.status = 4) order by i.version_number")
    public List<RemoteVehicleEcuVersionEntity> getList(@Param("ecuId") Long id);

    @Select("select *,#{versionId} as versionId from r_ecu_version i where i.ecu_id = #{ecuId} and (i.status = 3 or i.status = 4) order by i.version_number")
//    @Results({
//            @Result(property = "flag",column = "{id=id,versionId=versionId}",
//                    one = @One(select = "org.dsa.modules.remoteDiagnostic.dao.RemoteEcuVersionDao.getFlag"))
//    })
    public List<RemoteVehicleEcuVersionEntity> getListById(@Param("ecuId") Long id, @Param("versionId") Long versionId);


//    public Long getFlag(@Param("id") Long id, @Param("versionId") Long versionId);

    @Select("select * from r_ecu_version i where i.ecu_id = #{ecuId} order by i.version_number")
    public List<RemoteVehicleEcuVersionEntity> getVersionList(@Param("ecuId") Long id);


    @Select("select *,'expect' as expect,'target' as target from r_ecu_version i where i.ecu_id = #{ecuId} order by i.version_number")
    @Results({
            @Result(property = "expectedIdentifications", column = "{editionId=id,type=expect}",
                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.RemoteEcuIdentificationInfoDao.getListByVersion")),
            @Result(property = "targetIdentifications", column = "{editionId=id,type=target}",
                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.RemoteEcuIdentificationInfoDao.getListByVersion")),
    })
    public List<RemoteVehicleEcuVersionEntity> getListAll(@Param("ecuId") Long id);

    @Select("select *,'expect' as expect,'target' as target from r_ecu_version i where id = #{id}")
    @Results({
            @Result(property = "expectedIdentifications", column = "{editionId=id,type=expect}",
                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.RemoteEcuIdentificationInfoDao.getListByVersion")),
            @Result(property = "targetIdentifications", column = "{editionId=id,type=target}",
                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.RemoteEcuIdentificationInfoDao.getListByVersion")),
            @Result(property = "files", column = "id",
                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.RemoteVehicleEcuVersionFileDao.getList")),
            @Result(property = "ecus", column = "id",
                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.RemoteVehicleEcuDao.getListByVersionId"))
    })
    public RemoteVehicleEcuVersionEntity getInfo(@Param("id") Long id);

    List<RemoteVehicleEcuEntity> selectEcuVersionDepend(@Param("versionId") Long id, @Param("ecuId") Long ecuId);

    List<RemoteVehicleEcuVersionEntity> selectAll(@Param("status") Integer status);

    Page<RemoteVehicleEcuVersionEntity> getPage(Page<RemoteVehicleEcuVersionEntity> page,@Param("vo") PublicPageReqVo vo);

    List<EcuVersionDto> findEcuVersionByVehicleVersion(@Param("versionId") Long versionId);


    List<EcuVersionDto> findEcuVersionDependencyByIds(@Param("ids") List<Long> ids);

    RemoteVehicleEcuVersionEntity findEcuVersionByDid(@Param("did") String did, @Param("didValue") String didValue, @Param("versionName") String versionName);

}
