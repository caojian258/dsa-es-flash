package org.dsa.modules.remoteDiagnostic.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.common.validator.group.UpdateGroup;
import org.dsa.modules.remoteDiagnostic.pojo.BaseEntity;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@ToString
@TableName("r_vehicle_pool")
public class RemoteVehiclePoolEntity extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;


    @NotBlank(message = "名称不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String name;

    private String descriptionTi;

    @NotNull(message = "状态不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private Integer status;

    // 销售区域
    @TableField(exist = false)
    private List<Long> salesArea;

    // 生产车间
    @TableField(exist = false)
    private List<Long> placeOfProduction;

    // 生产年份
    @TableField(exist = false)
    private List<Long> productionDate;

    @TableField(exist = false)
    private List<Long> vehicleTypeIds;

    @TableField(exist = false)
    private List<RemoteVehicleVersionEntity> versions;

    @TableField(exist = false)
    private List<RemoteVehicleEcuVersionEntity> ecuVersions;

    @TableField(exist = false)
    private List<String> vin;

    @TableField(exist = false)
    private List<Long> tags;

}
