package org.dsa.modules.reporter.job;

import lombok.extern.slf4j.Slf4j;
import org.dsa.common.utils.RedisUtils;
import org.dsa.config.SyncConfig;
import org.dsa.modules.reporter.constant.RedisConstant;
import org.dsa.modules.reporter.enums.FileCateEnum;
import org.dsa.modules.reporter.handler.WorkshopFileHandler;
import org.dsa.modules.reporter.handler.WorkshopUserFileHandler;
import org.dsa.modules.reporter.service.FileLogService;
import org.dsa.modules.reporter.util.FilesUtil;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

/***
 * 解析Workshop user 文件 xml
 */
@Slf4j
@Component
@DisallowConcurrentExecution
public class WorkshopJob extends QuartzJobBean {

    @Autowired
    WorkshopFileHandler workshopHandler;

    @Autowired
    WorkshopUserFileHandler workshopUserHandler;

    @Autowired
    SyncConfig syncConfig;

    @Autowired
    RedisUtils redisUtils;

    @Autowired
    FileLogService logService;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.info("WorkshopJob Start................");
        String archiveDir = syncConfig.getWorkshopArchiveDir();
        String dateFormat =  syncConfig.getDirFormat();
        SimpleDateFormat format = new SimpleDateFormat(dateFormat);
        String dayDir = format.format(new Date());

        String uploadDir = syncConfig.getWorkshopUploadDir() + File.separator + dayDir;
        String workshopFile = syncConfig.getWorkshopFileName();
        String path = uploadDir + File.separator + workshopFile;
        String key = RedisConstant.FILE_KEY+path;
        String flag = redisUtils.get(key);

        log.info("WorkshopJob file: {}", path);

        String archivePath = archiveDir + File.separator + dayDir;
        File archiveDirFile = new File(archivePath);
        if(!archiveDirFile.exists()){
            FilesUtil.mkdir(archiveDirFile);
        }
        File uploadDirs = new File(uploadDir);
        if(!uploadDirs.exists()){
            FilesUtil.mkdir(uploadDirs);
        }
        // 归档路径
        String archiveFile = "workshop_"+System.currentTimeMillis()+".xml";
        String archiveFilePath = archivePath + File.separator + archiveFile;

        if(StringUtils.isEmpty(flag)) {
            redisUtils.set(key, 1, 1200L);

            if(FilesUtil.checkExist(path)){
                try {
                    workshopHandler.storeData(path,archiveFilePath);
                }catch (Exception ex){
                    logService.failedLog(FileCateEnum.WORKSHOP.getValue(),workshopFile, path, null, "解析文件失败,错误信息:["+ ex.getMessage()+"]");
                }
            }

            try {

//                String archiveFile = "workshop_"+System.currentTimeMillis()+".xml";
                Files.copy(Paths.get(path), Paths.get(archiveFilePath));

            } catch (IOException e) {
                log.error(e.getMessage());
            }

            redisUtils.delete(key);
        }


        String userFileName = syncConfig.getWorkshopUserFileName();
        String userPath = uploadDir + File.separator + userFileName;

        // 归档路径
        archiveFile = "user_"+System.currentTimeMillis()+".xml";
        archiveFilePath = archivePath + File.separator + archiveFile;

        String uKey = RedisConstant.FILE_KEY+userPath;
        String uFlag = redisUtils.get(uKey);
        Integer fileId = null;
        if(StringUtils.isEmpty(uFlag)) {
            redisUtils.set(uKey, 1, 1200L);
            if(FilesUtil.checkExist(userPath)){
                try {
                    workshopUserHandler.storeData(userPath,archiveFilePath);
                }catch (Exception ex){
                    logService.failedLog(FileCateEnum.WORKSHOPUSER.getValue(), userFileName, archiveFilePath, null,"解析文件失败,错误信息:["+ ex.getMessage()+"]");
                }
            }
            try {
                Files.copy(Paths.get(userPath), Paths.get(archiveFilePath));
            } catch (IOException e) {
                log.error(e.getMessage());
            }
            redisUtils.delete(uKey);
        }

        log.info("WorkshopJob End................");
    }
}
