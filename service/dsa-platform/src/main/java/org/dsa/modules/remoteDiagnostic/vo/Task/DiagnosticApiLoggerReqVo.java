package org.dsa.modules.remoteDiagnostic.vo.Task;

import lombok.Data;
import lombok.ToString;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.common.validator.group.ReqGroup;
import org.dsa.common.validator.group.UpdateGroup;
import org.hibernate.validator.constraints.Length;
import org.springframework.web.multipart.MultipartFile;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

@Data
@ToString()
public class DiagnosticApiLoggerReqVo {


    @NotBlank(message = "vin is null", groups = {AddGroup.class, UpdateGroup.class, ReqGroup.class})
    @Length(message = "vin length error", min = 17, max = 17, groups = {AddGroup.class, UpdateGroup.class, ReqGroup.class} )
    private String vin;

    @NotBlank(message = "sessionId is null", groups = {UpdateGroup.class, ReqGroup.class})
    private String sessionId;

    @NotBlank(message = "md5 is null", groups = {UpdateGroup.class, ReqGroup.class})
    private String md5;

    @NotNull(message = "file is null", groups = {UpdateGroup.class, ReqGroup.class})
    private MultipartFile file;

    private Integer flag;

}
