package org.dsa.modules.reporter.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.pojo.CodeMessage;
import org.dsa.common.utils.HttpUtil;
import org.dsa.common.utils.Md5Utils;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.reporter.config.OffBoardConfig;
import org.dsa.modules.reporter.constant.FlashConstant;
import org.dsa.modules.reporter.constant.RedisConstant;
import org.dsa.modules.reporter.constant.VehicleConstant;
import org.dsa.modules.reporter.dao.VehicleFlashDetailMapper;
import org.dsa.modules.reporter.dao.VehicleFlashRecordMapper;
import org.dsa.modules.reporter.dto.EcuVersionHistoryDto;
import org.dsa.modules.reporter.dto.FlashEcuVersionsDto;
import org.dsa.modules.reporter.entity.VehicleFlashDetail;
import org.dsa.modules.reporter.entity.VehicleFlashRecord;
import org.dsa.modules.reporter.service.FlashLogService;
import org.dsa.modules.reporter.util.TspUtil;
import org.dsa.modules.reporter.vo.api.FlashLogReVo;
import org.dsa.modules.reporter.vo.api.SyncEcuVersionDto;
import org.dsa.modules.reporter.vo.api.SyncFlashLogRevo;
import org.dsa.modules.sys.entity.SysLogEntity;
import org.dsa.modules.sys.service.SysLogService;
import org.dsa.modules.vehicle.dao.DVehicleInfoDao;
import org.dsa.modules.vehicle.dao.DVehicleTypeDao;
import org.dsa.modules.vehicle.entity.DVehicleInfoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;
import java.util.*;


@Slf4j
@Service
public class FlashLogServiceImpl implements FlashLogService {

    @Autowired
    private VehicleFlashDetailMapper detailMapper;
    @Autowired
    private VehicleFlashRecordMapper recordMapper;
    @Autowired
    private DVehicleInfoDao dVehicleInfoDao;

    @Autowired
    private DVehicleTypeDao dVehicleTypeDao;

    @Resource
    private RedisUtils redisUtils;

    @Resource
    private OffBoardConfig offBoardConfig;

    @Resource
    private SysLogService sysLogService;


    @Override
    public Boolean validateSign(FlashLogReVo vo) {
        if(!offBoardConfig.getSignCheck()){
            return true;
        }
        if(null == vo.getSign()){
            return false;
        }
        String raw = "mod="+vo.getMode()+"&vin="+vo.getVin()+"&sessionId="+vo.getSessionId()+"&systemId="+offBoardConfig.getSystemId()+"&updateTime="+vo.getUpdateTime()+offBoardConfig.getSecret();
        String md5 =  Md5Utils.toMD5(raw);
        if(vo.getSign().equals(md5) ){
            return true;
        }
        return false;
    }

    @Override
    public Boolean validateTimestamp(FlashLogReVo vo) {
        if(null == vo.getUpdateTime()){
            return true;
        }
        Date updateDate = new Date(vo.getUpdateTime());
        Date nowDate = new Date();
        //客户端时间大于服务器时间 或者客户端时间小于服务器时间 30s以上验证失败
        if(nowDate.compareTo(updateDate)<0 || nowDate.compareTo(updateDate)> 30000){
            return false;
        }
        return true;
    }

    @Override
    public void collectData(FlashLogReVo vo) {

        VehicleFlashRecord vehicleFlashRecord = new VehicleFlashRecord();
        DVehicleInfoEntity dVehicleInfoEntity = dVehicleInfoDao.selectOneByVin(vo.getVin());

        String model = vo.getModel();
        if (dVehicleInfoEntity == null) {
            Long vehicleTypeId = dVehicleTypeDao.getVehicleTypeId(model);

            dVehicleInfoEntity = new DVehicleInfoEntity();
            dVehicleInfoEntity.setVin(vo.getVin());
            dVehicleInfoEntity.setVehicleTypeId(vehicleTypeId);
            dVehicleInfoEntity.setSource(VehicleConstant.AsMaintained);
            dVehicleInfoDao.insert(dVehicleInfoEntity);
        }

        vehicleFlashRecord.setVin(vo.getVin());
        vehicleFlashRecord.setTag(vo.getUpdateTime().toString());
        vehicleFlashRecord.setModel(vo.getModel());
        vehicleFlashRecord.setVehicleVersion(vo.getTargetVersion());
        vehicleFlashRecord.setPrevVersion(vo.getSourceVersion());
        vehicleFlashRecord.setCollectTime(new Date(Long.parseLong(vo.getUpdateTime().toString())));
        vehicleFlashRecord.setFeature(JSONObject.toJSONString(vo.getEcus()));
        vehicleFlashRecord.setSessionId(vo.getSessionId());
        vehicleFlashRecord.setMode(vo.getMode());
        recordMapper.insert(vehicleFlashRecord);

        Integer result = 1;
        String ss = "";

        if(CollectionUtil.isNotEmpty(vo.getEcus())){
            for (FlashEcuVersionsDto ecu: vo.getEcus()) {
                Map<String, String> expected = ecu.getExpectedIdents();
                if(null != expected){
                    Iterator<Map.Entry<String, String>> entries = expected.entrySet().iterator();
                    while (entries.hasNext()) {
                        Map.Entry<String, String> entry = entries.next();
                        String key = entry.getKey();
                        String value = entry.getValue();
                        VehicleFlashDetail vehicleFlashDetail = new VehicleFlashDetail();
                        vehicleFlashDetail.setVersionName(key);
                        vehicleFlashDetail.setVersionValue(value);
                        vehicleFlashDetail.setVin(vo.getVin());
                        vehicleFlashDetail.setFlashId(vehicleFlashRecord.getId().intValue());
                        vehicleFlashDetail.setCollectTime(vehicleFlashRecord.getCollectTime());
                        vehicleFlashDetail.setFlashTimestamp(vehicleFlashRecord.getCollectTime());
                        vehicleFlashDetail.setVersion(vehicleFlashRecord.getVehicleVersion());
                        vehicleFlashDetail.setEcuName(ecu.getName());
                        vehicleFlashDetail.setTag(vo.getUpdateTime().toString());
                        vehicleFlashDetail.setResult(ecu.getResult());
                        vehicleFlashDetail.setReason(ecu.getReason());
                        vehicleFlashDetail.setTarget(0);
                        detailMapper.insert(vehicleFlashDetail);
                    }
                }

                    Map<String, String> dto = ecu.getTargetIdents();
                    if(null != dto){
                        Iterator<Map.Entry<String, String>> entries = dto.entrySet().iterator();
                        while (entries.hasNext()) {
                            Map.Entry<String, String> entry = entries.next();
                            String key = entry.getKey();
                            String value = entry.getValue();
                            VehicleFlashDetail vehicleFlashDetail = new VehicleFlashDetail();
                            vehicleFlashDetail.setVersionName(key);
                            vehicleFlashDetail.setVersionValue(value);
                            vehicleFlashDetail.setVin(vo.getVin());
                            vehicleFlashDetail.setFlashId(vehicleFlashRecord.getId().intValue());
                            vehicleFlashDetail.setCollectTime(vehicleFlashRecord.getCollectTime());
                            vehicleFlashDetail.setFlashTimestamp(vehicleFlashRecord.getCollectTime());
                            vehicleFlashDetail.setVersion(vehicleFlashRecord.getVehicleVersion());
                            vehicleFlashDetail.setEcuName(ecu.getName());
                            vehicleFlashDetail.setTag(vo.getUpdateTime().toString());
                            vehicleFlashDetail.setResult(ecu.getResult());
                            vehicleFlashDetail.setReason(ecu.getReason());
                            vehicleFlashDetail.setTarget(1);
                            detailMapper.insert(vehicleFlashDetail);
                        }
                    }

                if(ecu.getResult() == FlashConstant.FAILED){
                    ss = ecu.getReason();
                    result = ecu.getResult();
                }
            }

            redisUtils.lSet(RedisConstant.FLASH_VIN_KEY,vo.getVin() +":"+vo.getSessionId()+":"+vo.getUpdateTime());
            vehicleFlashRecord.setResult(result);
            vehicleFlashRecord.setReason(ss);
            recordMapper.updateById(vehicleFlashRecord);
        }

    }

    @Override
    public void syncData(SyncFlashLogRevo vo) throws Exception {
        String domain = offBoardConfig.getSyncDomain();

        String url =  domain + offBoardConfig.getSyncUrl();

        Long timestamp = System.currentTimeMillis();
        if(null == vo.getUpdateTime()){
            vo.setUpdateTime(timestamp);
        }
        String params = JSONObject.toJSONString(vo);

        String encryptBody = TspUtil.aesEncrypt(params.getBytes(), offBoardConfig.getSecretKey());
        String sign = TspUtil.sign(encryptBody+timestamp, offBoardConfig.getSecretKey());

        Map<String, String> headers = new HashMap<>();

        headers.put("x-niutron-ak", offBoardConfig.getAccessKey());
        headers.put("x-niutron-sign", sign);
        headers.put("timestamp", timestamp.toString());
        Long endTime = System.currentTimeMillis();

        try {
           CodeMessage result =  HttpUtil.post(url, encryptBody, headers);
            log.info("sync result: {}", JSONObject.toJSONString(result));
            logSyncResult(result, vo, endTime-timestamp);
        }catch (Exception ex){
            log.error(ex.getMessage());
            CodeMessage result = new CodeMessage();
            result.setCode("500");
            result.setMessage(ex.getMessage());
            logSyncResult(result, vo, endTime-timestamp);
        }
    }

    @Override
    public List<EcuVersionHistoryDto> successRecord(String vin, String tag) {
        return detailMapper.findVehicleEcuVersion(vin, tag);
    }

    @Override
    public SyncFlashLogRevo formatSyncData(FlashLogReVo vo) {
        SyncFlashLogRevo syncVo = new SyncFlashLogRevo();
        syncVo.setUpdateTime(vo.getUpdateTime());
        syncVo.setMode(vo.getMode());
        syncVo.setVersion(vo.getTargetVersion());
        syncVo.setVin(vo.getVin());

        List<SyncEcuVersionDto> syncEcus = new ArrayList<>();
        List<FlashEcuVersionsDto> ecus = vo.getEcus();
        for (FlashEcuVersionsDto dto: ecus) {
            Map<String, String> targetIdents = dto.getTargetIdents();
            if(null != targetIdents) {
                Iterator<Map.Entry<String, String>> entries = targetIdents.entrySet().iterator();
                SyncEcuVersionDto ecu = new SyncEcuVersionDto();
                ecu.setName(dto.getName());
                ecu.setResult(dto.getResult());
                ecu.setReason(dto.getReason());
                while (entries.hasNext()) {
                    Map.Entry<String, String> entry = entries.next();
                    String key = entry.getKey();
                    String value = entry.getValue();
                    if("bootVersion".equals(key)){
                        ecu.setBootVersion(value);
                    }
                    if("hardVersion".equals(key)){
                        ecu.setHardVersion(value);
                    }
                    if("partCode".equals(key)){
                        ecu.setPartCode(value);
                    }
                    if("partNo".equals(key)){
                        ecu.setPartNo(value);
                    }
                    if("softwareVersion".equals(key)){
                        ecu.setSoftwareVersion(value);
                    }
                }
                syncEcus.add(ecu);
            }
        }
        syncVo.setEcus(syncEcus);
        return syncVo;
    }

    private void logSyncResult(CodeMessage res, SyncFlashLogRevo vo, Long time){

        SysLogEntity logEntity = new SysLogEntity();
        logEntity.setCreateDate(new Date());
        logEntity.setResult(res.getMessage());
        logEntity.setMethod(offBoardConfig.getSyncUrl());
        logEntity.setDisplayUserName("System");
        logEntity.setOperation(offBoardConfig.getSyncUrl());
        logEntity.setParams(JSONObject.toJSONString(vo));
        logEntity.setTime(time);
        logEntity.setIp(offBoardConfig.getSyncDomain());
        sysLogService.save(logEntity);

    }
}
