package org.dsa.modules.remoteDiagnostic.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskDetailEntity;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskLogEntity;
import org.dsa.modules.remoteDiagnostic.vo.PublicPageReqVo;

import java.util.List;

public interface DiagnosticTaskDetailService {

    Page<DiagnosticTaskDetailEntity> selectPage(PublicPageReqVo vo);

    List<DiagnosticTaskDetailEntity> getDetailList(String sessionId);

    List<DiagnosticTaskLogEntity> getTaskLogList(String sessionId);

    void saveDetail(Long taskId, String sessionId, String vin, String Type);

    void saveDetail(Long taskId, String sessionId, String vin, String Type, Integer Active, String code);

}
