package org.dsa.modules.sys.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class UserInfoEntity {
    String userId;
    String username;
    String userDisplayName;
    String userDisplayENName;
    String phone;
    String mail;
    Long workShopId;
    String workShop;
    String workShopAdd;
    String oem;
}
