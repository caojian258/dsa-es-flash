package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.entity.FutureSoftwareInfo;

import java.util.List;

@Mapper
public interface FutureSoftwareInfoMapper extends BaseMapper<FutureSoftwareInfo> {

    public FutureSoftwareInfo querySoftwareInfo(@Param("id") Integer id, @Param("versionNumber") Integer versionNumber, @Param("tag") String tag);

    public List<FutureSoftwareInfo> querySoftwareInfoByTag(@Param("tag") String tag, @Param("deviceId") String deviceId, @Param("idList") List<Integer> idList);
}
