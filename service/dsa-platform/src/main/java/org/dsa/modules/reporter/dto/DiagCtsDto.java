package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class DiagCtsDto implements Serializable {

    private String ctsName;

    private String title;

    private Date startTime;

    private Date endTime;

    private Integer resultFlag;

    private String resultTitle;
}
