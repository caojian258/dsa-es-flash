package org.dsa.modules.sys.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.utils.R;
import org.dsa.modules.sys.dto.SysTranslationsDto;
import org.dsa.modules.sys.service.SysLocaleService;
import org.dsa.modules.sys.vo.LocaleCheckVo;
import org.dsa.modules.sys.vo.LocalePageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RequestMapping("sys/locale")
@RestController
public class SysLocaleController {
    @Autowired
    SysLocaleService SysLocaleServiceImpl;

    /***
     * 下载多语言包
     * @param localeVo
     * @return
     */
    @PostMapping("/ti")
    public R queryLocaleTi(@RequestBody LocalePageVo localeVo){
        List<String> ts = new ArrayList<>();
        Page<SysTranslationsDto> result = SysLocaleServiceImpl.queryLocaleTiPage(localeVo);
        return R.ok().put("data", result);
    }

    /***
     * 检查是否要更新
     * @return
     */
    @PostMapping("/checkUpdate")
    public R checkLocaleUpdate(@RequestBody @Valid LocaleCheckVo vo){
        Integer size = SysLocaleServiceImpl.countTi(vo.getLocale());
        Boolean flag = false;
        if(size > vo.getSize()){
            flag = true;
        }
        Map<String, Object> map = new HashMap<>();
        map.put("flag", flag);
        map.put("size", size);
        map.put("locale", vo.getLocale());
        return R.ok().put("data", map);
    }
}
