package org.dsa.modules.reporter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain = true)
@TableName("d_system_metadata")
public class SystemMetadata implements Serializable {
    @TableId(type = IdType.AUTO)
    private Integer id;

    private String item;

    private String name;

    private String nameCn;

    private int xDim;

    private int yDim;

    private String unit;

    private Date createdAt;

    private Date updatedAt;
}
