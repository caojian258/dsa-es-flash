package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.dsa.modules.reporter.entity.ClientDependenceOverview;

import java.util.List;

@Mapper
public interface ClientDependenceOverviewMapper extends BaseMapper<ClientDependenceOverview> {

    public List<ClientDependenceOverview> queryByDeviceId(String id);
}
