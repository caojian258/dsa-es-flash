package org.dsa.common.annotation;

import java.lang.annotation.*;
// 多语言注解
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SysInternationalization {

    String value() default "select";
}
