package org.dsa.modules.remoteDiagnostic.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author weishunxin
 * @since 2023-05-11
 */
@RestController
@RequestMapping("/cert-dsa")
public class DSACertController {

    @RequestMapping("/xx")
    public String xx() {
        return "dsa https";
    }

}
