package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class SessionDto implements Serializable {

    private String source;

    private Integer category;

    private String sessionIndex;

    private String sessionInfoId;

    private String sessionId;

    private String workshop;

    private String pcid;

    private String tag;
    private String vinCode;

    private int diagnosticTotal = 0;

    private int diagnosticDetailTotal = 0;

    private int diagnosticVersionTotal = 0;

    private String msg;

    private Long userId;

    private String userName;

    private Long workshopId;

    private int didTotal = 0;

    private int dtcTotal = 0;

    private int version;

    private int dtcSnapshotTotal = 0;

    private int dtcExtendedTotal = 0;

    private int vehicleReportTotal = 0;

    private int asMaintainTotal = 0;

    private int ecuTotal = 0;

    private Date startTime;

    private Date endTime;

    public String getMsg(){
       return String.format("Source:%s, VIN:%s, SessionId:%s, Version:%s, Diagnostic Total:%s, ECU Total:%s, DTC Total:%s, DID Total:%s, DTC_SNAPSHOT Total:%s, DTC_EXTEND Total:%s, AsMaintain Total:%s",
               source, vinCode, sessionId, version, diagnosticTotal, ecuTotal, dtcTotal, didTotal, dtcSnapshotTotal, dtcExtendedTotal, asMaintainTotal);
    }
}
