package org.dsa.modules.diagnostic.service.impl;

import ch.qos.logback.classic.Logger;
import com.alibaba.fastjson.JSONObject;
import org.dsa.common.utils.Constant;
import org.dsa.modules.diagnostic.dao.DActionDtcGridDao;
import org.dsa.modules.diagnostic.dao.DActionEcuDao;
import org.dsa.modules.diagnostic.entity.DActionDtcGridEntity;
import org.dsa.modules.diagnostic.entity.DActionEcuEntity;
import org.dsa.modules.diagnostic.entity.mqtt.callback.MqBodyDataDTCDataGridVO;
import org.dsa.modules.diagnostic.entity.mqtt.callback.MqEntity;
import org.dsa.modules.diagnostic.service.DiagnosticMqService;
import org.dsa.modules.diagnostic.util.MqAsynchronousData;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service("dtcDataGridService")
public class DtcDataGridServiceImpl implements DiagnosticMqService {
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass());

    @Resource
    private DActionDtcGridDao dActionDtcGridDao;
    @Resource
    private DActionEcuDao dActionEcuDao;
    @Resource
    private MqAsynchronousData<MqBodyDataDTCDataGridVO> mqAsynchronousData;

    @Override
    public void onMessage(MqEntity mqEntity) {
        logger.info("dtcDataGrid MQ Call back---begin");
        logger.info("dtcDataGrid MQ Call bakc----actionID:{}",mqEntity.getHeader().getRid());
        try{
            Constant.RESULTS results =mqAsynchronousData.handle("dtcDataGrid",mqEntity,new MqBodyDataDTCDataGridVO());

            saveActionVersion(mqEntity);
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        logger.info("dtcDataGrid_read MQ Call back-----end");
    }

    private void saveActionVersion(MqEntity mqEntity){
        logger.info("Save dtcDataGrid data in DB");
        List<MqBodyDataDTCDataGridVO> dtcDataGridVos= new ArrayList<MqBodyDataDTCDataGridVO>();
        dtcDataGridVos= JSONObject.parseArray( JSONObject.toJSONString( mqEntity.getBody().getData()),MqBodyDataDTCDataGridVO.class);
        //插入读取冻结帧表
        if(dtcDataGridVos!=null){
            for(MqBodyDataDTCDataGridVO dtcdatagridVo:dtcDataGridVos){
                DActionDtcGridEntity dActionDtcGridEntity=new DActionDtcGridEntity();
                dActionDtcGridEntity.setActionId(mqEntity.getHeader().getRid());
                dActionDtcGridEntity.setDtcCode(dtcdatagridVo.getDtcCode());
                dActionDtcGridEntity.setDtcCode_Int(dtcdatagridVo.getDtcCode_Int());
                dActionDtcGridEntity.setDtcCodeHex(dtcdatagridVo.getDtcCodeHex());
                dActionDtcGridEntity.setEcuName(dtcdatagridVo.getEcuName());
                dActionDtcGridEntity.setName(dtcdatagridVo.getName());
                dActionDtcGridEntity.setTi(dtcdatagridVo.getTi());
                dActionDtcGridEntity.setValue(dtcdatagridVo.getValue());
                dActionDtcGridEntity.setTiValue(dtcdatagridVo.getTiValue());
                dActionDtcGridEntity.setUnit(dtcdatagridVo.getUnit());
                dActionDtcGridDao.insert(dActionDtcGridEntity);
            }

        }
        //插入ecu表
        for(String ecuName : mqEntity.getBody().getEcuName()) {
            DActionEcuEntity dActionEcuEntity = new DActionEcuEntity();
            dActionEcuEntity.setActionId(mqEntity.getHeader().getRid());
            dActionEcuEntity.setEcuName(ecuName);
            dActionEcuEntity.setDescription(mqEntity.getBody().getMessage());
            dActionEcuEntity.setEndTime(new Date());
            dActionEcuEntity.setStatus(mqEntity.getBody().getStatus());
            //保存诊断执行的ecu
            dActionEcuDao.updateByActionAndEcuName(dActionEcuEntity);
        }
    }
}
