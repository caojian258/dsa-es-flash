package org.dsa.modules.remoteDiagnostic.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.*;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuGroupEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleVersionEcuGroupEntity;

import java.util.List;

@Mapper
public interface RemoteVehicleEcuGroupDao extends BaseMapper<RemoteVehicleEcuGroupEntity> {


    List<RemoteVehicleEcuGroupEntity> getYList(Long id);

    List<RemoteVehicleEcuGroupEntity> getNList(Long id);

//    @Select("select i.*,i.id as groupId,k.ecu_id as ecuId from r_vehicle_ecu_group i left join (select ecu_id,ecu_group_id from r_vehicle_type_ecu where type_id = #{typeId} ) k on i.id = k.ecu_group_id where i.id in (select j.ecu_group_id from r_vehicle_type_ecu j where j.type_id = #{typeId})")
//    @Results({
//            @Result(property = "ecus", column = "groupId",
//                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.RemoteVehicleEcuDao.getFListByGroupId")),
//    })
    List<RemoteVehicleEcuGroupEntity> selectTypeGroup(@Param("typeId") Long typeId);

    @Select("select i.*,i.id as groupId from r_vehicle_ecu_group i where i.status = 0 and i.id in (select distinct j.ecu_group_id from r_vehicle_type_ecu j where j.type_id in ( " +
            "with recursive t_vehicle_type(id , value , parent_id , level ,have_odx_version ,have_otx_version) as ( " +
            "select dvt.id , dvt.value , dvt.parent_id , dvt.level , dvt.have_odx_version ,dvt.have_otx_version from d_vehicle_type dvt  where dvt.id  = #{typeId} " +
            "union all " +
            "select dlt.id , dlt.value , dlt.parent_id , dlt.level , dlt.have_odx_version ,dlt.have_otx_version from d_vehicle_type dlt , t_vehicle_type tvt where dlt.id = tvt.parent_id " +
            ") " +
            "select id  from t_vehicle_type " +
            "))")
    @Results({
            @Result(property = "ecus", column = "groupId",
                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.RemoteVehicleEcuDao.getListByGroupId")),
    })
    List<RemoteVehicleEcuGroupEntity> selectTypeGroups(@Param("typeId") Long typeId);

    @Select("select i.*,i.id as groupId  from r_vehicle_ecu_group i where i.id = #{id}")
    @Results({
            @Result(property = "ecus", column = "groupId",
                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.RemoteVehicleEcuDao.getListByGroupId")),
    })
    RemoteVehicleEcuGroupEntity getInfo(@Param("id") Long id);

    @Select(
            " <script> "
                    + " select i.*,i.id as groupId from r_vehicle_ecu_group i where i.status = 0 "
                    + " <if test='ids!=null and ids.size!=0 '>"
                    + " and i.id in  "
                    + " <foreach collection='ids' item='item' open=\"(\" separator=\",\" close=\")\" >"
                    + " #{item} "
                    + " </foreach> "
                    + " </if> "
                    + " order by i.id "
                    + " </script>")
    @Results({
            @Result(property = "ecus", column = "groupId",
                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.RemoteVehicleEcuDao.getListByGroupId")),
    })
    List<RemoteVehicleEcuGroupEntity> getGroups(@Param("ids") List<Long> ids);

    @Select(
            " <script> "
                    + " select i.*,i.id as groupId,#{typeId} as typeId from r_vehicle_ecu_group i where i.status = 0 "
                    + " <if test='ids!=null and ids.size!=0 '>"
                    + " and i.id in  "
                    + " <foreach collection='ids' item='item' open=\"(\" separator=\",\" close=\")\" >"
                    + " #{item} "
                    + " </foreach> "
                    + " </if> "
                    + " order by i.id "
                    + " </script>")
    @Results({
            @Result(property = "ecus", column = "groupId",
                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.RemoteVehicleEcuDao.getListByGroupId")),
            @Result(property = "vehicleEcuGroupVersion",javaType = RemoteVehicleVersionEcuGroupEntity.class, column = "{typeId=typeId,groupId=groupId}",
                    one = @One(select = "org.dsa.modules.remoteDiagnostic.dao.RemoteVehicleVersionEcuGroupDao.getInfoByGroup")),
    })
    List<RemoteVehicleEcuGroupEntity> getGroupsByVersion(@Param("ids") List<Long> ids,@Param("typeId") Long typeId);

//    @Select(" select i.*,i.id as groupId,j.version_id as versionId from r_vehicle_version_ecu_group j inner join" +
//            "  r_vehicle_ecu_group i on j.version_id=#{versionId} and j.group_id = i.id and i.status = 0" )
//    @Results({
//            @Result(property = "vehicleEcuGroupVersion",javaType = RemoteVehicleVersionEcuGroupEntity.class, column = "{versionId=versionId,groupId=groupId}",
//                    one = @One(select = "org.dsa.modules.remoteDiagnostic.dao.RemoteVehicleVersionEcuGroupDao.getInfoByGroup")),
//    })
//    List<RemoteVehicleEcuGroupEntity> getGroupsByVersionId(@Param("versionId") Long versionId);


    @Select("select id from r_vehicle_ecu_group where group_name  = #{name} ")
    Long selectByName(@Param("name") String name);

    @Select(
            " <script> "
                    + " select i.*,i.id as groupId from r_vehicle_ecu_group i where 1=1  "
                    + " <if test=\"name!= null and name!='' \">"
                    + " and i.group_name ilike concat('%',#{name},'%')   "
                    + " </if> "
                    + " <if test='status!=null'>"
                    + " and i.status = #{status}  "
                    + " </if> "
                    + " </script>")
    @Results({
            @Result(property = "ecus", column = "groupId",
                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.RemoteVehicleEcuDao.getFListByGroupId")),
    })
    Page<RemoteVehicleEcuGroupEntity> selectEcuPage(Page<RemoteVehicleEcuGroupEntity> page,@Param("name") String name,@Param("status")  Integer status);

    List<RemoteVehicleEcuGroupEntity> selectGroups();

    @Select("select t2.group_name from r_vehicle_type_ecu t1 left join r_vehicle_ecu_group t2 on t1.ecu_group_id = t2.id where t1.type_id  = #{vehicleTypeId}")
    List<String> getECUGroupNames(Long vehicleTypeId);

}
