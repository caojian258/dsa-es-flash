package org.dsa.modules.onlineUpdate.utils;


import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;

import net.lingala.zip4j.model.enums.AesKeyStrength;
import net.lingala.zip4j.model.enums.CompressionLevel;
import net.lingala.zip4j.model.enums.CompressionMethod;
import net.lingala.zip4j.model.enums.EncryptionMethod;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class ZipUtils {
    private static final Logger log = LoggerFactory.getLogger(ZipUtils.class);

    /**
     *
     * 压缩指定路径的文件
     * @param srcFilePath 待压缩文件路径
     * @param zipPathFileName zip文件全路径名
     * @param password 加密密码
     * @return
     */
    public static boolean zipFile(String srcFilePath, String zipPathFileName, String password){

        try {
            // 生成的压缩文件
            ZipFile zipFile = new ZipFile(zipPathFileName);
            ZipParameters parameters = new ZipParameters();
            // 压缩级别
            parameters.setCompressionMethod(CompressionMethod.DEFLATE);
            parameters.setCompressionLevel(CompressionLevel.NORMAL);

            if(!StringUtils.isEmpty(password)){
                parameters.setEncryptFiles(true);
                parameters.setEncryptionMethod(EncryptionMethod.AES);
                parameters.setAesKeyStrength(AesKeyStrength.KEY_STRENGTH_256);
                zipFile.setUseUtf8CharsetForPasswords(true);
                zipFile.setPassword(password.toCharArray());
            }

            // 要打包的文件夹
            File currentFile = new File(srcFilePath);
            File[] fs = currentFile.listFiles();
            // 遍历test文件夹下所有的文件、文件夹
            for (File f : fs) {
                if (f.isDirectory()) {
                    zipFile.addFolder(f, parameters);
                } else {
                    zipFile.addFile(f, parameters);
                }
            }
            return true;
        } catch (ZipException e) {
            e.printStackTrace();
            log.error("压缩文件【"+srcFilePath+"】到路径【"+zipPathFileName+"】失败：\n"+e.getMessage());
            return false;
        }

    }

    /**
     *  @param zipFileFullName zip文件所在的路径名
     * @param filePath 解压到的路径
     * @param password 需要解压的密码
     * @return
     */
    public static boolean unZipFile(String zipFileFullName, String filePath, String password) {
        try {
            ZipFile zipFile = new ZipFile(zipFileFullName);
            // 如果解压需要密码
            if(StringUtils.isNotEmpty(password)&&zipFile.isEncrypted()) {
                zipFile.setUseUtf8CharsetForPasswords(true);
                zipFile.setPassword(password.toCharArray());

            }
            zipFile.extractAll(filePath);
            return true;
        } catch (ZipException e) {
            e.printStackTrace();
            log.error("解压文件【"+zipFileFullName+"】到路径【"+filePath+"】失败：\n"+e.getMessage());
            return false;
        }
    }



}
