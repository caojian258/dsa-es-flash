package org.dsa.modules.reporter.constant;

public final class ConfigConstant {

    public final static String REPORTING_CLIENT = "REPORTING_CLIENT";

    public final static String REPORTING_CLIENT_APP = "REPORTING_CLIENT_APP";

    public final static String REPORTING_CHART_APPS = "REPORTING_CHART_APPS";

    //诊断会话列表ecu配置 多个ECU英文分号分割
    public final static String ECU_SELECT = "ECU_SELECT";

    public final static String DEFAULT_SOURCE = "001";

}
