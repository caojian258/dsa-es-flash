package org.dsa.modules.vehicle.util;

import org.apache.commons.lang.StringUtils;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.vehicle.dao.DVehicleInfoDao;
import org.dsa.modules.vehicle.dao.DVehicleTypeDao;
import org.dsa.modules.vehicle.entity.DVehicleTypeEntity;
import org.dsa.modules.vehicle.entity.VehicleInfoEntity;
import org.springframework.stereotype.Component;

import jakarta.annotation.Resource;
import java.util.List;

@Component
public class VehicleUtils {

    @Resource
    private RedisUtils redisUtils;

    @Resource
    private DVehicleTypeDao dVehicleTypeDao;

    @Resource
    private DVehicleInfoDao dVehicleInfoDao;

    /**
     * 校验入参
     */
    public void checkInput(List<VehicleInfoEntity> list) {
        StringBuffer sb = new StringBuffer();
        for (VehicleInfoEntity vehicleInfoEntity : list) {
            //根据入参判断vin码信息是否存在
            if (StringUtils.isEmpty(vehicleInfoEntity.getVin())) {
                sb.append("vin码:为空,请检查;");
            } else {
                if (vehicleInfoEntity.getVin().length() != 17) {
                    sb.append("vin码:" + vehicleInfoEntity.getVin() + "不合法;");
                }
            }
            //根据入参判断车型信息是否存在
            if (StringUtils.isEmpty(vehicleInfoEntity.getMaker())) {
                sb.append("vin码:" + vehicleInfoEntity.getVin() + ",厂商不允许为空;");
            }
            //根据入参判断车型信息是否存在,
            if (StringUtils.isEmpty(vehicleInfoEntity.getModel())) {
                sb.append("vin码:" + vehicleInfoEntity.getVin() + ",车型不允许为空;");
            }
        }
        if (StringUtils.isNotEmpty(sb.toString())) {
            throw new RRException(sb.toString(), 541);
        }
    }

    /**
     * 保存级别数据
     */
    public Long synLevelInfo(VehicleInfoEntity vehicleInfoEntity) {
        Long makerId = 0L;
        Long modelId = 0L;
        Long platformId = 0L;
        Long trimLevelId = 0L;
        Long yearId = 0L;

        //必填项
        if (redisUtils.hasKey(Constant.VEHICLE_TYPE.MAKER.getName() + vehicleInfoEntity.getMaker())) {
            makerId = Long.valueOf(redisUtils.get(Constant.VEHICLE_TYPE.MAKER.getName() + vehicleInfoEntity.getMaker()));
        } else {
            makerId = insertVehicleType(0L, Constant.VEHICLE_TYPE.MAKER, vehicleInfoEntity.getMaker());
            redisUtils.set(Constant.VEHICLE_TYPE.MAKER.getName() + vehicleInfoEntity.getMaker(), makerId, 30 * 60);
        }
        //必填项
        if (redisUtils.hasKey(Constant.VEHICLE_TYPE.MODEL.getName() + vehicleInfoEntity.getModel())) {
            modelId = Long.valueOf(redisUtils.get(Constant.VEHICLE_TYPE.MODEL.getName() + vehicleInfoEntity.getModel()));
        } else {
            modelId = insertVehicleType(makerId, Constant.VEHICLE_TYPE.MODEL, vehicleInfoEntity.getModel());
            redisUtils.set(Constant.VEHICLE_TYPE.MODEL.getName() + vehicleInfoEntity.getModel(), modelId,30 * 60);
        }

        if (StringUtils.isNotEmpty(vehicleInfoEntity.getPlatform())) {
            if (redisUtils.hasKey(modelId + "_" + Constant.VEHICLE_TYPE.PLATFORM.getName() + vehicleInfoEntity.getPlatform())) {
                platformId = Long.valueOf(redisUtils.get(modelId + "_" + Constant.VEHICLE_TYPE.PLATFORM.getName() + vehicleInfoEntity.getPlatform()));
            } else {
                platformId = insertVehicleType(modelId, Constant.VEHICLE_TYPE.PLATFORM, vehicleInfoEntity.getPlatform());
                // 防止出现redis重复key,拼接model Id
                redisUtils.set(modelId + "_" + Constant.VEHICLE_TYPE.PLATFORM.getName() + vehicleInfoEntity.getPlatform(), platformId,30 * 60);
            }
        }
//        else {
//            return modelId;
//        }

        if (StringUtils.isNotEmpty(vehicleInfoEntity.getYear())) {
            if (redisUtils.hasKey(platformId + "_" + Constant.VEHICLE_TYPE.YEAR.getName() + vehicleInfoEntity.getYear())) {
                yearId = Long.valueOf(redisUtils.get(platformId + "_" + Constant.VEHICLE_TYPE.YEAR.getName() + vehicleInfoEntity.getYear()));
            } else {
                yearId = insertVehicleType(platformId, Constant.VEHICLE_TYPE.YEAR, vehicleInfoEntity.getYear());
                redisUtils.set(platformId + "_" + Constant.VEHICLE_TYPE.YEAR.getName() + vehicleInfoEntity.getYear(), yearId,30 * 60);
            }
        }
//        else {
//            return platformId;
//        }

        if (StringUtils.isNotEmpty(vehicleInfoEntity.getTrimLevel())) {
            if (redisUtils.hasKey(yearId + "_" + Constant.VEHICLE_TYPE.TRIM_LEVEL.getName() + vehicleInfoEntity.getTrimLevel())) {
                trimLevelId = Long.valueOf(redisUtils.get(yearId + "_" + Constant.VEHICLE_TYPE.TRIM_LEVEL.getName() + vehicleInfoEntity.getTrimLevel()));
            } else {
                trimLevelId = insertVehicleType(yearId, Constant.VEHICLE_TYPE.TRIM_LEVEL, vehicleInfoEntity.getTrimLevel());
                redisUtils.set(yearId + "_" + Constant.VEHICLE_TYPE.TRIM_LEVEL.getName() + vehicleInfoEntity.getTrimLevel(), trimLevelId,30 * 60);
            }
        }

        //@TODO platform是第三层，目前有五层结构，是否需要返回最后一层(trimLevelId)
        return platformId;
    }

    /**
     * db插入车型信息并返回id
     */
    private Long insertVehicleType(Long parentId, Constant.VEHICLE_TYPE VEHICLE_TYPE, String value) {
//        Integer orderNum = dVehicleTypeDao.selectCountByLevel(Constant.VEHICLE_TYPE.MAKER.getLevel());
//        orderNum = orderNum == null ? 1 : orderNum;
        int orderNum = 0;
        if (parentId != 0) {
            Integer maxOrderNum = dVehicleTypeDao.getMaxOrderNum(parentId);
            if (maxOrderNum != null) {
                orderNum = maxOrderNum + 1;
            }
        }

        //insert DVehicleInfo
        DVehicleTypeEntity dVehicleTypeEntity = dVehicleTypeDao.selectByValueAndParentId(value, parentId);
//        DVehicleTypeEntity dVehicleTypeEntity = new DVehicleTypeEntity();
        if (dVehicleTypeEntity == null) {
            dVehicleTypeEntity = new DVehicleTypeEntity();
            dVehicleTypeEntity.setId(0L);
            dVehicleTypeEntity.setParentId(parentId);
            dVehicleTypeEntity.setLevel(VEHICLE_TYPE.getLevel());
            dVehicleTypeEntity.setValue(value);
            dVehicleTypeEntity.setOrderNum(orderNum);
            dVehicleTypeDao.insertDVehicleType(dVehicleTypeEntity);
        }
        return dVehicleTypeEntity.getId();
    }
}
