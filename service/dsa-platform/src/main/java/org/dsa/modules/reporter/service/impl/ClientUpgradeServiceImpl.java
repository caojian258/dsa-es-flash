package org.dsa.modules.reporter.service.impl;


import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.reporter.constant.RedisConstant;
import org.dsa.modules.reporter.dao.*;
import org.dsa.modules.reporter.dto.ClientUpgradeFullDto;
import org.dsa.modules.reporter.entity.*;
import org.dsa.modules.reporter.enums.AppTypeEnum;
import org.dsa.modules.reporter.enums.StatusTypeEnum;
import org.dsa.modules.reporter.service.ClientUpgradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;


@Slf4j
@Service
public class ClientUpgradeServiceImpl implements ClientUpgradeService {

    @Resource
    ClientUpgradeMapper clientUpgradeMapper;

    @Resource
    AppHistoryMapper appHistoryMapper;

    @Autowired
    RedisUtils redisUtils;

    @Autowired
    CurrentSoftwareInfoMapper currentSoftwareInfoMapper;

    @Autowired
    FutureSoftwareInfoMapper futureSoftwareInfoMapper;

    @Autowired
    CurrentComponentInfoMapper currentComponentInfoMapper;

    @Autowired
    FutureComponentInfoMapper futureComponentInfoMapper;


    @Override
    public List<ClientUpgrade> queryDataByTag(String tag) {
        return clientUpgradeMapper.queryDataByTag(tag);
    }


    @Override
    public List<ClientUpgradeFullDto> allData(String tag) {
        return clientUpgradeMapper.getOneFullData(tag);
    }

    @Override
    public void insertHistory(ClientUpgradeFullDto dto) {
        AppHistory entity = new AppHistory();

        entity.setDeviceId(dto.getDeviceId());
        entity.setUsername(dto.getUsername());
        entity.setWorkshopId(dto.getWorkshopId());
        entity.setWorkshopName(dto.getWorkshopName());

        //0 软件 1 组件

        if( dto.getUpdateTarget().equals(0)){
            entity.setAppType(AppTypeEnum.SOFTWARE.getValue());
            entity.setAppId(dto.getSoftwareId());
            entity.setAppName(dto.getSoftwareName());
            entity.setAppVersion(dto.getSoftwareTargetVersionName());
            entity.setAppPrevVersion(dto.getSoftwareCurrentVersionName());
            entity.setVersionNumber(dto.getSoftwareCurrentVersionNumber());
            entity.setPrevVersionNumber(dto.getSoftwarePreviousVersionNumber());
            if(dto.getInstallAction().equals(StatusTypeEnum.rollback.getValue())){
                entity.setAppVersion(dto.getSoftwareRollbackVersionName());
            }
        }else{
            entity.setAppType(AppTypeEnum.COMPONENT.getValue());
            entity.setAppId(dto.getComponentId());
            entity.setAppName(dto.getComponentName());
            entity.setAppVersion(dto.getComponentTargetVersionName());
            entity.setAppPrevVersion(dto.getComponentCurrentVersionName());
            entity.setVersionNumber(dto.getComponentCurrentVersionNumber());
            entity.setPrevVersionNumber(dto.getComponentPreviousVersionNumber());
            if(dto.getInstallAction().equals(StatusTypeEnum.rollback.getValue())){
                entity.setAppVersion(dto.getComponentRollbackVersionName());
            }
        }

        entity.setStatusType(dto.getInstallAction());
        entity.setStatus(dto.getUpdateResult());
        entity.setCollectDate(dto.getCtime());
        entity.setTag(dto.getTag());
        appHistoryMapper.insert(entity);

    }

    @Override
    public Set<Integer> queryClientUpdateId(String deviceId) {

        Set<String> ids =  new HashSet<String>();
        Set<Integer> idRecords =  new HashSet<Integer>();
        String key = RedisConstant.CLIENT_UPDATE_KEY+ ":" + deviceId;
        ids =  (Set<String>) redisUtils.sMembers(key);
        log.info("ids cached: {}", JSONObject.toJSONString(ids));

        if(CollectionUtil.isEmpty(ids)){
             idRecords = clientUpgradeMapper.queryClientUpdateId(deviceId);
            if(CollectionUtil.isNotEmpty(idRecords)){
                Set<String> idStrSet = idRecords.stream().map(String::valueOf).collect(Collectors.toSet());
                long size = redisUtils.sSet(key, idStrSet.toArray());
                log.info("ids cache size: {}", size);
            }
        }else{
            idRecords = ids.stream().map(Integer::valueOf).collect(Collectors.toSet());
        }

        return idRecords;
    }

    @Override
    public Boolean checkClientUpdateId(String deviceId, Integer updateId) {
        String key = RedisConstant.CLIENT_UPDATE_KEY+":"+deviceId;
        Boolean flag = false;
        if(!redisUtils.exists(key)){
            queryClientUpdateId(deviceId);
        }
        if(redisUtils.sIsMember(key, updateId+"")){
            flag = true;
        }
        return flag;
    }

    @Override
    public long cacheClientUpdateId(String deviceId, Integer updateId) {
        String key = RedisConstant.CLIENT_UPDATE_KEY+":"+deviceId;
        return redisUtils.sSet(key, updateId+"");
    }

    @Override
    public Set<Integer> queryClientUpdateIdWithoutCache(String deviceId) {
        Set<Integer> idRecords =  new HashSet<Integer>();
        idRecords = clientUpgradeMapper.queryClientUpdateId(deviceId);
        return idRecords;
    }

    @Override
    public Map<Integer, CurrentSoftwareInfo> queryCurrentSoftwareInfoByTag(String deviceId, String tag, List<Integer> idList) {
        List<CurrentSoftwareInfo>  items = currentSoftwareInfoMapper.querySoftwareInfoByTag(deviceId, tag, idList);
        Map<Integer, CurrentSoftwareInfo> currentSoftwareInfoMap = new HashMap<>();

        for (CurrentSoftwareInfo info: items) {
            currentSoftwareInfoMap.put(info.getSoftwareId(), info);
        }
        return currentSoftwareInfoMap;
    }

    @Override
    public Map<Integer, FutureSoftwareInfo> queryFutureSoftwareInfoByTag(String deviceId, String tag, List<Integer> idList) {
        List<FutureSoftwareInfo> items = futureSoftwareInfoMapper.querySoftwareInfoByTag(tag, deviceId, idList);
        Map<Integer, FutureSoftwareInfo> futureSoftwareInfoMap = new HashMap<>();

        for (FutureSoftwareInfo info: items) {
            futureSoftwareInfoMap.put(info.getSoftwareId(), info);
        }

        return futureSoftwareInfoMap;
    }

    @Override
    public Map<Integer, CurrentComponentInfo> queryCurrentComponentInfoByTag(String deviceId, String tag, List<Integer> idList) {
        List<CurrentComponentInfo> currentComponentInfos = currentComponentInfoMapper.queryComponentInfoByTag(deviceId, tag, idList);
        Map<Integer, CurrentComponentInfo> currentComponentInfoMap = new HashMap<>();

        for (CurrentComponentInfo info: currentComponentInfos){
            currentComponentInfoMap.put(info.getComponentId(), info);
        }
        return currentComponentInfoMap;
    }

    @Override
    public Map<Integer, FutureComponentInfo> queryFutureComponentInfoByTag(String deviceId, String tag, List<Integer> idList) {
        List<FutureComponentInfo> futureComponentInfos = futureComponentInfoMapper.queryComponentInfoByTag(deviceId, tag, idList);
        Map<Integer, FutureComponentInfo> futureComponentInfoMap = new HashMap<>();

        for (FutureComponentInfo info: futureComponentInfos){
            futureComponentInfoMap.put(info.getComponentId(), info);
        }
        return futureComponentInfoMap;
    }


}
