package org.dsa.modules.diagnostic.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 已激活列表
 * 
 */
@Data
public class VehicleActiveEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 车辆VIN码
	 */
	private String vin;

	/**
	 * 会话ID
	 */
	private String sessionId;

	/**
	 * 经销商ID
	 */
	private Long workShopId;

	/**
	 * 经销商名称
	 */
	private String workShopName;

	/**
	 * 发起激活用户ID
	 */
	private Long activeUserId;

	/**
	 * 发起激活用户名称
	 */
	private String activeUserName;

	/**
	 * 激活时间
	 */
	private Long activeTime;

	/**
	 * 诊断用户id
	 */
	private Long diagnosticUserId;

	/**
	 * 诊断用户名称
	 */
	private String diagnosticUserName;

	/**
	 * 诊断开始时间
	 */
	private Long diagnosticTime;

	/**
	 * 激活状态
	 */
	private int status;


	/**
	 * 最后一次心跳时间
	 */
	private Long lastConnectTime;

}
