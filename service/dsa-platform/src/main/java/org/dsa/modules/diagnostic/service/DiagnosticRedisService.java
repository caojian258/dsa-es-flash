package org.dsa.modules.diagnostic.service;


/**
 * redis 数据处理
 *
 */
public interface DiagnosticRedisService {

    
    /**
     * redis过期处理
     * @param redisKey
     */
    public void overdue(String redisKey);

}

