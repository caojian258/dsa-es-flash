package org.dsa.modules.remoteDiagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;
import org.dsa.modules.remoteDiagnostic.dto.IdentificationInfoDto;
import org.dsa.modules.remoteDiagnostic.entity.RemoteEcuIdentificationInfoEntity;

import java.util.List;
import java.util.Map;

@Mapper
public interface RemoteEcuIdentificationInfoDao extends BaseMapper<RemoteEcuIdentificationInfoEntity> {


    @Insert("<script>"
            + " insert into r_ecu_identification_info (ecu_id,group_id,ecu_version_id,did,did_name,description_ti) values "
            + " <foreach collection='list' item='item' separator=','> "
            + " (#{ecuId},#{groupId},#{editionId},#{item.did},#{item.didName},#{item.descriptionTi}) "
            + " </foreach>"
            + " </script>")
    void inserts(@Param("groupId") Long groupId, @Param("ecuId") Long ecuId, @Param("editionId") Long editionId, @Param("list") List<RemoteEcuIdentificationInfoEntity> list);

    @Insert("<script>"
            + " insert into r_ecu_identification_info (ecu_version_id,did,did_val,status,identify_group,did_name,description_ti) values "
            + " <foreach collection='list' item='item' separator=','> "
            + " (#{editionId},#{item.did},#{item.didVal},#{item.status},#{item.identifyGroup},#{item.didName},#{item.descriptionTi}) "
            + " </foreach>"
            + " </script>")
    void insertsByVal(@Param("editionId") Long editionId, @Param("list") List<RemoteEcuIdentificationInfoEntity> list);

    @Insert("<script>"
            + " insert into r_ecu_identification_info (version_id,did,did_val,status,identify_group,did_name,description_ti) values "
            + " <foreach collection='list' item='item' separator=','> "
            + " (#{versionId},#{item.did},#{item.didVal},#{item.status},#{item.identifyGroup},#{item.didName},#{item.descriptionTi}) "
            + " </foreach>"
            + " </script>")
    void insertsByVehicleVersion(@Param("versionId") Long versionId, @Param("list") List<RemoteEcuIdentificationInfoEntity> list);


    @Delete("<script>"
            + " delete from r_ecu_identification_info where ecu_id = #{ecuId} and  group_id = #{groupId}  and  ecu_version_id = #{editionId} "
            + " </script>")
    void deletes(@Param("groupId") Long groupId, @Param("ecuId") Long ecuId, @Param("editionId") Long editionId);


    @MapKey("did")
    List<Map<String, String>> getList(@Param("groupId") Long groupId, @Param("ecuId") Long ecuId, @Param("editionId") Long editionId);

    @Select("select * from r_ecu_identification_info i where i.ecu_version_id = #{editionId} and i.identify_group = #{type} ")
    List<RemoteEcuIdentificationInfoEntity> getListByVersion(@Param("editionId") Long editionId, @Param("type") String type);

    @Select("select * from r_ecu_identification_info i where i.version_id = #{versionId} and i.identify_group = #{type} ")
    List<RemoteEcuIdentificationInfoEntity> getListByVehicleVersion(@Param("versionId") Long versionId, @Param("type") String type);

    void insertEcuExists(@Param("ecuId") Long ecuId, @Param("list") List<RemoteEcuIdentificationInfoEntity> list);

    List<RemoteEcuIdentificationInfoEntity> getDidListByEcuNames(@Param("ecuNames") List<String> ecuIds);

 	List<RemoteEcuIdentificationInfoEntity> findEcuIdentification(@Param("ecuVersionId") Long id,  @Param("group") String group);

    List<IdentificationInfoDto> getIdentificationByVersionAndEcu(@Param("group") String group, @Param("ecuVersionId") List<Long> ecuVersionId, @Param("versionId") Long versionId);}
