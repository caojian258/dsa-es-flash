package org.dsa.modules.reporter.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.dsa.modules.reporter.dao.AppVersionMapper;
import org.dsa.modules.reporter.entity.AppVersion;
import org.dsa.modules.reporter.service.AppVersionIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppVersionServiceImpl extends ServiceImpl<AppVersionMapper, AppVersion> implements AppVersionIService {

    @Autowired
    AppVersionMapper mapper;

    @Override
    public List<AppVersion> getPcItems(String pcid, String type, String startDate, String endDate) {
        return mapper.getGroupList(pcid, type, startDate, endDate);
    }
}
