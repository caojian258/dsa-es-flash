package org.dsa.modules.onlineUpdate.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dsa.common.utils.PageUtils;
import org.dsa.modules.onlineUpdate.entity.OemSoftwareVersionEntity;
import org.dsa.modules.onlineUpdate.vo.CascadeOptions;

import java.util.List;
import java.util.Map;

/**
 * OemSoftwareVersion
 *
 */
public interface OemSoftwareVersionService extends IService<OemSoftwareVersionEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveOrUpdate2(OemSoftwareVersionEntity entity);

    List<CascadeOptions> queryVersionTree();
}

