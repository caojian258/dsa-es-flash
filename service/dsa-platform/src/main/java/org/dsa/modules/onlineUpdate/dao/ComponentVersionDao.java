package org.dsa.modules.onlineUpdate.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.onlineUpdate.entity.ComponentVersionEntity;
import org.dsa.modules.onlineUpdate.vo.ComponentVersionVo;

import java.util.List;

/**
 * 组件版本
 * 
 */
@Mapper
public interface ComponentVersionDao extends BaseMapper<ComponentVersionEntity> {

    List<ComponentVersionVo> selectByComponentId(Long componentId);

    Boolean hasRequiredComponent(@Param("componentVersionIds") List<Long> componentVersionIds);

    void updateRequiredComponent(@Param("componentVersionIds") List<Long> componentVersionIds);

    ComponentVersionVo selectCompVoByCVersionId(@Param("id") Long id);

    boolean isRelease(@Param("componentId") Long componentId);
}
