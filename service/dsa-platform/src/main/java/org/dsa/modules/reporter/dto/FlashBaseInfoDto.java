package org.dsa.modules.reporter.dto;

import lombok.Data;
import jakarta.validation.constraints.NotNull;

@Data
public class FlashBaseInfoDto implements Comparable{
    private String title;

    private String field;

    private String value;

    private Integer sorted;

    public FlashBaseInfoDto() {
    }

    public FlashBaseInfoDto(String title, String value) {
        this.title = title;
        this.value = value;
    }

    @Override
    public int compareTo(@NotNull Object o) {
        int compare =((ClientBaseInfoDto)o).getSorted();
        /* 正序排列 */
        return this.sorted - compare;
    }

    @Override
    public String toString() {
        return "[ title=" + title + ", field=" + field + ", value=" + value + ", sorted=" + sorted + "]";
    }
}
