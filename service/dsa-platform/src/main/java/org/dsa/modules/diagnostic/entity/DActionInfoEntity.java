package org.dsa.modules.diagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("d_action_info")
@AllArgsConstructor
@NoArgsConstructor
public class DActionInfoEntity implements Serializable
{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 消息id
     */
    private Long mid;
    /**
     * 功能执行id
     */
    private String actionId;
    /**
     * 功能码
     */
    private String functionCode;
    /**
     * 操作码
     */
    private String actionCode;
    /**
     * 备注
     */
    private String description;
    /**
     * 执行消息
     */
    private String message;
    /**
     * result_message_en
     */
    private String messageEn;
    /**
     * result_message_cn
     */
    private String messageCn;
    /**
     * 状态
     */
    private String results;
    /**
     * 是否超时执行
     */
    private int timeout;


    /**
     * 功能执行开始时间
     */
    private Date startTime;

    /**
     * 功能执行结束时间
     */
    private Date endTime;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * functionAction
     */
    private String functionAction;
    /**
     * endStatus
     */
    private Integer endStatus;

    //t_diagnostic  title_cn
    private String titleCn;

    //t_diagnostic  title_en
    private String titleEn;

}
