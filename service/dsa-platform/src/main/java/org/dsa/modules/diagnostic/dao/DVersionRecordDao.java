package org.dsa.modules.diagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.dsa.modules.diagnostic.entity.DVersionRecordEntity;

import java.util.List;

/**
 * 版本操作记录表
 *
 */
@Mapper
public interface DVersionRecordDao extends BaseMapper<DVersionRecordEntity> {


    List<DVersionRecordEntity> selectListByVersionId(Long versionId);
}
