package org.dsa.modules.sys.entity;

import lombok.Data;

@Data
public class SSOMessage {
    String ErrCode;
    String ErrMsg;
    Boolean Success;
    Object ResData;
}
