package org.dsa.modules.reporter.vo.api;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ClientInfoReVo implements Serializable {

    private String vin;

    private String workshop;

    private String username;

    private String applicationLocale;

    //会话ID
    private String sessionId;

    //会话类型
    private String type;

    private String mainBoardSerial;

    private String diskSerial;

    private String cpuSerial;

    private String macAddr;

    private String pcid;

    private String cpuInfo;

    private List<String> firewalls;

    private String windowsVersion;

    private String windowsId;

    private String systemLocale;

    private String smartLauncherVersion;

    //车企品牌
    private String brand;

    private String antivirInstalled;

    private String antivirFlag;


    private List<String> wlanNetworks;

    private String downloadSpeed;

    //磁盘
    private Long diskTotal;

    //磁盘
    private Long diskUsed;

    //内存使用
    private Long ramUsed;

    //内存
    private Long ramTotal;

    private List<String> localIps;

    private String wlanFlag;

    private String cpuArch;
    //代理
    private List<String> proxies;

    //显示器分辨率
    private String desktopResolution;

    //CPU 处理器
    private String processors;

    private Long workshopId;
}
