-- 目前基于reporting 2.0 client的数据量可以不做分表

-- client_application_installation

create or replace function client_app_install_insert_trigger()
returns trigger as $$
declare
ins_sql text;
begin
ins_sql := 'insert into client_application_installation_' || to_char(new.collect_time, 'yyyy') ||
'(workshop,pcid,type,name,version,install_result,install_time,install_msg,update_oid,tag,collect_date,collect_time) values ' || '('
 || quote_nullable(new.workshop) || ',' || quote_nullable(new.pcid) || ',' || quote_nullable(new.type) || ',' || quote_nullable(new.name) ||  ',' || quote_nullable(new.version) ||
 ',' || quote_nullable(new.install_result) || ',' || quote_nullable(new.install_time) || ',' || quote_nullable(new.install_msg) || ',' || quote_nullable(new.update_oid) || ',' || quote_nullable(new.tag) || ',' || quote_nullable(new.collect_date) || ',' || quote_nullable(new.collect_time)
 || ')';
execute ins_sql;
return null;
end
$$ language plpgsql;

CREATE TRIGGER client_app_install_insert_trigger
  BEFORE INSERT ON client_application_installation
FOR EACH ROW EXECUTE PROCEDURE client_app_install_insert_trigger();


-- client_application_version

create or replace function client_app_version_insert_trigger()
returns trigger as $$
declare
ins_sql text;
begin
ins_sql := 'insert into client_application_version_' || to_char(new.collect_time, 'yyyy') ||
'(workshop,pcid,type,name,version,tag,collect_date,collect_time) values ' || '('
 || quote_nullable(new.workshop) || ',' || quote_nullable(new.pcid) || ',' || quote_nullable(new.type) || ',' || quote_nullable(new.name) ||  ',' || quote_nullable(new.version) ||
  ',' || quote_nullable(new.tag) || ',' || quote_nullable(new.collect_date) || ',' || quote_nullable(new.collect_time)
 || ')';
execute ins_sql;
return null;
end
$$ language plpgsql;

CREATE TRIGGER client_app_version_insert_trigger
  BEFORE INSERT ON client_application_version
FOR EACH ROW EXECUTE PROCEDURE client_app_version_insert_trigger();


-- client_os_protocol_item

create or replace function client_os_protocol_item_insert_trigger()
returns trigger as $$
declare
ins_sql text;
begin
ins_sql := 'insert into client_os_protocol_item_' || to_char(new.collect_time, 'yyyy') ||
'(workshop,pcid,type,severity,timestamp_text,reference_id,message,tag,collect_date,collect_time) values ' || '('
 || quote_nullable(new.workshop) || ',' || quote_nullable(new.pcid) || ',' || quote_nullable(new.type) || ',' || quote_nullable(new.severity) ||  ',' || quote_nullable(new.timestamp_text) || ',' || quote_nullable(new.reference_id) || ',' || quote_nullable(new.message) ||
  ',' || quote_nullable(new.tag) || ',' || quote_nullable(new.collect_date) || ',' || quote_nullable(new.collect_time)
 || ')';
execute ins_sql;
return null;
end
$$ language plpgsql;


CREATE TRIGGER client_os_protocol_item_insert_trigger
  BEFORE INSERT ON client_os_protocol_item
FOR EACH ROW EXECUTE PROCEDURE client_os_protocol_item_insert_trigger();



-- d_action_dtc
create or replace function d_action_dtc_insert_trigger()
returns trigger as $$
declare
ins_sql text;
begin
ins_sql := 'insert into d_action_dtc_' || to_char(new.create_time, 'yyyy') ||
'(action_id,serial,ecu_name,ti,dtc_code,dtc_code_int,description,status,session_id,dtc_code_hex,create_time) values ' || '('
 || quote_nullable(new.action_id) || ',' || quote_nullable(new.serial) || ',' || quote_nullable(new.ecu_name) || ','
 || quote_nullable(new.ti) ||  ',' || quote_nullable(new.dtc_code) || ','
 || quote_nullable(new.dtc_code_int) || ',' || quote_nullable(new.description) || ',' || quote_nullable(new.status) || ','
 || quote_nullable(new.session_id) || ',' || quote_nullable(new.dtc_code_hex) || ',' || quote_nullable(new.create_time) || ')';
execute ins_sql;
return null;
end
$$ language plpgsql;

CREATE TRIGGER d_action_dtc_insert_trigger
  BEFORE INSERT ON d_action_dtc
FOR EACH ROW EXECUTE PROCEDURE d_action_dtc_insert_trigger();



-- d_action_dtc_grid
create or replace function d_action_dtc_grid_insert_trigger()
returns trigger as $$
declare
ins_sql text;
begin
ins_sql := 'insert into d_action_dtc_grid_' || to_char(new.create_time, 'yyyy') ||
'(action_id,dtc_code,ecu_name,name,value,unit,ti,ti_value,dtc_code_int,dtc_code_hex,create_time) values ' || '('
 || quote_nullable(new.action_id) || ',' || quote_nullable(new.dtc_code) || ',' || quote_nullable(new.ecu_name) || ','
 || quote_nullable(new.name) ||  ',' || quote_nullable(new.value) || ','
 || quote_nullable(new.unit) || ',' || quote_nullable(new.ti) || ',' || quote_nullable(new.ti_value) || ','
 || quote_nullable(new.dtc_code_int) || ',' || quote_nullable(new.dtc_code_hex) || ',' || quote_nullable(new.create_time) || ')';
execute ins_sql;
return null;
end
$$ language plpgsql;

CREATE TRIGGER d_action_dtc_grid_insert_trigger
  BEFORE INSERT ON d_action_dtc_grid
FOR EACH ROW EXECUTE PROCEDURE d_action_dtc_grid_insert_trigger();



-- d_action_ecu
create or replace function d_action_ecu_insert_trigger()
returns trigger as $$
declare
ins_sql text;
begin
ins_sql := 'insert into d_action_ecu_' || to_char(new.create_time, 'yyyy') ||
'(action_id,ecu_name,description,status,create_time,end_time) values ' || '('
 || quote_nullable(new.action_id) || ',' || quote_nullable(new.ecu_name) || ',' || quote_nullable(new.description) || ','
 || quote_nullable(new.status) || ',' ||  quote_nullable(new.create_time) || ',' || quote_nullable(new.end_time)  || ')';
execute ins_sql;
return null;
end
$$ language plpgsql;

CREATE TRIGGER d_action_ecu_insert_trigger
  BEFORE INSERT ON d_action_ecu
FOR EACH ROW EXECUTE PROCEDURE d_action_ecu_insert_trigger();


-- d_action_info
create or replace function d_action_info_insert_trigger()
returns trigger as $$
declare
ins_sql text;
begin
ins_sql := 'insert into d_action_info_' || to_char(new.create_time, 'yyyy') ||
  '(action_id,function_code,action_code,description,message,results,mid,timeout,message_en,message_cn,start_time,end_time,title_cn,title_en,create_time) values ' || '('
 || quote_nullable(new.action_id) || ',' || quote_nullable(new.function_code) || ',' || quote_nullable(new.action_code) || ','
 || quote_nullable(new.description) ||  ',' || quote_nullable(new.message) || ','
 || quote_nullable(new.results) || ',' || quote_nullable(new.mid) || ',' || quote_nullable(new.timeout) || ','
 || quote_nullable(new.message_en) || ',' || quote_nullable(new.message_cn) || ','
 || quote_nullable(new.start_time) || ',' || quote_nullable(new.end_time) || ','
   || quote_nullable(new.title_cn) || ',' || quote_nullable(new.title_en) || ',' || quote_nullable(new.create_time) ||
  ')';
execute ins_sql;
return null;
end
$$ language plpgsql;

CREATE TRIGGER d_action_info_insert_trigger
  BEFORE INSERT ON d_action_info
FOR EACH ROW EXECUTE PROCEDURE d_action_info_insert_trigger();



-- d_action_version
create or replace function d_action_version_insert_trigger()
returns trigger as $$
declare
ins_sql text;
begin
ins_sql := 'insert into d_action_version_' || to_char(new.create_time, 'yyyy') ||
'(action_id,serial,ecu_name,ti,semantic,name,did,value,session_id,ti_value,create_time) values ' || '('
 || quote_nullable(new.action_id) || ',' || quote_nullable(new.serial) || ',' || quote_nullable(new.ecu_name) || ','
 || quote_nullable(new.ti) ||  ',' || quote_nullable(new.semantic) || ','
 || quote_nullable(new.name) || ',' || quote_nullable(new.did) || ',' || quote_nullable(new.value) || ','
 || quote_nullable(new.session_id) || ',' || quote_nullable(new.ti_value) || ',' || quote_nullable(new.create_time) || ')';
execute ins_sql;
return null;
end
$$ language plpgsql;

CREATE TRIGGER d_action_version_insert_trigger
  BEFORE INSERT ON d_action_version
FOR EACH ROW EXECUTE PROCEDURE d_action_version_insert_trigger();




-- d_session_action
create or replace function d_session_action_insert_trigger()
returns trigger as $$
declare
ins_sql text;
begin
ins_sql := 'insert into d_session_action_' || to_char(new.create_time, 'yyyy') ||
'(session_id,action_id,create_time) values ' || '('
 || quote_nullable(new.session_id) || ',' || quote_nullable(new.action_id) || ',' || quote_nullable(new.create_time) || ')';
execute ins_sql;
return null;
end
$$ language plpgsql;

CREATE TRIGGER d_session_action_insert_trigger
  BEFORE INSERT ON d_session_action
FOR EACH ROW EXECUTE PROCEDURE d_session_action_insert_trigger();


-- d_session_info
create or replace function d_session_info_insert_trigger()
returns trigger as $$
declare
ins_sql text;
begin
ins_sql := 'insert into d_session_info_' || to_char(new.create_time, 'yyyy') ||
'(session_id,vin,active_user_id,diagnostic_user_id,log_path,log_name,source,hardware_info,start_time,
end_time,active_display_user_name,diagnostic_display_user_name,end_status,workshop,pcid,create_time,update_time) values ' || '('
 || quote_nullable(new.session_id) || ',' || quote_nullable(new.vin) || ',' || quote_nullable(new.active_user_id) || ','
 || quote_nullable(new.diagnostic_user_id) ||  ',' || quote_nullable(new.log_path) || ','
 || quote_nullable(new.log_name) || ',' || quote_nullable(new.source) || ',' || quote_nullable(new.hardware_info) || ','
 || quote_nullable(new.start_time) || ',' || quote_nullable(new.end_time) || ','
 || quote_nullable(new.active_display_user_name) || ',' || quote_nullable(new.diagnostic_display_user_name) || ','
 || quote_nullable(new.end_status) || ',' || quote_nullable(new.workshop) || ','
 || quote_nullable(new.pcid) || ',' || quote_nullable(new.create_time) || ',' || quote_nullable(new.update_time)  || ')';
execute ins_sql;
return null;
end
$$ language plpgsql;

CREATE TRIGGER d_session_info_insert_trigger
  BEFORE INSERT ON d_session_info
FOR EACH ROW EXECUTE PROCEDURE d_session_info_insert_trigger();


