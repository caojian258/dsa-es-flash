package org.dsa.modules.reporter.job;

import com.alibaba.fastjson.JSONObject;
import jakarta.annotation.Resource;
import junit.framework.TestCase;
import org.dsa.common.utils.RedisUtils;
import org.dsa.common.utils.SqliteUtils;
import org.dsa.modules.reporter.dao.*;
import org.dsa.modules.reporter.dto.ClientDependencyDto;
import org.dsa.modules.reporter.entity.*;
import org.dsa.modules.reporter.enums.AppTypeEnum;
import org.dsa.modules.reporter.service.*;
import org.dsa.modules.reporter.upgrade.UpgradeContext;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.print.attribute.HashAttributeSet;
import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SystemDataMinerJobTest extends TestCase {

    @Autowired
    ClientDependenceTreeService treeService;

    @Autowired
    ApplicationHistoryService historyService;

    @Autowired
    UpgradeContext upgradeContext;

    @Autowired
    AppService appService;

    @Autowired
    ClientUpgradeMapper upgradeResultMapper;

    @Autowired
    CurrentSoftwareInfoMapper currentSoftwareInfoMapper;

    @Autowired
    FutureSoftwareInfoMapper futureSoftwareInfoMapper;

    @Autowired
    CurrentComponentInfoMapper currentComponentInfoMapper;

    @Autowired
    FutureComponentInfoMapper futureComponentInfoMapper;

    @Autowired
    AppHistoryMapper appHistoryMapper;

    @Autowired
    ClientUpgradeService clientUpgradeService;

    @Autowired
    SystemService systemService;

    @Resource
    private LocaleService localeService;

    @Test
    public void testTi(){
         String source = "IOCTROL";
         System.out.println(JSONObject.toJSONString(source.split("\\|")));
        String[] sourceArr = source.split("\\|");
        StringBuffer stringBuffer = new StringBuffer("");
        for (String ss: sourceArr ) {
            if(ss.startsWith("{") && ss.endsWith("}") ){
                String raw = ss.replace("{", "").replace("}","");
                stringBuffer.append(raw);
            }else{
                String ti = localeService.translateDescription(ss,"zh-CN", ss);
                stringBuffer.append(ti);
            }
        }

        System.out.println(stringBuffer.toString());
    }
    @Test
    public void testJob(){
        String tag = "1685079664781";

        List<ClientUpgrade> items = clientUpgradeService.queryDataByTag(tag);

        List<Integer> softwareIds = new ArrayList<>();
        List<Integer> componentIds = new ArrayList<>();
        String deviceId = "";

        for (ClientUpgrade record: items){
            deviceId = record.getDeviceId();
            //软件
            if(record.getUpdateTarget()  == 0 ){
                if(!softwareIds.contains(record.getSoftwareId())){
                    softwareIds.add(record.getSoftwareId());
                }
            }else{
                //组件
                if(!componentIds.contains(record.getComponentId())){
                    componentIds.add(record.getComponentId());
                }
            }
        }

        Map<Integer, CurrentSoftwareInfo> currentSoftwareInfoMap = clientUpgradeService.queryCurrentSoftwareInfoByTag(deviceId, tag, softwareIds);
        Map<Integer, FutureSoftwareInfo> futureSoftwareInfos = clientUpgradeService.queryFutureSoftwareInfoByTag(deviceId, tag, softwareIds);

        Map<Integer, CurrentComponentInfo> currentComponentInfoMap = clientUpgradeService.queryCurrentComponentInfoByTag(deviceId, tag, componentIds);
        Map<Integer, FutureComponentInfo> futureComponentInfoMap = clientUpgradeService.queryFutureComponentInfoByTag(deviceId, tag, componentIds);

        historyService.insertHisData(items, currentSoftwareInfoMap, futureSoftwareInfos, currentComponentInfoMap, futureComponentInfoMap);

        List<ClientDependence> dependencyDtos = treeService.queryDependencyByTag(tag);

        if(null != dependencyDtos && dependencyDtos.size()>0){
            treeService.deleteByDeviceId(deviceId);

            //整合数据 从current表拿不到数据的 从future表拿数据 名称也是一样的
            List<ClientDependenceTree> list =  treeService.formatInertData(dependencyDtos, tag);
            treeService.saveBatch(list);
        }

    }

    @Test
    public void testJobHis(){
        String tag = "1685079664781";

        List<ClientUpgrade> records =  upgradeResultMapper.queryDataByTag(tag);
        List<Integer> softwareIds = new ArrayList<>();
        List<Integer> componentIds = new ArrayList<>();
        String deviceId = "";

        for (ClientUpgrade record: records){
            deviceId = record.getDeviceId();
            //软件
            if(record.getUpdateTarget()  == 0 ){
                if(!softwareIds.contains(record.getSoftwareId())){
                    softwareIds.add(record.getSoftwareId());
                }
            }else{
            //组件
                if(!componentIds.contains(record.getComponentId())){
                    componentIds.add(record.getComponentId());
                }
            }
        }

        List<CurrentSoftwareInfo> currentSoftwareInfos = currentSoftwareInfoMapper.querySoftwareInfoByTag(deviceId, tag, softwareIds);
        Map<Integer, CurrentSoftwareInfo> currentSoftwareInfoMap = new HashMap<>();

        for (CurrentSoftwareInfo info: currentSoftwareInfos) {
            currentSoftwareInfoMap.put(info.getSoftwareId(), info);
        }

        List<FutureSoftwareInfo> futureSoftwareInfos = futureSoftwareInfoMapper.querySoftwareInfoByTag(tag, deviceId, softwareIds);
        Map<Integer, FutureSoftwareInfo> futureSoftwareInfoMap = new HashMap<>();

        for (FutureSoftwareInfo info: futureSoftwareInfos) {
            futureSoftwareInfoMap.put(info.getSoftwareId(), info);
        }

        List<CurrentComponentInfo> currentComponentInfos = currentComponentInfoMapper.queryComponentInfoByTag(deviceId, tag, componentIds);
        Map<Integer, CurrentComponentInfo> currentComponentInfoMap = new HashMap<>();

        for (CurrentComponentInfo info: currentComponentInfos){
            currentComponentInfoMap.put(info.getComponentId(), info);
        }

        List<FutureComponentInfo> futureComponentInfos = futureComponentInfoMapper.queryComponentInfoByTag(deviceId, tag, componentIds);
        Map<Integer, FutureComponentInfo> futureComponentInfoMap = new HashMap<>();

        for (FutureComponentInfo info: futureComponentInfos){
            futureComponentInfoMap.put(info.getComponentId(), info);
        }

        for (ClientUpgrade upgrade: records) {

            AppHistory check = appHistoryMapper.queryAppHisByDeviceId(upgrade.getDeviceId(), upgrade.getUpdateId());
            if(null != check){
                continue;
            }

            AppHistory his = new AppHistory();

            his.setTag(upgrade.getTag());
            his.setUserId(0L);
            his.setUsername(upgrade.getUsername());
            his.setWorkshopId(upgrade.getWorkshopId());
            his.setWorkshopName(upgrade.getWorkshopName());
            his.setDeviceId(upgrade.getDeviceId());
            his.setUpdateId(upgrade.getUpdateId());
            his.setStartTime(upgrade.getStartTime());
            his.setEndTime(upgrade.getEndTime());
            his.setCollectDate(upgrade.getCtime());
            his.setStatus(upgrade.getUpdateResult());
            his.setStatusType(upgrade.getInstallAction());

            if(upgrade.getUpdateTarget()  == 0 ){
                his.setAppType(AppTypeEnum.SOFTWARE.getValue());
                his.setAppId(upgrade.getSoftwareId());

                his.setAppVersion(upgrade.getSoftwareTargetVersion());
                his.setAppPrevVersion(upgrade.getSoftwareCurrentVersion());

                his.setVersionNumber(upgrade.getSoftwareCurrentVersionNumber());
                his.setPrevVersionNumber(upgrade.getSoftwareTargetVersionNumber());

                CurrentSoftwareInfo csi = currentSoftwareInfoMap.get(upgrade.getSoftwareId());
                if(null != csi){
                    his.setAppName(csi.getSoftwareName());
                }

                FutureSoftwareInfo fsi = futureSoftwareInfoMap.get(upgrade.getSoftwareId());
                if(null != fsi){
                    his.setExpireDate(fsi.getExpireDate());
                    his.setExpireWarningDays(fsi.getExpireWarningDays());
                    his.setReleaseNotes(fsi.getReleaseNotes());
                }

            }else{
                his.setAppType(AppTypeEnum.COMPONENT.getValue());
                his.setAppId(upgrade.getComponentId());
                his.setAppVersion(upgrade.getComponentTargetVersion());
                his.setAppPrevVersion(upgrade.getComponentCurrentVersion());
                his.setVersionNumber(upgrade.getComponentTargetVersionNumber());
                his.setPrevVersionNumber(upgrade.getComponentCurrentVersionNumber());

                CurrentComponentInfo cci = currentComponentInfoMap.get(upgrade.getComponentId());

                if(null != cci){
                    his.setAppName(cci.getComponentName());
                }
                FutureComponentInfo fci = futureComponentInfoMap.get(upgrade.getComponentId());
                if(null != fci){
                    his.setReleaseNotes(fci.getReleaseNotes());
                }
            }

            appHistoryMapper.insert(his);
        }
    }
    @Test
    public void testSqliteTotal(){
        String path = "C:\\Users\\jian8\\Downloads\\dump\\sys\\sovd-launcher.db";
        try {
            SqliteUtils utils = new SqliteUtils(path);
            String sql = "select count(*) as total from t_softwareback";
            int rr = utils.executeStatsQuery(sql);
            System.out.println(rr);
        }catch (Exception ex){

        }
    }
    @Test
    public void testAppVersion(){
     AppVersion v =  appService.queryDeviceAppVersionByTag("3189de1ff1f8afed0f70e352dfcd2abb", 200,
                "V1.0", "1684480673759");

     System.out.println(JSONObject.toJSONString(v));
    }

    @Test
    public void testVersion(){
        String tag = "1684481611970";
        List<AppHistory> records =  historyService.queryHistoryByTag(tag);
        System.out.println(JSONObject.toJSONString(records));
//        for (AppHistory result: records) {
//            if(result.getStatusType()>0){
//                System.out.println(JSONObject.toJSONString(result));
//                upgradeContext.getInstance(result.getStatusType()).saveHis(result);
//            }
//        }
    }

    @Test
    public void testDependency(){
        String tag = "1684481611970";
        List<ClientDependence> dependencyDtos =  treeService.queryDependencyByTag(tag);

        System.out.println(JSONObject.toJSONString(dependencyDtos));

        List<ClientDependenceTree> tt = treeService.formatInertData(dependencyDtos, tag);

        System.out.println(JSONObject.toJSONString(tt));

        treeService.saveBatch(tt);
    }

    @Test
    public void testOverview(){
       SystemHistory history =  systemService.getOneHistory("1690795503800");
        systemService.saveOverview(history);
    }
}