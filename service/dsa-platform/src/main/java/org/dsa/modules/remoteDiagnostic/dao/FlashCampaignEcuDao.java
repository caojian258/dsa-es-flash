package org.dsa.modules.remoteDiagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.remoteDiagnostic.dto.flash.FlashCampaignEcuDto;
import org.dsa.modules.remoteDiagnostic.entity.FlashCampaignEcuEntity;

import java.util.List;

@Mapper
public interface FlashCampaignEcuDao extends BaseMapper<FlashCampaignEcuEntity> {

    /***
     * 获取ecu版本文件列表
     * @param ecuId
     * @param ecuVersionId
     * @return
     */
    public FlashCampaignEcuDto findEcuFile(@Param("ecuId") Long ecuId, @Param("ecuVersionId") Long ecuVersionId);

    /***
     * 根据campaign id获取列表 ecu版本列表
     * @param ids
     * @return
     */
    public List<FlashCampaignEcuEntity> selectByCampaignIds(@Param("ids") List<Long> ids);


    public List<FlashCampaignEcuEntity> selectByCampaignId(@Param("id") Long id);
}
