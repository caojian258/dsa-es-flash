package org.dsa.modules.client.config;


import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
public class ClientConfiguration {

    @Value("${file.cert.client}")
    private String client; // crt证书地址

    @Value("${file.cert.key}")
    private String key; // crt密钥地址

}
