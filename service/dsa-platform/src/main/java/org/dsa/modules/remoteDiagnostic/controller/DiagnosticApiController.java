package org.dsa.modules.remoteDiagnostic.controller;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.dsa.common.annotation.SysLog;
import org.dsa.common.exception.RRException;
import org.dsa.common.exception.RemoteApiException;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.R;
import org.dsa.common.validator.ValidatorUtils;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.common.validator.group.ReqGroup;
import org.dsa.common.validator.group.UpdateGroup;
import org.dsa.modules.remoteDiagnostic.config.RemoteDiagnosticConfig;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticFileEntity;
import org.dsa.modules.remoteDiagnostic.service.*;
import org.dsa.modules.remoteDiagnostic.utils.ResUtils;
import org.dsa.modules.remoteDiagnostic.vo.Task.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

@Slf4j
@RestController
@RequestMapping("/diagnosticApi")
public class DiagnosticApiController {
    @Autowired
    DiagnosticFunctionService functionService;
    @Autowired
    DiagnosticConditionService conditionService;
    @Autowired
    DiagnosticFileService fileService;
    @Autowired
    DiagnosticTaskService taskService;
    @Autowired
    DiagnosticTaskCampaignService campaignService;
    @Autowired
    DiagnosticConditionCopyService copyService;
    @Autowired
    DiagnosticTaskOverviewService overviewService;
    @Autowired
    DiagnosticTaskDetailService detailService;
    @Autowired
    RemoteDiagnosticConfig remoteDiagnosticConfig;

    /**
     * 2.1.	获取诊断任务
     *
     * @param vo 请求参数
     * @return List<DiagnosticTaskDto> 诊断任务
     */
    @SysLog("3.1获取诊断任务")
    @PostMapping("/getDiagnosticTaskList")
    public R getDiagnosticTaskList(@RequestBody DiagnosticApiReqVo vo) {

        ValidatorUtils.validateEntityByRemote(1, new ArrayList<>(), vo, AddGroup.class);
//        taskService.getDiagnosticTaskList(vo);
        try {
            return R.ok().put("data", campaignService.getDiagnosticTaskList(vo));
        } catch (RemoteApiException e) {
            log.error(e.getMessage(), e);
            return R.error(1, e.getMessage()).put("data", new ArrayList<>());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return R.error(1, "获取任务失败，请联系管理员").put("data", new ArrayList<>());
        }
    }

    /**
     * 2.2.	上传任务执行结果
     *
     * @param vo 请求参数
     * @return 状态码
     */
    @SysLog("3.2上传任务执行结果")
    @PostMapping("/uploadTaskResult")
    public R uploadTaskResult(@RequestBody DiagnosticApiReqVo vo) {

        ValidatorUtils.validateEntity(1, vo, ReqGroup.class);

        try {
            campaignService.uploadTaskResult(vo);
            return R.ok();
        } catch (RemoteApiException e) {
            log.error(e.getMessage(), e);
            return R.error(1, e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return R.error(1, "上传结果失败，请联系管理员");
        }
    }

    /**
     * 2.3 上传任务执行状态
     *
     * @param vo 请求参数
     * @return R
     */
    @SysLog("3.3上传任务执行状态")
    @PostMapping("/uploadTaskStatus")
    public R uploadScriptResult(@RequestBody DiagnosticApiStatusReqVo vo) {

        ValidatorUtils.validateEntity(1, vo, AddGroup.class);

        try {
            campaignService.saveActiveStatus(vo);
        } catch (RemoteApiException e) {
            log.error(e.getMessage(), e);
            return R.error(1, e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return R.error(1, "上传任务执行状态失败，请联系管理员");
        }

        return R.ok();
    }

    /**
     * 2.4.	诊断任务校验
     *
     * @param vo 请求参数
     * @return 状态码
     */
    @SysLog("3.4诊断任务校验")
    @PostMapping("/checkTask")
    public R checkTask(@RequestBody DiagnosticApiReqVo vo) {

        ValidatorUtils.validateEntityByRemote(1, ResUtils.getStatus(), vo, UpdateGroup.class);

        try {
            return R.ok().put("data", campaignService.checkTask(vo));
        } catch (RemoteApiException e) {
            log.error(e.getMessage(), e);
            return R.error(1, e.getMessage()).put("data", ResUtils.getStatus());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return R.error(1, "校验失败，请联系管理员").put("data", ResUtils.getStatus());
        }
    }


    /**
     * 2.5.	下载诊断文件
     *
     * @param vo 请求参数
     *           返回io流
     */
    @SysLog("3.5下载诊断文件")
    @PostMapping("/downloadOtxFile")
    public void downloadOtxFile(@RequestBody DiagnosticDownloadReqVo vo, HttpServletRequest request, HttpServletResponse response) {

        ValidatorUtils.validateEntity(1, vo, ReqGroup.class);

        DiagnosticFileEntity file = fileService.getInfo(vo.getMd5());
        if (null == file) {
            return;
        }
        String range = request.getHeader("range");
        File downloadFile = new File(file.getFilePath());
        if (!downloadFile.exists()) {
            throw new RRException(Constant.Msg.FILE_NOT_EXIST);
        }

        long begin = 0L;
        long end = downloadFile.length();

        if (StringUtils.isNotBlank(range)) {
            String[] strings = range.split("-");
            begin = Long.parseLong(strings[0]);
            end = Long.parseLong(strings[1]);
        }
//        System.out.println(range);
        if (end <= 0 || begin < 0 || begin > downloadFile.length()) {
            log.error("下载OTX文件，断点下载参数校验失败 ---range:" + range + "--- fileLength:" + downloadFile.length());
            return;
        }
        if (end > downloadFile.length() || (begin + end) > downloadFile.length()) {
            end = downloadFile.length() - begin;
        }

//        FileInputStream ips = null;
//        OutputStream ops = null;
        FileOutputStream out = null;
        FileInputStream is = null;
        File file1 = null;
        FileChannel inChannel = null;
        FileChannel outChannel = null;

        try (FileInputStream ips = new FileInputStream(downloadFile); OutputStream ops = response.getOutputStream()) {

            inChannel = ips.getChannel();
            File dir = new File(remoteDiagnosticConfig.getTemplate());
            if (!dir.exists()) {
                boolean mkdir = dir.mkdir();
            }
            String temp = remoteDiagnosticConfig.getTemplate() + File.separator + System.currentTimeMillis();
            file1 = new File(temp);
            if (!file1.exists()) {
                boolean newFile = file1.createNewFile();
            }
            out = new FileOutputStream(file1);
            outChannel = out.getChannel();
            inChannel.transferTo(begin, end, outChannel);

            is = new FileInputStream(temp);

            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(file.getFileName(), "UTF-8"));
            IOUtils.copy(is, ops);
            ops.flush();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } finally {
            try {
                if (inChannel != null) {
                    inChannel.close();
                }
                if (outChannel != null) {
                    outChannel.close();
                }
                if (out != null) {
                    out.close();
                }
                if (is != null) {
                    is.close();
                }
                if (file1 != null && file1.exists()) {
                    boolean delete = file1.delete();
                }
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }


//            InputStream ips = null;
//            OutputStream ops = null;
//            try {
//                ips = new FileInputStream(downloadFile);
//                ops = response.getOutputStream();
//                response.setContentType("application/octet-stream");
//                response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(file.getFileName(), "UTF-8"));
//                IOUtils.copy(ips, ops);
//                ips.close();
//                ops.flush();
//
//                overviewService.updateTask(vo.getSessionId(), vo.getVin());
//
//            } catch (IOException e) {
//
//            } finally {
//                try {
//                    if (ips != null) {
//                        ips.close();
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                try {
//                    if (ops != null) {
//                        ops.flush();
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }

    }


    /**
     * 2.6.	上传日志文件
     *
     * @param sessionId 会话id
     * @param vin       vin
     * @param md5       日志md5
     * @param file      file
     * @return R
     */
    @SysLog("3.6上传日志文件")
    @PostMapping("/uploadTaskLog/{sessionId}/{vin}/{md5}")
    public R uploadOtxFile(@PathVariable("sessionId") String sessionId, @PathVariable("vin") String vin
            , @PathVariable("md5") String md5, @RequestBody MultipartFile file, HttpServletResponse response) {

        DiagnosticApiLoggerReqVo vo = new DiagnosticApiLoggerReqVo();
        vo.setVin(vin);
        vo.setSessionId(sessionId);
        vo.setMd5(md5);
        vo.setFile(file);

        ValidatorUtils.validateEntity(1, vo, ReqGroup.class);
        try {
            campaignService.saveLogger(vo);
            return R.ok();
        } catch (RemoteApiException e) {
            log.error(e.getMessage(), e);
            return R.error(1, e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return R.error(1, "上传文件失败，请联系管理员");
        }
    }

    /**
     * 3.7 查询车辆状态
     *
     * @param vo 请求对象
     * @return R
     */
    @SysLog("3.7查询车辆状态")
    @PostMapping("/selectVehicleCode")
    public R uploadOtxFile(@RequestBody DiagnosticApiStatusReqVo vo) {

        ValidatorUtils.validateEntityByRemote(1, ResUtils.getStatus(), vo, UpdateGroup.class);
        try {
            return R.ok().put("data", campaignService.selectVehicleCode(vo));
        } catch (RemoteApiException e) {
            log.error(e.getMessage(), e);
            return R.error(1, e.getMessage()).put("data", ResUtils.getStatus());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return R.error(1, "校验失败，请联系管理员").put("data", ResUtils.getStatus());
        }
    }


    //    @SysLog("3.9通过md5获取文件大小")
//    @RequestMapping("/otxFileSize")
//    public Map<String, Long> otxFileSize(@RequestBody DiagnosticApiLoggerReqVo vo) {
//        HashMap<String, Long> result = new HashMap<String, Long>();
//        try {
//            if (StringUtils.isBlank(vo.getMd5())) {
//                result.put("size", -1L);
//            } else {
//                result.put("size", releaseService.selectOtxFileSize(vo.getMd5()));
//            }
//        } catch (Exception e) {
//            log.error(e.getMessage(), e);
//            result.put("size", -1L);
//        }
//        return result;
//    }
}
