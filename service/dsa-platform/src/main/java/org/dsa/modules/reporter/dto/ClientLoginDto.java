package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.util.Date;

@Data
public class ClientLoginDto {

    private String pcid;

    private String workshop;

    private Long workshopId;

    private String username;

    private Date time;

    private String ip;
}
