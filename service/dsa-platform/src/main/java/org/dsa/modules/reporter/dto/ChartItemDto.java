package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class ChartItemDto {

    private String name;

    private Object value;

    private String stack;

    private String type;

    private List<Map<String,Object>> children;

    private long[] data;

    private String barWidth;

    private String barMaxWidth;
}
