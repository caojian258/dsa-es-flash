package org.dsa.modules.reporter.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class SuggestionVo implements Serializable {

    private String vin;

    private String ecu;

    private String dtc;

    private String startTime;

    private String endTime;
}
