package org.dsa.modules.onlineUpdate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.dsa.modules.onlineUpdate.entity.CompVGroupEntity;
import org.dsa.modules.onlineUpdate.entity.ComponentVersionEntity;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class ComponentVersionDto extends ComponentVersionEntity {

    /**
     * 关联的版本
     */
    private List<CompVGroupEntity> compVGroup;

    /**
     * 文件名
     */
    public String fileName;




}
