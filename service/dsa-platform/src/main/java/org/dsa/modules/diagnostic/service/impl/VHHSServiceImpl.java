package org.dsa.modules.diagnostic.service.impl;

import ch.qos.logback.classic.Logger;
import com.alibaba.fastjson.JSONObject;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.diagnostic.dao.DActionDtcDao;
import org.dsa.modules.diagnostic.dao.DActionEcuDao;
import org.dsa.modules.diagnostic.dao.DActionInfoDao;
import org.dsa.modules.diagnostic.dao.DSessionActionDao;
import org.dsa.modules.diagnostic.entity.DActionDtcEntity;
import org.dsa.modules.diagnostic.entity.DActionEcuEntity;
import org.dsa.modules.diagnostic.entity.mqtt.callback.MqBodyDataDTCVO;
import org.dsa.modules.diagnostic.entity.mqtt.callback.MqBodyDataVHHSVO;
import org.dsa.modules.diagnostic.entity.mqtt.callback.MqEntity;
import org.dsa.modules.diagnostic.service.DiagnosticMqService;
import org.dsa.modules.diagnostic.util.MqAsynchronousData;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service("vHHSService")
public class VHHSServiceImpl implements DiagnosticMqService {
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass());

    @Resource
    private RedisUtils redisUtils;
    @Resource
    private DSessionActionDao dSessionActionDao;
    @Resource
    private DActionInfoDao dActionInfoDao;
    @Resource
    private DActionDtcDao dActionDtcDao;
    @Resource
    private DActionEcuDao dActionEcuDao;
    @Resource
    private MqAsynchronousData<MqBodyDataVHHSVO> mqAsynchronousData;

    @Override
    public void onMessage(MqEntity mqEntity) {
        logger.info("VHHS MQ Call back ---- begin");
        logger.info("VHHS MQ Call back ---- actionId:{}",mqEntity.getHeader().getRid());
        try {
            mqAsynchronousData.handle(Constant.Function.VHSS.getFunctionCode(), mqEntity, new MqBodyDataVHHSVO());
            //保存诊断数据
            saveActionDtc(mqEntity);
        }catch (Exception e){
            logger.error(e.getMessage());
        }

        logger.info("VHHS MQ Call back ---- end");
    }

    /**
    *保存诊断故障码信息
    */
    private void saveActionDtc(MqEntity mqEntity) {
        //判断命令执行是否成功
        if(Constant.ACK.NAK.getValue() != mqEntity.getHeader().getAck()){
            MqBodyDataVHHSVO vo = JSONObject.parseObject( JSONObject.toJSONString(mqEntity.getBody().getData()),MqBodyDataVHHSVO.class);
            //故障信息汇总
            if(vo!= null ){
                //修改诊断执行的ecu状态
                DActionEcuEntity dActionEcuEntity = new DActionEcuEntity();
                dActionEcuEntity.setActionId(mqEntity.getHeader().getRid());
                dActionEcuEntity.setEcuName(vo.getEcuName());
                dActionEcuEntity.setDescription(String.valueOf(vo.getDtcNumber()));
                dActionEcuEntity.setEndTime(new Date());
                dActionEcuEntity.setStatus(vo.getOnlineStatus());
                dActionEcuDao.updateByActionAndEcuName(dActionEcuEntity);
                //详细故障码信息
                List<MqBodyDataDTCVO> dtcVOs = vo.getDtcList();
                if(dtcVOs != null && dtcVOs.size() >0) {
                    String sessionId = dSessionActionDao.selectSessionIdByActionId(mqEntity.getHeader().getRid());
                    for (MqBodyDataDTCVO dtcVO : dtcVOs) {
                        DActionDtcEntity dActionDtcEntity = new DActionDtcEntity();
                        dActionDtcEntity.setActionId(mqEntity.getHeader().getRid());
                        dActionDtcEntity.setEcuName(dtcVO.getEcuName());
                        dActionDtcEntity.setDtcCode(dtcVO.getDtcCode());
                        dActionDtcEntity.setDtcCodeInt(dtcVO.getDtcCode_Int());
                        dActionDtcEntity.setDtcCodeHex(dtcVO.getDtcCodeHex());
                        dActionDtcEntity.setTi(dtcVO.getTi());
                        dActionDtcEntity.setDescription(dtcVO.getDescription());
                        dActionDtcEntity.setStatus(dtcVO.getTiStatus());
                        dActionDtcEntity.setSessionId(sessionId);
                        //保存数据 入库
                        dActionDtcDao.insert(dActionDtcEntity);
                    }
                }
            }
        }
    }
}