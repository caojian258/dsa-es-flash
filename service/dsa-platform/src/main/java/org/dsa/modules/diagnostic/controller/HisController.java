package org.dsa.modules.diagnostic.controller;

import org.apache.commons.io.IOUtils;
import org.dsa.common.utils.PageUtils;
import org.dsa.common.utils.R;
import org.dsa.modules.diagnostic.entity.AnalysisEntity;
import org.dsa.modules.diagnostic.entity.DSessionReportEntity;
import org.dsa.modules.diagnostic.entity.OptionEntity;
import org.dsa.modules.diagnostic.service.HisService;
import org.dsa.modules.sys.controller.AbstractController;
import org.dsa.modules.sys.entity.FileEntity;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

/**
 * 历史诊断功能
 *
 */
@RestController
@RequestMapping("his")
public class HisController extends AbstractController {
	@Resource
	private HisService hisService;

	@Value("${file.sessionLog.path}")
	private String sessionLogPath;

	/**
	 * 历史诊断会话查询
	 */
	@RequestMapping("/sessionList")
	public R getSessionList(@RequestBody Map<String, Object> params, HttpServletRequest httpRequest){
		PageUtils page = hisService.queryPage(params,httpRequest);
		return R.ok().put("page", page);
	}

	/**
	 * 诊断结果查询
	 */
	@RequestMapping("/sessionReport/{id}")
	public R sessionReport(@PathVariable Long id ,HttpServletRequest httpRequest){
		logger.info("查询诊断报告开始");
		DSessionReportEntity dSessionInfoEntity = hisService.getSessionInfoById(id,httpRequest);
		return R.ok().put("data",dSessionInfoEntity);
	}

	/**
	 * 历史诊断会话 - 日志下载
	 */
	@RequestMapping("/downloadSessionLog")
	public void downloadSessionLog(@RequestBody FileEntity fileEntity , HttpServletRequest req, HttpServletResponse response){
		InputStream ips = null;
		OutputStream ops = null;
		try {
			ips =  new FileInputStream(new File(fileEntity.getUrl()));
			ops = response.getOutputStream();
			response.setContentType("application/x-download");
			response.setHeader("Content-Disposition", "attachment;filename=utf-8'zh_cn'" + URLEncoder.encode(fileEntity.getName(), "UTF-8"));
			IOUtils.copy(ips, ops);
			ips.close();
			ops.flush();

		} catch (IOException e) {
			logger.error("下载文件出错："+e);
		} finally {
			try {
				if(ips != null ) {
					ips.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				if(ops != null) {
					ops.flush();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 故障码统计历表
	 */
	@RequestMapping("/dicAnalysisList")
	public R dicAnalysisList(@RequestBody Map<String, Object> params){
		logger.info("dicAnalysisList in {}",params);
		PageUtils page = hisService.queryDicAnalysisPage(params);
		return R.ok().put("page", page);
	}

	/**
	 * 故障码统计 饼图
	 */
	@RequestMapping("/dicAnalysisPir")
	public R dicAnalysisPir(@RequestBody AnalysisEntity analysisEntity ){
		logger.info("dicAnalysisPir in {}",analysisEntity);
		Map<String,Object> map = hisService.dicAnalysisPir(analysisEntity);
		return R.ok().put("title", map.get("title")).put("data",map.get("data"));
	}

	/**
	 * 故障码统计 柱状图
	 */
	@RequestMapping("/dicAnalysisBar")
	public R dicAnalysisBar(@RequestBody AnalysisEntity analysisEntity){
		logger.info("dicAnalysisBar in {}",analysisEntity);
		Map<String,Object> map = hisService.dicAnalysisBar(analysisEntity);
		return R.ok().put("dtcCode", map.get("dtcCode")).put("dtcMonth", map.get("dtcMonth")).put("dtcData",map.get("dtcData"));
	}


	/**
	 * 诊断功能分析 分页
	 */
	@RequestMapping("/reqAnalysisList")
	public R reqAnalysisList(@RequestBody Map<String, Object> params){
		logger.info("reqAnalysisList in {}",params);
		PageUtils page = hisService.queryReqAnalysisPage(params);
		return R.ok().put("page", page);
	}

	/**
	 * 诊断功能分析 饼图
	 */
	@RequestMapping("/reqAnalysisPir")
	public R reqAnalysisPir(@RequestBody AnalysisEntity analysisEntity ){
		logger.info("reqAnalysisPir in {}",analysisEntity);
		Map<String,Object> map = hisService.reqAnalysisPir(analysisEntity);
		return R.ok().put("title", map.get("title")).put("data",map.get("data"));
	}

	/**
	 * 诊断功能分析 柱状图
	 */
	@RequestMapping("/reqAnalysisBar")
	public R reqAnalysisBar(@RequestBody AnalysisEntity analysisEntity){
		logger.info("reqAnalysisBar in {}",analysisEntity);
		Map<String,Object> map = hisService.reqAnalysisBar(analysisEntity);
		return R.ok().put("dtcCode", map.get("dtcCode")).put("dtcMonth", map.get("dtcMonth")).put("dtcData",map.get("dtcData"));
	}

	/**
	 * 诊断功能选择
	 */
	@RequestMapping("/getFunctionList")
	public R getFunctionList(){
		logger.info("getFunctionList begin");
		List<OptionEntity> list = hisService.getFunctionList();

		logger.info("getFunctionList end");
		return R.ok().put("data", list);
	}

	/**
	 * 查询故障码所在的ecu集合
	 */
	@PostMapping("/getEcuSet")
	public R getEcuSet(@RequestBody  Map<String, Object> params){
		List<Map<String,String>> ecuSet = hisService.getEcuList(params);
		return R.ok().put("ecuSet",ecuSet);
	}

	/**
	 * dtcInEcu列表
	 */
	@PostMapping("/dtcInEcuPage")
	public R page(@RequestBody  Map<String, Object> params){
		List<Map<String,String>> dtcList = hisService.queryDtcInEcuList(params);
		return R.ok().put("dtcList",dtcList);
	}

	/**
	 * dtcInEcu列表
	 */
	@RequestMapping("/getEcus")
	public R getEcus(){
		return R.ok().put("data",hisService.getEcus());
	}

}
