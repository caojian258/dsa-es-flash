package org.dsa.modules.reporter.constant;

public class EcuVersionConstant {

    //车辆大版本
    public final static String VehicleVersion = "PA211876846";

    //供应商编号
    public final static String partCode = "";

    //零件号
    public final static String partNo = "PA1987068246";

    //硬件版本号
    public final static String hardwareVersion = "PA962352631";

    //软件号
    public final static String softwareVersion = "PA1678812340";

    //boot号
    public final static String bootNo = "PA1483826555";

    //vin
    public final static String vin = "PA757368270";
}
