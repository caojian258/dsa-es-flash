package org.dsa.modules.remoteDiagnostic.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.common.utils.R;
import org.dsa.modules.remoteDiagnostic.dto.EcuDto;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuEntity;
import org.dsa.modules.remoteDiagnostic.vo.CheckNameReqVo;
import org.dsa.modules.remoteDiagnostic.vo.ECUFillingCertificateReqVo;
import org.dsa.modules.remoteDiagnostic.vo.EcuVersion.EcuVersionEcuReqVo;
import org.dsa.modules.remoteDiagnostic.vo.PublicPageReqVo;
import org.dsa.modules.remoteDiagnostic.vo.VehicleEcuPageReVo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.List;
import java.util.Map;

public interface RemoteVehicleEcuService {

    Page<RemoteVehicleEcuEntity> selectPage(VehicleEcuPageReVo vo);

    RemoteVehicleEcuEntity getInfo(Long id);

    void saveEcu(RemoteVehicleEcuEntity ecu) throws IOException;

    void updateEcu(RemoteVehicleEcuEntity ecu) throws IOException;

    // 获取ecu，根据车辆平台id
    Map<String, Object> getEcus(PublicPageReqVo vo);

    List<RemoteVehicleEcuEntity> getEcus(EcuVersionEcuReqVo vo);
    // 获取所有的ecu且状态为可使用
    List<RemoteVehicleEcuEntity> getEcus();

    List<RemoteVehicleEcuEntity> getNoIdEcus(Long id,Long versionId);

    // 检查ecu是否重复
    Boolean check(CheckNameReqVo vo);

    //保存ECU业务关联
    void analyzingEcu(List<EcuDto> dto);

    Map<String, Object> getECUFillingCertificate(ECUFillingCertificateReqVo vo) throws Exception;

}
