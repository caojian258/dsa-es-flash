package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.entity.VehicleVersionHistory;

import java.util.List;

@Mapper
public interface VehicleVersionHistoryMapper extends BaseMapper<VehicleVersionHistory> {

    List<VehicleVersionHistory> queryByVin(@Param("vin") String vin, @Param("timestamp") String timestamp);

    void inserts(@Param("histories") List<VehicleVersionHistory> histories);

}
