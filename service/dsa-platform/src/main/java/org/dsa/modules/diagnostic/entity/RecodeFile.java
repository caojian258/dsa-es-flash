package org.dsa.modules.diagnostic.entity;


import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class RecodeFile {
    MultipartFile file;
    String fileMd5;
    String systemId;
    String requestUser;
    String requestTime;
    String sign;

}
