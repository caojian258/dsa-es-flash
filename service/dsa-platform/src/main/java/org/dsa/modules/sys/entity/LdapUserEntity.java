package org.dsa.modules.sys.entity;

import lombok.Data;
import lombok.ToString;
import org.springframework.ldap.odm.annotations.Attribute;
import org.springframework.ldap.odm.annotations.Entry;

import java.util.Date;

@Data
@ToString
@Entry(objectClasses = {"inceptioDealerPerson"}, base = "cn=appuser,dc=INCEPTIO,dc=COM")
public class LdapUserEntity {
    /**
     * 主键
     */
    @Attribute
    String cn;
    /**
     * 主键
     */
    @Attribute
    String sn;
    /**
     * 主键
     */
    @Attribute
    String uid;

    /**
     * 状态
     */
    @Attribute
    String status;

    /**
     * 联系电话
     */
    @Attribute
    String Telephone;

    /**
     * 帐户创建时间
     */
    @Attribute
    String accountCreateDate;

    /**
     * 经销商id
     */
    @Attribute
    String ou;
}
