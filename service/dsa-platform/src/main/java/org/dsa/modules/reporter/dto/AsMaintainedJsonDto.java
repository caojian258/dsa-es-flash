package org.dsa.modules.reporter.dto;

import lombok.Data;
import lombok.ToString;

import java.util.List;
import java.util.Map;

@Data
@ToString
public class AsMaintainedJsonDto {
    private String mode;
    private String vin; // 车架号
    private String make;// 品牌
    private String model;// 车型
    private String year;  // 年款牛创置空
    private String trimlevel;  // 牛创置空
    private String sourceVersion; // 刷写前大版本
    private String targetVersion; // 刷写后大版本
    private String sessionId; // 会话ID
    private String updateTime; //升级时间 格式:毫秒值

    List<Map<String, String>> ecus;
}
