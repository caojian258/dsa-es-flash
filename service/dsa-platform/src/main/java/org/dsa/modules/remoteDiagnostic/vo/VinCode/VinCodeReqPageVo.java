package org.dsa.modules.remoteDiagnostic.vo.VinCode;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.modules.remoteDiagnostic.pojo.PageParam;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class VinCodeReqPageVo extends PageParam {

    private String vin;

    private String key;

    private String regular;

    private Long tag;

    private List<Long> tags;

    private Long id;
}
