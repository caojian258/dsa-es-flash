package org.dsa.modules.reporter.config;

import lombok.Data;

import java.io.Serializable;

@Data
public class ClientAppConfig implements Serializable {

    private String type;

    private Integer id;

    private String name;
}
