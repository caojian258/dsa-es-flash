package org.dsa.modules.remoteDiagnostic.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;
import org.dsa.modules.remoteDiagnostic.dto.api.DiagnosticFunctionDto;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticFunctionEntity;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskEcuEntity;

import java.util.List;

@Mapper
public interface DiagnosticTaskEcuDao extends BaseMapper<DiagnosticTaskEcuEntity> {

    void insertEcu(@Param("taskId") Long id, @Param("funId") Long funId, @Param("ecu") List<Long> ecu);

    void insertGroupAndEcu(@Param("taskId") Long id, @Param("funId") Long funId, @Param("ecu") List<DiagnosticTaskEcuEntity> ecu);

    @Insert("insert into r_task_ecu (task_id,fun_id) values (#{taskId},#{funId}) " )
    void insertFun(@Param("taskId") Long id, @Param("funId") Long funId);

//    @Insert("<script>"
//            + " insert into r_task_ecu (task_id,fun_id) values "
//            + " <foreach collection='fun' item='item' separator=','> "
//            + " (#{taskId},#{item}) "
//            + " </foreach>"
//            + " </script>")
//    void insertFun(@Param("taskId") Long id, @Param("fun") List<DiagnosticFunctionEntity> fun);

//    @Select("select i.ecu_group_id from r_task_ecu i where i.task_id = #{taskId} and i.ecu_group_id is not null")
//    List<Long> selectEcu(@Param("taskId") Long id);
//
//    @Select("select i.fun_id from r_task_ecu i where i.task_id = #{taskId} and i.fun_id is not null")
//    List<Long> selectFun(@Param("taskId") Long id);

//    @Select("select distinct j.fun_val from r_task_ecu i left join r_diag_fun j on i.fun_id = j.id where j.id is not null and i.fun_id is not null and i.task_id = #{taskId} ")
//    List<DiagnosticFunctionDto> selectFunName(@Param("taskId") Long id);
//
//    @Select("select distinct j.group_name from r_task_ecu i left join r_vehicle_ecu_group j on i.ecu_group_id = j.id where j.id is not null and i.ecu_group_id is not null and i.task_id = #{taskId} ")
//    List<String> selectGroupName(@Param("taskId") Long id);

    @Select(" select j.fun_val,i.task_id as taskId,i.fun_id as funId from (select i.task_id,i.fun_id from r_task_ecu i where i.task_id = #{taskId} group by i.task_id,i.fun_id) i inner join r_diag_fun j on i.fun_id = j.id ")
    @Results({
            @Result(property = "ecuList", column = "{taskId=taskId,funId=funId}",
                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.DiagnosticTaskEcuDao.selectEcuGroupNames")),
    })
    List<DiagnosticFunctionDto> selectFunListByApi(@Param("taskId") Long id);

    @Select(" select j.fun_name,j.id,j.fun_val,i.task_id as taskId,i.fun_id as funId from (select i.task_id,i.fun_id from r_task_ecu i where i.task_id = #{taskId} group by i.task_id,i.fun_id) i inner join r_diag_fun j on i.fun_id = j.id ")
    @Results({
            @Result(property = "ecuList", column = "{taskId=taskId,funId=funId}",
                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.DiagnosticTaskEcuDao.selectEcuGroupAndEcu")),
    })
    List<DiagnosticFunctionEntity> selectFunListByTask(@Param("taskId") Long id);

    @Select("<script>"
            + " select j.fun_name,j.id,j.fun_val,i.task_id as taskId,i.fun_id as funId from (select * from r_diag_fun j "
            + "    <if test='ids!=null and ids.size!=0 '>\n" +
            "            where j.id in\n" +
            "            <foreach collection='ids' item='item' open=\"(\" separator=\",\" close=\")\">\n" +
            "                #{item}\n" +
            "            </foreach>\n" +
            "        </if>) j "
            + " left join   (select i.task_id,i.fun_id from r_task_ecu i where i.task_id = #{taskId} group by i.task_id,i.fun_id) i  on i.fun_id = j.id "
            + " </script>")
    @Results({
            @Result(property = "ecuList", column = "{taskId=taskId,funId=funId}",
                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.DiagnosticTaskEcuDao.selectEcuGroupAndEcu")),
    })
    List<DiagnosticFunctionEntity> selectFunList(@Param("taskId") Long id, @Param("ids") List<Long> ids);

    @Select(" select i.ecu_group_id,i.ecu_id from r_task_ecu i where i.task_id = #{taskId} and i.fun_id = #{funId} and i.ecu_group_id is not null ")
    List<DiagnosticTaskEcuEntity> selectEcuGroupAndEcu(@Param("taskId") Long taskId, @Param("funId") Long funId);

//    @Select(" select j.group_name from r_task_ecu i inner join r_vehicle_ecu_group j on i.ecu_group_id = j.id and  i.task_id = #{taskId} and i.fun_id = #{funId}  group by j.group_name ")
    List<String> selectEcuGroupNames(@Param("taskId") Long taskId, @Param("funId") Long funId);

}
