package org.dsa.modules.diagnostic.service;


import org.dsa.common.utils.PageUtils;
import org.dsa.modules.diagnostic.dto.SessionActionDto;
import org.dsa.modules.diagnostic.entity.AnalysisEntity;
import org.dsa.modules.diagnostic.entity.DSessionReportEntity;
import org.dsa.modules.diagnostic.entity.OptionEntity;

import jakarta.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 会话功能
 *
 */
public interface HisService {

    /**
     * 历史诊断会话查询
     */
    PageUtils queryPage(Map<String, Object> params,HttpServletRequest httpRequest);


    /**
     * 诊断结果查询
     */
    DSessionReportEntity getSessionInfoById(Long id, HttpServletRequest httpRequest);

    /**
     * 故障码统计列表
     */
    PageUtils queryDicAnalysisPage(Map<String, Object> params);

    /**
     * 故障码统计 饼图
     */
    Map<String, Object> dicAnalysisPir(AnalysisEntity analysisEntity);

    /**
     * 故障码统计 柱状图
     */
    Map<String, Object> dicAnalysisBar(AnalysisEntity analysisEntity);

    /**
     * 诊断功能分析 分页
     */
    PageUtils queryReqAnalysisPage(Map<String, Object> params);
    /**
     * 诊断功能分析 饼图
     */
    Map<String, Object> reqAnalysisPir(AnalysisEntity analysisEntity);
    /**
     * 诊断功能分析 柱状图
     */
    Map<String, Object> reqAnalysisBar(AnalysisEntity analysisEntity);
    /**
     * 诊断功能  select 数据
     */
    List<OptionEntity> getFunctionList();

    /**
     * 查询故障码所在的ecu集合
     */
    List<Map<String,String>> getEcuList(Map<String, Object> params);

    /**
     * 故障码统计列表
     */
    List<Map<String,String>> queryDtcInEcuList(Map<String, Object> params);

    /**
     * 根据车辆和action 获取列表
     * @param vin
     * @param action
     * @param startTime
     * @return
     */
    List<SessionActionDto> queryVehicleSession(String vin, String startTime, String function, String action);

    List<String> getEcus();

}

