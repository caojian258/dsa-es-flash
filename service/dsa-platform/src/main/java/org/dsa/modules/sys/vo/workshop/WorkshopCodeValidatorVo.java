package org.dsa.modules.sys.vo.workshop;

import lombok.Data;

import jakarta.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class WorkshopCodeValidatorVo implements Serializable {

    @NotNull(message = "Workshop ID Not NUll")
    private Long workshopId;

    @NotNull(message = "Workshop Code Not NUll")
    private String code;
}
