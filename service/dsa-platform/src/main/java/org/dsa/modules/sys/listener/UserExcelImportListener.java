package org.dsa.modules.sys.listener;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.metadata.data.ReadCellData;
import com.alibaba.excel.util.ConverterUtils;
import com.alibaba.excel.util.ListUtils;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import jakarta.annotation.Nullable;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.dsa.common.exception.RRException;
import org.dsa.modules.sys.dao.SysUserDao;
import org.dsa.modules.sys.dao.SysWorkshopsDao;
import org.dsa.modules.sys.entity.SysUserEntity;
import org.dsa.modules.sys.entity.SysWorkshopsEntity;
import org.dsa.modules.sys.utils.ShiroUtils;
import org.dsa.modules.sys.utils.SpringContextHolder;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Slf4j
public class UserExcelImportListener extends AnalysisEventListener<Map<Integer, String>> {

    /**
     * 每次批量保存数据量
     */
    private static final int BATCH_COUNT = 200;
    /**
     * 每次批量保存的list
     */
    private List<Map<Integer, String>> cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
    private Map<String, String> errorDataMap = new HashMap<>();

    @Override
    public void invokeHead(Map<Integer, ReadCellData<?>> headMap, AnalysisContext context) {
        log.info("解析到一条头数据:{}", JSON.toJSONString(headMap));
        Map<Integer, String> headsMap = ConverterUtils.convertToStringMap(headMap, context);
        //表头校验
        if (CollectionUtils.isEmpty(headsMap)) {
            throw new RRException("excel表头设置异常");
        }
        if (!Objects.equals(headsMap.get(0), "*账户名") || !Objects.equals(headsMap.get(1), "*用户名") || !Objects.equals(headsMap.get(2), "英文名") || !Objects.equals(headsMap.get(3), "*所属经销商")
                || !Objects.equals(headsMap.get(4), "*密码") || !Objects.equals(headsMap.get(5), "*手机号") || !Objects.equals(headsMap.get(6), "邮箱")){
            throw new RRException("请使用固定模板填写导入用户数据");
        }

    }
    @Override
    public void invoke(Map<Integer, String> data, AnalysisContext context) {
        // 非空判断
        if (Objects.equals(data.get(0), null) || Objects.equals(data.get(1), null) || Objects.equals(data.get(3), null)
            ||Objects.equals(data.get(4), null) || Objects.equals(data.get(5), null)){
            throw new RRException(String.format("第 %d 行用户信息有误！", context.readRowHolder().getRowIndex()));
        }
        cachedDataList.add(data);
        if (cachedDataList.size() >= BATCH_COUNT|| cachedDataList.size()==(context.readSheetHolder().getApproximateTotalRowNumber()-1)) {
            saveUserData();
            cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        if (errorDataMap.isEmpty()){
            log.info("用户数据导入完成！");
        }else{
            throw new RRException("批量导入用户数据异常: "+errorDataMap);
        }
    }

    private void saveUserData() {
        try {
            SysUserDao sysUserDao = SpringContextHolder.getBean(SysUserDao.class);
            SysWorkshopsDao workshopsDao = SpringContextHolder.getBean(SysWorkshopsDao.class);
            Map<String,Long> workshopMap = new HashMap<>();
            List<SysUserEntity> userList = new ArrayList<>();
            AtomicBoolean valid = new AtomicBoolean(true);
            for (Map<Integer, String> record : cachedDataList) {
                SysUserEntity user = setUser(workshopsDao, workshopMap, valid, record);
                if (user == null) break;
                user.setCreateTime(new Date());
                valid.set(validatorUser(user, sysUserDao));
                //sha256加密
                String salt = RandomStringUtils.randomAlphanumeric(20);
                user.setPassword(new Sha256Hash(user.getPassword(), salt).toHex());
                user.setSalt(salt);
                user.setCreateUserId(ShiroUtils.getUserId());
                if (valid.get()) {
                    userList.add(user);
                }
            }
            if (valid.get()){
                sysUserDao.insertBatch(userList);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("批量用户数据异常 {}", e.getMessage());
            throw new RRException("批量导入用户数据异常");
        }
    }

    @Nullable
    private SysUserEntity setUser(SysWorkshopsDao workshopsDao, Map<String, Long> workshopMap, AtomicBoolean valid, Map<Integer, String> record) {
        SysUserEntity user = new SysUserEntity();
        user.setUsername(record.get(0));
        user.setChineseName(record.get(1));
        user.setEnglishName(record.get(2));
        String workshopName = record.get(3);
        user.setWorkshopName(workshopName);
        user.setPassword(record.get(4));
        user.setMobile(record.get(5));
        user.setEmail(record.get(6));
        Long workshopId = workshopMap.get(workshopName);
        if (ObjectUtil.isNull(workshopId)) {
            SysWorkshopsEntity sysWorkshopsEntity = workshopsDao.selectOne(new LambdaQueryWrapper<SysWorkshopsEntity>().eq(SysWorkshopsEntity::getName, workshopName));
            if (BeanUtil.isEmpty(sysWorkshopsEntity)) {
                valid.set(false);
                log.error("用户数据导入失败，经销商信息有误！data:{}", record);
                errorDataMap.put(record.get(0), "经销商有误！");
                return null;
            }
            user.setWorkshopId(workshopId);
            workshopMap.put(workshopName, workshopId);
        } else {
            user.setWorkshopId(workshopId);
        }
        return user;
    }

    private boolean validatorUser(SysUserEntity user, SysUserDao sysUserDao) {
        //用户名校验
        LambdaQueryWrapper<SysUserEntity> query = new LambdaQueryWrapper<SysUserEntity>()
                .eq(SysUserEntity::getUsername, user.getUsername());
        SysUserEntity user1 = sysUserDao.selectOne(query);
        if (user1 != null) {
            //用户名已存在 // 处理错误的数据
            errorDataMap.put(user.getUsername(), "用户数据导入失败，账户名已存在！");
            return false;
//            throw new RRException(Constant.Msg.USERNAME_EXIST_ERROR);
        }
        if (StringUtils.isNotBlank(user.getPassword().trim())){
            // 密码强度校验  长度大于16 大写字母、小写字母、数字、特殊字符
            String expr = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%.^&*?]).{16,}$";
            Pattern p = Pattern.compile(expr);
            Matcher m = p.matcher(user.getPassword());
            if (!m.matches()){
                errorDataMap.put(user.getUsername(), "用户数据导入失败，密码格式错误！");
                return false;
//                throw new RRException(Constant.Msg.PASSWORD_FORMAT_ERROR);
            }
            if (user.getPassword().contains("admin") || user.getPassword().contains("root") ||user.getPassword().contains("password") ||
                      user.getPassword().contains("r00t") ||user.getPassword().contains("weblogic")){
                errorDataMap.put(user.getUsername(), "密码中不得包含系统默认密码admin,password等及其变种！");
            }
        }
        return true;
    }
}
