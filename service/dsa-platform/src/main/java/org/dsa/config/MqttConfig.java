package org.dsa.config;


import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.RedisUtils;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.dsa.modules.diagnostic.entity.mqtt.callback.MqEntity;
import org.dsa.modules.diagnostic.service.DiagnosticMqFactory;
import org.eclipse.paho.client.mqttv3.internal.security.SSLSocketFactoryFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.core.MessageProducer;
import org.springframework.integration.mqtt.core.DefaultMqttPahoClientFactory;
import org.springframework.integration.mqtt.core.MqttPahoClientFactory;
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter;
import org.springframework.integration.mqtt.outbound.MqttPahoMessageHandler;
import org.springframework.integration.mqtt.support.DefaultPahoMessageConverter;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Component;

import jakarta.annotation.Resource;
import javax.net.ssl.*;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Properties;

/**
 * MqttProperties
 * MQTT客户端连接服务器所需设置的一些属性
 **/
@Component
@ConfigurationProperties(prefix = "mqtt-properties")
@Data
public class MqttConfig {

    private String host;
    private String clientId;
    private String username;
    private String password;
    private Integer qos;
    private Boolean cleanSession;
    private Integer connectionTimeout;
    private Integer keepAliveInterval;
    private Boolean automaticReconnect;
    private String topic;
    private String sslProtocol;
    private String keyStore;
    private String keyStorePassword;
    private String keyStoreType;
    private String trustStore;
    private String trustStorePassword;
    private String trustStoreType;

    @Resource
    DiagnosticMqFactory diagnosticMqFactory;
    @Resource
    private RedisUtils redisUtils;
    @Resource
    private MqttSSLSocketFactory mqttSSLSocketFactory;

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    /**
     *   mq连接
     */
    public MqttConnectOptions getMqConnectOptions(){
        MqttConnectOptions options = new MqttConnectOptions();
        Properties sslClientProps = new Properties();
        sslClientProps.setProperty(SSLSocketFactoryFactory.SSLPROTOCOL, sslProtocol);
        sslClientProps.setProperty(SSLSocketFactoryFactory.KEYSTORE, keyStore);
        sslClientProps.setProperty(SSLSocketFactoryFactory.KEYSTOREPWD, keyStorePassword);
        sslClientProps.setProperty(SSLSocketFactoryFactory.KEYSTORETYPE, keyStoreType);
        sslClientProps.setProperty(SSLSocketFactoryFactory.TRUSTSTORE, trustStore);
        sslClientProps.setProperty(SSLSocketFactoryFactory.TRUSTSTOREPWD, trustStorePassword);
        sslClientProps.setProperty(SSLSocketFactoryFactory.TRUSTSTORETYPE, trustStoreType);
        options.setSSLProperties(sslClientProps);
        logger.info("sslClientProps:{}",sslClientProps.toString());
        options.setCleanSession(cleanSession);
        options.setConnectionTimeout(connectionTimeout);
        options.setKeepAliveInterval(keepAliveInterval);
        options.setAutomaticReconnect(automaticReconnect);
        if(StrUtil.isNotBlank(username)&&StrUtil.isNotBlank(password)){
            options.setUserName(username);
            options.setPassword(password.toCharArray());
        }
        options.setServerURIs(new String[]{host});
        return options;
    }

    /**
    *   mq连接池
    */
    @Bean
    public MqttPahoClientFactory mqttPahoClientFactory(){
        DefaultMqttPahoClientFactory factory = new DefaultMqttPahoClientFactory();
        factory.setConnectionOptions(getMqConnectOptions());
        return factory;
    }

    /**
     *   mq 发送消息channel
     */
    @Bean
    public MessageChannel senMessageChannel() {
        DirectChannel directChannel = new DirectChannel();
        return directChannel;
    }


    @Bean
    @ServiceActivator(inputChannel = "senMessageChannel")
    public MessageHandler mqttOutbound(MqttPahoClientFactory factory) {
        //订阅者发布者ID相同会导致掉线重连
        MqttPahoMessageHandler messageHandler =  new MqttPahoMessageHandler(clientId+"_pub", factory);
        messageHandler.setAsync(true);
        messageHandler.setDefaultTopic(topic);
        return messageHandler;
    }

    //接收通道
    @Bean
    public MessageChannel consumerMessageChannel() {
        DirectChannel directChannel = new DirectChannel();
        return directChannel;
    }
    //通过通道获取数据
    @Bean
    @ServiceActivator(inputChannel = "consumerMessageChannel")
    public MessageHandler messageHandler() {
        return new MessageHandler() {
            @Override
            public void handleMessage(Message<?> message) {
                try {
                    String topic = (String)message.getHeaders().get("mqtt_receivedTopic");
                    logger.info("主题：{}，消息接收到的数据：{}",topic, message.getPayload());
                    MqEntity m = JSONObject.parseObject(message.getPayload().toString(),MqEntity.class) ;

                    logger.info("Mqtt Redis key ：{}{}{}",Constant.MQ_TOPIC , m.getHeader().getRid(),m.getHeader().getMid());
                    if(!redisUtils.hasKey(Constant.MQ_TOPIC + m.getHeader().getRid()+m.getHeader().getMid())){
                        redisUtils.set(Constant.MQ_TOPIC + m.getHeader().getRid()+m.getHeader().getMid(),"0",Constant.MQ_TOPIC_EXPIRE);
                        diagnosticMqFactory.getFunction(m.getHeader().getFunctionCode(), m.getHeader().getAction()).onMessage(m);
                    }
                }catch (Exception e){
                    logger.error(e.getMessage());
                }
            }
        };
    }
    //配置client,监听的topic
    @Bean
    public MessageProducer inbound(MqttPahoClientFactory factory, @Qualifier("consumerMessageChannel") MessageChannel channel) {
        MqttPahoMessageDrivenChannelAdapter adapter =  new MqttPahoMessageDrivenChannelAdapter(clientId, factory,Constant.MQ_RESP_TOIPIC);
        adapter.setCompletionTimeout(connectionTimeout);
        adapter.setConverter(new DefaultPahoMessageConverter());
        adapter.setQos(qos);
        adapter.setOutputChannel(channel);
        return adapter;
    }
}
