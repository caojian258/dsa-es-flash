package org.dsa.modules.reporter.event;

import org.springframework.context.ApplicationEvent;

public class SessionEvent extends ApplicationEvent {

    /***
     * 文件名
     */
    private String name;

    /***
     * 批处理的标识
     */
    private String tag;

    /***
     * 文件的路径
     */
    private String path;

    public SessionEvent(Object source) {
        super(source);
    }

    public SessionEvent(Object source, String tag, String path, String name){
        super(source);
        this.tag = tag;
        this.path = path;
        this.name = name;
    }

    public String getName(){
        return name;
    }
    public String getTag(){
        return tag;
    }

    public String getPath(){
        return path;
    }

}
