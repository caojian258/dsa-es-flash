package org.dsa.modules.reporter.service.impl;

import cn.hutool.core.util.StrUtil;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.reporter.service.LocaleService;
import org.dsa.modules.sys.service.SysTranslationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class LocaleServiceImpl implements LocaleService {

    @Autowired
    SysTranslationsService translationsService;

    @Autowired
    RedisUtils redisUtils;

    @Override
    public String translateByTi(String ti, String locale) {
        String cache = redisUtils.get(locale+":"+ti);
        if(StrUtil.isNotBlank(cache)){
            return cache;
        }
        return translationsService.translationTi(ti, locale);
    }

    @Override
    public Map<String, String> translateByTis(List<String> tiList, String locale) {
        return translationsService.translationTis(tiList, locale);
    }

    @Override
    public String translateDescription(String ti, String locale, String defaultDescription) {
        if(StrUtil.isBlank(ti) && StrUtil.isBlank(defaultDescription)){
            return "";
        }
        if(StrUtil.isBlank(ti)){
            return defaultDescription;
        }
        String result = translateByTi(ti, locale);
        if(StrUtil.isBlank(result)){
            if(StrUtil.isNotBlank(defaultDescription)){
                result = defaultDescription;
            }else{
                result = ti;
            }
        }
        return result;
    }

    @Override
    public String translateDtcDescription(String ti, String locale, String defaultDescription) {

        String result = translateDescription(ti, locale, defaultDescription);
        if(result == ti){
            ti = "CV.DTC.UNKNOWN";
            result = translateByTi(ti, locale);
        }
        return result;
    }

    @Override
    public String translateErrorMsg(String ti, String locale, String msg) {
        return translateDescription("ec"+ti, locale, msg);
    }

    @Override
    public String multipleTiTranslate(String ti, String locale) {
        String[] sourceArr = ti.split("\\|");
        StringBuffer stringBuffer = new StringBuffer("");
        for (String ss: sourceArr ) {
            if(ss.startsWith("{") && ss.endsWith("}") ){
                String raw = ss.replace("{", "").replace("}","");
                stringBuffer.append(raw);
            }else{
                String translate = translateDescription(ss,locale, ss);
                stringBuffer.append(translate);
            }
        }

        return stringBuffer.toString();
    }
}
