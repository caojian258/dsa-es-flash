package org.dsa.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("sys_translations")
public class SysTranslations implements Serializable {

    /***
     * 主键ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * TI
     */
    private String ti;

    /**
     * 语种
     */
    private Integer locale;

    /**
     * 词条内容
     */
    private String text;

    /**
     * 诊断库字段
     */
    private String geometry;

    private Date createTime;
}
