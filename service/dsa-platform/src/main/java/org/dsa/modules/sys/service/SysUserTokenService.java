package org.dsa.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dsa.common.utils.R;
import org.dsa.modules.sys.entity.DisplayUserEntity;
import org.dsa.modules.sys.entity.SysUserTokenEntity;

/**
 * 用户Token
 *
 */
public interface SysUserTokenService extends IService<SysUserTokenEntity> {

	/**
	 * 生成token
	 *
	 * @param userId 用户ID
	 * @param type
	 */
	String createToken(long userId, DisplayUserEntity user, Integer type);

	/**
	 * sso 登陆保存 token
	 * @param userId  用户ID
	 */
	R saveToken(long userId,String token);

	/**
	 * 退出，修改token值
	 * @param userId  用户ID
	 */
	void logout(long userId);

	SysUserTokenEntity getToken(String token);

}
