package org.dsa.modules.sys.controller;


import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.common.annotation.SysLog;
import org.dsa.common.utils.PageUtils;
import org.dsa.common.utils.R;
import org.dsa.common.validator.ValidatorUtils;
import org.dsa.modules.remoteDiagnostic.vo.ConfigPageReqVo;
import org.dsa.modules.sys.entity.SysConfigEntity;
import org.dsa.modules.sys.service.SysConfigService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 系统配置信息
 *
 */
@RestController
@RequestMapping("/sys/config")
public class SysConfigController extends AbstractController {
	@Autowired
	private SysConfigService sysConfigService;
	
	/**
	 * 所有配置列表
	 */
	@PostMapping("/list")
	@RequiresPermissions("sys:config:list")
	public R list(@RequestBody ConfigPageReqVo vo){
		Page<SysConfigEntity> page = sysConfigService.queryPage(vo);

		return R.ok().put("page", page);
	}
	
	
	/**
	 * 配置信息
	 */
	@GetMapping("/info/{id}")
	@RequiresPermissions("sys:config:info")
	public R info(@PathVariable("id") Long id){
		SysConfigEntity config = sysConfigService.getById(id);
		
		return R.ok().put("config", config);
	}
	
	/**
	 * 保存配置
	 */
	@SysLog("保存配置")
	@PostMapping("/save")
	@RequiresPermissions("sys:config:save")
	public R save(@RequestBody SysConfigEntity config){
		ValidatorUtils.validateEntity(config);

		sysConfigService.saveConfig(config);
		
		return R.ok();
	}
	
	/**
	 * 修改配置
	 */
	@SysLog("修改配置")
	@PostMapping("/update")
	@RequiresPermissions("sys:config:update")
	public R update(@RequestBody SysConfigEntity config){
		ValidatorUtils.validateEntity(config);
		
		sysConfigService.update(config);
		
		return R.ok();
	}
	
	/**
	 * 删除配置
	 */
	@SysLog("删除配置")
	@PostMapping("/delete")
	@RequiresPermissions("sys:config:delete")
	public R delete(@RequestBody Long[] ids){
		sysConfigService.deleteBatch(ids);
		
		return R.ok();
	}

	/**
	 * 查询
	 */
	@RequestMapping("/queryLikeKey/{key}")
	public R queryLikeKey(@PathVariable("key") String key){
		if (StringUtils.isBlank(key)) {
			return R.error("key is null");
		}
		List<SysConfigEntity> likeValues = sysConfigService.getLikeValues(key);
		return R.ok().put("data", likeValues);
	}

	/**
	 * 查询
	 */
	@RequestMapping("/queryLikeGroup/{group}")
	public R queryLikeGroup(@PathVariable("group") String group){
		if (StringUtils.isBlank(group)) {
			return R.error("group is null");
		}
		List<SysConfigEntity> likeValues = sysConfigService.getLikeGroup(group);
		return R.ok().put("data", likeValues);
	}

	/**
	 * 查询
	 */
	@RequestMapping("/queryConfByKey/{key}")
	public R queryConfByKey(@PathVariable("key") String key){
		if (StringUtils.isBlank(key)) {
			return R.error("key is null");
		}
		SysConfigEntity likeValues = sysConfigService.queryConfByKey(key);
		return R.ok().put("data", likeValues);
	}


}
