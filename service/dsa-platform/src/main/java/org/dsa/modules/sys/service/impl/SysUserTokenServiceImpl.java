package org.dsa.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import jakarta.annotation.Resource;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.R;
import org.dsa.common.utils.RedisKeys;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.sys.dao.SysUserTokenDao;
import org.dsa.modules.sys.entity.DisplayUserEntity;
import org.dsa.modules.sys.entity.SysNoticeEntity;
import org.dsa.modules.sys.entity.SysUserTokenEntity;
import org.dsa.modules.sys.oauth2.TokenGenerator;
import org.dsa.modules.sys.service.SysUserTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;


@Service("sysUserTokenService")
public class SysUserTokenServiceImpl extends ServiceImpl<SysUserTokenDao, SysUserTokenEntity> implements SysUserTokenService {
	//12小时后过期
	private final static int EXPIRE = 3600 * 12;

	@Autowired
	SysUserTokenDao sysUserTokenDao;
	@Resource
	private RedisUtils redisUtils;

	/**
	 * @param userId 用户ID
	 * @param user
	 * @param type 0 serverToken 1 clientToken
	 * @return
	 */
	@Override
	public String createToken(long userId, DisplayUserEntity user, Integer type) {
		//生成一个token
		String token = TokenGenerator.generateValue();
		//当前时间
		Date now = new Date();
		//过期时间
		Date expireTime = new Date(now.getTime() + EXPIRE * 1000);
		//判断是否生成过token
		// userId+type 为唯一
		LambdaQueryWrapper<SysUserTokenEntity> query = Wrappers.lambdaQuery();
		query.eq(SysUserTokenEntity::getUserId,userId);
		query.eq(SysUserTokenEntity::getType,type);
		SysUserTokenEntity tokenEntity = baseMapper.selectOne(query);
		if(tokenEntity == null){
			tokenEntity = new SysUserTokenEntity();
			tokenEntity.setUserId(userId);
			tokenEntity.setToken(token);
			tokenEntity.setUpdateTime(now);
			tokenEntity.setExpireTime(expireTime);
			tokenEntity.setType(type);
			//保存token
			this.save(tokenEntity);
		}else{
			if(tokenEntity.getExpireTime().getTime()>now.getTime()){
				// 未失效，被挤掉，缓存被挤掉的token
				long expire = redisUtils.getExpire(tokenEntity.getToken());
				redisUtils.set(RedisKeys.getExpireTokenKey(tokenEntity.getToken()),null,expire);
			}
			// 删除redis缓存
			redisUtils.delete(tokenEntity.getToken());
			tokenEntity.setToken(token);
			tokenEntity.setUpdateTime(now);
			tokenEntity.setExpireTime(expireTime);
			//更新token
			baseMapper.update(tokenEntity,query);
		}
		user.setToken(token);
		redisUtils.set(token,user,EXPIRE);
		return token;
	}

	public R saveToken(long userId,String token) {

		//当前时间
		Date now = new Date();
		//过期时间
		Date expireTime = new Date(now.getTime() + EXPIRE * 1000);

		//判断是否生成过token
		SysUserTokenEntity tokenEntity = this.getById(userId);
		if(tokenEntity == null){
			tokenEntity = new SysUserTokenEntity();
			tokenEntity.setUserId(userId);
			tokenEntity.setToken(token);
			tokenEntity.setUpdateTime(now);
			tokenEntity.setExpireTime(expireTime);

			//保存token
			this.save(tokenEntity);
		}else{
			tokenEntity.setToken(token);
			tokenEntity.setUpdateTime(now);
			tokenEntity.setExpireTime(expireTime);

			//更新token
			this.updateById(tokenEntity);
		}

		R r = R.ok().put("token", token).put("expire", EXPIRE);

		return r;
	}

	@Override
	public void logout(long userId) {
		//生成一个token
		String token = TokenGenerator.generateValue();

		//修改token
		SysUserTokenEntity tokenEntity = new SysUserTokenEntity();
		tokenEntity.setUserId(userId);
		tokenEntity.setToken(token);
		this.updateById(tokenEntity);
	}

	@Override
	public SysUserTokenEntity getToken(String token) {
		SysUserTokenEntity tokenEntity = sysUserTokenDao.queryByToken(token);
		return tokenEntity;
	}
}
