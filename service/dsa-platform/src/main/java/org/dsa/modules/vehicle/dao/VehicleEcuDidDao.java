package org.dsa.modules.vehicle.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.vehicle.entity.VehicleEcuDid;

import java.util.List;

@Mapper
public interface VehicleEcuDidDao extends BaseMapper<VehicleEcuDid> {

     void oneInsert(@Param("did") VehicleEcuDid did);

     void batchInsert(@Param("didList") List<VehicleEcuDid> didList);

     void deleteByVehicleType(@Param("vehicleTypeId") Long id);

     void deleteByVehicleTypeEcu(@Param("vehicleTypeId") Long id,
                                 @Param("ecuName") String ecuName);

     void deleteByVehicleTypeEcuDid(@Param("vehicleTypeId") Long id,
                                    @Param("ecuName") String ecuName,
                                    @Param("did") String did);

     Integer countByVehicleTypeIdEcu(@Param("vehicleTypeId") Long id,
                                     @Param("ecuName") String ecuName);
}
