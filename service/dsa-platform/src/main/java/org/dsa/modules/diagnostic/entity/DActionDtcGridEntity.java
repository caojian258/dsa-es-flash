package org.dsa.modules.diagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("d_action_dtc_grid")
public class DActionDtcGridEntity implements Serializable
{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 功能执行id
     */
    private String actionId;
    /**
     * 故障码  Code
     */
    private String dtcCode;
    /**
     * 故障码  Code
     */
    private String dtcCode_Int;
    /**
     * 故障码  Code
     */
    private String dtcCodeHex;
    /**
     * ecu 名称
     */
    private String ecuName;
    /**
     * 故障码
     */
    private String name;
    /**
     * 冻结帧名称多语言标识
     */
    private String ti;
    /**
     * 冻结帧值
     */
    private String value;
    /**
     * 冻结帧值
     */
    private String tiValue;

    /**
     * 冻结帧单位
     */
    private String unit;

    /**
     * 创建时间
     */
    private Date createTime;

}
