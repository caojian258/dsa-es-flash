package org.dsa.modules.sys.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.ObjectUtils;
import org.dsa.common.utils.PageUtils;
import org.dsa.common.utils.Query;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.remoteDiagnostic.constant.RemoteDiagnosticConstant;
import org.dsa.modules.remoteDiagnostic.dao.RemotePoolVehicleDao;
import org.dsa.modules.remoteDiagnostic.entity.RemotePoolDependEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemotePoolVehicleEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuEntity;
import org.dsa.modules.remoteDiagnostic.vo.ConfigPageReqVo;
import org.dsa.modules.remoteDiagnostic.vo.VehicleEcuPageReVo;
import org.dsa.modules.sys.dao.SysConfigDao;
import org.dsa.modules.sys.entity.SysConfigEntity;
import org.dsa.modules.sys.redis.SysConfigRedis;
import org.dsa.modules.sys.service.SysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("sysConfigService")
public class SysConfigServiceImpl extends ServiceImpl<SysConfigDao, SysConfigEntity> implements SysConfigService {
	@Autowired
	private SysConfigRedis sysConfigRedis;
	@Autowired
	RemotePoolVehicleDao poolVehicleDao;
	@Autowired
	SysConfigDao sysConfigDao;
	@Autowired
	private RedisUtils redisUtils;

//	@Override
//	public PageUtils queryPage(Map<String, Object> params) {
//		String paramKey = (String)params.get("paramKey");
//		Integer type = ObjectUtils.isNotNull(params.get("type")) ? Integer.parseInt(params.get("type").toString()) : null;
//		IPage<SysConfigEntity> page = this.page(
//			new Query<SysConfigEntity>().getPage(params),
//			new QueryWrapper<SysConfigEntity>()
//				.like(StringUtils.isNotBlank(paramKey),"param_key", paramKey)
//				.eq(type != null,"type", type)
//				.eq("status", 1)
//				.orderByDesc("id")
//		);
//
//		return new PageUtils(page);
//	}

	@Override
	public Page<SysConfigEntity> queryPage(ConfigPageReqVo vo) {
		Page<SysConfigEntity> page = new Page<>(vo.getPageNo(), vo.getPageSize());

		String column = StrUtil.toUnderlineCase(vo.getSortColumn());
		OrderItem oi = new OrderItem(column, vo.getSortAsc());
		page.addOrder(oi);
		LambdaQueryWrapper<SysConfigEntity> qw = Wrappers.<SysConfigEntity>lambdaQuery();

		if (!StringUtils.isEmpty(vo.getName())) {
//            qw.like(VehicleEcuEntity::getEcuName, vo.getName().trim());
			qw.apply("param_key" + " ilike {0}", "%" + vo.getName().trim() + "%");
		}
		if (!StringUtils.isEmpty(vo.getGroupName())) {
			qw.apply("param_group" + " ilike {0}", "%" + vo.getGroupName().trim() + "%");
		}

		if (vo.getType() != null) {
			qw.eq(SysConfigEntity::getType, vo.getType());
		}
		Page<SysConfigEntity> resultPage = sysConfigDao.selectPage(page, qw);

		return resultPage;
	}

	@Override
	public void saveConfig(SysConfigEntity config) {
		this.save(config);
		sysConfigRedis.saveOrUpdate(config);

		if (StringUtils.isNotEmpty(config.getParamGroup()) && sysConfigRedis.hasGroup(config.getParamGroup())) {
			sysConfigRedis.deleteGroup(config.getParamGroup());
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void update(SysConfigEntity config) {
		this.updateById(config);
		sysConfigRedis.saveOrUpdate(config);

		if (StringUtils.isNotEmpty(config.getParamGroup()) && sysConfigRedis.hasGroup(config.getParamGroup())) {
			sysConfigRedis.deleteGroup(config.getParamGroup());
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateValueByKey(String key, String value) {
		baseMapper.updateValueByKey(key, value);
		sysConfigRedis.delete(key);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteBatch(Long[] ids) {
		for(Long id : ids){
			SysConfigEntity config = this.getById(id);
			sysConfigRedis.delete(config.getParamKey());
			if (StringUtils.isNotEmpty(config.getParamGroup())) {
				if (sysConfigRedis.hasGroup(config.getParamGroup()))
				    sysConfigRedis.deleteGroup(config.getParamGroup());

				// 删除车辆池相关得缓存
				String group = config.getParamGroup();
				List<Long> poolId = poolVehicleDao.selectPoolByGroup(config.getParamGroup());

				poolVehicleDao.delete(Wrappers.<RemotePoolVehicleEntity>lambdaUpdate().eq(RemotePoolVehicleEntity::getConfigId, id));

				if (poolId.size() != 0) {
					poolId.forEach(e ->{
						sysConfigRedis.deleteByKey(RemoteDiagnosticConstant.VEHICLE_POOL_CONFIG_KEY + e + ":" + group); });
					redisUtils.set((RemoteDiagnosticConstant.VEHICLE_POOL_LOCK), System.currentTimeMillis());
				}
			}
		}

		this.removeByIds(Arrays.asList(ids));
	}

	@Override
	public String getValue(String key) {
		SysConfigEntity config = sysConfigRedis.get(key);
		if(config == null){
			config = baseMapper.queryByKey(key);
			sysConfigRedis.saveOrUpdate(config);
		}

		return config == null ? null : config.getParamValue();
	}

	@Override
	public <T> T getConfigObject(String key, Class<T> clazz) {
		String value = getValue(key);
		if(StringUtils.isNotBlank(value)){
			return new Gson().fromJson(value, clazz);
		}

		try {
			return clazz.newInstance();
		} catch (Exception e) {
			throw new RRException("获取参数失败");
		}
	}

	@Override
	public Map<String, String> keyValueMap(String remark) {
		List<SysConfigEntity> items =  baseMapper.keyValueMap(remark);
		Map<String, String> maps = new HashMap<>();
		if(null != items){
			for (SysConfigEntity i: items) {
				maps.put(i.getParamValue(), i.getParamKey());
			}
		}

		return maps;
	}

	@Override
	public List<SysConfigEntity> getLikeValues(String key) {
		LambdaQueryWrapper<SysConfigEntity> qw = Wrappers.<SysConfigEntity>lambdaQuery();
//		qw.like(SysConfigEntity::getParamKey, key);
		qw.apply("param_key"+" ilike {0}", "%"+key.trim()+"%");
		return baseMapper.selectList(qw);
	}

	@Override
	public List<SysConfigEntity> getLikeGroup(String group) {
		Gson gson = new Gson();
		if (sysConfigRedis.hasGroup(group)) {
			return gson.fromJson(sysConfigRedis.getGroup(group),new TypeToken<List<SysConfigEntity>>() {
			}.getType() );
		}
		LambdaQueryWrapper<SysConfigEntity> qw = Wrappers.<SysConfigEntity>lambdaQuery();
//		qw.like(SysConfigEntity::getParamKey, key);
		qw.apply("param_group"+" ilike {0}", "%"+group.trim()+"%");
		qw.orderByAsc(SysConfigEntity::getOrderNum);
        List<SysConfigEntity> sysConfigEntities = baseMapper.selectList(qw);

        sysConfigRedis.saveGroup(gson.toJson(sysConfigEntities), group);

        return baseMapper.selectList(qw);
	}

	@Override
	public SysConfigEntity queryConfByKey(String key) {
		return baseMapper.selectOne(Wrappers.<SysConfigEntity>lambdaQuery().eq(SysConfigEntity::getParamKey, key));
	}
}
