package org.dsa.modules.remoteDiagnostic.dto.api;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.ToString;
import org.dsa.common.handler.JsonTypeHandler;
import org.dsa.modules.remoteDiagnostic.dto.TaskGradientDto;

import java.util.Date;
import java.util.List;

@Data
@ToString()
public class DiagnosticTaskDto {

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Long id;

    private String campaignName;

    private String sessionId;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Long poolId;

    private Date startTime;

    private Date endTime;

    private String model;

    private Integer failedAttemptsNum;

    private Integer cycle;

    private String priority;

    private List<DiagnosticConditionDto> conditionList;

    private List<DiagnosticFunctionDto> funList;

    private List<DiagnosticFileDto> file;

}
