package org.dsa.modules.sys.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.utils.Constant;
import org.dsa.modules.sys.dao.SysMenuDao;
import org.dsa.modules.sys.dao.SysUserDao;
import org.dsa.modules.sys.dao.SysUserTokenDao;
import org.dsa.modules.sys.entity.SysMenuEntity;
import org.dsa.modules.sys.entity.SysUserEntity;
import org.dsa.modules.sys.entity.SysUserTokenEntity;
import org.dsa.modules.sys.service.ShiroService;
import org.dsa.modules.sys.service.SysUserGroupService;
import org.dsa.modules.sys.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ShiroServiceImpl implements ShiroService {
    @Autowired
    private SysMenuDao sysMenuDao;
    @Autowired
    private SysUserDao sysUserDao;
    @Autowired
    private SysUserTokenDao sysUserTokenDao;
    @Autowired
    private SysUserGroupService sysUserGroupService;

    @Override
    public Set<String> getUserPermissions(long userId) {
        List<String> permsList;

        //系统管理员，拥有最高权限
        if(ShiroUtils.getUserName().equals(Constant.SUPER_ADMIN_USER_NAME)){
            List<SysMenuEntity> menuList = sysMenuDao.selectList(null);
            permsList = new ArrayList<>(menuList.size());
            for(SysMenuEntity menu : menuList){
                permsList.add(menu.getPerms());
            }
        }else{
            permsList = sysUserDao.queryAllPerms(userId);
        }
        //用户权限列表
        Set<String> permsSet = new HashSet<>();
        for(String perms : permsList){
            if(StringUtils.isBlank(perms)){
                continue;
            }
            permsSet.addAll(Arrays.asList(perms.trim().split(",")));
        }
        return permsSet;
    }

    @Override
    public SysUserTokenEntity queryByToken(String token) {
        return sysUserTokenDao.queryByToken(token);
    }

    @Override
    public SysUserEntity queryUser(Long userId) {
         
        SysUserEntity sysUserEntity=sysUserDao.queryByUserId(userId);
        //获取用户所属组列表
//        List<Long> groupIdList=sysUserGroupService.queryGroupIdList(sysUserEntity.getUserId());
//        sysUserEntity.setGroupIdList(groupIdList);
        return sysUserEntity;
    }

    @Override
    public Set<String> getPermissionsByRole(List<String> roles, Integer owningSystem) {
        List<String> permsList;
        List<Integer> systems = CollUtil.newArrayList(owningSystem, 0);
        //系统管理员，拥有最高权限
        if(ShiroUtils.getUserName().equals(Constant.SUPER_ADMIN_USER_NAME)){
            List<SysMenuEntity> menuList = sysMenuDao.selectList(new QueryWrapper<SysMenuEntity>().in("owning_system", systems));
            permsList = new ArrayList<>(menuList.size());
            for(SysMenuEntity menu : menuList){
                permsList.add(menu.getPerms());
            }
        }else{
            // 指定系统
            permsList = sysUserDao.queryPermissionsByRole(roles,owningSystem);
        }
        //用户权限列表
        Set<String> permsSet = new HashSet<>();
        for(String perms : permsList){
            if(StringUtils.isBlank(perms)){
                continue;
            }
            permsSet.addAll(Arrays.asList(perms.trim().split(",")));
        }
        return permsSet;
    }
}
