package org.dsa.modules.reporter.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.modules.reporter.pojo.PageParam;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class VehInfoPageReVo extends PageParam {

    private String vinCode;

    private String model;
}
