package org.dsa.modules.vehicle.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import jakarta.servlet.http.HttpServletResponse;
import org.dsa.common.utils.PageUtils;
import org.dsa.common.utils.R;
import org.dsa.modules.remoteDiagnostic.vo.Vehicle.VehicleInfoExcelReqVo;
import org.dsa.modules.vehicle.entity.DVehicleInfoEntity;
import org.dsa.modules.vehicle.entity.DVehicleTypeEntity;
import org.dsa.modules.vehicle.vo.VehicleInfoPageReqVo;


import java.util.List;
import java.util.Map;

/**
 * 车型管理
 */

public interface VehicleTypeService extends IService<DVehicleTypeEntity> {

    /**
     * 查询车型列表
     * @return List<DVehicleTypeEntity>
     */
    List<DVehicleTypeEntity> queryList(String name);

    /**
     * 查询子节点
     */
    List<DVehicleTypeEntity> queryChildrenList(Map<String, Object> params);

    /**
     * 根据vehicleTypeId获取车辆信息
     */
    Page<DVehicleInfoEntity> getListByVehicleTypeIdAndVin(VehicleInfoPageReqVo vo);

    /**
     * 车型校验
     */
    R checkVehicleType(DVehicleTypeEntity vehicleType);


    Long getVehicleTypeId(String vehicleType);


    List<DVehicleTypeEntity> getTreeList();

    List<DVehicleTypeEntity> selectListById(Long vehicleTypeId);

    void updateTypeOrder(List<DVehicleTypeEntity> typeEntities);

    void updateOrder(Long parentId);

    void saveInfo(DVehicleTypeEntity vehicleType);

    void updateInfo(DVehicleTypeEntity vehicleType);

    void batchDelete(List<DVehicleTypeEntity> childrenList);

    DVehicleTypeEntity selectById(Long vehicleTypeId);

    String getWholeVehicleType(Long typeId);

    void exportVehicle(HttpServletResponse response, VehicleInfoExcelReqVo vo);
}
