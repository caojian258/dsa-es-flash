package org.dsa.modules.remoteDiagnostic.service;

import org.dsa.modules.remoteDiagnostic.entity.RemoteVersionEcuRelyEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVersionRecordEntity;

import java.util.List;

public interface RemoteEcuDependencyService {


    List<RemoteVersionEcuRelyEntity> getEcuDependencyByVehicleVersion(Long id);
}
