package org.dsa.modules.remoteDiagnostic.constant;

/***
 * 刷写常量类
 */
public class FlashConstant {


    //ECU零件号
    public final static String EPN = "EcuPartNumberDID";

    //车辆硬件版本
    public final static String VHV = "VehicleHardwareVersionDID";

    //车辆软件版本
    public final static String VSV = "VehicleSoftwareVersionDID";

    //ECU软件版本
    public final static String ESV = "EcuSoftwareVersionDID";

    //ECU硬件版本
    public final static String EHV = "EcuHardwareVersionDID";

    //不需要升级和对齐
    public final static Integer NO_VERSION = 0;

    //需要对齐
    public final static Integer ALIGN_VERSION = 1;

    //需要刷写
    public final static Integer HAS_VERSION = 2;

    //目标版本
    public final static String VERSION_TARGET = "target";

    //当前版本
    public final static String VERSION_EXPECT = "expect";

    //刷写文件类型
    public final static String FILE_TYPE = "flashFile";

    //关键ECU名称
    public final static String KEY_ECU = "KEY_ECU";

    public final static String MANDATORY = "mandatory";

    public final static String OPTIONAL = "optional";

    //不可跳过
    public final static Integer NO_CROSS = 1;

    public final static Integer MUST_FLASH = 1;

    //下载地址
    public final static String DOWNLOAD_PATH = "ecu/flash/downloadFile";

    //0 正常 1 关键ecu版本存在有ecu版本不存在 2 版本都不存在 3 关键版本不存在
    public final static Integer ECU_VERSION_NORMAL = 0;

    public final static Integer ECU_VERSION_MISS = 1;

    public final static Integer ECU_VERSION_ALL_MISS = 2;

    public final static Integer ECU_VERSION_KEY_MISS = 3;

}
