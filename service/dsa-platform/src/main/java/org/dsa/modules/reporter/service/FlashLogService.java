package org.dsa.modules.reporter.service;

import org.dsa.modules.reporter.dto.EcuVersionHistoryDto;
import org.dsa.modules.reporter.vo.api.FlashLogReVo;
import org.dsa.modules.reporter.vo.api.SyncFlashLogRevo;

import java.util.List;

public interface FlashLogService {

    public Boolean validateSign(FlashLogReVo vo);

    public Boolean validateTimestamp(FlashLogReVo vo);

    /***
     * 收集来自离线诊断仪的数据
     * @param vo
     */
    public void collectData(FlashLogReVo vo);

    /***
     * 同步数据给TSP
     * @param vo
     */
    public void syncData(SyncFlashLogRevo vo) throws Exception;


    public List<EcuVersionHistoryDto> successRecord(String vin, String tag);

    public SyncFlashLogRevo formatSyncData(FlashLogReVo vo);
}
