package org.dsa.modules.onlineUpdate.rule;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.jexl2.Expression;
import org.apache.commons.jexl2.JexlEngine;
import org.apache.commons.jexl2.MapContext;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.onlineUpdate.dao.RuleMatchedRecordDao;
import org.dsa.modules.onlineUpdate.dao.RuleUserDao;
import org.dsa.modules.onlineUpdate.dao.RuleWorkshopDao;
import org.dsa.modules.onlineUpdate.entity.*;
import org.dsa.modules.onlineUpdate.service.RuleService;
import org.dsa.modules.onlineUpdate.service.SoftwareVersionService;
import org.dsa.modules.onlineUpdate.vo.SoftwareVersionVo;
import org.dsa.modules.remoteDiagnostic.dao.*;
import org.dsa.modules.remoteDiagnostic.entity.*;
import org.dsa.modules.remoteDiagnostic.service.RemoteVehiclePoolService;
import org.dsa.modules.sys.dao.SysConfigDao;
import org.dsa.modules.sys.entity.DisplayUserEntity;
import org.dsa.modules.sys.entity.SysConfigEntity;
import org.dsa.modules.vehicle.dao.DVehicleInfoDao;
import org.dsa.modules.vehicle.entity.DVehicleInfoEntity;
import org.dsa.modules.vehicle.entity.DVehicleTypeEntity;
import org.dsa.modules.vehicle.service.impl.VehicleServiceImpl;
import org.dsa.modules.vehicle.service.impl.VehicleTypeServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;


/**
 * 规则匹配组件
 */
@Slf4j
@Component
public class RuleComponent {


    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private RuleService ruleService;
    @Autowired
    private RuleUserDao ruleUserDao;
    @Autowired
    private RuleWorkshopDao ruleWorkshopDao;
    @Autowired
    private RuleMatchedRecordDao ruleMatchedRecordDao;
    @Autowired
    private SoftwareVersionService softwareVersionService;
    @Autowired
    private RemoteVehiclePoolService remoteVehiclePoolService;
    @Autowired
    private VehicleServiceImpl vehicleService;
    @Autowired
    private VehicleTypeServiceImpl vehicleTypeService;
    // 逻辑表达式字符串设置为常量
    public static final String USER_WORKSHOP_CONDITION = "user||workshop";
    public static final String VEHICLE_GROUP_REG = "vin||productionDate||placeOfProduction||vehicleType||version||ecuVersion";
    @Autowired
    private DVehicleInfoDao dVehicleInfoDao;
    @Autowired
    private RemoteVehicleTypeEcuGroupDao remoteVehicleTypeEcuGroupDao;
    @Autowired
    private RemoteVehicleVersionDao remoteVehicleVersionDao;
    @Autowired
    private RemoteVehicleVersionEcuGroupDao remoteVehicleVersionEcuGroupDao;
    @Autowired
    private RemotePoolVehicleDao poolVehicleDao;
    @Autowired
    private SysConfigDao sysConfigDao;
    @Autowired
    private DiagnosticTaskCampaignDao diagnosticTaskReleaseDao;
    @Autowired
    private RVehicleTagDao rVehicleTagDao;

    /**
     * 在线升级——远程诊断
     * redis缓存 车辆->任务list
     */
    public List<DiagnosticTaskCampaignEntity> matchRelease(Map<String, Object> param) {
        String vin = MapUtil.getStr(param, "vin");
        // 车辆信息
        DVehicleInfoEntity vehicleInfo = dVehicleInfoDao.selectOneByVin(vin);
        if (vehicleInfo == null) {
            return new ArrayList<>();
        }
        // 车型
        List<Long> vehicleTypeIds = vehicleTypeService.selectListById(vehicleInfo.getVehicleTypeId())
                .stream().map(DVehicleTypeEntity::getId).collect(Collectors.toList());

        // ECU版本
        List<Long> ecuVersionIds = remoteVehicleVersionEcuGroupDao.getListByTypes(vehicleTypeIds)
                .stream().map(RemoteVehicleVersionEcuGroupEntity::getEcuVersionId)
                .collect(Collectors.toList());

        // 车辆版本
        List<Long> versionIds = remoteVehicleVersionDao.getListByTypes(vehicleTypeIds)
                .stream().map(RemoteVehicleVersionEntity::getId)
                .collect(Collectors.toList());

        // 车辆池匹配条件
        Map<String, Object> filter = new HashMap<>();
        filter.put("vin", vin);
        //待定 车辆池——车辆信息：销售区域、生产车厂、生产年份
//        filter.put("vehicle", "");
        // 生产日期
        filter.put("productionDate", vehicleInfo.getProductionDate());
        // 产地
        filter.put("placeOfProduction", vehicleInfo.getPlaceOfProduction());
        filter.put("vehicleType", vehicleTypeIds);
        // 车辆版本
        filter.put("version", versionIds);
        // ECU版本
        filter.put("ecuVersion", ecuVersionIds);
        // 查询所有生效的车辆池基本信息
        List<RemoteVehiclePoolEntity> poolList = remoteVehiclePoolService.getList();
        List<DiagnosticTaskCampaignEntity> filterList = new ArrayList<>();
        // 遍历过滤符合条件的pool,并转化为预发布的诊断任务
        poolList.forEach(pool -> {
            List<DiagnosticTaskCampaignEntity> taskList = matchPoolByFilter(pool, filter);
            if (taskList != null && !taskList.isEmpty()) {
                filterList.addAll(taskList);
            }
        });
        return filterList;
    }

    private List<DiagnosticTaskCampaignEntity> matchPoolByFilter(RemoteVehiclePoolEntity pool, Map<String, Object> filter) {
        // 获取详情
        pool = remoteVehiclePoolService.getInfo(pool.getId());
        // 替换后的
        String condition2 = RuleComponent.VEHICLE_GROUP_REG;
        condition2 = condition2.replace("(", " ")
                .replace(")", " ")
                .replace("&&", " ")
                .replace("||", " ")
                .replace("!", " ");
        List<String> collect = Arrays.stream(condition2.split(" ")).filter(ObjectUtil::isNotNull).collect(Collectors.toList());
        // collect条件判断
        String condition3 = RuleComponent.VEHICLE_GROUP_REG;
        if (collect.contains("vin")) {
            String vin = MapUtil.getStr(filter, "vin");
            List<String> vinListByPool = rVehicleTagDao.getVinListByPoolId(pool.getId());
            boolean vinBool = vinListByPool.contains(vin);
            condition3 = condition3.replaceAll("vin", Boolean.toString(vinBool));
        }
        if (collect.contains("vehicleType")) {
            List<Long> vehicleTypeIds = (List<Long>) MapUtils.getObject(filter, "vehicleType");
            pool.getVehicleTypeIds().retainAll(vehicleTypeIds);
            boolean poolBool = !pool.getVehicleTypeIds().isEmpty();
            condition3 = condition3.replaceAll("vehicleType", Boolean.toString(poolBool));
        }
        //vehicle 查配置表 生产日期，产地，销售区域（sys_config）
//        if (collect.contains("vehicle")) {
//            String vehicle = MapUtil.getStr(filter, "vehicle");
//            boolean vinBool = false;
//            condition3 = condition3.replaceAll("vehicle", Boolean.toString(vinBool));
//        }
        // 生产日期
        if (collect.contains("productionDate")) {
            String productionDate = MapUtil.getStr(filter, "productionDate");
            Map<String, String> map = pool.getProductionDate() == null || pool.getProductionDate().size() == 0 ? new HashMap<>() :
                    sysConfigDao.selectList(Wrappers.<SysConfigEntity>lambdaQuery().
                                    in(SysConfigEntity::getId, pool.getProductionDate()).select(SysConfigEntity::getParamValue)).
                            stream().collect(Collectors.toMap(SysConfigEntity::getParamValue, SysConfigEntity::getParamValue));
            boolean vinBool = map.size() != 0 && map.containsKey(productionDate);
            condition3 = condition3.replaceAll("productionDate", Boolean.toString(vinBool));
        }
        // 产地
        if (collect.contains("placeOfProduction")) {
            String placeOfProduction = MapUtil.getStr(filter, "placeOfProduction");
            Map<String, String> map = pool.getPlaceOfProduction() == null || pool.getPlaceOfProduction().size() == 0 ? new HashMap<>() :
                    sysConfigDao.selectList(Wrappers.<SysConfigEntity>lambdaQuery().
                                    in(SysConfigEntity::getId, pool.getPlaceOfProduction()).select(SysConfigEntity::getParamValue)).
                            stream().collect(Collectors.toMap(SysConfigEntity::getParamValue, SysConfigEntity::getParamValue));

            boolean vinBool = map.size() != 0 && map.containsKey(placeOfProduction);
            condition3 = condition3.replaceAll("placeOfProduction", Boolean.toString(vinBool));
        }

        // 校验版本
        if (collect.contains("version")) {
            List<Long> version = (List<Long>) MapUtils.getObject(filter, "version");
            List<Long> poolVersionIds = pool.getVersions().stream().map(RemoteVehicleVersionEntity::getId).collect(Collectors.toList());
            poolVersionIds.retainAll(version);
            boolean versionPool = !poolVersionIds.isEmpty();
            condition3 = condition3.replaceAll("version", Boolean.toString(versionPool));
        }
        // 校验ECU分组
        if (collect.contains("ecuVersion")) {
            List<Long> ecuGroup = (List<Long>) MapUtils.getObject(filter, "ecuVersion");
            List<Long> groupIds = pool.getEcuVersions().stream().map(RemoteVehicleEcuVersionEntity::getId).collect(Collectors.toList());
            groupIds.retainAll(ecuGroup);
            boolean poolBool = !groupIds.isEmpty();
            condition3 = condition3.replaceAll("ecuVersion", Boolean.toString(poolBool));
        }
        //使用Jexl工具包 判断逻辑表达式字符串的结果
        JexlEngine engine = new JexlEngine();
        Expression expression = engine.createExpression(condition3);
        Boolean trueCondition = (Boolean) expression.evaluate(new MapContext());
        // 满足表达式的规则
        if (trueCondition) {
            return diagnosticTaskReleaseDao.selectList(new QueryWrapper<DiagnosticTaskCampaignEntity>().select("id").eq("pool_id", pool.getId()));
        }
        return null;
    }

    /**
     * 在线升级——规则匹配
     *
     * @return ruleList
     */
    // 匹配主方法
    public List<RuleEntity> match(Map<String, Object> param) {
        // filterRules 根据字符串匹配的规则结果
        List<RuleEntity> filterRules = new ArrayList<>();
        // 1. 查该软件 是否有 有效规则
        Long softwareCode = MapUtil.getLong(param, "softwareId");
        // 同一软件多个版本的规则，匹配唯一规则
        Integer softwareVersionId = MapUtil.getInt(param, "softwareVersionNumber");
        List<RuleEntity> effectiveRules = ruleService.existEffectiveRule(softwareCode);
        if (effectiveRules.isEmpty()) {
            return new ArrayList<>();
        }
        // 2. 根据token获取用户及其workshop信息       
        String token = MapUtils.getString(param, "token");
        DisplayUserEntity userEntity = JSONObject.parseObject(redisUtils.get(token), DisplayUserEntity.class);
        if (userEntity == null) {
            log.error("token：{} 失效", token);
            throw new RRException("token失效",401);
        }
        param.put("userId", userEntity.getUserInfo().getUserId());
        param.put("workshopId", userEntity.getUserInfo().getWorkShopId());
        //解析条件表达式
        filterRules = analysisCondition(USER_WORKSHOP_CONDITION, param, effectiveRules, filterRules);
        if(filterRules.isEmpty()){
            return new ArrayList<>();
        }
        // 首次安装,返回最新匹配到的版本规则
        if (Objects.equals(softwareVersionId,0)){
            List<RuleEntity> res = new ArrayList<>(1);
             res.add(filterRules.get(filterRules.size()-1));
            return res;
        }
        SoftwareVersionVo softwareVersionVo = softwareVersionService.info(softwareVersionId);
        return matchVersionRule(filterRules, softwareVersionVo);
    }

    private List<RuleEntity> matchVersionRule(List<RuleEntity> filterRules, SoftwareVersionVo softwareVersionVo) {

        // 不可跨越版本规则
        RuleEntity noCrossRule = null;
        // 可跨越版本规则
        RuleEntity crossRule = null;
        // filterRules顺序是从小到大的版本
        for (RuleEntity rule : filterRules) {
            SoftwareVersionEntity softwareVersionEntity = softwareVersionService.getById(rule.getSoftwareVersionId());
            // 升级： 规则发布版本序号>入参版本序号
            if (softwareVersionEntity.getVersionNumber() > softwareVersionVo.getVersionNumber()) {
                // 存在不可跨版本 当前规则对于的软件版本则为最小的不可跨软件版本
                // 规则list 是按照software version id从小到大排序 第一个不可跨的的软件版本即为要升级的版本
                if (Objects.equals(softwareVersionEntity.getIsCross(), Constant.CROSS_VERSION.NO_CROSS.getCodeInt())) {
                    noCrossRule = rule;
                    break;
                }
                // 存在多个可跨的版本 -> 判断存在强制版本，取最小的强制版本
                // 规则list 是按照software version id从小到大排序 第一个强制更新的软件版本即为要升级的版本
                // 目前都是全量，可以取最大的强制版本
                if (Objects.equals(softwareVersionEntity.getUpdateType(), Constant.SOFTWARE_UPDATE_TYPE.FORCE_UPDATE.getCodeInt())) {
                    crossRule = rule;
                    break;
                }
                // 多个可跨版本且无强制更新类型，取最大的软件版本
                crossRule = rule;
            }
        }
        if (noCrossRule != null) {
            List<RuleEntity> res = new ArrayList<>();
            res.add(noCrossRule);
            return res;
        }
        if (crossRule != null) {
            List<RuleEntity> res = new ArrayList<>();
            res.add(crossRule);
            return res;
        }
        // 没有匹配到新版本，返回当前版本
        List<RuleEntity> oldestRule = filterRules.stream().filter(obj -> Objects.equals(obj.getSoftwareVersionId(), softwareVersionVo.getId())).collect(Collectors.toList());
        if (!oldestRule.isEmpty()){
            return oldestRule;
        }
        return new ArrayList<>();
    }

    @Transactional
    public void saveMatchRuleRecord(List<RuleEntity> ruleEntityList, Long oldSoftwareVersionId, String result) {
        ruleEntityList.forEach(ruleEntity -> {
            RuleMatchedRecordEntity ruleMatchedRecordEntity = RuleMatchedRecordEntity.builder()
                    .ruleId(ruleEntity.getId())
                    .oldSoftwareVersionId(oldSoftwareVersionId)
                    .result(result)
                    .build();
            ruleMatchedRecordDao.insert(ruleMatchedRecordEntity);
        });
    }

    // 解析条件
    public List<RuleEntity> analysisCondition(final String condition, Map<String, Object> param, final List<RuleEntity> rules, List<RuleEntity> filterRules) {
        // 替换后的
        String condition2 = condition;
        if (condition.contains("||") || condition.contains("&&") || condition.contains("!")) {
            condition2 = condition2.replace("(", " ")
                    .replace(")", " ")
                    .replace("&&", " ")
                    .replace("||", " ")
                    .replace("!", " ");
            List<String> collect = Arrays.stream(condition2.split(" ")).filter(ObjectUtil::isNotNull).collect(Collectors.toList());
            rules.forEach(rule -> {
                String condition3 = condition;
                boolean userBool = false;
                boolean workshopBool = false;
                if (collect.contains("user")) {
                    Long userId = MapUtil.getLong(param, "userId");
                    // 查询当前规则是否应用userId
                    LambdaQueryWrapper<RuleUserEntity> userQuery = new QueryWrapper<RuleUserEntity>().lambda();
                    List<Long> userIds = ruleUserDao.selectList(userQuery.eq(RuleUserEntity::getRuleId, rule.getId()))
                            .stream()
                            .map(RuleUserEntity::getUserId)
                            .toList();
                    userBool = userIds.contains(userId);
                    condition3 = condition3.replaceAll("user", Boolean.toString(userBool));
                }
                if (collect.contains("workshop")) {
                    Long workshopId = MapUtil.getLong(param, "workshopId");
                    LambdaQueryWrapper<RuleWorkshopEntity> userQuery = new QueryWrapper<RuleWorkshopEntity>().lambda();
                    List<Long> workshopIds = ruleWorkshopDao.selectList(userQuery.eq(RuleWorkshopEntity::getRuleId, rule.getId()))
                            .stream()
                            .map(RuleWorkshopEntity::getWorkshopId)
                            .toList();
                    workshopBool = workshopIds.contains(workshopId);
                    condition3 = condition3.replaceAll("workshop", Boolean.toString(workshopBool));
                }
                // 规则范围却不涵盖此用户 ,判断版本类型
//                if (!userBool && !workshopBool){
//                    LambdaQueryWrapper<SoftwareVersionEntity> queryWrapper = new LambdaQueryWrapper<SoftwareVersionEntity>()
//                            .eq(SoftwareVersionEntity::getId,rule.getSoftwareVersionId());
//                    SoftwareVersionEntity softwareVersionEntity = softwareVersionService.getBaseMapper().selectOne(queryWrapper);
//                    // 当前规则对应软件版本强制更新或者不可跨，则强制返回该规则
//                    if (softwareVersionEntity.getUpdateType().equals(Constant.SOFTWARE_UPDATE_TYPE.FORCE_UPDATE.getCodeInt())
//                    ||softwareVersionEntity.getIsCross().equals(Constant.CROSS_VERSION.NO_CROSS.getCodeInt())){
//                        throw new RRException("用户匹配规则有误，软件的强制或不可跨版本未涵盖此用户！！！",1001);
//                    }
//                }
                //使用Jexl工具包 判断逻辑表达式字符串的结果
                JexlEngine engine = new JexlEngine();
                Expression expression = engine.createExpression(condition3);
                Boolean trueCondition = (Boolean) expression.evaluate(new MapContext());
                // 满足表达式的规则
                if (trueCondition) {
                    filterRules.add(rule);
                }
            });
        }
        return filterRules;
    }

    /**
     * 获取DB升级结果
     *
     * @param ruleId       规则id
     * @param oldVersionId 要升级的版本id
     * @return
     */
    public String getCacheDataByRuleIdAndOldVersionId(Long ruleId, Long oldVersionId) {
        RuleMatchedRecordEntity entity = ruleMatchedRecordDao.selectOne(new QueryWrapper<RuleMatchedRecordEntity>()
                .select("result")
                .eq("rule_id", ruleId)
                .eq("old_software_version_id", oldVersionId));
        if (entity == null) {
            return null;
        }
        return entity.getResult();
    }

    public List<RuleEntity> getNewestRuleBySoftwareId(Long softwareId) {
        RuleEntity newestRule = ruleService.getSoftwareNewestRuleBySoftwareId(softwareId);
        List<RuleEntity> res = new ArrayList<>();
        res.add(newestRule);
        return res;
    }

//    public RuleEntity getSoftwareNewestRuleBySoftwareId(Long softwareId) {
//        return ruleService.getSoftwareNewestRuleBySoftwareId(softwareId);
//    }

    public Boolean checkPoolByFilter(RemoteVehiclePoolEntity pool, Map<String, Object> filter){
        // 获取详情
        pool = remoteVehiclePoolService.getInfo(pool.getId());
        // 替换后的
        String condition2 = RuleComponent.VEHICLE_GROUP_REG;
        condition2 = condition2.replace("(", " ")
                .replace(")", " ")
                .replace("&&", " ")
                .replace("||", " ")
                .replace("!", " ");
        List<String> collect = Arrays.stream(condition2.split(" ")).filter(ObjectUtil::isNotNull).collect(Collectors.toList());
        // collect条件判断
        String condition3 = RuleComponent.VEHICLE_GROUP_REG;
        if (collect.contains("vin")) {
            String vin = MapUtil.getStr(filter, "vin");
            List<String> vinListByPool = rVehicleTagDao.getVinListByPoolId(pool.getId());
            boolean vinBool = vinListByPool.contains(vin);
            condition3 = condition3.replaceAll("vin", Boolean.toString(vinBool));
        }
        if (collect.contains("vehicleType")) {
            List<Long> vehicleTypeIds = (List<Long>) MapUtils.getObject(filter, "vehicleType");
            pool.getVehicleTypeIds().retainAll(vehicleTypeIds);
            boolean poolBool = !pool.getVehicleTypeIds().isEmpty();
            condition3 = condition3.replaceAll("vehicleType", Boolean.toString(poolBool));
        }
        //vehicle 查配置表 生产日期，产地，销售区域（sys_config）
//        if (collect.contains("vehicle")) {
//            String vehicle = MapUtil.getStr(filter, "vehicle");
//            boolean vinBool = false;
//            condition3 = condition3.replaceAll("vehicle", Boolean.toString(vinBool));
//        }
        // 生产日期
        if (collect.contains("productionDate")) {
            String productionDate = MapUtil.getStr(filter, "productionDate");
            Map<String, String> map = pool.getProductionDate() == null || pool.getProductionDate().size() == 0 ? new HashMap<>() :
                    sysConfigDao.selectList(Wrappers.<SysConfigEntity>lambdaQuery().
                                    in(SysConfigEntity::getId, pool.getProductionDate()).select(SysConfigEntity::getParamValue)).
                            stream().collect(Collectors.toMap(SysConfigEntity::getParamValue, SysConfigEntity::getParamValue));
            boolean vinBool = map.size() != 0 && map.containsKey(productionDate);
            condition3 = condition3.replaceAll("productionDate", Boolean.toString(vinBool));
        }
        // 产地
        if (collect.contains("placeOfProduction")) {
            String placeOfProduction = MapUtil.getStr(filter, "placeOfProduction");
            Map<String, String> map = pool.getPlaceOfProduction() == null || pool.getPlaceOfProduction().size() == 0 ? new HashMap<>() :
                    sysConfigDao.selectList(Wrappers.<SysConfigEntity>lambdaQuery().
                                    in(SysConfigEntity::getId, pool.getPlaceOfProduction()).select(SysConfigEntity::getParamValue)).
                            stream().collect(Collectors.toMap(SysConfigEntity::getParamValue, SysConfigEntity::getParamValue));

            boolean vinBool = map.size() != 0 && map.containsKey(placeOfProduction);
            condition3 = condition3.replaceAll("placeOfProduction", Boolean.toString(vinBool));
        }

        // 校验版本
        if (collect.contains("version")) {
            List<Long> version = (List<Long>) MapUtils.getObject(filter, "version");
            List<Long> poolVersionIds = pool.getVersions().stream().map(RemoteVehicleVersionEntity::getId).collect(Collectors.toList());
            poolVersionIds.retainAll(version);
            boolean versionPool = !poolVersionIds.isEmpty();
            condition3 = condition3.replaceAll("version", Boolean.toString(versionPool));
        }
        // 校验ECU分组
        if (collect.contains("ecuVersion")) {
            List<Long> ecuGroup = (List<Long>) MapUtils.getObject(filter, "ecuVersion");
            List<Long> groupIds = pool.getEcuVersions().stream().map(RemoteVehicleEcuVersionEntity::getId).collect(Collectors.toList());
            groupIds.retainAll(ecuGroup);
            boolean poolBool = !groupIds.isEmpty();
            condition3 = condition3.replaceAll("ecuVersion", Boolean.toString(poolBool));
        }
        //使用Jexl工具包 判断逻辑表达式字符串的结果
        JexlEngine engine = new JexlEngine();
        Expression expression = engine.createExpression(condition3);
        Boolean trueCondition = (Boolean) expression.evaluate(new MapContext());

        return trueCondition;
    }
}
