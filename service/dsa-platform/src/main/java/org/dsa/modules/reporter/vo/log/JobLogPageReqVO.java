package org.dsa.modules.reporter.vo.log;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.modules.reporter.pojo.PageParam;
import java.util.Date;


@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class JobLogPageReqVO extends PageParam {

    private Long jobId;

    private String handlerName;


    private Date beginTime;


    private Date endTime;

    private Integer status;

}
