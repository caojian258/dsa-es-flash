package org.dsa.modules.onlineUpdate.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.dsa.modules.onlineUpdate.utils.BaseEntity;

/**
 * ${comments}
 * 
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("o_component_version")
public class ComponentVersionEntity extends BaseEntity {

	/**
	 * 组件id
	 */
	private Long componentId;
	/**
	 * 版本名称
	 */
	private String versionName;
	/**
	 * 版本序列号
	 */
	private Long versionNumber;
	/**
	 * 强制更新/可选;0为强制更新；1为可选更新
	 */
	private Integer updateType;
	/**
	 * 版本类型
	 */
	private Integer versionType;
	/**
	 * 基础版本id
	 */
	private Long baseVersionId;
	/**
	 * 开发/测试/拒绝/释放;0开发；1测试；2释放；3拒绝
	 */
	private Integer status;
	/**
	 * 组件地址;组件版本文件保存路径
	 */
	private String url;
	/**
	 * 版本文件大小
	 */
	private Long fileSize;
	/**
	 * 版本文件MD5;文件md5字符串，用于文件完整性校验
	 */
	private String fileMd5;
	/**
	 * 版本说明ti;版本说明多语言key
	 */
	private String releaseNoteTi;
	/**
	 * 描述ti
	 */
	private String descriptionTi;

	/**
	 * 版本唯一标识
	 */
	private String uuid;

	/**
	 * 版本是否被发布 1 发布
	 */
	@TableField(exist = false)
	private Boolean isPublish;

}
