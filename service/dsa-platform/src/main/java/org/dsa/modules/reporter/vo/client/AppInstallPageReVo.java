package org.dsa.modules.reporter.vo.client;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.modules.reporter.pojo.PageParam;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class AppInstallPageReVo extends PageParam {

    /***
     * 客户端ID
     */
    private String pcid;

    /***
     * 标签
     */
    private String tag;

    /***
     * 安装消息
     */
    private String message;

    /***
     * 名称
     */
    private String name;
}
