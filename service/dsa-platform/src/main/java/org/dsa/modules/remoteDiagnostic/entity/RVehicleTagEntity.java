package org.dsa.modules.remoteDiagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("r_vehicle_tag")
public class RVehicleTagEntity {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;

    private String vin;

    private Long tagId;

    public RVehicleTagEntity() {
    }

    public RVehicleTagEntity(String vin, Long tId) {
        this.vin = vin;
        this.tagId = tId;
    }
}
