package org.dsa.modules.remoteDiagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.dsa.modules.remoteDiagnostic.pojo.BaseEntity;


@Data
@TableName("r_flash_campaign")
public class FlashCampaignEntity extends BaseEntity {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String vin;

    private String vehicleVersion;

    private Long vehicleVersionId;

    private Long userId;
}
