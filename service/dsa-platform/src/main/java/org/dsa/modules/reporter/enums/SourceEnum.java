package org.dsa.modules.reporter.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SourceEnum {

    ONBOARD("001"),
    OFFLINE("002"),
    REMOTE("003"),
    UNKNOWN("N/A");

    @EnumValue
    private final String value;
}
