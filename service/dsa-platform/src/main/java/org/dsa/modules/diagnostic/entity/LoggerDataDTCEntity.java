package org.dsa.modules.diagnostic.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 离线、车载日志接受类
 *
 */
@Data
public class LoggerDataDTCEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ECU名称数组
     */
    private String ecuName;
    /**
     * 故障码Code
     */
    private String dtcCode;
    /**
     * 故障码CodeHex
     */
    private String dtcCodeHex;
    /**
     * 故障码Code , Int型
     */
    private String dtcCode_Int;
    /**
     * 故障码描述多语言标识
     */
    private String ti;
    /**
     * 故障码描述
     */
    private String description;
    /**
     * 故障码状态
     */
    private String status;


}
