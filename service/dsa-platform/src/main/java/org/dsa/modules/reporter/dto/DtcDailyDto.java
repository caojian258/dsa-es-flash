package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class DtcDailyDto implements Serializable {

    private String ecuName;

    private String dtcCode;

    private String dtcCodeInt;

    private String dtcCodeHex;

    //dtc description ti
    private String ti;

    private String description;

    private String dtcDay;

    private Long dtcTotal;
}
