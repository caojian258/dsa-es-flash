package org.dsa.modules.diagnostic.util;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.ZipUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.exception.RRException;
import org.dsa.common.pojo.RowMapper;
import org.dsa.common.pojo.SynLogger;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.DateUtils;
import org.dsa.common.utils.ObjectUtils;
import org.dsa.common.utils.SqliteUtils;
import org.dsa.modules.diagnostic.dao.*;
import org.dsa.modules.diagnostic.entity.*;
import org.dsa.modules.sys.entity.UserInfoEntity;
import org.dsa.modules.sys.utils.YCUserUtils;
import org.dsa.modules.vehicle.dao.DVehicleInfoDao;
import org.dsa.modules.vehicle.entity.DVehicleInfoEntity;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * 离线日志，车载日志工具类
 */

@Component
@Slf4j
public class LogsUtils {

//    private static final Log log = LogFactory.get();
    @Resource
    private DSessionInfoDao dSessionInfoDao;
    @Resource
    private DSessionActionDao dSessionActionDao;
    @Resource
    private DActionInfoDao dActionInfoDao;
    @Resource
    private DActionDtcDao dActionDtcDao;
    @Resource
    private DActionEcuDao dActionEcuDao;
    @Resource
    private DActionVersionDao dActionVersionDao;
    @Resource
    private DVehicleInfoDao dVehicleInfoDao;

    @Value("${file.record.path}")
    private String recordPath;
    @Value("${file.synlogger.path}")
    private String synloggerPath;

    /**
     * @description 开始记录日志
     * @param params params.source来源
     */
    public void startLogging(Map<String, Object> params){
        try {
            log.info("LogUtils start operation log");
            String source = (String) params.get("source");
            if (Constant.SESSION_SOURCE.VEHICLE.getDbName().equals(source)){
                log.info("VEHICLE: Start synchronizing...");
                HttpServletRequest req = (HttpServletRequest) params.get("req");
                File saveFile = (File) params.get("file");
                //session校验更新
                updateSession(saveFile,req,Constant.SESSION_SOURCE.VEHICLE.getDbName());
                //操作日志文件
                String sqliteSuffix = ".sqlite";
                if (saveFile.getName().endsWith(sqliteSuffix)){
                    operateLogFile(saveFile,source);
                }
                log.info("VEHICLE: Complete synchronizing...");
            }else {
                log.info("OFFLINE: Start synchronizing...");
                List <File> sqliteFiles = getOfflineLogFile();
                //遍历操作日志文件
                sqliteFiles.forEach(file -> {
                    operateLogFile(file,source);
                });
                log.info("OFFLINE: Complete synchronizing...");
            }
            log.info("LogUtils complete operation log");
        }catch (Exception ex){
            log.error("LogUtils startLogging fail,{}",ex.getMessage());
            saveErrorLog();
        }

    }

    /**
     * @description 获取离线日志中.sqlite文件
     * @return List<File>
     */
    public List<File> getOfflineLogFile() {
        log.info("getOfflineLogFile()->start");
        List<File> sqliteFiles = new ArrayList<>(16);
        try {
            String dateStr = DateUtils.format(new Date(), "yyyyMMdd");

            if(!FileUtil.exist(synloggerPath + "/" + dateStr)){
                return sqliteFiles;
            }
            File sourceFolder = new File(synloggerPath + "/" + dateStr);
            //日志文件过滤条件:文件最后修改时间:当前时间+1小时 至 当前时间+2小时.
            long startTime = System.currentTimeMillis() - 2 * 60 * 60 * 1000;
            long endTime = System.currentTimeMillis() - 60 * 60 * 1000;
            //1.查找zip文件、解压zip文件
            List<File> unZipFiles = new ArrayList<>(16);
            Arrays.stream(Objects.requireNonNull(sourceFolder.listFiles()))
                    .filter(file -> file.getName().startsWith("Session_")
                            && file.getName().endsWith(".zip")
//                            && file.lastModified() > startTime
//                            && file.lastModified() < endTime
                    )
                    .forEach(file -> {
                        try {
                            //查看session更新log_path
                            updateSession(file, null, Constant.SESSION_SOURCE.OFFLINE.getDbName());
                            File unzips = ZipUtil.unzip(file);
                            unZipFiles.addAll(FileUtil.loopFiles(unzips));
                        }catch (Exception e){
                            log.error("getOfflineLogFile() fail,errMsg {}",e.getMessage());
                        }
                    });

            //获取解压文件中的VIN_开头的sqlite文件
            unZipFiles.stream()
                    .filter(f -> f.getName().startsWith("VIN_")
                                && f.getName().endsWith(".sqlite"))
                    .forEach(sqlite->{
                        //更新vin
                        try {
                                updateSession(sqlite,null,Constant.SESSION_SOURCE.OFFLINE.getDbName());
                                sqliteFiles.add(sqlite);
                            }catch (Exception e){
                                log.error("getOfflineLogFile() fail,errMsg {}",e.getMessage());
                            }

                        });

        }catch (Exception e){
            log.error("getOfflineLogFile() fail,errMsg {}",e.getMessage());
            saveErrorLog();
        }
        log.info("getOfflineLogFile()->end; return logFiles {}", sqliteFiles);
        return sqliteFiles;
    }


    /**
     * @description 操作日志文件
     * @param file
     */
    private void operateLogFile(File file,String source) {
        try {
            log.info("operateLogFile()->start");
            String[] names = file.getName().split("_");
            String sessionId  = names[2];;
            if (Constant.SESSION_SOURCE.VEHICLE.getDbName().equals(source)){
                if (!checkVin(names[1])){
                    saveInitVehicleInfo(names[1]);
                }
            }
            //解析操作记录
            List<SynLogger> synLoggers = readSqlite(file);

            synLoggers.forEach(this::saveSyncLog);
            log.info("operateLogFile()->end");
        }catch (Exception e){
            log.error("read or save {} fail",file.getName());
            log.error("operateLogFile():errMsg,{}",e.getMessage());
            saveErrorLog();
        }

    }

    private int IDFunctionCount= 0;
    /**
     * @description 日志入库
     * @param synLogger
     */
    private void saveSyncLog(SynLogger synLogger) {
        log.info("saveSyncLog()->start:");
        //action_id 4位随机数加时间戳
        String actionId = RandomUtil.randomString(4) + System.currentTimeMillis();
        synLogger.setActionId(actionId);
        //保存d_session_action、d_action_info
        saveSessionAction(synLogger);
        saveActionInfo(synLogger);
        //第一条记录更新session(结束时间)和明细
        if (ObjectUtils.isNotNull(synLogger.getSessionEndTime())){
            versionMap.clear();
            ecuList.clear();
            updateSession(synLogger);
            //存详情
            saveDiagnosticDetail(synLogger);
        }

        //离线诊断日志关联版本
        if ("IDENTIFICATION".equals(synLogger.getFunctionCode())&&IDFunctionCount==0
                &&synLogger.getSource().equals(Constant.SESSION_SOURCE.OFFLINE.getValue())){
            ecuList.forEach(dActionEcuEntity -> {
                dActionEcuEntity.setActionId(actionId);
                dActionEcuDao.insert(dActionEcuEntity);

            });
            versionMap.keySet().forEach(key->{
                versionMap.get(key).stream().filter(map->ObjectUtils.isNotNull(map.get("ti")))
                        .forEach(map->{
                            //key为version对应的ecu
                            synLogger.setEcuName(key);
                            saveActionVersion(map,synLogger);
                        });
            });
            IDFunctionCount++;
        }

        log.info("saveSyncLog()->end");
    }

    private final List<DActionEcuEntity> ecuList = new ArrayList<>(16);
    private final Map<String,List<Map<String, String>>> versionMap = new HashMap<>(16);
    /**
     * @description save diagnostic detail
     * @param  synLogger
     */
    private void saveDiagnosticDetail(SynLogger synLogger) {
        log.info("saveDiagnosticDetail()->start:");
        String source = Constant.SESSION_SOURCE.getDbNameByValue(synLogger.getSource());
        try {
            List<SynLogger> details = synLogger.getDetails();
            details.forEach(info->{
                String jsonData = info.getJsonData();
                Map<String, String> json = JSON.parseObject(jsonData, new TypeReference<Map<String, String>>() {});
                boolean condition = Constant.SESSION_SOURCE.VEHICLE.getDbName().equals(source);
                boolean conditionA;
                boolean conditionB;
                if (condition){
                    conditionA = "DTC".equals(info.getFunctionCode()) && json.containsKey("dtcData");
                    conditionB = "VHSS".equals(info.getFunctionCode()) && json.containsKey("didData");
                }else {
                    conditionA = ("DTC".equals(info.getFunctionCode())||"VHSS".equals(info.getFunctionCode()))&& json.containsKey("dtcData");
                    conditionB = "VEHICLE_REPORT".equals(info.getFunctionCode())&& json.containsKey("didData") ;
                }
                if (conditionA){
                    synLogger.setCreateTime(info.getCreateTime());
                    List<LoggerDataDTCEntity> detailList = JSONObject.parseArray(json.get("dtcData"), LoggerDataDTCEntity.class);
                    if (ObjectUtils.isNotNull(detailList)){
                        detailList.stream()
                                .filter(detail-> ObjectUtils.isNotNull(detail.getTi()))
                                .forEach(detail->{
                                    saveDTC(detail,synLogger);
                                });
                    }
                }
                if (conditionB){
                    LoggerDataDTCEntity dtcEntity = new LoggerDataDTCEntity();
                    List<Map<String, String>> didList =  new ArrayList<>();
                    //车载离线的jsonData数据结构问题
                    if (condition){
                        dtcEntity.setEcuName(json.get("ecuName"));
                        dtcEntity.setDescription(json.get("dtcNumber"));
                        dtcEntity.setStatus(json.get("onlineStatus"));
                        List<Map<String, String>> vDid = JSON.parseObject(json.get("didData"), new TypeReference<List<Map<String, String>>>() {});
                        if (ObjectUtils.isNotNull(vDid)){
                            didList.addAll(vDid);
                        }
                    }else {
                        Map<String, String> didData = JSON.parseObject(json.get("didData"), new TypeReference<Map<String, String>>() {});
                        dtcEntity.setEcuName(didData.get("ecuName"));
                        dtcEntity.setDescription(didData.get("dtcNumber"));
                        dtcEntity.setStatus(didData.get("onlineStatus"));
                        List<Map<String, String>> oDid = JSON.parseObject(didData.get("didList"), new TypeReference<List<Map<String, String>>>() {});
                        if (ObjectUtils.isNotNull(oDid)){
                            didList.addAll(oDid);
                        }
                    }
                    //DTC已经分离出来在conditionA条件下存储
//                    saveDTC(dtcEntity,synLogger);
                    synLogger.setLastUpdateTime(info.getLastUpdateTime());
                    synLogger.setEcuName(dtcEntity.getEcuName());
                    //版本信息 存到集合，第一次出现IDENTIFICATION时再存 ecuList
                    if ("VEHICLE_REPORT".equals(info.getFunctionCode())){
                        DActionEcuEntity dActionEcuEntity = new DActionEcuEntity();
                        dActionEcuEntity.setEcuName(info.getEcuName());
                        //description存dtcNumber故障码数量
                        dActionEcuEntity.setDescription(dtcEntity.getDescription());
                        dActionEcuEntity.setStatus(Convert.toInt(dtcEntity.getStatus()));
                        dActionEcuEntity.setEndTime(DateUtil.parseDate(synLogger.getLastUpdateTime()));
                        versionMap.put(dtcEntity.getEcuName(),didList);
                        ecuList.add(dActionEcuEntity);
                    }else{
                        saveEcu(dtcEntity,synLogger);
                        didList.stream()
                                .filter(map->ObjectUtils.isNotNull(map.get("ti")))
                                .forEach(map->{
                                    saveActionVersion(map,synLogger);
                                });
                    }
                    
                }
            });
            log.info("saveDiagnosticDetail()->end");
        }catch (Exception ex){
            log.error("saveDiagnosticDetail()->save {} detail fail {}",source,ex.getMessage());
            saveErrorLog();
        }
    }


    /**
     * @description 校验session文件是否已经同步
     * @param sessionId
     */
    public void checkSessionActions(String sessionId) {
        //同一sessionId日志不重复保存
        List<String> actionIds = dSessionActionDao.selectListBySessionId(sessionId);
        if(ObjectUtils.isNotNull(actionIds) && actionIds.size()>0){
            throw new RRException(sessionId+" logs already sync");
        }
    }

    /**
     * @description 没有vin则新建vehicleInfo
     * @param vin
     */
    public void saveInitVehicleInfo(String vin) {
        try {
            log.info("LogUtils->saveVehicleInfo(): vehicle sync save vehicle");
            DVehicleInfoEntity dVehicleInfo = new DVehicleInfoEntity();
            dVehicleInfo.setVin(vin);
            dVehicleInfo.setVehicleTypeId(0L);
            dVehicleInfo.setUpdateTime(new Date());
            dVehicleInfoDao.insert(dVehicleInfo);
        }catch (Exception e){
            log.error("saveInitVehicleInfo() fail:errMsg: {}",e.getMessage());
            saveErrorLog();
        }

    }


    /**
     * @description 校验sessionId是否已存在
     * @param sessionId sessionId
     * @return boolean
     */
    public boolean checkSessionId(@NonNull String sessionId) {
        return ObjectUtils.isNotNull(dSessionInfoDao.checkUnique(sessionId));
    }

    /**
     * @description 校验vin码
     * @param vin vin码
     * @return boolean
     */
    public boolean checkVin(@NonNull String vin) {
        return ObjectUtils.isNotNull(dVehicleInfoDao.selectByVin(vin));
    }

    /**
     * @description 解析sqlite文件获取数据
     * @param file
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    private List<SynLogger> readSqlite(File file) throws SQLException, ClassNotFoundException {
        log.info("readSqlite({})->start",file.getName());
        SqliteUtils sqliteUtils = new SqliteUtils(file.getAbsolutePath());
        String vin = file.getName().split("_")[1];
        String sessionId = file.getName().split("_")[2];

        //先判断版本
        List<Integer> versions =  sqliteUtils.executeQuery("select version from t_version order by create_time desc limit 1", new RowMapper<Integer>() {
            @Override
            public Integer mapRow(ResultSet rs, int index) throws SQLException {
                return rs.getInt("version");
            }
        });
        List<SynLogger> info = new ArrayList<>();
        Integer version=versions.get(0);
        // 版本小于101 不查t_diagnostic_detail(低版本没有这张表)
        if(version>100){
            // 读取诊断明细信息,只记录最后ecu的详情信息
            List<SynLogger> detail = sqliteUtils.executeQuery("select * from t_diagnostic_detail", new RowMapper<SynLogger>() {
                @Override
                public SynLogger mapRow(ResultSet rs, int index) throws SQLException {
                    SynLogger synLogger = new SynLogger();
                    synLogger.setId(rs.getInt("id"));
                    synLogger.setSessionId(sessionId);
                    synLogger.setFunctionCode(rs.getString("function_code"));
                    synLogger.setActionCode(rs.getString("action"));
                    synLogger.setSource(rs.getString("source"));
                    synLogger.setCreateTime(rs.getString("create_time"));
                    synLogger.setLastUpdateTime(rs.getString("last_update_time"));
                    synLogger.setEcuName(rs.getString("ecu_name"));
                    synLogger.setJsonData(rs.getString("json_data"));
                    return synLogger;
                }
            });
            info.addAll(detail);
        }
        //查询最大结束时间作为会话结束时间
        List<String> sessionEndTimes =  sqliteUtils.executeQuery("select session_id, end_time from t_diagnostic order by end_time desc limit 1", new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int index) throws SQLException {
                return rs.getString("end_time");
            }
        });

        List<SynLogger> res = sqliteUtils.executeQuery("select * from t_diagnostic order by start_time", new RowMapper<SynLogger>() {
            @Override
            public SynLogger mapRow(ResultSet rs, int index) throws SQLException {
                SynLogger synLogger = new SynLogger();
                synLogger.setVin(vin);
                synLogger.setLogPath(file.getAbsolutePath());
                synLogger.setId(rs.getInt("id"));
                synLogger.setUserName(rs.getString("user_id"));
                //synLogger.setSessionId(rs.getString("session_id"));
                synLogger.setSessionId(sessionId);
                synLogger.setFunctionCode(rs.getString("function_code"));
                synLogger.setActionCode(rs.getString("action"));
                synLogger.setTitleEn(rs.getString("title_en"));
                synLogger.setTitleCn(rs.getString("title_cn"));
                synLogger.setStartTime(rs.getString("start_time"));
                synLogger.setEndTime(rs.getString("end_time"));
                String result = rs.getString("result");
                if (("0".equals(result))) {
                    synLogger.setResult("1");
                } else {
                    synLogger.setResult("2");
                }
                synLogger.setResultMessageEn(rs.getString("result_message_en"));
                synLogger.setResultMessageCn(rs.getString("result_message_cn"));
                synLogger.setSource(rs.getString("source"));
                //将详情和sessionEndTime存在第一条记录中
                if (index==2){
                    if (sessionEndTimes.size()>0){
                        synLogger.setSessionEndTime(sessionEndTimes.get(0));
                    }
                    synLogger.setDetails(info);
                }
                return synLogger;
            }
        });
        log.info("diagnostic detail,{}",info.toString());
        sqliteUtils.destroyed();
        return res;
    }

    /**
     * @param file      日志文件
     * @param req       req
     * @param source    来源 OFFLINE VEHICLE
     */
    public void updateSession(File file, HttpServletRequest req, String source) {
        try {

            log.info("updateSession()->start {}",file.getName());
            String[] names = file.getName().split("_");
            //  fileNameFormat :
            //  vehicle: VIN_${vin}_${sessionId}_${userId}.sqlite.zip
            //  offline: Session_${sessionId}_${username}_${yyyymmddHHMMSS}.zip
            if (names.length < 4) {
                throw new RuntimeException("file name format is error!");
            }
            
            DSessionInfoEntity dSessionInfoEntity = new DSessionInfoEntity();
            String token = "";

            if ("VEHICLE".equals(source)) {
                dSessionInfoEntity.setSource("VEHICLE");
                dSessionInfoEntity.setVin(names[1]);
                dSessionInfoEntity.setSessionId(names[2]);
                //防止空文件case,设置初始时间
                dSessionInfoEntity.setStartTime(new Date());
                dSessionInfoEntity.setEndTime(DateUtil.offsetMillisecond(new Date(), 1000));
                //如果header中不存在token，则从参数中获取token
                token = StringUtils.isBlank(req.getHeader("token")) ? (req.getParameter("token")) : req.getHeader("token");
                //获取调用接口的用户信息
                UserInfoEntity userInfo = null;
                if (ObjectUtils.isNotNull(userInfo)) {
                    dSessionInfoEntity.setDiagnosticUserId(Convert.toLong(userInfo.getUserId()));
                    dSessionInfoEntity.setDiagnosticDisplayUserName(userInfo.getWorkShop() + "-" + userInfo.getUserDisplayName());
                }
            } else {
                dSessionInfoEntity.setSource("OFFLINE");
                if(file.getName().endsWith(".sqlite")){
                    dSessionInfoEntity.setSessionId(names[2]);
                    dSessionInfoEntity.setVin(names[1]);
                    dSessionInfoEntity.setDiagnosticDisplayUserName(names[3]);
                }else {
                    dSessionInfoEntity.setSessionId(names[1]);
                    dSessionInfoEntity.setVin("");
                }
            }
            checkSessionActions(dSessionInfoEntity.getSessionId());
            if (!checkSessionId(dSessionInfoEntity.getSessionId())){
                //存zip文件，车载存在先上传.sqlite的情况，此时不存logPath
                if (file.getName().endsWith(".zip")) {
                    dSessionInfoEntity.setLogName(file.getName());
                    dSessionInfoEntity.setLogPath(file.getAbsolutePath());
                }
                dSessionInfoDao.insert(dSessionInfoEntity);
            }else {
                //已存在session则更新日志路径
                DSessionInfoEntity entity = new DSessionInfoEntity();
                entity.setSessionId(dSessionInfoEntity.getSessionId());
                entity.setVin(dSessionInfoEntity.getVin());
                //存zip文件，车载存在先上传.sqlite的情况，此时不存logPath
                if (source.equals(Constant.SESSION_SOURCE.VEHICLE.getDbName()) && file.getName().endsWith(".zip")){
                    entity.setLogName(file.getName());
                    entity.setLogPath(file.getAbsolutePath());
                }
                if (ObjectUtils.isNotNull(entity.getVin())){
                    dSessionInfoDao.updateBySessionId(entity);
                }
            }
            log.info("updateSession()->end");
        } catch (Exception ex) {
            log.error("LogsUtils->saveSession() fail,errMsg: {}",ex.getMessage());
            saveErrorLog();
            //异常抛出,
            throw new RRException(ex.getMessage());
        }
    }

    /**
     *
     */
    private void saveErrorLog(){

    }

    private void saveActionVersion(Map<String, String> map,SynLogger synLogger) {
        try {
            DActionVersionEntity dActionVersionEntity = new DActionVersionEntity();
            dActionVersionEntity.setEcuName(synLogger.getEcuName());
            dActionVersionEntity.setName(map.get("name"));
            dActionVersionEntity.setTi(map.get("ti"));
            dActionVersionEntity.setValue(map.get("value"));
            dActionVersionEntity.setActionId(synLogger.getActionId());
            dActionVersionEntity.setSessionId(synLogger.getSessionId());
            dActionVersionEntity.setDid("");
            dActionVersionDao.insert(dActionVersionEntity);
        }catch (Exception e){
            log.error("saveActionVersion() fail,{}",e.getMessage());
            saveErrorLog();
        }
    }

    private void saveEcu(LoggerDataDTCEntity info,SynLogger synLogger) {
        try {
            DActionEcuEntity dActionEcuEntity = new DActionEcuEntity();
            dActionEcuEntity.setEcuName(info.getEcuName());
            dActionEcuEntity.setActionId(synLogger.getActionId());
            //description存dtcNumber故障码数量
            dActionEcuEntity.setDescription(info.getDescription());
            dActionEcuEntity.setStatus(Convert.toInt(info.getStatus()));
            dActionEcuEntity.setEndTime(DateUtil.parseDate(synLogger.getLastUpdateTime()));
            dActionEcuDao.insert(dActionEcuEntity);
        }catch (Exception e){
            log.error("saveEcu() fail,{}",e.getMessage());
            saveErrorLog();
        }

    }


    private void saveDTC(LoggerDataDTCEntity info,SynLogger synLogger) {
        try {
            DActionDtcEntity dActionDtcEntity = new DActionDtcEntity();
            dActionDtcEntity.setSessionId(synLogger.getSessionId());
            dActionDtcEntity.setActionId(synLogger.getActionId());
            dActionDtcEntity.setStatus(info.getStatus());
            dActionDtcEntity.setDtcCode(info.getDtcCode());
            dActionDtcEntity.setDtcCodeInt(info.getDtcCode_Int());
            dActionDtcEntity.setDtcCodeHex(info.getDtcCodeHex());
            dActionDtcEntity.setEcuName(info.getEcuName());
            dActionDtcEntity.setTi(info.getTi());
            dActionDtcEntity.setCreateTime(DateUtil.parse(synLogger.getCreateTime(), DatePattern.NORM_DATETIME_PATTERN));
            dActionDtcEntity.setDescription(info.getDescription() == null ? "0" : info.getDescription());

            dActionDtcDao.insert(dActionDtcEntity);
        }catch (Exception e){
            log.error("saveDTC() fail {}",e.getMessage());
            saveErrorLog();
        }

    }

    private void saveActionInfo(SynLogger synLogger) {
        try {
            DActionInfoEntity dActionInfoEntity = new DActionInfoEntity();
            dActionInfoEntity.setActionId(synLogger.getActionId());
            dActionInfoEntity.setActionCode(synLogger.getActionCode());
            dActionInfoEntity.setFunctionCode(synLogger.getFunctionCode());
            dActionInfoEntity.setStartTime(DateUtils.stringToDate(synLogger.getStartTime(), "yyyy-MM-dd HH:mm:ss"));
            dActionInfoEntity.setEndTime(DateUtils.stringToDate(synLogger.getEndTime(), "yyyy-MM-dd HH:mm:ss"));
            dActionInfoEntity.setCreateTime(DateUtils.stringToDate(synLogger.getStartTime(), "yyyy-MM-dd HH:mm:ss"));
            dActionInfoEntity.setResults(synLogger.getResult());
            dActionInfoEntity.setMessageCn(synLogger.getResultMessageCn());
            dActionInfoEntity.setMessageEn(synLogger.getResultMessageEn());
            dActionInfoDao.insert(dActionInfoEntity);
        }catch (Exception e){
            log.error("saveActionInfo() fail {}",e.getMessage());
            saveErrorLog();
        }
    }

    private void saveSessionAction(SynLogger synLogger) {
        try {
            DSessionActionEntity dSessionActionEntity = new DSessionActionEntity();
            dSessionActionEntity.setSessionId(synLogger.getSessionId());
            dSessionActionEntity.setActionId(synLogger.getActionId());
            dSessionActionEntity.setCreateTime(new Date());
            dSessionActionDao.insert(dSessionActionEntity);
        }catch (Exception e){
            log.error("saveSessionAction() fail {}",e.getMessage());
            saveErrorLog();
        }
    }
    private void updateSession(SynLogger synLogger) {
        try {
            DSessionInfoEntity entityInfo = new DSessionInfoEntity();
            entityInfo.setSessionId(synLogger.getSessionId());
            entityInfo.setStartTime(DateUtils.stringToDate(synLogger.getStartTime(),"yyyy-MM-dd HH:mm:ss"));
            entityInfo.setEndTime(DateUtils.stringToDate(synLogger.getSessionEndTime(),"yyyy-MM-dd HH:mm:ss"));
            dSessionInfoDao.updateBySessionId(entityInfo);
        }catch (Exception e){
            log.error("updateSession() fail {}",e.getMessage());
            saveErrorLog();
        }
    }

}
