package org.dsa.modules.remoteDiagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticConditionEntity;

import java.util.List;

@Mapper
public interface DiagnosticConditionDao extends BaseMapper<DiagnosticConditionEntity> {


//    @Select(
//            " <script> "
//                    +" select i.id,i.condition_name,i.condition_val,i.unity,j.min_val,j.max_val,j.eq_val,j.in_val,j.condition_code"
//                    +" from r_diag_condition i left join r_task_condition j on i.id = j.raw_id  where i.status = 0 "
//                    +" and j.task_id = #{taskId} "
//                    + " <if test='ids!=null and ids.size!=0 '>"
//                    + " and i.id in  "
//                    + " <foreach collection='ids' item='item' open=\"(\" separator=\",\" close=\")\" >"
//                    + " #{item} "
//                    + " </foreach> "
//                    + " </if> "
//                    + " </script>")
    List<DiagnosticConditionEntity> getListByTask(@Param("ids") List<Long> ids,@Param("taskId") Long taskId);
}
