package org.dsa.modules.vehicle.dto;

import lombok.Getter;
import lombok.Setter;
import org.dsa.modules.vehicle.entity.DVehicleInfoEntity;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author weishunxin
 * @since 2023-05-31
 */
@Getter
@Setter
public class VehicleInfoDTO extends DVehicleInfoEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 车型平台
     */
    private String vehiclePlatform;

    /**
     * 品牌
     */
    private String make;

    /**
     * 品牌
     */
    private String brand;

    /**
     * 配置级别
     */
    private String trimLevel;

    /**
     * 生产年份
     */
    private String year;

    /**
     * 车型编码
     */
    private String modelCode;

    /**
     * 下线日期
     */
    private String factoryDate;

    /**
     * 产地
     */
    private String placeOfProductionCode;

    /**
     * 销售区域
     */
    private String regionCode;

    /**
     * 销售城市
     */
    private String cityCode;

    /**
     * 4S店名称
     */
    private String workshopName;

    /**
     * 图片
     */
    private String vehiclePicture = "";

    /**
     * ECU列表
     */
    private List<String> ecuList;

    /**
     * sdgList
     */
    private List<VehicleTypeSdgDto> oemVehicleInfoList;

}
