package org.dsa.modules.reporter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@TableName("d_workshop_user_groups")
public class WorkshopUserGroups extends BaseEntity{

    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer groupId;

    private String groupName;

    private Long userId;

    private String username;

}
