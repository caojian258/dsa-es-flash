package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.dsa.modules.reporter.entity.SystemMetadata;

@Mapper
public interface SystemMetadataMapper extends BaseMapper<SystemMetadata> {

    public void deleteByItem(String item);
}
