package org.dsa.modules.reporter.dto;


import lombok.Data;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicInteger;

@Data
public class WorkshopFileDto implements Serializable {

    private Integer workshopTotal;

    private Integer workshopGroupTotal;

    private String msg;

    private AtomicInteger updateTotal;

    private AtomicInteger insertTotal;

    private AtomicInteger updateGroupTotal;

    private AtomicInteger insertGroupTotal;

}
