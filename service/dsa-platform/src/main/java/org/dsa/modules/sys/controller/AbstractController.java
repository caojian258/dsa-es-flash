package org.dsa.modules.sys.controller;

import org.apache.http.HttpStatus;
import org.apache.shiro.SecurityUtils;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.sys.entity.DisplayUserEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller公共组件
 *
 */
public abstract class AbstractController {
	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Resource
	private RedisUtils redisUtils;

	protected DisplayUserEntity getUser() {
		if(SecurityUtils.getSubject().getPrincipal()==null) {
			new RRException("invalid token",HttpStatus.SC_UNAUTHORIZED);
		}
		return (DisplayUserEntity) SecurityUtils.getSubject().getPrincipal();
	}

	protected String getToken() {
		return getUser().getToken();
	}

	protected Long getUserId() {
		return Long.valueOf(getUser().getUserInfo().getUserId());
	}

	protected String getUserName() {
		return getUser().getUserInfo().getUsername();
	}

	//查询自己所拥有的角色列表
	protected List<String> getRoleList(){
		DisplayUserEntity displayUserEntity =(DisplayUserEntity) SecurityUtils.getSubject().getPrincipal();
		return displayUserEntity.getRoles().stream().map(DisplayUserEntity.Role::getRoleCode).collect(Collectors.toList());
	}

	/*protected SysUserEntity getUser() {
		return (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
	}
	//查询自己所拥有的角色列表
	protected List<String> getRoleList(){
		List<String> roleList=sysRoleService.queryRoleNameList(getUserId());
		return roleList;
	}

	//查询自己所拥有的角色列表
	protected List<Long> getWorkSheetList(){
		List<Long> workSheetList=sysWorkshopService.getSubWorkshopIdList(getUser().getWorkshopId());
		return workSheetList;
	}*/

}
