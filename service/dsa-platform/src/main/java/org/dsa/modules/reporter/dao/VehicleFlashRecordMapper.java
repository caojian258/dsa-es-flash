package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.dto.FlashVehicleStatsDto;
import org.dsa.modules.reporter.entity.VehicleFlashRecord;

import java.util.List;

@Mapper
public interface VehicleFlashRecordMapper extends BaseMapper<VehicleFlashRecord> {

    IPage<FlashVehicleStatsDto> selectStatsPage(IPage<FlashVehicleStatsDto> page,
                                                  @Param("startDate") String startDate,
                                                  @Param("endDate") String endDate,
                                                  @Param("vin") String vinCode,
                                                  @Param("vehType") List<Long> vehType);
}
