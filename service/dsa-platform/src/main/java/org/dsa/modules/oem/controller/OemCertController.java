package org.dsa.modules.oem.controller;


import lombok.extern.slf4j.Slf4j;
import org.dsa.common.annotation.SysLog;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.R;
import org.dsa.common.validator.ValidatorUtils;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.modules.oem.config.OemCertComponent;
import org.dsa.modules.oem.service.OemCertService;
import org.dsa.modules.oem.vo.SWTSGetCertReqVo;
import org.dsa.modules.oem.vo.WhiteSyncReqVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/oem/cert")
@RestController
@Slf4j
public class OemCertController {

    @Autowired
    private OemCertService oemCertService;
    @Autowired
    private OemCertComponent oemForwardComponent;

    /**
     * 客户端请求OEM证书分为两种情况，一种是已注册下载证书，一种是未注册申请证书
     * 下载证书文件
     *
     * @return R
     */
    @PostMapping("/getSWTSCert")
    @SysLog("下载SWTS证书")
    public Map<String, Object> getCert(@RequestBody SWTSGetCertReqVo vo) {

        if (StringUtils.isEmpty(vo.getPcid())) {
            HashMap<String, Object> result = new HashMap<>();
            result.put("key", "");
            result.put("ca", "");
            result.put("cert", "");
            return R.error(Constant.Msg.CHECK_INPUT).put("data", result);
        }

        return oemCertService.getCert(vo);
    }

    /**
     * CSR文件证书申请
     *
     * @return R
     */
    @PostMapping("/CSRCertification")
    @SysLog("OEM-OBD认证")
    public Map<String, Object> CSRCertification(@RequestBody SWTSGetCertReqVo vo) {
        if (StringUtils.isEmpty(vo.getCsr()) && StringUtils.isEmpty(vo.getPcid())) {
            HashMap<String, Object> result = new HashMap<>();
            result.put("key", "");
            result.put("ca", "");
            result.put("cert", "");
            return R.error(Constant.Msg.CHECK_INPUT).put("data", result);
        }

        if (!StringUtils.isEmpty(vo.getCsr())) {
            return oemCertService.CSRCertification(vo);
        } else {
            return oemCertService.getCert(vo);
        }
    }

    @PostMapping("/whiteListSync")
    @SysLog("OEM-白名单认证")
    public Map<String, Object> whiteListSync(@RequestBody WhiteSyncReqVo vo) {

        ValidatorUtils.validateEntity(513, vo, AddGroup.class);

        return oemCertService.whiteListSync(vo);
    }

    @RequestMapping("/test")
    public R test() {
//        System.out.println(oemForwardComponent.getForward());
//        System.out.println(oemForwardComponent.getCert());
        System.out.println("接收请求------test");
        return R.ok();
    }


}
