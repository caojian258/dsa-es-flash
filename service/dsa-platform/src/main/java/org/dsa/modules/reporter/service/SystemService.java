package org.dsa.modules.reporter.service;

import org.dsa.modules.reporter.dto.AppsInfoDto;
import org.dsa.modules.reporter.entity.*;
import org.dsa.modules.reporter.vo.api.ClientInfoReVo;
import org.dsa.modules.reporter.vo.api.UpgradeLogReVo;
import org.dsa.modules.reporter.vo.api.UpgradeResultReVo;

import java.util.List;


public interface SystemService {

    /***
     * 解析smartstart_status.json文件
     * @param filePath
     * @param tag
     */
    public void addHistory(String filePath, String tag);

    public void insertMetadata(SystemMetadata sm);

    public void delMetadataById(Long id);

    public void delMetadataByItem(String item);

    public void updateMetadata(SystemMetadata sm);

    /**
     * 获取已安装应用列表
     * */
    public AppsInfoDto getApplicationsInfo(String tag);

    public SystemHistory getOneHistory(String tag);

    public void saveOverview(SystemHistory item);


    public void saveProtocolItem(SystemHistory item);

    public void addUpgradeLog(UpgradeLogReVo vo, String tag);

    public void addProtocolItem(UpgradeResultReVo vo);
    public AppsInfoDto getApplicationsInfoByUpgradeLog(String tag);

    public ClientUpgrade getOneRecord(String tag);

    public List<ClientUpgrade> queryDataByTag(String tag);

    public void addClientInfo(ClientInfoReVo clientInfo, String tag);
}
