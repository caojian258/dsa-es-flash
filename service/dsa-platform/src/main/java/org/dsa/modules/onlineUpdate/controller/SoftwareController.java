package org.dsa.modules.onlineUpdate.controller;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.annotation.SysLog;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.PageUtils;
import org.dsa.common.utils.R;
import org.dsa.modules.onlineUpdate.entity.SoftwareEntity;
import org.dsa.modules.onlineUpdate.service.SoftwareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 软件信息
 */
@Slf4j
@RestController
@RequestMapping("onlineUpdate/software")
public class SoftwareController {
    @Autowired
    private SoftwareService softwareService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestBody Map<String, Object> params) {
        try {
            PageUtils page = softwareService.queryPage(params);
            return R.ok().put("page", page);
        } catch (Exception e) {
            log.error("software list error:"+ e.getMessage());
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Integer id) {
        try {
            SoftwareEntity software = softwareService.getById(id);
            if (ObjectUtil.isNull(software)){
                throw new RRException(Constant.Msg.DATA_INFO_NULL_ERROR);
            }
            return R.ok().put("software", software);
        } catch (RRException rre) {
            log.error(String.format("software info error: id: %s msg: %s",id,rre.getMessage()));
            return R.error(rre.getCode(), "查询详情失败： " + rre.getMsg());
        } catch (Exception e) {
            log.error("software info error:"+ e.getMessage());
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    /**
     * 保存
     */
    @SysLog("保存软件")
    @RequestMapping("/save")
    public R save(@RequestBody SoftwareEntity software) {
        try {
            log.info(String.format("save software param {} %s", JSON.toJSONString(software)));
            softwareService.saveOrUpdate2(software);
            return R.ok();
        } catch (RRException rre) {
            log.error("software save error:"+ rre.getMessage());
            return R.error(rre.getCode(), "软件保存失败： " + rre.getMsg());
        } catch (Exception e) {
            log.error("software save error:"+ e.getMessage());
            if (e.getMessage().contains("ERROR: duplicate key value violates unique")) {
                return R.error(Constant.Msg.NAME_DUPLICATION_ERROR);
            }
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }


    /**
     * 修改
     */
    @SysLog("修改软件")
    @RequestMapping("/update")
    public R update(@RequestBody SoftwareEntity software) {
        try {
            log.info(String.format("update software {} %s", JSON.toJSONString(software)));
            if (ObjectUtil.isNull(software.getId())) {
                throw new RRException(Constant.Msg.CHECK_ID_ERROR);
            }
            softwareService.saveOrUpdate2(software);
            return R.ok();
        } catch (RRException rre) {
            log.error(rre.getMessage());
            return R.error(rre.getCode(), "操作失败： " + rre.getMsg());
        } catch (Exception e) {
            log.error("software update error:"+ e.getMessage());
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    /**
     * 禁用
     */
    @SysLog("禁用软件")
    @RequestMapping("/disabled")
    public R delete(@RequestParam Integer id) {
        try {
            log.info(String.format("disabled software param {} %d", id));
            softwareService.disabledById(id);
            return R.ok();
        } catch (RRException rre) {
            log.error("disabled software param {} %s" + rre.getMessage());
            return R.error(rre.getCode(), "禁用软件失败： " + rre.getMsg());
        } catch (Exception e) {
            log.error("software disabled error:"+ e.getMessage());
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    @RequestMapping("/select")
    public R selectAll(){
        try {
            return R.ok().put("options",softwareService.queryAll());
        }catch (Exception e) {
            log.error("software error:"+ e.getMessage());
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

}
