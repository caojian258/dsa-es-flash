package org.dsa.modules.oem.util;

import java.util.HashMap;
import java.util.Map;

public class ResUtils {

    public static Map<Object, Object> getCertResult() {
        HashMap<Object, Object> map = new HashMap<>();
        map.put("ca", "");
        map.put("cert", "");
        map.put("key", "");
        return map;
    }

    public static Map<Object, Object> getCsrResult() {
        HashMap<Object, Object> map = new HashMap<>();
        map.put("crt", "");

        return map;
    }
}
