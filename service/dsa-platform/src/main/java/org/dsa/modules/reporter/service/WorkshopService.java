package org.dsa.modules.reporter.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.reporter.entity.Workshop;
import org.dsa.modules.reporter.entity.WorkshopGroup;
import org.dsa.modules.reporter.vo.workshop.WorkshopPageReVo;

import java.util.Date;
import java.util.List;

public interface WorkshopService {

    public Page<Workshop> selectPage(WorkshopPageReVo vo);

    public Workshop getOne(String id);

    public List<WorkshopGroup> getGroup();

    public List<String> selectWorkshopIdByGroup(String name);

    public void updateLoginTime(String id, Date loginTime);

    public List<String> countryList();

    public List<String> tagList();
}
