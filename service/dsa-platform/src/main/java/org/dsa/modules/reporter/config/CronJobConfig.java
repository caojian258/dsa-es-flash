package org.dsa.modules.reporter.config;

import lombok.Data;

@Data
public class CronJobConfig {

    private String name;

    private String className;

    private Boolean hasListener;

    private String listenerClass;
}
