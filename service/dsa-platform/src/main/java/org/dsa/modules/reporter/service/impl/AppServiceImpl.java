package org.dsa.modules.reporter.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.dsa.modules.reporter.dao.AppInstallMapper;
import org.dsa.modules.reporter.dao.AppOverviewMapper;
import org.dsa.modules.reporter.dao.AppUnInstallMapper;
import org.dsa.modules.reporter.dao.AppVersionMapper;
import org.dsa.modules.reporter.dto.AppInstallDto;
import org.dsa.modules.reporter.dto.AppUnInstallDto;
import org.dsa.modules.reporter.dto.AppVersionDto;
import org.dsa.modules.reporter.entity.AppOverview;
import org.dsa.modules.reporter.entity.AppInstall;
import org.dsa.modules.reporter.entity.AppUnInstall;
import org.dsa.modules.reporter.entity.AppVersion;
import org.dsa.modules.reporter.service.AppInstallIService;
import org.dsa.modules.reporter.service.AppService;
import org.dsa.modules.reporter.service.AppVersionIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Service
public class AppServiceImpl extends AppBaseService implements AppService {

    @Autowired
    AppOverviewMapper appOverviewMapper;

    @Autowired
    AppInstallMapper appInstallMapper;

    @Autowired
    AppInstallIService installIService;

    @Autowired
    AppVersionIService versionIService;

    @Autowired
    AppUnInstallMapper unInstallMapper;

    @Autowired
    AppVersionMapper appVersionMapper;


    @Override
    public void addInstallation(AppInstall installation) {
        appInstallMapper.insert(installation);
    }

    @Override
    public void saveOverview(AppOverview overview) {
        if(null == overview.getId()){
            appOverviewMapper.insert(overview);
        }else{
            overview.setUpdatedAt(new Date());
            appOverviewMapper.updateById(overview);
        }
    }

    @Override
    public AppOverview getAppOverview(String workshop, String pcid, String name) {
        return appOverviewMapper.findByWorkshopPcidName(workshop, pcid, name);
    }

    @Override
    public AppOverview getAppOverview(String pcid, Integer appId) {
        return appOverviewMapper.findByPcidAppId(pcid, appId);
    }

    @Override
    public boolean batchAddInstall(List<AppInstallDto> dtos, String workshop, String pcid, Date timestamp, String tag) {
        Date collectedTime = timestamp;

        List<AppInstall> installs = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        for (AppInstallDto installDto: dtos){
            AppInstall install = new AppInstall();
            install.setInstallMsg(installDto.getInstallationMessage());
            install.setInstallResult(installDto.getInstallationResult());
            install.setInstallTime(installDto.getInstallationTime());
            install.setName(installDto.getApplicationName());
            install.setUpdateOid(installDto.getUpdateOid());
            install.setVersion(installDto.getUpdateVersion());
            install.setPcid(pcid);
            install.setWorkshop(workshop);
            install.setCollectTime(collectedTime);
            install.setCollectDate(sdf.format(collectedTime));

            if(null != installDto.getType()){
                install.setType(installDto.getType());
            }
            if(null != installDto.getAppId()){
                install.setAppId(installDto.getAppId());
            }

            install.setTag(tag);
            installs.add(install);
        }

          return installIService.saveBatch(installs);
    }

    @Override
    public boolean batchAddInstall(Set<AppInstallDto> dtos, String workshop, String pcid, Date timestamp, String tag) {
        Date collectedTime = timestamp;

        List<AppInstall> installs = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        for (AppInstallDto installDto: dtos){
            AppInstall install = new AppInstall();
            install.setInstallMsg(installDto.getInstallationMessage());
            install.setInstallResult(installDto.getInstallationResult());
            install.setInstallTime(installDto.getInstallationTime());
            install.setName(installDto.getApplicationName());
            install.setUpdateOid(installDto.getUpdateOid());
            install.setVersion(installDto.getUpdateVersion());
            install.setPcid(pcid);
            install.setWorkshop(workshop);
            install.setCollectTime(collectedTime);
            install.setCollectDate(sdf.format(collectedTime));

            if(null != installDto.getType()){
                install.setType(installDto.getType());
            }
            if(null != installDto.getAppId()){
                install.setAppId(installDto.getAppId());
            }

            install.setTag(tag);
            installs.add(install);
        }

        return installIService.saveBatch(installs);
    }

    @Override
    public void batchSaveOverview(List<AppVersionDto> dtos, String workshop, String pcid, Date timestamp, String tag) {
        Date collectedTime = timestamp;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        for (AppVersionDto versionDto:  dtos){

            AppOverview overview = getAppOverview(pcid, versionDto.getAppId());

            if(null == overview){
                overview = new AppOverview();
                if(null != versionDto.getPrevVersion()){
                    overview.setPrevVersion(versionDto.getPrevVersion());
                }
            }else{
                if(null != versionDto.getVersion() &&
                        !versionDto.getVersion().equals(overview.getVersion())){
                    overview.setPrevVersion(overview.getVersion());
                }
            }
            overview.setName(versionDto.getName());
            overview.setVersion(versionDto.getVersion());
            overview.setPcid(pcid);
            overview.setWorkshop(workshop);
            overview.setCollectTime(collectedTime);
            overview.setCollectDate(sdf.format(collectedTime));
            overview.setTag(tag);
            if(null != versionDto.getTimestamp()){
                overview.setUpgradeTime(versionDto.getTimestamp());
            }
            if(null != versionDto.getAppId()){
                overview.setAppId(versionDto.getAppId());
            }
            if(null != versionDto.getType()){
                overview.setType(versionDto.getType());
            }

            saveOverview(overview);
        }
    }

    @Override
    public void batchSaveOverview(Set<AppVersionDto> dtos, String workshop, String pcid, Date timestamp, String tag) {
        Date collectedTime = timestamp;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        for (AppVersionDto versionDto:  dtos){

            AppOverview overview = getAppOverview(pcid, versionDto.getAppId());

            if(null == overview){
                overview = new AppOverview();
                if(null != versionDto.getPrevVersion()){
                    overview.setPrevVersion(versionDto.getPrevVersion());
                }
            }else{
                if(null != versionDto.getVersion() &&
                        !versionDto.getVersion().equals(overview.getVersion())){
                    overview.setPrevVersion(overview.getVersion());
                }
            }
            overview.setName(versionDto.getName());
            overview.setVersion(versionDto.getVersion());
            overview.setPcid(pcid);
            overview.setWorkshop(workshop);
            overview.setCollectTime(collectedTime);
            overview.setCollectDate(sdf.format(collectedTime));
            overview.setTag(tag);
            if(null != versionDto.getTimestamp()){
                overview.setUpgradeTime(versionDto.getTimestamp());
            }
            if(null != versionDto.getAppId()){
                overview.setAppId(versionDto.getAppId());
            }
            if(null != versionDto.getType()){
                overview.setType(versionDto.getType());
            }

            saveOverview(overview);
        }
    }

    @Override
    public void batchAddVersion(List<AppVersionDto> dtos, String workshop, String pcid, Date timestamp, String tag) {
        List<AppVersion> avs = new ArrayList<>();
        Date collectedTime =  timestamp;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        for (AppVersionDto versionDto:  dtos){
            AppVersion av = new AppVersion();

            av.setName(versionDto.getName());
            av.setVersion(versionDto.getVersion());
            av.setPcid(pcid);
            av.setWorkshop(workshop);
            av.setCollectTime(collectedTime);
            av.setCollectDate(sdf.format(collectedTime));
            av.setTag(tag);

            if(null != versionDto.getAppId()){
                av.setAppId(versionDto.getAppId());
            }
            if(null != versionDto.getType()){
                av.setType(versionDto.getType());
            }

            avs.add(av);
        }

        if(!CollectionUtil.isEmpty(avs)){
            versionIService.saveBatch(avs);
        }

    }

    @Override
    public void batchAddVersion(Set<AppVersionDto> dtos, String workshop, String pcid, Date timestamp, String tag) {
        List<AppVersion> avs = new ArrayList<>();
        Date collectedTime =  timestamp;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        //同批次的要去重
        for (AppVersionDto versionDto:  dtos){

            AppVersion version =  queryDeviceAppVersionByTag(pcid, versionDto.getAppId(),
                    versionDto.getVersion(), tag);
            if(null != version){
                continue;
            }
            AppVersion av = new AppVersion();
            av.setName(versionDto.getName());
            av.setVersion(versionDto.getVersion());
            av.setPcid(pcid);
            av.setWorkshop(workshop);
            av.setCollectTime(collectedTime);
            av.setCollectDate(sdf.format(collectedTime));
            av.setTag(tag);

            if(null != versionDto.getAppId()){
                av.setAppId(versionDto.getAppId());
            }
            if(null != versionDto.getType()){
                av.setType(versionDto.getType());
            }

            avs.add(av);
        }

        if(!CollectionUtil.isEmpty(avs)){
            versionIService.saveBatch(avs);
        }
    }

    @Override
    public AppOverview getAppOverviewByName(String workshop, String pcid, String name) {
        return appOverviewMapper.findByName(workshop, pcid, name);
    }

    @Override
    public void addUnInstall(List<AppUnInstallDto> dtos, String workshop, String pcid, Date timestamp, String tag) {
        Date collectedTime =  timestamp;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        for (AppUnInstallDto dto: dtos){
            AppUnInstall entity = new AppUnInstall();
            entity.setAppId(dto.getAppId());
            entity.setPcid(pcid);
            entity.setWorkshop(workshop);
            entity.setVersion(dto.getVersion());
            entity.setCollectDate(sdf.format(collectedTime));
            entity.setCollectTime(collectedTime);
            entity.setType(dto.getType());
            entity.setName(dto.getName());
            entity.setUpdateOid(dto.getUpdateOid());
            entity.setTag(tag);
            entity.setResult(dto.getResult());
            unInstallMapper.insert(entity);

            if(dto.getResult().equals("0")){
                QueryWrapper<AppOverview> qw = new QueryWrapper<>();
                qw.eq("pcid", pcid);
                qw.eq("app_id", dto.getAppId());
                appOverviewMapper.delete(qw);
            }
        }
    }

    @Override
    public void addUnInstall(Set<AppUnInstallDto> dtos, String workshop, String pcid, Date timestamp, String tag) {
        Date collectedTime =  timestamp;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        for (AppUnInstallDto dto: dtos){
            AppUnInstall entity = new AppUnInstall();
            entity.setAppId(dto.getAppId());
            entity.setPcid(pcid);
            entity.setWorkshop(workshop);
            entity.setVersion(dto.getVersion());
            entity.setCollectDate(sdf.format(collectedTime));
            entity.setCollectTime(collectedTime);
            entity.setType(dto.getType());
            entity.setName(dto.getName());
            entity.setUpdateOid(dto.getUpdateOid());
            entity.setTag(tag);
            entity.setResult(dto.getResult());
            unInstallMapper.insert(entity);

            if(dto.getResult().equals("0")){
                QueryWrapper<AppOverview> qw = new QueryWrapper<>();
                qw.eq("pcid", pcid);
                qw.eq("app_id", dto.getAppId());
                appOverviewMapper.delete(qw);
            }
        }
    }

    @Override
    public AppVersion queryDeviceAppVersionByTag(String pcid, Integer appId, String version, String tag) {
        return appVersionMapper.queryDeviceAppVersion(pcid, appId, version, tag);
    }
}
