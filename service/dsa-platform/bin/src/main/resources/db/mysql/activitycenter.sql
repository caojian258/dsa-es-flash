-- 变更申请表
CREATE TABLE `t_change` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `change_number` varchar(20) COMMENT '变更号',
  `plan_start_time` datetime  COMMENT '计划开始时间',
  `plan_end_time` datetime COMMENT '计划结束时间',
  `status` varchar(50) COMMENT '变更状态',
  `content` text COMMENT '变更内容描述',
  `scene_id` varchar(20) COMMENT '场景的Id',
  `system_id` varchar(20) COMMENT '系统的Id',
  `applicant` varchar(50) COMMENT '申请人',
  `created_time` datetime COMMENT '创建时间',
  `created_by` varchar(50) COMMENT '创建人',
  `modify_by` varchar(50) COMMENT '修改人',
  `modify_time` datetime COMMENT '修改时间',
  `is_delete`  varchar(6) DEFAULT 0 COMMENT '是否删除(0:正常,1：删除)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='变更申请表';

-- 任务表
CREATE TABLE `t_task` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `change_number` varchar(20) COMMENT '变更号',
  `task_owner` varchar(50)  COMMENT '任务负责人',
  `content` text COMMENT '变更内容描述',
  `attached` text COMMENT '附件地址',
  `status` varchar(50) COMMENT '状态',
  `task_start_time` datetime COMMENT '任务开始时间',
  `task_end_time` datetime COMMENT '任务创建时间',
  `task_finish_time` datetime COMMENT '任务完成时间',
  `comment` varchar(200) COMMENT '意见',
  `created_time` datetime COMMENT '创建时间',
  `created_by` varchar(50) COMMENT '创建人',
  `modify_by` varchar(50) COMMENT '修改人',
  `modify_time` datetime COMMENT '修改时间',
  `is_delete` varchar(6) DEFAULT 0 COMMENT '是否删除(0:正常,1：删除)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='任务表';

-- 审批表
CREATE TABLE `t_approve` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `change_number` varchar(20) COMMENT '变更号',
  `approve_time` datetime  COMMENT '审批时间',
  `approver` varchar(20) COMMENT '审批人',
  `comment` text COMMENT '审批意见',
  `status` varchar(50) COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='审批表';


INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('场景','scene','GeneralRelease','常规发布',1,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('场景','scene','EmergencyRelease','紧急发布',2,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('场景','scene','StructureAdjustment','架构调整',3,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('场景','scene','ModifyParameters','参数修改',4,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('场景','scene','Maintenance','例行维护',5,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('场景','scene','Equipment','设备上下架',6,'',0);

INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system1','经纪业务集中运营平台（UF2.0）',1,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system2','投资赢家网上交易系统',2,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system3','同花顺网上交易系统',3,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system4','财人汇网开网厅系统',4,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system5','根网QFII系统',5,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system6','新意法人清算系统',6,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system7','投保报送平台',7,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system8','风险管理系统',8,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system9','市场风险系统',9,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system10','全面风险系统',10,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system11','经纪反洗钱系统',11,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system12','资管反洗钱系统',12,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system13','邮件系统',13,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system14','邮件归档系统',14,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system15','邮件防泄密系统',15,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system16','资管投资系统（恒生O32）',16,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system17','自营投资系统（恒生O32）',17,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system18','银行间交易系统',18,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system19','银行间结算系统（中债）',19,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system20','银行间结算系统（上清）',20,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system21','恒泰固定收益平台',21,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system22','深交所交易终端平台',22,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system23','直销系统（DS）',23,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system24','估值核算系统（FA）',24,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system25','登记过户系统（TA）',25,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system26','资金清算系统（CC',26,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system27','短信主动服务平台',27,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system28','信息披露与监管报送平台',28,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system29','投资研究管理系统',29,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system30','用友财务系统',30,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system31','HR系统',31,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system32','OA系统',32,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system33','公司网站系统',33,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system34','Veritas备份系统',34,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system35','服务器虚拟化',35,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system36','堡垒机',36,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system37','英方统一数据管理平台',37,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system38','防病毒',38,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system39','录音系统',39,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system40','门禁系统',40,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system41','视频监控系统',41,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system42','域控系统',42,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system43','终端管理系统',43,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system44','上网行为管理系统',44,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system45','基础监控系统',45,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('系统','system','system46','企业QQ系统',46,'',0);




INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('changestatus','changestatus','DRAFT','草稿',1,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('changestatus','changestatus','LEADER_APPROVE','待组长审批',2,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('changestatus','changestatus','MANAGER_APPROVE','待经理审批',3,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('changestatus','changestatus','DONE','完成',4,'',0);
INSERT INTO `sys_dict` (`name`, `type`, `code`, `value`, `order_num`, `remark`, `del_flag`) VALUES('changestatus','changestatus','REJECT','已拒绝',5,'',0);