package org.dsa.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("sys_locales")
public class SysLocales implements Serializable {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String locale;

    private String name;

    private String units;

    private Date createTime;
}
