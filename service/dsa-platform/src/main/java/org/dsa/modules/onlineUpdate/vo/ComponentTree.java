package org.dsa.modules.onlineUpdate.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 组件树
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ComponentTree {

    /**
     * 组件名称
     */
    private String name;

    private List<ComponentVo> componentVos;

}
