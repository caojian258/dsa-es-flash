package org.dsa.modules.remoteDiagnostic.dto.api;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class DiagnosticFileDto implements Serializable {

    private String fileName;

    private String fileMd5;

    private Long fileLength;

}
