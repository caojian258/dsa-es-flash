package org.dsa.modules.reporter.vo;

import lombok.Data;
import org.dsa.modules.reporter.dto.ChartItemDto;

import java.util.List;
import java.util.Set;

@Data
public class ChartResVo {

    /***
     * 数据类型
     */
    private String category;

    /***
     * 图表类型
     */
    private String chart;

    /***
     * X轴数据列表
     */
    private List<String> xAxisData;

    /***
     * 分类数据列表
     */
    private Set<String> legendData;

    /***
     * 实际的数据列表
     */
    private List<ChartItemDto> seriesData;
}
