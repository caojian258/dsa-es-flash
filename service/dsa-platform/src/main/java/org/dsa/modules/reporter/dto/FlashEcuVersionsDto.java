package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.util.Map;

@Data
public class FlashEcuVersionsDto {
    //ecu名称
    private String name;

    //刷写前的版本信息
    private Map<String, String> expectedIdents;

    //刷写后的目标版本信息
    private Map<String, String> targetIdents;

    //刷写结果 1 成功 2失败 3未刷写
    private Integer result;

    //原因
    private String reason;

}
