package org.dsa.modules.remoteDiagnostic.service.impl;

import clojure.lang.IFn;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.gson.Gson;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.utils.*;
import org.dsa.modules.remoteDiagnostic.config.RemoteDiagnosticConfig;
import org.dsa.modules.remoteDiagnostic.constant.RemoteDiagnosticConstant;
import org.dsa.modules.remoteDiagnostic.dao.*;
import org.dsa.modules.remoteDiagnostic.dto.EcuDto;
import org.dsa.modules.remoteDiagnostic.entity.*;
import org.dsa.modules.remoteDiagnostic.service.RemoteEcuVersionService;
import org.dsa.modules.remoteDiagnostic.service.RemoteVehicleEcuGroupService;
import org.dsa.modules.remoteDiagnostic.service.RemoteVehicleEcuService;
import org.dsa.modules.remoteDiagnostic.vo.CheckNameReqVo;
import org.dsa.modules.remoteDiagnostic.vo.ECUFillingCertificateReqVo;
import org.dsa.modules.remoteDiagnostic.vo.EcuVersion.EcuVersionEcuReqVo;
import org.dsa.modules.remoteDiagnostic.vo.PublicPageReqVo;
import org.dsa.modules.remoteDiagnostic.vo.VehicleEcuPageReVo;
import org.dsa.modules.reporter.util.LocalUtil;
import org.dsa.modules.sys.dao.SysCacheDao;
import jakarta.validation.constraints.NotNull;
import org.dsa.modules.sys.entity.SysLogEntity;
import org.dsa.modules.sys.service.SysLogService;
import org.dsa.modules.sys.utils.TokenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.io.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class RemoteVehicleEcuServiceImpl implements RemoteVehicleEcuService {

    @Autowired
    RemoteVehicleEcuDao ecuDao;
    @Autowired
    RemoteEcuIdentificationInfoDao identificationInfoDao;
    @Autowired
    RemoteEcuGroupDao ecuGroupDao;

    @Autowired
    RemoteVehicleEcuGroupDao vehicleEcuGroupDao;
    @Autowired
    RemoteVehicleEcuGroupService vehicleEcuGroupService;
    @Autowired
    RemoteEcuVersionService versionService;
    @Autowired
    RemoteVehicleEcuImageDao ecuImageDao;
    @Autowired
    RemoteVehicleTypeEcuGroupDao vehicleTypeEcuGroupDao;
    @Autowired
    RemoteDiagnosticConfig remoteDiagnosticConfig;
    @Autowired
    RestTemplate restTemplate;
    @Autowired
    RedisUtils redisUtils;
    @Autowired
    SysCacheDao sysCacheDao;
    @Autowired
    private SysLogService sysLogService;

    @Override
    public Page<RemoteVehicleEcuEntity> selectPage(VehicleEcuPageReVo vo) {
        Page<RemoteVehicleEcuEntity> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);
        LambdaQueryWrapper<RemoteVehicleEcuEntity> qw = Wrappers.<RemoteVehicleEcuEntity>lambdaQuery();

        if (!StringUtils.isEmpty(vo.getName())) {
//            qw.like(VehicleEcuEntity::getEcuName, vo.getName().trim());
            qw.apply("ecu_name" + " ilike {0}", "%" + vo.getName().trim() + "%");
        }

        if (!StringUtils.isEmpty(vo.getStatus())) {
            qw.eq(RemoteVehicleEcuEntity::getStatus, vo.getStatus());
        }
        Page<RemoteVehicleEcuEntity> resultPage = ecuDao.selectPage(page, qw);

        return resultPage;
    }

    @Override
    public RemoteVehicleEcuEntity getInfo(Long id) {
        RemoteVehicleEcuEntity vehicleEcuEntity = ecuDao.getInfo(id);

        if (vehicleEcuEntity != null) {
            vehicleEcuEntity.setIdentifications(identificationInfoDao.selectList(Wrappers.<RemoteEcuIdentificationInfoEntity>lambdaQuery().eq(RemoteEcuIdentificationInfoEntity::getEcuId, id)));
            vehicleEcuEntity.setImage(ecuImageDao.selectOne(Wrappers.<RemoteVehicleEcuImageEntity>lambdaQuery().eq(RemoteVehicleEcuImageEntity::getEcuId, id)));
        }

        return vehicleEcuEntity;
    }

    @Override
    @Transactional
    public void saveEcu(RemoteVehicleEcuEntity ecu) throws IOException {
        ecuDao.insert(ecu);
        // 分组关联

        ecuGroupDao.delete(Wrappers.<RemoteEcuGroupEntity>lambdaQuery().eq(RemoteEcuGroupEntity::getEcuId, ecu.getId()).eq(RemoteEcuGroupEntity::getGroupId, ecu.getGroupId()));
        if (ecu.getGroupId() != null) {
            RemoteEcuGroupEntity ecuGroupEntity = new RemoteEcuGroupEntity();
            ecuGroupEntity.setEcuId(ecu.getId());
            ecuGroupEntity.setGroupId(ecu.getGroupId());
            ecuGroupDao.insert(ecuGroupEntity);
        } else {
            // 保存ECU分组
            RemoteVehicleEcuGroupEntity group = new RemoteVehicleEcuGroupEntity();
            group.setImage(ecu.getImage());
            group.setGroupName(ecu.getEcuName());
            group.setIdentifications(ecu.getIdentifications());
            group.setStatus(ecu.getStatus());
            vehicleEcuGroupService.saveGroup(group);

            // 保存ECU分组与ECU的关系
            RemoteEcuGroupEntity ecuGroupEntity = new RemoteEcuGroupEntity();
            ecuGroupEntity.setEcuId(ecu.getId());
            ecuGroupEntity.setGroupId(group.getId());
            ecuGroupDao.insert(ecuGroupEntity);
        }
        if (ecu.getIdentifications() != null && !ecu.getIdentifications().isEmpty()) {
//            LambdaQueryWrapper<VehiclePlatformEcuEntity> qw = Wrappers.<VehiclePlatformEcuEntity>lambdaQuery();
//            qw.eq(VehiclePlatformEcuEntity::getPlatformId, platform.getId());
            //批量插入
            identificationInfoDao.inserts(null, ecu.getId(), null, ecu.getIdentifications());
        }
        if (ecu.getImage() != null) {
            RemoteVehicleEcuImageEntity image = ecu.getImage();
            image.setId(null);
            image.setEcuGroupId(null);
            image.setEcuId(ecu.getId());
            if (!StringUtils.isEmpty(image.getFilePath()))
                image.setFileMd5(DigestUtils.md5DigestAsHex(new FileInputStream(new File(image.getFilePath()))));
            ecuImageDao.insert(image);
        }
    }

    @Override
    @Transactional
    public void updateEcu(RemoteVehicleEcuEntity ecu) throws IOException {
        ecu.setUpdatedAt(new Date());

        ecuDao.updateById(ecu);

        if (!ecu.getGroupId().equals(ecu.getYgroupId())) {
//            qwd.eq(RemoteEcuGroupEntity::getGroupId, ecu.getYGroupId());
            ecuGroupDao.delete(Wrappers.<RemoteEcuGroupEntity>lambdaQuery().eq(RemoteEcuGroupEntity::getEcuId, ecu.getId()));
            if (ecu.getGroupId() != null) {
                RemoteEcuGroupEntity ecuGroupEntity = new RemoteEcuGroupEntity();
                ecuGroupEntity.setEcuId(ecu.getId());
                ecuGroupEntity.setGroupId(ecu.getGroupId());
                ecuGroupDao.insert(ecuGroupEntity);
            }
        }

        // 先删除
        identificationInfoDao.delete(Wrappers.<RemoteEcuIdentificationInfoEntity>lambdaQuery().eq(RemoteEcuIdentificationInfoEntity::getEcuId, ecu.getId()));
        if (ecu.getIdentifications() != null && !ecu.getIdentifications().isEmpty()) {
            // 后插入
            identificationInfoDao.inserts(null, ecu.getId(), null, ecu.getIdentifications());
        }

        if (ecu.getImage() != null) {
            RemoteVehicleEcuImageEntity image = ecuImageDao.selectOne(Wrappers.<RemoteVehicleEcuImageEntity>lambdaQuery().eq(RemoteVehicleEcuImageEntity::getEcuId, ecu.getId()));
            if (image == null) image = new RemoteVehicleEcuImageEntity();
            image.setFileName(ecu.getImage().getFileName());
            image.setFilePath(ecu.getImage().getFilePath());
            image.setEcuId(ecu.getId());

            if (!StringUtils.isEmpty(image.getFilePath()))
                image.setFileMd5(DigestUtils.md5DigestAsHex(new FileInputStream(new File(image.getFilePath()))));
            else image.setFileMd5(null);

            if (image.getId() == null) {
                ecuImageDao.insert(image);
            } else {
                image.setUpdatedAt(new Date());
                ecuImageDao.updateById(image);
            }
        }
    }

    @Override
    public Map<String, Object> getEcus(PublicPageReqVo vo) {
        HashMap<String, Object> map = new HashMap<>();
        if (vo.getId() != null) {
            map.put("yList", ecuDao.getYList(vo.getId(), vo.getName()));
            map.put("nList", ecuDao.getNList(vo.getId(), vo.getName()));
        } else {
            map.put("yList", new ArrayList<>());
            map.put("nList", ecuDao.getNoEcusByGroup(vo.getName()));
        }
        return map;
    }

    @Override
    public List<RemoteVehicleEcuEntity> getEcus(EcuVersionEcuReqVo vo) {
        if (vo.getId() == null) {
            return ecuDao.getList(vo.getIds());
        }
        return ecuDao.getListById(vo.getIds(), vo.getId());

    }

    @Override
    public List<RemoteVehicleEcuEntity> getEcus() {
        return ecuDao.selectList(Wrappers.<RemoteVehicleEcuEntity>lambdaQuery().eq(RemoteVehicleEcuEntity::getStatus, 0));
    }

    @Override
    public List<RemoteVehicleEcuEntity> getNoIdEcus(Long id, Long versionId) {
//        LambdaQueryWrapper<RemoteVehicleEcuEntity> qw = Wrappers.<RemoteVehicleEcuEntity>lambdaQuery();
//        qw.eq(RemoteVehicleEcuEntity::getStatus, 0);
//        qw.ne(RemoteVehicleEcuEntity::getId, id);
//        if (versionId == null) {
        return ecuDao.getNoIdEcus(id);
//        }
//        return ecuDao.getEcusFilterVersion(id, versionId);

    }

    @Override
    public Boolean check(CheckNameReqVo vo) {
        switch (vo.getType()) {
            case "ecu":
                LambdaQueryWrapper<RemoteVehicleEcuEntity> qw = Wrappers.<RemoteVehicleEcuEntity>lambdaQuery();
                qw.eq(RemoteVehicleEcuEntity::getEcuName, vo.getName());
                if (vo.getId() != null) {
                    qw.ne(RemoteVehicleEcuEntity::getId, vo.getId());
                }

                if (vo.getGroupId() == null) {
                    Long aLong = vehicleEcuGroupDao.selectByName(vo.getName());
                    if (aLong != null) {
                        return false;
                    }
                }
                qw.select(RemoteVehicleEcuEntity::getId);
                return ecuDao.selectOne(qw) == null;
            case "group":
                return vehicleEcuGroupService.check(vo);
            case "version":
                return versionService.check(vo);
            default:
                return true;
        }

    }


    @Override
    @Transactional
    public void analyzingEcu(List<EcuDto> dto) {
        if (dto == null || dto.isEmpty()) {
            return;
        }
        RemoteVehicleEcuEntity ecu;
        List<RemoteVehicleTypeEcuGroupEntity> typeEcuGroupList = new ArrayList<>();
        for (EcuDto ecuDto : dto) {

            // 若没有零件号 则跳过
            if (StrUtil.isBlank(ecuDto.getPartNumberName()) || StrUtil.isBlank(ecuDto.getPartNumberVal())) {
                continue;
            }
            if (StrUtil.isBlank(ecuDto.getEcuName()) || StrUtil.isBlank(ecuDto.getEcuName())) {
                continue;
            }
            // 查询是否存在同名ECU
            ecu = ecuDao.selectOne(Wrappers.<RemoteVehicleEcuEntity>lambdaQuery().eq(RemoteVehicleEcuEntity::getEcuName, ecuDto.getEcuName()));
            if (ecu != null) {
                // 判断零件号是否相同
                RemoteEcuIdentificationInfoEntity ecuIdentificationInfoEntity = identificationInfoDao.selectOne(Wrappers.<RemoteEcuIdentificationInfoEntity>lambdaQuery().eq(RemoteEcuIdentificationInfoEntity::getDid, ecuDto.getPartNumberName()).eq(RemoteEcuIdentificationInfoEntity::getEcuId, ecu.getId()));

                //  找到相同零件号  修改ECU
                if (ecuIdentificationInfoEntity != null && ecuDto.getPartNumberVal().equals(ecuIdentificationInfoEntity.getDidVal())) {
                    analyzingUpdateEcu(ecu, typeEcuGroupList, ecuDto);
                } else { // 零件号不同
                    ecu = new RemoteVehicleEcuEntity();

                    // 查询同类型ECU
                    List<RemoteVehicleEcuEntity> ecuEntities = ecuDao.selectList(Wrappers.<RemoteVehicleEcuEntity>lambdaQuery().apply("ecu_name" + " ilike {0}", "%" + ecuDto.getGroupName().trim() + "%").orderByDesc(RemoteVehicleEcuEntity::getId));
                    for (RemoteVehicleEcuEntity ecuEntity : ecuEntities) {
                        RemoteEcuIdentificationInfoEntity ecuIdentification = identificationInfoDao.selectOne(Wrappers.<RemoteEcuIdentificationInfoEntity>lambdaQuery().eq(RemoteEcuIdentificationInfoEntity::getDid, ecuDto.getPartNumberName()).eq(RemoteEcuIdentificationInfoEntity::getEcuId, ecuEntity.getId()));
                        // 找到相同零件号  修改ECU
                        if (ecuIdentification != null && ecuDto.getPartNumberVal().equals(ecuIdentification.getDidVal())) {
                            ecu = ecuEntity;
                            analyzingUpdateEcu(ecu, typeEcuGroupList, ecuDto);
                            break;
                        }
                    }
                    // 未找到相同零件号 新增ECU
                    if (ecu.getId() == null) {
                        analyzingSaveEcu(ecu, typeEcuGroupList, ecuDto, ecuEntities);
                    }
                }
            } else { // 不存在同名ECU
                ecu = new RemoteVehicleEcuEntity();
                // 查询同类型ECU
                List<RemoteVehicleEcuEntity> ecuEntities = ecuDao.selectList(Wrappers.<RemoteVehicleEcuEntity>lambdaQuery().apply("ecu_name" + " ilike {0}", "%" + ecuDto.getGroupName().trim() + "%").orderByDesc(RemoteVehicleEcuEntity::getId));
                for (RemoteVehicleEcuEntity ecuEntity : ecuEntities) {
                    RemoteEcuIdentificationInfoEntity ecuIdentification = identificationInfoDao.selectOne(Wrappers.<RemoteEcuIdentificationInfoEntity>lambdaQuery().eq(RemoteEcuIdentificationInfoEntity::getDid, ecuDto.getPartNumberName()).eq(RemoteEcuIdentificationInfoEntity::getEcuId, ecuEntity.getId()));
                    // 找到相同零件号  修改ECU
                    if (ecuIdentification != null && ecuDto.getPartNumberVal().equals(ecuIdentification.getDidVal())) {
                        ecu = ecuEntity;
                        analyzingUpdateEcu(ecu, typeEcuGroupList, ecuDto);
                        break;
                    }
                }
                // 未找到相同零件号 新增ECU
                if (ecu.getId() == null) {
                    analyzingSaveEcu(ecu, typeEcuGroupList, ecuDto, ecuEntities);
                }
            }
        }

    }

    public void analyzingSaveEcu(RemoteVehicleEcuEntity ecu, List<RemoteVehicleTypeEcuGroupEntity> typeEcuGroupList, EcuDto ecuDto, List<RemoteVehicleEcuEntity> ecuEntities) {
        ecu.setDescriptionTi(ecuDto.getDescription());
        ecu.setShortNameTi(ecuDto.getShortName());

        Long groupId = null;
        char len = 65;
        String suffix = "";
        List<Character> afterName = new ArrayList<>();
        // 获取后缀 和 group
        if (ecuEntities != null && !ecuEntities.isEmpty()) {
            RemoteVehicleEcuEntity last = ecuEntities.get(0);
            char ecuLen = 65;
            for (RemoteVehicleEcuEntity ecuEntity : ecuEntities) {
                String[] names = ecuEntity.getEcuName().split("-");
                if (names.length > 0) {
                    String name = names[names.length - 1];
                    List<Character> collect = name.chars().mapToObj(c -> (char) c).toList();
                    try {
                        ecuLen = collect.get(collect.size() - 1);
                    } catch (Exception ignored) {
                    }

                    if (afterName.size() <= collect.size()) {
                        afterName = new ArrayList<>(collect);
                        if (afterName.size() == collect.size()) {
                            if (ecuLen >= len) {
                                len = (char) (ecuLen + 1);
                            }
                        } else {
                            len = (char) (ecuLen + 1);
                        }
                    }

//                    afterName = collect.stream().map(String::valueOf).collect(Collectors.joining());
                }
            }
            if (afterName.isEmpty()) {
                afterName.add(len);
            } else {
                afterName.set(afterName.size() - 1, len);
            }

            RemoteEcuGroupEntity ecuGroupAssociation = ecuGroupDao.selectOne(Wrappers.<RemoteEcuGroupEntity>lambdaQuery().eq(RemoteEcuGroupEntity::getEcuId, last.getId()));
            if (ecuGroupAssociation != null) {
                groupId = ecuGroupAssociation.getGroupId();
            }
        } else {
            afterName.add(len);
        }
        if (len > 90) {
//            Character last = afterName.get(afterName.size() - 1);
            afterName.set(afterName.size() - 1, 'A');
            afterName.add((char) (65 + (len - 91)));
        }

        suffix = afterName.stream().map(String::valueOf).collect(Collectors.joining());

        ecu.setEcuName(ecuDto.getGroupName() + "-" + suffix);
        ecu.setStatus(0);

        ecuDao.insert(ecu);

        if (groupId != null) {
            RemoteEcuGroupEntity build = RemoteEcuGroupEntity.builder().ecuId(ecu.getId()).groupId(groupId).build();
            ecuGroupDao.insert(build);
        } else {
            // 保存ECU分组
            RemoteVehicleEcuGroupEntity buildGroup = RemoteVehicleEcuGroupEntity.builder().groupName(ecuDto.getGroupName()).descriptionTi(ecu.getDescriptionTi()).shortNameTi(ecu.getShortNameTi()).status(0).build();
            vehicleEcuGroupDao.insert(buildGroup);
            groupId = buildGroup.getId();

            // 保存关联关系
            RemoteEcuGroupEntity buildAssociation = RemoteEcuGroupEntity.builder().ecuId(ecu.getId()).groupId(groupId).build();
            ecuGroupDao.insert(buildAssociation);
        }

        // 保存识别信息
        if (ecuDto.getIdentifications() != null && !ecuDto.getIdentifications().isEmpty()) {
            List<RemoteEcuIdentificationInfoEntity> identificationInfoEntities = ecuDto.getIdentifications().stream().map(e -> {
                return RemoteEcuIdentificationInfoEntity.builder().did(e.getDid()).didVal(e.getDidVal()).descriptionTi(e.getDescription()).didName(e.getDidName()).build();
            }).collect(Collectors.toList());
            identificationInfoDao.insertEcuExists(ecu.getId(), identificationInfoEntities);
        }

        RemoteVehicleTypeEcuGroupEntity typeEcuGroupEntity = vehicleTypeEcuGroupDao.selectOne(Wrappers.<RemoteVehicleTypeEcuGroupEntity>lambdaQuery().eq(RemoteVehicleTypeEcuGroupEntity::getTypeId, ecuDto.getVehicleTypeId()).eq(RemoteVehicleTypeEcuGroupEntity::getEcuGroupId, groupId));
//            RemoteVehicleTypeEcuGroupEntity typeEcuGroupEntity = RemoteVehicleTypeEcuGroupEntity.builder().ecuGroupId(ecuGroupAssociation.getGroupId()).ecuId(ecu.getId()).typeId(ecuDto.getVehicleTypeId()).build();
        if (typeEcuGroupEntity == null) {
            typeEcuGroupEntity = new RemoteVehicleTypeEcuGroupEntity();
            typeEcuGroupEntity.setEcuId(ecu.getId());
            typeEcuGroupEntity.setEcuGroupId(groupId);
            typeEcuGroupEntity.setTypeId(ecuDto.getVehicleTypeId());
            typeEcuGroupList.add(typeEcuGroupEntity);
            vehicleTypeEcuGroupDao.insertsGroup(ecuDto.getVehicleTypeId(), typeEcuGroupList);
        }
    }

    public void analyzingUpdateEcu(RemoteVehicleEcuEntity ecu, List<RemoteVehicleTypeEcuGroupEntity> typeEcuGroupList, EcuDto ecuDto) {
        ecu.setDescriptionTi(ecuDto.getDescription());
        ecu.setShortNameTi(ecuDto.getShortName());

        RemoteEcuGroupEntity ecuGroupAssociation = ecuGroupDao.selectOne(Wrappers.<RemoteEcuGroupEntity>lambdaQuery().eq(RemoteEcuGroupEntity::getEcuId, ecu.getId()));
        // 判断是否存在车型
        if (ecuGroupAssociation != null && ecuDto.getVehicleTypeId() != null) {
            RemoteVehicleTypeEcuGroupEntity typeEcuGroupEntity = vehicleTypeEcuGroupDao.selectOne(Wrappers.<RemoteVehicleTypeEcuGroupEntity>lambdaQuery().eq(RemoteVehicleTypeEcuGroupEntity::getTypeId, ecuDto.getVehicleTypeId()).eq(RemoteVehicleTypeEcuGroupEntity::getEcuGroupId, ecuGroupAssociation.getGroupId()).last(" limit 1"));
//            RemoteVehicleTypeEcuGroupEntity typeEcuGroupEntity = RemoteVehicleTypeEcuGroupEntity.builder().ecuGroupId(ecuGroupAssociation.getGroupId()).ecuId(ecu.getId()).typeId(ecuDto.getVehicleTypeId()).build();
            if (typeEcuGroupEntity == null) {
                typeEcuGroupEntity = new RemoteVehicleTypeEcuGroupEntity();
                typeEcuGroupEntity.setEcuId(ecu.getId());
                typeEcuGroupEntity.setEcuGroupId(ecuGroupAssociation.getGroupId());
                typeEcuGroupEntity.setTypeId(ecuDto.getVehicleTypeId());
                typeEcuGroupList.add(typeEcuGroupEntity);
                vehicleTypeEcuGroupDao.insertsGroup(ecuDto.getVehicleTypeId(), typeEcuGroupList);
            }
//            else {
//                typeEcuGroupEntity.setEcuId(ecu.getId());
//                vehicleTypeEcuGroupDao.updateById(typeEcuGroupEntity);
//            }
        }

        identificationInfoDao.delete(Wrappers.<RemoteEcuIdentificationInfoEntity>lambdaUpdate().eq(RemoteEcuIdentificationInfoEntity::getEcuId, ecu.getId()));
        if (ecuDto.getIdentifications() != null && !ecuDto.getIdentifications().isEmpty()) {
            List<RemoteEcuIdentificationInfoEntity> identificationInfoEntities = ecuDto.getIdentifications().stream().map(e -> {
                return RemoteEcuIdentificationInfoEntity.builder().did(e.getDid()).didVal(e.getDidVal()).descriptionTi(e.getDescription()).didName(e.getDidName()).build();
            }).collect(Collectors.toList());
            identificationInfoDao.insertEcuExists(ecu.getId(), identificationInfoEntities);
        }

        ecu.setUpdatedAt(new Date());
        ecuDao.updateById(ecu);
    }


    @Override
    public Map<String, Object> getECUFillingCertificate(ECUFillingCertificateReqVo vo) throws Exception {
        Gson gson = new Gson();
//        String vehicle_project_id = vo.getProjectId();
//        HashMap<String, String> req = new HashMap<>();
//        req.put("vehicle_project_id", vehicle_project_id); //车系id  @TODO 待确认值
//        req.put("device_id", vo.getPcid());
        String url = remoteDiagnosticConfig.getEcuFillingCertificateUrl() + "?" + "vehicle_project_id" + "=" + vo.getProjectId() +
                "&" + "device_id" + "=" + vo.getDeviceID();
        HashMap<String, Object> result;

        String value = sysCacheDao.getValueByKey(RemoteDiagnosticConstant.ECU_FILLING_CERTIFICATE + ":" + vo.getProjectId() + ":" + vo.getDeviceID());

//        if (vo.getFlag() != 0) {
        if (!StringUtils.isEmpty(value) && checkCertificateExpiry(value)) {
            return getResult(0, "success", value);
        } else {
            result = getOemECUFillingCertificate(vo, gson, url);
        }
//        } else {
//            result = getOemECUFillingCertificate(vo, gson, url);
//        }

        return result;
    }

    private boolean checkCertificateExpiry(String value) throws CertificateException {
        byte[] certificateData = Base64.getDecoder().decode(value);

        // Create a ByteArrayInputStream from the certificate data
        ByteArrayInputStream inputStream = new ByteArrayInputStream(certificateData);

        // Load the X.509 certificate from the input stream
        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
        X509Certificate certificate = (X509Certificate) certificateFactory.generateCertificate(inputStream);

        // Get the expiration date from the certificate
        java.util.Date expiryDate = certificate.getNotAfter();
        log.info("证书过期时间:" + expiryDate);
        // Compare the expiry date with the current date
        java.util.Date currentDate = new java.util.Date();
        return expiryDate.after(currentDate);
    }

    private HashMap<String, Object> getOemECUFillingCertificate(ECUFillingCertificateReqVo vo, Gson gson, String url) {
        HashMap<String, Object> result;
        try {
            HttpHeaders headers = new HttpHeaders();
//        TokenUtils.getHuaWeiAccessToken(new HashMap<>());
            String token = "111";
            headers.add("Authorization", token);

//            headers.add("Authorization", "MIIDbgYJKoZIhvcNAQcCoIIDXzCCA1sCAQExDTALBglghkgBZQMEAgEwgYoGCSqGSIb3DQEHAaB9BHt7ImFwcCI6eyJ0eXBlIjowLCJpZCI6MTAsInBlcm1pc3Npb25zIjpbInBraS5wcm94eS5jZXJ0Lm1nbXQiXSwic3ZjSWQiOjU3MX0sImV4cCI6MTY5MjA3MTQ3NiwibWV0aG9kIjoiY2xpZW50X2NyZWRlbnRpYWxzIn0xggK4MIICtAIBATCBkDB8MQswCQYDVQQGEwJDTjEOMAwGA1UECBMFQW5IdWkxDTALBgNVBAcTBFd1SHUxDjAMBgNVBAoTBUNIRVJZMRswGQYDVQQLExJTbWFydFNlbGVjdGlvbkF1dG8xITAfBgNVBAMTGENIRVJZIFNTQSBSU0E0MDk2IFNVQiBDQQIQAYdE47cSAhzAE-JGw+ZMLDALBglghkgBZQMEAgEwCwYJKoZIhvcNAQEKBIICACFsHVA99IouWUOxVZWVZzOfXJSBU1agmVyWMLQydPotNLWPJxjK3pFX7zwtggy1Wd+q8yvE2YoeOBl43JMdwEiNb-cJg30AGc7+XKtPwEnQx90mPXXci4LuvwZ5v31axi3k5m64zPj5KtooEkTU18EbVxQsHJtz7zN8xdYffmA6JNIa0obXs-3G8nzWb3Nl0s-kic7bqtUYB9OidG2EWANp7GJGqwJv8flm0I4neF24n0wPZPHygw0ihusQlita9e9aZuW9-HuZVBf06fEEe2b+GieFtC6oKoAGHhAN721JlFq5tOzPzgp93dmyTmw3dtJmfzdNmpifKi3ZtV-LJKi2aql9U3UZFRA9ERpM4yfWEx6D2ube9wXXbiWpd9jVuHRw4baFpYtkaFOdbG4kKFerVnbXk5PrYHAsVolCq08w0b68F8XeWz1tMzHN4+v+KjVO-+zapg0lxra5epV4c3zwiLYVsyvb1tBMxNNNlm4QVMeFZ+fC3fnLedQBSoATxtT6ywGXCpu03UmEGZKO6dpdncu8X5LeAlZuLfpDHHkZpyVc7-bqa9FQwbKj6kJcFsE+rmxH9LOpHFMedqZu2KwAIkiA7EMnUi1U1w9igzWJxcpFDSznx3Ra+gp1c-42bPdD4MGgR37e1E0o7iRlhSGhmadWY--xqRMrGyKe6d96");

//            HashMap<String, Object> response = restTemplate.postForObject(url, new HttpEntity<Map>(map,headers), HashMap.class);
            long reqTime = System.currentTimeMillis();
            log.info("获取单个ECU灌装证书:请求" + reqTime + ":token:" + token + ":" + "url" + "=" + url);
            log.info("获取单个ECU灌装证书:请求" + reqTime + ":" + "vehicle_project_id" + "=" + vo.getProjectId() + "&" + "device_id" + "=" + vo.getDeviceID());
//            HashMap<String, Object> rps = (HashMap<String, Object>) restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<Map>(headers), HashMap.class).getBody();
            HashMap<String, Object> rps = new HashMap<>();
            log.info("获取单个ECU灌装证书:返回" + System.currentTimeMillis() + ":" + rps);
            saveSysLog(url, System.currentTimeMillis() - reqTime, rps);
            if (rps != null) {
                rps.put("cert", "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUdORENDQkJ5Z0F3SUJBZ0lRQVllVHBWcUFBQmQxS092UGJON3dBakFOQmdrcWhraUc5dzBCQVFzRkFEQmQKTVFzd0NRWURWUVFHRXdKRFRqRUxNQWtHQTFVRUNCTUNaMlF4Q3pBSkJnTlZCQWNUQW5ONk1Rc3dDUVlEVlFRSwpFd0pvZHpFTU1Bb0dBMVVFQ3hNRGFXRnpNUmt3RndZRFZRUUREQkJqYUdWdWFtbGhibDloY0dsMFpYTjBNQjRYCkRUSXpNRFF4T0RBNU1UVXdNRm9YRFRNek1USXhOakEzTkRRd01Gb3dQREVMTUFrR0ExVUVCaE1DUTA0eEN6QUoKQmdOVkJBZ1RBbWRrTVFzd0NRWURWUVFIRXdKemVqRVRNQkVHQTFVRUF4TUtZbUZwWkhVdWRHOXdORENDQWlJdwpEUVlKS29aSWh2Y05BUUVCQlFBRGdnSVBBRENDQWdvQ2dnSUJBTTBONFJtQVRHMVVHeGV3RDZlY3J2dnA1ZXBUCk54RjZoSDA4bGEyZ2habTRseVN4ZHpyWUNvSmM3Z1VWWHpnbmJSbmN0Z3RMV0F6ZGw3Nzg1cE83SlgvUXpxR1YKNzd0dzEvVVRyVDRDdHRJcU5DZXdhcXo2a3FVcGRnVjhHMDU5S0hlRjVTTkpzQlhqOVdmdGd6OVJMSTlzOUJsRApxTm1lSGpSK2Q3dDlYM01jNDJFbFd3M3RMK2FuWStmY3haSlBxQVlvcDFtY2RGRFBLMU52VHdJdWc3a2Foa0wvCjA2NVZyTWJIVWljK1ZWaGo4RW5DVmpELzkvTHhudEVDNE44NE5oTFN1Unl3MndmSHd5dnhjZkJKMUJKUTRjYXIKNzVYUGVJcXZWTzBuZlduTkU5TmZtakw5b2tqTjFpMENIdTVrL2dlQ2NsazRONzV1cis3KzJvVmJRZnlmZEhIaQptL1VwMGVTb093RFZxZDFxRzQ1MW1HYldHZ0RYc2dnenlxc2laSUxnb0hGM29vcEVVQjdFOUpjRFkxaGJRUVJ1CkdZNkRxVmtudWRPVlBMTnpURmZQWWJSSzQyZEt3UE0rVkI5Z1ZmeU0yRksyaU1wUUY5ZVlPSHdoS1F6OURhT3UKMWF4NXFITGdEZnpHTzhhNmxaZnF2TXJIbnZVdWNRR0ZyaWZiZElKcHZXZTVaRG5IT2VvQmFNV3JOZzdsYk5hVApwK1A0L3RjSHFkeVBTMWFrd2VDOFRMMUFnSS9BQVhiVXlEallKbXl3M1Y4SWhKZjZ6aTdUNnlaMnFZSC9CdFY1CjRLaUZMNnVibFE3bW1zcEZPWFRiTnV4NDc1SFY0dFlibDEvMStOanozWUNlcDgzdExSc2ZCeW9IZS9BdzQ4N1AKV2swczF4S3ZCNUZ6bjRTN0FnTUJBQUdqZ2dFUE1JSUJDekFPQmdOVkhROEJBZjhFQkFNQ0EvZ3dIUVlEVlIwbApCQll3RkFZSUt3WUJCUVVIQXdJR0NDc0dBUVVGQndNQk1Bd0dBMVVkRXdFQi93UUNNQUF3SFFZRFZSME9CQllFCkZGQU9TNzNrOTd4SjM2Wmo2RTMzUHI1RG1JcVJNQjhHQTFVZEl3UVlNQmFBRkdLQTVza3RvV1Vib3pzMzZ1U24KMXVHRVVHV0pNQlVHQTFVZEVRUU9NQXlDQ21KaGFXUjFMblJ2Y0RRd2RRWURWUjBmQkc0d2JEQnFvR2lnWm9aawphSFIwY0hNNkx5OXZZbk10Y0d0cExXSjFZMnRsZEM1dlluTXVZMjR0WldGemRDMHlNVEl1YUdSdFlYQXViWGxvCmRXRjNaV2xqYkc5MVpDNWpiMjB2WTNKc2N5OWphR1Z1YW1saGJsOWhjR2wwWlhOMEwwUmFPRzl3Ym5GaVVqQlQKTG1OeWJEQU5CZ2txaGtpRzl3MEJBUXNGQUFPQ0FnRUFyQ2VFRmJncjhYbXVNRlM4MzRDdmlOL2R2My9XQXJ6MwprRmVvQmlpT1A1dnJGbDczeHY4REZXYkFyYmFlYlE2OTd1Ty9ZU3NVQ0xZVThpZDNGMjlJK09LckZDVHVaWEZNClJCZHRJa25hOXgyUytvTnBIaGQrbnJGV09vODhNK2xhK0QzZ05VVkFhamtneWNHZHdGdTRUUDM4VWV4L0RMM1kKZlU0WHFoQm5HQVVyNGJpdWRVRzdyNXUyWUNhaWV0WEVJZEZhMUNuY0s3cU1nY0hvR2tma2JGcmdWd29DSVVPdgpXWjduWnhyRDRsbHRMNUhpT1pPVUlYM0FtZy9JWWxnNmNsR0FNd1dZSGF6V2kvU2lLeVVTZWFGQWpMYzd5VzU1CkRhRG9mTCtaWTVsaDdvQmxyaGFuaklHamdMQzdGZHM0c3h1dGE2Uk1KM3R4VmZMRGVtVm10RFZqZS9LRTR3QWUKbjB5WVRscnZEaTExNkx2Z3dTaFZnWXZMVXVsaCtyZjlIRnRHTGtJS0M4WmludkxXU3dPUWh3d3JLZ3RjMzV3WQozRmNBZXRFQ3N3cUMydGZsQVVpaTZYNUUycEg1M3E5WDlHN3duNm9KWkdNR3czSnJpNEgxUXhzZitkMElwK1hICi9rZmJUUmlZcTc1VTQrT2MyTWlWbE1qbUVaWE1WWkJCQXdNMmFrVUE5YkZrNGcwMEtiZitZWEdwM3hWaEIyenEKM1FCTWNmWEhDWGJ3M2xoYUFJQWVIM0hRVmttdmx4OG96cWZmNG1zS0JqZmtwcURCS2ppbjRxaFVLM1h2UVd2ZApsbmNGclBvRFphcU1LRHA0S0MzSGJ6cU9jVHZNZ3NTcTNtcjZLb2lVcWJXSFQwTEdNWW1DaFVCUUdjMmdEL0h0CnR0MDNzQUFEKzFzPQotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0tCg==");
                rps.put("failure_reason", "0");
                if (rps.containsKey("code")) {
                    result = getResult(rps.get("code"), LocalUtil.get("message.699") + ":" + rps.get("msg"), "");
                } else {
                    Object cert = rps.get("cert");
                    if (ObjectUtils.isEmpty(cert)) {
                        result = getResult(1, ObjectUtils.isNotEmpty(rps.get("failure_reason")) ? rps.get("failure_reason") : "get cert is null", "");
                    } else {
                        sysCacheDao.saveOrUpdate(RemoteDiagnosticConstant.ECU_FILLING_CERTIFICATE + ":" + vo.getProjectId() + ":" + vo.getDeviceID(), rps.get("cert").toString());
                        result = getResult(0, ObjectUtils.isNotEmpty(rps.get("failure_reason")) ? LocalUtil.get("message.699") + ":" + rps.get("failure_reason") : "success", rps.get("cert"));
                    }
                }
            } else {
                result = getResult(500, LocalUtil.get("message.698"), "");
            }

        } catch (Exception e) {
            log.error("获取ECU灌装证书异常:" + e.getMessage(), e);
            result = getResult(500, LocalUtil.get("message.698"), "");
        }

        return result;
    }

    private HashMap<String, Object> getResult(Object code, Object msg, Object data) {
        HashMap<String, Object> result = new HashMap<>();
//        result.put("key", "");
//        result.put("ca", "");
        result.put("cert", data);

        HashMap<String, Object> map = new HashMap<>();
        map.put("code", code);
        map.put("msg", msg);
        map.put("data", result);
        return map;
    }

    private void saveSysLog(String req, long time, Object result) {

        Gson gson = new Gson();

        SysLogEntity sysLog = new SysLogEntity();

        sysLog.setOperation("ECUFillingCertificate");

        //请求的方法名
        sysLog.setMethod("");

//        String params = gson.toJson(vo);
        sysLog.setParams(req);

        sysLog.setResult(result == null ? null : gson.toJson(result));

        //获取request
        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
        //设置IP地址
        sysLog.setIp(IPUtils.getIpAddr(request));

        //用户名
        sysLog.setUsername(ShiroUtils.getUserName());

        sysLog.setDisplayUserName(ShiroUtils.getUserDisplayName());

        sysLog.setTime(time);
        sysLog.setCreateDate(new Date());
        //保存系统日志
        sysLogService.save(sysLog);
    }
}
