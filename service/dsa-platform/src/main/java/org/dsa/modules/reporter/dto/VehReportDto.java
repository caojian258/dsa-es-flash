package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class VehReportDto implements Serializable {

    String ecuName;

    Integer onlineStatus;

    Integer dtcNumber;
}
