package org.dsa.modules.onlineUpdate.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dsa.common.utils.PageUtils;
import org.dsa.modules.onlineUpdate.dto.RuleDto;
import org.dsa.modules.onlineUpdate.entity.RuleEntity;

import java.util.List;
import java.util.Map;

/**
 * 规则
 *
 */
public interface RuleService extends IService<RuleEntity> {

    PageUtils queryPage(Map<String, Object> params);

    RuleDto info(Long id);

    void saveOrUpdate(RuleDto ruleDto);

    /**
     * @param softwareId 软件id
     * @return List<RuleEntity> 按软件版本升序排序
     */
    List<RuleEntity> existEffectiveRule(Long softwareId);

    RuleEntity getSoftwareNewestRuleBySoftwareId(Long softwareId);

    void disabledRule(Long softwareId,Integer status);

    void disabled(Long id);

    RuleDto queryBySwVersionId(Long swVersionId);

    /**
     * @param cVersion
     * @return 0未发布 1发布
     */
    Integer hasPublishCompVersion(Long cVersion);

}

