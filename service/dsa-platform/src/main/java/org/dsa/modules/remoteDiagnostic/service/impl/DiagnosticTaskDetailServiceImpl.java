package org.dsa.modules.remoteDiagnostic.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.remoteDiagnostic.dao.DiagnosticTaskDetailDao;
import org.dsa.modules.remoteDiagnostic.dao.DiagnosticTaskLogDao;
import org.dsa.modules.remoteDiagnostic.dao.DiagnosticTaskOverviewDao;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskDetailEntity;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskLogEntity;
import org.dsa.modules.remoteDiagnostic.service.DiagnosticTaskDetailService;
import org.dsa.modules.remoteDiagnostic.vo.PublicPageReqVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class DiagnosticTaskDetailServiceImpl implements DiagnosticTaskDetailService {
    @Autowired
    private DiagnosticTaskOverviewDao taskOverviewDao;
    @Autowired
    private DiagnosticTaskDetailDao taskDetailDao;
    @Autowired
    private DiagnosticTaskLogDao taskLogDao;

    @Override
    public Page<DiagnosticTaskDetailEntity> selectPage(PublicPageReqVo vo) {
        return null;
    }

    @Override
    public List<DiagnosticTaskDetailEntity> getDetailList(String sessionId) {
        return taskDetailDao.getListBySession(sessionId);
    }

    @Override
    public List<DiagnosticTaskLogEntity> getTaskLogList(String sessionId) {
        return taskLogDao.getListBySession(sessionId);
    }

    @Override
    public void saveDetail(Long taskId, String sessionId, String vin, String type) {
        DiagnosticTaskDetailEntity diagnosticTaskDetailEntity = new DiagnosticTaskDetailEntity();
        diagnosticTaskDetailEntity.setCampaignId(taskId);
        diagnosticTaskDetailEntity.setVin(vin);
        diagnosticTaskDetailEntity.setSessionId(sessionId);
        diagnosticTaskDetailEntity.setType(type);
        diagnosticTaskDetailEntity.setStartTime(new Date());
//        diagnosticTaskDetailEntity.setActive(active);

        taskDetailDao.insert(diagnosticTaskDetailEntity);
    }

    @Override
    public void saveDetail(Long taskId, String sessionId, String vin, String type,Integer action, String code) {
        DiagnosticTaskDetailEntity diagnosticTaskDetailEntity = new DiagnosticTaskDetailEntity();
        diagnosticTaskDetailEntity.setCampaignId(taskId);
        diagnosticTaskDetailEntity.setVin(vin);
        diagnosticTaskDetailEntity.setSessionId(sessionId);
        diagnosticTaskDetailEntity.setCode(code);
        diagnosticTaskDetailEntity.setType(type);
        diagnosticTaskDetailEntity.setAction(action);
        diagnosticTaskDetailEntity.setStartTime(new Date());

        taskDetailDao.insert(diagnosticTaskDetailEntity);

    }
}
