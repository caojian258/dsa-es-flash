package org.dsa.modules.sys.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class SysActiveUserDto implements Serializable {

    //0 serverToken 1 clientToken
    private Integer type;

    private Long userId;

    private Long workshopId;

    private String userName;

    private String workshopName;

    private Date updateTime;

    private Date expireTime;
}
