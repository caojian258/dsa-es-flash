package org.dsa.modules.reporter.vo.api;

import lombok.Data;

import java.io.File;

@Data
public class SessionLogReVo {
    private String vin; // 车架号
    private String model;// 车型
    private File log;
}
