package org.dsa.modules.onlineUpdate.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.dsa.modules.onlineUpdate.entity.RuleEntity;
import org.dsa.modules.onlineUpdate.entity.SoftwareVersionEntity;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SoftwareVersionVo extends SoftwareVersionEntity {

    /**
     * 上级版本
     */
    public String baseVersion;

    /**
     * 软件规则
     */
    public RuleEntity rule;

    /**
     * 组件关联
     */
    public List<CompVGroupVo> groups;


    /**
     * 入口
     */
    private Long componentId;
    /**
     * 入口名称
     */
    private String componentName;
}
