package org.dsa.modules.reporter.vo;

import lombok.Data;

import jakarta.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class VehicleReportVo implements Serializable {

    @NotBlank(message = "sessionInfoId不能为空")
    private String sessionInfoId;

    private String sessionId;

    @NotBlank(message = "开始时间不能为空")
    private String startTime;

    private String endTime;
}
