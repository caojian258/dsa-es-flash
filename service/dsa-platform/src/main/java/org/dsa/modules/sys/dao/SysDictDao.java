package org.dsa.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.sys.entity.SysDictEntity;

import java.util.List;

/**
 * 数据字典
 *
 */
@Mapper
public interface SysDictDao extends BaseMapper<SysDictEntity> {
    /**
     * 根据类型获取字典表信息
     * @param type
     * @return
     */
    List<SysDictEntity> getSysDictByType(@Param("type") String type);
}
