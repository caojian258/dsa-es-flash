package org.dsa.modules.reporter.service.impl;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.dsa.modules.reporter.dto.SqlBuilderDto;
import org.dsa.modules.reporter.enums.DateAttributeEnum;
import org.dsa.modules.reporter.enums.SqlOperatorEnum;
import org.dsa.modules.reporter.pojo.SqlBuilderParam;
import org.dsa.modules.reporter.service.SqlBuilderService;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class SqlBuilderServiceImpl implements SqlBuilderService {

	public static final String VERSION = "0.0.1";

	@Override
	public String render(SqlBuilderDto cube, Map<String, Object> conditionMap, Integer limit) {

		//cube 唯一标识
		String sqlId = cube.getId();
		boolean sqlDebug = true;
		//cube 原始的sql 不加条件的 不排序的
		String sqlBefore = cube.getSql();

		//cube top
		Integer top = null;
		if(null != limit){
			top = limit;
		}else {
			top = cube.getTop();
		}

		//cube 条件
		String condition = null;
		if(conditionMap != null && conditionMap.size()>0){
			condition = formatCondition(cube, conditionMap);
		}

		String sqlAfter = buildSql(sqlBefore, condition, top);
		// 打印至控制台
		if (sqlDebug) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			log.info("SqlBuilder Report ----------- " + format.format(new Date()) + " ------------------------------");
			log.info("sqlId       : {}", sqlId);
			log.info("before      : {}", sqlBefore);
			log.info("after       : {}", sqlAfter);
			log.info("parameter   : {}", JSONObject.toJSONString(conditionMap));
			log.info("--------------------------------------------------------------------------------");
		}
		return sqlAfter;
	}

	/**
	 * @todo 注意未来要判断没有group by的情况
	 * @param sql
	 * @param condition
	 * @param top
	 * @return
	 */
	@Override
	public String buildSql(String sql, String condition, Integer top) {
		log.info("buildSql: {}", sql);
		if(StrUtil.isEmpty(sql)){
			return null;
		}
		StringBuilder sb = new StringBuilder();
		int groupByPos =  sql.indexOf("group by");
		sb.append(sql, 0, groupByPos);

		if(StrUtil.isNotBlank(condition)){
			if(groupByPos>1 && groupByPos<sql.length()){
				sb.append(sb.toString().contains("where") ? " and " :  " where ").append(condition);
			}
		}

		sb.append(" ").append(sql.substring(groupByPos));

		if (!sql.contains("order by")) {
			sb.append(" order by value desc ");
		}

		if (null != top) {
			sb.append("  limit ").append(top);
		}
		return sb.toString();
	}


	public String formatCondition(SqlBuilderDto cube, Map<String, Object> mc) {
		String cc = cube.getCondition();
		List<SqlBuilderParam> params = new ArrayList<>();
		if (StringUtils.isNotBlank(cc)) {
			params =  generateCondition(cc.toLowerCase(), mc);
		}
		String condition = combineParam(params);
		String dateCondition = handleDateCondition(cube, mc);

		if(StrUtil.isNotBlank(dateCondition) && StrUtil.isNotBlank(condition)){
			condition += " and (" + dateCondition+ ")";
		}else if(StrUtil.isNotBlank(dateCondition)){
			condition = dateCondition;
		}
		log.info("buildSql formatCondition result: {}", condition);
		return condition;
	}

	private String handleDateCondition(SqlBuilderDto cube, Map<String, Object> mc){
		String startDate = null;
		String endDate = null;
		if(null == cube.getDateColumn()){
			return null;
		}
		if(mc.containsKey("startDate") && mc.get("startDate")!=null){
			startDate = mc.get("startDate").toString();
		}

		if(mc.containsKey("endDate") && mc.get("endDate")!=null){
			endDate = mc.get("endDate").toString();
		}

		return generateDateCondition(cube, startDate, endDate);
	}

	private String generateDateCondition(SqlBuilderDto cube, String startDate, String endDate){
		String condition = "";

		if(StrUtil.isNotBlank(startDate) && StrUtil.isNotBlank(endDate)){
			if (cube.getDateAttribute().equals(DateAttributeEnum.STRING.getValue())){
				condition = cube.getDateColumn() + " between '"+startDate+"' and '"+endDate+"'";
			}else if(cube.getDateAttribute().equals(DateAttributeEnum.TIMESTAMP.getValue()) ){
				condition = "to_char("+cube.getDateColumn()+", 'yyyy-mm-dd') between '"+startDate+"' and '"+endDate+"'";
			}else if(cube.getDateAttribute().equals(DateAttributeEnum.UNIX_TIMESTAMP.getValue())){
				condition = "to_char("+cube.getDateColumn()+", 'yyyy-mm-dd') between '"+startDate+"' and '"+endDate+"'";
			}else if(cube.getDateAttribute().equals(DateAttributeEnum.LONG_UNIX_TIMESTAMP.getValue())) {
				condition = "to_char(" + cube.getDateColumn() + ", 'yyyy-mm-dd') between '" + startDate + "' and '" + endDate + "'";
			}

		}else if(StrUtil.isNotBlank(startDate)){
			if (cube.getDateAttribute().equals(DateAttributeEnum.STRING.getValue())){
				condition = cube.getDateColumn() + " >= '"+startDate+"'";
			}else if(cube.getDateAttribute().equals(DateAttributeEnum.TIMESTAMP.getValue()) ){
				condition = "to_char("+cube.getDateColumn()+", 'yyyy-mm-dd') >= '"+startDate+"'";
			}else if(cube.getDateAttribute().equals(DateAttributeEnum.UNIX_TIMESTAMP.getValue())){
				condition = "to_char("+cube.getDateColumn()+", 'yyyy-mm-dd') >= '"+startDate+"'";
			}else if(cube.getDateAttribute().equals(DateAttributeEnum.LONG_UNIX_TIMESTAMP.getValue())) {
				condition = "to_char(" + cube.getDateColumn() + ", 'yyyy-mm-dd') >= '" + startDate + "'";
			}
		}
		log.info("generateDateCondition result: {}", condition);
		return condition;
	}

	/***
	 * @todo 1.map中的key 必须是小写字符 2.condition的变量内容去掉单引号
	 * @param condition
	 * @param conditionMap
	 * @return
	 */
	public List<SqlBuilderParam> generateCondition(String condition, Map<String, Object> conditionMap){
		String cc = condition.toLowerCase();
		List<SqlBuilderParam> params = new ArrayList<>();
		String[] conditions = cc.split("and");
		int len = conditions.length;

		List<String> operators = Stream.of(SqlOperatorEnum.values()).map(
				SqlOperatorEnum::getValue).collect( Collectors.toList());

		for (int i=0; i< len;i++){
			String item = conditions[i].trim();
			String[] items = new String[3];
			items = item.split("\\s", 3);
			if(items.length == 1){
				String operate =  operators.stream().filter(s -> item.split(s).length == 2 ).findFirst().orElse(null);
				if(null == operate ){
					continue;
				}
				String[] kvs = item.split(operate);
				items = new String[]{kvs[0], operate, kvs[1]};
			}

			log.info("generateCondition: {}",Arrays.stream(items).collect(Collectors.joining(",")));

			if(items.length<3 || StrUtil.isEmpty(items[0]) || StrUtil.isEmpty(items[2])){
				continue;
			}

			String key = items[2].replace("{","").replace("}", "").replaceAll("'", "");
			if(null !=conditionMap.get(key) ){
				SqlBuilderParam p = new SqlBuilderParam();
				if(StrUtil.isEmpty(items[1])){
					items[1] = SqlOperatorEnum.EQUAL.getValue();
				}

				p.setKey(items[0]);
				p.setOperator(items[1]);
				p.setValue("\'" + conditionMap.get(key).toString() + "\'");

				if(items[1].toUpperCase().equals(SqlOperatorEnum.LIKE.getValue())){
					p.setValue("\'%" + conditionMap.get(key).toString() + "%\'");
				}

				boolean hasUnknown = false;
				if(items[1].toUpperCase().equals(SqlOperatorEnum.IN.getValue())){
					if(conditionMap.get(key) instanceof List){
						List<Object> listValue = (List<Object>) conditionMap.get(key);
						List<String> targetValue = new ArrayList<>();
						for (Object lv: listValue) {
							if(lv.equals("unknown")){
								hasUnknown = true;
							}else{
								targetValue.add(lv + "");
							}
						}
						String valueStr = "";
						if(targetValue.size()>0){
							valueStr = targetValue.stream().map(s -> "\'" + s + "\'").collect(Collectors.joining(", "));
							valueStr = "("+valueStr+")";
						}

						if(hasUnknown){
							if(valueStr.length()>0){
								valueStr +=" or " +items[0] +" is NULL or "+items[0] + " = ''";
							}else{
								p.setOperator(SqlOperatorEnum.IS.getValue());
								valueStr = " NULL or "+items[0] + " = ''";
							}
						}
						p.setValue(  valueStr );

					}else if(conditionMap.get(key) instanceof String){
						String[] valueStrs = conditionMap.get(key).toString().split(";");
						String valueStr = Arrays.stream(valueStrs).map(s -> "\'" + s + "\'").collect(Collectors.joining(", "));
						p.setValue("(" + valueStr +")" );
					}else{
						p.setOperator(SqlOperatorEnum.EQUAL.getValue());
						p.setValue("\'" + conditionMap.get(key).toString() + "\'");
					}
				}

				if( "unknown".equals(conditionMap.get(key).toString())){
					p.setKey(items[0]);
					p.setOperator(SqlOperatorEnum.IS.getValue());
					p.setValue("NULL or " + items[0] + " = ''");
				}
				if("N/A".equals(conditionMap.get(key).toString())){
					p.setKey(items[0]);
					p.setOperator(SqlOperatorEnum.IS.getValue());
					p.setValue("NULL or " + items[0] + " = ''");
				}
				params.add(p);
			}

		}
		return params;
	}

	public String combineParam(List<SqlBuilderParam> params){
		String str = "";
		List<String> builderStr = new ArrayList<>();
		for (SqlBuilderParam param: params){
			builderStr.add("(" + param.getKey() +" "+ param.getOperator() + " " + param.getValue() + ")");
		}
		str = builderStr.stream().collect(Collectors.joining(" and "));

		log.info("combineParam result: {}", str);
		return str;
	}
}