package org.dsa.modules.oss.service;

import cn.hutool.core.util.StrUtil;
import org.dsa.modules.oss.config.StorageConfig;
import org.dsa.modules.oss.dto.FileDto;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class DiskStorageService {

    /***
     * 配置
     */
    protected StorageConfig config;

    /***
     * 备份基础目录
     */
    protected String basePath;


    /***
     * 默认的工作目录
     */
    protected String workPath;

    /***
     * 创建目录
     */
    protected void makeDir(){

        String dirFormat = config.getDirFormat();
        Date date = new Date();
        String baseDir = config.getBasePath();
        SimpleDateFormat sdf = new SimpleDateFormat(dirFormat);
        String dirStr = sdf.format(date);
        File file = new File( baseDir.trim() + File.separator + dirStr);
        if(!file.exists()){
            file.mkdirs();
            file.setReadable(true);
            file.setWritable(true);
        }

        if(StrUtil.isNotBlank(config.getWorkDir())){
            File work = new File(config.getWorkDir());
            if(!work.exists()){
                work.mkdirs();
                work.setReadable(true);
                work.setWritable(true);
            }
            this.workPath = work.getAbsolutePath();
        }


        basePath = file.getAbsolutePath();
    }

    /***
     * 构造文件目录
     * @param fileName
     * @return
     */
    public String buildPath(String fileName){
        if(null == this.basePath){
            makeDir();
        }
        return this.basePath+ File.separator + fileName;
    }

    /***
     * 构造文件目录
     * @param fileName
     * @return
     */
    public String buildWorkPath(String fileName){
        if(null != this.workPath){
            return this.workPath+ File.separator + fileName;
        }
        return null;
    }


    public void setWorkPath(String path){
        File work = new File(path);
        if(!work.exists()){
            work.mkdirs();
            work.setReadable(true);
            work.setWritable(true);
        }
        this.workPath = work.getAbsolutePath();
    }
    /***
     * 返回工作目录
     * @return
     */
    public String getWorkPath(){
        return workPath;
    }

    /***
     * 返回基本目录
     * @return
     */
    public String getBasePath(){
        return basePath;
    }

    /***
     * 获取配置信息
     * @return
     */
    public StorageConfig getConfig(){
        return config;
    }

    /***
     * 从form表单上传数据
     * @param file
     * @param filePath 绝对路径
     * @return
     */
    public abstract FileDto upload(MultipartFile file, String filePath);

    /****
     * 目录移动文件
     * @param targetPath 目标目录 绝对路径
     * @param sourcePath 源目录 绝对路径
     * @return
     */
    public abstract FileDto move(String targetPath, String sourcePath);

    /***
     * 根据文件流保存文件
     * @param fileStream
     * @param filePath
     * @return
     */
    public abstract FileDto upload(InputStream fileStream, String filePath) throws IOException;


    public abstract FileDto upload(byte[] bytes, String filePath) throws IOException;
}
