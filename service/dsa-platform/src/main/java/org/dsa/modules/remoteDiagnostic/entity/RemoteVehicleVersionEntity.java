package org.dsa.modules.remoteDiagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import jakarta.validation.Valid;
import lombok.Data;
import lombok.ToString;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.common.validator.group.UpdateGroup;
import org.dsa.modules.remoteDiagnostic.dto.ECUDependenceGroupDto;
import org.dsa.modules.remoteDiagnostic.pojo.BaseEntity;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@ToString
@TableName("r_vehicle_version")
public class RemoteVehicleVersionEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    @NotNull(message = "id不能为空", groups = {UpdateGroup.class})
    private Long id;

    @NotNull(message = "车型不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private Long typeId;

    private Long poolId;

    private Long versionNumber;

    @NotBlank(message = "版本名称不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String versionName;

    @NotBlank(message = "版本类型不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String versionType;

    private String descriptionTi;

    private String releaseNodeTi;

    @NotNull(message = "状态不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private Integer status;

    private Integer isCross;

    @TableField(exist = false)
    private String releaseNode;

    @TableField(exist = false)
    private String description;

    @TableField(exist = false)
    private String  typeName;

    @TableField(exist = false)
    private List<RemoteEcuIdentificationInfoEntity> expectedIdentifications;

    @TableField(exist = false)
    private List<RemoteEcuIdentificationInfoEntity> targetIdentifications;

    @TableField(exist = false)
    private List<RemoteVehicleEcuGroupEntity> ecuGroups;

    @TableField(exist = false)
    private List<RemoteVehicleVersionEcuGroupEntity> vehicleVersionEcuGroups;

    @TableField(exist = false)
    private RemoteVehiclePoolEntity pool;

    @TableField(exist = false)
    private Integer flashFlag;

    @TableField(exist = false)
    @Valid
    private List<ECUDependenceGroupDto> ecuDependencyGroup;
}
