package org.dsa.modules.remoteDiagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import org.dsa.modules.remoteDiagnostic.pojo.BaseEntity;

import java.io.Serializable;

@Data
@ToString
@TableName("r_version_ecu_rely")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RemoteVersionEcuRelyEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long vehicleVersionId;

    private String name;

    private String groupId;

    private Long ecuGroupId;

    private Long ecuId;

    private Long ecuVersionId;

    private Integer priority;

}
