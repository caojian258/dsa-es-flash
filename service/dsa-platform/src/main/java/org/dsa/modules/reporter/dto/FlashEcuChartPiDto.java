package org.dsa.modules.reporter.dto;

import lombok.Data;

@Data
public class FlashEcuChartPiDto {

    private Long value;

    private String ecuName;
}
