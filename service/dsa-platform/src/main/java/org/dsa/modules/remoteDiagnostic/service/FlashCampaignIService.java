package org.dsa.modules.remoteDiagnostic.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.dsa.modules.remoteDiagnostic.entity.FlashCampaignEcuEntity;
import org.dsa.modules.remoteDiagnostic.entity.FlashCampaignEntity;
import org.dsa.modules.remoteDiagnostic.vo.Flash.FlashCampaignPageVo;
import org.dsa.modules.remoteDiagnostic.vo.Flash.FlashCampaignResVo;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FlashCampaignIService extends IService<FlashCampaignEntity> {

    public FlashCampaignEntity queryCampaign(String vin, Long versionId);

    public void disabledById(Long id, Long userId);

    public void enabledById(Long id, Long userId);

    Page<FlashCampaignEntity> queryPage(FlashCampaignPageVo vo);

    public void saveCampaign(FlashCampaignEntity entity);

    public void saveEcuVersion(Long campaignId, Long ecuId, Long ecuVersionId);

    public FlashCampaignEcuEntity queryEcuVersion(Long campaignId, Long ecuId, Long ecuVersionId);

    public String upload(MultipartFile file) throws IOException;

    public String buildDownloadUrl(String path);

    Page<FlashCampaignResVo> queryPageForClient(FlashCampaignPageVo vo);

}
