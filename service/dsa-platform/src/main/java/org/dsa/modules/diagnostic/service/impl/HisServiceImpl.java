package org.dsa.modules.diagnostic.service.impl;

import ch.qos.logback.classic.Logger;
import cn.hutool.core.convert.Convert;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.*;
import org.dsa.modules.diagnostic.dao.*;
import org.dsa.modules.diagnostic.dto.SessionActionDto;
import org.dsa.modules.diagnostic.entity.*;
import org.dsa.modules.diagnostic.service.HisService;
import org.dsa.modules.reporter.enums.FlashEnum;
import org.dsa.modules.sys.dao.SysUserDao;
import org.dsa.modules.sys.entity.SysUserEntity;
import org.dsa.modules.vehicle.dao.DVehicleInfoDao;
import org.dsa.modules.vehicle.dao.DVehicleTypeDao;
import org.dsa.modules.vehicle.entity.DVehicleInfoEntity;
import org.dsa.modules.vehicle.entity.DVehicleTypeEntity;
import org.dsa.modules.vehicle.entity.VehicleInfoEntity;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import java.util.*;

@Service("hisService")
public class HisServiceImpl implements HisService {
    private final Logger logger = (Logger) LoggerFactory.getLogger(this.getClass());

    @Resource
    private DSessionInfoDao dSessionInfoDao;

    @Resource
    private DSessionActionDao dSessionActionDao;

    @Resource
    private DVehicleInfoDao dVehicleInfoDao;

    @Resource
    private DVehicleTypeDao dVehicleTypeDao;

    @Resource
    private DActionEcuDao dActionEcuDao;

    @Resource
    private DActionVersionDao dActionVersionDao;

    @Resource
    private DActionDtcDao dActionDtcDao;

    @Resource
    private DActionInfoDao dActionInfoDao;

    @Resource
    private DFunctionActionDao dFunctionActionDao;

    @Resource
    private SysUserDao sysUserDao;

    private List<String> titleList;

    @Resource
    private RedisUtils redisUtils;

    @Override
    public PageUtils queryPage(Map<String, Object> params, HttpServletRequest httpRequest) {
        logger.info("queryDicAnalysisPage begin");
        Map<String, Object> map = new HashMap<>();
        map.put(Constant.PAGE, String.valueOf(params.get(Constant.PAGE)));
        map.put(Constant.LIMIT, String.valueOf(params.get(Constant.LIMIT)));

        IPage<DSessionInfoEntity> page = new Query<DSessionInfoEntity>().getPage(map);
        String vin = (String) params.get("vin");
        String startTime = (String) params.get("startTime");
        String endTime = (String) params.get("endTime");
        //故障码
        String dtcCode = (String) params.get("dtcCode");
        //车型信息
        List<String> vehicleTypeIds = (List<String>) params.get("vehicleTypeIds");

        //诊断功能码,诊断操作码
        String functionAction = (String) params.get("functionAction");
        String actionId = (String) params.get("actionId");
        String factionCode = "";
        String actionCode = "";
        if (StringUtils.isNotBlank(functionAction)) {
            if (functionAction.contains(",")){
                String[] fas = functionAction.split(",");
                factionCode = fas[0];
                actionCode = fas[1];
            }else {
                factionCode = functionAction;
            }
        }
        if (StringUtils.isNotEmpty(startTime)) {
            startTime = startTime + " 00:00:00";
        }
        if (StringUtils.isNotEmpty(endTime)) {
            endTime = endTime + " 23:59:59";
        }
        //来自远程用户只能查看自己
        Integer userId = Convert.toInt(params.get("userId"));
        if (ObjectUtils.isNotNull(userId)){
            IPage<DSessionInfoEntity> pageObj = dSessionInfoDao.selectSessionList(page, vin,userId,
                    vehicleTypeIds, factionCode, actionCode, actionId, dtcCode, startTime, endTime);
            return new PageUtils(pageObj);
        }
        IPage<DSessionInfoEntity> pageObj = dSessionInfoDao.selectSessionList(page, vin,null,
                vehicleTypeIds, factionCode, actionCode, actionId, dtcCode, startTime, endTime);
        return new PageUtils(pageObj);
    }

    @Override
    public DSessionReportEntity getSessionInfoById(Long id, HttpServletRequest httpRequest) {
        String token = httpRequest.getHeader("token");
        //如果header中不存在token，则从参数中获取token
        if(StringUtils.isBlank(token)){
            token = httpRequest.getParameter("token");
        }
        DSessionReportEntity dSessionReportEntity = new DSessionReportEntity();
        //1.查session信息
        searchSessionInfo(dSessionReportEntity,id);
        //2.查询车型信息
        searchVehicleInfo(dSessionReportEntity);
        //3.车辆版本信息
        searchVersionInfos(dSessionReportEntity);
        //4.车辆故障信息
        searchDtcInfos(dSessionReportEntity);
        //5.操作记录
        searchActionInfos(dSessionReportEntity);
        //6. 查询诊断信息
        searchDiagnosticInfos(dSessionReportEntity,id,token);
        //7. 查询健康检测信息
        searchVehicleHealthy(dSessionReportEntity);
        return dSessionReportEntity;
    }

    private void searchVehicleHealthy(DSessionReportEntity dSessionReportEntity) {
        List<String> actionIds = new ArrayList<>(16);
        //根据session_id查找actionId,根据actionId获取ecuName
        if ("VEHICLE".equals(dSessionReportEntity.getSource())||"OFFLINE".equals(dSessionReportEntity.getSource())){
            List<String> voActionIds = dSessionActionDao.selectListBySessionId(dSessionReportEntity.getSessionId());
            actionIds.addAll(voActionIds);
        }else{
            List<String> remoteActionIds = dSessionActionDao.selectListBySessionIdAndFunctionCode(dSessionReportEntity.getSessionId(),Constant.Function.VHSS.getFunctionCode());
            actionIds.addAll(remoteActionIds);
        }
        List<DActionEcuEntity> ecuEntityList = new ArrayList<>(16);
        if (actionIds.size()>0){
            ecuEntityList.addAll(dActionEcuDao.selectBatchActionIds(actionIds));
        }
        dSessionReportEntity.setEcuList(ecuEntityList);
    }

    private void searchDiagnosticInfos(DSessionReportEntity dSessionReportEntity,Long id,String token) {
        //根据id查询诊断信息，
        DSessionInfoEntity dSessionInfoEntity = dSessionInfoDao.selectById(id);
        if (ObjectUtils.isNotNull(dSessionInfoEntity)){
            dSessionReportEntity.setSource(dSessionInfoEntity.getSource());
            dSessionReportEntity.setStartTime(dSessionInfoEntity.getStartTime());
            dSessionReportEntity.setEndTime(dSessionInfoEntity.getEndTime());
            String source = dSessionInfoEntity.getSource();
            dSessionReportEntity.setSource(source);
            if ("OFFLINE".equals(source)) {
                //离线查询本地库
                if (ObjectUtils.isNotNull(dSessionInfoEntity.getDiagnosticUserId())){
                    SysUserEntity sysUserEntity = sysUserDao.queryByUserId(dSessionInfoEntity.getDiagnosticUserId());
                    if (ObjectUtils.isNotNull(sysUserEntity)){
                        dSessionReportEntity.setDiagnosticUserName(sysUserEntity.getUsername());
                        dSessionReportEntity.setWorkShop(sysUserEntity.getWorkshopName());
                    }
                }
            }else{
                //vehicle DiagnosticDisplayUserName-> workshop+"-"+name
                if ("VEHICLE".equals(dSessionReportEntity.getSource())){
                    if(ObjectUtils.isNotNull(dSessionInfoEntity.getDiagnosticDisplayUserName())&&dSessionInfoEntity.getDiagnosticDisplayUserName().contains("-")){
                        String[] split = dSessionInfoEntity.getDiagnosticDisplayUserName().split("-");
                        dSessionReportEntity.setWorkShop(split[0]);
                        dSessionReportEntity.setDiagnosticUserName(split[1]);
                    }
                }else {
                    SysUserEntity sysUserEntity = sysUserDao.queryByUserName(dSessionInfoEntity.getDiagnosticDisplayUserName());
                    dSessionReportEntity.setDiagnosticUserName(dSessionInfoEntity.getDiagnosticDisplayUserName());
                    dSessionReportEntity.setActiveUserName(dSessionInfoEntity.getActiveDisplayUserName());
                    dSessionReportEntity.setDiagnosticUserName(sysUserEntity.getChineseName());
                    dSessionReportEntity.setWorkShop(sysUserEntity.getWorkshopName());
                }

            }
        }
    }


    @Override
    public PageUtils queryDicAnalysisPage(Map<String, Object> params) {
        logger.info("queryDicAnalysisPage begin");

        Map<String, Object> map = new HashMap<>();
        map.put(Constant.PAGE, String.valueOf(params.get(Constant.PAGE)));
        map.put(Constant.LIMIT, String.valueOf(params.get(Constant.LIMIT)));

        IPage<AnalysisDtcDataEntity> page = new Query<AnalysisDtcDataEntity>().getPage(map);
        ArrayList<String> ecuNames = (ArrayList<String>) params.get("ecuNames");
        String startTime = (String) params.get("startTime");
        String endTime = (String) params.get("endTime");
        String vin = (String) params.get("vin");
        List<String> vehicleTypeIds = (List<String>) params.get("vehicleTypeIds");
        if (StringUtils.isNotEmpty(startTime)) {
            startTime = startTime + " 00:00:00";
        }
        if (StringUtils.isNotEmpty(endTime)) {
            endTime = endTime + " 23:59:59";
        }

        IPage<AnalysisDtcDataEntity> pageObj = dActionDtcDao.selectAnalysisDtcData(page, ecuNames,titleList, startTime, endTime, vin, vehicleTypeIds);
        logger.info("queryDicAnalysisPage end");
        return new PageUtils(pageObj);
    }

    @Override
    public Map<String, Object> dicAnalysisPir(AnalysisEntity analysisEntity){
        logger.info("dicAnalysisPir begin");
        //入参校验
        if(StringUtils.isEmpty(analysisEntity.getStartTime()) || StringUtils.isEmpty(analysisEntity.getEndTime())){
            throw new RRException( Constant.Msg.CHECK_INPUT);
        }
        analysisEntity.setStartTime(analysisEntity.getStartTime() + " 00:00:00");
        analysisEntity.setEndTime(analysisEntity.getEndTime() + " 23:59:59");

        Map<String, Object> map = new HashMap<>();
        //查询
        List<AnalysisDtcPirEntity> list = dActionDtcDao.selectAnalysisDtcPir(analysisEntity);

        titleList  = new ArrayList<>();
        list.forEach((analysisDtcPirEntity -> {titleList.add(analysisDtcPirEntity.getName());}));
        map.put("title", titleList);
        map.put("data", list);
        logger.info("dicAnalysisPir end");
        return map;
    }

    @Override
    public Map<String, Object> dicAnalysisBar(AnalysisEntity analysisEntity) {
        logger.info("dicAnalysisBar begin");
        //入参校验
        if(StringUtils.isEmpty(analysisEntity.getStartTime()) || StringUtils.isEmpty(analysisEntity.getEndTime())){
            throw new RRException(Constant.Msg.CHECK_INPUT);
        }
        analysisEntity.setStartTime(analysisEntity.getStartTime() + " 00:00:00");
        analysisEntity.setEndTime(analysisEntity.getEndTime() + " 23:59:59");

        Map<String, Object> map = new HashMap<>();
        //查询
        List<AnalysisDtcBarEntity> list = dActionDtcDao.selectAnalysisDtcBar(analysisEntity);

        //X轴 月份 排序
        Set<String> monthSet = new HashSet<String>();
        list.stream().forEach((analysisDtcBarEntity -> {
            monthSet.add(analysisDtcBarEntity.getDtcMonth());
        }));
        Set<String> sortSet = new TreeSet<String>(String::compareTo);
        sortSet.addAll(monthSet);
        //柱形图数据
        List<AnalysisDtcBarDataEntity> dataList = new ArrayList<>();
        //data 统计
        if(ObjectUtils.isNull(titleList)){
            titleList = new ArrayList<>(16);
        }
        for (String dtc : titleList) {
            AnalysisDtcBarDataEntity analysisDtcBarDataEntity = new AnalysisDtcBarDataEntity();
            analysisDtcBarDataEntity.setName(dtc);
            analysisDtcBarDataEntity.setType("bar");

            List<Integer> countList = new ArrayList<>();
            for (String month : monthSet) {
                int dtcCount = 0;
                for (AnalysisDtcBarEntity analysisDtcBarEntity : list) {
                    if (dtc.equals(analysisDtcBarEntity.getDtcCodeHex()) && month.equals(analysisDtcBarEntity.getDtcMonth())) {
                        dtcCount = analysisDtcBarEntity.getDtcCount();
                        break;
                    }
                }
                countList.add(dtcCount);
            }
            analysisDtcBarDataEntity.setData(countList);
            dataList.add(analysisDtcBarDataEntity);
        }
        map.put("dtcCode", titleList);
        map.put("dtcMonth", sortSet);
        map.put("dtcData", dataList);
        logger.info("dicAnalysisBar end");
        return map;
    }

    @Override
    public PageUtils queryReqAnalysisPage(Map<String, Object> params) {
        logger.info("queryReqAnalysisPage begin");
        Map<String, Object> map = new HashMap<>();
        map.put(Constant.PAGE, String.valueOf(params.get(Constant.PAGE)));
        map.put(Constant.LIMIT, String.valueOf(params.get(Constant.LIMIT)));

        IPage<AnalysisDtcDataEntity> page = new Query<AnalysisDtcDataEntity>().getPage(map);
//        将SESSION_SOURCE的value转换为dbName用作查询
        List<String> sourcesValue = (ArrayList<String>) params.get("sources");
        List<String> sources = new ArrayList<>(3);
        sourcesValue.stream().filter(item -> Constant.SESSION_SOURCE.isSessionSourceByValue(item))
                .forEach(item -> sources.add(Constant.SESSION_SOURCE.getDbNameByValue(item)));
        List<String> vehicleTypeIds = (ArrayList<String>) params.get("vehicleTypeIds");
        String vin = (String) params.get("vin");
        String startTime = (String) params.get("startTime");
        String endTime = (String) params.get("endTime");
        if (StringUtils.isNotEmpty(startTime)) {
            startTime = startTime + " 00:00:00";
        }

        if (StringUtils.isNotEmpty(endTime)) {
            endTime = endTime + " 23:59:59";
        }
        Set<String> actionCodes = new HashSet<>();
        //title做拆分处理，title格式为functionCode或者functionCode_actionCode
        if(ObjectUtils.isNull(titleList)){
            titleList = new ArrayList<>(16);
        }

        Set<String> noDtcSet = new HashSet<>();
        Set<String> dtcSet = new HashSet<>();
        //funcionCode 分离为 含有DTC的和不含DTC的
        List<String> flashTitles = new ArrayList<>();
        flashTitles.add(FlashEnum.ONLINE.getValue());
        flashTitles.add(FlashEnum.REMOTE.getValue());

        titleList.forEach(title->{
            if (title.contains("_") && !flashTitles.contains(title)){
                String[] s = title.split("_");
                dtcSet.add(s[0]);
                if(s[1].equals("EXTENDEDDATA")){
                    actionCodes.add("extendedData");
                }else{
                    actionCodes.add(StringUtils.lowerCase(s[1]));
                }
            }else {
                noDtcSet.add(title);
            }
        });
        //dtcSet
        logger.info("queryReqAnalysisPage dtcSet:{}", JSONObject.toJSONString(dtcSet));
        logger.info("queryReqAnalysisPage actionCodes:{}", JSONObject.toJSONString(actionCodes));
        logger.info("queryReqAnalysisPage noDtcSet:{}", JSONObject.toJSONString(noDtcSet));

        IPage<AnalysisReqDataEntity> pageObj = dActionInfoDao.queryReqAnalysisPage(page, sources, vehicleTypeIds,noDtcSet,dtcSet,actionCodes, vin, startTime, endTime);

        logger.info("queryReqAnalysisPage end");
        return new PageUtils(pageObj);
    }

    @Override
    public Map<String, Object> reqAnalysisPir(AnalysisEntity analysisEntity) {
        logger.info("reqAnalysisPir begin");
        //入参校验
        if (StringUtils.isEmpty(analysisEntity.getStartTime()) || StringUtils.isEmpty(analysisEntity.getEndTime())) {
            throw new RRException(Constant.Msg.CHECK_INPUT);
        }
        analysisEntity.setStartTime(analysisEntity.getStartTime() + " 00:00:00");
        analysisEntity.setEndTime(analysisEntity.getEndTime() + " 23:59:59");
        ArrayList<String> sources = new ArrayList<>(3);
        Arrays.asList(analysisEntity.getSources()).forEach(source->sources.add(Constant.SESSION_SOURCE.getDbNameByValue(source)));
        analysisEntity.setSources(Convert.toStrArray(sources));
        Map<String, Object> map = new HashMap<>();
        //查询
        List<AnalysisReqPirEntity> list = dActionInfoDao.selectReqAnalysisPir(analysisEntity,Constant.FUNCTION_FILTER);
        titleList = new ArrayList<>(16);
        List<Map<String, Object>> dataList = new ArrayList<>(16);
        list.stream().forEach((pir -> {
            Map<String, Object> map1 = new HashMap<>();
            map1.put("name",pir.getFunctionCode());
            map1.put("value",pir.getRequestCount());
            dataList.add(map1);
            titleList.add(pir.getFunctionCode());
        }));

        map.put("title", titleList);
        map.put("data", dataList);
        logger.info("reqAnalysisPir end");
        return map;
    }

    @Override
    public Map<String, Object> reqAnalysisBar(AnalysisEntity analysisEntity) {
        logger.info("reqAnalysisBar begin");
        //入参校验
        if(StringUtils.isEmpty(analysisEntity.getStartTime()) || StringUtils.isEmpty(analysisEntity.getEndTime())){
            throw new RRException( Constant.Msg.CHECK_INPUT );
        }
        analysisEntity.setStartTime(analysisEntity.getStartTime() + " 00:00:00");
        analysisEntity.setEndTime(analysisEntity.getEndTime() + " 23:59:59");
        ArrayList<String> sources = new ArrayList<>(3);
        Arrays.asList(analysisEntity.getSources()).forEach(source->sources.add(Constant.SESSION_SOURCE.getDbNameByValue(source)));
        analysisEntity.setSources(Convert.toStrArray(sources));
        Map<String, Object> map = new HashMap<>();
        //查询
        List<AnalysisReqBarEntity> list = dActionInfoDao.selectReqAnalysisBar(analysisEntity);
        //X轴 月份 排序
        Set<String> monthSet = new HashSet<String>();
        if (ObjectUtils.isNotNull(list)){
            list.stream().filter(ObjectUtils::isNotNull).forEach((analysisReqBarEntity -> {
                monthSet.add(analysisReqBarEntity.getRequestMonth());
            }));
        }
        Set<String> sortSet = new TreeSet<String>((o1, o2) -> o1.compareTo(o2));
        sortSet.addAll(monthSet);
        //柱状图的每月也按照总的topN的诊断功能 来显示
        if (ObjectUtils.isNull(titleList)){
            Map<String, Object> pirMap = reqAnalysisPir(analysisEntity);
            if (ObjectUtils.isNotNull(pirMap.get("title"))){
                titleList.addAll((List<String>) pirMap.get("title"));
            }else {
                titleList = new ArrayList<>(16);
            }
        }
        //柱形图数据
        List<AnalysisReqBarDataEntity> dataList = new ArrayList<AnalysisReqBarDataEntity>();
        //data 统计
        for (String req : titleList) {
            AnalysisReqBarDataEntity analysisDtcBarDataEntity = new AnalysisReqBarDataEntity();
            analysisDtcBarDataEntity.setName(req);
            analysisDtcBarDataEntity.setType("bar");
            List<Integer> countList = new ArrayList<>();
            for (String month : monthSet) {
                int dtcCount = 0;
                for (AnalysisReqBarEntity analysisReqBarEntity : list) {
                    if (req.startsWith("DTC")
                            && req.equals(analysisReqBarEntity.getFunctionCode())
                            && month.equals(analysisReqBarEntity.getRequestMonth())) {
                        dtcCount = analysisReqBarEntity.getRequestCount();
                        break;
                    }
                    if ((req.equals(analysisReqBarEntity.getFunctionCode()))
                            && month.equals(analysisReqBarEntity.getRequestMonth())) {
                        dtcCount = analysisReqBarEntity.getRequestCount();
                        break;
                    }
                }
                countList.add(dtcCount);
            }
            analysisDtcBarDataEntity.setData(countList);
            dataList.add(analysisDtcBarDataEntity);
        }

        map.put("dtcCode", titleList);
        map.put("dtcMonth", sortSet);
        map.put("dtcData", dataList);
        logger.info("reqAnalysisBar end");
        return map;
    }



    /**
     * 查session信息
     * @param dSessionReportEntity
     * @param id
     */
    private void searchSessionInfo(DSessionReportEntity dSessionReportEntity, Long id) {
        DSessionInfoEntity dSessionInfoEntity = dSessionInfoDao.selectById(id);
        if (ObjectUtils.isNotNull(dSessionInfoEntity)){
            dSessionReportEntity.setVin(dSessionInfoEntity.getVin());
            dSessionReportEntity.setSessionId(dSessionInfoEntity.getSessionId());
            dSessionReportEntity.setStartTime(dSessionInfoEntity.getStartTime());
            dSessionReportEntity.setSource(dSessionInfoEntity.getSource());
            if(null != dSessionInfoEntity.getWorkshop()){
                dSessionReportEntity.setWorkShop(dSessionInfoEntity.getWorkshop());
            }

            if(null != dSessionInfoEntity.getDiagnosticDisplayUserName()){
                dSessionReportEntity.setDiagnosticUserName(dSessionInfoEntity.getDiagnosticDisplayUserName());
            }

        }
    }

    /**
     * 查询车型信息
     * @param dSessionReportEntity
     */
    private void searchVehicleInfo(DSessionReportEntity dSessionReportEntity) {
        DVehicleInfoEntity dVehicleInfoEntity = dVehicleInfoDao.selectOneByVin(dSessionReportEntity.getVin());
        if(dVehicleInfoEntity == null){
            throw new RRException( Constant.Msg.CHECK_VIN );
        }
        VehicleInfoEntity vehicleInfoEntity = new VehicleInfoEntity();
        List<DVehicleTypeEntity> list = dVehicleTypeDao.selectListById(dVehicleInfoEntity.getVehicleTypeId());
        if(list != null && list.size() >0){
            for(DVehicleTypeEntity t : list){
                if(t.getLevel() == Constant.VEHICLE_TYPE.MAKER.getLevel()){
                    vehicleInfoEntity.setMaker(t.getValue());
                }else if(t.getLevel() == Constant.VEHICLE_TYPE.MODEL.getLevel()){
                    vehicleInfoEntity.setModel(t.getValue());
                }else if(t.getLevel() == Constant.VEHICLE_TYPE.PLATFORM.getLevel()){
                    vehicleInfoEntity.setPlatform(t.getValue());
                }else if(t.getLevel() == Constant.VEHICLE_TYPE.TRIM_LEVEL.getLevel()){
                    vehicleInfoEntity.setTrimLevel(t.getValue());
                }else if(t.getLevel() == Constant.VEHICLE_TYPE.YEAR.getLevel()){
                    vehicleInfoEntity.setYear(t.getValue());
                }
            }
        }else{
            throw new RRException( Constant.Msg.CHECK_VIN_HAS_TYPE);
        }
        vehicleInfoEntity.setProductionDate(StringUtils.isEmpty(dVehicleInfoEntity.getProductionDate())?"":dVehicleInfoEntity.getProductionDate());
        vehicleInfoEntity.setPlaceOfProduction(StringUtils.isEmpty(dVehicleInfoEntity.getPlaceOfProduction())?"":dVehicleInfoEntity.getPlaceOfProduction());

        dSessionReportEntity.setVehicleInfo(vehicleInfoEntity);
    }


    /**
     * 车辆版本信息
     * @param dSessionReportEntity
     */
    private void searchVersionInfos(DSessionReportEntity dSessionReportEntity) {
        List<DActionVersionEntity> list = dActionVersionDao.selectListBySessionId2(dSessionReportEntity.getSessionId());
        dSessionReportEntity.setVersionList(list);
    }

    /**
     * 车辆故障信息
     * @param dSessionReportEntity
     */
    private void searchDtcInfos(DSessionReportEntity dSessionReportEntity) {
        //根据session_id查询action_id,一个session_id可能包含多个action_id
        //action_id>2 获取首尾action_id(第一次最后一次) 根据action_id获取dtc_code,比较得出修复状态
        String source = dSessionReportEntity.getSource();
        String functionCode = null;
        if ("REMOTE".equals(source)){
            functionCode=Constant.Function.VHSS.getFunctionCode();
        }
        List<String> actionIds = dSessionActionDao.selectListBySessionIdAndFunctionCode(dSessionReportEntity.getSessionId(),functionCode);
        List<DActionDtcEntity> res = new ArrayList<>(16);
        if (actionIds.size()>0){
            res.addAll(dActionDtcDao.selectListByBatchActionId(actionIds));
        }
        dSessionReportEntity.setDtcList(res);
    }


    /**
     * 操作记录
     * @param dSessionReportEntity
     */
    private void searchActionInfos(DSessionReportEntity dSessionReportEntity) {
        List<DActionInfoEntity> list = dActionInfoDao.selectListBySessionId(dSessionReportEntity.getSessionId());
        if(ObjectUtils.isNotNull(list)){
            for(DActionInfoEntity dActionInfoEntity : list){
                dActionInfoEntity.setMessage(ObjectUtils.isNull(dActionInfoEntity.getMessage())?dActionInfoEntity.getMessageEn():dActionInfoEntity.getMessage());
                dActionInfoEntity.setFunctionCode(dActionInfoEntity.getFunctionCode());
                if (Constant.Function.DTC.getFunctionCode().equals(dActionInfoEntity.getFunctionCode())){
                    dActionInfoEntity.setFunctionAction(dActionInfoEntity.getFunctionCode()+","+dActionInfoEntity.getActionCode());
                }else if (Constant.Function.END.getFunctionCode().equals(dActionInfoEntity.getFunctionCode())) {
                    // condition="SESSION_END" concat functionCode and endStatus
                    if (ObjectUtils.isNotNull(dActionInfoEntity.getEndStatus())){
                        dActionInfoEntity.setFunctionAction(dActionInfoEntity.getFunctionCode()+dActionInfoEntity.getEndStatus());
                    }else {
                        dActionInfoEntity.setFunctionAction(dActionInfoEntity.getFunctionCode());
                    }
                }else {
                    dActionInfoEntity.setFunctionAction(dActionInfoEntity.getFunctionCode());
                }
                if (ObjectUtils.isNotNull(dActionInfoEntity.getResults())){
                    if("1".equals(dActionInfoEntity.getResults())){
                        dActionInfoEntity.setResults(Constant.RESULTS.SUCCESS.getName());
                    }else{
                        dActionInfoEntity.setResults(Constant.RESULTS.FAIL.getName());
                    }
                }else{
                    dActionInfoEntity.setResults(Constant.RESULTS.FAIL.getName());
                }
            }
        }
        dSessionReportEntity.setActionList(list);
    }



    /**
     * 诊断功能 select 数据
     */
    @Override
    public List<OptionEntity> getFunctionList() {
        List<OptionEntity> list = new ArrayList<>();
        List<DFunctionActionEntity> dFunctionActionEntitys =dFunctionActionDao.selectShowList();
        if(ObjectUtils.isNull(dFunctionActionEntitys)){
            return list;
        }
        for(DFunctionActionEntity d : dFunctionActionEntitys){
            OptionEntity optionEntity = new OptionEntity();
            optionEntity.setId(d.getId());
            String value = "";
            if(d.getActionName().isEmpty()){
                value = d.getFunctionName();
            }else{
                value = d.getFunctionName()+"-"+d.getActionName();
            }
            optionEntity.setValue(value);
        }
        return list;
    }

    @Override
    public List<Map<String,String>> getEcuList(Map<String, Object> params){
        List<String> ecuNameList = dActionDtcDao.getEcuNameList();
        List<Map<String,String>> ecuTabList = new ArrayList<>(16);
        ecuNameList.forEach(ecuName->{
            Map<String,String> component = new HashMap<>(3);
            component.put("title",ecuName);
            component.put("name",ecuName);
            component.put("content",ecuName);
            ecuTabList.add(component);
        });
        logger.info("queryDicAnalysisPage end");
        return ecuTabList;
    }

    @Override
    public List<Map<String,String>> queryDtcInEcuList(Map<String, Object> params) {
        logger.info("queryDicAnalysisPage begin");
        ArrayList<String> ecuNames = (ArrayList<String>) params.get("ecuNames");
        String startTime = (String) params.get("startTime");
        String endTime = (String) params.get("endTime");
        String vin = (String) params.get("vin");
        List<String> vehicleTypeIds = (List<String>) params.get("vehicleTypeIds");
        if (StringUtils.isNotEmpty(startTime)) {
            startTime = startTime + " 00:00:00";
        }
        if (StringUtils.isNotEmpty(endTime)) {
            endTime = endTime + " 23:59:59";
        }
        List<Map<String,String>> res = dActionDtcDao.selectDtcByEcu(ecuNames,startTime, endTime, vehicleTypeIds);
        logger.info("queryDicAnalysisPage end");
        return res;
    }

    @Override
    public List<SessionActionDto> queryVehicleSession(String vin, String startTime, String function, String action) {
        return dSessionActionDao.findSessionAction(vin, startTime, function, action);
    }

    @Override
    public List<String> getEcus() {

        return dActionEcuDao.queryNams();
    }

}