package org.dsa.modules.sys.entity;

import lombok.Data;
import lombok.ToString;
import org.springframework.ldap.odm.annotations.Attribute;
import org.springframework.ldap.odm.annotations.Entry;

@Data
@ToString
@Entry(objectClasses = {"dealerOrg"}, base = "cn=dealer,dc=INCEPTIO,dc=COM")
public class LdapWorkshopEntity {
    /**
     * 主键
     */
    @Attribute
    String cn;

    /**
     * 主键
     */
    @Attribute
    String sn;

    /**
     * 主键
     */
    @Attribute
    String ou;

    /**
     * 备注信息
     */
    @Attribute
    String description;

    /**
     * 经销商地址
     */
    @Attribute
    String postalAddress;
}
