package org.dsa.modules.oem.vo;


import lombok.Data;
import lombok.ToString;
import org.dsa.common.validator.group.ReqGroup;

import jakarta.validation.constraints.NotBlank;

@Data
@ToString(callSuper = true)
public class SWTSGetCertReqVo {

    private String pcid;

    private String csr;

    private String type;

    private String name;

}
