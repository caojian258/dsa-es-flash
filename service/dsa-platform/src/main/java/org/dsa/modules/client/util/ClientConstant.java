package org.dsa.modules.client.util;

import java.util.HashMap;
import java.util.Map;

public class ClientConstant {


    public static Map<String, Object> getCertResult(Integer code, Long exp, String cert,String key) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("code", code);
        map.put("expiredDays", exp);
        map.put("cert", cert);
        map.put("key", key);
        return map;
    }

}
