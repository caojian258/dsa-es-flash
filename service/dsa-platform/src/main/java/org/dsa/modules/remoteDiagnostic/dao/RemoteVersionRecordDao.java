package org.dsa.modules.remoteDiagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVersionRecordEntity;

@Mapper
public interface RemoteVersionRecordDao extends BaseMapper<RemoteVersionRecordEntity> {


}
