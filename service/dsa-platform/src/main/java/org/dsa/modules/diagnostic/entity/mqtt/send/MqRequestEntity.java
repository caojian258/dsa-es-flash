package org.dsa.modules.diagnostic.entity.mqtt.send;

import lombok.Data;

import java.io.Serializable;

/**
 * mq 消息 实体类
 * 
 */
@Data
public class MqRequestEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * mqHeader
	 */
	private MqRequestHeaderEntity header;
	/**
	 * mqBody
	 */
	private MqRequestBodyEntity body;

}
