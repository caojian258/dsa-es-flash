package org.dsa.modules.remoteDiagnostic.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticConditionEntity;
import org.dsa.modules.remoteDiagnostic.vo.CheckNameReqVo;
import org.dsa.modules.remoteDiagnostic.vo.CheckStatusReqVo;
import org.dsa.modules.remoteDiagnostic.vo.DiagnosticConditionPageVo;

import java.util.List;

public interface DiagnosticConditionService {

    Page<DiagnosticConditionEntity> selectPage(DiagnosticConditionPageVo vo);

    DiagnosticConditionEntity getInfo(Long id);

    void saveCond(DiagnosticConditionEntity fun);

    void updateCond(DiagnosticConditionEntity fun);

    List<DiagnosticConditionEntity> getConditions();

    List<DiagnosticConditionEntity> getConditions(List<Long> ids,Long taskId);

    Boolean check(CheckNameReqVo vo);

    Boolean checkStatus(CheckStatusReqVo vo);
}
