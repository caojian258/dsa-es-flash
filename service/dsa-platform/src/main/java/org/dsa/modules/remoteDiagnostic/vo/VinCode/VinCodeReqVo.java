package org.dsa.modules.remoteDiagnostic.vo.VinCode;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString(callSuper = true)
public class VinCodeReqVo {

    private Long id;

    private List<String> vin;

    private Long tag;
}
