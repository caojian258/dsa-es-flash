package org.dsa.remote;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import jakarta.annotation.Resource;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.reporter.constant.RedisConstant;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.DefaultTypedTuple;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.scripting.support.ResourceScriptSource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisTest {

    @Resource
    private RedisUtils redisUtils;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Test
    public void testTag(){
        redisUtils.lSet(RedisConstant.TAG_BASE_KEY, "1697439240023");
        redisUtils.lSet(RedisConstant.TAG_BASE_KEY, "1697439720010");
    }

    @Test
    public void test() {
        List<Object> list = new ArrayList<>();
        list.add("1");
        list.add("aa");
        list.add("bb");
        boolean testList = redisUtils.lSet("testList", list, 60);
        System.out.println(testList);
        List<Object> objects = redisUtils.lGet("testList", 0, -1);

        System.out.println(objects.toString());

        redisUtils.lRemove("testList", 2, "cc");

        objects = redisUtils.lGet("testList", 0, -1);

        System.out.println(objects.toString());
    }


    @Test
    public void method() {
        List<Object> list = new ArrayList<>();
        list.add("1");
        list.add("aa");
        list.add("bb");
        boolean testList = redisUtils.lSet("testList", list, 60);
        Set<ZSetOperations.TypedTuple<Object>> set = new HashSet<>();
        set.add(new DefaultTypedTuple<Object>("133", (double)0));
        set.add(new DefaultTypedTuple<Object>("222", (double)0));
        set.add(new DefaultTypedTuple<Object>("33", (double)0));
        set.add(new DefaultTypedTuple<Object>("43", (double)0));
        set.add(new DefaultTypedTuple<Object>("511", (double)0));

        redisUtils.zAdd("testZSet", set);
        redisUtils.expire("testZSet", 60 * 5);
//        redisTemplate.opsForZSet().add("testZSet", set, 0);
        System.out.println("-----------------------");
        Set<Object> testZSet = redisTemplate.opsForZSet().range("testZSet", 0, 3);
        System.out.println(testZSet);

        DefaultRedisScript<List> redisScript = new DefaultRedisScript<>();
        redisScript.setResultType(List.class);
        redisScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("redis/vin_code.lua")));
        List<String> list1 = new ArrayList<>();
        list1.add("testZSet");
        String arg = "";
        List execute;
        if (StringUtils.isBlank(arg)) {
            execute = redisTemplate.opsForZSet().getOperations().execute(redisScript, list1);
        } else {
            execute = redisTemplate.opsForZSet().getOperations().execute(redisScript, list1, arg);
        }

        System.out.println(execute);
    }

}
