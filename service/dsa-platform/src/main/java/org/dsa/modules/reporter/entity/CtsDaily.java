package org.dsa.modules.reporter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/***
 * 诊断操作按天统计
 */
@Data
@Accessors(chain = true)
@TableName("d_cts_daily")
public class CtsDaily extends BaseEntity{

    @TableId(type = IdType.AUTO)
    private Long id;

    /***
     * 来源
     */
    private String source;

    /***
     * 车架号
     */
    private String vin;

    /**
     * 车型
     */
    private Long vehicleTypeId;


    /**
     * function-action
     */
    private String ctsName;

    /**
     * 功能码
     */
    private String functionCode;
    /**
     * 操作码
     */
    private String actionCode;


    /***
     * 日期
     */
    private String ctsDay;

    /***
     * 诊断次数
     */
    private Long ctsNum;

}
