package org.dsa.modules.reporter.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.reporter.dto.ClientLoginDto;
import org.dsa.modules.reporter.entity.ClientUser;
import org.dsa.modules.reporter.vo.client.ClientPageReVo;

import java.util.Map;

public interface ClientUserService {

    public void updateLogin(ClientLoginDto dto);

    Map<String, Object> clientDevice(String pcid);

    Page<ClientUser> selectClientPage(ClientPageReVo vo);
}
