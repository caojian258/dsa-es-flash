package org.dsa.modules.reporter.dto;

import lombok.Data;

@Data
public class SessionStateDto {

    private String vciIdentification;

    private String vciSerialNumber;

    private String date;

    private String language;

    private String userName;
}
