package org.dsa.modules.remoteDiagnostic.service;

import org.dsa.modules.remoteDiagnostic.entity.RemoteVersionRecordEntity;

import java.util.List;

public interface RemoteVersionRecordService {


    public List<RemoteVersionRecordEntity> getList(String type, Long id);
}
