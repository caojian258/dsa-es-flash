package org.dsa.modules.diagnostic.service.impl;

import cn.hutool.core.io.FileUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import net.lingala.zip4j.exception.ZipException;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.exception.RRException;
import org.dsa.common.service.LinuxService;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.DateUtils;
import org.dsa.common.utils.ShiroUtils;
import org.dsa.common.utils.ZipUtil;
import org.dsa.common.utils.ZipUtils;
import org.dsa.modules.diagnostic.dao.PackageVersionDao;
import org.dsa.modules.diagnostic.dto.DDiagPackageVersionDTO;
import org.dsa.modules.diagnostic.dto.PackageCompareDTO;
import org.dsa.modules.diagnostic.dto.PackageCompareReqDTO;
import org.dsa.modules.diagnostic.entity.DDiagPackageVersionEntity;
import org.dsa.modules.diagnostic.entity.DVersionInfoEntity;
import org.dsa.modules.diagnostic.service.PackageVersionService;
import org.dsa.modules.diagnostic.service.VersionService;
import org.dsa.modules.vehicle.entity.DVehicleInfoEntity;
import org.dsa.modules.vehicle.service.VehicleService;
import org.dsa.modules.vehicle.service.VehicleTypeService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.DigestUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author weishunxin
 * @since 2023-05-29
 */
@Slf4j
@Service
public class PackageVersionServiceImpl extends ServiceImpl<PackageVersionDao, DDiagPackageVersionEntity> implements PackageVersionService {

    @Autowired
    private VersionService versionService;
    @Autowired
    private VehicleService vehicleService;
    @Autowired
    private VehicleTypeService vehicleTypeService;
    @Autowired
    private LinuxService linuxService;
    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Value("${file.menu.path}")
    private String menuPath;
    @Value("${file.package.path}")
    private String packagePath;
    @Value("${file.version.download.path}")
    private String versionDownloadPath;
    @Value("${file.package.zip_pwd}")
    private String zipPwd;

    /**
     *
     * 根据车型查询包版本列表
     *
     * @param vehicleTypeId vehicleTypeId
     * @return List<DDiagPackageVersionEntity>
     */
    @Override
    public List<DDiagPackageVersionEntity> list(Long vehicleTypeId) {
        LambdaQueryWrapper<DDiagPackageVersionEntity> qw = Wrappers.lambdaQuery();
        qw.eq(DDiagPackageVersionEntity::getVehicleTypeId, vehicleTypeId)
                .orderByAsc(DDiagPackageVersionEntity::getId);
        return baseMapper.selectList(qw);
    }

    @Override
    public DDiagPackageVersionDTO getInfo(Long id) {
        DDiagPackageVersionEntity entity = baseMapper.selectById(id);
        if (Objects.isNull(entity)) {
            throw new RRException(Constant.Msg.CHECK_INPUT);
        }

        DDiagPackageVersionDTO packageVersionDTO = new DDiagPackageVersionDTO();
        BeanUtils.copyProperties(entity, packageVersionDTO);
        DVersionInfoEntity odxVersionEntity = versionService.getDVersionInfoById(entity.getOdxVersionId());
        DVersionInfoEntity otxVersionEntity = versionService.getDVersionInfoById(entity.getOtxVersionId());
        packageVersionDTO.setOdxVersionNum(odxVersionEntity.getVersionNum());
        packageVersionDTO.setOtxVersionNum(otxVersionEntity.getVersionNum());
        return packageVersionDTO;
    }

    @Override
    public List<DDiagPackageVersionEntity> list(Long vehicleTypeId, String status, boolean idDesc) {
        LambdaQueryWrapper<DDiagPackageVersionEntity> qw = Wrappers.lambdaQuery();
        qw.eq(DDiagPackageVersionEntity::getVehicleTypeId, vehicleTypeId)
                .eq(DDiagPackageVersionEntity::getStatus, status)
                .orderByDesc(idDesc, DDiagPackageVersionEntity::getId);
        return baseMapper.selectList(qw);
    }

    @Override
    public void savePackage(DDiagPackageVersionEntity entity) {
        // check
        Long vehicleTypeId = entity.getVehicleTypeId();
        if (this.hasNotReleasedPackage(vehicleTypeId)) {
            throw new RRException(Constant.Msg.VERSION_CHECK_STATUS);
        }

        boolean release = Constant.versionStatus.OPEN.getCode().equals(entity.getStatus());
        if (release) {
            this.checkReleasePackage(entity);
        }

        List<DDiagPackageVersionEntity> list = list(vehicleTypeId, Constant.versionStatus.OPEN.getCode(), true);

        String parentVersionNum;
        if (CollectionUtils.isEmpty(list)) {
            parentVersionNum = "0";
        } else {
            parentVersionNum = list.get(0).getVersionNum();
        }

        Date now = new Date();
        String versionNum = DateUtils.format(now,"yyyyMMddHHmm") + "_V" + list.size();
        entity.setId(null);
        entity.setVersionNum(versionNum);
        entity.setParentVersionNum(parentVersionNum);
        //entity.setStatus(Constant.versionStatus.DEV.getCode());
        entity.setCreatedAt(now);
        entity.setCreatedUserId(ShiroUtils.getUserId());
        super.save(entity);

        if (release) {
            this.invalidateCache(entity.getVehicleTypeId());
        }
    }

    @Override
    public void updatePackage(DDiagPackageVersionEntity entity) {
        // release
        boolean release = Constant.versionStatus.OPEN.getCode().equals(entity.getStatus());
        if (release) {
            this.checkReleasePackage(entity);
        }

        // update
        entity.setUpdatedAt(new Date());
        entity.setUpdatedUserId(ShiroUtils.getUserId());
        super.updateById(entity);
        if (release) {
            this.invalidateCache(entity.getVehicleTypeId());
        }
    }

    @Override
    public void copyPackage(Long id) {
        DDiagPackageVersionEntity entity = baseMapper.selectById(id);
        if (Objects.isNull(entity)) {
            throw new RRException(Constant.Msg.CHECK_INPUT);
        }

        entity.setId(null);
        entity.setUpdatedAt(null);
        entity.setUpdatedUserId(null);
        entity.setStatus(Constant.versionStatus.DEV.getCode());
        this.savePackage(entity);
    }

    @Override
    public PackageCompareDTO comparePackage(PackageCompareReqDTO compareReqDTO) {
        log.info("comparePackage params={}", JSON.toJSON(compareReqDTO));
        DVehicleInfoEntity vehicleInfo = vehicleService.getOne(compareReqDTO.getVin());
        if (Objects.isNull(vehicleInfo)) {
            log.error("vin={} is not exist.", compareReqDTO.getVin());
            throw new RRException(Constant.Msg.CHECK_VIN);
        }

        // req params
        Long vehicleTypeId = vehicleInfo.getVehicleTypeId();
        String packageVersion = compareReqDTO.getDiagPackageVersion();
        String odxVersion = compareReqDTO.getPdxVersion();
        String otxVersion = compareReqDTO.getPtxVersion();
        boolean increment = compareReqDTO.isIncrement();

        if (StringUtils.isEmpty(packageVersion)) {
            return this.getLatestPackage(vehicleTypeId, increment, packageVersion, odxVersion, otxVersion, null);
        }

        DDiagPackageVersionEntity packageVersionEntity = this.get(vehicleTypeId, packageVersion);
        if (Objects.isNull(packageVersionEntity)) {
            log.error("packageVersion={} is not exist. vehicleTypeId is {} ", packageVersion, vehicleTypeId);
            throw new RRException(Constant.Msg.CHECK_INPUT);
        }

        DDiagPackageVersionEntity latestPackageVersion = this.getLatest(vehicleTypeId);
        if (Objects.isNull(latestPackageVersion)) {
            log.info("res---package---无版本");
            return null;
        }

        if (!Objects.equals(packageVersion, latestPackageVersion.getVersionNum())) {
            return this.getLatestPackage(vehicleTypeId, increment, packageVersion, odxVersion, otxVersion, packageVersionEntity);
        }

        DVersionInfoEntity odxVersionEntity = versionService.getDVersionInfoById(packageVersionEntity.getOdxVersionId());
        DVersionInfoEntity otxVersionEntity = versionService.getDVersionInfoById(packageVersionEntity.getOtxVersionId());

        if (Objects.equals(odxVersion, odxVersionEntity.getVersionNum())
                && Objects.equals(otxVersion, otxVersionEntity.getVersionNum())) {
            log.info("res---package");
            return null;
        }

        String zipFileName = this.getPackageFileName(increment, vehicleTypeId, packageVersion, packageVersion, odxVersion, otxVersion);
        PackageCompareDTO compareDTO = this.getPackage(vehicleTypeId, zipFileName);
        if (Objects.nonNull(compareDTO)) {
            log.info("res---package");
            return compareDTO;
        }

        // res obj
        PackageCompareDTO packageCompareDTO = new PackageCompareDTO();
        List<String> filePaths = new ArrayList<>();

        this.initPackageCompareDTO(packageCompareDTO);

        if (!Objects.equals(odxVersion, odxVersionEntity.getVersionNum())) {
            String odxFilePath;
            if (increment) {
                if (Objects.equals(Constant.fileType.FULL.getCode(), odxVersionEntity.getFileType())) {
                    odxFilePath = versionDownloadPath + odxVersionEntity.getVersionFileName();
                } else {
                    DVersionInfoEntity entity = versionService.get(vehicleTypeId, Constant.versionType.ODX.getName(), odxVersion);
                    if (Objects.isNull(entity)) {
                        throw new RRException(Constant.Msg.CHECK_INPUT);
                    }
                    odxFilePath = this.getIncrement(vehicleTypeId, entity, odxVersionEntity, Constant.versionType.ODX.getName());
                }
            } else {
                odxFilePath = versionDownloadPath + odxVersionEntity.getVersionFileName();
            }

            packageCompareDTO.setPdxVersion(odxVersionEntity.getVersionNum());
            packageCompareDTO.setI18nOdx(this.getFileName(odxVersionEntity.getDbFilePath()));
            packageCompareDTO.setPdxPackage(this.getFileName(odxFilePath));
            filePaths.add(odxFilePath);
            filePaths.add(odxVersionEntity.getDbFilePath());
        }

        if (!Objects.equals(otxVersion, otxVersionEntity.getVersionNum())) {
            packageCompareDTO.setPtxVersion(otxVersionEntity.getVersionNum());
            packageCompareDTO.setI18nOtx(this.getFileName(otxVersionEntity.getDbFilePath()));
            packageCompareDTO.setMenuFileName(this.getFileName(otxVersionEntity.getMenuFilePath()));

            String otxFilePath;
            if (increment) {
                if (Objects.equals(Constant.fileType.FULL.getCode(), otxVersionEntity.getFileType())) {
                    otxFilePath = versionDownloadPath + otxVersionEntity.getVersionFileName();
                } else {
                    DVersionInfoEntity entity = versionService.get(vehicleTypeId, Constant.versionType.OTX.getName(), otxVersion);
                    if (Objects.isNull(entity)) {
                        throw new RRException(Constant.Msg.CHECK_INPUT);
                    }
                    otxFilePath = this.getIncrement(vehicleTypeId, entity, otxVersionEntity, Constant.versionType.OTX.getName());
                }
            } else {
                otxFilePath = versionDownloadPath + otxVersionEntity.getVersionFileName();
            }

            packageCompareDTO.setPtxPackage(this.getFileName(otxFilePath));
            filePaths.add(otxFilePath);
            filePaths.add(otxVersionEntity.getDbFilePath());
            filePaths.add(otxVersionEntity.getMenuFilePath());
        }

        String perspectiveFileName = "swts-perspective.xml";
        String themeFileName = "swts-theme.xml";
        String workflowFileName = "swts-workflow.xml";
        String topologyFileName = "swts-topology.svg";

        filePaths.add(menuPath + perspectiveFileName);
        filePaths.add(menuPath + themeFileName);
        filePaths.add(menuPath + workflowFileName);
        filePaths.add(menuPath + topologyFileName);

        String md5;
        try {
            boolean c = ZipUtils.compressFiles(filePaths, packagePath + zipFileName, zipPwd);
            if (!c) {
                log.error("zip files error.");
                throw new RRException("zip files error.");
            }
            md5 = DigestUtils.md5DigestAsHex(Files.newInputStream(Paths.get(packagePath + zipFileName)));
        } catch (IOException e) {
            log.error("operate files error.", e);
            throw new RRException("operate files error.");
        }

        packageCompareDTO.setPackageName(zipFileName);
        packageCompareDTO.setUrl(packagePath + zipFileName);
        packageCompareDTO.setMd5(StringUtils.isEmpty(md5) ? "" : md5);
        packageCompareDTO.setPackageFileSize(new File(packagePath + zipFileName).length());
        packageCompareDTO.setDiagPackageVersion(packageVersionEntity.getVersionNum());
        //packageCompareDTO.setMenuFileName(this.getFileName(otxVersionEntity.getMenuFilePath()));
        packageCompareDTO.setPerspectiveFileName(perspectiveFileName);
        packageCompareDTO.setThemeFileName(themeFileName);
        packageCompareDTO.setWorkflowFileName(workflowFileName);
        packageCompareDTO.setTopologyFileName(topologyFileName);

        String key = "package" + Constant.STRIKE + "cache" + Constant.STRIKE + vehicleTypeId;
        redisTemplate.opsForHash().put(key, zipFileName, JSON.toJSONString(packageCompareDTO));
        log.info("res---package");
        return packageCompareDTO;
    }

    @Override
    public DDiagPackageVersionEntity get(Long vehicleTypeId, String versionNum) {
        LambdaQueryWrapper<DDiagPackageVersionEntity> qw = Wrappers.lambdaQuery();
        qw.eq(DDiagPackageVersionEntity::getVehicleTypeId, vehicleTypeId)
                .eq(DDiagPackageVersionEntity::getVersionNum, versionNum);
        return baseMapper.selectOne(qw);
    }

    @Override
    public DDiagPackageVersionEntity getLatest(Long vehicleTypeId) {
        LambdaQueryWrapper<DDiagPackageVersionEntity> qw = Wrappers.lambdaQuery();
        qw.eq(DDiagPackageVersionEntity::getVehicleTypeId, vehicleTypeId)
                .orderByDesc(DDiagPackageVersionEntity::getId)
                .last("limit 1");
        return baseMapper.selectOne(qw);
    }

    @Override
    public PackageCompareDTO getLatestPackage(Long vehicleTypeId, boolean increment, String packageVersion, String odxVersion, String otxVersion, DDiagPackageVersionEntity packageVersionEntity) {
        DDiagPackageVersionEntity latestPackage = this.getLatest(vehicleTypeId);
        if (Objects.isNull(latestPackage)) {
            log.info("res---package---无版本");
            return null;
        }

        String zipFileName = this.getPackageFileName(increment, vehicleTypeId, packageVersion, latestPackage.getVersionNum(), odxVersion, otxVersion);
        PackageCompareDTO compareDTO = this.getPackage(vehicleTypeId, zipFileName);
        if (Objects.nonNull(compareDTO)) {
            log.info("res---package");
            return compareDTO;
        }

        DVersionInfoEntity odxEntity = null;
        DVersionInfoEntity otxEntity = null;
        if (StringUtils.isEmpty(packageVersion)) {
            increment = false;
        } else {
            if (StringUtils.isNotEmpty(odxVersion)) {
                odxEntity = versionService.get(vehicleTypeId, Constant.versionType.ODX.getName(), odxVersion);
                if (Objects.isNull(odxEntity)) {
                    log.error("vehicleTypeId={} does not has {} ODX version.", vehicleTypeId, odxVersion);
                    throw new RRException(Constant.Msg.CHECK_INPUT);
                }
            }

            if (StringUtils.isNotEmpty(otxVersion)) {
                otxEntity = versionService.get(vehicleTypeId, Constant.versionType.OTX.getName(), otxVersion);
                if (Objects.isNull(otxEntity)) {
                    log.error("vehicleTypeId={} does not has {} OTX version.", vehicleTypeId, otxVersion);
                    throw new RRException(Constant.Msg.CHECK_INPUT);
                }
            }
        }

        DVersionInfoEntity odxVersionEntity = versionService.getDVersionInfoById(latestPackage.getOdxVersionId());
        DVersionInfoEntity otxVersionEntity = versionService.getDVersionInfoById(latestPackage.getOtxVersionId());

        String odxFilePath;
        String otxFilePath;
        if (increment) {
            if (Objects.equals(Constant.fileType.FULL.getCode(), odxVersionEntity.getFileType())) {
                odxFilePath = versionDownloadPath + odxVersionEntity.getVersionFileName();
            } else {
                odxFilePath = this.getIncrement(vehicleTypeId, odxEntity, odxVersionEntity, Constant.versionType.ODX.getName());
            }

            if (Objects.equals(Constant.fileType.FULL.getCode(), otxVersionEntity.getFileType())) {
                otxFilePath = versionDownloadPath + otxVersionEntity.getVersionFileName();
            } else {
                otxFilePath = this.getIncrement(vehicleTypeId, otxEntity, otxVersionEntity, Constant.versionType.OTX.getName());
            }
        } else {
            odxFilePath = versionDownloadPath + odxVersionEntity.getVersionFileName();
            otxFilePath = versionDownloadPath + otxVersionEntity.getVersionFileName();
        }

        String odxDbFilePath = odxVersionEntity.getDbFilePath();
        String otxDbFilePath = otxVersionEntity.getDbFilePath();
        String perspectiveFileName = "swts-perspective.xml";
        String themeFileName = "swts-theme.xml";
        String workflowFileName = "swts-workflow.xml";
        String topologyFileName = "swts-topology.svg";

        List<String> filePaths = Lists.newArrayList(odxFilePath, odxDbFilePath, otxFilePath, otxDbFilePath);
        filePaths.add(menuPath + perspectiveFileName);
        filePaths.add(menuPath + themeFileName);
        filePaths.add(menuPath + workflowFileName);
        filePaths.add(menuPath + topologyFileName);
        filePaths.add(otxVersionEntity.getMenuFilePath());

        String md5;
        try {
            boolean c = ZipUtils.compressFiles(filePaths, packagePath + zipFileName, zipPwd);
            if (!c) {
                log.error("zip files error.");
                throw new RRException("zip files error.");
            }
            md5 = DigestUtils.md5DigestAsHex(Files.newInputStream(Paths.get(packagePath + zipFileName)));
        } catch (IOException e) {
            log.error("operate files error.", e);
            throw new RRException("operate files error.");
        }

        // res
        PackageCompareDTO packageCompareDTO = new PackageCompareDTO();
        packageCompareDTO.setPackageName(zipFileName);
        packageCompareDTO.setUrl(packagePath + zipFileName);
        packageCompareDTO.setMd5(md5);
        packageCompareDTO.setPackageFileSize(new File(packagePath + zipFileName).length());
        packageCompareDTO.setDiagPackageVersion(latestPackage.getVersionNum());
        packageCompareDTO.setPdxVersion(odxVersionEntity.getVersionNum());
        packageCompareDTO.setPtxVersion(otxVersionEntity.getVersionNum());
        packageCompareDTO.setPdxPackage(this.getFileName(odxFilePath));
        packageCompareDTO.setPtxPackage(this.getFileName(otxFilePath));
        packageCompareDTO.setI18nOdx(this.getFileName(odxDbFilePath));
        packageCompareDTO.setI18nOtx(this.getFileName(otxDbFilePath));
        packageCompareDTO.setMenuFileName(this.getFileName(otxVersionEntity.getMenuFilePath()));
        packageCompareDTO.setPerspectiveFileName(perspectiveFileName);
        packageCompareDTO.setThemeFileName(themeFileName);
        packageCompareDTO.setWorkflowFileName(workflowFileName);
        packageCompareDTO.setTopologyFileName(topologyFileName);

        String key = "package" + Constant.STRIKE + "cache" + Constant.STRIKE + vehicleTypeId;
        redisTemplate.opsForHash().put(key, zipFileName, JSON.toJSONString(packageCompareDTO));
        log.info("res---package");
        return packageCompareDTO;
    }

    @Override
    public void checkReleasePackage(DDiagPackageVersionEntity entity) {
        DVersionInfoEntity odxVersion = versionService.getDVersionInfoById(entity.getOdxVersionId());
        if (!Constant.versionStatus.OPEN.getCode().equals(odxVersion.getStatus())) {
            log.error("ODX is not released!");
            throw new RRException("ODX is not released!");
        }

        DVersionInfoEntity otxVersion = versionService.getDVersionInfoById(entity.getOtxVersionId());
        if (!Constant.versionStatus.OPEN.getCode().equals(otxVersion.getStatus())) {
            log.error("OTX is not released!");
            throw new RRException("OTX is not released!");
        }
    }

    @Override
    public boolean hasNotReleasedPackage(Long vehicleTypeId) {
        LambdaQueryWrapper<DDiagPackageVersionEntity> qw = Wrappers.lambdaQuery();
        qw.eq(DDiagPackageVersionEntity::getVehicleTypeId, vehicleTypeId)
                .eq(DDiagPackageVersionEntity::getStatus, Constant.versionStatus.DEV.getCode())
                .or()
                .eq(DDiagPackageVersionEntity::getStatus, Constant.versionStatus.TEST.getCode())
                .last("limit 1");
        DDiagPackageVersionEntity entity = baseMapper.selectOne(qw);
        return Objects.nonNull(entity);
    }

    private PackageCompareDTO getPackage(Long vehicleTypeId, String zipFileName) {
        String key = "package" + Constant.STRIKE + "cache" + Constant.STRIKE + vehicleTypeId;
        String json = (String) redisTemplate.opsForHash().get(key, zipFileName);
        if (StringUtils.isEmpty(json)) {
            return null;
        }

        File zipFile = new File(packagePath + zipFileName);
        if (!zipFile.exists()) {
            redisTemplate.opsForHash().delete(key, zipFileName);
            return null;
        }
        log.info("getPackage......");
        return JSON.parseObject(json, PackageCompareDTO.class);
    }

    private String getPackageFileName(boolean increment, Long vehicleTypeId, String packageVersion, String targetPackageVersion, String odxVersion, String otxVersion) {
        StringBuilder sb = new StringBuilder();
        if (StringUtils.isEmpty(packageVersion)) {
            sb.append("LATEST").append(Constant.STRIKE)
                    .append(vehicleTypeId).append(Constant.STRIKE)
                    .append(targetPackageVersion).append(Constant.STRIKE)
                    .append("package").append(Constant.ZIP_FILE_PREFIX);
        } else {
            if (increment) {
                sb.append("ADD");
            } else {
                sb.append("FULL");
            }
            sb.append(Constant.STRIKE);
            sb.append(vehicleTypeId).append(Constant.STRIKE);
            sb.append(packageVersion).append(Constant.STRIKE);

            if (StringUtils.isEmpty(odxVersion)) {
                sb.append(0);
            } else {
                sb.append(odxVersion);
            }
            sb.append(Constant.STRIKE);

            if (StringUtils.isEmpty(otxVersion)) {
                sb.append(0);
            } else {
                sb.append(otxVersion);
            }
            sb.append(Constant.STRIKE);

            sb.append(targetPackageVersion).append(Constant.STRIKE)
                    .append("package").append(Constant.ZIP_FILE_PREFIX);
        }
        return sb.toString();
    }

    private String getIncrement(Long vehicleTypeId, DVersionInfoEntity s, DVersionInfoEntity e, String versionType) {
        if (Objects.nonNull(s) && s.getId().equals(e.getId())) {
            return null;
        }

        StringBuilder toPathName = new StringBuilder();
        toPathName.append(versionType).append(Constant.STRIKE)
                .append(vehicleTypeId).append(Constant.STRIKE);
        if (Objects.nonNull(s)) {
            toPathName.append(s.getVersionNum());
        }
        toPathName.append(e.getVersionNum());

        String zipFileDir = versionDownloadPath + toPathName;
        String zipFileName = zipFileDir + Constant.ZIP_FILE_PREFIX;
        File zipFile = new File(zipFileName);
        if (zipFile.exists()) {
            log.info("getIncrement......");
            return zipFileName;
        }

        List<DVersionInfoEntity> l = versionService.list(vehicleTypeId, s, e, versionType);
        if (CollectionUtils.isEmpty(l)) {
            throw new RRException(Constant.Msg.CHECK_INPUT);
        }

        int index = -1;
        for (int i = 0; i < l.size(); i++) {
            if (Objects.equals(Constant.fileType.FULL.getCode(), l.get(i).getFileType())) {
                index = i;
            }
        }

        if (index < 0) {
            index = 0;
        }

        List<String> collect = l.stream().skip(index)
                .map(DVersionInfoEntity::getFilePath).toList();

        for (String path : collect) {
            linuxService.runCommand(Constant.UNZIP_VERSION_COPY_FILE + path + " " + zipFileDir);
        }

        // zip file
        ZipUtil.toZip(zipFileDir, zipFileName,true);

        // del temp file
        boolean del = FileUtil.del(new File(zipFileDir));
        if (!del) {
            log.warn("del temp file failed.");
        }
        return zipFileName;
    }

    private void invalidateCache(Long vehicleTypeId) {
        String key = "package" + Constant.STRIKE + "cache" + Constant.STRIKE + vehicleTypeId;
        //String menuKey = "menu" + Constant.STRIKE + "cache" + Constant.STRIKE + vehicleTypeId;
        redisTemplate.delete(key);
    }

    private void initPackageCompareDTO(PackageCompareDTO packageCompareDTO) {
        packageCompareDTO.setPdxVersion("");
        packageCompareDTO.setPtxVersion("");
        packageCompareDTO.setPdxPackage("");
        packageCompareDTO.setPtxPackage("");
        packageCompareDTO.setI18nOdx("");
        packageCompareDTO.setI18nOtx("");
        //packageCompareDTO.setMenu("menu.xml");
    }

    private String getFileName(String filePath) {
        if (StringUtils.isBlank(filePath)) {
            return null;
        }

        int index = filePath.lastIndexOf("/");
        if (index < 0) {
            return null;
        }

        return filePath.substring(index + 1);
    }

}
