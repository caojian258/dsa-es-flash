package org.dsa.modules.oem.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * @author weishunxin
 * @since 2023-05-30
 */
@Getter
@Setter
public class SyncVehicleInfoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * vin码
     */
    private String vin;

    /**
     * 车型平台
     */
    private String vehiclePlatform;

    /**
     * 品牌
     */
    private String make;

    /**
     * 配置级别
     */
    private String trimLevel;

    /**
     * 生产年份
     */
    private String year;

    /**
     * 车型编码
     */
    private String modelCode;

    /**
     * 下线日期
     */
    private String factoryDate;

    /**
     * 产地
     */
    private String placeOfProductionCode;

    /**
     * 销售区域
     */
    private String regionCode;

    /**
     * 销售城市
     */
    private String cityCode;

    /**
     * ECU列表
     */
    private List<String> ecuList;

}
