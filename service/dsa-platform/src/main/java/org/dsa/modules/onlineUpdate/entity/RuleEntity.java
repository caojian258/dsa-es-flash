package org.dsa.modules.onlineUpdate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.dsa.modules.onlineUpdate.utils.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 规则
 * 
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("o_rule")
public class RuleEntity extends BaseEntity{

	/**
	 * 软件id
	 */
	private Long softwareId;
	/**
	 * 软件版本id
	 */
	private Long softwareVersionId;
	/**
	 * 规则名称
	 */
	private String name;
	/**
	 * 生效开始时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@DateTimeFormat
	private Date startTime;
	/**
	 * 生效结束时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@DateTimeFormat
	private Date endTime;
	/**
	 * 禁用/启用 0正常 1禁用
	 */
	private Integer status;

}
