package org.dsa.modules.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.sys.entity.SysGroupEntity;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 组管理
 *
 */
@Mapper
public interface SysGroupDao extends BaseMapper<SysGroupEntity> {

    //List<SysGroupEntity> queryList(Map<String, Object> params);
	
	List<SysGroupEntity> querySelectList();
	
	/**
	 * 查询转派组
	 * @return
	 */
	List<SysGroupEntity> queryTransferGroupList(@Param("groupCategory") String groupCategory);
	
    /**
     * 查询组所属部门ID
     * @param workshopId  组ID
     */
    List<SysGroupEntity> queryByWorkshopId(Long workshopId);

    /**
     * 通过组名称模糊查询
     * @param groupName
     * @return
     */
    List<SysGroupEntity> queryByName(@Param("groupName") String groupName);
}
