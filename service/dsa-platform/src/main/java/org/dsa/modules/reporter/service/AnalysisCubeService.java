package org.dsa.modules.reporter.service;

import org.dsa.modules.reporter.entity.AnalysisCube;
import org.dsa.modules.reporter.vo.client.ClientChartResVo;
import org.dsa.modules.sys.entity.SysConfigEntity;

import java.util.List;
import java.util.Map;

public interface AnalysisCubeService {

    public String buildSql(AnalysisCube cube, Map<String, Object> where);

    public List<Map<String, Object>> getData(String sql);

    public String formatCondition(AnalysisCube cube, Map<String, Object> mc);

    public AnalysisCube getCube(String table, String dimesion, String visible);

    public AnalysisCube getCubeByCode(String code);

    public ClientChartResVo formatChartData(List<Map<String, Object>> mc, String di,String visible);

}
