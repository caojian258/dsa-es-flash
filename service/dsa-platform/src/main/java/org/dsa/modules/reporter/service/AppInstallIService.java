package org.dsa.modules.reporter.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dsa.modules.reporter.entity.AppInstall;

public interface AppInstallIService extends IService<AppInstall> {

}
