package org.dsa.modules.remoteDiagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("r_pool_depend")
public class RemotePoolDependEntity implements Serializable {


    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long poolId;

    private Long vehicleTypeId;

    private Long vehicleVersionId;

    private Long ecuVersionId;

    private Long tagId;


}
