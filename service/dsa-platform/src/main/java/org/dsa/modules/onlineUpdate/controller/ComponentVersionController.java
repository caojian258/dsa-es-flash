package org.dsa.modules.onlineUpdate.controller;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.annotation.SysLog;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.R;
import org.dsa.modules.onlineUpdate.dto.ComponentVersionDto;
import org.dsa.modules.onlineUpdate.entity.ComponentVersionEntity;
import org.dsa.modules.onlineUpdate.service.ComponentVersionService;
import org.dsa.modules.onlineUpdate.utils.PageUtils;
import org.dsa.modules.onlineUpdate.vo.CascadeOptions;
import org.dsa.modules.onlineUpdate.vo.CompVGroupVo;
import org.dsa.modules.onlineUpdate.vo.ComponentVersionVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * 组件版本
 *
 */
@Slf4j
@RestController
@RequestMapping("/onlineUpdate/componentVersion")
public class ComponentVersionController {
    @Autowired
    private ComponentVersionService componentVersionService;

    /**
     * 列表
     */
    @RequestMapping("/list")
//    @RequiresPermissions("onlineUpdate:componentVersion:list")
    public R list(@RequestBody Map<String, Object> params){
        try {
            PageUtils page = componentVersionService.queryPage(params);
            return R.ok().put("page", page);
        }catch (Exception e){
            log.error("component version list error: "+e.getMessage());
            return  R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }

    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
//    @RequiresPermissions("onlineUpdate:componentVersion:info")
    public R info(@PathVariable("id") Long id){
        try {
            ComponentVersionVo componentVersionVo = componentVersionService.info(id);
            return R.ok().put("componentVersion", componentVersionVo);
        }catch (Exception e){
            log.error("component version info error: "+e.getMessage());
            return  R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    /**
     * 保存
     */
    @SysLog("保存组件版本")
    @RequestMapping("/save")
//    @RequiresPermissions("onlineUpdate:componentVersion:save")
    public R save(@RequestBody ComponentVersionDto componentVersion){
        try {
            componentVersionService.saveOrUpdate2(componentVersion);
            return R.ok();
        }catch (RRException rre){
            log.error("component version save error: "+rre.getMessage());
            return  R.error(rre.getCode(), "操作失败： "+ rre.getMsg());
        }catch (Exception e){
            log.error("component version save error: "+e.getMessage());
            if (e.getMessage().contains("ERROR: duplicate key value violates unique")){
                return  R.error(Constant.Msg.NAME_DUPLICATION_ERROR);
            }
            return  R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }

    }

    /**
     * 修改
     */
    @SysLog("修改组件版本")
    @RequestMapping("/update")
//    @RequiresPermissions("onlineUpdate:componentVersion:update")
    public R update(@RequestBody ComponentVersionDto componentVersion){
        try {
            if (ObjectUtil.isNull(componentVersion.getId())){
                throw new RRException(Constant.Msg.CHECK_ID_ERROR);
            }
            componentVersionService.saveOrUpdate2(componentVersion);
            return R.ok();
        }catch (RRException rre){
            log.error("component version update error: "+rre.getMessage());
            return  R.error(rre.getCode(), "操作失败： "+ rre.getMsg());
        }catch (Exception e){
            log.error("component version update error: "+e.getMessage());
            if (e.getMessage().contains("ERROR: duplicate key value violates unique")){
                return  R.error(Constant.Msg.NAME_DUPLICATION_ERROR);
            }
            return  R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    /**
     * 组件版本树
     */
    @RequestMapping("/versionTree")
    public R versionTree(@RequestBody Map<String, Object> params){
        try {
            Long componentId = MapUtil.getLong(params, "componentId");
            List<CascadeOptions> options = componentVersionService.queryComponentVersionTree(componentId);
            return R.ok().put("options",options);
        }catch (RRException rre){
            log.error("versionTree error: "+rre.getMessage());
            rre.printStackTrace();
            return  R.error(rre.getCode(), "error： "+ rre.getMsg());
        }catch (Exception e){
            log.error("versionTree error: "+e.getMessage());
            e.printStackTrace();
            return  R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    /**
     * 根据版本查询对应的依赖项
     */
    @RequestMapping("/queryRelatedVersion")
    public R queryRelatedVersion(@RequestBody Map<String, Object> params){
        try {
            log.info("queryRelatedVersion params: {}",params);
            Long cVersionId = MapUtil.getLong(params,"cVersionId");
            Map<Long,Long> selected = JSONObject.parseObject(JSON.toJSONString(params.get("selected")), new TypeReference<Map<Long,Long>>() {});
            List<List<Long>> options = componentVersionService.queryRelatedVersion(selected,cVersionId,false);
            return R.ok().put("related",options);
        }catch (RRException rre){
            log.error("queryRelatedVersion error: "+rre.getMessage());
            return  R.error(rre.getCode(), "操作失败： "+ rre.getMsg());
        }catch (Exception e){
            log.error("queryRelatedVersion error: "+e.getMessage());
            return  R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    /**
     * 关联组件排序
     */
    @PostMapping("/sort")
    public R sortComponent(@RequestBody Map<String,Object> param) {
        try {
            List<CompVGroupVo> groups = JSONObject.parseObject(JSON.toJSONString(param.get("groups")), new TypeReference<List<CompVGroupVo>>() {});
            log.info("component groups sort param {}",groups);
            componentVersionService.sortCompVersion(groups);
            return R.ok();
        } catch (RRException rre) {
            log.error("component groups sort error: " + rre.getMessage());
            return R.error(rre.getCode(), "操作失败： " + rre.getMsg());
        } catch (Exception e) {
            log.error("component groups sort error: " + e.getMessage());
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    /**
     * 根据组件id获取所有版本
     * @param cId
     * @return
     */
    @RequestMapping("/listByCompId/{cId}")
    public R listByCompId(@PathVariable("cId") Long cId){
        try {
            LambdaQueryWrapper<ComponentVersionEntity> select = Wrappers.<ComponentVersionEntity>lambdaQuery()
                    .select(ComponentVersionEntity::getVersionName,
                            ComponentVersionEntity::getId,
                            ComponentVersionEntity::getStatus,
                            ComponentVersionEntity::getUrl,
                            ComponentVersionEntity::getCreatedAt)
                    .eq(ComponentVersionEntity::getComponentId, cId)
                    .orderByAsc(ComponentVersionEntity::getVersionNumber);
            List<ComponentVersionEntity> list = componentVersionService.getBaseMapper().selectList(select);
            return R.ok().put("versions", list);
        }catch (Exception e){
            log.error(e.getMessage());
            e.printStackTrace();
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }
    /**
     * 根据组件版本id获取详情，组件版本信息，组件group信息，操作记录
     */
    @RequestMapping("/infoByCompVersionId/{versionId}")
    public R infoByCompVersionId(@PathVariable("versionId") Long versionId){
        try {
            Map<String,Object> info = componentVersionService.queryInfoByCompVersionId(versionId);
            return R.ok().put("info", info);
        }catch (Exception e){
            log.error(e.getMessage());
            e.printStackTrace();
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    /**
     * 版本复制
     */
    @RequestMapping("/copy/{versionId}")
    public R copy(@PathVariable() Long versionId){
        try {
            componentVersionService.copyVersionByVersionId(versionId);
            return R.ok();
        }catch (Exception e){
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

}
