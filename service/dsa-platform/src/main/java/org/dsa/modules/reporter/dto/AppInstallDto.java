package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class AppInstallDto implements Serializable {

    /***
     * 应用名称
     */
    private String applicationName;

    /***
     * 更新的版本
     */
    private String updateVersion;

    /***
     * 安装结果
     */
    private String installationResult;

    /***
     * 安装时间
     */
    private String installationTime;


    /***
     * 安装说明
     */
    private String installationMessage;

    /***
     *
     */
    private Integer updateOid;

    private Integer appId;

    private String type;

}
