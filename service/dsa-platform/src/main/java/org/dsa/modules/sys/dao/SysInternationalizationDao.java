package org.dsa.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.dsa.modules.sys.entity.SysInternationalizationEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SysInternationalizationDao extends BaseMapper<SysInternationalizationEntity> {


    @Select("select key,value,local from  sys_internationalization where key = #{key} ")
    public List<SysInternationalizationEntity> selectByKey(@Param("key") String key);

    @Select("select key,value,local from  sys_internationalization where key = #{key} and local = #{local} ")
    public SysInternationalizationEntity selectByKeyAndLocal(@Param("key") String key, @Param("local") String local);

    @Insert("insert into sys_internationalization(key,value,local) values(#{key},#{value},#{local}) ON conflict(key,local) " +
            "DO UPDATE SET value = EXCLUDED.value ")
    void saveOrUpdate(SysInternationalizationEntity sysInternationalizationEntity);

}
