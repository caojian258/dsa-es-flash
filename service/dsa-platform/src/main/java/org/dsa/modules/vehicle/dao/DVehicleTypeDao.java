package org.dsa.modules.vehicle.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;
import org.dsa.modules.vehicle.entity.DVehicleTypeEntity;

import java.util.List;
import java.util.Map;

/**
 * 车型信息表
 */
@Mapper
public interface DVehicleTypeDao extends BaseMapper<DVehicleTypeEntity> {

    /**
     * 根据level查询orderNum
     */
    Integer selectCountByLevel(int level);

    /**
     * 插入车型信息表
     */
    Long insertDVehicleType(DVehicleTypeEntity dVehicleTypeEntity);

    /**
     * 根据id查询车型信息表及上面节点
     */
    List<DVehicleTypeEntity> selectListById(Long vehicleTypeId);

    /**
     * 查询树数据
     */
    List<DVehicleTypeEntity> selectTreeList();

    /**
     * 车型信息 是否已关联版本信息
     */
    void updateVehicleTypeVersion(@Param("id") Long id, @Param("versionType") String versionType);

    /**
     * 根据车型 获取车型树分支
     */
//    List<DVehicleTypeEntity> selectTreeLineById(Long vehicleTypeId);

    List<DVehicleTypeEntity> queryList(@Param("name") String name);

    List<DVehicleTypeEntity> queryChildrenListByParentId(@Param("parentId") Long parentId);

    Long getVehicleTypeId(@Param("value") String value);

    @Select("select *,i.id as typeId from d_vehicle_type i where i.id = #{typeId} ")
    @Results({
            @Result(property = "groupList", column = "typeId",
                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.RemoteVehicleEcuGroupDao.selectTypeGroups")),
    })
    DVehicleTypeEntity getTypeAndEcuGroups(@Param("typeId") Long typeId);

    List<DVehicleTypeEntity> selectListByIdAsc(Long vehicleTypeId);

    DVehicleTypeEntity selectByValue(@Param("value") String value);

    DVehicleTypeEntity selectByValueAndParentId(@Param("value") String value, @Param("parentId") Long parentId);

    DVehicleTypeEntity getInfo(@Param("id") Long id);

    Integer getMaxOrderNum(@Param("parentId") Long parentId);

    void updateOrder(@Param("parentId") Long parentId);

    List<DVehicleTypeEntity> selectTreeListById(Long vehicleTypeId);

    void updateEcuList(DVehicleTypeEntity entity);

    List<DVehicleTypeEntity> selectListFilterVersion();

    int processSdgList(@Param("list") List<DVehicleTypeEntity> list);

}
