package org.dsa.modules.reporter.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.dsa.modules.reporter.dao.AppInstallMapper;
import org.dsa.modules.reporter.entity.AppInstall;
import org.dsa.modules.reporter.service.AppInstallIService;
import org.springframework.stereotype.Service;

@Service
public class AppInstallIServiceImpl extends ServiceImpl<AppInstallMapper, AppInstall> implements AppInstallIService {
}
