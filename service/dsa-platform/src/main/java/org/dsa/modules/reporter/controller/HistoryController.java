package org.dsa.modules.reporter.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang.ArrayUtils;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.R;
import org.dsa.modules.reporter.constant.ConfigConstant;
import org.dsa.modules.reporter.document.*;
import org.dsa.modules.reporter.dto.ChartItemDto;
import org.dsa.modules.reporter.dto.CtsDailyDto;
import org.dsa.modules.reporter.dto.DtcDailyDto;
import org.dsa.modules.reporter.dto.VehicleReportDto;
import org.dsa.modules.reporter.entity.CtsDaily;
import org.dsa.modules.reporter.entity.DtcDaily;
import org.dsa.modules.reporter.entity.FileLog;
import org.dsa.modules.reporter.enums.FileCateEnum;
import org.dsa.modules.reporter.enums.RecordFlagEnum;
import org.dsa.modules.reporter.service.*;
import org.dsa.modules.reporter.vo.SuggestionVo;
import org.dsa.modules.reporter.vo.VehicleReportVo;
import org.dsa.modules.reporter.vo.client.ClientChartResVo;
import org.dsa.modules.reporter.vo.diag.DiagnosticPageReVo;
import org.dsa.modules.reporter.vo.diag.DtcChartReVo;
import org.dsa.modules.reporter.vo.diag.DtcPageReVo;
import org.dsa.modules.reporter.vo.diag.SessionPageReVo;
import org.dsa.modules.reporter.vo.doc.*;
import org.dsa.modules.sys.controller.AbstractController;
import org.dsa.modules.sys.entity.SysUserEntity;
import org.dsa.modules.sys.service.SysConfigService;
import org.dsa.modules.sys.service.SysUserService;
import org.dsa.modules.sys.service.SysWorkshopsIService;
import org.dsa.modules.vehicle.entity.VehicleInfoEntity;
import org.dsa.modules.vehicle.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.*;

import jakarta.annotation.Resource;
import jakarta.validation.Valid;

import java.text.SimpleDateFormat;
import java.util.*;


@RestController
@RequestMapping("history")
public class HistoryController  extends AbstractController {

    @Resource
    DiagnosticEsService diagnosticEsService;

    @Resource
    VehicleService VehicleService;

    @Resource
    DtcDailyService dtcDailyService;

    @Resource
    CtsDailyService ctsDailyService;

    @Resource
    SysWorkshopsIService sysWorkshopsIService;

    @Autowired
    FileLogService fileLogService;

    @Autowired
    SysConfigService sysConfigService;

    @Autowired
    LocaleService localeService;

    @Autowired
    SysUserService sysUserService;

    private final static Integer SUGGESTION_TOP = 10;

    /***
     * 诊断会话 历史记录 分页 @todo 这里根据 dtc和cts 进行过滤后再根据sessionInfoId查session_info信息
     * @param vo
     * @return
     */
    @RequestMapping(value = "/session/history", method = {RequestMethod.POST })
    public R sessionHistory(@RequestBody SessionPageReVo vo){
        //@todo 前端返回车型的最后一级就可以了
        Map<String, Object> searchMap = null;
        if(StrUtil.isNotBlank(vo.getStartTime()) && StrUtil.isNotBlank(vo.getEndTime())){
            List<String> dates = new ArrayList<>();
            dates.add(vo.getStartTime());
            dates.add(vo.getEndTime());
            vo.setDates(dates);
        }
        if(StrUtil.isNotBlank(vo.getDtcCode())){
            DtcPageReVo dtcPageReVo = new DtcPageReVo();
            BeanUtil.copyProperties(vo,dtcPageReVo);
            List<SessionDocVo> items = new ArrayList<>();
            searchMap =  diagnosticEsService.dtcSessionPage(dtcPageReVo);
            if(null != searchMap.get("ids")){
                List<SessionDoc> sessionDocs =  diagnosticEsService.sessionListById((List<String>) searchMap.get("ids"), dtcPageReVo.getDates());
                if(CollectionUtil.isNotEmpty(sessionDocs)){
                    for (SessionDoc doc: sessionDocs ) {
                        SessionDocVo docVo = new SessionDocVo();
                        BeanUtil.copyProperties(doc, docVo);
                        items.add(docVo);
                    }
                }
            }
            searchMap.put("items", items);

        }else if(StrUtil.isNotBlank(vo.getCtsName())){

            DiagnosticPageReVo ctsPageVo = new DiagnosticPageReVo();
            BeanUtil.copyProperties(vo,ctsPageVo);
            searchMap =  diagnosticEsService.diagnosticSessionPage(ctsPageVo);
            List<SessionDocVo> items = new ArrayList<>();
            if(null != searchMap.get("ids")){
                List<SessionDoc> sessionDocs =  diagnosticEsService.sessionListById((List<String>) searchMap.get("ids"), ctsPageVo.getDates());
                if(CollectionUtil.isNotEmpty(sessionDocs)){
                    for (SessionDoc doc: sessionDocs ) {
                        SessionDocVo docVo = new SessionDocVo();
                        BeanUtil.copyProperties(doc, docVo);
                        items.add(docVo);
                    }
                    searchMap.put("items", items);
                }
            }
        }else {
           searchMap = diagnosticEsService.sessionPage(vo);
            List<SessionDocVo> items = new ArrayList<>();
           if(null != searchMap.get("items") && searchMap.get("items") instanceof ArrayList){
               ((ArrayList<Map<String, Object>>) searchMap.get("items")).forEach(doc -> {
                   SessionDocVo docVo = new SessionDocVo();
                   BeanUtil.fillBeanWithMapIgnoreCase(doc, docVo, true);
                   items.add(docVo);
               });
               searchMap.put("items", items);
           }
        }
        return R.ok().put("data", searchMap);
    }

    @RequestMapping(value = "/session/dtc", method = {RequestMethod.POST })
    public R sessionDtc(@RequestBody DtcPageReVo vo){
        String lang = LocaleContextHolder.getLocale().toLanguageTag();

        if(StrUtil.isNotBlank(vo.getStartTime()) && StrUtil.isNotBlank(vo.getEndTime())){
            List<String> dates = new ArrayList<>();
            dates.add(vo.getStartTime());
            dates.add(vo.getEndTime());
            vo.setDates(dates);
        }

        Map<String, Object> searchMap =  diagnosticEsService.dtcPage(vo);

        if(null != searchMap.get("items")){
            List<Map<String, Object>> items = (List<Map<String, Object>>) searchMap.get("items");
            List<DtcDocVo> records = new ArrayList<>(items.size());
            for (Map<String, Object> map: items) {
                DtcDocVo docVo = BeanUtil.fillBeanWithMapIgnoreCase(map, new DtcDocVo(), false);

                docVo.setDescription(localeService.translateDtcDescription(docVo.getDescriptionTi(),lang, docVo.getDescription()));
                docVo.setErrorMessage(localeService.translateErrorMsg(docVo.getErrorCode(), lang, docVo.getErrorMessage()));
                docVo.setNrcDescriptionTi(localeService.translateDescription(docVo.getNrcDescriptionTi(),lang, docVo.getNrcDescription()));
                docVo.setStatus(localeService.translateDescription(docVo.getStatusTi(), lang, docVo.getStatus()));

                records.add(docVo);
            }
            searchMap.put("items", records);
        }


        return R.ok().put("data", searchMap);
    }

    @RequestMapping(value = "/session/dtcPi", method = {RequestMethod.POST })
    public R dtcPi(@RequestBody DtcChartReVo vo){
        ClientChartResVo resVo = new ClientChartResVo();

        if(StrUtil.isNotBlank(vo.getStartTime()) && StrUtil.isNotBlank(vo.getEndTime())){
            List<String> dates = new ArrayList<>();
            dates.add(vo.getStartTime());
            dates.add(vo.getEndTime());
            vo.setDates(dates);
        }

        List<Map<String, Object>> items = diagnosticEsService.dtcPi(vo);
        logger.info("dtcPi: {}", JSONObject.toJSONString(items));

        Set<String> legendData = new LinkedHashSet<>();
        List<ChartItemDto> dtoList = new ArrayList<>();
        Long itemTotal = 0L;

        for (Map<String, Object> item: items){
                ChartItemDto dto = new ChartItemDto();
                String name = String.valueOf(item.get("key"));
                dto.setType(item.get("key").toString());
                legendData.add(name);
                dto.setName(name);
                itemTotal +=  Long.valueOf(item.get("count").toString());
                dto.setValue(item.get("count"));
                dtoList.add(dto);
        }
        resVo.setItemTotal(itemTotal);
        resVo.setLegendData(legendData);
        resVo.setSeriesData(dtoList);

        return R.ok().put("data", resVo);
    }

    @RequestMapping(value = "/session/dtcBar", method = {RequestMethod.POST })
    public R dtcBar(@RequestBody DtcChartReVo vo){
        if(StrUtil.isNotBlank(vo.getStartTime()) && StrUtil.isNotBlank(vo.getEndTime())){
            List<String> dates = new ArrayList<>();
            dates.add(vo.getStartTime());
            dates.add(vo.getEndTime());
            vo.setDates(dates);
        }
        List<Map<String, Object>> items = diagnosticEsService.dtcMonthBar(vo);

        logger.info("dtcBar:{}", JSONObject.toJSONString(items));

        ClientChartResVo resVo = new ClientChartResVo();
        Set<String> legendData = new LinkedHashSet<>();
        List<ChartItemDto> dtoList = new ArrayList<>();
        Long itemTotal = 0L;

        Set<String> xAxisData = new LinkedHashSet<>();
        Map<String,Map<String, Object>> barStore = new HashMap<>();

        for (Map<String, Object> item: items){
            String name = String.valueOf(item.get("groupKey"));
            if(StrUtil.isNotBlank(name)){
                legendData.add(name);
                xAxisData.add(String.valueOf(item.get("key")));
                Map<String, Object> map = null;
                map = barStore.get(name);
                if(null == map){
                    map = new HashMap<>();
                }
                itemTotal +=  Long.valueOf(item.get("groupCount").toString());
                map.put(String.valueOf(item.get("key")), item.get("groupCount"));
                barStore.put(name, map);
            }
        }

        //这里要notice xAxisData 保证这里面的数据按照顺序填满
        logger.info("bar store:{}", JSONObject.toJSONString(barStore));

        String[] xAxisDatas = new String[xAxisData.size()];
        xAxisData.toArray(xAxisDatas);

        for (String key : barStore.keySet()) {
            ChartItemDto dto = new ChartItemDto();
            dto.setName(key);
            dto.setType("bar");
            dto.setStack("dtc");
            dto.setBarWidth("50%");
            dto.setBarMaxWidth("50%");

            long[] childrenTotal = new long[xAxisData.size()];

            Map<String,Object> bs = barStore.get(key);
            for (String k: bs.keySet()){
                int index = ArrayUtils.indexOf(xAxisDatas, k);
                if(index>-1){
                    childrenTotal[index] = Integer.valueOf(bs.get(k).toString());
                }
            }
            dto.setData(childrenTotal);
            dtoList.add(dto);
        }

        resVo.setXAxisData(xAxisData);
        resVo.setSeriesData(dtoList);
        resVo.setLegendData(legendData);

        return R.ok().put("data", resVo);
    }

    /**
     * @todo DTC 每天数据统计 d_dtc_daily
     * @param vo
     * @return
     */
    @RequestMapping(value = "/session/dtcDaily", method = {RequestMethod.POST })
    public R dtcDaily(@RequestBody DtcChartReVo vo){
        String lang = LocaleContextHolder.getLocale().toLanguageTag();
        if(StrUtil.isNotBlank(vo.getStartTime()) && StrUtil.isNotBlank(vo.getEndTime())){
            List<String> dates = new ArrayList<>();
            dates.add(vo.getStartTime());
            dates.add(vo.getEndTime());
            vo.setDates(dates);
        }
        Page<DtcDailyDto> dtcdailyPage = dtcDailyService.selectPage(vo);
        List<DtcDailyDto> records =  dtcdailyPage.getRecords();
        List<DtcDailyDto> items = new ArrayList<>(records.size());
        if(CollectionUtil.isNotEmpty(records)){
            for (DtcDailyDto dto: records) {
                dto.setDescription(localeService.translateDescription(dto.getTi(), lang, dto.getDescription()));
                items.add(dto);
            }
        }
        dtcdailyPage.setRecords(items);
        logger.info("{}", JSONObject.toJSONString(items));
        return R.ok().put("data", dtcdailyPage);
    }

    /***
     * 诊断操作 数据
     * @param vo
     * @return
     */
    @RequestMapping(value = "/session/diagnostic", method = {RequestMethod.POST})
    public R sessionDiagnostic(@RequestBody DiagnosticPageReVo vo){
        if(StrUtil.isNotBlank(vo.getStartTime()) && StrUtil.isNotBlank(vo.getEndTime())){
            List<String> dates = new ArrayList<>();
            dates.add(vo.getStartTime());
            dates.add(vo.getEndTime());
            vo.setDates(dates);
        }
        Map<String, Object> searchMap =  diagnosticEsService.diagnosticPage(vo);

        return R.ok().put("data", searchMap);
    }

    /***
     * @todo 诊断操作按天统计 d_cts_daily
     * @param vo
     * @return
     */
    @RequestMapping(value = "/session/diagnosticDaily", method = {RequestMethod.POST})
    public R diagnosticDaily(@RequestBody DiagnosticPageReVo vo){
        if(StrUtil.isNotBlank(vo.getStartTime()) && StrUtil.isNotBlank(vo.getEndTime())){
            List<String> dates = new ArrayList<>();
            dates.add(vo.getStartTime());
            dates.add(vo.getEndTime());
            vo.setDates(dates);
        }
        Page<CtsDailyDto> result = ctsDailyService.selectPage(vo);

        logger.info("diagnosticDaily:{}", JSONObject.toJSONString(result));
        return R.ok().put("data", result);
    }

    @RequestMapping(value = "/session/diagnosticPi", method = {RequestMethod.POST})
    public R diagnosticPi(@RequestBody DiagnosticPageReVo vo){
        ClientChartResVo resVo = new ClientChartResVo();

        if(StrUtil.isNotBlank(vo.getStartTime()) && StrUtil.isNotBlank(vo.getEndTime())){
            List<String> dates = new ArrayList<>();
            dates.add(vo.getStartTime());
            dates.add(vo.getEndTime());
            vo.setDates(dates);
        }
        List<Map<String, Object>> items = diagnosticEsService.diagnosticPi(vo);

        logger.info("diagnosticPi:{}", JSONObject.toJSONString(items));

        Set<String> legendData = new LinkedHashSet<>();
        List<ChartItemDto> dtoList = new ArrayList<>();
        Long itemTotal = 0L;

        for (Map<String, Object> item: items){
            ChartItemDto dto = new ChartItemDto();
            String key = String.valueOf(item.get("key"));
            String name = key;
            if(StrUtil.endWith(key, "-")){
                name = key.replace("-","");
            }
            dto.setType(name);
            legendData.add(name);
            dto.setName(name);
            itemTotal +=  Long.valueOf(item.get("count").toString());
            dto.setValue(item.get("count"));
            dtoList.add(dto);
        }
        resVo.setItemTotal(itemTotal);
        resVo.setLegendData(legendData);
        resVo.setSeriesData(dtoList);
        return R.ok().put("data", resVo);
    }

    @RequestMapping(value = "/session/diagnosticBar", method = {RequestMethod.POST})
    public R diagnosticBar(@RequestBody DiagnosticPageReVo vo){
        if(StrUtil.isNotBlank(vo.getStartTime()) && StrUtil.isNotBlank(vo.getEndTime())){
            List<String> dates = new ArrayList<>();
            dates.add(vo.getStartTime());
            dates.add(vo.getEndTime());
            vo.setDates(dates);
        }
        List<Map<String, Object>> items = diagnosticEsService.diagnosticMonthBar(vo);

        logger.info("diagnosticBar:{}", JSONObject.toJSONString(items));

        ClientChartResVo resVo = new ClientChartResVo();
        Set<String> legendData = new LinkedHashSet<>();
        List<ChartItemDto> dtoList = new ArrayList<>();
        Long itemTotal = 0L;

        Set<String> xAxisData = new LinkedHashSet<>();
        Map<String,Map<String, Object>> barStore = new HashMap<>();

        for (Map<String, Object> item: items){
            String key = String.valueOf(item.get("groupKey"));
            String name = key;
            if(StrUtil.endWith(key, "-")){
                name = key.replace("-","");
            }
            if(StrUtil.isNotBlank(name)){
                legendData.add(name);
                xAxisData.add(String.valueOf(item.get("key")));
                Map<String, Object> map = null;
                map = barStore.get(name);
                if(null == map){
                    map = new HashMap<>();
                }
                itemTotal +=  Long.valueOf(item.get("groupCount").toString());
                map.put(String.valueOf(item.get("key")), item.get("groupCount"));
                barStore.put(name, map);
            }
        }

        //这里要notice xAxisData 保证这里面的数据按照顺序填满
        logger.info("cts bar store:{}", JSONObject.toJSONString(barStore));

        String[] xAxisDatas = new String[xAxisData.size()];
        xAxisData.toArray(xAxisDatas);

        for (String key : barStore.keySet()) {
            ChartItemDto dto = new ChartItemDto();
            dto.setName(key);
            dto.setType("bar");
            dto.setStack("cts");
            dto.setBarWidth("50%");
            dto.setBarMaxWidth("50%");

            long[] childrenTotal = new long[xAxisData.size()];

            Map<String,Object> bs = barStore.get(key);
            for (String k: bs.keySet()){
                int index = ArrayUtils.indexOf(xAxisDatas, k);
                if(index>-1){
                    childrenTotal[index] = Integer.valueOf(bs.get(k).toString());
                }
            }
            dto.setData(childrenTotal);
            dtoList.add(dto);
        }

        resVo.setXAxisData(xAxisData);
        resVo.setSeriesData(dtoList);
        resVo.setLegendData(legendData);

        return R.ok().put("data", resVo);
    }

    @RequestMapping(value = "/suggestion", method = {RequestMethod.POST })
    public R suggestion(@RequestBody SuggestionVo vo){
        List<String> suggestionList = new ArrayList<>();

        if(vo.getVin() != null){
            suggestionList =  diagnosticEsService.vinRecommend(vo.getVin(), vo.getEndTime(), SUGGESTION_TOP);
        }else if(vo.getEcu() != null){
            suggestionList = diagnosticEsService.ecuRecommend(vo.getEcu(), vo.getEndTime(), SUGGESTION_TOP);
        }else if(vo.getDtc() != null){
            suggestionList = diagnosticEsService.dtcRecommend(vo.getDtc(), vo.getEndTime(), SUGGESTION_TOP);
        }

        return R.ok().put("data", suggestionList);
    }
    /***
     * 诊断报告 基本信息
     * @param id
     * @return
     */
    @RequestMapping(value = "/session/report", method = {RequestMethod.POST })
    public R sessionReport(@Valid @RequestBody VehicleReportVo id){

        SessionDoc sessionDoc = diagnosticEsService.vehicleReportSession(id);
        logger.info("session: {}", JSONObject.toJSONString(sessionDoc) );
        VehicleInfoEntity vehicleInfo = null;

        VehicleReportDto dto = diagnosticEsService.vehicleReport(id);
        logger.info("Report: {}", dto);

        if(null == dto  || null == dto.getId() ){
            dto = new VehicleReportDto();
            BeanUtil.copyProperties(sessionDoc, dto);
        }else{
            dto.setWorkshopId(sessionDoc.getWorkshopId());
            if(null != sessionDoc.getUserId()){
                dto.setUserId(sessionDoc.getUserId().toString());
            }
            dto.setUserName(sessionDoc.getUserName());
            dto.setDiagCategory(sessionDoc.getDiagCategory());
            dto.setDiagSource(sessionDoc.getDiagSource());
            dto.setStartTime(sessionDoc.getStartTime());
            dto.setEndTime(sessionDoc.getEndTime());
            dto.setSourceVersion(sessionDoc.getSourceVersion());
        }
        if(StrUtil.isNotBlank(dto.getUserName())){
            SysUserEntity sysUserEntity = sysUserService.queryByUserName(dto.getUserName());
            if(null != sysUserEntity){
                dto.setWorkshop(sysUserEntity.getWorkshopName());
                dto.setWorkshopId(sysUserEntity.getWorkshopId());
            }
        }

        if(null != dto.getVin()) {
            try {
                vehicleInfo = VehicleService.getVehicleInfoByVin(dto.getVin());
                logger.info("sessionReport vin: {}", JSONObject.toJSONString(vehicleInfo));
                if(StrUtil.isEmpty(dto.getBrand())){
                    dto.setBrand(vehicleInfo.getMaker());
                }
                if(StrUtil.isEmpty(dto.getModel())){
                    dto.setModel(vehicleInfo.getModel());
                }
                if(StrUtil.isEmpty(dto.getYear())){
                    dto.setYear(vehicleInfo.getYear());
                }
                if(StrUtil.isEmpty(dto.getMaker())){
                    dto.setMaker(vehicleInfo.getMaker());
                }
                if(StrUtil.isEmpty(dto.getTrimLevel())){
                    dto.setTrimLevel(vehicleInfo.getTrimLevel());
                }
                dto.setPlaceOfProduction(vehicleInfo.getPlaceOfProduction());
                dto.setProductionDate(vehicleInfo.getProductionDate());

            }catch (Exception ex){
               logger.error("sessionReport can not find vin: {}", dto.getVin());
            }
        }


        return R.ok().put("data",dto);
    }

    /***
     * 诊断报告 首次DTC
     * @param vo
     * @return
     */
    @RequestMapping(value = "/session/report/firstDtc", method = {RequestMethod.POST })
    public R sessionReportFirstDtc(@RequestBody VehicleReportVo vo){
        String lang = LocaleContextHolder.getLocale().toLanguageTag();
        List<Integer> ids = new ArrayList<>();
        ids.add(RecordFlagEnum.first.getValue());
        ids.add(RecordFlagEnum.both.getValue());
        List<DtcDocVo> voList = new ArrayList<>();
        if(null == vo.getEndTime()){
            vo.setEndTime(vo.getStartTime());
        }
        List<DtcDoc> dtcs = diagnosticEsService.sessionDtcForVehicleReport(vo.getSessionInfoId(), vo.getEndTime(), ids);
        for (DtcDoc doc: dtcs ) {
            DtcDocVo vos = new DtcDocVo();
            doc.setDescription(localeService.translateDtcDescription(doc.getDescriptionTi(), lang, doc.getDescription()));
            doc.setNrcDescription(localeService.translateDescription(doc.getNrcDescriptionTi(), lang, doc.getNrcDescription()));
            doc.setStatus(localeService.translateDescription(doc.getStatusTi(), lang, doc.getStatus()));
            BeanUtil.copyProperties(doc, vos);
            voList.add(vos);
        }

        List<DtcSnapshotDoc> dtcSnapshotDocList = diagnosticEsService.sessionDtcSnapshotVehicleReport(vo.getSessionInfoId(), vo.getEndTime());
        List<DtcExtendedDoc> dtcExtendedDocList = diagnosticEsService.sessionDtcExtendedVehicleReport(vo.getSessionInfoId(), vo.getEndTime());

        List<DtcDocVo> result = new ArrayList<>(voList.size());

        for (DtcDocVo dtcVo: voList ) {
            DtcDocVo resultVo = dtcVo;
            List<DtcSnapshotDocVo> snapshotDocVos = new ArrayList<>();
            if(CollectionUtil.isNotEmpty(dtcSnapshotDocList)){
                for (DtcSnapshotDoc dtcSnapshot: dtcSnapshotDocList) {
                    String dtc = dtcSnapshot.getDtcCode();
                    String ecu = dtcSnapshot.getEcuName();
                    if(dtcVo.getDtcCode().equals(dtc) && dtcVo.getEcuName().equals(ecu)){
                        DtcSnapshotDocVo snapshotDocVo = new DtcSnapshotDocVo();
                        dtcSnapshot.setName(localeService.translateDescription(dtcSnapshot.getNameTi(), lang, dtcSnapshot.getName()));
                        dtcSnapshot.setValue(localeService.translateDescription(dtcSnapshot.getValueTi(), lang, dtcSnapshot.getValue()));
                        dtcSnapshot.setUnit(localeService.translateDescription(dtcSnapshot.getUnitTi(), lang, dtcSnapshot.getUnit()));
                        dtcSnapshot.setNrcDescription(localeService.translateDescription(dtcSnapshot.getNrcDescriptionTi(), lang, dtcSnapshot.getNrcDescription()));
                        dtcSnapshot.setErrorMessage(localeService.translateErrorMsg(dtcSnapshot.getErrorCode(), lang, dtcSnapshot.getErrorMessage()));

                        BeanUtil.copyProperties(dtcSnapshot, snapshotDocVo);
                        snapshotDocVos.add(snapshotDocVo);
                    }
                }

            }
            resultVo.setDtcSnapshotList(snapshotDocVos);
            List<DtcExtendDataDocVo> extendDocVos = new ArrayList<>();
            if(CollectionUtil.isNotEmpty(dtcExtendedDocList)){
                for (DtcExtendedDoc dtcExtend: dtcExtendedDocList) {
                    String dtc = dtcExtend.getDtcCode();
                    String ecu = dtcExtend.getEcuName();
                    if(dtcVo.getDtcCode().equals(dtc) && dtcVo.getEcuName().equals(ecu)){
                        DtcExtendDataDocVo extendDocVo = new DtcExtendDataDocVo();
                        dtcExtend.setName(localeService.translateDescription(dtcExtend.getNameTi(), lang, dtcExtend.getName()));
                        dtcExtend.setValue(localeService.translateDescription(dtcExtend.getValueTi(), lang, dtcExtend.getValue()));
                        dtcExtend.setUnit(localeService.translateDescription(dtcExtend.getUnitTi(), lang, dtcExtend.getUnit()));
                        dtcExtend.setNrcDescription(localeService.translateDescription(dtcExtend.getNrcDescriptionTi(), lang, dtcExtend.getNrcDescription()));
                        dtcExtend.setErrorMessage(localeService.translateErrorMsg(dtcExtend.getErrorCode(), lang, dtcExtend.getErrorMessage()));

                        BeanUtil.copyProperties(dtcExtend, extendDocVo);
                        extendDocVos.add(extendDocVo);
                    }
                }
            }
            resultVo.setDtcExtendDataList(extendDocVos);
            result.add(resultVo);
        }

        return R.ok().put("data", result);
    }

    /***
     * 诊断报告 最后一次DTC
     * @param vo
     * @return
     */
    @RequestMapping(value = "/session/report/lastDtc", method = {RequestMethod.POST })
    public R sessionReportLastDtc(@RequestBody VehicleReportVo vo){
        List<Integer> ids = new ArrayList<>();
        String lang = LocaleContextHolder.getLocale().toLanguageTag();
        if(null == vo.getEndTime()){
            vo.setEndTime(vo.getStartTime());
        }
        ids.add(RecordFlagEnum.last.getValue());
        ids.add(RecordFlagEnum.both.getValue());
        List<DtcDocVo> voList = new ArrayList<>();

        List<DtcDoc> dtcs = diagnosticEsService.sessionDtcForVehicleReport(vo.getSessionInfoId(), vo.getEndTime(), ids);

        for (DtcDoc doc: dtcs ) {
            DtcDocVo vos = new DtcDocVo();
            doc.setDescription(localeService.translateDtcDescription(doc.getDescriptionTi(), lang, doc.getDescription()));
            doc.setNrcDescription(localeService.translateDescription(doc.getNrcDescriptionTi(), lang, doc.getNrcDescription()));
            doc.setStatus(localeService.translateDescription(doc.getStatusTi(), lang, doc.getStatus()));
            doc.setErrorMessage(localeService.translateErrorMsg(doc.getErrorCode(), lang, doc.getErrorMessage()));

            BeanUtil.copyProperties(doc, vos);
            voList.add(vos);
        }

        List<DtcSnapshotDoc> dtcSnapshotDocList = diagnosticEsService.sessionDtcSnapshotVehicleReport(vo.getSessionInfoId(), vo.getEndTime());
        List<DtcExtendedDoc> dtcExtendedDocList = diagnosticEsService.sessionDtcExtendedVehicleReport(vo.getSessionInfoId(), vo.getEndTime());

        List<DtcDocVo> result = new ArrayList<>(voList.size());

        for (DtcDocVo dtcVo: voList ) {
            DtcDocVo resultVo = dtcVo;
            List<DtcSnapshotDocVo> snapshotDocVos = new ArrayList<>();
            if(CollectionUtil.isNotEmpty(dtcSnapshotDocList)){
                for (DtcSnapshotDoc dtcSnapshot: dtcSnapshotDocList) {
                    String dtc = dtcSnapshot.getDtcCode();
                    String ecu = dtcSnapshot.getEcuName();
                    if(dtcVo.getDtcCode().equals(dtc) && dtcVo.getEcuName().equals(ecu)){
                        DtcSnapshotDocVo snapshotDocVo = new DtcSnapshotDocVo();
                        dtcSnapshot.setName(localeService.translateDescription(dtcSnapshot.getNameTi(), lang, dtcSnapshot.getName()));
                        dtcSnapshot.setValue(localeService.translateDescription(dtcSnapshot.getValueTi(), lang, dtcSnapshot.getValue()));
                        dtcSnapshot.setUnit(localeService.translateDescription(dtcSnapshot.getUnitTi(), lang, dtcSnapshot.getUnit()));
                        dtcSnapshot.setNrcDescription(localeService.translateDescription(dtcSnapshot.getNrcDescriptionTi(), lang, dtcSnapshot.getNrcDescription()));
                        dtcSnapshot.setErrorMessage(localeService.translateErrorMsg(dtcSnapshot.getErrorCode(), lang, dtcSnapshot.getErrorMessage()));
                        BeanUtil.copyProperties(dtcSnapshot, snapshotDocVo);
                        snapshotDocVos.add(snapshotDocVo);
                    }
                }

            }
            resultVo.setDtcSnapshotList(snapshotDocVos);
            List<DtcExtendDataDocVo> extendDocVos = new ArrayList<>();
            if(CollectionUtil.isNotEmpty(dtcExtendedDocList)){
                for (DtcExtendedDoc dtcExtend: dtcExtendedDocList) {
                    String dtc = dtcExtend.getDtcCode();
                    String ecu = dtcExtend.getEcuName();
                    if(dtcVo.getDtcCode().equals(dtc) && dtcVo.getEcuName().equals(ecu)){
                        DtcExtendDataDocVo extendDocVo = new DtcExtendDataDocVo();
                        dtcExtend.setName(localeService.translateDescription(dtcExtend.getNameTi(), lang, dtcExtend.getName()));
                        dtcExtend.setValue(localeService.translateDescription(dtcExtend.getValueTi(), lang, dtcExtend.getValue()));
                        dtcExtend.setUnit(localeService.translateDescription(dtcExtend.getUnitTi(), lang, dtcExtend.getUnit()));
                        dtcExtend.setNrcDescription(localeService.translateDescription(dtcExtend.getNrcDescriptionTi(), lang, dtcExtend.getNrcDescription()));
                        dtcExtend.setErrorMessage(localeService.translateErrorMsg(dtcExtend.getErrorCode(), lang, dtcExtend.getErrorMessage()));
                        BeanUtil.copyProperties(dtcExtend, extendDocVo);
                        extendDocVos.add(extendDocVo);
                    }
                }
            }
            resultVo.setDtcExtendDataList(extendDocVos);
            result.add(resultVo);
        }

        return R.ok().put("data", result);
    }

    /***
     * 诊断报告 第一次DID
     * @param vo
     * @return
     */
    @RequestMapping(value = "/session/report/firstDid", method = {RequestMethod.POST })
    public R sessionReportFirstDid(@RequestBody VehicleReportVo vo){
        String lang = LocaleContextHolder.getLocale().toLanguageTag();
        if(null == vo.getEndTime()){
            vo.setEndTime(vo.getStartTime());
        }
        List<Integer> ids = new ArrayList<>();
        ids.add(RecordFlagEnum.first.getValue());
        ids.add(RecordFlagEnum.both.getValue());
        List<DidDocVo> voList = new ArrayList<>();
        List<DidDoc> didDocs =  diagnosticEsService.sessionDidForVehicleReport(vo.getSessionInfoId(), vo.getEndTime(), ids);
        for (DidDoc did: didDocs){
            DidDocVo vos = new DidDocVo();
            did.setDidName(localeService.translateDescription(did.getDidNameTi(), lang, did.getDidName()));
            did.setValue(localeService.translateDescription(did.getValueTi(), lang, did.getValue()));
            did.setParameterName(localeService.translateDescription(did.getParameterNameTi(), lang, did.getParameterName()));
            did.setNrcDescription(localeService.translateDescription(did.getNrcDescriptionTi(), lang, did.getNrcDescription()));
            did.setErrorMessage(localeService.translateErrorMsg(did.getErrorCode(), lang, did.getErrorMessage()));

            BeanUtil.copyProperties(did, vos);
            voList.add(vos);
        }
        return R.ok().put("data", voList);
    }

    /***
     * 诊断报告 最后一次DID
     * @param vo
     * @return
     */
    @RequestMapping(value = "/session/report/lastDid", method = {RequestMethod.POST })
    public R sessionReportLastDid(@RequestBody VehicleReportVo vo){
        List<Integer> ids = new ArrayList<>();
        ids.add(RecordFlagEnum.last.getValue());
        ids.add(RecordFlagEnum.both.getValue());
        List<DidDocVo> voList = new ArrayList<>();
        if(null == vo.getEndTime()){
            vo.setEndTime(vo.getStartTime());
        }
        List<DidDoc> didDocs =  diagnosticEsService.sessionDidForVehicleReport(vo.getSessionInfoId(), vo.getEndTime(), ids);
        String lang = LocaleContextHolder.getLocale().toLanguageTag();

        for (DidDoc did: didDocs){
            DidDocVo vos = new DidDocVo();
            did.setDidName(localeService.translateDescription(did.getDidNameTi(), lang, did.getDidName()));
            did.setValue(localeService.translateDescription(did.getValueTi(), lang, did.getValue()));
            did.setParameterName(localeService.translateDescription(did.getParameterNameTi(), lang, did.getParameterName()));
            did.setNrcDescription(localeService.translateDescription(did.getNrcDescriptionTi(), lang, did.getNrcDescription()));
            did.setErrorMessage(localeService.translateErrorMsg(did.getErrorCode(), lang, did.getErrorMessage()));

            BeanUtil.copyProperties(did, vos);
            voList.add(vos);
        }
        return R.ok().put("data", voList);
    }

    /***
     * 诊断报告 ECU在线情况
     * @param vo
     * @return
     */
    @RequestMapping(value = "/session/report/ecu", method = {RequestMethod.POST })
    public R sessionReportEcu(@RequestBody VehicleReportVo vo){
        if(null == vo.getEndTime()){
            vo.setEndTime(vo.getStartTime());
        }
        List<DiagnosticEcuDoc> ecuDocList =  diagnosticEsService.sessionEcuForVehicleReport(vo.getSessionInfoId(), vo.getEndTime());
        List<DiagnosticEcuDocVo> voList = new ArrayList<>();
        for (DiagnosticEcuDoc doc: ecuDocList ) {
            DiagnosticEcuDocVo vos = new DiagnosticEcuDocVo();
            BeanUtil.copyProperties(doc, vos);
            voList.add(vos);
        }

        return R.ok().put("data", voList);
    }

    /***
     * 诊断报告 诊断操作
     * @param vo
     * @return
     */
    @RequestMapping(value = "/session/report/diagnostic", method = {RequestMethod.POST })
    public R sessionReportDiagnostic(@RequestBody VehicleReportVo vo){
        String lang = LocaleContextHolder.getLocale().toLanguageTag();
        if(null == vo.getEndTime()){
            vo.setEndTime(vo.getStartTime());
        }
        List<DiagnosticDoc> diagDocList = diagnosticEsService.sessionDiagnosticVehicleReport(vo.getSessionInfoId(), vo.getEndTime());
        List<DiagnosticDocVo> voList = new ArrayList<>();

        for (DiagnosticDoc doc: diagDocList ) {
            DiagnosticDocVo vos = new DiagnosticDocVo();

            doc.setNrcDescription(localeService.translateDescription(doc.getNrcDescriptionTi(), lang, doc.getNrcDescription()));

            if(doc.getErrorMessage().contains("|") && doc.getErrorMessage().contains("{") && doc.getErrorMessage().contains("}")){
                doc.setErrorMessage(localeService.multipleTiTranslate(doc.getErrorMessage(), lang));
            }else{
                doc.setErrorMessage(localeService.translateErrorMsg(doc.getErrorCode(), lang, doc.getErrorMessage()));
            }

            String sfn = localeService.multipleTiTranslate(doc.getSpecialFunctionName(), lang);
            doc.setSpecialFunctionName(sfn);

            BeanUtil.copyProperties(doc, vos);
            String ti = "";
            if(StrUtil.isNotBlank(vos.getFunctionCode()) && StrUtil.isNotBlank(vos.getAction()) ){
                ti = vos.getFunctionCode()+"_"+vos.getAction();
            }else if(StrUtil.isNotBlank(vos.getFunctionCode()) ){
                ti = vos.getFunctionCode();
            }
            String ctsName = localeService.translateDescription(ti, lang, ti);
            vos.setCtsName(ctsName);
            voList.add(vos);
        }

        return R.ok().put("data", voList);
    }

    @RequestMapping("/getEcus")
    public R getEcus(){
        List<String> ecuList = new ArrayList<>();

//        String ecus = sysConfigService.getValue(ConfigConstant.ECU_SELECT);
//        if(StrUtil.isNotBlank(ecus)){
//            ecuList =  Arrays.stream(ecus.split(",")).toList();
//        }
        ecuList = dtcDailyService.findAllEcu();
        return R.ok().put("data", ecuList);
    }

    /***
     * 校验文件是否存在
     * @param sessionId
     * @return
     */
    @RequestMapping("/session/checkFile/{sessionId}")
    public R sessionCheckFile(@PathVariable String sessionId){
        FileLog fileLog = fileLogService.checkFile(FileCateEnum.SQLITE.getValue(), sessionId);
        if(null == fileLog){
            return R.error(Constant.Msg.FILE_NOT_EXIST.getCode(), Constant.Msg.FILE_NOT_EXIST.getMessage());
        }
        Map<String, String> map = new HashMap<>();
        map.put("logPath", fileLog.getFilePath());
        map.put("logName", fileLog.getFileName());
        return R.ok().put("data", map);
    }

    @RequestMapping("/session/deleteSession")
    public R deleteSessionData(@RequestBody @Valid CheckSessionVo vo){
        String sessionId = vo.getSessionId();
        String day = vo.getDay();

        SessionDoc doc = diagnosticEsService.querySessionBySessionId(sessionId, day);


        if(null != doc && null != doc.getCreateTime()){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            day = sdf.format(doc.getCreateTime());
            diagnosticEsService.deleteSessionDoc(doc.getId(), day);

            Long vehId = 0L;
            if(CollectionUtil.isNotEmpty(doc.getVehicleTypeId())){
                Object typeIdObject = doc.getVehicleTypeId().get(doc.getVehicleTypeId().size()-1);
                vehId = typeIdObject instanceof Integer == true ? Long.valueOf(typeIdObject.toString()): (Long) typeIdObject;
            }
            String vin = doc.getVin();
            List<DtcDaily> dtcDailyList =  diagnosticEsService.dtcDailyByVinDay(vin, day);
            if(CollectionUtil.isNotEmpty(dtcDailyList)){
                for (DtcDaily dtc: dtcDailyList){
                    dtcDailyService.upsertData(dtc);
                }
            }else{
                dtcDailyService.deleteData(vin, day);
            }

            List<CtsDaily> ctsDailyList =  diagnosticEsService.ctsDailyByVinDay(vin, day);
            if(CollectionUtil.isNotEmpty(ctsDailyList)){
                for (CtsDaily cts: ctsDailyList){
                    cts.setVehicleTypeId(vehId);
                    ctsDailyService.upsertData(cts);
                }
            }else{
                ctsDailyService.deleteData(vin, day);
            }
        }
        doc.setVehicleTypeId(null);
        return R.ok().put("data", doc);
    }
}
