package org.dsa.modules.reporter.dto;

import lombok.Data;

@Data
public class WorkshopUserGroupTotalDto {

    String groupName;

    String parentGroupName;

    Long groupId;

    Long parentId;

    Integer value;

}
