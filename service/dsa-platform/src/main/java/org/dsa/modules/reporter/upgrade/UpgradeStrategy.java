package org.dsa.modules.reporter.upgrade;

import org.dsa.modules.reporter.entity.AppHistory;
import org.dsa.modules.reporter.entity.ClientUpgrade;

public interface UpgradeStrategy {

    public void save(ClientUpgrade log);

    public void saveHis(AppHistory data);
}
