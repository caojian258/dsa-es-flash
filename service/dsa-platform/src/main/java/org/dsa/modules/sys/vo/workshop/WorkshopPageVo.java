package org.dsa.modules.sys.vo.workshop;

import lombok.Data;
import org.dsa.modules.sys.vo.PageParam;

@Data
public class WorkshopPageVo extends PageParam {

    private String region;

    private String name;

    private Long province;

    private Long city;

    private String code;

    private Integer delFlag;
}
