-- 事件附件表
CREATE TABLE `t_event_attach` (
	`id` bigint NOT NULL AUTO_INCREMENT,
	`event_no` varchar(30) COMMENT '事件号',
	`type` varchar(50) COMMENT '类型',
	`attach_name` varchar(100) COMMENT '附件名',
	`attach_url` varchar(200) COMMENT '附件路径',
	`created_time` datetime COMMENT '创建时间',
	`created_by` varchar(50) COMMENT '创建人',
	`modify_by` varchar(50) COMMENT '修改人',
	`modify_time` datetime COMMENT '修改时间',
	`is_delete`  varchar(6) DEFAULT 0 COMMENT '是否删除(0:正常,1：删除)',
	 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='事件附件表';



-- 附件表
CREATE TABLE `t_problem_attach` (
	`id` bigint NOT NULL AUTO_INCREMENT,
	`event_no` varchar(30) COMMENT '事件号',
	`type` varchar(50) COMMENT '类型',
	`attach_name` varchar(100) COMMENT '附件名',
	`attach_url` varchar(200) COMMENT '附件路径',
	`created_time` datetime COMMENT '创建时间',
	`created_by` varchar(50) COMMENT '创建人',
	`modify_by` varchar(50) COMMENT '修改人',
	`modify_time` datetime COMMENT '修改时间',
	`is_delete`  varchar(6) DEFAULT 0 COMMENT '是否删除(0:正常,1：删除)',
	 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='问题附件表';


-- 问题表
CREATE TABLE `t_problem` (
	`id` bigint NOT NULL AUTO_INCREMENT,
	`event_no` varchar(30) COMMENT '事件号',
	`contact_name` varchar(50) COMMENT '联系人姓名',
  	`contact_no` varchar(50) COMMENT '联系人员工号',
  	`contact_landline` varchar(50) COMMENT '联系人座机',
  	`contact_telphone` varchar(50) COMMENT '联系人手机',
  	`organ_name` varchar(50) COMMENT '机构名称',
  	`dep_name` varchar(50) COMMENT '部门名称',
	`description` text COMMENT '问题描述',
	`acceptor` varchar(50) COMMENT '受理人',
	`opinions` text COMMENT '审批意见',
	`approver` text COMMENT '审批人',
	`problem_status` varchar(30) COMMENT '问题状态',
	`created_time` datetime COMMENT '创建时间',
	`created_by` varchar(50) COMMENT '创建人',
	`modify_by` varchar(50) COMMENT '修改人',
	`modify_time` datetime COMMENT '修改时间',
	`is_delete`  varchar(6) DEFAULT 0 COMMENT '是否删除(0:正常,1：删除)',
	 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='问题表';

-- 问题解决表
CREATE TABLE `t_problem_solve` (
	`id` bigint NOT NULL AUTO_INCREMENT,
	`event_no` varchar(30) COMMENT '事件号',
	`solution_category` varchar(50) COMMENT '解决方法分类',
	`problem_system` varchar(50) COMMENT '问题系统',
	`comments` text COMMENT '方案说明',
	`created_time` datetime COMMENT '创建时间',
	`created_by` varchar(50) COMMENT '创建人',
	`modify_by` varchar(50) COMMENT '修改人',
	`modify_time` datetime COMMENT '修改时间',
	`is_delete`  varchar(6) DEFAULT 0 COMMENT '是否删除(0:正常,1：删除)',
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='问题解决表';


 INSERT INTO eap_fast.sys_dict
 (name, `type`, code, value, order_num, remark, del_flag)
 VALUES('问题状态', 'problem_status', 'waiting_approve', '待审批', 1, NULL, 0);
 
  INSERT INTO eap_fast.sys_dict
 (name, `type`, code, value, order_num, remark, del_flag)
 VALUES('问题状态', 'problem_status', 'rejected', '已驳回', 2, NULL, 0);
 
 INSERT INTO eap_fast.sys_dict
 (name, `type`, code, value, order_num, remark, del_flag)
 VALUES('问题状态', 'problem_status', 'waiting_solve', '待处理', 3, NULL, 0);
 
  INSERT INTO eap_fast.sys_dict
 (name, `type`, code, value, order_num, remark, del_flag)
 VALUES('问题状态', 'problem_status', 'done', '完成', 4, NULL, 0);
 
-- alter table sys_group add dept_name varchar(50) comment '部门名称';
 
 
 
 
 
 