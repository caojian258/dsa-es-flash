package org.dsa.modules.reporter.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class BaseEntity implements Serializable {

    protected Date createdAt;

    protected Date updatedAt;
}
