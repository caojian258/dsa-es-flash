package org.dsa.modules.reporter.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dsa.modules.reporter.entity.AppOverview;

public interface AppOverviewIService extends IService<AppOverview> {
}
