package org.dsa.modules.sys.vo.monitor;

import lombok.Data;

@Data
public class RedisInfoVo {
    private String item;
    private Object value;
}
