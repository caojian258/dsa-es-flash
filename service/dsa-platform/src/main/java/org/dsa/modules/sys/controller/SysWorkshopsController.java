package org.dsa.modules.sys.controller;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.R;
import org.dsa.modules.sys.dto.WorkshopImportMsgDto;
import org.dsa.modules.sys.entity.FileEntity;
import org.dsa.modules.sys.entity.SysWorkshopsEntity;
import org.dsa.modules.sys.service.SysUserService;
import org.dsa.modules.sys.service.SysWorkshopsIService;
import org.dsa.modules.sys.vo.workshop.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.*;

@RestController
@RequestMapping("/sys/workshops")
public class SysWorkshopsController extends AbstractController{

    @Resource
    private SysUserService sysUserService;

    @Resource
    private SysWorkshopsIService sysWorkshopsIService;

    @Value("${file.base.path}")
    private String uploadPath;

    @RequestMapping("/page")
    public R page(@RequestBody WorkshopPageVo vo){

        Page<SysWorkshopsEntity> page =  sysWorkshopsIService.queryPage(vo);
        return R.ok().put("data", page);
    }

    @RequestMapping("/select")
    public R select(@RequestBody WorkshopClientVo vo){
        List<SysWorkshopsEntity> workshopList = new ArrayList<>();
        vo.setStatus(0);
        workshopList =  sysWorkshopsIService.selectWorkshop(vo);
        return R.ok().put("data", workshopList);
    }

    @RequestMapping("/info/{workshopId}")
    public R info(@PathVariable("workshopId") Long workshopId){
        SysWorkshopsEntity workshop = sysWorkshopsIService.findById(workshopId);
        return R.ok().put("workshop", workshop);
    }

    @RequestMapping("/save")
    public R save(@RequestBody WorkshopVo workshop){
        if(StrUtil.isBlank(workshop.getName())){
            return R.error(Constant.Msg.CHECK_NAME_ERROR.getCode(), Constant.Msg.CHECK_NAME_ERROR.getMessage());
        }

        if(CollectionUtil.isEmpty(workshop.getLocation())){
            return R.error(Constant.Msg.WORKSHOP_LOCATION_MISS.getMessage(), Constant.Msg.WORKSHOP_LOCATION_MISS.getMessage());
        }

        sysWorkshopsIService.saveWorkshop(workshop);
        return R.ok();
    }

    @RequestMapping("/update")
    public R update(@RequestBody WorkshopVo workshop){
        sysWorkshopsIService.saveWorkshop(workshop);
        return R.ok();
    }

    @RequestMapping("/delete")
    public R delete(@RequestBody WorkshopAuditVo vo){

        if(vo.getStatus().equals(1)){
            sysWorkshopsIService.disabledById(vo.getWorkshopId());
        }else{
            sysWorkshopsIService.enabledById(vo.getWorkshopId());
        }

        return R.ok();
    }

    @RequestMapping("/getName/{workshopId}")
    public R queryWorkshopNameById(@PathVariable("workshopId") Long workshopId) {
        return R.ok().put("workshopName", sysWorkshopsIService.queryWorkshopNameById(workshopId) );
    }

    @RequestMapping("/checkCode")
    public R checkCode(@RequestBody WorkshopCodeValidatorVo vo){
        return R.ok().put("flag", sysWorkshopsIService.checkWorkshopCode(vo));
    }

    @RequestMapping("/checkName")
    public R checkName(@RequestBody WorkshopNameValidatorVo vo){
        SysWorkshopsEntity entity =  sysWorkshopsIService.findByName(vo.getName());
        boolean flag = false;
        if(null != entity && !entity.getId().equals(vo.getWorkshopId())){
            flag = true;
        }
        return R.ok().put("flag", flag);
    }


    @PostMapping("/workshops_excel_import")
    public R workshopsExcelImport(@RequestParam("file") MultipartFile mFile) {
        WorkshopImportMsgDto msg = null;
        try {
            //MultipartFile 转化为file easyexcel接受file类型的文件

            File file = new File(uploadPath+Objects.requireNonNull(mFile.getOriginalFilename()));
            if (!file.getName().endsWith(".xlsx")) {
                return R.error("请导入excel类型的文件");
            }
            FileUtils.copyInputStreamToFile(mFile.getInputStream(), file);
            msg =  sysWorkshopsIService.importFromExcel(file.getAbsolutePath());

            if (file.exists()) {
                file.delete();
            }
        }catch (RRException re){
            logger.error("excel导入出错123：" + re);
            return R.error(Constant.Msg.EXCEL_IMPORT_ERROR.getCode(),re.getMsg());
        }catch (Exception e) {
            logger.error("excel导入出错：" + e);
            return R.error(Constant.Msg.EXCEL_IMPORT_ERROR);
        }

        if(null != msg){
            if( msg.getTotal() == msg.getFailedTotal() ){
                return R.error(msg.getMsg());
            }else{
                return R.ok(msg.getMsg());
            }
        }

        return R.ok();
    }

    @PostMapping("/workshops_excel_export")
    public R workshops_excel_export(@RequestBody WorkshopClientVo vo){
        Map<String, String> map = sysWorkshopsIService.exportToExcel(vo);

        return R.ok().put("data", map);
    }

    @RequestMapping(value = "/workshops_excel_template")
    public R workshops_excel_template(){
        Map<String, String> map = sysWorkshopsIService.excelTemplate();
        return R.ok().put("data", map);
    }

    @RequestMapping(value="/download")
    public void download(@RequestBody FileEntity fileEntity, HttpServletRequest req, HttpServletResponse response){
        File file = new File(fileEntity.getUrl());
        if (!file.exists()) {
            throw new RRException(Constant.Msg.FILE_NOT_EXIST);
        }

        InputStream ips = null;
        OutputStream ops = null;

        try {

            ips =  new FileInputStream(file);
            ops = response.getOutputStream();
            response.setContentType("application/x-download");
            response.setHeader("Content-Disposition", "attachment;filename=utf-8'zh_cn'" + URLEncoder.encode(fileEntity.getName(), "UTF-8"));
            IOUtils.copy(ips, ops);
            ips.close();
            ops.flush();
        } catch (IOException e) {
            logger.error("下载文件出错："+e);
        } finally {
            try {
                if(ips != null ) {
                    ips.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if(ops != null) {
                    ops.flush();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    @RequestMapping(value="/region")
    public R queryRegions(){
        List<String> regions = new ArrayList<>();
        regions.add("东区");
        regions.add("南区");
        regions.add("西区");
        regions.add("北区");
        regions.add("N/A");
        return R.ok().put("data", regions);
    }
}
