package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.util.Map;

@Data
public class BasicDiagnosticDto {

    private Map<String,Map<String, String>> vehicleIdentification;

    private Map<String,Map<String, String>> dtcs;

}
