package org.dsa.modules.reporter.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.reporter.dto.AppOptionDto;
import org.dsa.modules.reporter.dto.ClientDetailDto;
import org.dsa.modules.reporter.entity.*;
import org.dsa.modules.reporter.vo.ChartReVo;
import org.dsa.modules.reporter.vo.client.*;

import java.util.List;
import java.util.Map;

public interface ClientService {

    public Page<AppInstall> selectInstallPage(AppInstallPageReVo vo);

    /***
     * 客户端应用列表
     * @param vo
     * @return
     */
    public Page<AppOverview> selectAppPage(AppPageReVo vo);

    /***
     * 客户端列表
     * @param vo
     * @return
     */
    public Page<SystemOverview> selectPage(ClientPageReVo vo);

    /***
     * 客户端详情
     * @param vo
     * @return
     */
    public ClientDetailDto detail(ClientDetailReVo vo);

    /***
     * 客户端协议列表
     * @param vo
     * @return
     */
    public Page<SystemProtocolItem> protocolItemPage(ProtocolItemPageReVo vo);

    /***
     * 客户端软件历史记录
     * @param vo
     * @return
     */
    public Page<AppVersion> appVersionHistoryPage(AppVersionPageReVo vo);

    public Page<SystemHistory> clientHistoryPage(ClientHisPageReVo vo);
    /***
     * 客户端最近一次的升级记录
     * @param vo
     * @return
     */
    public List<AppInstallResVo> appInstallList(ClientDetailReVo vo);


    /***
     * 客户端软件版本 安装日期分组
     * @param vo
     * @return
     */
    public List<AppVersionResVo> appVersionDateGroup(AppVersionPageReVo vo);


    public List<String> applicationList();


    Page<AppVersion> selectAppHisPage(AppPageReVo vo);

    Map<String, Object> getInputs();

    List<String> getAllAnivirHistory();

    public List<AppOptionDto> queryAppOption();

    public List<Map<String, Object>> clientWorkshopStatics(ChartReVo vo);
}
