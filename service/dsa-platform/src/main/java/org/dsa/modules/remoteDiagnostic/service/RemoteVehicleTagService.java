package org.dsa.modules.remoteDiagnostic.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleTagEntity;
import org.dsa.modules.remoteDiagnostic.vo.CheckNameReqVo;
import org.dsa.modules.remoteDiagnostic.vo.VehiclePlatformPageReVo;
import org.dsa.modules.remoteDiagnostic.vo.VehicleTag.VehicleTagReqVo;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * ${comments}
 *
 */
public interface RemoteVehicleTagService {

    Page<RemoteVehicleTagEntity> selectPage(VehiclePlatformPageReVo vo);

    RemoteVehicleTagEntity getInfo(Long id);

    void saveTag(RemoteVehicleTagEntity tag);

    void updateTag(RemoteVehicleTagEntity tag);

    void deleteTag(Long id);

    List<RemoteVehicleTagEntity> getList(Boolean flag);

    void updateVehicleTag(VehicleTagReqVo vo);

    // 检查平台是否重复
    Boolean check(CheckNameReqVo vo);

    List<Long> getVehicleTagInfo(String vin);

    Map<String,String> templateExcelExport();

    void readExcel(File file);

    List<RemoteVehicleTagEntity> getVehicleTagList(String vin);
}

