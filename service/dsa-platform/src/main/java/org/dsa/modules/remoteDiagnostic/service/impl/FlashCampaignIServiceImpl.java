package org.dsa.modules.remoteDiagnostic.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.dsa.common.utils.AESUtils;
import org.dsa.config.SyncConfig;
import org.dsa.modules.remoteDiagnostic.constant.FlashConstant;
import org.dsa.modules.remoteDiagnostic.dao.FlashCampaignDao;
import org.dsa.modules.remoteDiagnostic.dao.FlashCampaignEcuDao;
import org.dsa.modules.remoteDiagnostic.entity.FlashCampaignEcuEntity;
import org.dsa.modules.remoteDiagnostic.entity.FlashCampaignEntity;
import org.dsa.modules.remoteDiagnostic.service.FlashCampaignIService;
import org.dsa.modules.remoteDiagnostic.vo.Flash.FlashCampaignPageVo;
import org.dsa.modules.remoteDiagnostic.vo.Flash.FlashCampaignResVo;
import org.dsa.modules.reporter.util.FilesUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service("flashCampaignIService")
public class FlashCampaignIServiceImpl extends ServiceImpl<FlashCampaignDao, FlashCampaignEntity> implements FlashCampaignIService {

    @Value("${file.ecu.custom}")
    private String ecuFlashCustomPath;

    @Value("${file.ecu.download}")
    private String ecuFlashDownload;

    @Value("${file.ecu.flash}")
    private String ecuFlashPath;

    @Value("${project.url}")
    private String domainUrl;

    @Autowired
    private SyncConfig syncConfig;

    @Autowired
    private FlashCampaignEcuDao flashCampaignEcuDao;


    @Override
    public FlashCampaignEntity queryCampaign(String vin, Long versionId) {
        QueryWrapper<FlashCampaignEntity> qw = new QueryWrapper<>();
        qw.eq("vin", vin);
        qw.eq("vehicle_version_id", versionId);
        return baseMapper.selectOne(qw);
    }

    @Override
    public void disabledById(Long id, Long userId) {
        FlashCampaignEntity entity = baseMapper.selectById(id);

    }

    @Override
    public void enabledById(Long id, Long userId) {
        FlashCampaignEntity entity = baseMapper.selectById(id);

    }

    @Override
    public Page<FlashCampaignEntity> queryPage(FlashCampaignPageVo vo) {

        Page<FlashCampaignEntity> page = new Page<>(vo.getPageNo(), vo.getPageSize());
        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);

        LambdaQueryWrapper<FlashCampaignEntity> qw = Wrappers.<FlashCampaignEntity>lambdaQuery();

        if(StrUtil.isNotBlank(vo.getVin())){
            qw.eq(FlashCampaignEntity::getVin, vo.getVin());
        }

        if(StrUtil.isNotBlank(vo.getVersion())){
            qw.apply("vehicle_version" + " ilike {0}", "%" + vo.getVersion().trim() + "%");
        }
        if(null != vo.getUserId()){
            qw.eq(FlashCampaignEntity::getUserId, vo.getUserId());
        }

        Page<FlashCampaignEntity> result =  baseMapper.selectPage(page, qw);

        return result;
    }

    @Override
    public void saveCampaign(FlashCampaignEntity entity) {
        if(null != entity.getId()){
            entity.setUpdatedAt(new Date());
            baseMapper.updateById(entity);
        }else{
            baseMapper.insert(entity);
        }
    }

    @Override
    public void saveEcuVersion(Long campaignId, Long ecuId, Long ecuVersionId) {
        FlashCampaignEcuEntity entity = new FlashCampaignEcuEntity();
        entity.setCampaignId(campaignId);
        entity.setEcuId(ecuId);
        entity.setEcuVersionId(ecuVersionId);
        entity.setCreatedAt(new Date());
        entity.setUpdatedAt(new Date());
        flashCampaignEcuDao.insert(entity);
    }

    @Override
    public FlashCampaignEcuEntity queryEcuVersion(Long campaignId, Long ecuId, Long ecuVersionId) {
        QueryWrapper<FlashCampaignEcuEntity> qw = new QueryWrapper<>();
        qw.eq("campaign_id", campaignId);
        qw.eq("ecu_id", ecuId);
        qw.eq("ecu_version_id", ecuVersionId);
        return flashCampaignEcuDao.selectOne(qw);
    }

    @Override
    public String upload(MultipartFile file) throws IOException {
        String dateFormat = syncConfig.getDirFormat();
        SimpleDateFormat format = new SimpleDateFormat(dateFormat);
        String dayDir = format.format(new Date());

        File dir = new File(ecuFlashCustomPath + File.separator + dayDir);
        if (!dir.exists()) {
            FilesUtil.mkdir(dir);
        }

        File destFile = new File(dir.getAbsoluteFile()+ File.separator + file.getOriginalFilename());

        FileUtils.copyInputStreamToFile(file.getInputStream(), destFile);

        return destFile.getPath();
    }

    @Override
    public String buildDownloadUrl(String path) {
        String param = AESUtils.encrypt(path, AESUtils.AES_KEY);
        if(null == param){
            return "";
        }

        try {
            return domainUrl+FlashConstant.DOWNLOAD_PATH+"?params="+ URLEncoder.encode(param, StandardCharsets.UTF_8.name());
        }catch (Exception ex){
            log.error("buildDownloadUrl {} error: {}", path, ex);
            return "";
        }

    }

    @Override
    public Page<FlashCampaignResVo> queryPageForClient(FlashCampaignPageVo vo) {
        Page<FlashCampaignResVo> page = new Page<>(vo.getPageNo(), vo.getPageSize());
        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        Map<String, String> sortColumns = new HashMap<>(2);
        sortColumns.put("time", "updated_at");
        sortColumns.put("version", "vehicle_version");
        if(null != sortColumns.get(column)){
            column = sortColumns.get(column);
        }else{
            column = "updated_at";
        }
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);

        Page<FlashCampaignResVo> result =  baseMapper.selectPageForClient(page, vo.getVin(), vo.getVersion());

        return result;
    }

}
