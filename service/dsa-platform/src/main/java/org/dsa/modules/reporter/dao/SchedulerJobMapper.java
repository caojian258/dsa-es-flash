package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.entity.ScheduleJob;

@Mapper
public interface SchedulerJobMapper  extends BaseMapper<ScheduleJob> {

    public ScheduleJob findByJobName(@Param("jobName") String jobName);
}
