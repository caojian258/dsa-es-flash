package org.dsa.modules.reporter.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum FlashEnum {

    ONLINE("ONLINE_FLASH"),
    REMOTE("REMOTE_RESCUE"),;

    @EnumValue
    private final String value;
}
