package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.entity.AppHistory;
import org.dsa.modules.reporter.entity.AppVersion;
import org.dsa.modules.reporter.vo.client.AppVersionResVo;


import java.util.List;

@Mapper
public interface AppVersionMapper extends BaseMapper<AppVersion> {

    public List<AppVersion> getGroupList(@Param("pcid") String pcid, @Param("type") String type ,
                                         @Param("startDate") String startDate, @Param("endDate") String endDate);


    public List<AppVersionResVo> getListGroupByDate(@Param("workshop") String workshop, @Param("pcid") String pcid, @Param("type") String type ,
                                                    @Param("startDate") String startDate, @Param("endDate") String endDate,
                                                    @Param("offset") Integer offset, @Param("limit") Integer limit);

    public List<AppVersion> selectComponentVersion(@Param("appId") Integer appId, @Param("versionId") Integer versionId);

    public AppVersion queryDeviceAppVersion(@Param("pcid") String pcid, @Param("appId") Integer appId, @Param("version") String version, @Param("tag") String tag);
}
