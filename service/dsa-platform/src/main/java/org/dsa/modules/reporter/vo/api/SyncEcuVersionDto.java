package org.dsa.modules.reporter.vo.api;

import lombok.Data;

@Data
public class SyncEcuVersionDto {
    //ecu名称
    private String name;
    //刷写结果 1 成功 2失败 3未刷写
    private Integer result;
    //原因
    private String reason;

    private String bootVersion; //要升级的版本

    private String hardVersion; //要升级的版本

    private String partCode;//要升级的版本

    private String softwareVersion;//要升级的版本

    private String partNo; //要升级的版本

}
