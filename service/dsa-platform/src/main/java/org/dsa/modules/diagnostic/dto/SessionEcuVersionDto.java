package org.dsa.modules.diagnostic.dto;

import lombok.Data;

@Data
public class SessionEcuVersionDto {

    String sessionId;

    String vin;

    String pcid;

    String workshop;

    String ecuName;

    //多语言KEY
    String ti;
    //版本名称
    String name;
    //版本值
    String value;
}
