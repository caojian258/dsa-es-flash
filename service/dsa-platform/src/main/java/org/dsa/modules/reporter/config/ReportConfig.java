package org.dsa.modules.reporter.config;


import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Data
@Configuration
@ConfigurationProperties(prefix = "report")
public class ReportConfig{

    @Value("${report.dir.format}")
    private String dirFormat;

    @Value("${report.dir.upload}")
    private String uploadDir;

    @Value("${report.dir.report}")
    private String reportDir;

    @Value("${report.dir.work}")
    private String workDir;

    @Value("${report.dir.archive}")
    private String archiveDir;

    @Value("${report.dir.workshop}")
    private String workshopDir;

    @Value("${report.dir.workshop_user}")
    private String workshopUserDir;

    @Value("${report.dir.fault}")
    private String faultDir;

//    @Value("${report.dir.asmaintained}")
    private String asmaintainedDir;


    private List<CronJobConfig> jobs;

    @Value("${report.client.hardware}")
    private String clientHardware;

    @Value("${report.client.software}")
    private String clientSoftware;

    @Value("${report.client.workshop}")
    private String clientWorkshop;

    @Value("${report.sqlcipher.windows}")
    private String sqlcipherWindowsScript;

    @Value("${report.sqlcipher.linux}")
    private String sqlcipherLinuxScript;
}
