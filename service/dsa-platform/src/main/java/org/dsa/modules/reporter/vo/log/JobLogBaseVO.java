package org.dsa.modules.reporter.vo.log;

import lombok.Data;
import jakarta.validation.constraints.NotNull;
import java.util.Date;


/**
* 定时任务日志 Base VO，提供给添加、修改、详细的子 VO 使用
* 如果子 VO 存在差异的字段，请不要添加到这里，影响 Swagger 文档生成
*/
@Data
public class JobLogBaseVO {

    @NotNull(message = "任务编号不能为空")
    private Long jobId;

    @NotNull(message = "处理器的名字不能为空")
    private String handlerName;

    private String handlerParam;

    @NotNull(message = "第几次执行不能为空")
    private Integer executeIndex;

    @NotNull(message = "开始执行时间不能为空")
    private Date beginTime;

    private Date endTime;

    private Integer duration;

    @NotNull(message = "任务状态不能为空")
    private Integer status;

    private String result;

}
