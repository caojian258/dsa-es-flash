package org.dsa.modules.reporter.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "offboard")
public class OffBoardConfig {

    @Value("${offboard.systemId}")
    String systemId;

    @Value("${offboard.secret}")
    String secret;

    @Value("${offboard.signCheck}")
    Boolean signCheck;

    @Value("${offboard.syncDomain}")
    String syncDomain;

    @Value("${offboard.syncUrl}")
    String syncUrl;

    @Value("${offboard.accessKey}")
    String accessKey;

    @Value("${offboard.secretKey}")
    String secretKey;

    @Value("${offboard.uploadUrl}")
    String uploadUrl;
}
