package org.dsa.modules.reporter.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.dsa.modules.reporter.dao.EcuVersionHistoryMapper;
import org.dsa.modules.reporter.dao.EcuVersionOverviewMapper;
import org.dsa.modules.reporter.dto.EcuVersionHistoryDto;
import org.dsa.modules.reporter.entity.EcuVersionHistory;
import org.dsa.modules.reporter.entity.EcuVersionOverview;
import org.dsa.modules.reporter.enums.VersionSourceEnum;
import org.dsa.modules.reporter.service.EcuVersionService;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;


@Slf4j
@Service
public class EcuVersionServiceImpl implements EcuVersionService {

    @Resource
    EcuVersionOverviewMapper overviewMapper;

    @Resource
    EcuVersionHistoryMapper historyMapper;


    @Override
    public void addHistory(EcuVersionHistoryDto dto) {
        EcuVersionHistory entity = new EcuVersionHistory();
        entity.setEcuName(dto.getEcuName());
        entity.setVin(dto.getVin());
        entity.setCollectTime(dto.getCollectTime());
        entity.setVehicleVersion(dto.getVehicleVersion());
        entity.setVersionName(dto.getVersionName());
        entity.setVersionValue(dto.getVersionValue());
        entity.setSource(VersionSourceEnum.AsMaintained.getValue());
        entity.setTag(dto.getTag());
        if(null != dto.getSessionId()){
            entity.setSessionId(dto.getSessionId());
        }
        historyMapper.insert(entity);
    }

    @Override
    public void updateOverview(EcuVersionHistoryDto dto) {
        EcuVersionOverview overview = null;

        overview = overviewMapper.checkEcuVersionOverview(dto.getVin(), dto.getEcuName(), dto.getVersionName());
        if(null == overview){
            overview = new EcuVersionOverview();
            overview.setEcuName(dto.getEcuName());
            overview.setVin(dto.getVin());
            overview.setCollectTime(dto.getCollectTime());
            overview.setVehicleVersion(dto.getVehicleVersion());
            overview.setVersionName(dto.getVersionName());
            overview.setVersionValue(dto.getVersionValue());
            overview.setSource(VersionSourceEnum.AsMaintained.getValue());
            overview.setTag(dto.getTag());
            if(null != dto.getSessionId()){
                overview.setSessionId(dto.getSessionId());
            }
            overviewMapper.insert(overview);
        }else{
            if(!overview.getVersionValue().equals(dto.getVersionValue())){
                overview.setVersionValue(dto.getVersionValue());
                overview.setVehicleVersion(dto.getVehicleVersion());
                overview.setSource(VersionSourceEnum.AsMaintained.getValue());
                overview.setTag(dto.getTag());
                overview.setCollectTime(dto.getCollectTime());
                if(null != dto.getSessionId()){
                    overview.setSessionId(dto.getSessionId());
                }
                overviewMapper.updateById(overview);
            }
        }
    }
}
