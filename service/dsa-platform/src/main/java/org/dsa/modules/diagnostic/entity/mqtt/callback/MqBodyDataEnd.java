package org.dsa.modules.diagnostic.entity.mqtt.callback;

import lombok.Data;

import java.io.Serializable;

/**
 * mq body data Begin
 *
 */
@Data
public class MqBodyDataEnd implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 状态码 0 成功 1 失败
     */
    private int status;
    /**
     * message 消息提示
     */
    private String message;
    /**
     * data
     */
    private Object data;

}
