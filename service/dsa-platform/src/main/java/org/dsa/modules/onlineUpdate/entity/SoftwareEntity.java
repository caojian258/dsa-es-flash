package org.dsa.modules.onlineUpdate.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.dsa.modules.onlineUpdate.utils.BaseEntity;

/**
 * 软件基础数据表
 * 
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("o_software")
public class SoftwareEntity extends BaseEntity {

	/**
	 * 软件编码
	 */
	private Long code;
	/**
	 * 软件名称
	 */
	private String name;
	/**
	 * 启用/禁用;0启用；1禁用
	 */
	private Integer status;
	/**
	 * 描述ti;描述字段多语言key
	 */
	private String descriptionTi;
	
	@TableField(exist = false)
	private Boolean isRelease;


}
