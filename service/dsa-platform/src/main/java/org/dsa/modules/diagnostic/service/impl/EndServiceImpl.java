package org.dsa.modules.diagnostic.service.impl;

import ch.qos.logback.classic.Logger;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.diagnostic.dao.DActionInfoDao;
import org.dsa.modules.diagnostic.dao.DSessionInfoDao;
import org.dsa.modules.diagnostic.entity.VehicleActiveEntity;
import org.dsa.modules.diagnostic.entity.mqtt.callback.MqBodyDataEnd;
import org.dsa.modules.diagnostic.entity.mqtt.callback.MqEntity;
import org.dsa.modules.diagnostic.service.DiagnosticMqService;
import org.dsa.modules.diagnostic.util.DiagnosticUtils;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service("endService")
public class EndServiceImpl implements DiagnosticMqService {
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass());

    @Resource
    private RedisUtils redisUtils;
    @Resource
    private DSessionInfoDao dSessionInfoDao;
    @Resource
    private DActionInfoDao dActionInfoDao;
    @Resource
    private DiagnosticUtils diagnosticUtils;

    @Override
    public void onMessage(MqEntity mqEntity) {
        logger.info("End Service Impl MQ Call back ---- begin");
        logger.info("End Service Impl MQ Call back ---- sessionId:{}",mqEntity.getHeader().getRid());


        //判断车辆信息是有缓存   key H_sessionId
        if(redisUtils.hasKey(Constant.REDIS_ACTION + mqEntity.getHeader().getRid())){
            //接收到车辆健康检查信息回调
            logger.info("End Service Impl MQ Call back Redis key:" + Constant.REDIS_ACTION + mqEntity.getHeader().getRid());
            logger.info("End Service Impl MQ Call back Redis value:" + redisUtils.get(Constant.REDIS_ACTION + mqEntity.getHeader().getRid()));
            String vin = redisUtils.get(Constant.REDIS_ACTION+ mqEntity.getHeader().getRid());
            if (redisUtils.hasKey(Constant.REDIS_VIN + vin)) {
                VehicleActiveEntity v = redisUtils.get(Constant.REDIS_VIN + vin, VehicleActiveEntity.class);
                logger.info("End Service Impl MQ Call back ---- Redis key:" + Constant.REDIS_VIN + vin);
                logger.info("End Service Impl MQ Call back ---- Redis value:" + JSONObject.toJSONString(v));
                actionResultUpdate(mqEntity,v);
            }else {
                //诊断车辆 非诊断中状态
                logger.info("End Service Impl MQ Call ---- redis value vehicle status not peoces, select db inster and update data");
                actionResultUpdate(mqEntity,null);
            }
        }else{
            //断开连接，清除经销商关联诊断车辆
            logger.info("End Service Impl MQ Call  ---- action is null ------- ");
            actionResultUpdate(mqEntity,null);
        }

        //更新诊断session结束时间,和结束状态
        dSessionInfoDao.updateSessionEndTimeAndEndStatusByActionId(mqEntity.getHeader().getRid(),Constant.SESSION_END_STATUS.INITIATIVE_TO_END.getCode());

        logger.info("End Service Impl MQ Call back ---- end");
    }
    /**
    *诊断缓存 清除及修改  db数据修改
    */
    private void actionResultUpdate(MqEntity mqEntity,VehicleActiveEntity v) {
        logger.info("End Service Impl MQ Call ---- update status by actionId:{}", mqEntity.getHeader().getRid());
        //命令执行成功
        if((Constant.ACK.FIN.getValue() & mqEntity.getHeader().getAck()) != 0 ) {
            MqBodyDataEnd mqBodyDataEnd = ((JSONObject)mqEntity.getBody().getData()).toJavaObject(MqBodyDataEnd.class);
            //诊断执行成功
            if(mqBodyDataEnd.getStatus() == 0) {
                if(v==null){
                    //修改db诊断执行状态
                    dActionInfoDao.updateByActionId(mqEntity.getHeader().getRid(), String.valueOf(Constant.RESULTS.SUCCESS.getValue()),1);
                    return;
                }
                //修改db诊断执行状态
                dActionInfoDao.updateByActionId(mqEntity.getHeader().getRid(), String.valueOf(Constant.RESULTS.SUCCESS.getValue()),0);

                //删除redis 诊断key
                logger.info("End Service Impl MQ Call ---- delete redis actionId:{}", Constant.REDIS_ACTION + mqEntity.getHeader().getRid());
                logger.info("End Service Impl MQ Call ---- delete redis run vin:{}", Constant.REDIS_RUN_VIN + v.getVin());
                logger.info("End Service Impl MQ Call ---- delete redis vin:{}", Constant.REDIS_VIN + v.getVin());
                redisUtils.delete(Constant.REDIS_ACTION + mqEntity.getHeader().getRid());
                redisUtils.delete(Constant.REDIS_RUN_VIN + v.getVin());
                redisUtils.delete(Constant.REDIS_VIN + v.getVin());

                //缓存执行结果-> 用于页面显示，key actionId ,value status
                redisUtils.set(Constant.REDIS_ACTION_STATUS + mqEntity.getHeader().getRid(), Constant.RESULTS.SUCCESS.getValue(), Constant.Function.END.getExpire());

                //设置 车辆检测列表
                List vehicleActiveEntityList = redisUtils.get(Constant.REDIS_LIST + v.getWorkShopName(), (new ArrayList()).getClass());
                if (vehicleActiveEntityList != null && vehicleActiveEntityList.size() > 0) {
                    vehicleActiveEntityList.removeIf(versionActiveEntity -> (versionActiveEntity.toString().indexOf(v.getVin()) > 0));
                }
                if (StringUtils.isNotBlank(v.getWorkShopName())){
                    redisUtils.set(Constant.REDIS_LIST + v.getWorkShopName(), vehicleActiveEntityList);
                }
                return;
            }
        }
        //修改db诊断执行状态
        dActionInfoDao.updateByActionId(mqEntity.getHeader().getRid(), String.valueOf(Constant.RESULTS.FAIL.getValue()),1);
        redisUtils.set(Constant.REDIS_ACTION_STATUS + mqEntity.getHeader().getRid(), Constant.RESULTS.FAIL.getValue(),Constant.Function.END.getExpire());
    }


}