package org.dsa.modules.reporter.dto;

import lombok.Data;

@Data
public class FlashVehicleStatsDto {

    private String vin;

    private String collectDate;

    private String name;

    private String value;
}
