package org.dsa.modules.remoteDiagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskLogEntity;

import java.util.List;

@Mapper
public interface DiagnosticTaskLogDao extends BaseMapper<DiagnosticTaskLogEntity> {

    @Select("select * from r_diag_task_log i where i.session_id = #{sessionId} order by i.id desc")
    List<DiagnosticTaskLogEntity> getListBySession(String sessionId);
}
