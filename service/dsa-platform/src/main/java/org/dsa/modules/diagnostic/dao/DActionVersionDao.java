package org.dsa.modules.diagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.diagnostic.dto.SessionEcuVersionDto;
import org.dsa.modules.diagnostic.entity.DActionVersionEntity;

import java.util.List;

/**
 * 读取版本标识表
 */
@Mapper
public interface DActionVersionDao extends BaseMapper<DActionVersionEntity>{

    List<DActionVersionEntity> selectListBySessionId(@Param("sessionId") String sessionId
            , @Param("functionCode") String functionCode,@Param("actionCode")  String actionCode);

    List<DActionVersionEntity> selectListBySessionId2(@Param("sessionId") String sessionId);

    List<SessionEcuVersionDto> selectSessionEcuVersion(@Param("sessionId") String sessionId);
}

