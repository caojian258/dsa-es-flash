package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.dto.DtcDailyDto;
import org.dsa.modules.reporter.entity.DtcDaily;

import java.util.List;

@Mapper
public interface DtcDailyMapper extends BaseMapper<DtcDaily> {

    Page<DtcDailyDto> selectPageByDate(Page<DtcDaily> page,
                                       @Param("vin") String vin,
                                       @Param("ecuNames") List<String> ecuNames,
                                       @Param("startTime") String startTime,
                                       @Param("endTime") String endTime,
                                       @Param("vehType") List<Long> vehType
                                       );

    public void oneInsert(@Param("entity") DtcDaily entity);

    public void oneUpsert(@Param("entity") DtcDaily entity);

    public void deleteData(@Param("vin") String vin, @Param("day") String day);

    public List<String> findAllEcu();
}
