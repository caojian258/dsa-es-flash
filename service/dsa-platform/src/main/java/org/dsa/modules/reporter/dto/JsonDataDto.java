package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class JsonDataDto implements Serializable {

    /**
     * 离线结构
     */
    DIDDto didData;
    List<DTCDto> dtcData;



}
