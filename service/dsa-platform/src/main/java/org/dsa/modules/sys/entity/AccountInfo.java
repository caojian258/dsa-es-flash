package org.dsa.modules.sys.entity;

import lombok.Data;
import java.io.Serializable;


/**
 * 用户帐号信息
 */
@Data
public class AccountInfo implements Serializable {


    protected String id;//9f2ec079-7d0f-4fe2-90ab-8b09a8302aba",
    protected String userCode;
    protected String account;//zhangsan",
    protected String realName;//张三",
    protected String nickName;//张三",
    protected String enName;//英文名
    protected String headIcon;// null,
    protected String gender;// 1,
    protected String birthday;//1989-06-09 15:00:00",
    protected String mobilePhone;//150140*****",
    protected String userPassword;// null,
    protected String email;//cd***@gmail.com",
    protected String weChat;//zhangsan",
    protected String country;// null,
    protected String province;// null,
    protected String city;// null,
    protected String district;// null,
    protected String isMember;// null,
    protected String memberGradeId;// null,
    protected String referralUserId;// null,
    protected String managerId;//1",
    protected String securityLevel;// 1,
    protected String signature;// null,
    protected String organizeId;//201806131523019338",
    protected String organizeName;// null,
    protected String roleId;//F0A2B36F-35A7-4660-B46C-D4AB796591EB",
    protected String roleName;// null
    protected String dutyId;//B2624F67-E092-461A-AAAD-13592A9429D9",
    protected String dutyName;// null,
    protected String isAdministrator;// true,
    protected String sortCode;// 1,
    protected String description;//",
    protected String deleteMark;// false,
    protected String enabledMark;// true,
    protected String creatorTime;// null,
    protected String creatorUserId;// null,
    protected String lastModifyTime;//2020-09-21 13:44:03",
    protected String lastModifyUserId;//9f2ec079-7d0f-4fe2-90ab-8b09a8302aba",
    protected String deleteTime;//2018-04-22 12:01:11",
    protected String deleteUserId;//d4dc3a3ecb884a2fa446afdc2552f20d"
}
