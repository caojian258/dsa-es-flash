package org.dsa.modules.oss.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.dsa.modules.oss.entity.SysFileEntity;

@Mapper
public interface SysFileDao extends BaseMapper<SysFileEntity> {
}
