package org.dsa.modules.onlineUpdate.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dsa.common.utils.PageUtils;
import org.dsa.modules.onlineUpdate.entity.SoftwareEntity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ${comments}
 *
 */
public interface SoftwareService extends IService<SoftwareEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveOrUpdate2(SoftwareEntity entity);

    void disabledById(Integer id);

    List<HashMap> queryAll();
}

