package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.dto.VehicleModelDto;
import org.dsa.modules.reporter.entity.VehicleType;

import java.util.List;

@Mapper
public interface VehicleTypeMapper extends BaseMapper<VehicleType> {

    VehicleType getOneByModel(@Param("model") String model);

    List<VehicleType> allRecords();

    List<VehicleModelDto> allModel(@Param("locale") String locale);
}
