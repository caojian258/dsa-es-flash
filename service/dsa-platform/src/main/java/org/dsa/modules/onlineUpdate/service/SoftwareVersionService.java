package org.dsa.modules.onlineUpdate.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dsa.common.utils.PageUtils;
import org.dsa.modules.onlineUpdate.dto.SoftwareVersionDto;
import org.dsa.modules.onlineUpdate.entity.SoftwareVersionEntity;
import org.dsa.modules.onlineUpdate.po.SoftwareVersionPo;
import org.dsa.modules.onlineUpdate.vo.SoftwareVersionVo;

import java.util.List;
import java.util.Map;

/**
 * SoftwareVersion
 *
 */
public interface SoftwareVersionService extends IService<SoftwareVersionEntity> {

    PageUtils queryPage(Map<String, Object> params);
    void saveOrUpdate2(SoftwareVersionDto entity);
    SoftwareVersionVo info(Integer id);
    List<SoftwareVersionPo.ComponentInfo> getRelatedCompVersion(Long softwareVersionId, String locale, Long oldSoftwareVersion);

    Map<String,Object> querySoftwareEntry(Long softwareId);

    SoftwareVersionEntity getOldestVersionBySoftwareId(Long id);

    Map<String, Object> queryInfoBySoftwareVersionId(Long versionId);

    void copyVersionByVersionId(Long versionId);
}

