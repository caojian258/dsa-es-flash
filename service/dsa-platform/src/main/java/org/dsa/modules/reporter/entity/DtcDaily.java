package org.dsa.modules.reporter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/***
 * ECU DTC按天统计
 */
@Data
@Accessors(chain = true)
@TableName("d_dtc_daily")
public class DtcDaily extends BaseEntity{

    @TableId(type = IdType.AUTO)
    private Long id;

    /***
     * 车架号
     */
    private String vin;

    /**
     * 车型
     */
    private Long vehicleTypeId;

    /**
     * ECU
     */
    private String ecuName;

    /***
     * description TI
     */
    private String ti;

    /***
     * description
     */
    private String description;

    /**
     * 故障码
     */
    private String dtcCode;

    /**
     * 故障码
     */
    private String dtcCodeInt;

    /**
     * 故障码CodeHex
     */
    private String dtcCodeHex;

    /***
     * 日期
     */
    private String dtcDay;

    /***
     * 诊断次数
     */
    private Long dtcNum;

}
