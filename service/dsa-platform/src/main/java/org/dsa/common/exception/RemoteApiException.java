package org.dsa.common.exception;

import org.dsa.common.utils.Constant;

/**
 * 自定义异常
 *
 */
public class RemoteApiException extends RuntimeException {
	private static final long serialVersionUID = 1L;

    private String msg;
    private int code = 500;
	private Object data;

    public RemoteApiException(String msg) {
		super(msg);
		this.msg = msg;
	}

	public RemoteApiException(String msg, Throwable e) {
		super(msg, e);
		this.msg = msg;
	}

	public RemoteApiException(String msg, int code) {
		super(msg);
		this.msg = msg;
		this.code = code;
	}

	public RemoteApiException(String msg, int code, Object data) {
		super(msg);
		this.msg = msg;
		this.code = code;
		this.data = data;
	}

	public RemoteApiException(Constant.Msg msg) {
		super(msg.getMessage());
		this.msg = msg.getMessage();
		this.code = msg.getCode();
	}

	public RemoteApiException(String msg, int code, Throwable e) {
		super(msg, e);
		this.msg = msg;
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
