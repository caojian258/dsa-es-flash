package org.dsa.modules.reporter.vo.workshop;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.modules.reporter.pojo.PageParam;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class WorkshopUserPageReVo extends PageParam {

    private String id;

    private String name;

    private List<String> group;

    private String workshop;

    private Boolean active;
}
