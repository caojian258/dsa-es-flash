package org.dsa.modules.remoteDiagnostic.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.apache.commons.io.FileUtils;
import org.dsa.common.utils.ShiroUtils;
import org.dsa.config.SyncConfig;
import org.dsa.modules.remoteDiagnostic.config.RemoteDiagnosticConfig;
import org.dsa.modules.remoteDiagnostic.constant.RemoteDiagnosticConstant;
import org.dsa.modules.remoteDiagnostic.dao.*;
import org.dsa.modules.remoteDiagnostic.entity.*;
import org.dsa.modules.remoteDiagnostic.enums.RemoteVersionStatusEnum;
import org.dsa.modules.remoteDiagnostic.enums.RemoteVersionTypeEnum;
import org.dsa.modules.remoteDiagnostic.service.RemoteEcuVersionService;
import org.dsa.modules.remoteDiagnostic.vo.CheckNameReqVo;
import org.dsa.modules.remoteDiagnostic.vo.PublicPageReqVo;
import org.dsa.modules.reporter.util.FilesUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SessionCallback;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class RemoteEcuVersionServiceImpl implements RemoteEcuVersionService {

    @Autowired
    RemoteEcuVersionDao vehicleEcuVersionDao;
    //    @Autowired
//    RemoteEcuEcuVersionDao ecuEcuVersionDao;
    @Autowired
    RemoteEcuIdentificationInfoDao identificationInfoDao;
    @Autowired
    RemoteVehicleEcuDao ecuDao;
    @Autowired
    SyncConfig syncConfig;
    @Autowired
    RemoteDiagnosticConfig remoteDiagnosticConfig;
    @Autowired
    RemoteVehicleEcuVersionFileDao fileDao;
    @Autowired
    RemotePoolDependDao dependDao;
    @Autowired
    RemoteVersionRecordDao versionRecordDao;
    @Autowired
    RedisTemplate<String, Object> redisTemplate;

    @Override
    public Page<RemoteVehicleEcuVersionEntity> selectPage(PublicPageReqVo vo) {
        Page<RemoteVehicleEcuVersionEntity> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);

        Page<RemoteVehicleEcuVersionEntity> resultPage = vehicleEcuVersionDao.getPage(page, vo);

        return resultPage;
    }

    @Override
    public RemoteVehicleEcuVersionEntity getInfo(Long id) {
        return vehicleEcuVersionDao.getInfo(id);
    }

    @Override
    public RemoteVehicleEcuVersionEntity getOneInfo(Long id) {
        return vehicleEcuVersionDao.selectById(id);
    }

    @Override
    @Transactional
    public void saveVersion(RemoteVehicleEcuVersionEntity version) {
        vehicleEcuVersionDao.insert(version);

        if (version.getExpectedIdentifications() != null && version.getExpectedIdentifications().size() > 0) {
            identificationInfoDao.insertsByVal(version.getId(), version.getExpectedIdentifications());
        }
        if (version.getTargetIdentifications() != null && version.getTargetIdentifications().size() > 0) {
            identificationInfoDao.insertsByVal(version.getId(), version.getTargetIdentifications());
        }

        if (version.getFiles() != null && version.getFiles().size() > 0) {
            version.getFiles().forEach(item -> {
                try {
                    if (StringUtils.isBlank(item.getFileMd5()))
                        item.setFileMd5(DigestUtils.md5DigestAsHex(new FileInputStream(new File(item.getFilePath()))));
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            });
            fileDao.inserts(version.getId(), version.getFiles());
        }

        saveRecord(version, RemoteVersionTypeEnum.ADD);

    }

    @Override
    @Transactional
    public void updateVersion(RemoteVehicleEcuVersionEntity version) {
        version.setUpdatedAt(new Date());
        vehicleEcuVersionDao.updateById(version);

        // 先删除
        identificationInfoDao.delete(Wrappers.<RemoteEcuIdentificationInfoEntity>lambdaUpdate().eq(RemoteEcuIdentificationInfoEntity::getEcuVersionId, version.getId()));

        if (version.getExpectedIdentifications() != null && version.getExpectedIdentifications().size() > 0) {
            identificationInfoDao.insertsByVal(version.getId(), version.getExpectedIdentifications());
        }
        if (version.getTargetIdentifications() != null && version.getTargetIdentifications().size() > 0) {
            identificationInfoDao.insertsByVal(version.getId(), version.getTargetIdentifications());
        }

        // 先删除
        fileDao.delete(Wrappers.<RemoteVehicleEcuVersionFileEntity>lambdaUpdate().eq(RemoteVehicleEcuVersionFileEntity::getVersionId, version.getId()));
        if (version.getFiles() != null && version.getFiles().size() > 0) {
            version.getFiles().forEach(item -> {
                try {
                    if (StringUtils.isBlank(item.getFileMd5()))
                        item.setFileMd5(DigestUtils.md5DigestAsHex(new FileInputStream(new File(item.getFilePath()))));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
//            String md5 = DigestUtils.md5DigestAsHex(new FileInputStream(new File(file.getFilePath())));
//            file.setFileMd5(md5);

            fileDao.inserts(version.getId(), version.getFiles());
        }

        saveRecord(version, RemoteVersionTypeEnum.EDIT);


        if (version.getStatus() != 3) {
            List<String> poolKeys = dependDao.selectList(Wrappers.<RemotePoolDependEntity>lambdaQuery().eq(RemotePoolDependEntity::getEcuVersionId, version.getId())).stream().map(e -> {
                return RemoteDiagnosticConstant.VEHICLE_POOL_ITEM_KEY + e.getPoolId();
            }).collect(Collectors.toList());
            if (poolKeys.size() != 0) {
                redisTemplate.opsForValue().set((RemoteDiagnosticConstant.VEHICLE_POOL_LOCK), System.currentTimeMillis() + "");

                redisTemplate.execute(new SessionCallback<Object>() {
                    @Override
                    public <K, V> Object execute(RedisOperations<K, V> operations) throws DataAccessException {
                        operations.multi(); // 开启事务
                        poolKeys.forEach(e -> redisTemplate.opsForHash().delete(RemoteDiagnosticConstant.VEHICLE_POOL_KEY, e));
                        return operations.exec();  //提交
                    }
                });
            }
        }

    }

    private void saveRecord(RemoteVehicleEcuVersionEntity version, RemoteVersionTypeEnum edit) {
        RemoteVersionRecordEntity versionRecord = new RemoteVersionRecordEntity();
        versionRecord.setType(RemoteVersionTypeEnum.ECUVersion.getValue());
        versionRecord.setValueId(version.getId());
        versionRecord.setEditType(edit.getValue());
        versionRecord.setStatus(version.getStatus());
        versionRecord.setCreateBy(ShiroUtils.getUserId());
        versionRecord.setCreateUserName(ShiroUtils.getUserName());
        versionRecordDao.insert(versionRecord);
    }

    @Override
    public void copyVersionById(Long id) {
        RemoteVehicleEcuVersionEntity version = vehicleEcuVersionDao.selectById(id);

        RemoteVehicleEcuVersionEntity lastVersion = vehicleEcuVersionDao.selectOne(Wrappers.<RemoteVehicleEcuVersionEntity>lambdaQuery().eq(RemoteVehicleEcuVersionEntity::getEcuId, version.getEcuId()).orderByDesc(RemoteVehicleEcuVersionEntity::getVersionNumber).last(" limit 1"));

        version.setId(null);
        version.setCreatedAt(null);
        version.setUpdatedAt(null);
        version.setVersionName(version.getVersionName() + " copy");
        version.setStatus(RemoteVersionStatusEnum.DEV.getValue());
        version.setVersionNumber(lastVersion.getId());

        vehicleEcuVersionDao.insert(version);

        this.saveRecord(version, RemoteVersionTypeEnum.ADD);

    }

    @Override
    public List<RemoteVehicleEcuVersionEntity> getVersions() {
        return null;
    }

    @Override
    public List<RemoteVehicleEcuVersionEntity> getVersions(Long ecuId) {

        return vehicleEcuVersionDao.getVersionList(ecuId);
    }

    @Override
    public Boolean check(CheckNameReqVo vo) {
        LambdaQueryWrapper<RemoteVehicleEcuVersionEntity> qw = Wrappers.<RemoteVehicleEcuVersionEntity>lambdaQuery();
        qw.eq(RemoteVehicleEcuVersionEntity::getVersionName, vo.getName());
        qw.select(RemoteVehicleEcuVersionEntity::getId);

        if (vo.getId() != null && vo.getGroupId() != null) {
            qw.ne(RemoteVehicleEcuVersionEntity::getId, vo.getId());
            qw.eq(RemoteVehicleEcuVersionEntity::getEcuId, vo.getGroupId());
            return vehicleEcuVersionDao.selectOne(qw) == null;
        } else if (vo.getGroupId() != null) {
            qw.eq(RemoteVehicleEcuVersionEntity::getEcuId, vo.getGroupId());
            return vehicleEcuVersionDao.selectOne(qw) == null;
        } else {
            return true;
        }
    }

    @Override
    public Map<String, String> upload(File file, String type) throws Exception {
        Map<String, String> map = new HashMap<>();

        String dateFormat = syncConfig.getDirFormat();
        SimpleDateFormat format = new SimpleDateFormat(dateFormat);
        String dayDir = format.format(new Date());
        int i = file.getName().indexOf(".");

        String yname = file.getName();
        // 服务器自定义文件名/文件夹
        String firstName = i != -1 ? file.getName().substring(0, i) + "_" + System.currentTimeMillis() : file.getName() + "_" + System.currentTimeMillis();
        // 目标文件名
        String lastName = i != -1 ? firstName + file.getName().substring(i) : firstName;

        File dir = new File(remoteDiagnosticConfig.getEcuVersion() + dayDir);

        if (!dir.exists()) {
            FilesUtil.mkdir(dir);
        }
        File destFile = new File(remoteDiagnosticConfig.getEcuVersion() + dayDir + "/" + lastName);

        File zipDir = null;
        ZipArchiveInputStream zipInputStream = null;

        try {
            FileUtils.copyFile(file, destFile);
            if ("releaseNote".equals(type) && destFile.getName().endsWith(".zip")) {
                String zipBackup = dir.getPath() + File.separator + firstName;
//            if (!zipBackup.exists()) {
//                FilesUtil.mkdir(zipBackup);
//            }
                zipDir = new File(zipBackup);
                if (!zipDir.exists()) {
                    FilesUtil.mkdir(zipDir);
                }
                zipInputStream = new ZipArchiveInputStream(new FileInputStream(destFile));

                if (decompressZip(zipInputStream, zipDir)) {
                    return map;
                }
            }
        } catch (Exception e) {
            log.error("------------- 导入版本文件异常 --------------", e);
            return map;
        } finally {
            boolean delete = file.delete();
            if (zipInputStream != null) {
                zipInputStream.close();
            }
            if (zipDir != null && zipDir.exists()) {
                boolean delete1 = zipDir.delete();
            }
        }

        map.put("name", yname);
        map.put("path", destFile.getPath());
        return map;
    }

    @Override
    public RemoteVehicleEcuEntity getEcuVersionByEcuId(Long id) {
        return ecuDao.getEcu(id);
    }

    @Override
    public Map<String, String> uploadImage(File file) throws IOException {
        Map<String, String> map = new HashMap<>();

        String dateFormat = syncConfig.getDirFormat();
        SimpleDateFormat format = new SimpleDateFormat(dateFormat);
        String dayDir = format.format(new Date());
        int i = file.getName().indexOf(".");
        String name = file.getName().substring(0, i) + "_" + System.currentTimeMillis() + file.getName().substring(i);


        File dir = new File(remoteDiagnosticConfig.getEcuImage() + dayDir);

        if (!dir.exists()) {
            FilesUtil.mkdir(dir);
        }
        File destFile = new File(remoteDiagnosticConfig.getEcuImage() + dayDir + "/" + name);

        FileUtils.copyFile(file, destFile);
        boolean delete = file.delete();

        map.put("name", name);
        map.put("path", destFile.getPath());
        return map;
    }

    @Override
    public List<RemoteVehicleEcuVersionFileEntity> getVersionFile(String type, Long id) {
        return fileDao.selectList(Wrappers.<RemoteVehicleEcuVersionFileEntity>lambdaQuery().eq(RemoteVehicleEcuVersionFileEntity::getVersionId, id).eq(RemoteVehicleEcuVersionFileEntity::getFileType, type));
    }

    @Override
    public List<RemoteVehicleEcuEntity> selectEcuVersionDepend(Long id, Long ecuId) {
        return vehicleEcuVersionDao.selectEcuVersionDepend(id, ecuId);
    }

    @Override
    public List<RemoteVehicleEcuVersionEntity> getAllVersions(Integer status) {
        return vehicleEcuVersionDao.selectAll(status);
    }

    private boolean decompressZip(ZipArchiveInputStream zipInputStream, File desDirectory) throws Exception {

        // 读入流
//        ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(zipFile));
        // 遍历每一个文件
        ArchiveEntry zipEntry = zipInputStream.getNextEntry();
        // 此处校验是否至少有一个文件
        boolean flag = true;
        while (zipEntry != null) {
            if (!zipEntry.isDirectory() && zipEntry.getSize() != 0) {
                flag = false;
                break;
            }
            zipEntry = zipInputStream.getNextEntry();
        }
        return flag;
//        while (zipEntry != null) {
//            if (zipEntry.isDirectory()) { // 文件夹
//                String unzipFilePath = desDirectory.getPath() + File.separator + zipEntry.getName();
//                // 直接创建
//                FilesUtil.mkdir(new File(unzipFilePath));
//
//            } else { // 文件
//                String unzipFilePath = desDirectory.getPath() + File.separator + zipEntry.getName();
//                File file = new File(unzipFilePath);
//                // 创建父目录
//                FilesUtil.mkdir(file.getParentFile());
//                // 写出文件流
//                BufferedOutputStream bufferedOutputStream =
//                        new BufferedOutputStream(new FileOutputStream(unzipFilePath));
//                byte[] bytes = new byte[1024];
//                int readLen;
//                while ((readLen = zipInputStream.read(bytes)) != -1) {
//                    bufferedOutputStream.write(bytes, 0, readLen);
//                }
//                bufferedOutputStream.close();
//            }
////            zipInputStream.c()
//            zipEntry = zipInputStream.getNextEntry();
//        }
    }
}
