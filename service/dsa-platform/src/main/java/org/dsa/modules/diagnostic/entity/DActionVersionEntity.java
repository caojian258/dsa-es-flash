package org.dsa.modules.diagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("d_action_version")
public class DActionVersionEntity implements Serializable
{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 功能执行id
     */
    private String actionId;
    /**
     * 排序
     */
    private int serial;
    /**
     * ecu 名称
     */
    private String ecuName;
    /**
     * 语言
     */
    private String ti;
    /**
     * 执行消息
     */
    private String semantic;
    /**
     * 标识名称
     */
    private String name;
    /**
     * 标识值
     */
    private String tiValue;
    /**
     * 标识值
     */
    private String value;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * session_id
     */
    private String sessionId;

    /**
     * did
     */
    private String did;
}
