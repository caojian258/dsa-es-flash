//package org.dsa.modules.remoteDiagnostic.entity;
//
//import com.baomidou.mybatisplus.annotation.IdType;
//import com.baomidou.mybatisplus.annotation.TableField;
//import com.baomidou.mybatisplus.annotation.TableId;
//import com.baomidou.mybatisplus.annotation.TableName;
//import lombok.Data;
//import lombok.ToString;
//import org.dsa.modules.remoteDiagnostic.pojo.BaseEntity;
//
//import java.io.Serializable;
//import java.util.List;
//
//@Data
//@ToString
//@TableName("r_ecu_ecu_version")
//public class RemoteEcuEcuVersionEntity extends BaseEntity implements Serializable {
//
//
//    private static final long serialVersionUID = 1L;
//
//    @TableId(type = IdType.AUTO)
//    private Long id;
//
//    private String type;
//
//    private Long initId;
//
//    private Long ecuId;
//
//    @TableField(exist = false)
//    private Long vId;
//
//    @TableField(exist = false)
//    private List<RemoteVehicleEcuVersionEntity> versions;
//}
