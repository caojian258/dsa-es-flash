package org.dsa.modules.reporter.service;

import org.dsa.modules.reporter.dto.ClientUpgradeFullDto;
import org.dsa.modules.reporter.entity.*;


import java.util.List;
import java.util.Map;
import java.util.Set;

public interface ClientUpgradeService {

    public List<ClientUpgrade> queryDataByTag(String tag);

    public List<ClientUpgradeFullDto> allData(String tag);


    public void insertHistory(ClientUpgradeFullDto dto);


    /****
     * 通过客户端查询更新记录 缓存
     * @param deviceId
     * @return
     */
    public Set<Integer> queryClientUpdateId(String deviceId);

    /***
     * 检查客户端是否更新过
     * @param deviceId
     * @param updateId
     * @return
     */
    public Boolean checkClientUpdateId(String deviceId, Integer updateId);

    /***
     * 缓存设备更新记录
     * @param deviceId
     * @param updateId
     * @return
     */
    public long cacheClientUpdateId(String deviceId, Integer updateId);

    /***
     * 通过客户端查询更新记录不缓存
     * @param deviceId
     * @return
     */
    public Set<Integer> queryClientUpdateIdWithoutCache(String deviceId);


    public Map<Integer, CurrentSoftwareInfo> queryCurrentSoftwareInfoByTag(String deviceId, String tag, List<Integer> idList);


    public Map<Integer, FutureSoftwareInfo> queryFutureSoftwareInfoByTag(String deviceId, String tag, List<Integer> idList);


    public Map<Integer, CurrentComponentInfo> queryCurrentComponentInfoByTag(String deviceId, String tag, List<Integer> idList);


    public Map<Integer, FutureComponentInfo> queryFutureComponentInfoByTag(String deviceId, String tag, List<Integer> idList);
}
