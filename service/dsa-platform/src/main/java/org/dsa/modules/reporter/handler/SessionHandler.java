package org.dsa.modules.reporter.handler;

import org.dsa.modules.reporter.dto.SessionStateDto;
import org.dsa.modules.reporter.dto.SharedDataDto;
import org.dsa.modules.reporter.dto.StatusDto;
import org.dsa.modules.reporter.service.DiagStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class SessionHandler{

    @Autowired
    DiagStatusService statusService;

    public StatusDto readStatus(String path, String tag, String sessionId, String vinCode){
        return statusService.storeData(path, tag, sessionId, vinCode);
    }

    public SharedDataDto readSharedData(String path, String vinCode) throws Exception {
        SharedDataDto dto = statusService.storeVehicleInfo(path, vinCode);
        return dto;
    }

    public SessionStateDto readSessionState(String path, String tag, String sessionId, String vinCode, String pcid, String workshop, String lang) throws Exception {
        SessionStateDto dto = statusService.storeSessionState(path, tag, sessionId, vinCode, pcid, workshop, lang);
        return dto;
    }


}
