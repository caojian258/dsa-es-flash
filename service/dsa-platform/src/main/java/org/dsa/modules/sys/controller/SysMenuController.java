package org.dsa.modules.sys.controller;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.dsa.common.annotation.SysLog;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.R;
import org.dsa.modules.diagnostic.service.DiagnosticService;
import org.dsa.modules.sys.entity.SysMenuEntity;
import org.dsa.modules.sys.enums.SystemEnum;
import org.dsa.modules.sys.service.ShiroService;
import org.dsa.modules.sys.service.SysMenuService;
import org.dsa.modules.sys.service.SysRoleMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import jakarta.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 系统菜单
 *
 */
@RestController
@RequestMapping("/sys/menu")
public class SysMenuController extends AbstractController {
	@Autowired
	private SysMenuService sysMenuService;
	@Autowired
	private ShiroService shiroService;

	@Resource
	private SysRoleMenuService sysRoleMenuService;

	@Autowired
	DiagnosticService diagnosticService;

	/**
	 * 导航菜单
	 */
	@GetMapping("/nav")
	public R nav(@RequestHeader Integer owningSystem){
		if(Objects.isNull(owningSystem)){
			owningSystem = SystemEnum.DSA_PLATFORM.getCode();
		}
		logger.info("owningSystem {}",owningSystem);
		// 指定系统菜单,指定系统权限
		List<SysMenuEntity> menuList = sysMenuService.getUserMenuListByRoleList(getRoleList(),owningSystem);
		Set<String> permissions = shiroService.getPermissionsByRole(getRoleList(),owningSystem);
		return R.ok().put("menuList", menuList).put("permissions", permissions);
	}
	
	/**
	 * 所有菜单列表
	 */
	@GetMapping("/list")
	@RequiresPermissions("sys:menu:list")
	public List<SysMenuEntity> list(){
		List<SysMenuEntity> menuList = sysMenuService.list();
		for(SysMenuEntity sysMenuEntity : menuList){
			List<SysMenuEntity> collect = menuList.stream().filter(obj -> Objects.equals(obj.getMenuId(), sysMenuEntity.getParentId())).collect(Collectors.toList());
			if (collect.size()>0) {
				SysMenuEntity parentMenuEntity = collect.get(0);
				if(parentMenuEntity != null){
					sysMenuEntity.setParentName(parentMenuEntity.getName());
				}
			}
//			SysMenuEntity parentMenuEntity = sysMenuService.getById(sysMenuEntity.getParentId());
//			if(parentMenuEntity != null){
//				sysMenuEntity.setParentName(parentMenuEntity.getName());
//			}
		}
		// 按照优先级排序
		List<SysMenuEntity> sorted = menuList.stream().sorted(Comparator.comparing(SysMenuEntity::getOrderNum)).collect(Collectors.toList());
		return sorted;
	}
	/**
	 * 用户拥有的菜单列表
	 */
	@GetMapping("/userList")
	public List<SysMenuEntity> userList(@RequestHeader Integer owningSystem){
		// menu owningSystem 为系统类型 && General Menu
		if (Objects.isNull(owningSystem)){
			owningSystem = SystemEnum.DSA_PLATFORM.getCode();
		}
		List<SysMenuEntity> list = new ArrayList<SysMenuEntity>();
		List<SysMenuEntity> menuList = new ArrayList<>();
		if(Constant.SUPER_ADMIN_USER_NAME.equals(getUserName())) {
			menuList = sysRoleMenuService.queryAllMenuEntity();
		}else{
			menuList = sysRoleMenuService.queryRoleMenuEntity(getRoleList(),owningSystem);
		}
		for(SysMenuEntity sysMenuEntity : menuList){
			if(sysMenuEntity != null) {
				SysMenuEntity parentMenuEntity = sysMenuService.getById(sysMenuEntity.getParentId());
				if (parentMenuEntity != null) {
					sysMenuEntity.setParentName(parentMenuEntity.getName());
				}
				list.add(sysMenuEntity);
			}
		}
		return list;
	}
	
	/**
	 * 选择菜单(添加、修改菜单)
	 */
	@GetMapping("/select")
	@RequiresPermissions("sys:menu:select")
	public R select(){
		//查询列表数据
		List<SysMenuEntity> menuList = sysMenuService.queryNotButtonList();
		//添加顶级菜单
		SysMenuEntity root = new SysMenuEntity();
		root.setMenuId(0L);
		root.setName("一级菜单");
		root.setParentId(-1L);
		root.setOpen(true);
		menuList.add(root);
		return R.ok().put("menuList", menuList);
	}
	
	/**
	 * 菜单信息
	 */
	@GetMapping("/info/{menuId}")
	@RequiresPermissions("sys:menu:info")
	public R info(@PathVariable("menuId") Long menuId){
		SysMenuEntity menu = sysMenuService.getById(menuId);
		return R.ok().put("menu", menu);
	}
	
	/**
	 * 保存
	 */
	@PostMapping("/save")
	@SysLog("添加菜单")
	@RequiresPermissions("sys:menu:save")
	public R save(@RequestBody SysMenuEntity menu){
		//数据校验
		verifyForm(menu);

		if (Objects.isNull(menu.getUseTo())) {
			menu.setUseTo(0);
		}
		sysMenuService.save(menu);
		
		return R.ok();
	}
	
	/**
	 * 修改
	 */
	@PostMapping("/update")
	@SysLog("修改菜单")
	@RequiresPermissions("sys:menu:update")
	public R update(@RequestBody SysMenuEntity menu){
		//数据校验
		verifyForm(menu);
				
		sysMenuService.updateById(menu);
		
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping("/delete/{menuId}")
	@SysLog("删除菜单")
	@RequiresPermissions("sys:menu:delete")
	public R delete(@PathVariable("menuId") long menuId){
//		if(menuId <= 31){
//			return R.error("系统菜单，不能删除");
//		}
		//判断是否有子菜单或按钮
		List<SysMenuEntity> menuList = sysMenuService.queryListParentId(menuId);
		if(menuList.size() > 0){
			return R.error("请先删除子菜单或按钮");
		}

		sysMenuService.delete(menuId);

		return R.ok();
	}
	
	/**
	 * 验证参数是否正确
	 */
	private void verifyForm(SysMenuEntity menu){

		if(StringUtils.isBlank(menu.getName())){
			throw new RRException("名称不能为空");
		}
		//用户名校验
		LambdaQueryWrapper<SysMenuEntity> query = new LambdaQueryWrapper<SysMenuEntity>()
				.eq(SysMenuEntity::getName, menu.getName())
				.ne(ObjectUtil.isNotNull(menu.getMenuId()),SysMenuEntity::getMenuId,menu.getMenuId());
		SysMenuEntity menu1 = sysMenuService.getOne(query);
		if (menu1 != null) {
			//名称已存在
			throw new RRException(Constant.Msg.NAME_EXIST_ERROR);
		}

		if(menu.getParentId() == null){
			throw new RRException("上级菜单不能为空");
		}
		
		//菜单
		if(menu.getType() == Constant.MenuType.MENU.getValue()){
			if(StringUtils.isBlank(menu.getUrl())){
				throw new RRException("菜单URL不能为空");
			}
		}
		
		//上级菜单类型
		int parentType = Constant.MenuType.CATALOG.getValue();
		if(menu.getParentId() != 0){
			SysMenuEntity parentMenu = sysMenuService.getById(menu.getParentId());
			parentType = parentMenu.getType();
		}
		
		//目录、菜单
		if(menu.getType() == Constant.MenuType.CATALOG.getValue() ||
				menu.getType() == Constant.MenuType.MENU.getValue()){
			if(parentType != Constant.MenuType.CATALOG.getValue()){
				throw new RRException("上级菜单只能为目录类型");
			}
			return ;
		}
		
		//按钮
		if(menu.getType() == Constant.MenuType.BUTTON.getValue()){
			if(parentType != Constant.MenuType.MENU.getValue()){
				throw new RRException("上级菜单只能为菜单类型");
			}
			return ;
		}
	}

	/**
	 * 子系统调用查权限
	 * @param roles
	 * @param owningSystem
	 * @return
	 */
	@PostMapping("/permissions")
	public String getPermissionsByRole(@RequestBody List<String> roles,@RequestHeader Integer owningSystem){
		return shiroService.getPermissionsByRole(roles,owningSystem).toString();
	}


	/**
	 * 诊断导航菜单
	 */
	@GetMapping("/diagnosticNav")
	public R diagnosticNav(@RequestParam String vin){
		//检查诊断状态
		diagnosticService.checkRunRedisValue(vin);
		//获取菜单
		List<SysMenuEntity> menuList = sysMenuService.getDiagnosticUserMenuListByRoleList(getRoleList(),vin);
		Set<java.lang.String> permissions = shiroService.getPermissionsByRole(getRoleList(), SystemEnum.DSA_PLATFORM.getCode());

		return R.ok().put("menuList", menuList).put("permissions", permissions);
	}

	/**
	 * 客户端菜单列表(内部)
	 *
	 * @return R
	 */
	/*@PostMapping("/client-menus")
	public R clientMenus() {
		List<String> menuList = sysMenuService.clientMenus(getRoleList());
		return R.ok().put("data", menuList);
	}*/

}
