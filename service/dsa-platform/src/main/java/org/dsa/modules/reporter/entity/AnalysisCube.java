package org.dsa.modules.reporter.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@TableName("d_analysis_cube")
public class AnalysisCube extends BaseEntity{

    @TableId(type = IdType.AUTO)
    private Long id;

    private String code;

    /***
     * table name
     */
    private String tableName;

    /***
     * table fields
     */
    private String tableColumn;

    /***
     * 日期字段 非必填
     */
    private String dateColumn;

    /***
     * 日期字段 属性
     */
    private String dateAttribute;
    /***
     * topic
     */
    private String dimesion;

    /***
     * where
     */
    private String condition;

    /***
     * aggregator function
     */
    private String aggregator;

    /***
     * group by
     */
    private String measure;

    /***
     * visible format
     */
    private String visible;

    /***
     * 是否启用
     */
    private int enabled;

    /***
     * 生成的sql
     */
    private String sql;

    private Integer top;
}
