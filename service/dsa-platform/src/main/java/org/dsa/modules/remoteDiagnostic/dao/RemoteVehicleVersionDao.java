package org.dsa.modules.remoteDiagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleVersionEntity;

import java.util.Arrays;
import java.util.List;

@Mapper
public interface RemoteVehicleVersionDao extends BaseMapper<RemoteVehicleVersionEntity> {


    @Select("select i.*,i.id as versionId,'expect' as expect,'target' as target from r_vehicle_version i where id = #{id}")
    @Results({
//            @Result(property = "expectedIdentifications", column = "{versionId=versionId,type=expect}",
//                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.RemoteEcuIdentificationInfoDao.getListByVehicleVersion")),
//            @Result(property = "targetIdentifications", column = "{versionId=versionId,type=target}",
//                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.RemoteEcuIdentificationInfoDao.getListByVehicleVersion")),
            @Result(property = "vehicleVersionEcuGroups", column = "id",
                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.RemoteVehicleVersionEcuGroupDao.getInfoByVersion")),
    })
    RemoteVehicleVersionEntity getInfo(@Param("id") Long id);

    @Select("select i.*,j.value as typeName from r_vehicle_version i left join d_vehicle_type j on i.type_id = j.id where i.status = #{status} and j.id is not null")
    List<RemoteVehicleVersionEntity> getAllList(Integer status);

    List<RemoteVehicleVersionEntity> getListByType(@Param("typeId") Long typeId);

    // 向向上递归找所有车辆版本
    List<RemoteVehicleVersionEntity> getListByType2(@Param("typeId") Long typeId);

    // 向下递归找所有非释放的版本
    List<RemoteVehicleVersionEntity> getListByType3(@Param("typeId") Long typeId);

    List<RemoteVehicleVersionEntity> getListByTypes(@Param("typeIds") List<Long> typeId);

    List<RemoteVehicleVersionEntity> findTargetVersion(@Param("typeId") Long typeId, @Param("versionId") Long versionId);

    RemoteVehicleVersionEntity findVersion(@Param("typeId") Long typeId, @Param("version") String version);

    Integer checkTargetVersion(@Param("typeId") Long typeId, @Param("version") String version);
}
