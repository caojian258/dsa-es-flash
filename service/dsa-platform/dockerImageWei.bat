D:

cd D:\project_workspace\trunk\DsaPlatform\service\dsa-platform

call mvn package

docker build -t "dsa-platform" ./

docker tag dsa-platform:latest registry.cn-hangzhou.aliyuncs.com/dsa-dev/dsa-platform:dev

docker push registry.cn-hangzhou.aliyuncs.com/dsa-dev/dsa-platform:dev
