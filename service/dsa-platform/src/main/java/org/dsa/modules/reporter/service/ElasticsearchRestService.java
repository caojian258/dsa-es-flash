package org.dsa.modules.reporter.service;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.get.GetIndexRequest;
import org.elasticsearch.action.admin.indices.get.GetIndexResponse;
import org.elasticsearch.action.admin.indices.mapping.put.PutMappingRequest;
import org.elasticsearch.action.admin.indices.template.delete.DeleteIndexTemplateRequest;
import org.elasticsearch.action.admin.indices.template.get.GetIndexTemplatesResponse;
import org.elasticsearch.action.admin.indices.template.put.PutIndexTemplateRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.ClearScrollRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.GetIndexTemplatesRequest;
import org.elasticsearch.client.indices.IndexTemplatesExistRequest;
import org.elasticsearch.common.Strings;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.*;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.histogram.Histogram;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.elasticsearch.search.suggest.SuggestBuilders;
import org.elasticsearch.search.suggest.completion.CompletionSuggestion;
import org.elasticsearch.search.suggest.completion.CompletionSuggestionBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class ElasticsearchRestService {

    @Autowired
    RestHighLevelClient client;

    public RestHighLevelClient getClient(){
        return client;
    }

    /***
     * 创建索引模板
     * @param name
     * @param patterns
     * @param properties
     * @param setting
     * @throws IOException
     */
    public void createIndexTemplate(String name, List<String> patterns,
                                    Map<String,Map<String, Object>> properties, Map<String, Object> setting) throws IOException {
        PutIndexTemplateRequest request = new PutIndexTemplateRequest(name);
        request.patterns(patterns);

        Map<String, Object> jsonMap = new HashMap<>();
        {
            jsonMap.put("properties", properties);
        }

        Map<String, Object> source = new HashMap<>();
        source.put("enabled", false);
        jsonMap.put("_source", source);

        request.mapping("_doc", jsonMap);
        request.settings(setting);

        AcknowledgedResponse response = client.indices().putTemplate(request, RequestOptions.DEFAULT);
        log.info("createIndexTemplate: {}", JSONObject.toJSONString(response));
    }

    /***
     * 通过json字符串创建索引模板
     * @param name
     * @param strMapping
     * @return
     * @throws IOException
     */
    public Boolean createIndexTemplateByString(String name, String strMapping) throws IOException {

        PutIndexTemplateRequest request = new PutIndexTemplateRequest(name);
        request.source(strMapping, XContentType.JSON);
        request.create(true);
        request.order(20);
        AcknowledgedResponse response = client.indices().putTemplate(request, RequestOptions.DEFAULT);
        log.info("createIndexTemplate: {}", JSONObject.toJSONString(response.isAcknowledged()));
        return response.isAcknowledged();
    }

    /**
     * 检查模板是否存在
     * @param name
     * @return
     * @throws IOException
     */
    public Boolean checkExistTemplate(String name) throws IOException {
        IndexTemplatesExistRequest request;
        request = new IndexTemplatesExistRequest(name);
        boolean exists = client.indices().existsTemplate(request, RequestOptions.DEFAULT);
        log.info("{}", JSONObject.toJSONString(exists));
        return exists;
    }

    /***
     * 获取索引模板
     * @return
     * @throws IOException
     */
    public Map<String, String> getIndexTemplates(String name) throws IOException {
        GetIndexTemplatesRequest request = new GetIndexTemplatesRequest(name);

        GetIndexTemplatesResponse response = client.indices().getTemplate(request, RequestOptions.DEFAULT);
        Map<String, String> map = new HashMap<>();
        map.put("setting", response.getIndexTemplates().get(0).getMappings().toString());
        map.put("mapping", response.getIndexTemplates().get(0).getMappings().toString());
        return map;
    }

    /***
     * 删除特定索引模板
     * @param name
     * @throws IOException
     */
    public Boolean deleteIndexTemplates(String name) throws IOException {
        DeleteIndexTemplateRequest request = new DeleteIndexTemplateRequest();
        request.name(name);
        AcknowledgedResponse deleteTemplateAcknowledge = client.indices().deleteTemplate(request, RequestOptions.DEFAULT);
        log.info("{}", JSONObject.toJSONString(deleteTemplateAcknowledge.isAcknowledged()));
        return deleteTemplateAcknowledge.isAcknowledged();
    }

    /***
     * 创建索引
     * @param name
     * @return
     * @throws IOException
     */
    public boolean createIndex(String name) throws IOException {
        CreateIndexRequest request = new CreateIndexRequest(name);
        CreateIndexResponse response =  client.indices().create(request,RequestOptions.DEFAULT);
        log.info("{}", JSONObject.toJSONString(response));
        return response.isAcknowledged();
    }

    /***
     * 删除索引
     * @param name
     * @return
     * @throws IOException
     */
    public boolean deleteIndex(String name) throws IOException {
        DeleteIndexRequest request = new DeleteIndexRequest(name);
        AcknowledgedResponse response = client.indices().delete(request,RequestOptions.DEFAULT);
        return response.isAcknowledged();
    }

    /***
     * 根据名称确定索引是否存在
     * @param name
     * @return
     * @throws IOException
     */
    public boolean checkExistIndex(String name) throws IOException {
        GetIndexRequest request = new GetIndexRequest();
        request.indices(name);
        return client.indices().exists(request, RequestOptions.DEFAULT);
    }

    public String getIndex(String name) throws IOException {
        GetIndexRequest request = new GetIndexRequest();
        request.indices(name);
        GetIndexResponse getIndexResponse = client.indices().get(request, RequestOptions.DEFAULT);
        log.info("{}", JSONObject.toJSONString(getIndexResponse.getMappings()));
        return JSONObject.toJSONString(getIndexResponse.getMappings());
    }
    /***
     * 设置索引Mapping
     * @param name
     * @param properties
     * @param setting
     * @return
     * @throws IOException
     */
    public boolean putIndexMapping(String name, Map<String, Map<String, Object>> properties, Map<String, Object> setting) throws IOException {
        PutMappingRequest request =new PutMappingRequest(name);
        Map<String, Object> jsonMap = new HashMap<>();
        {
            jsonMap.put("properties", properties);
        }

        Map<String, Object> source = new HashMap<>();
        source.put("enabled", false);
        jsonMap.put("_source", source);
        request.type(name).source(jsonMap);

        AcknowledgedResponse response =  client.indices().putMapping(request, RequestOptions.DEFAULT);
        log.info("{}", JSONObject.toJSONString(response));
        return response.isAcknowledged();
    }

    /***
     * 单个添加
     * @param name
     * @param jsonMap
     * @return
     * @throws IOException
     */
    public void addIndexDoc(String name, Map<String, Object> jsonMap) throws IOException {
        IndexRequest request = new IndexRequest(name);
        request.type("_doc");
        if(null != jsonMap.get("id")){
            request.id(jsonMap.get("id").toString());
        }
        String json = JSONObject.toJSONString(jsonMap, SerializerFeature.WriteMapNullValue,
                SerializerFeature.DisableCircularReferenceDetect,
                SerializerFeature.WriteDateUseDateFormat);
        request.source(json, XContentType.JSON);

        IndexResponse response = client.index(request, RequestOptions.DEFAULT);
        log.info("{}", JSONObject.toJSONString(response));

    }

    /***
     * 获取单个文档
     * @param indexName
     * @param id
     * @return
     * @throws IOException
     */
    public Map<String, Object> getIndexDoc(String indexName, String id) throws IOException {
        GetRequest request = new GetRequest (
                indexName,
                "_doc",
                id);
        GetResponse getResponse = client.get(request, RequestOptions.DEFAULT);
        return getResponse.getSource();
    }

    /***
     * 批量添加
     * @param index
     * @param items
     * @return
     * @throws IOException
     */
    public Integer bulkAddIndexDoc(String index, List<Map<String, Object>> items) throws IOException {
        BulkRequest bulk = new BulkRequest();

        for (Map<String, Object> item: items) {
            IndexRequest request = new IndexRequest(index);
            String json = JSONObject.toJSONString(item, SerializerFeature.WriteMapNullValue,
                    SerializerFeature.DisableCircularReferenceDetect,
                    SerializerFeature.WriteDateUseDateFormat);
            request.type("_doc");
            if(null != item.get("id")){
                request.id(item.get("id").toString());
            }
            request.source(json, XContentType.JSON);
            bulk.add(request);
        }

        BulkResponse responses =  client.bulk(bulk,RequestOptions.DEFAULT);
        return responses.getItems().length;
    }


    /***
     * 根据某个字段的值查询
     * @param index
     * @param field
     * @param value
     * @param size
     * @param from
     * @param orderBy sort field
     * @param order desc or asc
     * @return
     * @throws IOException
     */
    public Map<String, Object> queryTerm(String index, String field, String value, Integer size, Integer from,
                          String orderBy, String order, String unmappedType) throws IOException {
        SearchRequest searchRequest = new SearchRequest(index);
        searchRequest.types("_doc");
        searchRequest.indices(index);

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        if(null != field && null != value){
            MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder(field, value);
            searchSourceBuilder.query(matchQueryBuilder);
        }

        searchSourceBuilder.from(from);
        searchSourceBuilder.size(size);
        if(null == orderBy){
            orderBy = "id";
        }

        searchSourceBuilder.fetchSource(true);
        if(null != order && null != orderBy){
            searchSourceBuilder.sort(new FieldSortBuilder(orderBy).order(SortOrder.fromString(order)).unmappedType(unmappedType));
        }
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        searchRequest.source(searchSourceBuilder);
        searchRequest.indices(index);
        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);
        Map<String, Object> searchMap = new HashMap<>();
        Long total = response.getHits().getTotalHits().value;
        List<Map<String, Object>> items = new ArrayList<>(response.getHits().getHits().length);
        Arrays.asList(response.getHits().getHits()).forEach(item -> {
            items.add(item.getSourceAsMap());
        });
        searchMap.put("total", total);
        searchMap.put("totalPage", Math.ceil(total/size));
        searchMap.put("from", from);
        searchMap.put("items", items);

        return searchMap;
    }

    /***
     * 更新文档
     * @param indexName
     * @param id
     * @param data
     * @return
     * @throws IOException
     */
    public Boolean updateDoc(String indexName, String id, Map<String, Object> data) throws IOException {

        UpdateRequest request = new UpdateRequest(indexName, "_doc", id);
        String json = JSONObject.toJSONString(data, SerializerFeature.WriteMapNullValue,
                SerializerFeature.DisableCircularReferenceDetect,
                SerializerFeature.WriteDateUseDateFormat);
        request.doc(json, XContentType.JSON);

        UpdateResponse updateResponse = client.update(
                request, RequestOptions.DEFAULT);

        return true;
    }

    /***
     * 搜索分页
     * @param request
     * @param from
     * @param size
     * @return
     */
    public Map<String, Object> search(SearchRequest request, Integer from, Integer size){
        Map<String, Object> searchMap = new HashMap<>();

        log.info("es search request: {}", request.source().toString());

        try {
            SearchResponse response = null;
            response = client.search(request, RequestOptions.DEFAULT);

            List<Map<String, Object>> items = new ArrayList<>();

            for (SearchHit his: response.getHits().getHits()) {
                items.add(his.getSourceAsMap());
            }

            Long total = response.getHits().getTotalHits().value;
            Double totalPage = Math.ceil(Double.valueOf(total)/size);

            searchMap.put("total", total);
            searchMap.put("totalPage", (int)Math.round(totalPage));
            searchMap.put("from", from);
            searchMap.put("items", items);
        } catch (IOException e) {
            log.error("es search error: {}", e.getMessage());
        }
        return searchMap;
    }

    /***
     * 根据索引名称和日期区间生成索引，这里要考虑多索引问题
     * @param name
     * @param dates
     * @return
     */
    public String formatIndex(String name, List<String> dates){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        String index = name + "-" + sdf.format(new Date());

        if(CollectionUtil.isNotEmpty(dates)){
            Date startDate = DateUtil.parse(dates.get(0));
            Integer startYear = DateUtil.year(startDate);

            Date endDate = DateUtil.parse(dates.get(1));
            Integer endYear = DateUtil.year(endDate);

            if(endYear == startYear){
                index = name + "-" + startYear;
            }else if(endYear > startYear){
                index = name + "-" + startYear;
                while (endYear > startYear){
                    startYear++;
                    index += "," + name + "-" + startYear;
                }
            }
        }

        return index;
    }

    /***
     * 构造 index名称 默认当年
     * @param name
     * @param date
     * @return
     */
    public String formatIndexByDate(String name, String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        String index = name + "-" + sdf.format(new Date());
        if(StrUtil.isNotBlank(date)){
            index = name + "-" + date.substring(0, 4);
        }
        return index;
    }
    /***
     * terms聚合，生成PI图
     * @param indexName
     * @param aggName
     * @param boolQueryBuilder
     * @param termsAggregationBuilder
     * @return
     */
    public List<Map<String, Object>> termsAggregation(String indexName, String aggName,
                                                      BoolQueryBuilder boolQueryBuilder,
                                                      TermsAggregationBuilder termsAggregationBuilder){
        List<Map<String, Object>> items = new ArrayList<>();
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(boolQueryBuilder);
        searchSourceBuilder.aggregation(termsAggregationBuilder).size(0);
        SearchRequest searchRequest = new SearchRequest(indexName);
        searchRequest.types("_doc");
        searchRequest.indices(indexName);
        searchRequest.source(searchSourceBuilder);

        log.info("index {}: {}",indexName,searchSourceBuilder.toString());

        try {
            SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
            Terms oneTerm = (Terms) searchResponse.getAggregations().asMap().get(aggName);
            log.info("terms aggregation: {}",oneTerm.getBuckets().size());
            for (Terms.Bucket bucket : oneTerm.getBuckets()) {
                Map<String, Object> ctsMap = new HashMap<>();
                ctsMap.put("key", bucket.getKeyAsString());
                ctsMap.put("count", bucket.getDocCount());
                items.add(ctsMap);
            }
        }catch (Exception ex){
            log.error("terms aggregation error: {}", ex.getMessage());
        }
        return items;
    }

    /***
     * 分组聚合直方图
     * @param indexName
     * @param aggName
     * @param subAggName
     * @param boolQueryBuilder
     * @param termsAggregationBuilder
     * @return
     */
    public List<Map<String, Object>> groupDateHistogramAggregation(String indexName,
                                                                   String aggName,
                                                                   String subAggName,
                                                                   BoolQueryBuilder boolQueryBuilder,
                                                                   AggregationBuilder termsAggregationBuilder){
        List<Map<String, Object>> items = new ArrayList<>();
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(boolQueryBuilder);
        searchSourceBuilder.aggregation(termsAggregationBuilder).size(0);
        SearchRequest searchRequest = new SearchRequest(indexName);
        searchRequest.types("_doc");
        searchRequest.indices(indexName);
        searchRequest.source(searchSourceBuilder);

        log.info("index {}: {}",indexName,searchSourceBuilder.toString());
        try {

            SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
            Aggregation agg = searchResponse.getAggregations().get(aggName);
            List<? extends Histogram.Bucket> buckets = ((Histogram) agg).getBuckets();
            for (Histogram.Bucket bucket : buckets) {

                Terms perCtsCountTerm = (Terms) bucket.getAggregations().asMap().get(subAggName);

                for (Terms.Bucket twoTermBucket : perCtsCountTerm.getBuckets()) {

                    Map<String, Object> ctsMap = new HashMap<>();
                    ctsMap.put("key", bucket.getKeyAsString());
                    ctsMap.put("count", bucket.getDocCount());
                    ctsMap.put("groupKey", twoTermBucket.getKeyAsString());
                    ctsMap.put("groupCount", twoTermBucket.getDocCount());
                    if(twoTermBucket.getKeyAsString().indexOf("-")>-1){
                        String[] keys = twoTermBucket.getKeyAsString().split("-");
                        ctsMap.put("subGroupName1", keys[0]);
                        if(keys.length== 2){
                            ctsMap.put("subGroupName2", keys[1]);
                        }
                    }
                    items.add(ctsMap);
                }
            }
        }catch (Exception ex){
            log.error("terms aggregation error: {}", JSONObject.toJSONString(ex));
        }
        return items;
    }

    /****
     * 日期直方图
     * @param indexName
     * @param aggName
     * @param boolQueryBuilder
     * @param termsAggregationBuilder
     * @return
     */
    public List<Map<String, Object>> dateHistogramAggregation(String indexName,
                                                                   String aggName,
                                                                   BoolQueryBuilder boolQueryBuilder,
                                                                   AggregationBuilder termsAggregationBuilder){
        List<Map<String, Object>> items = new ArrayList<>();
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(boolQueryBuilder);
        searchSourceBuilder.aggregation(termsAggregationBuilder).size(0);
        SearchRequest searchRequest = new SearchRequest(indexName);
        searchRequest.types("_doc");
        searchRequest.indices(indexName);
        searchRequest.source(searchSourceBuilder);

        try {
            SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
            Aggregation agg = searchResponse.getAggregations().get(aggName);
            List<? extends Histogram.Bucket> buckets = ((Histogram) agg).getBuckets();
            for (Histogram.Bucket bucket : buckets) {
                Map<String, Object> ctsMap = new HashMap<>();
                ctsMap.put("key", bucket.getKeyAsString());
                ctsMap.put("count", bucket.getDocCount());
                items.add(ctsMap);
            }
        }catch (Exception ex){
            log.error("terms aggregation error: {}", ex.getMessage());
        }
        return items;
    }

    /***
     * 根据条件去查询某个文档
     * @param index
     * @param map
     * @return
     * @throws IOException
     */
    public Map<String, Object> queryOneDoc(String index, Map<String, Object> map) throws IOException {
        SearchRequest searchRequest = new SearchRequest(index);
        searchRequest.types("_doc");
        searchRequest.indices(index);

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            boolQueryBuilder.must(QueryBuilders.termQuery(entry.getKey(), entry.getValue()));
        }

        searchSourceBuilder.query(boolQueryBuilder);
        searchSourceBuilder.from(0);
        searchSourceBuilder.size(1);

        searchSourceBuilder.fetchSource(true);

        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        searchRequest.source(searchSourceBuilder);
        searchRequest.indices(index);
        log.info("index {}: {}",index,searchSourceBuilder.toString());
        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);

        final Map<String, Object>[] searchMap = new Map[]{new HashMap<>()};

        Arrays.asList(response.getHits().getHits()).forEach(item -> {
            searchMap[0] = item.getSourceAsMap();
        });
        return searchMap[0];
    }

    /***
     * 根据特定条件搜索文档
     * @param index
     * @param map
     * @param orderBy
     * @param order
     * @param unmappedType
     * @return
     * @throws IOException
     */
    public List<Map<String, Object>> queryDoc(String index,
                                              Map<String, Object> map,
                                              String orderBy,
                                              String order,
                                              String unmappedType) throws IOException {
        SearchRequest searchRequest = new SearchRequest(index);
        searchRequest.types("_doc");
        searchRequest.indices(index);

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();

        for (Map.Entry<String, Object> entry : map.entrySet()) {
            if(entry.getValue() instanceof Collection){

                Iterator iter = ((Collection<?>) entry.getValue()).iterator();

                while(iter.hasNext()){
                    Object theme = (Object)iter.next();
                    TermQueryBuilder vehTypeQuery = QueryBuilders.termQuery(entry.getKey(),  theme);
                    boolQueryBuilder.should(vehTypeQuery);
                }
                boolQueryBuilder.minimumShouldMatch(1);
            }else{
                boolQueryBuilder.must(QueryBuilders.termQuery(entry.getKey(), entry.getValue()));
            }
        }



        searchSourceBuilder.query(boolQueryBuilder);

        searchSourceBuilder.from(0);
        if(null != order && null != orderBy){
            searchSourceBuilder.sort(new FieldSortBuilder(orderBy).order(SortOrder.fromString(order)).unmappedType(unmappedType));
        }
        log.info("index query {} : {}", index, searchSourceBuilder.toString());
        searchSourceBuilder.fetchSource(true);

        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        searchSourceBuilder.size(1000);

        searchRequest.source(searchSourceBuilder);
        searchRequest.indices(index);
        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);

        List<Map<String, Object>> items = new ArrayList<>(response.getHits().getHits().length);

        Arrays.asList(response.getHits().getHits()).forEach(item -> {
            items.add(item.getSourceAsMap());
        });
        return items;
    }

    /***
     * 根据前缀搜索
     * @param index
     * @param field
     * @param value
     * @param source
     * @param orderBy
     * @param order
     * @param unmappedType
     * @param size
     * @return
     * @throws IOException
     */
    public List<Map<String, Object>> queryDocByPrefix(String index,
                                              String field,
                                              String value,
                                              List<String> source,
                                              String orderBy,
                                              String order,
                                              String unmappedType,
                                              Integer size) throws IOException {
        List<Map<String, Object>> items = new ArrayList<>();

        SearchRequest searchRequest = new SearchRequest(index);
        searchRequest.types("_doc");
        searchRequest.indices(index);

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        String[] includes = new String[source.size()];
        source.toArray(includes);
        String[] excludes = Strings.EMPTY_ARRAY;

        FetchSourceContext fetchSourceContext = new FetchSourceContext(true, includes, excludes);

        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        PrefixQueryBuilder vinQuery = QueryBuilders.prefixQuery(field, value);
        boolQueryBuilder.must(vinQuery);

        searchSourceBuilder.query(boolQueryBuilder);
        searchSourceBuilder.from(0);
        searchSourceBuilder.size(size);
        if(null != order && null != orderBy){
            searchSourceBuilder.sort(new FieldSortBuilder(orderBy).order(SortOrder.fromString(order)).unmappedType(unmappedType));
        }

        searchSourceBuilder.fetchSource(fetchSourceContext);
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        searchRequest.source(searchSourceBuilder);
        searchRequest.indices(index);
        try{

            SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);
            Arrays.asList(response.getHits().getHits()).forEach(item -> {
                items.add(item.getSourceAsMap());
            });
        }catch (Exception ex){
            log.error("queryDocByPrefix error :{}", ex.getMessage());
        }

        return items;
    }

    /***
     * 根据某个字段的前缀做统计
     * @param index
     * @param field
     * @param value
     * @param size
     * @return
     * @throws IOException
     */
    public List<Map<String, Object>> queryTopDocByPrefix(String index, String field, String value, Integer size) throws IOException {

        List<Map<String, Object>> items = new ArrayList<>();
        SearchRequest searchRequest = new SearchRequest(index);
        searchRequest.types("_doc");
        searchRequest.indices(index);

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        PrefixQueryBuilder query = QueryBuilders.prefixQuery(field, value);
        boolQueryBuilder.must(query);
        searchSourceBuilder.query(boolQueryBuilder);
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        searchRequest.source(searchSourceBuilder);
        searchRequest.indices(index);

        String aggName = "fieldAggs";

        searchSourceBuilder.query(boolQueryBuilder);

        TermsAggregationBuilder termAgg = AggregationBuilders.terms(aggName)
                .field(field)
                .minDocCount(1)
                .size(size);

        searchSourceBuilder.aggregation(termAgg).size(0);
        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);
        Terms oneTerm = (Terms) response.getAggregations().asMap().get(aggName);
        log.info("terms aggregation: {}",oneTerm.getBuckets().size());
        for (Terms.Bucket bucket : oneTerm.getBuckets()) {
            Map<String, Object> ctsMap = new HashMap<>();
            ctsMap.put("key", bucket.getKeyAsString());
            ctsMap.put("count", bucket.getDocCount());
            items.add(ctsMap);
        }

        return items;
    }

    /***
     * 根据关键词解析完全提示
     * @param index
     * @param field
     * @param keyword
     * @param source
     * @param size
     * @return
     * @throws IOException
     */
    public List<Map<String, Object>> completionSuggest(String index, String field, String keyword, List<String> source, Integer size) throws IOException {
        List<Map<String, Object>> items = new ArrayList<>();

        SearchRequest searchRequest = new SearchRequest(index);
        searchRequest.types("_doc");
        searchRequest.indices(index);

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        String[] includes = new String[source.size()];
        source.toArray(includes);
        String[] excludes = Strings.EMPTY_ARRAY;

        FetchSourceContext fetchSourceContext = new FetchSourceContext(true, includes, excludes);

        CompletionSuggestionBuilder completionSuggestionBuilder = SuggestBuilders.completionSuggestion(field)
                .prefix(keyword).skipDuplicates(true)
                .size(size);

        String suggestionName = field;
        SuggestBuilder suggestBuilder = new SuggestBuilder();
        suggestBuilder.addSuggestion(suggestionName, completionSuggestionBuilder);

        searchSourceBuilder.suggest(suggestBuilder);
        searchSourceBuilder.fetchSource(fetchSourceContext);
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        searchRequest.source(searchSourceBuilder);
        searchRequest.indices(index);

        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);

        CompletionSuggestion completionSuggestion = response.getSuggest().getSuggestion(suggestionName);

        for (CompletionSuggestion.Entry entry : completionSuggestion.getEntries()) {
            for (CompletionSuggestion.Entry.Option option : entry) {
                items.add(option.getHit().getSourceAsMap());
            }
        }
        return items;
    }


    /***
     * 利用Scroll方式搜索
     * @param indexName
     * @param sourceBuilder
     * @param minutes
     * @param scrollId
     * @return
     */
    public Map<String, Object> scrollSearch(String indexName, SearchSourceBuilder sourceBuilder, Integer minutes, String scrollId){
        Map<String, Object> result = new HashMap<>();

        SearchResponse searchResponse = null;
        SearchRequest searchRequest = new SearchRequest(indexName);
        searchRequest.source(sourceBuilder);

        // 设置滚动查询过期时间 5分钟
        Scroll scroll = new Scroll(TimeValue.timeValueMinutes(minutes));
        searchRequest.scroll(scroll);
        // scrollId循环获取结果
        if(StrUtil.isNotBlank(scrollId)){
            SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
            scrollRequest.scroll(scroll);
            try {
                searchResponse = client.scroll(scrollRequest, RequestOptions.DEFAULT);
            } catch (IOException e) {
                log.error("es scrollSearch exception: {}", e.getMessage());
            }

        }else{
            try {
                // 查询es信息
                searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
            } catch (IOException e) {
                log.error("es scrollSearch exception: {}", e.getMessage());
            }
        }

        List<Map<String, Object>> items = this.buildResponse(searchResponse);

        Long total = searchResponse.getHits().getTotalHits().value;
        result.put("total", total);

        if(total>0){
            result.put("totalPage",   (int)Math.ceil(total/sourceBuilder.size()));
        }else{
            result.put("totalPage",  0);
        }
        result.put("items", items);
        result.put("scrollId", searchResponse.getScrollId());
        result.put("timestamp", System.currentTimeMillis());

        return result;
    }


    /***
     * 判断返回结果
     * @param searchResponse
     * @return
     */
    private boolean searchResponseIsNotNull(SearchResponse searchResponse) {
        return !Objects.isNull(searchResponse)
                && !Objects.isNull(searchResponse.getHits())
                && !Objects.isNull(searchResponse.getHits().getHits())
                && searchResponse.getHits().getHits().length > 0
                && searchResponse.getHits().getTotalHits().value > 0;
    }

    /***
     * 构造返回值
     * @param searchResponse
     * @return
     */
    private List<Map<String, Object>> buildResponse(SearchResponse searchResponse) {
        List<Map<String, Object>> items = new ArrayList<>();

        if (searchResponseIsNotNull(searchResponse)) {
            SearchHit[] hits = searchResponse.getHits().getHits();
            Arrays.asList(hits).forEach(item -> {
                items.add(item.getSourceAsMap());
            });
        }

        return items;
    }

    /***
     * 关闭 Scroll
     * @param scrollId
     */
    public void clearScroll(String scrollId) {
        // 清除 Scroll 连接
        ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
        clearScrollRequest.addScrollId(scrollId);
        try {
            client.clearScroll(clearScrollRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.error("ES clear Scroll exception: {}", e.getMessage());
        }
    }


    /***
     * search_after 搜索
     * @param indexName
     * @param sourceBuilder
     * @param searchAfterList
     * @return
     */
    public Map<String, Object> searchAfter(String indexName, SearchSourceBuilder sourceBuilder,  List<Object> searchAfterList){

        Map<String, Object> result = new HashMap<>();
        if(null !=searchAfterList && searchAfterList.size()>0){
            Object[] arr = searchAfterList.stream().toArray(String[] ::new);
            sourceBuilder.searchAfter(arr);
        }
        SearchRequest searchRequest = new SearchRequest(indexName);
        searchRequest.indices(indexName);
        searchRequest.types("_doc");
        searchRequest.source(sourceBuilder);

        log.info("index {}: {}",indexName,sourceBuilder.toString());

        List<Map<String, Object>> items = new ArrayList<>();
        Object[] nextSearchAfter =  new Object[searchAfterList.size()];

        try {
            SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);
            if(searchResponseIsNotNull(response)){
                Long total = response.getHits().getTotalHits().value;
                result.put("total", total);
                result.put("totalPage",  (int)Math.ceil(total/sourceBuilder.size()));
                SearchHit[] hits = response.getHits().getHits();
                List<SearchHit> hitList = Arrays.asList(hits);
                for(SearchHit item: hitList){
                    nextSearchAfter =  item.getSortValues();
                    items.add(item.getSourceAsMap());
                }
            }else{
                result.put("total", 0);
                result.put("totalPage",  0);
            }
        } catch (IOException e) {
            log.error("ES searchAfter error:{}", e.getMessage());
        }
        result.put("searchAfter", nextSearchAfter);
        result.put("items", items);
        return result;
    }

    /***
     * 通过文档_id查询某个文档
     * @param indexName
     * @param id
     * @return
     * @throws IOException
     */
    public Map<String, Object> queryDocById(String indexName, String id) throws IOException {
        Map<String, Object> doc = null;
        GetRequest getRequest = new GetRequest(indexName, "_doc", id);
        GetResponse getResponse = client.get(getRequest, RequestOptions.DEFAULT);

        if (getResponse.isExists()) {
            doc = getResponse.getSourceAsMap();
        }
        return doc;
    }


    /***
     * 根据文档ID删除某个文档 同步删除
     * @param indexName
     * @param id
     * @return
     */
    public void deleteDocById(String indexName, String id) throws IOException {
        DeleteRequest request = new DeleteRequest(indexName, "_doc", id);
        request.timeout("2m");
        DeleteResponse deleteResponse = client.delete(request, RequestOptions.DEFAULT);
        log.info("deleteDocById {}: {}",indexName,deleteResponse.status());
    }

    /***
     * 根据Map条件删除文档
     * @param indexName
     * @param map
     * @throws IOException
     */
    public void deleteDocByQuery(String indexName, Map<String, Object> map) throws IOException {

        DeleteByQueryRequest request = new DeleteByQueryRequest(indexName);

        request.setDocTypes("_doc");

        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();

        for (Map.Entry<String, Object> entry : map.entrySet()) {
            if(entry.getValue() instanceof Collection){
                Iterator iter = ((Collection<?>) entry.getValue()).iterator();
                while(iter.hasNext()){
                    Object theme = (Object)iter.next();
                    TermQueryBuilder vehTypeQuery = QueryBuilders.termQuery(entry.getKey(),  theme);
                    boolQueryBuilder.should(vehTypeQuery);
                }
                boolQueryBuilder.minimumShouldMatch(1);
            }else{
                boolQueryBuilder.must(QueryBuilders.termQuery(entry.getKey(), entry.getValue()));
            }
        }

        log.info("deleteDocByQuery: {}", boolQueryBuilder.toString());
        request.setQuery(boolQueryBuilder);
        BulkByScrollResponse resp = client.deleteByQuery(request, RequestOptions.DEFAULT);

        log.info("deleteDocByQuery: {}",resp.getDeleted());
    }
}
