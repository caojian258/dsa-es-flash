package org.dsa.modules.onlineUpdate.service.impl;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.ReUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.PageUtils;
import org.dsa.modules.onlineUpdate.dao.SoftwareDao;
import org.dsa.modules.onlineUpdate.dao.SoftwareVersionDao;
import org.dsa.modules.onlineUpdate.entity.SoftwareEntity;
import org.dsa.modules.onlineUpdate.service.RuleService;
import org.dsa.modules.onlineUpdate.service.SoftwareService;
import org.dsa.modules.onlineUpdate.utils.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;


@Service("SoftwareService")
public class SoftwareServiceImpl extends ServiceImpl<SoftwareDao, SoftwareEntity> implements SoftwareService {

    @Autowired
    private RuleService ruleService;

    @Autowired
    private SoftwareVersionDao softwareVersionDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String name = MapUtil.getStr(params,"name").trim();
        Integer status = MapUtil.getInt(params,"status");
        IPage<SoftwareEntity> page = this.page(
                new Query<SoftwareEntity>().getPage(params),
                new QueryWrapper<SoftwareEntity>()
                        .eq(ObjectUtil.isNotNull(status),"status",status)
                        .apply(StringUtils.isNotBlank(name),"lower(name) like CONCAT(CONCAT('%',{0},'%')) ",name.toLowerCase())
                        .orderByDesc("created_at")
        );
        page.getRecords().forEach(softwareEntity -> {
            softwareEntity.setIsRelease(softwareVersionDao.isRelease(softwareEntity.getId()));
        });
        return new PageUtils(page);
    }

    @Transactional
    @Override
    public void saveOrUpdate2(SoftwareEntity entity) {
        //参数校验
        checkParam(entity);
        //设置备注 key name_code
        if (ObjectUtil.isNull(entity.getId())){
            //新增
//            String key = entity.getName() +"_"+ entity.getCode();
//            entity.setDescriptionTi(key);
            this.baseMapper.insert(entity);
        }else{
            //修改
            if (Constant.STATUS.DISABLED.getCode().equals(entity.getStatus())){
                ruleService.disabledRule(entity.getId(),1);
            }else{
                ruleService.disabledRule(entity.getId(),0);
            }
            this.baseMapper.updateById(entity);
        }
    }

    @Override
    public void disabledById(Integer id) {
        SoftwareEntity software = this.baseMapper.selectById(id);
        if (ObjectUtil.isNull(software)){
            throw new RRException(Constant.Msg.DATA_INFO_NULL_ERROR);
        }
        software.setStatus(Objects.equals(software.getStatus(),0)?1:0);
        // 状态修改为禁用时，禁用所有规则
        if (Constant.STATUS.DISABLED.getCode().equals(software.getStatus())){
            ruleService.disabledRule(software.getId(),1);
        }else{
            ruleService.disabledRule(software.getId(),0);
        }
        this.baseMapper.updateById(software);
    }

    @Override
    public List<HashMap> queryAll() {
        return this.baseMapper.selectList(new QueryWrapper<SoftwareEntity>().eq("status", 0)).stream().map(c -> new HashMap() {{
            put("label", c.getName());
            put("value", c.getId());
        }}).collect(Collectors.toList());
    }

    private void checkParam(SoftwareEntity entity) {
        if (ObjectUtil.isNull(entity.getStatus())){
            throw new RRException(Constant.Msg.CHECK_STATUS_ERROR);
        }
        if (StringUtils.isBlank(entity.getName().trim())){
            throw new RRException(Constant.Msg.CHECK_NAME_ERROR);
        }
        // 名称重复性校验
        SoftwareEntity software = this.baseMapper.selectOne(new QueryWrapper<SoftwareEntity>()
                .eq("name", entity.getName()));
        if (software!=null && !software.getId().equals(entity.getId())) {
            throw new RRException(Constant.Msg.NAME_DUPLICATION_ERROR);
        }
        // code重复性校验
        SoftwareEntity software2 = this.baseMapper.selectOne(new QueryWrapper<SoftwareEntity>()
                .eq("code", entity.getCode()));
        boolean match = ReUtil.isMatch("^[1-9]\\d{2}$", String.valueOf(entity.getCode()));
        if (!match){
            throw new RRException(Constant.Msg.CHECK_CODE_ERROR);
        }
        if (software2!=null && !software2.getId().equals(entity.getId())) {
            throw new RRException(Constant.Msg.CODE_DUPLICATION_ERROR);
        }
        // 校验状态类型
        if (ObjectUtil.isNotNull(entity.getStatus())){
            if (!Arrays.asList(Constant.STATUS_CHECK_ARR).contains(entity.getStatus())){
                throw new RRException(Constant.Msg.UNKNOWN_STATUS_ERROR);
            }
        }
    }
}