package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.entity.FutureComponentInfo;

import java.util.List;

@Mapper
public interface FutureComponentInfoMapper extends BaseMapper<FutureComponentInfo> {

    public FutureComponentInfo queryComponentInfo(@Param("id") Integer id, @Param("versionNumber") Integer versionNumber, @Param("tag") String tag);

    public List<FutureComponentInfo> queryComponentInfoByTag(@Param("deviceId") String deviceId, @Param("tag") String tag, @Param("idList") List<Integer> idList);

}
