package org.dsa.modules.reporter.util;

import clojure.lang.Obj;
import org.dsa.common.utils.Md5Utils;
import org.dsa.modules.reporter.entity.LdapUser;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import javax.naming.ldap.Control;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public class LdapUtil {


    public static void main(String[] args) throws NamingException, UnsupportedEncodingException, NoSuchAlgorithmException {
        String url = "ldap://localhost:389/";
        String basedn = "dc=example,dc=com";  // basedn
        String root = "cn=admin,dc=example,dc=com";  // 用户
        String pwd ="123456";  // pwd

        LdapContext ctx = ldapConnect(url,root,pwd);//集团 ldap认证

        //ou
        List<Map<String, Object>> ous =  queryEntry(ctx, basedn, "(&(objectClass=organizationalUnit))");

        System.out.println(ous);

        //or
        List<Map<String, Object>> ors =  queryEntry(ctx, basedn, "(&(objectClass=organizationalRole))");

        System.out.println(ors);

//        addPosixGroup(ctx, "ou=product,"+basedn, "new_group", "506");

        //pg
        List<Map<String, Object>> pgs =  queryEntry(ctx, basedn, "(&(objectClass=posixGroup))");

        System.out.println(pgs);

//        delete("cn=wang wu,cn=zhang san,cn=tonny cao,cn=group1,ou=product,dc=example,dc=com", ctx);

        //iop
        List<Map<String, Object>> iops =  queryEntry(ctx, basedn, "(&(objectClass=inetOrgPerson))");

        System.out.println(iops);


//        List<LdapUser> jtlm = readLdap(ctx, basedn);//获取集团ldap中用户信息
//
//        for(int i=0;i<jtlm.size();i++) {
//            LdapUser jtu = jtlm.get(i);
//
//            System.out.println("uid:"+jtu.getUid());
//            System.out.println("UserPassword:"+jtu.getUserPassword());
//            System.out.println("uidNum:"+jtu.getUidNumber());
//            System.out.println("gidNum:"+jtu.getGidNumber());
//            System.out.println("Cn:"+jtu.getCn());
//            System.out.println("Sn:"+jtu.getSn());
//            System.out.println("status:"+jtu.getStatus());
//        }
                //登录还有问题
//            addPerson(ctx, "506", "tonny123", "cn=new-group,ou=product,dc=example,dc=com", "pwd");

//        deleteUserToGroup(ctx, "cn=new-group,ou=product,dc=example,dc=com", new String[]{"czj123"});""
        updatePersonPwd(ctx, "cn=czj123,cn=new-group,ou=product,dc=example,dc=com", "czj123");
        if(ctx!=null){
            ctx.close();
        }

    }

    public static boolean updatePersonPwd(LdapContext ctx, String dn, String newPwd) {
        try {
            ModificationItem[] mods = new ModificationItem[1];
            /*修改属性*/
            Attribute attr0 = new BasicAttribute("userPassword", "{md5}"+ldapEncoderByMd5(newPwd));
            mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, attr0);
            ctx.modifyAttributes(dn, mods);
            return true;
        } catch (NamingException ne) {
            System.out.println("修改失败");
            ne.printStackTrace();
            return false;
        } catch (UnsupportedEncodingException e) {
            return false;
        } catch (NoSuchAlgorithmException e) {
            return false;
        }

    }

    //还是有问题
    public static boolean deleteUserToGroup(LdapContext ctx, String groupName,String [] ids) {
//        StringBuffer sb = new StringBuffer();
//        sb.append("cn=").append(groupName);

        try {
            ModificationItem[] mods = new ModificationItem[1];
                Attribute memberUid = new BasicAttribute("memberUid");
                if (ids != null && ids.length > 0) {
                    for (String id : ids) {
                        memberUid.add(id);
                    }
                }
                mods[0] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, memberUid);
                ctx.modifyAttributes(groupName, mods);
                return true;
            } catch (NamingException e) {
                System.out.println(e.getMessage());
                return false;
            }

    }
    public static List<Map<String, Object>> queryEntry(LdapContext ctx, String basedn, String filter) throws NamingException {
//        String filter = "(&(objectClass=*)(cn=*))";
        List<Map<String, Object>> results = new ArrayList<>();

        SearchControls searchControls = new SearchControls();//搜索控件
        //1.要搜索的上下文或对象的名称；2.过滤条件，可为null，默认搜索所有信息；3.搜索控件，可为null，使用默认的搜索控件
        searchControls.setSearchScope(2);//搜索范围
        NamingEnumeration<SearchResult> answer = ctx.search(basedn, filter.toString(),searchControls);
        while (answer.hasMore()) {
            SearchResult result = (SearchResult) answer.next();
            Map<String, Object> hashMap = new HashMap<>();
            NamingEnumeration<? extends Attribute> attrs = result.getAttributes().getAll();

            hashMap.put("name", result.getName());
            hashMap.put("nameInNamespace", result.getNameInNamespace());

            while (attrs.hasMore()) {
                Attribute attr = (Attribute) attrs.next();
                hashMap.put(attr.getID(), attr.get().toString());
            }
            results.add(hashMap);
        }
        return results;
    }

    public static boolean addEntry(LdapContext ctx, String entryDN, BasicAttributes attributes) throws NamingException {
        ctx.createSubcontext(entryDN, attributes);
        return true;
    }

    public static boolean addPosixGroup(LdapContext ctx, String entryDN, String name, String gidNum) throws NamingException {
        BasicAttributes attrs = new BasicAttributes();
        BasicAttribute objclassSet = new BasicAttribute("objectClass");
        objclassSet.add("posixGroup");
        objclassSet.add("top");
        attrs.put(objclassSet);
        attrs.put("cn", name);
        attrs.put("gidNumber", gidNum);
        String dn = "cn="+name+","+entryDN;
        System.out.println(dn);
        addEntry(ctx, dn, attrs);
        return true;
    }

    public static boolean addPerson(LdapContext ctx, String gidNum, String name, String entryDN, String pwd) throws NamingException, UnsupportedEncodingException, NoSuchAlgorithmException {
        BasicAttributes attrs = new BasicAttributes();
        //要加入的组
        BasicAttribute objclassSet = new BasicAttribute("objectClass");
        objclassSet.add("posixGroup");
        objclassSet.add("top");
        attrs.put(objclassSet);

        attrs.put("cn", name);//显示账号
        attrs.put("userPassword", "{md5}"+ldapEncoderByMd5(pwd));//显示
        attrs.put("gidNumber", gidNum);/*显示组id */
        attrs.put("memberUid", name);//显示账号
        String dn = "cn="+name+","+entryDN;
        System.out.println(dn);
        addEntry(ctx, dn, attrs);
        return true;
    }
    public static boolean addGoups(LdapUser lu, LdapContext ctx) {
        BasicAttributes attrsbu = new BasicAttributes();
        BasicAttribute objclassSet = new BasicAttribute("objectClass");
        objclassSet.add("posixGroup");
        objclassSet.add("top");
        attrsbu.put(objclassSet);
        attrsbu.put("cn", lu.getCn());//显示账号
        attrsbu.put("userPassword", "{crypt}x");//显示

        attrsbu.put("gidNumber",lu.getGidNumber());/*显示组id */
        attrsbu.put("memberUid", lu.getCn());//显示账号
        try {
            String cn="cn="+lu.getCn()+",ou=Group,dc=tcjf,dc=com";
            System.out.println(cn);
            ctx.createSubcontext(cn, attrsbu);
            System.out.println("添加用户group成功");
            return true;
        } catch (Exception e) {
            System.out.println("添加用户group失败");
            e.printStackTrace();
            return false;
        }
    }



    /**
     * 添加用户
     * @param lu
     * @param ctx
     * @return
     */
    public static boolean addUser(LdapUser lu,LdapContext ctx) {
        BasicAttributes attrsbu = new BasicAttributes();
        BasicAttribute objclassSet = new BasicAttribute("objectClass");
        // objclassSet.add("account");
        objclassSet.add("posixAccount");
        objclassSet.add("inetOrgPerson");
        objclassSet.add("top");
        objclassSet.add("shadowAccount");
        attrsbu.put(objclassSet);
        attrsbu.put("uid",  lu.getUid());//显示账号
        attrsbu.put("sn", lu.getSn());//显示姓名
        attrsbu.put("cn", lu.getCn());//显示账号
        attrsbu.put("gecos", lu.getCn());//显示账号
        attrsbu.put("userPassword", lu.getUserPassword());//显示密码
        attrsbu.put("displayName", lu.getDisplayName());//显示描述
        attrsbu.put("mail", lu.getMail());//显示邮箱
        attrsbu.put("homeDirectory", "/home/" + lu.getCn());//显示home地址
        attrsbu.put("loginShell", "/bin/bash");//显示shell方式
        attrsbu.put("uidNumber", lu.getUidNumber());/*显示id */
        attrsbu.put("gidNumber", lu.getGidNumber());/*显示组id */

        try {
            String dn="uid="+lu.getCn()+",ou=People,dc=tcjf,dc=com";
            System.out.println(dn);
            ctx.createSubcontext(dn, attrsbu);
            System.out.println("添加用户成功");
            return true;
        } catch (Exception e) {
            System.out.println("添加用户失败");
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 修改属性
     * @param lu
     * @param ctx
     * @return
     */
    public static boolean modifyInformation(LdapUser lu,LdapContext ctx) {
        try {
            ModificationItem[] mods = new ModificationItem[1];
            String dn="uid="+lu.getCn()+",ou=People,dc=tcjf,dc=com";
            /*添加属性*/
            //  Attribute attr0 = new BasicAttribute("description", "测试");
            //  mods[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE,attr0);

            /*修改属性*/
            Attribute attr0 = new BasicAttribute("userPassword", lu.getUserPassword());
            mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, attr0);

            /*删除属性*/
            //  Attribute attr0 = new BasicAttribute("description", "测试");
            //  mods[0] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, attr0);
            ctx.modifyAttributes(dn, mods);
            System.out.println("修改成功");
            return true;
        } catch (NamingException ne) {
            System.out.println("修改失败");
            ne.printStackTrace();
            return false;
        }

    }

    public  static boolean deleteEntity(String dn, LdapContext ctx) {
        try {
            ctx.destroySubcontext(dn);
            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    /**
     * 删除
     * @param dn
     * @param ctx
     * @return
     */
    public  static boolean delete(String dn, LdapContext ctx) {
        try {
            ctx.destroySubcontext(dn);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    public static LdapContext ldapConnect(String url,String root,String pwd){
        String factory = "com.sun.jndi.ldap.LdapCtxFactory";
        String simple = "simple";
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY,factory);
        env.put(Context.PROVIDER_URL, url);
        env.put(Context.SECURITY_AUTHENTICATION, simple);
        env.put(Context.SECURITY_PRINCIPAL, root);
        env.put(Context.SECURITY_CREDENTIALS, pwd);
        LdapContext ctx = null;
        Control[] connCtls = null;
        try {
            ctx = new InitialLdapContext(env, connCtls);
            System.out.println( "认证成功:"+url);
        }catch (javax.naming.AuthenticationException e) {
            System.out.println("认证失败：");
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println("认证出错：");
            e.printStackTrace();
        }
        return ctx;
    }

    public static List<LdapUser> readLdap(LdapContext ctx, String basedn){

        List<LdapUser> lm=new ArrayList<LdapUser>();
        try {
            if(ctx!=null){
                //过滤条件
//                String filter = "(&(objectClass=*)(uid=*))";
                String filter = "(&(objectClass=*)(cn=*))";
                String[] attrPersonArray = { "uid", "userPassword", "gidNumber","uidNumber","displayName", "cn", "sn", "mail", "description" };
                SearchControls searchControls = new SearchControls();//搜索控件
                searchControls.setSearchScope(2);//搜索范围

//                searchControls.setReturningAttributes(attrPersonArray);
                //1.要搜索的上下文或对象的名称；2.过滤条件，可为null，默认搜索所有信息；3.搜索控件，可为null，使用默认的搜索控件
                NamingEnumeration<SearchResult> answer = ctx.search(basedn, filter.toString(),searchControls);

                while (answer.hasMore()) {
                    SearchResult result = (SearchResult) answer.next();
                    NamingEnumeration<? extends Attribute> attrs = result.getAttributes().getAll();
                    LdapUser lu = new LdapUser();
                    while (attrs.hasMore()) {
                        Attribute attr = (Attribute) attrs.next();
                        if("userPassword".equals(attr.getID())){
                            Object value = attr.get();
                            lu.setUserPassword(new String((byte [])value));
                        }else if("uid".equals(attr.getID())){
                            lu.setUid(attr.get().toString());
                        }else if("displayName".equals(attr.getID())){
                            lu.setDisplayName(attr.get().toString());
                        }else if("cn".equals(attr.getID())){
                            lu.setCn(attr.get().toString());
                        }else if("sn".equals(attr.getID())){
                            lu.setSn(attr.get().toString());
                        }else if("mail".equals(attr.getID())){
                            lu.setMail(attr.get().toString());
                        }else if("description".equals(attr.getID())){
                            lu.setDescription(attr.get().toString());
                        }else if("gidNumber".equals(attr.getID())){
                            lu.setGidNumber(attr.get().toString());
                        }else if("uidNumber".equals(attr.getID())){
                            lu.setUidNumber(attr.get().toString());
                        }else if("status".equals(attr.getID())){
                            lu.setStatus(attr.get().toString());
                        }
                    }
                    System.out.println("ssss:"+lu.getUid());
                    if(lu.getCn()!=null)
                        lm.add(lu);

                }
            }
        }catch (Exception e) {
            System.out.println("获取用户信息异常:");
            e.printStackTrace();
        }

        return lm;
    }

    public static String strToint(String m){
        if(m==null || "".equals(m))
            return "-1";
        char [] a=m.toCharArray();
        StringBuffer sbu = new StringBuffer();
        for(char c:a)
            sbu.append((int)c);
        System.out.println(sbu.toString());
        return sbu.toString();
    }

    /**
     * 获取时间戳
     * @return
     */
    public static long getNowTimeStamp() {
        long time = System.currentTimeMillis();
        time =  time / 1000;
        return time;
    }

    private static String ldapEncoderByMd5(String psw) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        byte[] byteArray = null;
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        md5.reset();
        md5.update(psw.getBytes("utf-8"));
        byteArray = md5.digest();
        String md5pwd = Base64.getEncoder().encodeToString(byteArray);
        return md5pwd;
    }
}

