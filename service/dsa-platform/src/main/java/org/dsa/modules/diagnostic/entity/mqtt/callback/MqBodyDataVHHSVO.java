package org.dsa.modules.diagnostic.entity.mqtt.callback;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * mq body data VHHS实体类
 * 
 */
@Data
public class MqBodyDataVHHSVO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * ECU名称数组
	 */
	private String ecuName;
	/**
	 * 在线状态
	 */
	private int onlineStatus;
	/**
	 * 故障码个数
	 */
	private int dtcNumber;
	/**
	 * 故障码列表
	 */
	private List<MqBodyDataDTCVO> dtcList;


}
