package org.dsa.modules.remoteDiagnostic.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.modules.remoteDiagnostic.pojo.PageParam;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class VinCodePageVo extends PageParam {


    private String vin;

    private String key;

}
