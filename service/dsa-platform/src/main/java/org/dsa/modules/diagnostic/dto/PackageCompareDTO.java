package org.dsa.modules.diagnostic.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author weishunxin
 * @since 2023-06-01
 */
@Setter
@Getter
public class PackageCompareDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 诊断数据包下载地址
     */
    private String url = "";

    /**
     * 数据包签名
     */
    private String md5 = "";

    /**
     * 数据包大小
     */
    private Long packageFileSize = 0L;

    /**
     * 诊断数据包文件名
     */
    private String packageName = "";

    /**
     * 诊断数据总版本号
     */
    private String diagPackageVersion = "";

    /**
     * ODX包文件名
     */
    private String pdxPackage = "";

    /**
     * ODX版本号
     */
    private String pdxVersion = "";

    /**
     * OTX包文件名
     */
    private String ptxPackage = "";

    /**
     * OTX版本号
     */
    private String ptxVersion = "";

    /**
     * odx i18n数据库文件名
     */
    private String i18nOdx = "";

    /**
     * otx i18n数据库文件名
     */
    private String i18nOtx = "";

    /**
     * perspective文件
     */
    private String perspectiveFileName = "";

    /**
     * theme文件
     */
    private String themeFileName = "";

    /**
     * workflow文件
     */
    private String workflowFileName = "";

    /**
     * menu文件
     */
    private String menuFileName = "";

    /**
     * topology文件
     */
    private String topologyFileName = "";

}
