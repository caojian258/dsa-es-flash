package org.dsa.modules.reporter.document;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
public class DiagnosticDoc extends ElasticEntity {

    private String diagSource;

    private Integer diagCategory;

    private String sessionInfoId;

    private String sessionId;

    private String vin;

    private List<Long> vehicleTypeId;

    private Long userId;

    private String userName;

    private String functionCode;

    private String action;

    private String specialFunctionName;

    private String ecuName;

    private Map<String, Object> ecuNameSuggest;

    private String nrcHex;

    private String nrcDescription;

    private String nrcDescriptionTi;

    private String errorCode;

    private String errorMessage;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date lastUpdateTime;


}
