package org.dsa.common.utils;

/**
 * Redis所有Keys
 *
 */
public class RedisKeys {

    public static String getSysConfigKey(String key){
        return "sys:config:" + key;
    }

    public static String getSysConfigGroupKey(String key){
        return "sys:config:group:" + key;
    }

    public static String getExpireTokenKey(String key) {
        return "token:expire:"+key;
    }
}
