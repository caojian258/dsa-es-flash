package org.dsa.modules.diagnostic.service.impl;

import ch.qos.logback.classic.Logger;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.diagnostic.dao.DActionInfoDao;
import org.dsa.modules.diagnostic.dao.DSessionInfoDao;
import org.dsa.modules.diagnostic.service.DiagnosticRedisService;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;


@Service("diagnosticRedisServiceImpl")
public class DiagnosticRedisServiceImpl implements DiagnosticRedisService {
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass());

    @Resource
    private RedisUtils redisUtils;
    @Resource
    private DSessionInfoDao dSessionInfoDao;
    @Resource
    private DActionInfoDao dActionInfoDao;


    @Override
    public void overdue(String redisKey) {
        logger.info("Redis key:{} expire ",redisKey);
        // S_ 开头车辆信息  R_ 诊断命令 其它key不需要处理

        //车辆信息超时 车辆断开连接 ,更新 session表 状态信息
        /*if(redisKey.startsWith(Constant.REDIS_VIN)){
            String vin = redisKey.replace(Constant.REDIS_VIN,"");
            logger.info("Redis expire update session end_time by vin:{}",vin);

            //修改session 表,状态为:断开连接
            dSessionInfoDao.updateSessionEndTimeByVin(vin);
        }else */
        if(redisKey.startsWith(Constant.REDIS_ACTION)){
            String actionId = redisKey.replace(Constant.REDIS_ACTION,"");

            logger.info("Redis expire update action status and end_time by actionId:{}",actionId);

            //更改action 表,执行状态为:超时
            dActionInfoDao.updateByActionId(actionId, String.valueOf(Constant.RESULTS.TIMEOUT.getValue()),1);

            //诊断命令执行结果
            logger.info("Redis expire  set redis key{},value{}", Constant.REDIS_ACTION_RESULT + actionId, Constant.RESULTS.TIMEOUT.getDisName());
            redisUtils.set(Constant.REDIS_ACTION_STATUS + actionId, Constant.RESULTS.TIMEOUT ,Constant.DIAGNOSTIC_TIMEOUT);
        }
    }
}