package org.dsa.modules.reporter.service;

import java.util.List;
import java.util.Map;

public interface LocaleService {

    /***
     * 单个TI翻译
     * @param ti
     * @param locale
     * @return
     */
    public String translateByTi(String ti, String locale);

    /***
     * 多个TI翻译
     * @param tiList
     * @param locale
     * @return
     */
    public Map<String, String> translateByTis(List<String> tiList, String locale);

    /***
     * 描述翻译
     * @param ti
     * @param locale
     * @param defaultDescription
     * @return
     */
    public String translateDescription(String ti, String locale, String defaultDescription);

    /***
     * DTC 描述特殊处理
     * @param ti
     * @param locale
     * @param defaultDescription
     * @return
     */
    public String translateDtcDescription(String ti, String locale, String defaultDescription);


    /***
     * 操作状态翻译
     * @param ti
     * @param locale
     * @param defaultMsg
     * @return
     */
    public String translateErrorMsg(String ti, String locale, String defaultMsg);

    /***
     * 多TI翻译
     * @param ti
     * @param locale
     * @return
     */
    public String multipleTiTranslate(String ti, String locale);
}
