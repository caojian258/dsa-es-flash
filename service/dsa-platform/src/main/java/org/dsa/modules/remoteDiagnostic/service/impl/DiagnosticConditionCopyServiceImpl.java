package org.dsa.modules.remoteDiagnostic.service.impl;


import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.dsa.modules.remoteDiagnostic.dao.DiagnosticConditionCopyDao;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticConditionCopyEntity;
import org.dsa.modules.remoteDiagnostic.service.DiagnosticConditionCopyService;
import org.dsa.modules.remoteDiagnostic.vo.DiagnosticConditionPageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class DiagnosticConditionCopyServiceImpl implements DiagnosticConditionCopyService {


    @Autowired
    private DiagnosticConditionCopyDao conditionDao;

    @Override
    public Page<DiagnosticConditionCopyEntity> selectPage(DiagnosticConditionPageVo vo) {
        Page<DiagnosticConditionCopyEntity> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);
        LambdaQueryWrapper<DiagnosticConditionCopyEntity> qw = Wrappers.<DiagnosticConditionCopyEntity>lambdaQuery();

        if(!StringUtils.isEmpty(vo.getName())){
//            qw.like(VehicleEcuEntity::getEcuName, vo.getName().trim());
            qw.apply("condition_name"+" ilike {0}", "%"+vo.getName().trim()+"%");
        }
        if(!StringUtils.isEmpty(vo.getVal())){
            qw.eq(DiagnosticConditionCopyEntity::getConditionVal, vo.getVal());
        }

        Page<DiagnosticConditionCopyEntity> resultPage = conditionDao.selectPage(page, qw);

        return resultPage;
    }

    @Override
    public DiagnosticConditionCopyEntity getInfo(Long id) {
        return conditionDao.selectById(id);
    }

    @Override
    public void saveCond(DiagnosticConditionCopyEntity cond) {
        conditionDao.insert(cond);
    }

    @Override
    public void updateCond(DiagnosticConditionCopyEntity cond) {
        cond.setUpdatedAt(new Date());
        conditionDao.updateById(cond);
    }

    @Override
    public void inserts(Long taskId, List<DiagnosticConditionCopyEntity> list) {
        conditionDao.inserts(taskId, list);
    }

    @Override
    public List<DiagnosticConditionCopyEntity> getConditions() {
        return conditionDao.selectList(Wrappers.<DiagnosticConditionCopyEntity>lambdaQuery());
    }


    @Override
    public List<DiagnosticConditionCopyEntity> getConditions(Long taskId) {
        return conditionDao.selectConditions(taskId);
    }

    @Override
    public Boolean check(String conditionName) {
        return null;
    }
}
