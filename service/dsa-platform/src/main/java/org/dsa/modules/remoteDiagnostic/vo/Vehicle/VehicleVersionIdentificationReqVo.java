package org.dsa.modules.remoteDiagnostic.vo.Vehicle;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

@Data
@ToString(callSuper = true)
public class VehicleVersionIdentificationReqVo {

    private List<Long> ecuVersionId;

    private Long versionId;
}
