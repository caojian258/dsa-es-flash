package org.dsa.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 词条翻译
 */
@Data
@TableName("d_language_translation")
public class SysTranslateEntity {
    /**
     * 键
     */
    private String key;

    /**
     * 语种Id
     */
//    private Integer localeId;
//
    /**
     * 语种
     */
    private String locale;

    /**
     * 词条内容
     */
    private String text;

    /**
     * 诊断库字段
     */
    private String geometry;

}
