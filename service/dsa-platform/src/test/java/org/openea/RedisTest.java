package org.openea;

import jakarta.annotation.Resource;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.reporter.constant.RedisConstant;
import org.dsa.modules.sys.entity.SysUserEntity;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisTest {

	@Resource
	private RedisUtils redisUtils;



	@Test
	public void contextLoads() {
		SysUserEntity user = new SysUserEntity();
		user.setEmail("qqq@qq.com");

		System.out.println(ToStringBuilder.reflectionToString(redisUtils.get("aa")));

		redisUtils.set("user", user);

		System.out.println(ToStringBuilder.reflectionToString(redisUtils.get("user")));

	}

}
