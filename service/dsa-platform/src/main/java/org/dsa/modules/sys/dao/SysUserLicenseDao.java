package org.dsa.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.dsa.modules.sys.entity.SysUserLicenseEntity;

/**
 * 系统用户
 *
 */
@Mapper
public interface SysUserLicenseDao extends BaseMapper<SysUserLicenseEntity> {
	

}
