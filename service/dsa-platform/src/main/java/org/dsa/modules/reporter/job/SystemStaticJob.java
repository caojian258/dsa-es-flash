package org.dsa.modules.reporter.job;


import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.dsa.modules.reporter.service.ApplicationHistoryService;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Slf4j
@Component
@DisallowConcurrentExecution
public class SystemStaticJob extends QuartzJobBean {

    @Autowired
    ApplicationHistoryService historyService;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        //处理当天数据
        log.info("SystemStaticJob start ");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

        Calendar todayStart = Calendar.getInstance();
        todayStart.set(Calendar.HOUR_OF_DAY, 0);
        todayStart.set(Calendar.MINUTE, 0);
        todayStart.set(Calendar.SECOND, 0);
        todayStart.set(Calendar.MILLISECOND, 0);
        Long start = todayStart.getTimeInMillis();

        Calendar todayEnd = Calendar.getInstance();
        todayEnd.set(Calendar.HOUR, 23);
        todayEnd.set(Calendar.MINUTE, 59);
        todayEnd.set(Calendar.SECOND, 59);
        todayEnd.set(Calendar.MILLISECOND, 999);
        Long end = todayEnd.getTime().getTime();

        String startDate = sdf.format(new Date(start));
        String endDate = sdf.format(new Date(end));

        try {
            //直接调用sql进行统计更新
             historyService.statsHistory(startDate, endDate);
        }catch (Exception ex){
            log.error("SystemStaticJob error: {}", JSONObject.toJSONString(ex));
        }


    }
}
