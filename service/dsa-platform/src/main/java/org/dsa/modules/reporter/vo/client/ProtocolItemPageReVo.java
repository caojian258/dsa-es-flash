package org.dsa.modules.reporter.vo.client;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.modules.reporter.pojo.PageParam;

import jakarta.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ProtocolItemPageReVo extends PageParam{

    /***
     * 客户端
     */
    @NotNull(message = "客户端不能为空")
    private String pcid;

    /***
     * 来源
     */
    private String referenceId;

    /***
     * 日期
     */
    private String date;

    /***
     * 经销商
     */
    private String workshop;

    /***
     * 搜索关键词
     */
    private String message;

    /***
     * 批处理tag
     */
    private String tag;
}
