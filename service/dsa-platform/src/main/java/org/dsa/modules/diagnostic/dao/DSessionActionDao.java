package org.dsa.modules.diagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.diagnostic.dto.SessionActionDto;
import org.dsa.modules.diagnostic.entity.DSessionActionEntity;

import java.util.List;

/**
 * 车辆诊断会话表关联诊断命令表
 * 
 */
@Mapper
public interface DSessionActionDao extends BaseMapper<DSessionActionEntity> {
	
	/**
	 * 根据actionId 查看实体信息
	 * @param actionId
	 * @return DSessionActionEntity
	 */
    DSessionActionEntity selectByActionId(String actionId);
	/**
	 * 根据actionId 查看实体信息
	 * @param actionId
	 * @return sessionId
	 */
	String selectSessionIdByActionId(String actionId);

	/**
	 * 根据actionId 查看实体信息
	 * @param actionId
	 * @return sessionId
	 */
	String selectVinIdByActionId(String actionId);

    List<String> selectListBySessionId(@Param("sessionId") String sessionId);

	List<String> selectListBySessionIdAndFunctionCode(@Param("sessionId") String sessionId
			,@Param("functionCode") String functionCode);

	List<SessionActionDto> findSessionAction(@Param("vin") String vi,
											 @Param("startTime") String startTime,
											 @Param("functionCode") String functionCode,
											 @Param("actionCode") String actionCode
											);
}
