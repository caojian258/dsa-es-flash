package org.dsa.modules.vehicle.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dsa.modules.remoteDiagnostic.vo.Vehicle.VehicleInfoExcelReqVo;
import org.dsa.modules.vehicle.dto.VehicleInfoDTO;
import org.dsa.modules.vehicle.entity.DVehicleInfoEntity;
import org.dsa.modules.vehicle.entity.VehicleInfoEntity;
import org.dsa.modules.vehicle.vo.VehicleInfoUpdateReqVo;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 车辆信息管理
 */

public interface VehicleService extends IService<DVehicleInfoEntity> {

    /**
     * 根据Id获取详情
     *
     * @param id
     * @return
     */
    DVehicleInfoEntity getVehicleInfoById(Long id);

    /**
     * update vehicle info
     *
     * @param entity
     * @return Integer
     */
    Integer updateVehicleInfo(DVehicleInfoEntity entity);

    /**
     * 根据vin码获取车辆信息
     *
     * @return
     */
    VehicleInfoEntity getVehicleInfoByVin(String vin);

    Map<String, String> vehicleExcelExport() throws IOException;

    Map<String, String> vehicleExcelExport(List<DVehicleInfoEntity> ids) throws IOException;

    Map<String, String> vehicleExcelExport(VehicleInfoExcelReqVo vo) throws IOException;

    List<DVehicleInfoEntity> getListByType(Long id);

    DVehicleInfoEntity getOne(String vin);

    VehicleInfoDTO get(String vin);

    void batchUpdateInfo(VehicleInfoUpdateReqVo vo);

    List<Long> getVehicleTypeIdsByVin(String vin);

    List<Map<String, String>> getDidValueByVehicleTypeIdsAndEcuNameAndDid(List<Long> vehicleTypeIds, List<String> ecuNames, String did);

}
