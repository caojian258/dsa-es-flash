package org.dsa.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dsa.common.utils.PageUtils;
import org.dsa.modules.sys.entity.SysUserLicenseEntity;

import java.util.List;
import java.util.Map;


/**
 * 系统用户
 *
 */
public interface SysUserLicenseService extends IService<SysUserLicenseEntity> {


    List<SysUserLicenseEntity> queryById(String userId);

    PageUtils queryPage(Map<String, Object> params);

    List<SysUserLicenseEntity> queryListByUserId(Long userId);

    SysUserLicenseEntity info(Map<String,Object> params);

    boolean save(SysUserLicenseEntity entity);

    void update(Map<String,Object> params);

    void delete(List<Long> ids);

}
