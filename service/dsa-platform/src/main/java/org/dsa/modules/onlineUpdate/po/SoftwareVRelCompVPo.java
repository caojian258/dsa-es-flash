package org.dsa.modules.onlineUpdate.po;

import lombok.Data;

/**
 * 软件版本关联组件版本
 */
@Data
public class SoftwareVRelCompVPo {
    private Long componentId;
    private Long componentVersionId;
}
