package org.dsa.modules.remoteDiagnostic.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.modules.remoteDiagnostic.pojo.PageParam;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class VehiclePlatformPageReVo extends PageParam {

    private String id;

    private String vin;

    private String name;

    private Integer status;

    private List<Long> vehicleTypeIds;
}
