package org.dsa.modules.remoteDiagnostic.vo.Vehicle;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.common.validator.group.UpdateGroup;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuGroupEntity;
import org.dsa.modules.remoteDiagnostic.pojo.PageParam;

import jakarta.validation.constraints.NotBlank;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class VehicleTypeEcuGroupReqVo extends PageParam {

    @NotBlank(message = "名称不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private Long typeId;

    private List<RemoteVehicleEcuGroupEntity> delGroupEntities;

    private List<RemoteVehicleEcuGroupEntity> groupEntities;
}
