package org.dsa.modules.remoteDiagnostic.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuGroupEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleTypeEcuGroupEntity;

import java.util.Arrays;
import java.util.List;

@Mapper
public interface RemoteVehicleTypeEcuGroupDao extends BaseMapper<RemoteVehicleTypeEcuGroupEntity> {

    @Insert("<script>"
            + " insert into r_vehicle_type_ecu (type_id,ecu_group_id,ecu_id) values "
            + " <foreach collection='ecus' item='item' separator=','> "
            + " (#{typeId},#{item.id},#{item.ecuId} ) "
            + " </foreach> "
            + " ON conflict(type_id,ecu_group_id,ecu_id)  DO NOTHING "
            + " </script> ")
    void inserts(@Param("typeId") Long id, @Param("ecus") List<RemoteVehicleEcuGroupEntity> ecus);

    @Select("select ecu_id from r_vehicle_type_ecu where type_id = #{typeId} and ecu_group_id = #{groupId} ")
    Long selectEcuId(@Param("typeId") Long typeId, @Param("groupId") Long groupId);

    List<RemoteVehicleTypeEcuGroupEntity> selectByTypeId(@Param("typeId") Long typeId);

    void insertsGroup(@Param("typeId") Long id, @Param("groups") List<RemoteVehicleTypeEcuGroupEntity> groups);

}
