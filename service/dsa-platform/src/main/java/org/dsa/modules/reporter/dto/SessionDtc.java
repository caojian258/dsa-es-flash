package org.dsa.modules.reporter.dto;

import lombok.Data;

@Data
public class SessionDtc {

    private String dtcCode;

    private String dtcTime;

    private String sourceName;

    private String dtcStatusCode;

    private String dtcStatus;

    private String dtcDefinition;

}
