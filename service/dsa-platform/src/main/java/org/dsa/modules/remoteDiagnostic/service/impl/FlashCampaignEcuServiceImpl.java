package org.dsa.modules.remoteDiagnostic.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.utils.AESUtils;
import org.dsa.modules.remoteDiagnostic.constant.FlashConstant;
import org.dsa.modules.remoteDiagnostic.dao.FlashCampaignEcuDao;
import org.dsa.modules.remoteDiagnostic.dto.flash.FlashCampaignEcuDto;
import org.dsa.modules.remoteDiagnostic.entity.FlashCampaignEcuEntity;
import org.dsa.modules.remoteDiagnostic.service.FlashCampaignEcuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class FlashCampaignEcuServiceImpl implements FlashCampaignEcuService {

    @Autowired
    FlashCampaignEcuDao flashCampaignEcuDao;

    @Value("${project.url}")
    private String domainUrl;

    @Override
    public List<FlashCampaignEcuDto> queryCampaignEcu(Long id) {
        List<FlashCampaignEcuEntity> entityList = flashCampaignEcuDao.selectByCampaignId(id);
        List<FlashCampaignEcuDto> campaignEcuDtos = new ArrayList<>();

        if(CollectionUtil.isNotEmpty(entityList)){
            for (FlashCampaignEcuEntity ecuEntity: entityList) {
                FlashCampaignEcuDto ecuVersionFile = flashCampaignEcuDao.findEcuFile(ecuEntity.getEcuId(), ecuEntity.getEcuVersionId());
                if(null != ecuVersionFile){
                    ecuVersionFile.setFileDownloadUrl(buildDownloadUrl(ecuVersionFile.getFileDownloadUrl()));
                    campaignEcuDtos.add(ecuVersionFile);
                }
            }
        }

        return campaignEcuDtos;
    }

    private String buildDownloadUrl(String path){
        String param = AESUtils.encrypt(path, AESUtils.AES_KEY);
        if(null == param){
            return "";
        }

        try {
            return domainUrl+ FlashConstant.DOWNLOAD_PATH+"?params="+ URLEncoder.encode(param, StandardCharsets.UTF_8.name());
        }catch (Exception ex){
            log.error("buildDownloadUrl {} error: {}", path, ex);
            return "";
        }
    }
}
