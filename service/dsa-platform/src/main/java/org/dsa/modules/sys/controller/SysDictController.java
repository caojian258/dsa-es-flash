package org.dsa.modules.sys.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.dsa.common.utils.PageUtils;
import org.dsa.common.utils.R;
import org.dsa.common.validator.ValidatorUtils;
import org.dsa.modules.sys.entity.SysDictEntity;
import org.dsa.modules.sys.service.SysDictService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 数据字典
 *
 */
@RestController
@RequestMapping("sys/dict")
public class SysDictController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private SysDictService sysDictService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:dict:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysDictService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("sys:dict:info")
    public R info(@PathVariable("id") Long id){
        SysDictEntity dict = sysDictService.getById(id);

        return R.ok().put("dict", dict);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:dict:save")
    public R save(@RequestBody SysDictEntity dict){
        //校验类型
        ValidatorUtils.validateEntity(dict);

        sysDictService.save(dict);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:dict:update")
    public R update(@RequestBody SysDictEntity dict){
        //校验类型
        ValidatorUtils.validateEntity(dict);

        sysDictService.updateById(dict);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:dict:delete")
    public R delete(@RequestBody Long[] ids){
        sysDictService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

    /**
     * 获取字典表信息
     * @param type
     * @return
     */
    @RequestMapping("/get/{type}")
    public R getSysDictInfo(@PathVariable("type") String type){
        logger.info("传入的type类型为："+type);
        try{
            List<SysDictEntity> dictlist=sysDictService.getSysDictByType(type);
            return R.ok().put("dictlist",dictlist);
        }catch (Exception e){
            logger.error("获取字典表数据出错 : "+e);
            return R.error("查询字典表数据出错");
        }

    }

}
