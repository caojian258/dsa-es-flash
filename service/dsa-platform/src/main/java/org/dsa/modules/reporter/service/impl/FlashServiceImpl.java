package org.dsa.modules.reporter.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang.ArrayUtils;
import org.dsa.common.utils.DateUtils;
import org.dsa.modules.reporter.dao.EcuVersionHistoryMapper;
import org.dsa.modules.reporter.dao.VehicleFlashDetailMapper;
import org.dsa.modules.reporter.dao.VehicleFlashRecordMapper;
import org.dsa.modules.reporter.dao.VehicleVersionHistoryMapper;
import org.dsa.modules.reporter.dto.*;
import org.dsa.modules.reporter.entity.AppInstall;
import org.dsa.modules.reporter.entity.VehicleFlashDetail;
import org.dsa.modules.reporter.entity.VehicleFlashRecord;
import org.dsa.modules.reporter.entity.VehicleInfo;
import org.dsa.modules.reporter.entity.*;
import org.dsa.modules.reporter.service.FlashService;
import org.dsa.modules.reporter.util.LocalUtil;
import org.dsa.modules.reporter.util.MapBeanUtil;
import org.dsa.modules.reporter.vo.ChartReVo;
import org.dsa.modules.reporter.vo.client.ClientChartResVo;
import org.dsa.modules.reporter.vo.flash.FlashReqVo;
import org.dsa.modules.reporter.vo.flash.VehicleStatPageReqVo;
import org.dsa.modules.vehicle.dao.DVehicleInfoDao;
import org.dsa.modules.vehicle.dao.DVehicleTypeDao;
import org.dsa.modules.vehicle.entity.DVehicleInfoEntity;
import org.dsa.modules.vehicle.entity.DVehicleTypeEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class FlashServiceImpl implements FlashService {
    @Autowired
    VehicleFlashDetailMapper detailMapper;
    @Autowired
    VehicleFlashRecordMapper recordMapper;
    @Autowired
    DVehicleTypeDao dVehicleTypeDao;
    @Autowired
    DVehicleInfoDao dVehicleInfoDao;
    @Autowired
    EcuVersionHistoryMapper ecuVersionHistoryMapper;
    @Autowired
    VehicleVersionHistoryMapper vehicleVersionHistoryMapper;


    @Override
    public Page<VehicleFlashRecord> getRecordPage(FlashReqVo vo) {
        Page<VehicleFlashRecord> page = new Page<>(vo.getPageNo(), vo.getPageSize());
        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);
        LambdaQueryWrapper<VehicleFlashRecord> qw = Wrappers.<VehicleFlashRecord>lambdaQuery();
        if (StringUtils.isNotBlank(vo.getVin())) {
            qw.apply("vin ilike {0}", "%"+vo.getVin()+"%");
        }

        if (vo.getTaskTime() != null && vo.getTaskTime().size() == 2) {
            qw.between(VehicleFlashRecord::getCollectTime, Timestamp.valueOf(vo.getTaskTime().get(0)+" 00:00:00.000"),Timestamp.valueOf(vo.getTaskTime().get(1)+" 23:59:59.999"));
        }
        if(CollectionUtil.isNotEmpty(vo.getVehicleTypes())){
            qw.in(VehicleFlashRecord::getModel, vo.getVehicleTypes());
        }
        return recordMapper.selectPage(page, qw);
    }

    @Override
    public List<Map<String, Object>> getByVin(FlashReqVo vo) {
        if (StringUtils.isBlank(vo.getVin())) {
            return null;
        }
        if (StringUtils.isBlank(vo.getVersion())) {
            return null;
        }
        if (null == vo.getId()) {
            return null;
        }

        List<VehicleFlashDetail> byVersionAndVinGroupName = detailMapper.findByVersionAndVinGroupName(vo.getId(), vo.getVin(), vo.getVersion());
        List<Map<String, Object>> list = new ArrayList<>();
        List<VehicleFlashDetail> byVersionAndVin;
        for (VehicleFlashDetail vehicleFlashDetail : byVersionAndVinGroupName) {
            byVersionAndVin = detailMapper.findByVersionAndVin(vo.getId(), vo.getVin(), vo.getVersion(), vehicleFlashDetail.getEcuName());
            Map<String, Object> map = new HashMap<>();
            map.put("ecuName", vehicleFlashDetail.getEcuName());
            for (VehicleFlashDetail flashDetail : byVersionAndVin) {
                map.put(flashDetail.getVersionName(), flashDetail.getVersionValue());
                map.computeIfAbsent("result", k -> flashDetail.getResult());
                map.computeIfAbsent("reason", k -> flashDetail.getReason());
            }
            list.add(map);
        }
        return list;
    }


    @Override
    public IPage<FlashVehicleStatsDto> selectVehicleStats(IPage<FlashVehicleStatsDto> page, VehicleStatPageReqVo vo) {
        return recordMapper.selectStatsPage(page, vo.getStartDate(), vo.getEndDate(), vo.getVin(), vo.getVehicleTypeIds());
    }

    @Override
    public Map<String,Object> getDetail(FlashReqVo flashReqVo) {
//        LambdaQueryWrapper<VehicleFlashRecord> qw = Wrappers.<VehicleFlashRecord>lambdaQuery();
        if (StringUtils.isBlank(flashReqVo.getVin())) {
            return null;
        }
//        if (StringUtils.isBlank(flashReqVo.getVersion()) && StringUtils.isBlank(flashReqVo.getPrevVersion())){
//            return null;
//        }
//        if (StringUtils.isBlank(flashReqVo.getVersion())) {
//            qw.eq(VehicleFlashRecord::getVehicleVersion, flashReqVo.getVersion());
//        } else {
//            qw.eq(VehicleFlashRecord::getPrevVersion, flashReqVo.getVersion());
//        }
//        qw.eq(VehicleFlashRecord::getVin, flashReqVo.getVin());
//        List<VehicleFlashRecord> list = recordMapper.selectList(qw);
//        if (list == null || list.size() == 0) {
//            return null;
//        }

        DVehicleInfoEntity dVehicleInfoEntity = dVehicleInfoDao.selectOneByVin(flashReqVo.getVin());
        List<DVehicleTypeEntity> dVehicleTypeEntities = dVehicleTypeDao.selectListById(dVehicleInfoEntity.getVehicleTypeId());
        HashMap<String, Object> map = new HashMap<>();
        List<FlashBaseInfoDto> list1 = new ArrayList<>();
        list1.add(new FlashBaseInfoDto(LocalUtil.get("flash.vin"), flashReqVo.getVin()));
        list1.add(new FlashBaseInfoDto(LocalUtil.get("flash.VehicleModel"), dVehicleTypeEntities.get(0).getValue()));
        list1.add(new FlashBaseInfoDto(LocalUtil.get("flash.brand"), dVehicleTypeEntities.get(1).getValue()));

        map.put("flashHardwareList", list1);
        List<FlashBaseInfoDto> list2 = new ArrayList<>();
        list2.add(new FlashBaseInfoDto(LocalUtil.get("flash.vehicleVersion"), flashReqVo.getVersion()));
        list2.add(new FlashBaseInfoDto(LocalUtil.get("flash.placeOfProduction"), dVehicleInfoEntity.getPlaceOfProduction()));
        list2.add(new FlashBaseInfoDto(LocalUtil.get("flash.productionDate"), dVehicleInfoEntity.getProductionDate()));
        map.put("flashHardVersionList", list2);
        return map;
    }

    @Override
    public  Page<VehicleFlashDetail> getEcu(FlashReqVo vo) {

//        LambdaQueryWrapper<VehicleFlashRecord> qw = Wrappers.<VehicleFlashRecord>lambdaQuery();
//        qw.eq(VehicleFlashRecord::getVin, vo.getVin());
//        VehicleFlashRecord vehicleFlashRecordPage = recordMapper.selectOne(qw);

        Page<VehicleFlashDetail> page = new Page<>(vo.getPageNo(), vo.getPageSize());
        if (!"id".equals(vo.getSortColumn())) {
            String column = StrUtil.toUnderlineCase(vo.getSortColumn());
            OrderItem oi = new OrderItem(column, vo.getSortAsc());
            page.addOrder(oi);
        }
        List<String> taskTimes = vo.getTaskTime();
        String startDate = null;
        String endDate = null;
        if(CollectionUtil.isNotEmpty(taskTimes)){
            startDate = taskTimes.get(0);
            endDate  = taskTimes.get(1);
        }
        Page<VehicleFlashDetail> vehicleFlashDetailPage = detailMapper.findEcuName(page, vo.getVin(), vo.getEcuName(), vo.getVehicleTypeIds(), startDate, endDate);
        List<VehicleFlashDetail> records = vehicleFlashDetailPage.getRecords();
        Map<String, Object> map;
        for (VehicleFlashDetail vehicleFlashDetail : records) {
            map = new HashMap<>();
//            map.put("ecuName", vehicleFlashDetail.getEcuName());
//            map.put("version", vehicleFlashDetail.getVersion());
//            map.put("vin", vehicleFlashDetail.getVin());
//            map.put("result", vehicleFlashDetail.getResult());
            List<VehicleFlashDetail> byVersionAndVin = detailMapper.findByEcuAndVin(vehicleFlashDetail.getVin(),vehicleFlashDetail.getVersion(), vehicleFlashDetail.getEcuName(), startDate, endDate);
            for (VehicleFlashDetail flashDetail : byVersionAndVin) {
                map.put(flashDetail.getVersionName(), flashDetail.getVersionValue());
            }
            vehicleFlashDetail.setEcus(map);
        }
//        map = new HashMap<>();
//        map.put("page", vehicleFlashDetailPage);
//        map.put("data", records);
        return vehicleFlashDetailPage;
    }

    @Override
    public List<EcuVersionHistory> getEcuHis(FlashReqVo flashReqVo) {
        LambdaQueryWrapper<VehicleFlashRecord> qw = Wrappers.<VehicleFlashRecord>lambdaQuery();
        if (StringUtils.isBlank(flashReqVo.getVin())) {
            return null;
        }
        qw.eq(VehicleFlashRecord::getVin, flashReqVo.getVin());
        VehicleFlashRecord vehicleFlashRecord = recordMapper.selectList(qw).get(0);

        String vin = vehicleFlashRecord.getVin();
        List<EcuVersionHistory> hard = new ArrayList<>();
        List<EcuVersionHistory> soft = new ArrayList<>();

        List<EcuVersionHistory> ecuHisVersion = ecuVersionHistoryMapper.findEcuHisVin(vin, flashReqVo.getEcuName(), flashReqVo.getVersion());
        for (EcuVersionHistory ecuVersionHistory : ecuHisVersion) {
            hard = new ArrayList<>();
            soft = new ArrayList<>();
            List<EcuVersionHistory> hisVersion = ecuVersionHistoryMapper.findEcuHisVersion(vin,ecuVersionHistory.getEcuName(), ecuVersionHistory.getVehicleVersion(), DateUtils.format(ecuVersionHistory.getCollectTime(), "yyyy-MM-dd HH:mm:ss"));
            for (EcuVersionHistory versionHistory : hisVersion) {
                if ("HardwareVersion".equals(versionHistory.getVersionName())) {
                    hard.add(versionHistory);
                } else {
                    soft.add(versionHistory);
                }
            }
            ecuVersionHistory.setHistoryHardwareList(hard);
            ecuVersionHistory.setHistorySoftwareList(soft);
        }
        return ecuHisVersion;
    }

    @Override
    public Object getVehicleHis(FlashReqVo vo) {
        if (StringUtils.isBlank(vo.getVin())) {
            return null;
        }
        Map<String, Object> result = new HashMap<>();
        VehicleFlashRecord record = recordMapper.selectById(vo.getId());
        String timestamp = null;
        if(null != record){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            timestamp = sdf.format(record.getCollectTime());
        }
        List<VehicleVersionHistory> histories =  vehicleVersionHistoryMapper.queryByVin(vo.getVin(),timestamp);
        result.put("data", histories);
        result.put("total", histories.size());
        return result;
    }

    @Override
    public List<String> getEcuNames() {
        return detailMapper.getEcuNames();
    }

    @Override
    public List<EcuVersionHistoryDto> getEcuVersionHistory(String vin, String vehVersion) {
        return  ecuVersionHistoryMapper.selectEcuVersionHistory(vin, vehVersion);
    }

    @Override
    public ClientChartResVo statVehicleEcuPi(ChartReVo vo) {
        ClientChartResVo resVo = new ClientChartResVo();
        Set<String> legendData = new HashSet<>();
        List<ChartItemDto> dtoList = new ArrayList<>();
        Long itemTotal = 0L;

        Map<String, Object> condition = vo.getCondition();
        String startDate = (String) condition.get("startDate");
        String endDate = (String) condition.get("endDate");
        String vin = (String) condition.get("vin");
        List<Long> vehicleTypeIds = (List<Long>) condition.get("vehicleTypeIds");

        List<FlashEcuChartPiDto> ecuChartPiDtos =  detailMapper.flashEcuChartPi(startDate, endDate, vin, vehicleTypeIds);
        for (FlashEcuChartPiDto item: ecuChartPiDtos){
            ChartItemDto dto = new ChartItemDto();
            String name = String.valueOf(item.getEcuName());
            dto.setType(name);
            if (name.equals("unknown")){
                name = LocalUtil.get("workshop.unknown");
            }
            legendData.add(name);
            dto.setName(name);
            itemTotal +=  item.getValue();
            dto.setValue(item.getValue());
            dtoList.add(dto);
        }
        resVo.setItemTotal(itemTotal);
        resVo.setLegendData(legendData);
        resVo.setSeriesData(dtoList);
        resVo.setTotal(Long.valueOf(dtoList.size()));
        return resVo;
    }

    @Override
    public ClientChartResVo statVehicleEcuBar(ChartReVo vo) {
        ClientChartResVo resVo = new ClientChartResVo();
        Set<String> legendData = new HashSet<>();
        List<ChartItemDto> dtoList = new ArrayList<>();
        Long itemTotal = 0L;
        Map<String, Object> condition = vo.getCondition();
        String startDate = (String) condition.get("startDate");
        String endDate = (String) condition.get("endDate");
        String vin = (String) condition.get("vin");
        List<Long> vehicleTypeIds = (List<Long>) condition.get("vehicleTypeIds");

        List<FlashEcuChartBarDto> items =  detailMapper.flashEcuChartBar(startDate, endDate, vin, vehicleTypeIds);

        Set<String> xAxisData = new LinkedHashSet<>();
        Map<String,Map<String, Object>> barStore = new HashMap<>();

        for (FlashEcuChartBarDto item: items){
            String name = String.valueOf(item.getEcuName());
            legendData.add(name);
            xAxisData.add(String.valueOf(item.getCollectMonth()));
            Map<String, Object> map = null;
            map = barStore.get(name);
            if(null == map){
                map = new HashMap<>();
            }
            itemTotal +=  (Long)item.getValue();
            map.put(String.valueOf(item.getCollectMonth()), item.getValue());
            barStore.put(name, map);
        }

        //这里要notice xAxisData 保证这里面的数据按照顺序填满
        String[] xAxisDatas = new String[xAxisData.size()];
        xAxisData.toArray(xAxisDatas);

        for (String key : barStore.keySet()) {
            ChartItemDto dto = new ChartItemDto();
            dto.setName(key);
            dto.setType("bar");
            dto.setStack("ecu_name");

            long[] childrenTotal = new long[xAxisData.size()];

            Map<String,Object> bs = barStore.get(key);
            for (String k: bs.keySet()){
                int index = ArrayUtils.indexOf(xAxisDatas, k);
                if(index>-1){
                    childrenTotal[index] = Integer.valueOf(bs.get(k).toString());
                }
            }
            dto.setData(childrenTotal);
            dtoList.add(dto);
        }

        resVo.setItemTotal(itemTotal);
        resVo.setXAxisData(xAxisData);
        resVo.setSeriesData(dtoList);
        resVo.setLegendData(legendData);
        resVo.setTotal(Long.valueOf(dtoList.size()));
        return resVo;
    }

}
