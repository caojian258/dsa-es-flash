package org.dsa.modules.remoteDiagnostic.service.impl;


import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.dsa.modules.remoteDiagnostic.dao.DiagnosticConditionCopyDao;
import org.dsa.modules.remoteDiagnostic.dao.DiagnosticConditionDao;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticConditionCopyEntity;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticConditionEntity;
import org.dsa.modules.remoteDiagnostic.service.DiagnosticConditionService;
import org.dsa.modules.remoteDiagnostic.vo.CheckNameReqVo;
import org.dsa.modules.remoteDiagnostic.vo.CheckStatusReqVo;
import org.dsa.modules.remoteDiagnostic.vo.DiagnosticConditionPageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class DiagnosticConditionServiceImpl implements DiagnosticConditionService {


    @Autowired
    private DiagnosticConditionDao conditionDao;
    @Autowired
    private DiagnosticConditionCopyDao conditionCopyDao;

    @Override
    public Page<DiagnosticConditionEntity> selectPage(DiagnosticConditionPageVo vo) {
        Page<DiagnosticConditionEntity> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);
        LambdaQueryWrapper<DiagnosticConditionEntity> qw = Wrappers.<DiagnosticConditionEntity>lambdaQuery();

        if (!StringUtils.isEmpty(vo.getName())) {
//            qw.like(VehicleEcuEntity::getEcuName, vo.getName().trim());
            qw.apply("condition_name" + " ilike {0}", "%" + vo.getName().trim() + "%");
        }
        if (!StringUtils.isEmpty(vo.getVal())) {
            qw.eq(DiagnosticConditionEntity::getConditionVal, vo.getVal());
        }
        if (!StringUtils.isEmpty(vo.getStatus())) {
            qw.eq(DiagnosticConditionEntity::getStatus, vo.getStatus());
        }
        Page<DiagnosticConditionEntity> resultPage = conditionDao.selectPage(page, qw);

        return resultPage;
    }

    @Override
    public DiagnosticConditionEntity getInfo(Long id) {
        return conditionDao.selectById(id);
    }

    @Override
    public void saveCond(DiagnosticConditionEntity fun) {
        conditionDao.insert(fun);
    }

    @Override
    public void updateCond(DiagnosticConditionEntity fun) {
        fun.setUpdatedAt(new Date());
        conditionDao.updateById(fun);
    }

    @Override
    public List<DiagnosticConditionEntity> getConditions() {
        return conditionDao.selectList(Wrappers.<DiagnosticConditionEntity>lambdaQuery().eq(DiagnosticConditionEntity::getStatus, 0));
    }

    @Override
    public List<DiagnosticConditionEntity> getConditions(List<Long> ids, Long taskId) {
        if (taskId == null) {
            return conditionDao.selectList(Wrappers.<DiagnosticConditionEntity>lambdaQuery().eq(DiagnosticConditionEntity::getStatus, 0).in(DiagnosticConditionEntity::getId, ids));
        } else {
            return conditionDao.getListByTask(ids, taskId);
        }
    }

    @Override
    public Boolean check(CheckNameReqVo vo) {
        LambdaQueryWrapper<DiagnosticConditionEntity> qw = Wrappers.<DiagnosticConditionEntity>lambdaQuery();
        qw.eq(DiagnosticConditionEntity::getConditionName, vo.getName());
        if (vo.getId() != null) {
            qw.ne(DiagnosticConditionEntity::getId, vo.getId());
        }
        qw.select(DiagnosticConditionEntity::getId);
        return conditionDao.selectOne(qw) == null;
    }

    @Override
    public Boolean checkStatus(CheckStatusReqVo vo) {
        LambdaQueryWrapper<DiagnosticConditionCopyEntity> qw = Wrappers.<DiagnosticConditionCopyEntity>lambdaQuery();
        qw.eq(DiagnosticConditionCopyEntity::getRawId, vo.getId());
        qw.select(DiagnosticConditionCopyEntity::getId);
        return ObjectUtils.isEmpty(conditionCopyDao.selectList(qw));
    }
}
