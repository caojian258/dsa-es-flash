package org.dsa.modules.remoteDiagnostic.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticFileEntity;
import org.dsa.modules.remoteDiagnostic.vo.CheckNameReqVo;
import org.dsa.modules.remoteDiagnostic.vo.CheckStatusReqVo;
import org.dsa.modules.remoteDiagnostic.vo.PublicPageReqVo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface DiagnosticFileService {


    Page<DiagnosticFileEntity> selectPage(PublicPageReqVo vo);

    DiagnosticFileEntity getInfo(Long id);

    DiagnosticFileEntity getInfo(String md5);

    void saveFile(DiagnosticFileEntity file) throws IOException;

    void updateFile(DiagnosticFileEntity file) throws IOException;

    Map<String, Object> getFiles(Long id);

    List<DiagnosticFileEntity> getFiles();

    Boolean check(CheckNameReqVo vo);;

    String upload(File file) throws IOException;

    Boolean checkStatus(CheckStatusReqVo vo);
}
