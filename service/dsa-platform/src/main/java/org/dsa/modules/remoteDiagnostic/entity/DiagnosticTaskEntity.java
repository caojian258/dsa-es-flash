package org.dsa.modules.remoteDiagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@ToString
@TableName("r_diag_task")
public class DiagnosticTaskEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;

    private String taskName;

    private String descriptionTi;

    private Integer status;

    private Date createdAt;

    private Date updatedAt;

    @TableField(exist = false)
    List<String> taskTime;

    @TableField(exist = false)
    List<DiagnosticConditionCopyEntity> conditionList;

//    @TableField(exist = false)
//    List<RemoteVehicleEcuEntity> ecuList;

    @TableField(exist = false)
    List<DiagnosticFunctionEntity> funList;

//    @TableField(exist = false)
//    List<Long> ecus;

    @TableField(exist = false)
    List<Long> functions;

    @TableField(exist = false)
    private List<Long> files;
}
