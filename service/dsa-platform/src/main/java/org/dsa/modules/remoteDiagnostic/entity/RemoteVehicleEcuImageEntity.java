package org.dsa.modules.remoteDiagnostic.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.ToString;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.common.validator.group.UpdateGroup;
import org.dsa.modules.remoteDiagnostic.pojo.BaseEntity;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ToString
@TableName("r_vehicle_ecu_image")
public class RemoteVehicleEcuImageEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long ecuGroupId;

    private Long ecuId;

    @NotBlank(message="文件名不能为空", groups = {AddGroup.class, UpdateGroup.class})
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String fileName;

    @NotBlank(message = "路径不能为空", groups = {AddGroup.class, UpdateGroup.class})
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String filePath;

    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String fileMd5;

}
