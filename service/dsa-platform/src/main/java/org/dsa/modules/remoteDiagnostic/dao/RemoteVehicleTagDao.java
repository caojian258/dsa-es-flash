package org.dsa.modules.remoteDiagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleTagEntity;

import java.util.List;
import java.util.Map;

/**
 * ${comments}
 * 
 */
@Mapper
public interface RemoteVehicleTagDao extends BaseMapper<RemoteVehicleTagEntity> {


    @Insert("insert into r_tag(tag_name,description_ti) values(#{tag.tagName},#{tag.descriptionTi}) ON conflict(tag_name)  DO NOTHING ")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    public void insertTag(@Param("tag") RemoteVehicleTagEntity tag);

    @Select("select * from  r_tag i ")
    @MapKey("tagName")
    public Map<String, RemoteVehicleTagEntity> selectMap();

    @Select("select distinct  i.* from r_tag i left join r_vehicle_tag j on i.id = j.tag_id where j.tag_id is not null")
    List<RemoteVehicleTagEntity> getList();

    @Insert("<script>"
            + " insert into r_tag(tag_name) values "
            + " <foreach collection='tags' item='item' separator=','> "
            + " (#{item.tagName}) "
            + " </foreach> "
            + " ON conflict(tag_name) DO UPDATE SET tag_name = EXCLUDED.tag_name"
            + " </script>")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void inserts(@Param("tags") List<RemoteVehicleTagEntity> tags);
}
