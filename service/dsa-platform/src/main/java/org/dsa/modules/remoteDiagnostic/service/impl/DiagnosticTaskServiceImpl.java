package org.dsa.modules.remoteDiagnostic.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.dsa.modules.remoteDiagnostic.dao.*;
import org.dsa.modules.remoteDiagnostic.entity.*;
import org.dsa.modules.remoteDiagnostic.service.*;
import org.dsa.modules.remoteDiagnostic.vo.CheckNameReqVo;
import org.dsa.modules.remoteDiagnostic.vo.CheckStatusReqVo;
import org.dsa.modules.remoteDiagnostic.vo.PublicPageReqVo;
import org.dsa.modules.remoteDiagnostic.vo.Task.TaskConditionReqVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.*;

@Slf4j
@Service
public class DiagnosticTaskServiceImpl implements DiagnosticTaskService {

    @Autowired
    DiagnosticTaskDao taskDao;
    @Autowired
    private DiagnosticConditionCopyDao conditionDao;
    @Autowired
    private DiagnosticTaskEcuDao taskEcuDao;
    @Autowired
    private DiagnosticTaskFileDao taskFileDao;
    @Autowired
    private DiagnosticFunctionDao functionDao;
    //    @Autowired
//    private VinCodeDao vinCodeDao;
    @Autowired
    private DiagnosticConditionService conditionService;
    @Autowired
    private DiagnosticFunctionService functionService;
    @Autowired
    private DiagnosticFileService fileService;
    @Autowired
    private DiagnosticTaskCampaignService campaignService;
    @Autowired
    private DiagnosticTaskCampaignDao campaignDao;
//    @Autowired
//    private DiagnosticTaskRecordDao taskRecordDao;
//    @Autowired
//    private DiagnosticTaskDetailDao taskDetailDao;
//    @Autowired
//    private DiagnosticTaskOverviewDao taskOverviewDao;
//    @Autowired
//    RemoteDiagnosticConfig remoteDiagnosticConfig;
//    @Autowired
//    SyncConfig syncConfig;
//    @Autowired
//    DiagnosticTaskLogDao taskLogDao;

    @Override
    public Page<DiagnosticTaskEntity> selectPage(PublicPageReqVo vo) {
        Page<DiagnosticTaskEntity> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);
        LambdaQueryWrapper<DiagnosticTaskEntity> qw = Wrappers.<DiagnosticTaskEntity>lambdaQuery();

        if (!StringUtils.isEmpty(vo.getName())) {
//            qw.like(VehicleEcuEntity::getEcuName, vo.getName().trim());
            qw.apply("task_name" + " ilike {0}", "%" + vo.getName().trim() + "%");
        }

        if (!StringUtils.isEmpty(vo.getStatus())) {
            qw.eq(DiagnosticTaskEntity::getStatus, vo.getStatus());
        }
        Page<DiagnosticTaskEntity> resultPage = taskDao.selectPage(page, qw);

        return resultPage;
    }

    @Override
    public DiagnosticTaskEntity getInfo(Long id) {

        DiagnosticTaskEntity diagnosticTaskEntity = taskDao.selectById(id);

        diagnosticTaskEntity.setFunList(taskEcuDao.selectFunListByTask(diagnosticTaskEntity.getId()));
        diagnosticTaskEntity.setFiles(taskFileDao.selectFiles(diagnosticTaskEntity.getId()));

        return diagnosticTaskEntity;
    }

    @Override
    @Transactional
    public void saveTask(DiagnosticTaskEntity task) {
        taskDao.insert(task);

        if (task.getConditionList() != null && task.getConditionList().size() > 0) {
            conditionDao.inserts(task.getId(), task.getConditionList());
        }

        if (task.getFunList() != null && task.getFunList().size() > 0) {
            for (DiagnosticFunctionEntity diagnosticFunctionEntity : task.getFunList()) {
                if (diagnosticFunctionEntity.getEcuList() != null && diagnosticFunctionEntity.getEcuList().size() != 0) {
                    taskEcuDao.insertGroupAndEcu(task.getId(), diagnosticFunctionEntity.getId(), diagnosticFunctionEntity.getEcuList());
                } else {
                    taskEcuDao.insertFun(task.getId(), diagnosticFunctionEntity.getId());
                }
            }
        }

        if (task.getFiles() != null && task.getFiles().size() > 0) {
            taskFileDao.inserts(task.getId(), task.getFiles());
        }
    }

    @Override
    @Transactional
    public void updateTask(DiagnosticTaskEntity task) {

//        LambdaUpdateWrapper<DiagnosticTaskEntity> qw = Wrappers.<DiagnosticTaskEntity>lambdaUpdate();

        task.setUpdatedAt(new Date());

//        qw.eq(DiagnosticTaskEntity::getId, task.getId());
        taskDao.update(task, Wrappers.<DiagnosticTaskEntity>lambdaUpdate().eq(DiagnosticTaskEntity::getId, task.getId()));
        // 诊断文件
        taskFileDao.delete(Wrappers.<DiagnosticTaskFileEntity>lambdaUpdate().eq(DiagnosticTaskFileEntity::getTaskId, task.getId()));
        if (task.getFiles() != null && task.getFiles().size() > 0) {
            taskFileDao.inserts(task.getId(), task.getFiles());
        }
        // 诊断条件
        conditionDao.delete(Wrappers.<DiagnosticConditionCopyEntity>lambdaUpdate().eq(DiagnosticConditionCopyEntity::getTaskId, task.getId()));
        if (task.getConditionList() != null && task.getConditionList().size() > 0) {
            conditionDao.inserts(task.getId(), task.getConditionList());
        }

        // 诊断功能 和 ECU
        taskEcuDao.delete(Wrappers.<DiagnosticTaskEcuEntity>lambdaUpdate().eq(DiagnosticTaskEcuEntity::getTaskId, task.getId()));
        if (task.getFunList() != null && task.getFunList().size() > 0) {
            for (DiagnosticFunctionEntity diagnosticFunctionEntity : task.getFunList()) {
                if (diagnosticFunctionEntity.getEcuList() != null && diagnosticFunctionEntity.getEcuList().size() != 0) {
                    taskEcuDao.insertGroupAndEcu(task.getId(), diagnosticFunctionEntity.getId(), diagnosticFunctionEntity.getEcuList());
                } else {
                    taskEcuDao.insertFun(task.getId(), diagnosticFunctionEntity.getId());
                }
            }
        }

//        if (task.getEcus() != null && task.getEcus().size() > 0) {
//            taskEcuDao.insertEcu(task.getId(), task.getEcus());
//        }
    }

    @Override
    public List<DiagnosticTaskEntity> getTasks() {
        return taskDao.selectList(Wrappers.<DiagnosticTaskEntity>lambdaQuery().eq(DiagnosticTaskEntity::getStatus, 0));
    }

    @Override
    public Boolean check(CheckNameReqVo vo) {
        switch (vo.getType()) {
            case "task":
                LambdaQueryWrapper<DiagnosticTaskEntity> qw = Wrappers.<DiagnosticTaskEntity>lambdaQuery();
                qw.eq(DiagnosticTaskEntity::getTaskName, vo.getName());
                if (vo.getId() != null) {
                    qw.ne(DiagnosticTaskEntity::getId, vo.getId());
                }
                qw.select(DiagnosticTaskEntity::getId);
                return taskDao.selectOne(qw) == null;
            case "condition":
                return conditionService.check(vo);
            case "function":
                return functionService.check(vo);
            case "file":
                return fileService.check(vo);
            case "release":
                return campaignService.check(vo);
            default:
                return true;
        }
    }

    @Override
    public Boolean checkStatus(CheckStatusReqVo vo) {
        switch (vo.getType()) {
            case "task":
                LambdaQueryWrapper<DiagnosticTaskCampaignEntity> qw = Wrappers.<DiagnosticTaskCampaignEntity>lambdaQuery();
                qw.eq(DiagnosticTaskCampaignEntity::getTaskId, vo.getId());
                qw.select(DiagnosticTaskCampaignEntity::getId);
                return ObjectUtils.isEmpty(campaignDao.selectList(qw));
            case "condition":
                return conditionService.checkStatus(vo);
            case "function":
                return functionService.checkStatus(vo);
            case "file":
                return fileService.checkStatus(vo);
            default:
                return true;
        }
    }

    @Override
    public List<DiagnosticFunctionEntity> getFunctionByTest(TaskConditionReqVo vo) {
        if (vo.getTaskId() == null) {
            return functionDao.selectList(Wrappers.<DiagnosticFunctionEntity>lambdaQuery().eq(DiagnosticFunctionEntity::getStatus, 0).in(DiagnosticFunctionEntity::getId, vo.getIds()));
        } else {
            return taskEcuDao.selectFunList(vo.getTaskId(), vo.getIds());
        }
    }

}
