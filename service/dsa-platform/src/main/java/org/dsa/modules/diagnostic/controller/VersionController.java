package org.dsa.modules.diagnostic.controller;

import ch.qos.logback.classic.Logger;
import jakarta.annotation.Resource;
import jakarta.mail.internet.MimeUtility;
import lombok.Cleanup;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.annotation.SysLog;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.Md5Utils;
import org.dsa.common.utils.R;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.diagnostic.entity.DVersionInfoEntity;
import org.dsa.modules.diagnostic.entity.DVersionRecordEntity;
import org.dsa.modules.diagnostic.entity.VersionInfoEntity;
import org.dsa.modules.diagnostic.service.VersionService;
import org.dsa.modules.sys.controller.AbstractController;
import org.dsa.modules.sys.entity.FileEntity;
import org.dsa.modules.vehicle.entity.DVehicleTypeEntity;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.annotation.Transformer;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * 版本功能
 *
 */
@RestController
@RequestMapping("version")
public class VersionController extends AbstractController {
	private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass());

	@Resource
	private VersionService versionService;

	@Resource
	private RedisUtils redisUtils;


	@Value("${project.url}")
	private String projectUrl;
	@Value("${file.version.download.url}")
	private String versionDownloadUrl;
	@Value("${file.version.download.trace}")
	private String versionDownloadTraceUrl;

	@Value("${file.version.download.path}")
	private String versionDownloadPath;

	/**
	 * 获取版本树
	 */
	@RequestMapping("/getTreeList")
	public R getTreeList(){
		List<DVehicleTypeEntity> list = versionService.getTreeList();
		return R.ok().put("treeList",list);
	}

	/**
	 * 根据车型类别获取版本列表
	 */
	@RequestMapping("/list")
	public R list(@RequestParam Long vehicleTypeId ,@RequestParam String versionType){
		List<DVersionInfoEntity> list = versionService.list(vehicleTypeId,versionType);
		return R.ok().put("list",list);
	}

	/**
	 * 根据车型类别获取版本列表
	 */
	@GetMapping("/available-list")
	public R availableList(@RequestParam Long vehicleTypeId ,@RequestParam String versionType){
		List<DVersionInfoEntity> list = versionService.availableList(vehicleTypeId,versionType);
		return R.ok().put("list",list);
	}

	/**
	 * 根据车型类别获取版本列表
	 */
	@RequestMapping("/getInfo")
	public R list(@RequestParam Long id ){
		VersionInfoEntity versionEntity = versionService.getVersionInfo(id);
		return R.ok().put("versionInfo",versionEntity);
	}

	/**
	 * 根据车型类别获取版本列表
	 */
	@RequestMapping("/getVersionRecord")
	public R getVersionRecord(@RequestParam Long id ){
		List<DVersionRecordEntity> list = versionService.getVersionRecord(id);
		return R.ok().put("versionRecord",list);
	}

	/**
	 * 检查是否有开发、测试状态 版本信息
	 */
	@RequestMapping("/check")
	public R check(@RequestParam Long vehicleTypeId ,@RequestParam String versionType){
		logger.info("{},check ,input:{},{}",this.getClass(),vehicleTypeId,versionType);
		//保存版本前,判断是否有开发/测试状态的版本.
		versionService.checkVersionStatus(vehicleTypeId,versionType);
		int size = versionService.selectVersions(vehicleTypeId,versionType);
		logger.info("{},end",this.getClass());
		return R.ok().put("versionSize",size);
	}

	/**
	 * 检查是否有开发、测试状态 版本信息
	 */
	@RequestMapping("/checkVersionSize")
	public R checkVersionSize(@RequestParam Long vehicleTypeId ,@RequestParam String versionType){
		logger.info("{},checkVersionSize ,input:{},{}",this.getClass(),vehicleTypeId,versionType);
		//保存版本前,判断是否有开发/测试状态的版本.
		int size = versionService.selectVersions(vehicleTypeId,versionType);
		logger.info("{},end",this.getClass());
		return R.ok().put("versionSize",size);
	}

	/**
	 * 保存版本信息
	 */
	@SysLog("保存版本信息")
	@RequestMapping("/save")
	@Transformer
	public R save(@RequestBody DVersionInfoEntity versionEntity ){
		logger.info("{},save ,input:{}",this.getClass(),versionEntity);
		//保存版本前,判断是否有开发/测试状态的版本.
		versionService.checkVersionStatus(versionEntity.getVehicleTypeId(),versionEntity.getVersionType());
		//保存版本及操作记录
		versionService.save(versionEntity);
		//释放状态
		if(versionEntity.getStatus() != null && Constant.versionStatus.OPEN.getCode().equals(versionEntity.getStatus()) ) {
			//生成下载版本文件
			versionService.buildVersionFile(versionEntity.getVehicleTypeId(),versionEntity.getVersionType());
			//修改车型是否关联版本
			versionService.updateVehicleTypeVersion(versionEntity.getVehicleTypeId(),versionEntity.getVersionType());
		}
		logger.info("{},end",this.getClass());
		return R.ok();
	}

	/**
	 * 保存版本信息
	 */
	@SysLog("复制版本信息")
	@RequestMapping("/copy")
	public R copy(@RequestParam Long id ){
		logger.info("{},cpoy ,input:{}",this.getClass(),id);
		DVersionInfoEntity dVersionInfoEntity = versionService.getDVersionInfoById(id);

		//保存版本前,判断是否有开发/测试状态的版本.
		versionService.checkVersionStatus(dVersionInfoEntity.getVehicleTypeId(),dVersionInfoEntity.getVersionType());

		dVersionInfoEntity.setId(null);
		dVersionInfoEntity.setStatus(Constant.versionStatus.DEV.getCode());
		dVersionInfoEntity.setVersionFileName(null);
		dVersionInfoEntity.setVersionFileMd5(null);
		dVersionInfoEntity.setCreateTime(new Date());

		//保存版本及操作记录
		versionService.save(dVersionInfoEntity);
		logger.info("{},end",this.getClass());
		return R.ok();
	}

	/**
	 * 修改版本信息
	 */
	@SysLog("修改版本信息")
	@RequestMapping("/update")
	@Transformer
	public R update(@RequestBody DVersionInfoEntity versionEntity  ){
		logger.info("{},update ,input:{}",this.getClass(),versionEntity);
		//修改版本及操作记录
		versionService.update(versionEntity);

		//释放状态
		if(versionEntity.getStatus() != null && Constant.versionStatus.OPEN.getCode().equals(versionEntity.getStatus()) ) {
			//生成下载版本文件
			versionService.buildVersionFile(versionEntity.getVehicleTypeId(),versionEntity.getVersionType());
			//修改车型是否关联版本
			versionService.updateVehicleTypeVersion(versionEntity.getVehicleTypeId(),versionEntity.getVersionType());
		}
		logger.info("{},end",this.getClass());
		return R.ok();
	}

	/**
	 * 比对版本  返回 文件url , 文件md5加签串
	 */
	@RequestMapping("/compareVersionFile")
	public R compareVersionFile(@RequestParam String vin,@RequestParam String versionNum, @RequestParam String versionType){
		logger.info("{},compareVersionFile ,input:{},{},{}",this.getClass(),vin,versionNum,versionType);
		DVersionInfoEntity versionInfoEntity = versionService.compareVersionFile(vin,versionNum,versionType);
		logger.info("{},end,return{}",this.getClass(),versionInfoEntity);
		if(versionInfoEntity != null) {
			if(StringUtils.isEmpty(versionInfoEntity.getVersionFileName())){
				return R.ok(Constant.Msg.VERSION_NOT_UPDATE.getMessage())
						.put("fileUrl", "")
						.put("fileMd5", "")
						.put("targetNum","")
						.put("targetType","")
						.put("releaseNote","");
			}else {
				return R.ok().put("fileUrl", versionDownloadUrl + versionInfoEntity.getVersionFileName())
						.put("fileMd5", versionInfoEntity.getVersionFileMd5())
						.put("targetNum",versionInfoEntity.getVersionNum())
						.put("targetType",versionInfoEntity.getTargetType())
						.put("releaseNote",versionInfoEntity.getDescription());
			}
		}else{
			return R.error(Constant.Msg.VERSION_EMPTY).put("fileUrl", "")
					.put("fileMd5", "")
					.put("targetNum","")
					.put("targetType","")
					.put("releaseNote","");
		}
	}

	/**
	 * 比对版本  返回 文件url , 文件md5加签串
	 */
//	@SysLog("下载版本文件")
	@RequestMapping("/download/{fileName}")
	public void downLoadFile(@PathVariable("fileName") String fileName , HttpServletRequest request, HttpServletResponse response){
		try {
			logger.info("filePath={}", versionDownloadPath + fileName);
			// 获取文件的长度
			File file = new File(versionDownloadPath + fileName);
			if (!file.exists()) {
				throw new RRException(Constant.Msg.FILE_NOT_EXIST);
			}
			long fileLength = file.length();

			// 下载
			response.setContentType("application/x-download");
			response.setHeader("Accept-Ranges", "bytes");
			response.setHeader("Content-Disposition", "attachment;" + getName(fileName, request));
			long pos = 0;
			if (null != request.getHeader("Range")) {
				// 断点续传
				response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
				try {
					pos = Long.parseLong(request.getHeader("Range").replaceAll(
							"bytes=", "").replaceAll("-", ""));
				} catch (NumberFormatException e) {
					logger.error(request.getHeader("Range") + " is not Number!");
					pos = 0;
				}
			}
			String contentRange = new StringBuffer("bytes=").append(
					Long.valueOf(pos)).append("-").append(
					Long.valueOf(fileLength - 1)).append("/").append(
					Long.valueOf(fileLength)).toString();
			response.setHeader("Content-Range", contentRange);
			logger.info("Content-Range={}", contentRange);
			response.setHeader("Content-Length", String.valueOf(fileLength - pos));

			// 获取输入流
			@Cleanup
			BufferedInputStream bis = new BufferedInputStream(new FileInputStream(versionDownloadPath + fileName));
			// 输出流
			@Cleanup
			BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream());

			bis.skip(pos);
			byte[] buff = new byte[4096];
			int bytesRead;
			while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
				bos.write(buff, 0, bytesRead);
			}
		} catch (Exception e) {
			logger.error("下载文件异常,输入参数=" + fileName, e);
		}
	}
	/**
	 * 比对版本  返回 文件url , 文件md5加签串
	 */
	@RequestMapping("/compareVersionFileBySign")
	public R compareVersionFileBySign(@RequestParam String vin,@RequestParam String versionNum, @RequestParam String versionType,
									  @RequestParam String systemId,@RequestParam String requestUser,@RequestParam String requestTime,@RequestParam String sign){
		if(StringUtils.isEmpty(vin)
				&& StringUtils.isEmpty(versionType)
				&& StringUtils.isEmpty(systemId)
				&& StringUtils.isEmpty(requestUser)
				&& StringUtils.isEmpty(requestTime)
				&& StringUtils.isEmpty(sign)){
			throw new RRException(Constant.Msg.CHECK_INPUT);
		}

		//md5校验
		String md5 = Md5Utils.CompareVersionFileBySign(vin,versionNum,versionType,requestUser,requestTime,systemId);
		if(!sign.equals(md5)){
			throw new RRException(Constant.Msg.CHECK_INPUT);
		}

		DVersionInfoEntity versionInfoEntity = versionService.compareVersionFile(vin,versionNum,versionType);
		if(versionInfoEntity != null) {
			if(StringUtils.isEmpty(versionInfoEntity.getVersionFileName())){
				return R.ok(Constant.Msg.VERSION_NOT_UPDATE.getMessage()).put("fileUrl", "")
						.put("fileMd5", "")
						.put("targetNum","")
						.put("targetType","")
						.put("releaseNote","");
			}else {
				String uuidStr = UUID.randomUUID().toString();
				redisUtils.set(uuidStr,uuidStr,Constant.VERSION_FILE_DOWNLOAD_TIME);
				return R.ok().put("fileUrl",projectUrl + versionDownloadTraceUrl + versionInfoEntity.getVersionFileName()+"/"+"uuidStr")
						.put("fileMd5", versionInfoEntity.getVersionFileMd5())
						.put("targetNum",versionInfoEntity.getVersionNum())
						.put("targetType",versionInfoEntity.getTargetType())
						.put("releaseNote",versionInfoEntity.getDescription());
			}
		}else{
			return R.error(Constant.Msg.VERSION_EMPTY).put("fileUrl", "")
					.put("fileMd5", "")
					.put("targetNum","")
					.put("targetType","")
					.put("releaseNote","");
		}


	}

	/**
	 * 比对版本  返回 文件url , 文件md5加签串
	 */
	@RequestMapping("/downloadByTraceId/{fileName}/{traceId}")
	public void traceDownload(@PathVariable("fileName") String fileName ,
							  @PathVariable("traceId") String traceId ,
							  HttpServletRequest req, HttpServletResponse response){
		if(StringUtils.isEmpty(traceId)){
			throw new RRException(Constant.Msg.VERSION_FILE_DOWNLOAD_TRACE_FAIL);
		}


		File file = new File(versionDownloadPath + fileName);
		if (!file.exists()) {
			throw new RRException(Constant.Msg.FILE_NOT_EXIST);
		}

		InputStream ips = null;
		OutputStream ops = null;
		try {
			ips =  new FileInputStream(file);
			ops = response.getOutputStream();
			response.setContentType("application/x-download");
			response.setHeader("Content-Disposition", "attachment;" +getName(fileName,req));
			IOUtils.copy(ips, ops);
			ips.close();
			ops.flush();

		} catch (IOException e) {
			logger.error("下载文件出错IOException：" + e.getMessage());
		} catch (Exception e) {
			logger.error("下载文件出错：" + e.getMessage());
		} finally {
			try {
				if(ips != null ) {
					ips.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				if(ops != null) {
					ops.flush();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private String getName(String filename, HttpServletRequest request) throws Exception {
		String userAgent = request.getHeader("userAgent");
		String new_filename = URLEncoder.encode(filename, "UTF8");
		// 如果没有UA，则默认使用IE的方式进行编码，因为毕竟IE还是占多数的
		String rtn = "filename=\"" + new_filename + "\"";
		if (userAgent != null) {
			userAgent = userAgent.toLowerCase();
			// IE浏览器，只能采用URLEncoder编码
			if (userAgent.indexOf("msie") != -1) {
				rtn = "filename=\"" + new_filename + "\"";
			}
			// Opera浏览器只能采用filename*
			else if (userAgent.indexOf("opera") != -1) {
				rtn = "filename*=UTF-8''" + new_filename;
			}
			// Safari浏览器，只能采用ISO编码的中文输出
			else if (userAgent.indexOf("safari") != -1) {
				rtn = "filename=\"" + new String(filename.getBytes("UTF-8"), "ISO8859-1") + "\"";
			}
			// Chrome浏览器，只能采用MimeUtility编码或ISO编码的中文输出
			else if (userAgent.indexOf("applewebkit") != -1) {
				new_filename = MimeUtility.encodeText(filename, "UTF8", "B");
				rtn = "filename=\"" + new_filename + "\"";
			}
			// FireFox浏览器，可以使用MimeUtility或filename*或ISO编码的中文输出
			else if (userAgent.indexOf("mozilla") != -1) {
//    	          rtn = "filename*=UTF-8''" + new_filename;
//    	    	   new_filename = MimeUtility.encodeText(filename, "UTF8", "B");
				rtn = "filename*=UTF-8''" + new String(filename.getBytes("UTF-8"), "ISO8859-1") + "\"";
			}
		}
		return rtn;

	}

	/**
	 * 上传文件
	 * @param file file
	 * @param req req
	 * @return R
	 */
	@RequestMapping("/upload")
	public R upload(@RequestParam("file") MultipartFile file, @RequestParam("versionType") String versionType,
					HttpServletRequest req){
		if(Objects.isNull(file) || StringUtils.isEmpty(file.getOriginalFilename())) {
			return R.error("上传的文件信息不能为空");
		}

		String fileName = file.getOriginalFilename().toLowerCase();
		if (Constant.versionType.ODX.getName().equals(versionType)
				&& !fileName.endsWith(".pdx-r")) {
			return R.error(Constant.Msg.FILE_PDX_R_LIMIT);
		}

		if (Constant.versionType.OTX.getName().equals(versionType)
				&& !fileName.endsWith(".zip")) {
			return R.error(Constant.Msg.FILE_ZIP_LIMIT);
		}

		FileEntity fileEntity = versionService.uploadFiles(file, versionType, req);
		return R.ok().put("data", fileEntity);
	}

	/**
	 * 上传db文件
	 * @param file file
	 * @param versionType versionType
	 * @return R
	 */
	@RequestMapping("/upload-db")
	public R uploadDB(@RequestParam("file") MultipartFile file, @RequestParam("versionType") String versionType) {
		if (file == null || StringUtils.isEmpty(file.getOriginalFilename())) {
			return R.error("上传的文件信息不能为空");
		}

		if (!file.getOriginalFilename().toLowerCase().endsWith(".db")) {
			return R.error(Constant.Msg.FILE_DB_LIMIT);
		}

		FileEntity fileEntity = versionService.uploadDBFiles(file, versionType);
		return R.ok().put("data", fileEntity);
	}

	/**
	 * 上传otx menu文件
	 * @param file file
	 * @return R
	 */
	@RequestMapping("/upload-otx-menu")
	public R uploadOtxMenu(@RequestParam("file") MultipartFile file) {
		if (file == null || StringUtils.isEmpty(file.getOriginalFilename())) {
			return R.error("上传的文件信息不能为空");
		}

		if (!file.getOriginalFilename().toLowerCase().endsWith(".xml")) {
			return R.error(Constant.Msg.FILE_XML_LIMIT);
		}

		FileEntity fileEntity = versionService.uploadOtxMenuFile(file);
		return R.ok().put("data", fileEntity);
	}

}