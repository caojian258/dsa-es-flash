package org.dsa.modules.diagnostic.service.impl;

import ch.qos.logback.classic.Logger;
import com.alibaba.fastjson.JSONObject;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.ObjectUtils;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.diagnostic.service.SynchronizedService;
import org.dsa.modules.vehicle.dao.DVehicleInfoDao;
import org.dsa.modules.vehicle.dao.DVehicleTypeDao;
import org.dsa.modules.vehicle.entity.DVehicleInfoEntity;
import org.dsa.modules.vehicle.entity.DVehicleTypeEntity;
import org.dsa.modules.vehicle.entity.VehicleInfoEntity;
import org.dsa.modules.vehicle.util.VehicleUtils;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


@Service("SynchronizedService")
public class SynchronizedServiceImpl implements SynchronizedService {
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass());

    @Resource
    private RedisUtils redisUtils;

    @Resource
    private DVehicleTypeDao dVehicleTypeDao;

    @Resource
    private DVehicleInfoDao dVehicleInfoDao;

    @Resource
    private VehicleUtils vehicleUtils;

    /**
     * TSP同步接口
     * @param list
     */
    @Override
    public void save(List<VehicleInfoEntity> list) {
        logger.info("Synchronized vehicleInfo ---- begin");
        logger.info("Synchronized vehicleInfo in:" + JSONObject.toJSONString(list));
        //检查入参
        vehicleUtils.checkInput(list);
        for(VehicleInfoEntity vehicleInfoEntity:list) {
            //限制同步东风和重汽
//            boolean filter = Arrays.asList(Constant.TSP_SYNC_MAKER_FILTER).contains(vehicleInfoEntity.getMaker());
//            if (filter){
                //判断层级是否存在,不存在保存db和redis
                Long vehicleTypeId = vehicleUtils.synLevelInfo(vehicleInfoEntity);
                //保存车辆信息
                saveDVehicleInfoEntity(vehicleInfoEntity,vehicleTypeId);
//            }
        }
        logger.info("vehicleInfoEntity ---- end");
    }


    /**
     * 同步DB信息至Redis
     * @return
     */
    @Override
    public void synRedis() {
        List<DVehicleTypeEntity> list  = dVehicleTypeDao.selectList(null);
        for (DVehicleTypeEntity d :list){
            if(d.getLevel() == Constant.VEHICLE_TYPE.MAKER.getLevel()) {
                redisUtils.set(Constant.VEHICLE_TYPE.MAKER.getName() + d.getValue(), d.getId());
            }else if(d.getLevel() == Constant.VEHICLE_TYPE.MODEL.getLevel()) {
                redisUtils.set(Constant.VEHICLE_TYPE.MODEL.getName() + d.getValue(), d.getId());
            }else if(d.getLevel() == Constant.VEHICLE_TYPE.PLATFORM.getLevel()) {
                redisUtils.set(d.getParentId()+"_"+Constant.VEHICLE_TYPE.PLATFORM.getName() + d.getValue(), d.getId());
            }else if(d.getLevel() == Constant.VEHICLE_TYPE.YEAR.getLevel()) {
                redisUtils.set(d.getParentId()+"_"+Constant.VEHICLE_TYPE.YEAR.getName() + d.getValue(), d.getId());
            }else{
                redisUtils.set(d.getParentId()+"_"+Constant.VEHICLE_TYPE.TRIM_LEVEL.getName() + d.getValue(), d.getId());
            }
        }
    }

    /**
     * 保存车辆信息
     */
    private void saveDVehicleInfoEntity(VehicleInfoEntity vehicleInfoEntity, Long vehicleTypeId) {
        DVehicleInfoEntity v = dVehicleInfoDao.selectOneByVin(vehicleInfoEntity.getVin());
        Integer source = vehicleInfoEntity.getSource();
        if(v == null) {
            DVehicleInfoEntity dVehicleInfoEntity = new DVehicleInfoEntity();
            dVehicleInfoEntity.setVin(vehicleInfoEntity.getVin());
            dVehicleInfoEntity.setVehicleTypeId(vehicleTypeId);
            dVehicleInfoEntity.setProductionDate(vehicleInfoEntity.getProductionDate());
            dVehicleInfoEntity.setPlaceOfProduction(vehicleInfoEntity.getPlaceOfProduction());
			dVehicleInfoEntity.setSource(ObjectUtils.isNull(source)?Constant.VEHICLE_SOURCE.TSP_SYNC.getCode(): source);
			dVehicleInfoDao.insert(dVehicleInfoEntity);
        }else{
            v.setVehicleTypeId(vehicleTypeId);
            v.setUpdateTime(new Date());
            v.setProductionDate(vehicleInfoEntity.getProductionDate());
            v.setPlaceOfProduction(vehicleInfoEntity.getPlaceOfProduction());
			v.setSource(ObjectUtils.isNull(source)?Constant.VEHICLE_SOURCE.TSP_SYNC.getCode(): source);
			dVehicleInfoDao.updateById(v);
        }
    }

}