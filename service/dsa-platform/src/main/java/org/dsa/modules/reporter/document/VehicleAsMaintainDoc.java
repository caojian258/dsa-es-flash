package org.dsa.modules.reporter.document;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class VehicleAsMaintainDoc extends ElasticEntity{
    private String sessionInfoId;

    private String sessionId;

    private String vin;

    private String workshop;

    private String userId;

    private String userName;

    private List<Long> vehicleTypeId;

    private String diagnosticId;

    private String ecuName;

    private String didHex;

    private String didName;

    private String didNameTi;

    private String value;

    private String valueTi;

    private String parameterName;

    private String parameterNameTi;

    private Integer didErrorCode;

    private String didErrorMessage;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date lastUpdateTime;
}
