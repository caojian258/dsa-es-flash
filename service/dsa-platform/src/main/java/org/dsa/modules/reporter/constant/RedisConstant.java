package org.dsa.modules.reporter.constant;

public class RedisConstant {

    /***
     * 正在处理的文件的键值
     */
    public final static String FILE_KEY = "reporting:session:file:";

    /***
     * 解析诊断会话 已经做了文件解析的任务标记队列
     */
    public final static String TAG_DIAG_KEY = "reporting:session:tag";

    /***
     * 维护基础数据 cts veh veh_type 队列
     */
    public final static String TAG_BASE_KEY = "reporting:base:tag";

    /***
     * 数据处理 队列
     */
    public final static String TAG_BIZ_KEY = "reporting:biz:tag";

    /***
     * 日期
     */
    public final static String TAG_DATE_KEY = "reporting:biz:date:";

    /***
     * 记录刷写车辆
     */
    public final static String FLASH_VIN_KEY = "reporting:flash:vin";

    /***
     * 记录刷写session信息
     */
    public final static String FLASH_SESSION_KEY = "reporting:flash:session";

    /***
     * 记录已经处理的session log
     */
    public final static String PROCESSED_SESSION_KEY = "reporting:processed:session";


    /***
     * 记录诊断仪应用更新记录的
     */
    public final static String CLIENT_UPDATE_KEY = "reporting:device:update";


    /***
     * 诊断操作CTS统计记录 cts:tag
     */
    public final static String CTS_DAILY_KEY = "reporting:cts:%s";

    /***
     * DTC统计记录 dtc:tag
     */
    public final static String DTC_DAILY_KEY = "reporting:dtc:%s";

    /***
     * 诊断操作CTS ITEM source:vin:cts:day
     */
    public final static String CTS_DAILY_ITEM =  "reporting:%s:%s:%s:%s";


    /***
     * DTC ITEM vin:ecu:dtc:dtc_int:dtc_hex:ti:description:day
     */
    public final static String DTC_DAILY_ITEM = "reporting:%s:%s:%s:%s:%s:%s:%s:%s";
}
