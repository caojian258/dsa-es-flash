package org.dsa.modules.reporter.dto;

import lombok.Data;

@Data
public class WorkshopUserTotalDto {

    /***
     * ID
     */
    String workshopId;

    /***
     * 名称
     */
    String workshopName;

    /***
     * 数量
     */
    Integer value;

}
