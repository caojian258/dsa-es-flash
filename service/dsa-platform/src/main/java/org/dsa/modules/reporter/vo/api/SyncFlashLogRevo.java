package org.dsa.modules.reporter.vo.api;

import lombok.Data;

import java.util.List;

@Data
public class SyncFlashLogRevo {
    private Integer mode; //模式 1工程 2正式
    private String vin; // 车架号
    private Long updateTime; //升级时间 格式:毫秒值
    private String version; //升级后的版本
    List<SyncEcuVersionDto> ecus;
}
