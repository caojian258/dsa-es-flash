package org.dsa.modules.reporter.vo.client;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.modules.reporter.pojo.PageParam;

import jakarta.validation.constraints.NotNull;


@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class AppVersionPageReVo  extends PageParam{

    private String date;

    /***
     * 客户端
     */
    @NotNull(message = "客户端不能为空")
    private String pcid;

    /***
     * 版本
     */
    private String version;

    /***
     * 版本名称
     */
    private String name;

    /**
     * 批处理tag
     * */
    private String tag;

    private Boolean groupBy;

    /***
     * 关键词搜索
     */
    private String message;
}
