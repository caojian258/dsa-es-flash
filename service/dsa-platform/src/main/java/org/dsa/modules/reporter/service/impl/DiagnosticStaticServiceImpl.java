package org.dsa.modules.reporter.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.reporter.constant.ConfigConstant;
import org.dsa.modules.reporter.constant.RedisConstant;
import org.dsa.modules.reporter.entity.CtsDaily;
import org.dsa.modules.reporter.entity.DtcDaily;
import org.dsa.modules.reporter.service.CtsDailyService;
import org.dsa.modules.reporter.service.DiagnosticStaticService;
import org.dsa.modules.reporter.service.DtcDailyService;
import org.dsa.modules.vehicle.entity.DVehicleInfoEntity;
import org.dsa.modules.vehicle.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.Map;

@Slf4j
@Service
public class DiagnosticStaticServiceImpl implements DiagnosticStaticService {

    @Autowired
    DtcDailyService dtcDailyService;

    @Autowired
    CtsDailyService ctsDailyService;

    @Autowired
    VehicleService VehicleService;

    @Autowired
    RedisUtils redisUtils;

    @Override
    public void ctsDaily(String tag) {

        //cts reporting:source:vin:cts:day
        String ctsKey =  String.format(RedisConstant.CTS_DAILY_KEY, tag);
        Map<Object, Object> ctsMap = redisUtils.hmget(ctsKey);
        log.info("cts redis:{}",  JSONObject.toJSONString(ctsMap));
        if(CollectionUtil.isNotEmpty(ctsMap)){
            Iterator entInfo = ctsMap.entrySet().iterator();
            while (entInfo.hasNext()){
                Map.Entry entry = (Map.Entry) entInfo.next();
                String key1 = entry.getKey().toString();
                String value1 = entry.getValue().toString();
                String[] result = key1.split(":");
                log.info("cts {}->{}:{}", key1, JSONObject.toJSONString(result), value1);
                if(result.length == 5){
                    DVehicleInfoEntity vehicle = VehicleService.getOne(result[2]);
                    CtsDaily ctsDaily = new CtsDaily();
                    if(result[1].equals("null")){
                        ctsDaily.setSource(ConfigConstant.DEFAULT_SOURCE);
                    }else{
                        ctsDaily.setSource(result[1]);
                    }

                    ctsDaily.setVin(result[2]);

                    if(null != vehicle){
                        ctsDaily.setVehicleTypeId(vehicle.getVehicleTypeId());
                    }

                    String[] ctsNames =  result[3].split("-");
                    ctsDaily.setCtsName(result[3]);
                    ctsDaily.setCtsDay(result[4]);
                    ctsDaily.setCtsNum(Long.valueOf(value1));

                    if(ctsNames.length == 2){
                        ctsDaily.setFunctionCode(ctsNames[0]);
                        if(!ctsNames[0].equals(ctsNames[1])){
                            ctsDaily.setActionCode(ctsNames[1]);
                        }else{
                            ctsDaily.setCtsName(ctsNames[0]);
                        }
                    }else{
                        ctsDaily.setCtsName(ctsNames[0]);
                        ctsDaily.setFunctionCode(ctsNames[0]);
                    }

                    ctsDailyService.insertData(ctsDaily);
                }

            }
        }
        redisUtils.delete(ctsKey);
    }

    @Override
    public void dtcDaily(String tag) {

        //dtc reporting:vin:ecu:dtc:dtc_int:dtc_hex:ti:description:day
        String dtcKey =  String.format(RedisConstant.DTC_DAILY_KEY, tag);
        Map<Object, Object> dtcMap = redisUtils.hmget(dtcKey);
        log.info("dtc redis:{}",  JSONObject.toJSONString(dtcMap));
        if(CollectionUtil.isNotEmpty(dtcMap)){
            Iterator entInfo = dtcMap.entrySet().iterator();
            while (entInfo.hasNext()) {
                Map.Entry entry = (Map.Entry) entInfo.next();
                String key = entry.getKey().toString();
                String value = entry.getValue().toString();
                String[] result = key.split(":");
                log.info("dtc {}->{}:{}", key, JSONObject.toJSONString(result), value);

                if(result.length == 9){
                    DtcDaily dtcDaily = new DtcDaily();
                    DVehicleInfoEntity vehicle = VehicleService.getOne(result[1]);
                    if(null != vehicle){
                        dtcDaily.setVehicleTypeId(vehicle.getVehicleTypeId());
                    }
                    dtcDaily.setDtcNum(Long.valueOf(value));
                    dtcDaily.setVin(result[1]);
                    dtcDaily.setEcuName(result[2]);
                    dtcDaily.setDtcCode(result[3]);
                    dtcDaily.setDtcCodeInt(result[4]);
                    dtcDaily.setDtcCodeHex(result[5]);
                    dtcDaily.setTi(result[6]);
                    dtcDaily.setDescription(result[7]);
                    dtcDaily.setDtcDay(result[8]);
                    dtcDailyService.insertData(dtcDaily);
                }
            }
        }
        redisUtils.delete(dtcKey);
    }
}
