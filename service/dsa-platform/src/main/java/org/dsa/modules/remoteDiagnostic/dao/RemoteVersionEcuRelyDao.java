package org.dsa.modules.remoteDiagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVersionEcuRelyEntity;

import java.util.List;

@Mapper
public interface RemoteVersionEcuRelyDao extends BaseMapper<RemoteVersionEcuRelyEntity> {



//    @Insert("<script>"
//            + " insert into r_version_ecu_rely (group_id,ecu_id,ecu_version_id,priority) values "
//            + " <foreach collection='ecus' item='item' index='index' separator=','> "
//            + " (#{groupId},#{item.id},#{item.versionId},#{index}+1) "
//            + " </foreach>"
//            + " </script>")
//    void inserts(@Param("groupId") String groupId, @Param("ecus") List<RemoteVehicleEcuEntity> ecus);
    @Insert("<script>"
            + " insert into r_version_ecu_rely (vehicle_version_id,name,group_id,ecu_group_id,ecu_id,ecu_version_id,priority) values "
            + " <foreach collection='list' item='item' index='index' separator=','> "
            + " (#{vehicleVersionId},#{item.name},#{item.groupId},#{item.ecuGroupId},#{item.ecuId},#{item.ecuVersionId},#{index}+1) "
            + " </foreach>"
            + " </script>")
    void inserts(@Param("vehicleVersionId") Long vehicleVersionId,@Param("list") List<RemoteVersionEcuRelyEntity> list);

    public List<RemoteVersionEcuRelyEntity> findEcuVersionDependencyList(@Param("idList") List<Long> idList);
}
