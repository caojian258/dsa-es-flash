package org.dsa.modules.reporter.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.reporter.dto.DtcDailyDto;
import org.dsa.modules.reporter.entity.DtcDaily;
import org.dsa.modules.reporter.vo.diag.DtcChartReVo;

import java.util.List;

public interface DtcDailyService {

    Page<DtcDailyDto> selectPage(DtcChartReVo vo);

    public void insertData(DtcDaily entity);

    public void upsertData(DtcDaily entity);

    public void deleteData(String vin, String day);

    public List<String> findAllEcu();
}
