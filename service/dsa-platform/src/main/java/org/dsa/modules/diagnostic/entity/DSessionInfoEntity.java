package org.dsa.modules.diagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("d_session_info")
public class DSessionInfoEntity implements Serializable
{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 会话id
     */
    private String sessionId;
    /**
     * vin码
     */
    private String vin;
    /**
     * 激活用户id
     */
    private Long activeUserId;
    /**
     * 激活用户显示名
     */
    private String activeDisplayUserName;
    /**
     * 诊断用户id
     */
    private Long diagnosticUserId;
    /**
     * 诊断用户显示名
     */
    private String diagnosticDisplayUserName;
    /**
     * 日志文件地址
     */
    private String logPath;
    /**
     * 日志文件名称
     */
    private String logName;
    /**
     * 来源
     */
    private String source;
    /**
     * 设备信息
     */
    private String hardwareInfo;
    /**
     * 设备信息
     */
    private String pcid;
    /***
     * 经销商ID
     */
    private String workshop;

    /**
     * 诊断开始时间
     */
    private Date startTime;

    /**
     * 诊断结束时间
     */
    private Date endTime;

    /**
     * 结束状态
     */
    private Integer endStatus;

}
