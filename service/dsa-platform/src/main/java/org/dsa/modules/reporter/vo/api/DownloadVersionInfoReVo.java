package org.dsa.modules.reporter.vo.api;

import lombok.Data;

import java.io.Serializable;

@Data
public class DownloadVersionInfoReVo implements Serializable {

    private String versionNo;

    private String versionName;

    private String versionReleaseNote;

    private String status;

}
