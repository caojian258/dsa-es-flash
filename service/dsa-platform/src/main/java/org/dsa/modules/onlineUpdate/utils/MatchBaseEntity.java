package org.dsa.modules.onlineUpdate.utils;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.Map;

/**
 * 匹配规则基类
 */
public abstract class MatchBaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat
    private Date ctime;
    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat
    private Date mtime;
    /**
     * 创建人id
     */
    @TableField(fill = FieldFill.INSERT)
    private Long cuserId;
    /**
     * 修改人id
     */
    @TableField(fill = FieldFill.UPDATE)
    private Long muserId;

    /**
     * 匹配方法
     * @return
     */
    public abstract Boolean match(Map<String,Object> param);
}
