package org.dsa.modules.remoteDiagnostic.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RemoteVersionStatusEnum {

    // 数据类型
    DEV(0),     //开发
    CLOSE(2),   //拒绝
    TEST(1),    //测试
    OPEN(3);    //释放

    @EnumValue
    private final Integer value;
}
