package org.dsa.modules.reporter.vo.flash;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.modules.reporter.pojo.PageParam;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class VehicleStatPageReqVo extends PageParam {

   private List<String> dates;

   private String startDate;

   private String endDate;

   private String vin;

   private List<Long> vehicleTypeIds;
}
