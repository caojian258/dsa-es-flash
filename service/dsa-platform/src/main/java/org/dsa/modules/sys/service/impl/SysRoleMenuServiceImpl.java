package org.dsa.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.dsa.modules.sys.dao.SysRoleMenuDao;
import org.dsa.modules.sys.entity.SysMenuEntity;
import org.dsa.modules.sys.entity.SysRoleMenuEntity;
import org.dsa.modules.sys.service.SysRoleMenuService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;



/**
 * 角色与菜单对应关系
 *
 */
@Service("sysRoleMenuService")
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuDao, SysRoleMenuEntity> implements SysRoleMenuService {

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void saveOrUpdate(Long roleId, List<Long> menuIdList) {
		//先删除角色与菜单关系
		deleteBatch(new Long[]{roleId});

		if(menuIdList.size() == 0){
			return ;
		}

		//保存角色与菜单关系
		for(Long menuId : menuIdList){
			SysRoleMenuEntity sysRoleMenuEntity = new SysRoleMenuEntity();
			sysRoleMenuEntity.setMenuId(menuId);
			sysRoleMenuEntity.setRoleId(roleId);

			this.save(sysRoleMenuEntity);
		}
	}

	@Override
	public List<Long> queryMenuIdList(Long roleId) {
		return baseMapper.queryMenuIdList(roleId);
	}

	@Override
	public int deleteBatch(Long[] roleIds){
		return baseMapper.deleteBatch(roleIds);
	}

	@Override
	public List<Long> queryRoleMenu(List<String> roleList, Integer owningSystem) {
		return baseMapper.queryRoleMenu(roleList,owningSystem);
	}

	@Override
	public List<SysMenuEntity> queryRoleMenuEntity(List<String> roleList,Integer owningSystem) {
		return baseMapper.queryRoleMenuEntity(roleList,owningSystem);
	}

	@Override
	public List<SysMenuEntity> queryAllMenuEntity() {
		return baseMapper.queryAllMenuEntity();
	}

    @Override
    public List<String> queryMenuNameList(String rName) {
		return baseMapper.queryMenuNameList(rName);
    }

}
