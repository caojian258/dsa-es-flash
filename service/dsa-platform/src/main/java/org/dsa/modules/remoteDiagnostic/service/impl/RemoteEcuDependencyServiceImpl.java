package org.dsa.modules.remoteDiagnostic.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.dsa.modules.remoteDiagnostic.dao.RemoteVersionEcuRelyDao;
import org.dsa.modules.remoteDiagnostic.dao.RemoteVersionRecordDao;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVersionEcuRelyEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVersionRecordEntity;
import org.dsa.modules.remoteDiagnostic.service.RemoteEcuDependencyService;
import org.dsa.modules.remoteDiagnostic.service.RemoteVersionRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class RemoteEcuDependencyServiceImpl extends ServiceImpl<RemoteVersionEcuRelyDao, RemoteVersionEcuRelyEntity> implements RemoteEcuDependencyService {

    @Override
    public List<RemoteVersionEcuRelyEntity> getEcuDependencyByVehicleVersion(Long id) {
        return this.list(Wrappers.<RemoteVersionEcuRelyEntity>lambdaQuery().eq(RemoteVersionEcuRelyEntity::getVehicleVersionId, id)
                .orderByAsc(RemoteVersionEcuRelyEntity::getEcuGroupId).orderByAsc(RemoteVersionEcuRelyEntity::getPriority));
    }
}
