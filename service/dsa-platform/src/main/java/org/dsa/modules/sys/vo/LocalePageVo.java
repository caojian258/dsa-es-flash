package org.dsa.modules.sys.vo;

import lombok.Data;

@Data
public class LocalePageVo extends PageParam{
    String locale;

}
