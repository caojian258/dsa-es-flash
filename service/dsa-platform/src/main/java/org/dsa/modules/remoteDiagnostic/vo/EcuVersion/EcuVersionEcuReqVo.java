package org.dsa.modules.remoteDiagnostic.vo.EcuVersion;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString()
public class EcuVersionEcuReqVo {

    private Long id;

    private Long versionId;

    private List<Long> ids;
}
