package org.dsa.modules.remoteDiagnostic.vo.Flash;

import lombok.Data;

import jakarta.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class FlashCampaignAuditVo implements Serializable {

    @NotNull(message = "campaignId Id Not NUll")
    private Long campaignId;

    @NotNull(message = "Campaign Status Not NUll")
    private Integer status;
}
