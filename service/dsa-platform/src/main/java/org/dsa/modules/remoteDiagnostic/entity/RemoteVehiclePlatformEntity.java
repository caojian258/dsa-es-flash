//package org.dsa.modules.remoteDiagnostic.entity;
//
//import com.baomidou.mybatisplus.annotation.IdType;
//import com.baomidou.mybatisplus.annotation.TableField;
//import com.baomidou.mybatisplus.annotation.TableId;
//import com.baomidou.mybatisplus.annotation.TableName;
//import lombok.Data;
//import org.dsa.common.validator.group.AddGroup;
//import org.dsa.common.validator.group.UpdateGroup;
//import org.dsa.modules.remoteDiagnostic.pojo.BaseEntity;
//
//import jakarta.validation.constraints.NotBlank;
//import jakarta.validation.constraints.NotNull;
//import java.io.Serializable;
//import java.util.List;
//
//@Data
//@TableName("r_vehicle_platform")
//public class RemoteVehiclePlatformEntity extends BaseEntity implements Serializable {
//
//    private static final long serialVersionUID = 1L;
//
//    @TableId(type = IdType.AUTO)
//    private Long id;
//
//    @NotBlank(message="车辆平台名称不能为空", groups = {AddGroup.class, UpdateGroup.class})
//    private String name;
//
//    @NotNull(message="车型不能为空", groups = {AddGroup.class, UpdateGroup.class})
//    private Long vehicleTypeId;
//
//    @TableField(exist = false)
//    private String vehicleType;
//
//    private String remark;
//
//    @NotNull(message="状态不能为空", groups = {AddGroup.class, UpdateGroup.class})
//    private Integer status;
//
//    @TableField(exist = false)
//    private List<RemoteVehicleEcuGroupEntity> ecus;
//}
