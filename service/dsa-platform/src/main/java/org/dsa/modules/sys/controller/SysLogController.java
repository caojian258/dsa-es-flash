package org.dsa.modules.sys.controller;

import org.dsa.common.utils.PageUtils;
import org.dsa.common.utils.R;
import org.dsa.modules.sys.service.LdapService;
import org.dsa.modules.sys.service.SysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;


/**
 * 系统日志
 *
 */
@Controller
@RequestMapping("/sys/log")
public class SysLogController {
	@Autowired
	private SysLogService sysLogService;

	@Autowired
	private LdapService ldapService;
	
	/**
	 * 列表
	 */
	@ResponseBody
	@GetMapping("/list")
	public R list(@RequestParam Map<String, Object> params){
//		ldapService.getWorkshop();
//		List l = ldapService.getLdapUser("lipan","Active","2002-06-06");

		/*
		LdapUserEntity ldapUserEntity = new LdapUserEntity();
		ldapUserEntity.setUid("ldm");
		ldapUserEntity.setCn("ldm");
		ldapUserEntity.setSn("ldm");
		ldapUserEntity.setStatus("Active");
		ldapUserEntity.setTelephone("18516583162");
		ldapUserEntity.setAccountCreateDate("2012-12-12 00:00:00");

		ldapService.insertLdapUser(ldapUserEntity);
		*/

		/*
		LdapUserEntity ldapUserEntity = new LdapUserEntity();
		ldapUserEntity.setUid("ldm");
		ldapUserEntity.setCn("ldm");
		ldapUserEntity.setSn("ldm");
		ldapUserEntity.setStatus("Active");
		ldapUserEntity.setTelephone("18516583162");
		ldapUserEntity.setAccountCreateDate("2012-12-12 00:00:00");

		ldapService.updateLdapUser(ldapUserEntity);
		*/

		/*
		LdapUserEntity ldapUserEntity = new LdapUserEntity();
		ldapUserEntity.setUid("ldm");
		ldapUserEntity.setStatus("Disable");

		ldapService.disableLdapUser(ldapUserEntity);
		*/

		PageUtils page = sysLogService.queryPage(params);

		return R.ok().put("page", page);
	}
	
}
