package org.dsa.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


/**
 * 组管理
 *
 */
@Data
@TableName("sys_group")
public class SysGroupEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 组ID
	 */
	@TableId(type = IdType.AUTO)
	private Long groupId;
	/**
	 * 部门ID
	 */
	private Long workshopId;
	/**
	 * 部门名称
	 */
	private String workshopName;
	/**
	 * 组名称
	 */
	private String name;

	/**
	 * 组类别
	 */
	private String groupCategory;
	
	private Integer orderNum;
	@TableLogic
	private Integer delFlag;

	/**
	 * ztree属性
	 */
	@TableField(exist=false)
	private Boolean open;
	@TableField(exist=false)
	private List<?> list;
}
