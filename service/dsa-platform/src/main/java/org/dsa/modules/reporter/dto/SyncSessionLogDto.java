package org.dsa.modules.reporter.dto;

import lombok.Data;

@Data
public class SyncSessionLogDto {

    private WtsInfoDto wtsInfo;

    private VehicleInfoDto vehicleInfo;

    private BasicDiagnosticDto basicDiagnostic;

    private ExecutedCtsDto executedCts;

}
