package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class AppOptionDto implements Serializable {

    private Integer appId;

    private String name;

}
