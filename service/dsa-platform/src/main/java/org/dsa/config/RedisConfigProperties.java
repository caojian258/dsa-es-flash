package org.dsa.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author weishunxin
 * @since 2023-10-10
 */
@Setter
@Getter
@Component
@ConfigurationProperties(prefix = "spring.redis")
public class RedisConfigProperties {

    private String host;

    private Integer port;

    private String password;

    private Integer database;

    @Autowired
    private RedisClusterConfig redisClusterConfig;

    @Setter
    @Getter
    @Component
    @ConfigurationProperties(prefix="spring.redis.cluster")
    public static class RedisClusterConfig {
        private List<String> nodes;
    }

}
