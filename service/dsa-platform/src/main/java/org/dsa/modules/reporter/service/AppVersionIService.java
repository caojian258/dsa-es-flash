package org.dsa.modules.reporter.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dsa.modules.reporter.entity.AppVersion;

import java.util.List;

public interface AppVersionIService extends IService<AppVersion> {
    public List<AppVersion> getPcItems(String pcid, String type, String startDate, String endDate);

}
