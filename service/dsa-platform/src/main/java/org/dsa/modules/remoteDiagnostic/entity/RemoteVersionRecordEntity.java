package org.dsa.modules.remoteDiagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("r_version_record")
public class RemoteVersionRecordEntity implements Serializable
{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 对应数据的id
     */
    private Long valueId;

    /**
     * 数据类型
     */
    private String type;

    /**
     * 操作类型
     */
    private String editType;

    /**
     * 版本状态
     */
    private Integer status;
    /**
     * 创建时间
     */
    private Date createdAt;
    /**
     * 创建用户
     */
    private String createUserName;
    /**
     * 创建用户id
     */
    private Long createBy;

}
