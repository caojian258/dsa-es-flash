package org.dsa.modules.remoteDiagnostic.dto.api;

import lombok.Data;
import lombok.ToString;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskCampaignEntity;

import java.util.List;

@Data
@ToString
public class MatchCampaignDto {
    // 获取的任务列表
    List<DiagnosticTaskCampaignEntity> campaigns;

    // 时间戳
    Long local;
}
