package org.dsa.modules.diagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("d_version_info")
public class DVersionInfoEntity implements Serializable
{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 车型id
     */
    private Long vehicleTypeId;
    /**
     * odx/otx版本
     */
    private String versionType;
    /**
     * 版本号
     */
    private String versionNum;
    /**
     * 上级版本号
     */
    private String parentVersionNum;
    /**
     * 版本文件名称
     */
    private String fileName;
    /**
     * 版本文件存放地址
     */
    private String filePath;
    /**
     * 版本类型（增量/全量）
     */
    private String fileType;

    /**
     * db文件名称
     */
    private String dbFileName;

    /**
     * db文件地址
     */
    private String dbFilePath;

    /**
     * menu文件名称
     */
    private String menuFileName;

    /**
     * menu文件地址
     */
    private String menuFilePath;

    /**
     * 备注
     */
    private String description;
    /**
     * 版本状态
     */
    private String status;
    /**
     * 版本更新文件
     */
    private String versionFileName;

    /**
     * 版本更新文件 Md5 完整性校验使用
     */
    private String versionFileMd5;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建用户id
     */
    private Long createBy;

    /**
     * 版本类型
     */
    @TableField(exist=false)
    private String targetType;

}
