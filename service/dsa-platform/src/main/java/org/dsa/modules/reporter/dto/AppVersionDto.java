package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Objects;

@Data
public class AppVersionDto implements Serializable {

    private String type;

    private Integer appId;

    private String name;

    private String version;

    private Integer versionId;

    private String timestamp;

    private String prevVersion;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AppVersionDto dto = (AppVersionDto) o;
        return Objects.equals(appId, dto.appId) &&
                Objects.equals(name, dto.name) &&
                Objects.equals(version, dto.version) &&
                Objects.equals(type, dto.type) &&
                Objects.equals(versionId, dto.versionId) &&
                Objects.equals(timestamp, dto.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, appId, name, version, versionId, timestamp);
    }

}
