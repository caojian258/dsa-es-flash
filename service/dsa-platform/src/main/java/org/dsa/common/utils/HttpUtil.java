package org.dsa.common.utils;

import org.dsa.common.pojo.CodeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

/**
 * HttpUtil 请求工具类
 */
public class HttpUtil {

    public static final int DEF_CONN_TIMEOUT = 30000;
    public static final int DEF_READ_TIMEOUT = 30000;
    private static final String DEFAULT_CHARSET = "UTF-8";

    private static final Logger logger = LoggerFactory.getLogger(HttpUtil.class);
    /***
     * 初始化证书管理
     */
    static {
        try {
            TrustManager tm = new SSLTrustManager();
            TrustManager[] trustAllCerts = new TrustManager[]{tm};
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, null);
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier((HostnameVerifier) tm);
        } catch (Exception e) {
            e.printStackTrace();
            throw  new RuntimeException(e);
        }
    }

    public static String get(String strUrl) throws IOException {
        StringBuffer buffer = new StringBuffer();
        InputStream inputStream = null;
        InputStreamReader inputStreamReader= null;
        BufferedReader bufferedReader = null;
        try{
            URL url = new URL(strUrl);
            logger.info("http get:"+url);
            HttpURLConnection connect = (HttpURLConnection) url.openConnection();
            connect.setRequestMethod("GET");
            connect.setDoInput(true);
            int code = connect.getResponseCode();
            logger.info("responseCode :"+code);
            if (code==200){
                // 将返回的输入流转换成字符串
                inputStream = connect.getInputStream();
                inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
                bufferedReader = new BufferedReader(inputStreamReader);
                String str = null;
                while ((str = bufferedReader.readLine()) != null) {
                    buffer.append(str);
                }
                bufferedReader.close();
                inputStreamReader.close();
                // 释放资源
                inputStream.close();
                inputStream = null;
                connect.disconnect();
                return buffer.toString();
            }
            return  null;
        } finally {
            if (bufferedReader!=null){
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(inputStreamReader!=null){
                try {
                    inputStreamReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (inputStream!=null){
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public static String post(String strUrl) throws IOException {
        StringBuffer buffer = new StringBuffer();
        InputStream inputStream = null;
        InputStreamReader inputStreamReader= null;
        BufferedReader bufferedReader = null;
        try{
            URL url = new URL(strUrl);
            logger.info("http get:"+url);
            HttpURLConnection connect = (HttpURLConnection) url.openConnection();
            connect.setRequestMethod("POST");
            connect.setDoInput(true);
            connect.setConnectTimeout(5000);// 5秒 连接主机的超时时间（单位：毫秒）
            connect.setReadTimeout(5000);// 5秒 从主机读取数据的超时时间（单位：毫秒）
            int code = connect.getResponseCode();
            logger.info("responseCode :"+code);
            if (code==200){
                // 将返回的输入流转换成字符串
                inputStream = connect.getInputStream();
                inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
                bufferedReader = new BufferedReader(inputStreamReader);
                String str = null;
                while ((str = bufferedReader.readLine()) != null) {
                    buffer.append(str);
                }
                bufferedReader.close();
                inputStreamReader.close();
                // 释放资源
                inputStream.close();
                inputStream = null;
                connect.disconnect();
                return buffer.toString();
            }
            return  null;
        } finally {
            if (bufferedReader!=null){
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(inputStreamReader!=null){
                try {
                    inputStreamReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (inputStream!=null){
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public static CodeMessage post(String url, String params, Map<String, String> headers) throws Exception {
        CodeMessage codeMessage = new CodeMessage();

        HttpURLConnection http = null;

        http = initHttp(url, "POST", headers);

        OutputStream out = http.getOutputStream();
        out.write(params.getBytes(DEFAULT_CHARSET));

        out.flush();
        out.close();

        codeMessage.setCode(String.valueOf(http.getResponseCode()));

        InputStream in = http.getInputStream();
        BufferedReader read = new BufferedReader(new InputStreamReader(in,
                DEFAULT_CHARSET));
        String valueString = null;
        StringBuffer bufferRes = new StringBuffer();
        while ((valueString = read.readLine()) != null) {
            bufferRes.append(valueString);
        }
        in.close();
        if (http != null) {
            http.disconnect();// 关闭连接
        }

        codeMessage.setMessage(bufferRes.toString());
        return codeMessage;
    }

    private static HttpURLConnection initHttp(String url, String method,
                                              Map<String, String> headers) throws Exception {
        URL _url = new URL(url);
        HttpURLConnection http = (HttpURLConnection) _url.openConnection();
        // 连接超时
        http.setConnectTimeout(DEF_CONN_TIMEOUT);
        // 读取超时 --服务器响应比较慢，增大时间
        http.setReadTimeout(DEF_READ_TIMEOUT);
        http.setUseCaches(false);
        http.setRequestMethod(method);
        http.setRequestProperty("Content-Type","application/json;charset=UTF-8");
        if (null != headers && !headers.isEmpty()) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                http.setRequestProperty(entry.getKey(), entry.getValue());
            }
        }
        http.setDoOutput(true);
        http.setDoInput(true);
        http.connect();
        return http;
    }
}
