package org.dsa.modules.onlineUpdate.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.PageUtils;
import org.dsa.modules.onlineUpdate.dao.RuleDao;
import org.dsa.modules.onlineUpdate.dao.RuleUserDao;
import org.dsa.modules.onlineUpdate.dao.RuleWorkshopDao;
import org.dsa.modules.onlineUpdate.dto.RuleDto;
import org.dsa.modules.onlineUpdate.entity.RuleEntity;
import org.dsa.modules.onlineUpdate.entity.RuleUserEntity;
import org.dsa.modules.onlineUpdate.entity.RuleWorkshopEntity;
import org.dsa.modules.onlineUpdate.service.RuleService;
import org.dsa.modules.onlineUpdate.vo.RuleVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;


@Service("ruleService")
public class RuleServiceImpl extends ServiceImpl<RuleDao, RuleEntity> implements RuleService {

    @Autowired
    private RuleUserDao ruleUserDao;

    @Autowired
    private RuleWorkshopDao ruleWorkshopDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String name = MapUtil.getStr(params, "name");
        String startTime = MapUtil.getStr(params, "startTime");
        String endTime = MapUtil.getStr(params, "endTime");
        Integer softwareId = MapUtil.getInt(params, "softwareId");
        Integer type = MapUtil.getInt(params, "type");
        Long page = Convert.convert(Long.class, params.get("page"));
        Long pageSize = Convert.convert(Long.class, params.get("limit"));
        IPage<RuleVo> pages = this.baseMapper.queryPage(new Page<>(page, pageSize), name, startTime, endTime, softwareId, type);
        return new PageUtils(pages);
    }

    @Override
    public RuleDto info(Long id) {
        RuleEntity ruleEntity = this.baseMapper.selectById(id);
        RuleDto ruleDto = new RuleDto();
        BeanUtil.copyProperties(ruleEntity, ruleDto, CopyOptions.create().setIgnoreNullValue(true));
        List<RuleUserEntity> users = ruleUserDao.selectList(new QueryWrapper<RuleUserEntity>().eq("rule_id", id));
        List<RuleWorkshopEntity> workshops = ruleWorkshopDao.selectList(new QueryWrapper<RuleWorkshopEntity>().eq("rule_id", id));
        List<Long> userIds = users.stream().map(RuleUserEntity::getUserId).collect(Collectors.toList());
        List<Long> workshopIds = workshops.stream().map(RuleWorkshopEntity::getWorkshopId).collect(Collectors.toList());
        ruleDto.setUserIds(userIds);
        ruleDto.setWorkshopIds(workshopIds);
        return ruleDto;
    }


    @Transactional
    @Override
    public void saveOrUpdate(RuleDto ruleDto) {
        checkParam(ruleDto);
        if (ObjectUtil.isNull(ruleDto.getId())) {
            //新增
            this.baseMapper.insert(ruleDto);
            ruleDto.getUserIds().forEach(userId -> {
                RuleUserEntity ruleUserEntity = new RuleUserEntity();
                ruleUserEntity.setRuleId(ruleDto.getId());
                ruleUserEntity.setUserId(userId);
                ruleUserDao.insert(ruleUserEntity);
            });
            ruleDto.getWorkshopIds().forEach(workshopId -> {
                RuleWorkshopEntity ruleWorkshopEntity = new RuleWorkshopEntity();
                ruleWorkshopEntity.setRuleId(ruleDto.getId());
                ruleWorkshopEntity.setWorkshopId(workshopId);
                ruleWorkshopDao.insert(ruleWorkshopEntity);
            });
        } else {
            //修改
            this.baseMapper.updateById(ruleDto);
            // 先删除关联用户，经销商
            ruleUserDao.delete(new QueryWrapper<RuleUserEntity>().eq("rule_id", ruleDto.getId()));
            ruleWorkshopDao.delete(new QueryWrapper<RuleWorkshopEntity>().eq("rule_id", ruleDto.getId()));
            // 重新加入
            ruleDto.getUserIds().forEach(userId -> {
                RuleUserEntity ruleUserEntity = new RuleUserEntity();
                ruleUserEntity.setRuleId(ruleDto.getId());
                ruleUserEntity.setUserId(userId);
                ruleUserDao.insert(ruleUserEntity);
            });
            ruleDto.getWorkshopIds().forEach(workshopId -> {
                RuleWorkshopEntity ruleWorkshopEntity = new RuleWorkshopEntity();
                ruleWorkshopEntity.setRuleId(ruleDto.getId());
                ruleWorkshopEntity.setWorkshopId(workshopId);
                ruleWorkshopDao.insert(ruleWorkshopEntity);
            });
        }

    }


    @Override
    public List<RuleEntity> existEffectiveRule(Long code) {
        String dateTimeNow = DateUtil.format(new Date(), DatePattern.NORM_DATETIME_PATTERN);
        return this.baseMapper.selectEffectiveRule(code, dateTimeNow);
    }


    @Override
    public RuleEntity getSoftwareNewestRuleBySoftwareId(Long softwareId) {
        return this.baseMapper.selectSoftwareNewestRule(softwareId);
    }

    @Override
    public void disabledRule(Long softwareId, Integer status) {
        QueryWrapper<RuleEntity> q = new QueryWrapper<RuleEntity>().select("id").eq("software_id", softwareId);
        List<RuleEntity> rules = new ArrayList<>(this.baseMapper.selectList(q));
        rules.forEach(r -> {
            r.setStatus(status);
            this.baseMapper.updateById(r);
        });
    }

    @Override
    public void disabled(Long id) {
        RuleEntity rule = this.baseMapper.selectOne(new QueryWrapper<RuleEntity>().eq("id", id));
        if (Objects.equals(Constant.STATUS.NORMAL.getCode(), rule.getStatus())) {
            rule.setStatus(Constant.STATUS.DISABLED.getCode());
            this.baseMapper.updateById(rule);
        } else {
            rule.setStatus(Constant.STATUS.NORMAL.getCode());
            this.baseMapper.updateById(rule);
        }
    }

    @Override
    public RuleDto queryBySwVersionId(Long swVersionId) {
        LambdaQueryWrapper<RuleEntity> queryWrapper = new QueryWrapper<RuleEntity>().lambda();
        queryWrapper.eq(RuleEntity::getSoftwareVersionId, swVersionId);
        RuleEntity ruleEntity = this.baseMapper.selectOne(queryWrapper);
        RuleDto ruleDto = new RuleDto();
        if (ruleEntity != null) {
            BeanUtil.copyProperties(ruleEntity, ruleDto, CopyOptions.create().setIgnoreNullValue(true));
            List<RuleUserEntity> users = ruleUserDao.selectList(new QueryWrapper<RuleUserEntity>().eq("rule_id", ruleEntity.getId()));
            List<RuleWorkshopEntity> workshops = ruleWorkshopDao.selectList(new QueryWrapper<RuleWorkshopEntity>().eq("rule_id", ruleEntity.getId()));
            List<Long> userIds = users.stream().map(RuleUserEntity::getUserId).collect(Collectors.toList());
            List<Long> workshopIds = workshops.stream().map(RuleWorkshopEntity::getWorkshopId).collect(Collectors.toList());
            ruleDto.setUserIds(userIds);
            ruleDto.setWorkshopIds(workshopIds);
            return ruleDto;
        }
        return null;
    }

    @Override
    public Integer hasPublishCompVersion(Long cVersion) {
        return this.baseMapper.hasPublishCompVersion(cVersion)!=0?1:0;
    }



    private void checkParam(RuleDto ruleDto) {
        // 校验id
        if (ObjectUtil.isNotNull(ruleDto.getId())) {
            RuleEntity rule = this.baseMapper.selectOne(new
                    QueryWrapper<RuleEntity>().eq("id", ruleDto.getId()));
            if (rule == null) {
                throw new RRException(Constant.Msg.DATA_INFO_NULL_ERROR);
            }
        }
        // 校验是否绑定软件
        if (Objects.isNull(ruleDto.getSoftwareId())) {
            throw new RRException(Constant.Msg.NO_BIND_SOFTWARE_ERROR);
        }
        // 空字符串
        if (StringUtils.isBlank(ruleDto.getName().trim())) {
            throw new RRException(Constant.Msg.CHECK_NAME_ERROR);
        }
        // 校验规则是否绑定
        if (Objects.isNull(ruleDto.getSoftwareVersionId())) {
            throw new RRException(Constant.Msg.VERSION_BLANK_ERROR);
        }
        if (Objects.isNull(ruleDto.getStatus())) {
            throw new RRException(Constant.Msg.CHECK_STATUS_ERROR);
        }
        if (Objects.isNull(ruleDto.getStartTime())) {
            throw new RRException(Constant.Msg.CHECK_START_TIME_ERROR);
        }
        if (Objects.isNull(ruleDto.getEndTime())) {
            throw new RRException(Constant.Msg.CHECK_END_TIME_ERROR);
        }
    }
}