package org.dsa.modules.reporter.dto;

import lombok.Data;

@Data
public class FlashEcuVersionDto {

    String BootNo;
    String HardwareVersion;
    String SoftwareVersion;
    String PartCode;
    String PartNo;
}
