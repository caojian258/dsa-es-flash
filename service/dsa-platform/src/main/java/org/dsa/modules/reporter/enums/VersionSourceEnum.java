package org.dsa.modules.reporter.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum VersionSourceEnum {

    AsMaintained(1),
    AsBuild(0),
    ;

    @EnumValue
    private final Integer value;
}
