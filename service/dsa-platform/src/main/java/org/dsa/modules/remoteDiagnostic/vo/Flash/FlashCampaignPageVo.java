package org.dsa.modules.remoteDiagnostic.vo.Flash;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.modules.remoteDiagnostic.pojo.PageParam;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class FlashCampaignPageVo extends PageParam{

    private String vin;

    private Long userId;

    private Integer status;

    private String version;
}
