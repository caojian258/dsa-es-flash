package org.dsa.modules.remoteDiagnostic.vo.Flash;

import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString(callSuper = true)
public class FlashCampaignVo implements Serializable {

    private Long id;

    @NotNull(message = "VIN不能为空")
    private String vin;

    @NotNull(message = "ECU不能为空")
    private Long ecuId;

    @NotNull(message = "ECU版本不能为空")
    private Long ecuVersionId;

    private Integer userId;

    @NotNull(message = "整车不能为空")
    private Long vehicleVersionId;
}
