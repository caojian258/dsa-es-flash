package org.dsa.modules.remoteDiagnostic.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.metadata.data.ReadCellData;
import com.alibaba.excel.util.ConverterUtils;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.SpringContextUtils;
import org.dsa.modules.remoteDiagnostic.dao.CacheDao;
import org.dsa.modules.vehicle.dao.DVehicleInfoDao;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * 直接用map接收数据
 * vin码excel导入数据拦截
 *
 * @author Jiaju Zhuang
 */
@Slf4j
public class VinCodeExcelImportListener extends AnalysisEventListener<Map<Integer, String>> {

//    RedisUtils redisUtils;

    /**
     * 每隔5条存储数据库，实际使用中可以100条，然后清理list ，方便内存回收
     */
    private static final int BATCH_COUNT = 100;
    private static final int CACHE_LENGTH = 1000;

    private String path;
    private String key;
    private CacheDao cacheDao;
    private DVehicleInfoDao vehicleInfoDao;

    private List<String> cachedDataList;
//    private LinkedHashSet<TypedTuple<Object>> cachedDataList = new LinkedHashSet<>();
    private Map<Integer, String> headsMap;
    public VinCodeExcelImportListener() {

    }
    public VinCodeExcelImportListener(String path, String key) {
        this.key = key;
        this.path = path;
    }

    /**
     * 这里会一行行的返回头
     *
     * @param headMap
     * @param context
     */
    @Override
    public void invokeHead(Map<Integer, ReadCellData<?>> headMap, AnalysisContext context) {
        log.info("解析到一条头数据:{}", JSON.toJSONString(headMap));
        headsMap = ConverterUtils.convertToStringMap(headMap, context);
        //获取语种数组,2-size()
        //表头数据校验
        if (CollectionUtils.isEmpty(headsMap) || headsMap.size() == 0) {
            // 抛出表头数据读取异常
            throw new RRException("excel表头设置异常");
        }
        if (headsMap.size() != 1) {
            throw new RRException("请设置表头");
        }
        // 初始化对象
//        cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
        cachedDataList = new ArrayList<>();
//        this.redisUtils = SpringContextUtils.getBean("redisUtils", RedisUtils.class);
        // 如果想转成成 Map<Integer,String>
        // 方案1： 不要implements ReadListener 而是 extends AnalysisEventListener
        // 方案2： 调用 ConverterUtils.convertToStringMap(headMap, context) 自动会转换
    }

    @Override
    public void invoke(Map<Integer, String> data, AnalysisContext context) {
//        log.info("解析到一条数据:{}", JSON.toJSONString(data));
//        cachedDataList.add(new DefaultTypedTuple<Object>(data.get(0), 0.0));
        cachedDataList.add(data.get(0));
//        if (cachedDataList.size() >= BATCH_COUNT) {
//            saveData();
//            cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
//        }

    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        // 2023/2/28 vin码与车辆信息绑定，不存在的vin码不会被缓存，存在的vin码查询车辆信息表，不需要通过缓存表

//        this.cacheDao = SpringContextUtils.getBean("cacheDao", CacheDao.class);
//        this.vehicleInfoDao = SpringContextUtils.getBean("DVehicleInfoDao", DVehicleInfoDao.class);
//        //excel列 index=0 key =1vKey =2...languages
//        if (cachedDataList.size() > CACHE_LENGTH) {
//            int page = cachedDataList.size() % CACHE_LENGTH == 0 ? cachedDataList.size() / CACHE_LENGTH : cachedDataList.size() / CACHE_LENGTH + 1;
//            for (int i = 0; i < page; i++) {
//                List<String> vin = vehicleInfoDao.vinCode(cachedDataList.subList(i * CACHE_LENGTH, Math.min(((i + 1) * CACHE_LENGTH), cachedDataList.size())));
//                if (vin != null && vin.size() != 0) {
//                    cacheDao.inserts(key, vin);
//                }
//            }
//        }else {
//
//            List<String> vin = new ArrayList<>();
//            if (cachedDataList.size() != 0 ){
//                vin = vehicleInfoDao.vinCode(cachedDataList);
//            }
//            if (vin != null && vin.size() != 0) {
//                cacheDao.inserts(key, vin);
//            }
//        }
//        redisUtils.lSet(key, cachedDataList, 60 * 60 * 5);
//        redisUtils.zAdd(key, cachedDataList);
//        redisUtils.expire(key, 60 * 60 * 30);
        log.info("所有数据解析完成！");
        cachedDataList = null;
    }

}