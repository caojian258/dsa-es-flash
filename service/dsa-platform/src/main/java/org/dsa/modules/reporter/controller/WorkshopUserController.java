package org.dsa.modules.reporter.controller;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.dsa.common.utils.R;
import org.dsa.modules.reporter.entity.AnalysisCube;
import org.dsa.modules.reporter.entity.WorkshopUser;
import org.dsa.modules.reporter.entity.WorkshopUserGroup;
import org.dsa.modules.reporter.service.AnalysisCubeService;
import org.dsa.modules.reporter.service.WorkshopUserService;
import org.dsa.modules.reporter.vo.ChartReVo;
import org.dsa.modules.reporter.vo.client.ClientChartResVo;
import org.dsa.modules.reporter.vo.workshop.WorkshopUserPageReVo;
import org.dsa.modules.reporter.vo.workshopUser.ActiveUserVo;
import org.dsa.modules.reporter.vo.workshopUser.UserChartReVo;
import org.dsa.modules.reporter.vo.workshopUser.UserGroupChartReVo;
import org.dsa.modules.reporter.vo.workshopUser.WorkshopsReVo;
import org.dsa.modules.sys.dto.SysActiveUserDto;
import org.dsa.modules.sys.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/workshop/user")
public class WorkshopUserController {

    @Autowired
    WorkshopUserService workshopUserService;

    @Autowired
    AnalysisCubeService cubeService;

    @Autowired
    SysUserService sysUserService;

    @RequestMapping(value = "/activeUser", method = {RequestMethod.POST })
    public R activeUser(@RequestBody ActiveUserVo vo){
        Page<SysActiveUserDto> userPage = workshopUserService.activeUserPage(vo);
        return R.ok().put("data", userPage);
    }

    @RequestMapping(value = "/page", method = {RequestMethod.POST })
    @RequiresPermissions("workshop:user:page")
    public R page(@RequestBody WorkshopUserPageReVo vo){
        Page<WorkshopUser> userPage =  workshopUserService.selectPage(vo);

        return R.ok().put("data", userPage);
    }

    @RequestMapping(value = "/group", method = {RequestMethod.POST })
    @RequiresPermissions("workshop:user:group")
    public R group(){
        List<WorkshopUserGroup> data = workshopUserService.getGroup();
        return R.ok().put("data", data);
    }

    /***
     * 用户分组
     * @param vo
     * @return
     */
    @RequestMapping(value = "/groupChart", method = {RequestMethod.POST })
    @RequiresPermissions("workshop:user:groupChart")
    public R groupChart(@Valid @RequestBody UserGroupChartReVo vo){
        ClientChartResVo resVo = workshopUserService.userGroups(vo);

        return R.ok().put("data", resVo);
    }

    @RequestMapping(value = "/workshopChart", method = {RequestMethod.POST })
    @RequiresPermissions("workshop:user:workshopChart")
    public R workshopGroupChart(@Valid @RequestBody UserChartReVo vo) {

        ClientChartResVo resVo = workshopUserService.workshopGroup(vo);
        return R.ok().put("data", resVo);
    }

    @RequestMapping(value = "/workshops", method = {RequestMethod.POST })
    public R workshops(@RequestBody WorkshopsReVo vo){
       List<Map<String, Object>> r =  sysUserService.workshopList(vo.getName());
        return R.ok().put("data", r);
    }

    /***
     * 通用图表
     * @param vo
     * @return
     */
    @RequestMapping(value = "/chart", method = {RequestMethod.POST })
    @RequiresPermissions("workshop:user:chart")
    public R chart(@Valid @RequestBody ChartReVo vo){
        Map<String, String> tableMap = new HashMap<>();
        tableMap.put("workshop", "d_workshop");
        tableMap.put("workshop_group", "d_workshop_group");
        tableMap.put("workshop_user", "d_workshop_user");
        tableMap.put("workshop_user_group", "d_workshop_user_group");

        AnalysisCube cube = null;

        if(null != vo.getCode()){
            cube =  cubeService.getCubeByCode(vo.getCode());
        }else{
            cube =  cubeService.getCube(tableMap.get(vo.getCategory()), vo.getDimension(), vo.getChart());
        }

        Map<String, Object> map = null;

        if(!CollectionUtil.isEmpty(vo.getCondition())){
            map = vo.getCondition();
        }

        if(StrUtil.isNotBlank(vo.getStartDate())){
            if(null == map){
                map = new HashMap<>();
            }
            map.put("startDate", vo.getStartDate());
        }
        String sql = cubeService.buildSql(cube, map);
        log.info("workshop user chart sql: {}", sql);
        List<Map<String, Object>> items =  cubeService.getData(sql);
        ClientChartResVo resVo = cubeService.formatChartData(items, vo.getDimension(), vo.getChart());
        return R.ok().put("data", resVo);
    }
}
