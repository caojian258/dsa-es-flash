
-- 车辆池
CREATE TABLE public.r_vehicle_pool (
	id bigserial NOT NULL PRIMARY KEY,
	name varchar(512) unique NOT NULL,
	express varchar NOT NULL,
	description_ti varchar(512) NULL,
	status int4 NOT NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now()
);

COMMENT ON TABLE public.r_vehicle_pool IS '车辆池基本信息表';
COMMENT ON COLUMN public.r_vehicle_pool.id IS '自增主键';
COMMENT ON COLUMN public.r_vehicle_pool.express IS '条件表达式';
COMMENT ON COLUMN public.r_vehicle_pool."name" IS '名称';
COMMENT ON COLUMN public.r_vehicle_pool.description_ti IS '描述/备注';
COMMENT ON COLUMN public.r_vehicle_pool.status IS '状态0可用1不可用2开发';
COMMENT ON COLUMN public.r_vehicle_pool.status IS '状态0可用1不可用2开发';


CREATE TABLE public.r_vehicle_pool_condition (
    id bigserial NOT NULL PRIMARY KEY,
    pool_id int8 NOT NULL,
    name varchar(100) NULL,
    identifier varchar(500) NOT NULL,
    type varchar(256) NOT NULL,
    operator varchar(10) NOT NULL,
    condition json NULL,
    description varchar NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now()
);

COMMENT ON TABLE public.r_vehicle_pool_condition IS '车辆池条件表';
COMMENT ON COLUMN public.r_vehicle_pool_condition.pool_id IS '车辆池主键';
COMMENT ON COLUMN public.r_vehicle_pool_condition.name IS '条件名称';
COMMENT ON COLUMN public.r_vehicle_pool_condition.identifier IS '条件标识符(对应车辆池表达式';
COMMENT ON COLUMN public.r_vehicle_pool_condition.type IS '条件类型(Tag、VehicleModel、SalesAreas、PlaceOfProduction、ProductionDate、VehicleVersion、ECUVersion';
COMMENT ON COLUMN public.r_vehicle_pool_condition.operator IS '计算方式(in';
COMMENT ON COLUMN public.r_vehicle_pool_condition.condition IS '值';
COMMENT ON COLUMN public.r_vehicle_pool_condition.description IS '条件描述';

CREATE TABLE public.r_vehicle_pool_result (
	id bigserial NOT NULL,
	pool_id int4 NOT NULL, -- 车辆池主键
	"result" json NULL, -- 结果
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now(),
	CONSTRAINT r_vehicle_pool_result_pkey PRIMARY KEY (id)
);
COMMENT ON TABLE public.r_vehicle_pool_result IS '车辆池结果表';

-- Column comments

COMMENT ON COLUMN public.r_vehicle_pool_result.pool_id IS '车辆池主键';
COMMENT ON COLUMN public.r_vehicle_pool_result."result" IS '结果';||||||| .r0
