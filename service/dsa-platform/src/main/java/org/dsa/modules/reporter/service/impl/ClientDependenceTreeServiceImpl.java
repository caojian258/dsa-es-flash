package org.dsa.modules.reporter.service.impl;


import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.dsa.modules.reporter.dao.*;
import org.dsa.modules.reporter.dto.AppOptionDto;
import org.dsa.modules.reporter.dto.ClientDependencyDto;
import org.dsa.modules.reporter.entity.*;
import org.dsa.modules.reporter.service.ClientDependenceTreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ClientDependenceTreeServiceImpl extends ServiceImpl<ClientDependenceTreeMapper, ClientDependenceTree>  implements ClientDependenceTreeService {

    @Autowired
    ClientDependenceMapper dependenceMapper;

    @Autowired
    CurrentSoftwareInfoMapper currentSoftwareInfoMapper;

    @Autowired
    CurrentComponentInfoMapper currentComponentInfoMapper;

    @Autowired
    FutureSoftwareInfoMapper futureSoftwareInfoMapper;

    @Autowired
    FutureComponentInfoMapper futureComponentInfoMapper;

    @Override
    public List<ClientDependence> queryDependencyByTag(String tag) {
        return dependenceMapper.queryByTag(tag);
    }

    @Override
    public List<ClientDependencyDto> queryDependencyDataByTag(String tag) {
        return dependenceMapper.queryDependencyData(tag);
    }

    @Override
    public List<ClientDependenceTree> formatInertData(List<ClientDependence> list, String tag) {
        List<ClientDependenceTree> trees = new ArrayList<>();
//        @todo 这里留个问题 依赖版本要结合升级状态判断也就是根据 current 表的更新状态
        for (ClientDependence d : list) {

            ClientDependenceTree tree = new ClientDependenceTree();
            tree.setPcid(d.getDeviceId());
            tree.setAppType("software");
            tree.setAppId(d.getSoftwareId());

            CurrentSoftwareInfo cfi = currentSoftwareInfoMapper.querySoftwareInfo(d.getSoftwareId(), d.getSoftwareVersionNumber(), tag);
            FutureSoftwareInfo fsi = futureSoftwareInfoMapper.querySoftwareInfo(d.getSoftwareId(), d.getSoftwareVersionNumber(), tag);

            if(null != cfi){
                tree.setAppName(cfi.getSoftwareName());
                tree.setAppVersion(cfi.getSoftwareVersionName());
            }

            if(null != fsi ){
                if(StrUtil.isBlank(tree.getAppVersion())){
                    tree.setAppVersion(fsi.getSoftwareVersionName());
                }

            }

            tree.setAppVersionNumber(d.getSoftwareVersionNumber());
            if(null != d.getComponentId()){
                tree.setDependencyId(d.getComponentId());
                tree.setDependencyType("component");
            }

            if(tree.getAppId()>0 && StrUtil.isNotBlank(tree.getAppName()) &&
                    StrUtil.isNotBlank(tree.getAppVersion())){
                trees.add(tree);
            }


            if(null != d.getComponentId() && d.getComponentId()>0){
                ClientDependenceTree tree1 = new ClientDependenceTree();
                tree1.setPcid(d.getDeviceId());
                tree1.setAppType("component");
                tree1.setAppId(d.getComponentId());

                CurrentComponentInfo cci = currentComponentInfoMapper.queryComponentInfo(d.getComponentId(), d.getComponentVersionNumber(), tag);
                FutureComponentInfo fci = futureComponentInfoMapper.queryComponentInfo(d.getComponentId(), d.getComponentVersionNumber(), tag);

                if(null != cci){
                    tree1.setAppName(cci.getComponentName());
                    tree1.setAppVersion(cci.getComponentVersionName());
                }

                if(null != fci && StrUtil.isBlank(tree1.getAppVersion())){
                    tree1.setAppVersion(fci.getComponentVersionName());
                }

                tree1.setAppVersionNumber(d.getComponentVersionNumber());

                if(null != d.getDependencyComponentId()){
                    tree1.setDependencyId(d.getDependencyComponentId());
                    tree1.setDependencyType("component");
                }
                if(tree1.getAppId()>0 && StrUtil.isNotBlank(tree1.getAppName()) &&
                        StrUtil.isNotBlank(tree1.getAppVersion())){
                    trees.add(tree1);
                }
            }

            if(null != d.getDependencyComponentId() && d.getDependencyComponentId()>0){
                ClientDependenceTree tree2 = new ClientDependenceTree();
                tree2.setPcid(d.getDeviceId());
                tree2.setAppType("component");
                tree2.setAppId(d.getDependencyComponentId());

                tree2.setAppVersionNumber(d.getDependencyComponentVersionNumber());
                CurrentComponentInfo cci = currentComponentInfoMapper.queryComponentInfo(d.getComponentId(), d.getComponentVersionNumber(), tag);
                FutureComponentInfo fci = futureComponentInfoMapper.queryComponentInfo(d.getComponentId(), d.getComponentVersionNumber(), tag);

                if(null != cci){
                    tree2.setAppName(cci.getComponentName());
                    tree2.setAppVersion(cci.getComponentVersionName());
                }

                if(null != fci && StrUtil.isBlank(tree2.getAppVersion())){
                    tree2.setAppVersion(fci.getComponentVersionName());
                }

                if(tree2.getAppId()>0 && StrUtil.isNotBlank(tree2.getAppName()) &&
                        StrUtil.isNotBlank(tree2.getAppVersion())){
                    trees.add(tree2);
                }
            }
        }
        return trees;
    }

    @Override
    public List<ClientDependenceTree> getDependenceListByDeviceId(String pcid) {
         List<ClientDependenceTree> trees = baseMapper.queryByDeviceId(pcid);
        List<ClientDependenceTree> result = new ArrayList<>();
         if(CollectionUtil.isNotEmpty(trees)){
             List<Integer> appIds = trees.stream().map(ClientDependenceTree::getAppId).distinct().collect(Collectors.toList());
             Map<Integer, String> appNameList = new HashMap<>();
             List<AppOptionDto> nameIds =  baseMapper.queryAppName(pcid, appIds);
             for (AppOptionDto dto: nameIds) {
                 if(StrUtil.isBlank(appNameList.get(dto.getAppId()))){
                     appNameList.put(dto.getAppId(), dto.getName());
                 }
             }
             for (ClientDependenceTree t: trees) {
                 if(StrUtil.isNotBlank(appNameList.get(t.getAppId()))){
                     t.setAppName(appNameList.get(t.getAppId()));
                 }
             }
             result = trees.stream().distinct().collect(Collectors.toList());
         }
        return result;
    }

    @Override
    public List<Map<String, Object>> formatTreeData(List<ClientDependenceTree> list) {

        List<Map<String, Object>> treeMap = new ArrayList<>();
        //software 集合
        List<Map<String, Object>> softwareList =  list.stream()
                .filter(i->i.getAppType().equals("software"))
                .map(item -> {
                    String name = "";
                    if(StrUtil.isNotBlank(item.getAppName())){
                        name = item.getAppName();
                    }
                    if(StrUtil.isNotBlank(item.getAppVersion())){
                        name += "_" +item.getAppVersion();
                    }
                    Map<String, Object> parentMap = new HashMap<>();
                    parentMap.put("id", item.getAppId());
                    parentMap.put("name", name);
                    parentMap.put("value", item.getAppVersion());
                    return parentMap;
                }).distinct().collect(Collectors.toList());

        System.out.println("softwareList:");
        System.out.println(JSONObject.toJSONString(softwareList));

        //software 依赖集合
        Map<String,List<Integer>> softwareDependenceIds = new HashMap<>();

        for (Map<String, Object> sm: softwareList){
            List<Integer> dids = list.stream()
                    .filter(i->i.getAppType().equals("software"))
                    .map(item -> {
                        return item.getDependencyId();
                    }).distinct().collect(Collectors.toList());

            softwareDependenceIds.put(sm.get("name").toString(), dids);
        }

        //组件集合及其依赖集合
        List<Map<String, Object>> componentList =  list.stream()
                .filter(i->i.getAppType().equals("component"))
                .map(item -> {
                    String name = "";
                    if(StrUtil.isNotBlank(item.getAppName())){
                        name = item.getAppName();
                    }
                    if(StrUtil.isNotBlank(item.getAppVersion())){
                        name += "_" +item.getAppVersion();
                    }
                    Map<String, Object> parentMap = new HashMap<>();
                    parentMap.put("id", item.getAppId());
                    parentMap.put("name", name);
                    parentMap.put("value", item.getAppVersion());
                    if(null != item.getDependencyId() && item.getDependencyId().equals(item.getAppId())){
                        parentMap.put("dependenceId", 0);
                    }else {
                        parentMap.put("dependenceId", item.getDependencyId());
                    }

                    return parentMap;
                }).distinct().collect(Collectors.toList());

        System.out.println("componentList:");
        System.out.println(JSONObject.toJSONString(componentList));

        Map<Integer, List<Integer>> dependencyIdMap = new HashMap<>();

        for (Map<String, Object> m: componentList){
            List<Integer> idList = dependencyIdMap.get(m.get("id"));
            if(null == idList){
                idList = new ArrayList<>();
            }
            idList.add((int)m.get("dependenceId"));
            dependencyIdMap.put((int)m.get("id"), idList);
        }

        for (Map<String, Object> softwareName: softwareList) {

            Map<String, Object> softwareMap = softwareName;

            List<Integer> dependencyIds = softwareDependenceIds.get(softwareName.get("name").toString());
            System.out.println("dependencyIds");
            System.out.println(JSONObject.toJSONString(dependencyIds));

            if(null != dependencyIds && dependencyIds.size()>0){


                List<Map<String, Object>> children = componentList.stream()
                        .filter(i ->  dependencyIds.indexOf(i.get("id"))>-1)
                    .map(i -> {
                        Map<String, Object> map = new HashMap<>();
                        map.put("id", i.get("id"));
                        map.put("name", i.get("name"));
                        map.put("value", i.get("value"));
                    return map;
                }).distinct().collect(Collectors.toList());

                for (Map<String, Object> childMap: children){
                    if( null != dependencyIdMap.get(childMap.get("id")) && dependencyIdMap.get(childMap.get("id")).size()>0){
                        List<Map<String, Object>> dependenceChild = createChildrenTreeMap(componentList, dependencyIdMap.get(childMap.get("id")));
                        childMap.put("children", dependenceChild);
                    }
                }

                softwareMap.put("children", children);
            }

            treeMap.add(softwareMap);
        }

        return  treeMap;
    }

    @Override
    public void deleteByDeviceId(String pcid) {
        baseMapper.deleteByDeviceId(pcid);
    }

    private List<Map<String, Object>> createTreeMap(List<Map<String, Object>> lists, Integer did){
        List<Map<String, Object>> tree = new ArrayList<>();

        for (Map<String, Object> catelog : lists) {
            if(catelog.get("id").equals(did)){
                Map<String, Object> map = new HashMap<>();
                map.put("id", catelog.get("id"));
                map.put("name", catelog.get("name"));
                map.put("value", catelog.get("value"));
                tree.add(map);
            }
        }

        return tree.stream().distinct().collect(Collectors.toList());
    }

    private List<Map<String, Object>> createChildrenTreeMap(List<Map<String, Object>> lists, List<Integer> didList){
        List<Map<String, Object>> tree = new ArrayList<>();

        for (Map<String, Object> catelog : lists) {
            if(didList.contains((int)catelog.get("id"))){
                Map<String, Object> map = new HashMap<>();
                map.put("id", catelog.get("id"));
                map.put("name", catelog.get("name"));
                map.put("value", catelog.get("value"));
                tree.add(map);
            }
        }

        return tree.stream().distinct().collect(Collectors.toList());
    }

    private List<Map<String, Object>> createTree(List<ClientDependenceTree> lists, Integer did) {
        List<Map<String, Object>> tree = new ArrayList<>();

        for (ClientDependenceTree catelog : lists) {
            if( catelog.getAppId() == did){
                Map<String, Object> map = new HashMap<>();
                map.put("name", catelog.getAppName()+"_"+catelog.getAppVersion());
                map.put("value", catelog.getAppVersionNumber());

                if(null != catelog.getDependencyId() && catelog.getDependencyId()>0 ){
                    List<Map<String, Object>> children = createTree(lists, catelog.getDependencyId());
                    if(null!=children && children.size()>0){
                        List<Map<String, Object>> uc = children.stream().distinct().collect(Collectors.toList());
                        map.put("children", uc);
                    }
                }
                tree.add(map);
            }
        }

        return tree.stream().distinct().collect(Collectors.toList());
    }
}
