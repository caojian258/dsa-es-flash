package org.dsa.modules.diagnostic.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class VersionInfoEntity implements Serializable
{
    private static final long serialVersionUID = 2L;

    /**
     * 版本详细
     */
    DVersionInfoEntity dVersionInfoEntity;
    /**
     * 操作记录
     */
    private List<DVersionRecordEntity> list;
}
