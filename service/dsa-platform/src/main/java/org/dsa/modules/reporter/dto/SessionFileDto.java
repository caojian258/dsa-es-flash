package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class SessionFileDto implements Serializable {

    private String sessionDir;

    private String fileName;

    private String vinCode;

    private String pcid;

    private String path;

    private String sqlitePath;

    private String statusPath;

    private String sessionId;

    private Date handlerTime;

    private String fileMD5;

    private String sharedDataPath;

    private String sessionStatePath;

    private Long userId;

    private String userName;
}
