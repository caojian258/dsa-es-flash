package org.dsa.modules.remoteDiagnostic.config;

import lombok.Data;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
public class EcuFlashConfig {
}
