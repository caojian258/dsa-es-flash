package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.entity.CurrentComponentInfo;

import java.util.List;

@Mapper
public interface CurrentComponentInfoMapper extends BaseMapper<CurrentComponentInfo> {
    public CurrentComponentInfo queryComponentInfo(@Param("id") Integer id, @Param("versionNumber") Integer versionNumber, @Param("tag") String tag);

    public List<CurrentComponentInfo> queryComponentInfoByTag(@Param("deviceId") String deviceId, @Param("tag") String tag, @Param("idList") List<Integer> idList);

}
