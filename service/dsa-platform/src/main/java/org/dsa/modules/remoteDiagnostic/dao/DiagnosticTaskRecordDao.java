package org.dsa.modules.remoteDiagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.*;
import org.dsa.modules.remoteDiagnostic.dto.page.DiagnosticTaskRecordDto;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskRecordEntity;

import java.util.List;

@Mapper
public interface DiagnosticTaskRecordDao extends BaseMapper<DiagnosticTaskRecordEntity> {


    @Select("select * from r_diag_task_record i where i.campaign_id = #{taskId} ")
    public DiagnosticTaskRecordEntity getInfoByTask(@Param("taskId") Long taskId);

    @Select("select * from r_diag_task_record i where i.session_id = #{sessionId} and i.vin = #{vin} ")
    DiagnosticTaskRecordEntity getInfoBySessionId(@Param("sessionId") String sessionId, @Param("vin") String vin);

//    @Select("<script>"
//            + "select *,i.session_id as sessionId from r_diag_task_record i "
//            + "<where> "
//            + " <if test=\"vin!=null and vin!='' \">"
//            + " i.vin ilike concat('%',#{vin},'%') "
//            + " </if>"
//            + " <if test=\"id!=null and id!=''  \"> "
//            + " i.task_id = #{id} "
//            + " </if>"
//            + "</where> "
//            + " </script>")
//    @Results({
//            @Result(property = "detail", column = "sessionId",
//                    one = @One(select = "org.dsa.modules.remoteDiagnostic.dao.DiagnosticTaskDetailDao.getOneBySession"))
//    })
//    Page<DiagnosticTaskRecordEntity> selectPage(Page<DiagnosticTaskRecordEntity> page, @Param("vin") String vin, @Param("id") Long id, @Param("type") String type);


    @Select("<script>"
            + "select i.*,k.type,k.action,k.code,k.message,k.created_at from r_diag_task_record i" +
            " left join LATERAL(select * from  r_diag_task_detail j where j.session_id = i.session_id order by j.id desc limit 1) k on i.session_id  = k.session_id "
            + "<where> "
            + " <if test=\"vin!=null and vin!='' \">"
            + " and i.vin ilike concat('%',#{vin},'%') "
            + " </if>"
            + " <if test=\"id!=null and id!=''  \"> "
            + " and i.campaign_id = #{id} "
            + " </if>"
            + " <if test=\"type!=null and type!=''  \"> "
            + " and k.type = #{type} "
            + " </if>"
            + "</where> "
            + " </script>")
    Page<DiagnosticTaskRecordDto> selectPage(Page<DiagnosticTaskRecordEntity> page, @Param("vin") String vin, @Param("id") Long id, @Param("type") String type);

    @Select(" select i.* from r_diag_task_record i left join r_diag_task_active j on i.session_id = j.session_id where i.campaign_id = #{taskId} and j.status = 0 ")
    List<DiagnosticTaskRecordEntity> getListByTask(@Param("taskId") Long id);

    @Select(" select i.* from r_diag_task_record i where i.campaign_id = #{taskId} and i.vin = #{vin} ")
    DiagnosticTaskRecordEntity getInfoByTaskAndVin(@Param("taskId") Long id, @Param("vin") String vin);


}
