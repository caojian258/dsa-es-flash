package org.dsa.modules.remoteDiagnostic.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(callSuper = true)
public class PublicFileReqVo {

    private String path;
}
