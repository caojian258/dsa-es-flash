package org.dsa.modules.reporter.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public enum SqlOperatorEnum {

    EQUAL("="),
    GREATER(">"),
    GREATER_EQUAL(">="),
    LESS("<"),
    LESS_EQUAL("<="),
    NOT("<>"),
    IN("IN"),
    LIKE("LIKE"),
    IS("IS"),
    ;
    @EnumValue
    private final String value;
}
