package org.dsa.modules.reporter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
@TableName("client_current_component_info")
public class CurrentComponentInfo extends BaseEntity{

    @TableId(type = IdType.AUTO)
    private Long id;

    private String deviceId;

    private Integer softwareId;

    private Integer componentId;
    private String componentName;
    private String componentVersionName;
    private Integer componentVersionNumber;
    private String componentInstallPath;

    private Date startUpdateTime;
    private Date endUpdateTime;
    private Integer updateFailCount;
    private Integer updateResult;
    /***
     * 0: 无状态
     * 1: 升级
     * 2：修复
     * 3：回退
     */
    private Integer installAction;

    /***
     * 0=未安装 ；
     * 1=无状态；
     * 2=执行中；
     * 3=下载中；
     * 4=安装中；
     * 5=修复中；
     * 6=回退中；
     * 7=卸载中
     */
    private Integer componentStatus;
    private Integer downloadProgress;

    private String fileSize;
    private String fileMd5;

    private String releaseNotes;
    private String featureDescription;
    private String tag;
    //目前没有用到
    private String sessionId;

    private Integer softwareVersionNumber;

    private Integer updateType;

    private Integer currentUpdateType;
}
