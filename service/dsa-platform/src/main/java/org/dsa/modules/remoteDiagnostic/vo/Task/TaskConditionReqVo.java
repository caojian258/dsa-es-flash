package org.dsa.modules.remoteDiagnostic.vo.Task;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString()
public class TaskConditionReqVo {

    private Long taskId;

    private List<Long> ids;
}
