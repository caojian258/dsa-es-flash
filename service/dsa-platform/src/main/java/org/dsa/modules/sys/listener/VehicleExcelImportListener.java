package org.dsa.modules.sys.listener;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.ReUtil;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.metadata.data.ReadCellData;
import com.alibaba.excel.util.ConverterUtils;
import com.alibaba.excel.util.ListUtils;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.SpringContextUtils;
import org.dsa.modules.diagnostic.service.SynchronizedService;
import org.dsa.modules.remoteDiagnostic.dao.RVehicleTagDao;
import org.dsa.modules.remoteDiagnostic.dao.RemoteVehicleTagDao;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleTagEntity;
import org.dsa.modules.reporter.util.LocalUtil;
import org.dsa.modules.sys.dao.SysLogDao;
import org.dsa.modules.sys.entity.SysLogEntity;
import org.dsa.modules.sys.utils.SpringContextHolder;
import org.dsa.modules.vehicle.entity.VehicleInfoEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;


/**
 * 直接用map接收数据
 * 多语言excel导入数据拦截
 *
 * @author Jiaju Zhuang
 */
@Slf4j
public class VehicleExcelImportListener extends AnalysisEventListener<Map<Integer, String>> {
    /**
     * 每隔5条存储数据库，实际使用中可以100条，然后清理list ，方便内存回收
     */
    private static final int BATCH_COUNT = 100;
    private List<Map<Integer, String>> cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
    private Map<Integer, String> headsMap = null;
    private RVehicleTagDao rVehicleTagDao;
    private RemoteVehicleTagDao tagDao;


    /**
     * 这里会一行行的返回头
     *
     * @param headMap
     * @param context
     */
    @Override
    public void invokeHead(Map<Integer, ReadCellData<?>> headMap, AnalysisContext context) {
        log.info("解析到一条头数据:{}", JSON.toJSONString(headMap));
        headsMap = ConverterUtils.convertToStringMap(headMap, context);
        //获取语种数组,2-size()
        //表头数据校验
        if (CollectionUtils.isEmpty(headsMap) || headsMap.size() == 0) {
            // 抛出表头数据读取异常
            throw new RRException("excel表头设置异常");
        }
        // 如果想转成成 Map<Integer,String>
        // 方案1： 不要implements ReadListener 而是 extends AnalysisEventListener
        // 方案2： 调用 ConverterUtils.convertToStringMap(headMap, context) 自动会转换
    }

    @Override
    public void invoke(Map<Integer, String> data, AnalysisContext context) {
//        log.info("解析到一条数据:{}", JSON.toJSONString(data));
        //过滤空行
        if (!ObjectUtil.equal(JSON.toJSONString(data), "{}")) {
            cachedDataList.add(data);
        }

        if (cachedDataList.size() >= BATCH_COUNT) {
            saveData();
            cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
        }

    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        //excel列
        saveData();
        log.info("所有车辆数据解析完成！");
    }

    /**
     * 翻译文件数据入库
     */
    @Transactional(rollbackFor = Exception.class)
    private void saveData() {
        SysLogDao sysLogDao = SpringContextHolder.getBean(SysLogDao.class);
        try {
            if (cachedDataList.size() == 0) {
                throw new RRException(LocalUtil.get("message.697"));
            }
            AtomicInteger index = new AtomicInteger(1);
            SynchronizedService service = SpringContextHolder.getBean(SynchronizedService.class);
            List<VehicleInfoEntity> vehicleInfoEntities = new ArrayList<>(16);
            List<String> errorMsgList = new ArrayList<>(16);
            cachedDataList.forEach(data -> {
                StringBuffer errorMsg = new StringBuffer("第" + index.get() + "行 ");
                VehicleInfoEntity vehicleInfoEntity = new VehicleInfoEntity();
                if (ObjectUtil.isNotNull(data.get(0)) && ObjectUtil.isNotNull(data.get(1)) && ObjectUtil.isNotNull(data.get(3))) {
                    String vin = data.get(0);
                    boolean checkVIN = ReUtil.isMatch("^[A-Za-z][A-Za-z0-9]{16}$", vin);
                    boolean checkYear = ReUtil.isMatch("\\d{4}", data.get(4));
                    boolean checkPDate = ReUtil.isMatch("\\d{4}-\\d{2}-\\d{2}", data.get(7));
                    if (!checkVIN || (ObjectUtil.isNotNull(data.get(4)) && !checkYear) || (ObjectUtil.isNotNull(data.get(7)) && !checkPDate)) {
                        errorMsg.append(vin);
                        if (!checkVIN) {
                            errorMsg.append(", VIN码格式错误！");
                        }
                        if (ObjectUtil.isNotNull(data.get(4)) && !checkYear) {
                            errorMsg.append(", 年份格式有误！");
                        }
                        if (ObjectUtil.isNotNull(data.get(7)) && !checkPDate) {
                            errorMsg.append(", 生产日期格式有误！");
                        }
                        errorMsg.append("<br/>");
                        errorMsgList.add(errorMsg.toString());
                    } else {
                        vehicleInfoEntity.setVin(data.get(0));
                        vehicleInfoEntity.setMaker(data.get(1));
                        vehicleInfoEntity.setPlatform(data.get(3));
                        vehicleInfoEntity.setModel(data.get(2));
                        vehicleInfoEntity.setYear(data.get(4));
                        vehicleInfoEntity.setTrimLevel(data.get(5));
                        vehicleInfoEntity.setPlaceOfProduction(data.get(6));
                        vehicleInfoEntity.setProductionDate(data.get(7));
                        vehicleInfoEntity.setSource(Constant.VEHICLE_SOURCE.REMOTE.getCode());
                        vehicleInfoEntities.add(vehicleInfoEntity);
                        if (ObjectUtil.isNotNull(data.get(8))) {
                            List<Long> tagList = getTagList(data.get(8));
                            if (tagList.size() > 0) {
                                insertVehTag(vin, tagList);
                            }
                        }
                    }
                } else {
                    if (ObjectUtil.isNull(data.get(0))) {
                        errorMsg.append(", VIN码为空");
                    }
                    if (ObjectUtil.isNull(data.get(1))) {
                        errorMsg.append(", 厂商为空");
                    }
                    if (ObjectUtil.isNull(data.get(3))) {
                        errorMsg.append(", 车型为空");
                    }
                    errorMsg.append("<br/>");
                    errorMsgList.add(errorMsg.toString());
                }
                index.getAndIncrement();
            });
            if (errorMsgList.size() == 0) {
                service.save(vehicleInfoEntities);
            } else {
                throw new RRException(String.join("", errorMsgList));
            }
        } catch (RRException re) {
            log.error("excel数据异常");
            throw re;
        } catch (Exception ex) {
            log.error("车辆持久化异常");
            ex.printStackTrace();
//            saveLog(ex,sysLogDao);
            throw ex;
        }
    }

    private void saveLog(Exception ex, SysLogDao sysLogDao) {
        SysLogEntity sysLogEntity = new SysLogEntity();
        sysLogEntity.setOperation("远程服务器翻译库持久化");
        sysLogEntity.setCreateDate(new Date());
        sysLogEntity.setMethod("MultiLanguageExcelImportListener->dataSaveDb()");
        sysLogEntity.setResult(ex.getMessage().substring(0, 200));
        sysLogDao.insert(sysLogEntity);
    }

    public List<Long> getTagList(String tagList) {
        List<Long> tagIds = new ArrayList<>();
        List<RemoteVehicleTagEntity> tags = new ArrayList<>();
        if (tagList.contains(",")) {
            String[] split = tagList.split(",");
            for (String s : split) {
                RemoteVehicleTagEntity remoteVehicleTagEntity = new RemoteVehicleTagEntity();
                remoteVehicleTagEntity.setTagName(s);
                tags.add(remoteVehicleTagEntity);
            }
        } else {
            RemoteVehicleTagEntity remoteVehicleTagEntity = new RemoteVehicleTagEntity();
            remoteVehicleTagEntity.setTagName(tagList);
            tags.add(remoteVehicleTagEntity);
        }
        if (tags.size() > 0) {
            //批量插入tag表
            this.tagDao = SpringContextUtils.getBean("remoteVehicleTagDao", RemoteVehicleTagDao.class);
            tagDao.inserts(tags);
        }
        tagIds = tags.stream().map(RemoteVehicleTagEntity::getId).collect(Collectors.toList());
        return tagIds;
    }

    private void insertVehTag(String vin, List<Long> tagList) {
        if (tagList.size() > 0) {
            //批量插入中间表
            this.rVehicleTagDao = SpringContextUtils.getBean("RVehicleTagDao", RVehicleTagDao.class);
            rVehicleTagDao.inserts(vin, tagList);
        }
    }
}