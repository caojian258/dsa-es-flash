package org.dsa.common.pojo;

import java.io.Serializable;


/**
 *  错误描述类
 */

public class CodeMessage implements Serializable {

	private String code;
	
	private String message;

	public CodeMessage() {
		super();
	}

	public CodeMessage(String code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("{");
		if (code != null) {
			builder.append("code=");
			builder.append(code);
			builder.append("_");
		}
		if (message != null) {
			builder.append("message=");
			builder.append(message);
		}
		builder.append("}");
		return builder.toString();
	}
	
}
