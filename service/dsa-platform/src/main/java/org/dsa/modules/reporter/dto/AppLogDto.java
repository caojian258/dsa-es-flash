package org.dsa.modules.reporter.dto;


import lombok.Data;

import java.util.Date;

@Data
public class AppLogDto extends AppVersionDto{

    private String result;

    private Integer appId;

    private String updateOid;

    private Date time;
}
