package org.dsa.modules.diagnostic.service;

import org.dsa.common.utils.Constant;
import org.dsa.modules.diagnostic.service.impl.*;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;

@Service("diagnosticMqFactory")
public class DiagnosticMqFactory {

    @Resource
    BeginServiceImpl beginServiceImpl;
    @Resource
    EndServiceImpl endServiceImpl;
    @Resource
    VHHSServiceImpl vHHSServiceImpl;
    @Resource
    IdentificationServiceImpl identificationServiceImpl;
    @Resource
    DtcReadServiceImpl dtcReadServiceImpl;
    @Resource
    DtcDataGridServiceImpl dtcDataGridService;
    @Resource
    DtcClearServiceImpl dtcClearService;


    public DiagnosticMqService getFunction(String functionCode ,String actionCode) {
        if(Constant.Function.BEGIN.getFunctionCode().equals(functionCode)){
            return beginServiceImpl;
        }else if(Constant.Function.END.getFunctionCode().equals(functionCode)){
            return endServiceImpl;
        }else if(Constant.Function.VHSS.getFunctionCode().equals(functionCode)){
            return vHHSServiceImpl;
        }else if(Constant.Function.IDENTIFICATION.getFunctionCode().equals(functionCode)){
            //读标识数据
            return identificationServiceImpl;
        }else if(Constant.Function.DTC.getFunctionCode().equals(functionCode)&&Constant.Action.DTC_READ.getActionCode().equals(actionCode)){
            //读取DTC数据
            return dtcReadServiceImpl;
        }else if(Constant.Function.DTC.getFunctionCode().equals(functionCode)&&Constant.Action.DTC_SNAPSHOT.getActionCode().equals(actionCode)){
            //读取DTC冻结帧数据
            return dtcDataGridService;
        }else if(Constant.Function.DTC.getFunctionCode().equals(functionCode)&&Constant.Action.DTC_CLEARDTC.getActionCode().equals(actionCode)){
            return dtcClearService;
        }
        else {
            return null;
        }
    }

}
