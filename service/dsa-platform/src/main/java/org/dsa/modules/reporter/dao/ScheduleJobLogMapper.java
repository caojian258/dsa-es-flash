package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.entity.ScheduleJobLog;

import java.util.List;

@Mapper
public interface ScheduleJobLogMapper extends BaseMapper<ScheduleJobLog> {


    public List<ScheduleJobLog> getJobLogList(@Param("jobId") Long jobId);

    public List<ScheduleJobLog> getList(@Param("status") Integer status, @Param("page") Integer page, @Param("pageSize") Integer size);
}
