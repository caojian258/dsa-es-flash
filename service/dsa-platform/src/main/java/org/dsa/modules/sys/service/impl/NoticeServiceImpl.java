package org.dsa.modules.sys.service.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.map.MapUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.AESUtils;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.FileHelper;
import org.dsa.common.utils.PageUtils;
import org.dsa.common.utils.Query;
import org.dsa.common.utils.ShiroUtils;
import org.dsa.modules.sys.dao.NoticeDao;
import org.dsa.modules.sys.dto.SysNoticeDTO;
import org.dsa.modules.sys.entity.FileEntity;
import org.dsa.modules.sys.entity.SysNoticeEntity;
import org.dsa.modules.sys.enums.NoticeStatusEnum;
import org.dsa.modules.sys.service.NoticeService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.DigestUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author weishunxin
 * @since 2023-06-20
 */
@Slf4j
@Service
public class NoticeServiceImpl extends ServiceImpl<NoticeDao, SysNoticeEntity> implements NoticeService {

    @Value("${file.notice.path}")
    private String noticePath;
    @Value("${file.notice.img_path}")
    private String noticeImgPath;
    @Value("${project.url}")
    private String projectUrl;

    @Override
    public FileEntity uploadNotice(MultipartFile file) {
        File fileDir = new File(noticeImgPath);

        if (!fileDir.exists()) {
            boolean b = fileDir.mkdirs();
            if (!b) {
                throw new RRException("mkdir failed.");
            }
        }

        String timePrefix = new Date().getTime() + "";
        String filename = timePrefix + Constant.TIME_PREFIX + file.getOriginalFilename();
        File sourcefile = new File(noticeImgPath + filename);
        try {
            FileUtils.copyInputStreamToFile(file.getInputStream(), sourcefile);
        } catch (IOException e) {
            log.error("uploadNotice error...", e);
            throw new RRException(Constant.Msg.VERSION_FILE_ERROR);
        }

        FileEntity fileEntity = new FileEntity();
        fileEntity.setRealName(filename);
        fileEntity.setName(file.getOriginalFilename());
        fileEntity.setUrl(noticeImgPath + filename);
        return fileEntity;
    }

    @Override
    public void saveNotice(SysNoticeDTO noticeDTO) {
        if (StringUtils.isEmpty(noticeDTO.getLocale())) {
            noticeDTO.setLocale(Constant.LOCALS[1]);
        }

        if(StringUtils.isEmpty(noticeDTO.getTitle()) || noticeDTO.getTitle().length() > 30
                || StringUtils.isEmpty(noticeDTO.getContent()) || noticeDTO.getContent().length() > 150) {
            throw new RRException(Constant.Msg.CHECK_INPUT);
        }

        String noticeImage = this.imgToBase64(noticeDTO.getFilePath());
        String rssFile = this.genRss(noticeDTO.getTitle(), noticeDTO.getContent(), noticeImage);
        String md5;
        try {
            md5 = DigestUtils.md5DigestAsHex(Files.newInputStream(Paths.get(rssFile)));
        } catch (IOException e) {
            log.error("get file md5 error.", e);
            throw new RRException(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }

        noticeDTO.setStatus(NoticeStatusEnum.NOT_EFFECTIVE.getCode());
        noticeDTO.setCreatedAt(new Date());
        noticeDTO.setCreatedUserId(ShiroUtils.getUserId());

        SysNoticeEntity entity = new SysNoticeEntity();
        BeanUtils.copyProperties(noticeDTO, entity);
        entity.setFileName(FileHelper.getFileName(rssFile));
        entity.setFilePath(rssFile);
        entity.setFileMd5(md5);
        super.save(entity);
    }

    @Override
    public void updateNotice(SysNoticeEntity entity) {
        SysNoticeEntity noticeEntity = this.get(entity.getId());
        if (Objects.isNull(noticeEntity)) {
            throw new RRException(Constant.Msg.CHECK_INPUT);
        }

        if (entity.getStatus() < NoticeStatusEnum.EFFECTIVE.getCode()) {
            throw new RRException("不能修改为未生效状态");
        }

        if (NoticeStatusEnum.EFFECTIVE.getCode().equals(entity.getStatus())
                && Objects.isNull(entity.getExpireDay())) {
            throw new RRException("生效时不能没有有效期");
        }

        entity.setUpdatedUserId(ShiroUtils.getUserId());
        entity.setUpdatedAt(new Date());
        super.updateById(entity);
    }

    @Override
    public void deleteNotice(Integer[] ids) {
        if (ids == null || ids.length < 1) {
            return;
        }

        List<Integer> idList = Arrays.asList(ids);

        LambdaQueryWrapper<SysNoticeEntity> dw = Wrappers.lambdaQuery();
        dw.in(SysNoticeEntity::getId, idList);
        baseMapper.delete(dw);
    }

    @Override
    public PageUtils pageList(Map<String, Object> params) {
        // params
        Integer status = MapUtil.getInt(params, "status");

        // page query
        IPage<SysNoticeEntity> iPage = new Query<SysNoticeEntity>().getPage(params);
        LambdaQueryWrapper<SysNoticeEntity> qw = Wrappers.lambdaQuery();
        if (Objects.nonNull(status)) {
            if (NoticeStatusEnum.EXPIRED.getCode().equals(status)) {
                qw.lt(SysNoticeEntity::getExpireDay, new Date());
            } else {
                qw.eq(SysNoticeEntity::getStatus, status);
            }
        }
        qw.orderByDesc(SysNoticeEntity::getCreatedAt);

        IPage<SysNoticeEntity> page = super.page(iPage, qw);
        return new PageUtils(page);
    }

    @Override
    public SysNoticeEntity get(Integer id) {
        return super.getById(id);
    }

    @Override
    public List<SysNoticeEntity> availableList(String locale, Integer limitNum) {
        if (StringUtils.isEmpty(locale)) {
            locale = Constant.LOCALS[1];
        }

        if (Objects.isNull(limitNum)) {
            limitNum = 5;
        }

        LambdaQueryWrapper<SysNoticeEntity> qw = Wrappers.lambdaQuery();
        qw.eq(SysNoticeEntity::getStatus, NoticeStatusEnum.EFFECTIVE.getCode())
                .eq(SysNoticeEntity::getLocale, locale)
                .ge(SysNoticeEntity::getExpireDay, new Date())
                .orderByAsc(SysNoticeEntity::getExpireDay)
                .last("limit " + limitNum);

        List<SysNoticeEntity> notices = baseMapper.selectList(qw);

        notices.forEach((e) -> {
            String encrypt = AESUtils.encrypt(e.getFilePath(), AESUtils.AES_KEY);
            Assert.notNull(encrypt, "encrypt can not be null.");
            String url;
            url = projectUrl + "/sys/file/security-download?params=" + URLEncoder.encode(encrypt, StandardCharsets.UTF_8);
            e.setUrl(url);
            e.setFilePath(null);
        });
        return notices;
    }

    private String imgToBase64(String image) {
        File imgFile = new File(image);
        try (InputStream in = Files.newInputStream(imgFile.toPath());
             ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            byte[] buf = new byte[1024];
            int len;

            while ((len = in.read(buf)) != -1) {
                out.write(buf, 0, len);
            }

            byte[] result = out.toByteArray();
            return Base64.getEncoder().encodeToString(result);
        } catch (Exception e) {
            log.error("imgToBase64 error...", e);
            throw new RRException("imgToBase64 error...");
        }
    }


    private String genRss(String noticeTitle, String noticeContent, String noticeImage) {
        String content = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<rss version=\"2.0\">\n" +
                "  <channel>\n" +
                "    <title></title>\n" +
                "    <description></description>\n" +
                "    <item>\n" +
                "      <title>\n" +
                "        " +
                noticeTitle + "\n" +
                "      </title>\n" +
                "      <description>\n" +
                "        <![CDATA[\n" +
                "        <p>" + noticeContent + "</p>\n" +
                "        " + "<p style=\"text-align: center;\">" +
                "<img src=\"" + "data:image/jpg;base64," + noticeImage + "\" /></p>\n" +
                "        " + "]]>\n" +
                "      </description>\n" +
                "      <source>DSA</source>\n" +
                "    </item>\n" +
                "  </channel>\n" +
                "</rss>";

        String timePrefix = new Date().getTime() + "";
        String filePath = noticePath + timePrefix + Constant.TIME_PREFIX + "notice.rss";
        FileUtil.writeUtf8String(content, new File(filePath));
        return filePath;
    }

}
