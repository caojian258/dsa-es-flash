package org.dsa.modules.reporter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("client_upgrade_result")
public class ClientUpgrade implements Serializable {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String deviceId;

    private Integer updateId;

    private Long workshopId;

    private String workshopName;

    private String username;

    private String userDisplayName;

    private Integer softwareId;

    private Integer componentId;

    private Date startTime;

    private Date endTime;

    private Date removeTime;

    private Integer failUpdateCount;

    /***
     * 0,software软件, 1,componen组件
     */
    private Integer updateTarget;

    /***
     * 0,success成功
     * 1, fail失败
     * 3 取消
     */
    private Integer updateResult;

    private String softwareCurrentVersion;

    private String componentCurrentVersion;

    private String softwareTargetVersion;

    private String componentTargetVersion;

    private Integer componentCurrentVersionNumber;

    private Integer componentTargetVersionNumber;

    private Integer softwareCurrentVersionNumber;

    private Integer softwareTargetVersionNumber;

    /***
     * 0 none 无状态
     * 1,upgrade(升级)
     * 2,repair(修复)
     * 3,rollback(回退)
     * 4,uninstall(卸载)
     */
    private Integer installAction;

    private Date ctime;

    private String tag;

    private String sessionId;
}
