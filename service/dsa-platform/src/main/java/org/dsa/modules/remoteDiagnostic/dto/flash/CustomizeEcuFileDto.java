package org.dsa.modules.remoteDiagnostic.dto.flash;

import lombok.Data;

import java.io.Serializable;

@Data
public class CustomizeEcuFileDto implements Serializable {

    private String ecuName;

    private String ecuVersionName;

    private String encryptionKey;

    private String encryptionMethod;

    private String fileDownloadUrl;

    private String fileDownloadName;

    private Long fileDownloadSize;

    private String fileDownloadMd5;
}
