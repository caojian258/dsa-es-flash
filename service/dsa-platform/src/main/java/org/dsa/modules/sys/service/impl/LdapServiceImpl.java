package org.dsa.modules.sys.service.impl;

import org.dsa.modules.sys.entity.LdapUserEntity;
import org.dsa.modules.sys.entity.LdapWorkshopEntity;
import org.dsa.modules.sys.service.LdapService;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.DistinguishedName;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;
import javax.naming.NamingException;
import javax.naming.directory.*;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.ldap.query.LdapQueryBuilder.query;

@Service
public class LdapServiceImpl  implements LdapService {

    @Resource
    private LdapTemplate ldapTemplate;

    public List<LdapWorkshopEntity> getWorkshop() {
        return ldapTemplate.search(query().where("objectclass").is("dealerOrg"), new AttributesMapper() {
            @Override
            public LdapWorkshopEntity mapFromAttributes(Attributes attributes) throws NamingException {
                LdapWorkshopEntity ldapWorkshopEntity = new LdapWorkshopEntity();
                ldapWorkshopEntity.setCn((String) attributes.get("cn").get());
                ldapWorkshopEntity.setSn((String) attributes.get("sn").get());
                ldapWorkshopEntity.setOu((String) attributes.get("ou").get());
                ldapWorkshopEntity.setDescription((String) attributes.get("description").get());
                ldapWorkshopEntity.setPostalAddress((String) attributes.get("postalAddress").get());

                return ldapWorkshopEntity;
            }
        });
    }

    @Override
    public void insertWorkshop(LdapWorkshopEntity ldapWorkshopEntity) {
        BasicAttribute ba = new BasicAttribute("objectclass");
        ba.add("dealerOrg"); //此处的inceptioDealerPerson对应的是core.schema文件中的objectClass：inceptioDealerPerson
        Attributes attr = new BasicAttributes();
        attr.put(ba);
        //必填属性，不能为null也不能为空字符串
        attr.put("cn", ldapWorkshopEntity.getCn());
        attr.put("sn", ldapWorkshopEntity.getSn());
        attr.put("ou", ldapWorkshopEntity.getOu());

        //可选字段需要判断是否为空，如果为空则不能添加
        if (ldapWorkshopEntity.getDescription() != null  && ldapWorkshopEntity.getDescription().length() > 0) {
            attr.put("description", ldapWorkshopEntity.getDescription());
        }
        if (ldapWorkshopEntity.getPostalAddress() != null && ldapWorkshopEntity.getPostalAddress().length() > 0) {
            attr.put("postalAddress", ldapWorkshopEntity.getPostalAddress());
        }

        //bind方法即是添加一条记录。
        ldapTemplate.bind(getDealerDn(ldapWorkshopEntity.getOu()), null, attr);
    }

    @Override
    public void updateWorkshop(LdapWorkshopEntity ldapWorkshopEntity) {
        List<ModificationItem> mList = new ArrayList<ModificationItem>();

        //可选字段需要判断是否为空，如果为空则不能添加
        if (ldapWorkshopEntity.getDescription() != null  && ldapWorkshopEntity.getDescription().length() > 0) {
            mList.add(new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("description",ldapWorkshopEntity.getDescription())));
        }
        if (ldapWorkshopEntity.getPostalAddress() != null && ldapWorkshopEntity.getPostalAddress().length() > 0) {
            mList.add(new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("postalAddress",ldapWorkshopEntity.getPostalAddress())));
        }
        if (mList.size() > 0) {
            ModificationItem[] mArray = new ModificationItem[mList.size()];
            for (int i = 0; i < mList.size(); i++) {
                mArray[i] = mList.get(i);
            }
            //modifyAttributes 方法是修改对象的操作，与rebind（）方法需要区别开
            ldapTemplate.modifyAttributes(getDealerDn(ldapWorkshopEntity.getOu()),  mArray);
        }
    }

    @Override
    public void disableWorkshop(LdapWorkshopEntity ldapWorkshopEntity) {

    }

    @Override
    public List<LdapUserEntity> getLdapUser(String uid,String status,String accountCreateDate) {
        return ldapTemplate.search(
                query().where("objectclass").is("inceptioDealerPerson")
                    .and("uid").is(uid)
                    .and("status").is(status)
                    .and("accountCreateDate").lte(accountCreateDate),
                new AttributesMapper() {
            @Override
            public LdapUserEntity mapFromAttributes(Attributes attributes) throws NamingException {
                LdapUserEntity ldapUserEntity = new LdapUserEntity();
                ldapUserEntity.setCn((String) attributes.get("cn").get());
                ldapUserEntity.setSn((String) attributes.get("sn").get());
                ldapUserEntity.setUid((String) attributes.get("uid").get());
                ldapUserEntity.setStatus((String) attributes.get("status").get());
                ldapUserEntity.setTelephone((String) attributes.get("telephoneNumber").get());
                ldapUserEntity.setAccountCreateDate((String) attributes.get("accountCreateDate").get());

                return ldapUserEntity;
            }
        });
    }

    @Override
    public void insertLdapUser(LdapUserEntity ldapUserEntity) {
        BasicAttribute ba = new BasicAttribute("objectclass");
        ba.add("inceptioDealerPerson"); //此处的inceptioDealerPerson对应的是core.schema文件中的objectClass：inceptioDealerPerson
        Attributes attr = new BasicAttributes();
        attr.put(ba);
        //必填属性，不能为null也不能为空字符串
        attr.put("cn", ldapUserEntity.getCn());
        attr.put("sn", ldapUserEntity.getSn());
        attr.put("uid", ldapUserEntity.getUid());

        //可选字段需要判断是否为空，如果为空则不能添加
        if (ldapUserEntity.getStatus() != null  && ldapUserEntity.getStatus().length() > 0) {
            attr.put("status", ldapUserEntity.getStatus());
        }
        if (ldapUserEntity.getTelephone() != null && ldapUserEntity.getTelephone().length() > 0) {
            attr.put("telephoneNumber", ldapUserEntity.getTelephone());
        }
        if (ldapUserEntity.getAccountCreateDate() != null && ldapUserEntity.getAccountCreateDate().length() > 0) {
            attr.put("accountCreateDate", ldapUserEntity.getAccountCreateDate());
        }
        if (ldapUserEntity.getOu() != null && ldapUserEntity.getOu().length() > 0) {
            attr.put("ou", ldapUserEntity.getOu());
        }
        //bind方法即是添加一条记录。
        ldapTemplate.bind(getDn(ldapUserEntity.getUid()), null, attr);

    }

    @Override
    public void updateLdapUser(LdapUserEntity ldapUserEntity) {
        List<ModificationItem> mList = new ArrayList<ModificationItem>();

        //可选字段需要判断是否为空，如果为空则不能添加
        if (ldapUserEntity.getStatus() != null  && ldapUserEntity.getStatus().length() > 0) {
            mList.add(new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("status",ldapUserEntity.getStatus())));
        }
        if (ldapUserEntity.getTelephone() != null && ldapUserEntity.getTelephone().length() > 0) {
            mList.add(new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("telephoneNumber",ldapUserEntity.getTelephone())));
        }
        if (ldapUserEntity.getAccountCreateDate() != null && ldapUserEntity.getAccountCreateDate().length() > 0) {
            mList.add(new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("accountCreateDate",ldapUserEntity.getAccountCreateDate())));
        }
        if (ldapUserEntity.getOu() != null && ldapUserEntity.getOu().length() > 0) {
            mList.add(new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("ou",ldapUserEntity.getOu())));
        }
        if (mList.size() > 0) {
            ModificationItem[] mArray = new ModificationItem[mList.size()];
            for (int i = 0; i < mList.size(); i++) {
                mArray[i] = mList.get(i);
            }
            //modifyAttributes 方法是修改对象的操作，与rebind（）方法需要区别开
            ldapTemplate.modifyAttributes(getDn(ldapUserEntity.getUid()),  mArray);
        }
    }

    @Override
    public void disableLdapUser(LdapUserEntity ldapUserEntity) {
        List<ModificationItem> mList = new ArrayList<ModificationItem>();

        //可选字段需要判断是否为空，如果为空则不能添加
        if (ldapUserEntity.getStatus() != null  && ldapUserEntity.getStatus().length() > 0) {
            mList.add(new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("status",ldapUserEntity.getStatus())));
        }
        if (mList.size() > 0) {
            ModificationItem[] mArray = new ModificationItem[mList.size()];
            for (int i = 0; i < mList.size(); i++) {
                mArray[i] = mList.get(i);
            }
            //modifyAttributes 方法是修改对象的操作，与rebind（）方法需要区别开
            ldapTemplate.modifyAttributes(getDn(ldapUserEntity.getUid()),  mArray);
        }
    }

    /**
     * 得到dn
     * @param uid
     * @return
     */
    private DistinguishedName getDn(String uid) {
        //得到根目录，也就是配置文件中配置的ldap的根目录
        DistinguishedName newContactDN = new DistinguishedName("cn=appuser");
        // 添加cn，即使得该条记录的dn为"cn=cn,根目录",例如"cn=abc,dc=testdc,dc=com"
        newContactDN.add("uid", uid);
        return newContactDN;
    }

    /**
     * 得到dn
     * @param ou
     * @return
     */
    private DistinguishedName getDealerDn(String ou) {
        //得到根目录，也就是配置文件中配置的ldap的根目录
        DistinguishedName newContactDN = new DistinguishedName("cn=dealer");
        // 添加cn，即使得该条记录的dn为"cn=cn,根目录",例如"cn=abc,dc=testdc,dc=com"
        newContactDN.add("ou", ou);
        return newContactDN;
    }

}
