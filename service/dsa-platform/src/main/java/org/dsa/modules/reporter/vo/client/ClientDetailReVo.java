package org.dsa.modules.reporter.vo.client;

import lombok.Data;

import jakarta.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class ClientDetailReVo implements Serializable {

    /***
     * 客户端
     */
    @NotNull(message = "客户端不能为空")
    private String pcid;

}
