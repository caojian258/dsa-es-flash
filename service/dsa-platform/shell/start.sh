#!/bin/bash
#start ssh
/usr/sbin/sshd -D &
#env
echo 'export PATH=$PATH:/usr/local/go/bin' >> /etc/profile
source /etc/profile
#path
cd "/usr/local/product"
#builc go file
go build xjar.go
#run diag
./xjar java --add-opens java.base/jdk.internal.loader=ALL-UNNAMED --add-opens java.base/java.lang=ALL-UNNAMED --add-opens java.base/java.net=ALL-UNNAMED --add-opens=java.base/java.lang.invoke=ALL-UNNAMED --add-opens=java.base/java.nio=ALL-UNNAMED -jar ./dsa-platform-1.0.0.xjar
#./xjar java -jar ./dsa-platform-1.0.0.xjar
