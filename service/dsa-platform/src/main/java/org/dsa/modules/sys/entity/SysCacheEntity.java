package org.dsa.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("sys_cache")
public class SysCacheEntity {

    private String key;

    private String value;
}
