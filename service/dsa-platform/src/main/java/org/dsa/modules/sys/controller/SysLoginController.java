package org.dsa.modules.sys.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.UUID;
import cn.hutool.core.map.MapUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.dsa.common.annotation.SysLog;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.R;
import org.dsa.common.utils.RedisKeys;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.sys.entity.*;
import org.dsa.modules.sys.form.SysLoginForm;
import org.dsa.modules.sys.redis.KeyConstant;
import org.dsa.modules.sys.service.*;
import org.dsa.modules.sys.vo.workshop.LocationClientVo;
import org.dsa.modules.sys.vo.workshop.WorkshopClientResponseVo;
import org.dsa.modules.sys.vo.workshop.WorkshopClientVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import jakarta.annotation.Resource;

import javax.imageio.ImageIO;

import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.*;


/**
 * 登录相关
 */
@RestController
public class SysLoginController extends AbstractController {
    @Resource
    private SysUserService sysUserService;
    @Resource
    private SysRoleService sysRoleService;
    @Resource
    private SysUserTokenService sysUserTokenService;
    @Resource
    private SysCaptchaService sysCaptchaService;
    @Autowired
    private SysRoleMenuService sysRoleMenuService;
    @Resource
    private RedisUtils redisUtils;
    @Autowired
    private SysUserLicenseService sysUserLicenseService;

    @Resource
    private SysWorkshopsIService sysWorkshopsIService;

    @Resource
    SysLocationService sysLocationService;
    @Autowired
    private SysMenuService sysMenuService;

    @Value("${license.oem.code}")
    private String oemCode;

    /**
     * 半小时 单位s
     */
    public static final int DISABLED_EXPIRE = 60 * 30;

    /**
     * 验证码
     */
    @GetMapping("captcha.jpg")
    public void captcha(HttpServletResponse response, HttpServletRequest request) {
        try {
            response.setHeader("Cache-Control", "no-store, no-cache");
            response.setContentType("image/jpeg");
            // 4位uuid
            String uuid = UUID.randomUUID().toString().substring(0, 4);
            //获取图片验证码
            BufferedImage image = sysCaptchaService.getCaptcha(uuid);
            String key = request.getSession().getId() + "captcha";
            redisUtils.set(key, uuid, 5 * 60);
            ServletOutputStream out = response.getOutputStream();
            ImageIO.write(image, "jpg", out);
            IOUtils.closeQuietly(out);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * 验证码登录
     */
    @PostMapping("/login")
    public Map<String, Object> clientLogin(@RequestBody SysLoginForm form, HttpServletRequest request) {
        try {
            String[] locales = new String[]{"en", "ch"};
            String locale = form.getLocale();
            if (StringUtils.isBlank(locale) || !Arrays.asList(locales).contains(locale)) {
                locale = "ch";
            }
            String userName = form.getUsername();
            String usernameKey = KeyConstant.getLoginErrorTimes(userName);
            String count = redisUtils.get(usernameKey);

            Map<Object, Object> params = MapUtil.builder()
                    .put("form", form)
                    .put("locale", locale)
                    .put("usernameKey", usernameKey)
                    .put("count", count)
                    .build();
            if (!Objects.equals(userName, Constant.SUPER_ADMIN_USER_NAME)) {
                // 校验登录次数，达到登录上限，24小时内不允许登录
                validateLoginCount(params);
            }
            // 验证码校验
            validateCaptcha2(request, form, locale);
            SysUserEntity user = sysUserService.queryByUserName(userName);
            //用户校验
            validateUser(user, params);
            DisplayUserEntity userEntity = buildDisPlayUser(user);
            // 登录成功，重置登录限制次数
            redisUtils.delete(usernameKey);
            String token = sysUserTokenService.createToken(user.getUserId(), userEntity, Constant.LoginSource.CLIENT_LOGIN.getType());
            Map<String, String> tokenMap = MapUtil.builder("token", token).build();
            return R.ok().put("data",tokenMap);
        } catch (RRException rre) {
            logger.error("clientLogin :", rre);
            return R.error(rre.getCode(), rre.getMessage());
        } catch (Exception e) {
            logger.error("clientLogin :", e);
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    /**
     * 登录
     */
    @PostMapping("/sys/login")
    public Map<String, Object> login(@RequestBody SysLoginForm form) throws IOException {
        try {
            String[] locales = new String[]{"en", "ch"};
            String locale = form.getLocale();
            if (StringUtils.isBlank(locale) || !Arrays.asList(locales).contains(locale)) {
                locale = "ch";
            }
            String userName = form.getUsername();
            // 校验登录次数，达到登录上限，24小时内不允许登录
            String usernameKey = KeyConstant.getLoginErrorTimes(userName);
            String count = redisUtils.get(usernameKey);
            Map<Object, Object> params = MapUtil.builder()
                    .put("form", form)
                    .put("locale", locale)
                    .put("usernameKey", usernameKey)
                    .put("count", count)
                    .build();
            if (!Objects.equals(userName, Constant.SUPER_ADMIN_USER_NAME)) {
                // 校验登录次数，达到登录上限，24小时内不允许登录
                validateLoginCount(params);
            }
            SysUserEntity user = sysUserService.queryByUserName(userName);
            validateUser(user,params);
            DisplayUserEntity userEntity = buildDisPlayUser(user);
            // 登录成功，重置登录限制次数
            redisUtils.delete(usernameKey);
            sysUserTokenService.createToken(user.getUserId(), userEntity, Constant.LoginSource.SERVER_LOGIN.getType());
            return R.ok().put("data",userEntity);
        }catch (RRException rre){
            logger.error("login: ",rre);
            return R.error(rre.getCode(),rre.getMessage());
        }catch (Exception e) {
            logger.error("login: ",e);
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    /**
     * 用户注册
     */
    @SysLog("用户注册")
    @PostMapping("/register")
    public R registerUser(@RequestBody Map<String, String> param) {
        SysUserEntity user = new SysUserEntity();
        try {
            String username = MapUtils.getString(param, "userName");
            String displayName = MapUtils.getString(param, "chineseName");
            String password = MapUtils.getString(param, "password");
            if (StringUtils.isBlank(username) || StringUtils.isBlank(displayName)
                    || StringUtils.isBlank(password)) {
                return R.error(Constant.Msg.CHECK_INPUT);
            }
            // 用户赋值
            setUserEntity(user, username.trim(), displayName.trim(), password);
            SysRoleEntity sysRoleEntity = sysRoleService.getDefaultClientRole();
            user.setRoleIdList(new ArrayList<Long>() {{
                add(sysRoleEntity.getRoleId());
            }});
            sysUserService.saveUser(user);
            return R.ok();
        } catch (RRException r) {
            logger.error("registerErr", r);
            return R.error(r.getCode(), r.getMsg());
        } catch (Exception e) {
            logger.error("registerErr", e);
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }

    }

    private void setUserEntity(SysUserEntity user, String username, String displayName, String password) {
        user.setUsername(username);
        user.setChineseName(displayName);
        user.setPassword(password);
        user.setOemCode(oemCode);
        user.setStatus(1);
    }

    /**
     * 获取license
     */
    @PostMapping("/sys/queryUserLicenses")
    public Map<String, Object> queryUserLicenses() {
        Map tokenMap = JSON.parseObject(redisUtils.get(getToken()));
        return R.ok().put("data", new HashMap() {{
            put("userInfo", tokenMap.get("userInfo"));
            put("licenses", tokenMap.get("licenses"));
        }});
    }

    /**
     * 获取用户信息
     */
    @RequestMapping("/sys/getUserInfo")
    public R getUserInfo() {
        if (redisUtils.hasKey(getToken())) {
            DisplayUserEntity user = redisUtils.get(getToken(), DisplayUserEntity.class);
            return R.ok().put("data", user);
        } else {
            return R.error(Constant.Msg.TOKEN_EXPIRE_ERROR);
        }
    }

    /**
     * 获取用户信息
     */
    @GetMapping("/sys/getCustomUserInfo")
    public R getCustomUserInfo() {
        JSONObject data = new JSONObject();
        DisplayUserEntity userEntity = this.getUser();
        UserInfoEntity userInfo = userEntity.getUserInfo();
        data.put("roles", userEntity.getRoles());
        data.put("name", userInfo.getUsername());
        data.put("avatar", "");
        data.put("introduction", userInfo.getUserDisplayName());
        return R.ok().put("data", data);
    }

    /**
     * 验证token是否可用
     */
    @RequestMapping(value = "/sys/checkToken", method = {RequestMethod.GET, RequestMethod.POST})
    public R checkToken(@RequestHeader String token) {
        if (!redisUtils.exists(token)) {
            // 被挤掉的账户,提示一次后，删除
            if (redisUtils.exists(RedisKeys.getExpireTokenKey(token))){
                redisUtils.delete(RedisKeys.getExpireTokenKey(token));
                return R.error(502,"该账户已在其他设备登录！");
            }
            return R.error(Constant.Msg.TOKEN_EXPIRE_ERROR);
        }
        return R.ok();
    }


    /**
     * 退出
     */
    @PostMapping("/sys/logout")
    @SysLog("退出登录")
    public R logout() {
        if (StringUtils.isEmpty(getToken())) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        redisUtils.delete(getToken());
        return R.ok();

    }

    @PostMapping("/sys/workshop/select")
    public R workshop(@RequestBody(required = false) WorkshopClientVo vo) {
        if (null == vo) {
            vo = new WorkshopClientVo();
        }
        vo.setStatus(0);
        List<SysWorkshopsEntity> workshops = sysWorkshopsIService.selectWorkshop(vo);
        List<WorkshopClientResponseVo> voList = new ArrayList<>(workshops.size());

        for (SysWorkshopsEntity entity : workshops) {
            WorkshopClientResponseVo responseVo = new WorkshopClientResponseVo();
            BeanUtil.copyProperties(entity, responseVo);
            voList.add(responseVo);
        }
        return R.ok().put("data", voList);
    }


    @RequestMapping("/sys/workshop/location")
    public R location(@RequestBody LocationClientVo vo) {
        Long pid = vo.getPid();
        Long defaultPid = 10000000L;
        if (pid == null || pid == 0) {
            pid = defaultPid;
        }
        List<SysLocationEntity> locations = sysLocationService.selectByPid(pid);
        return R.ok().put("data", locations);
    }

    private void validateLoginCount(Map<Object, Object> params) {
        Integer count = MapUtil.getInt(params, "count");
        String usernameKey = MapUtil.getStr(params, "usernameKey");
        String locale = MapUtil.getStr(params, "locale","");
        if (Objects.isNull(count)){
            return;
        }
        if (count == 5) {
            long expire = redisUtils.getExpire(usernameKey);
            if (expire > 60 && expire < 3600) {
                Integer expireM = (int) Math.ceil((double) expire / 60);
                String messageEn = String.format("The account has been disabled and there are %d minutes left to log back in!", expireM);
                String messageCh = String.format("该账户已被禁用,还剩%d分钟可以重新登录！", expireM);
                throw new RRException(Objects.equals(locale, "ch") ? messageCh : messageEn, 402);
            }
            if (expire > 3600) {
                Integer expireH = (int) Math.ceil((double) expire / 3600);
                String messageEn = String.format("The account has been disabled and there are %d hours left to log back in!", expireH);
                String messageCh = String.format("该账户已被禁用,还剩%d小时可以重新登录！", expireH);
                throw new RRException(Objects.equals(locale, "ch") ? messageCh : messageEn, 402);
            }
            String messageEn = String.format("The account has been disabled and there are %d seconds left to log back in!", expire);
            String messageCh = String.format("该账户已被禁用,还剩%d秒可以重新登录！", expire);
            throw new RRException(Objects.equals(locale, "ch") ? messageCh : messageEn, 402);
        }
    }
    private void validateCaptcha2(HttpServletRequest request, SysLoginForm form, String locale) {
        String key = request.getSession().getId() + "captcha";
        String uuid = redisUtils.get(key);
        if (StringUtils.isBlank(form.getCaptcha())) {
            String messageCh = "验证码不能为空！";
            String messageEn = "Captcha cannot be empty!";
            throw new RRException(Objects.equals(locale, "ch") ? messageCh : messageEn, 2001);
        }
        boolean validate = sysCaptchaService.validate(uuid, form.getCaptcha());
        if (!validate) {
            String messageCh = "验证码验证失败，请刷新后重新输入正确的验证码！";
            String messageEn = "The verification code failed to verify, please refresh and re-enter the correct verification code!";
            throw new RRException(Objects.equals(locale, "ch") ? messageCh : messageEn, 2002);
        }
    }
    private void validateUser(SysUserEntity user,Map<Object,Object> params) {
        Integer count = MapUtil.getInt(params, "count");
        String usernameKey = MapUtil.getStr(params, "usernameKey");
        String locale = MapUtil.getStr(params, "locale");
        SysLoginForm form = MapUtil.get(params, "form",SysLoginForm.class);
        //账号不存在、密码错误
        if (user == null || !user.getPassword().equals(new Sha256Hash(form.getPassword(), user.getSalt()).toHex())) {
            int validCount;
            if (Objects.isNull(count)) {
                redisUtils.set(usernameKey, "0", DISABLED_EXPIRE);
                validCount = 0;
            } else {
                validCount = count;
            }
            if (validCount < 5) {
                validCount++;
                redisUtils.set(usernameKey, validCount, DISABLED_EXPIRE);
                String messageCh = "用户名或密码错误,剩余尝试次数" + (6 - validCount);
                String messageEn = "The user name or password is incorrect. The remaining attempts are " + (6 - validCount);
                String message = Objects.equals(locale, "ch") ? messageCh : messageEn;
                throw new RRException(message, 493);
            } else {
                throw new RRException(Constant.Msg.LOGIN_USER_LOGIN_COUNT_UNNORMAL);
            }
        }
        //账号锁定
        if (!Constant.SUPER_ADMIN_USER_NAME.equals(user.getUsername()) && user.getStatus() == 0) {
            String messageCh = "该账户已被锁定，请联系管理员！";
            String messageEn = "The account has been locked, please contact the administrator!";
            String message = Objects.equals(locale, "ch") ? messageCh : messageEn;
            throw new RRException(message, 492);
        }
        //校验经销商
        if (user.getWorkshopId() == null) {
            throw new RRException(Constant.Msg.USER_NO_WORKSHOP_ERROR);
        }

    }
    private DisplayUserEntity buildDisPlayUser(SysUserEntity user) {
        DisplayUserEntity userEntity = new DisplayUserEntity();
        List<DisplayUserEntity.Role> roles = new ArrayList<>();
        List<String> roleNameList = null;
        if (Constant.SUPER_ADMIN_USER_NAME.equals(user.getUsername())) {
            // 超管
            roleNameList = sysRoleService.queryAllRoleNameList();
            roleNameList.add("vehicleTypeDel");  //用作判断是否有车型的删除权限，仅‘DsaSuperAdmin’可以删除
            //处理role
            roleNameList.forEach(rName -> {
                // functionName
                List<String> strings = sysRoleMenuService.queryMenuNameList(rName);
                DisplayUserEntity.Role role = DisplayUserEntity.Role.builder().roleCode(rName).function(strings).build();
                roles.add(role);
            });
        } else {
            // 其他用户
            roleNameList = sysRoleService.queryRoleNameList(user.getUserId());
            roleNameList.forEach(rName -> {
                // functionName
                List<String> strings = sysRoleMenuService.queryMenuNameList(rName);
                DisplayUserEntity.Role role = DisplayUserEntity.Role.builder().roleCode(rName).function(strings).build();
                roles.add(role);
            });
            List<SysUserLicenseEntity> userLicense = sysUserLicenseService.queryById(String.valueOf(user.getUserId()));
            userEntity.setLicenses(userLicense);
        }
        UserInfoEntity userInfoEntity = UserInfoEntity.builder()
                .userId(user.getUserId().toString())
                .userDisplayName(user.getChineseName())
                .username(user.getUsername())
                .workShop(user.getWorkshopName())
                .workShopAdd(user.getWorkshopName())
                .mail(user.getEmail())
                .phone(user.getMobile())
                .build();
        userEntity.setUserInfo(userInfoEntity);
        userEntity.setRoles(roles);
        List<String> menuList = sysMenuService.clientMenus(roleNameList);
        userEntity.setMenuList(menuList);
        return userEntity;
    }

}
