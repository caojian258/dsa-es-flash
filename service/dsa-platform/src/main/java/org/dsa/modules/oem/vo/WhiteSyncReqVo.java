package org.dsa.modules.oem.vo;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.ToString;
import org.dsa.common.validator.group.AddGroup;


@Data
@ToString(callSuper = true)
public class WhiteSyncReqVo {

    @NotBlank(message = "pcid is null", groups = {AddGroup.class})
    private String pcid;

    @NotBlank(message = "name is null", groups = {AddGroup.class})
    private String name;

    @NotBlank(message = "type is null", groups = {AddGroup.class})
    private String type;
}
