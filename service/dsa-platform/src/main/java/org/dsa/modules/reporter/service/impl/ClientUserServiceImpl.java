package org.dsa.modules.reporter.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.reporter.dao.ClientUserMapper;
import org.dsa.modules.reporter.dto.ClientLoginDto;
import org.dsa.modules.reporter.entity.ClientUser;
import org.dsa.modules.reporter.service.ClientUserService;
import org.dsa.modules.reporter.vo.client.ClientPageReVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service
public class ClientUserServiceImpl implements ClientUserService {

    @Autowired
    ClientUserMapper clientUserMapper;

    @Override
    public void updateLogin(ClientLoginDto dto) {
        ClientUser loginInfo =  clientUserMapper.getOneByPcid(dto.getPcid());
        if(null == loginInfo){
            loginInfo = new ClientUser();
            loginInfo.setPcid(dto.getPcid());
            loginInfo.setFirstUsername(dto.getUsername());
            loginInfo.setWorkshop(dto.getWorkshop());
            loginInfo.setFirstOnlineIp(dto.getIp());
            loginInfo.setFirstOnlineTime(dto.getTime());
            loginInfo.setLastOnlineIp(dto.getIp());
            loginInfo.setLastOnlineTime(dto.getTime());
            loginInfo.setLastUsername(dto.getUsername());
            loginInfo.setWorkshopId(dto.getWorkshopId());
            clientUserMapper.insert(loginInfo);
        }else{
            if(null != dto.getIp()){
                loginInfo.setLastOnlineIp(dto.getIp());
            }
            loginInfo.setLastUsername(dto.getUsername());
            loginInfo.setLastOnlineTime(dto.getTime());
            if(null != dto.getWorkshopId() ){
                loginInfo.setWorkshopId(dto.getWorkshopId());
            }
            if(StrUtil.isNotBlank(dto.getWorkshop())){
                loginInfo.setWorkshop(dto.getWorkshop());
            }
            clientUserMapper.updateById(loginInfo);
        }
    }

    @Override
    public Map<String, Object> clientDevice(String pcid) {
        return clientUserMapper.workshopDevice(pcid);
    }

    @Override
    public Page<ClientUser> selectClientPage(ClientPageReVo vo) {
        Page<ClientUser> page = new Page<>(vo.getPageNo(), vo.getPageSize());
        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);

        LambdaQueryWrapper<ClientUser> qw = Wrappers.<ClientUser>lambdaQuery();

        if(StrUtil.isNotBlank(vo.getPcid())) {
            qw.apply("pcid"+" ilike {0}",  "%"+vo.getPcid().trim()+"%");
        }

        if(StrUtil.isNotBlank(vo.getWorkshop())){
            qw.apply("workshop"+" ilike {0}", "%"+vo.getWorkshop().trim()+"%");
        }

        if(StrUtil.isNotBlank(vo.getUsername())){
            qw.apply("first_username"+" ilike {0}", "%"+vo.getUsername().trim()+"%")
                    .or()
                    .apply("last_username"+" ilike {0}", "%"+vo.getUsername().trim()+"%");
        }
        return clientUserMapper.selectPage(page, qw);
    }
}
