package org.dsa.modules.remoteDiagnostic.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.common.validator.group.UpdateGroup;
import org.dsa.modules.remoteDiagnostic.pojo.BaseEntity;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@ToString
@TableName("r_diag_fun")
public class DiagnosticFunctionEntity extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;


    @NotBlank(message="功能名称不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String funName;

    private String funVal;

    private String funGroup;

    private Integer orderNum;

    private String descriptionTi;

    @NotNull(message="状态不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private Integer status;

    @TableField(exist = false)
    List<Object> ecus;

    @TableField(exist = false)
    List<DiagnosticTaskEcuEntity> ecuList;

}
