package org.dsa.modules.diagnostic.entity.mqtt.send;

import lombok.Data;

import java.io.Serializable;

/**
 * mq header 实体类
 * 
 */
@Data
public class MqRequestHeaderEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 约定版本信息
	 */
	private int version = 100;
	/**
	 * 请求id
	 */
	private String rid;
	/**
	 * 消息id
	 */
	private String mid;
	/**
	 * 功能码
	 */
	private String functionCode;
	/**
	 * 操作码
	 */
	private String action;
	/**
	 * 状态标识
	 */
	private int ack;
	/**
	 * 监听队列名
	 */
	private String respTopic = "DIAGNOSTIC_RESP";
	/**
	 * 数据来源的诊断仪
	 */
	private String source;

}
