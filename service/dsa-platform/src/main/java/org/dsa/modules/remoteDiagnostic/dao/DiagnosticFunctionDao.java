package org.dsa.modules.remoteDiagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticFunctionEntity;

import java.util.List;

@Mapper
public interface DiagnosticFunctionDao extends BaseMapper<DiagnosticFunctionEntity> {

    @Select("select i.fun_group from r_diag_fun i where i.fun_group is not null and i.fun_group != '' limit 50")
    List<String> getFunGroups();
}
