package org.dsa.modules.remoteDiagnostic.dto;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.ToString;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.common.validator.group.UpdateGroup;

import java.io.Serializable;
import java.util.List;

@Data
@ToString
public class ECUDependenceGroupDto implements Serializable {

    private Long id;

    @NotBlank(message = "groupId is null", groups = {AddGroup.class, UpdateGroup.class})
    private String groupId;

    @NotBlank(message = "name is null", groups = {AddGroup.class, UpdateGroup.class})
    private String name;

    @Valid
    @NotNull(message = "dependency is null", groups = {AddGroup.class, UpdateGroup.class})
    @Size(min = 1, message = "dependency length is 0", groups = {AddGroup.class, UpdateGroup.class})
    private List<ECUDependenceDto> dependency;

}
