package org.dsa.modules.remoteDiagnostic.config;

import org.dsa.config.RedisConfigProperties;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.ClusterServersConfig;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class RedissonConfig {

    @Autowired
    private RedisConfigProperties redisProperties;

    @Bean(destroyMethod = "shutdown")
    public RedissonClient redissonClient(){
        Config config = new Config();
        if (redisProperties.getRedisClusterConfig().getNodes() == null) {
            config.useSingleServer()
                    .setAddress("redis://" + redisProperties.getHost() + ":" + redisProperties.getPort())
                    .setPassword(redisProperties.getPassword())
                    .setDatabase(redisProperties.getDatabase() == null ? 0 : redisProperties.getDatabase());
        } else {
            List<String> nodes = redisProperties.getRedisClusterConfig().getNodes();

            List<String> serverNodes = new ArrayList<>();
            for (String node : nodes) {
                serverNodes.add("redis://" + node);
            }

            ClusterServersConfig serversConfig = config.useClusterServers()
                    .addNodeAddress(serverNodes.toArray(new String[0]));
            serversConfig.setPassword(redisProperties.getPassword());
        }
        return Redisson.create(config);
    }

}
