package org.dsa.modules.sys.vo.workshop;

import lombok.Data;

import java.io.Serializable;

@Data
public class LocationClientVo implements Serializable {

    private Long pid;

    private String locale;
}
