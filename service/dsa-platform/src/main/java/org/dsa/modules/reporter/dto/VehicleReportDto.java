package org.dsa.modules.reporter.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class VehicleReportDto implements Serializable {

    private String id;

    private String sessionInfoId;

    private String sessionId;

    private String vin;

    private Integer diagCategory;

    private Integer sourceVersion;

    private String diagSource;

    private String maker;

    private String model;

    private String year;

    private String brand;

    private String trimLevel;

    private String workshop;

    private String workshopCode;

    private Long workshopId;

    private String userId;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    private String productionDate;

    private String placeOfProduction;

    private Date startTime;

    private Date endTime;

    private String userName;
}
