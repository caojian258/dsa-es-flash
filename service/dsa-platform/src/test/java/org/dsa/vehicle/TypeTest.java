package org.dsa.vehicle;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.dsa.modules.vehicle.dao.DVehicleTypeDao;
import org.dsa.modules.vehicle.entity.DVehicleTypeEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TypeTest {
    @Autowired
    DVehicleTypeDao typeDao;

    @Test
    public void test() {
        DVehicleTypeEntity dVehicleTypeEntity = typeDao.selectOne(Wrappers.<DVehicleTypeEntity>lambdaQuery().eq(DVehicleTypeEntity::getId, 22).select(DVehicleTypeEntity::getSdgList));
        System.out.println(dVehicleTypeEntity);
    }
}
