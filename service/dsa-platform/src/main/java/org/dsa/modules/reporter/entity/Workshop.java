package org.dsa.modules.reporter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
@TableName("d_workshop")
public class Workshop extends BaseEntity {

    @TableId(type = IdType.INPUT)
    private String id;

    private String address;

    private String name;

    private String nameEn;

    private String tag;

    private String workshopGroup;

    private Double latitude;

    private Double longitude;

    private String country;

    private String province;

    private String city;

    private String district;

    private String brand;

    private int state;

    private String contactName;

    private String contactWay;
}
