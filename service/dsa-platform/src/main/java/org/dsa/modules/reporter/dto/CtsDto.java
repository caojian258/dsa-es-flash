package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CtsDto implements Serializable {

    private String functionCode;

    private String actionCode;
}
