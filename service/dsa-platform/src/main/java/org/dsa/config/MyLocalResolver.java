package org.dsa.config;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.LocaleResolver;


//import jakarta.servlet.http.HttpServletRequest;
//import jakarta.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

@Slf4j
public class MyLocalResolver implements LocaleResolver {

    @Override
    public Locale resolveLocale(HttpServletRequest httpServletRequest) {

        //zh_CN/en_US 国家_地区 这是标准的方式
        String localName = httpServletRequest.getHeader("lang");

        if(null != httpServletRequest.getParameter("lang")){
            localName = httpServletRequest.getParameter("lang");
        }

        String lang = standardizeLocale(localName);

        Locale aDefault = Locale.getDefault();

        if (lang!=null){
            String[] s = lang.split("_");
            //国家_地区
            aDefault = new Locale(s[0],s[1]);
        }

        return aDefault;
    }

    @Override
    public void setLocale(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Locale locale) {

    }

    private String standardizeLocale(String lang){
        if(null == lang){
            return "zh_CN";
        }
        String[] langList = lang.split("_");
        if(langList.length>1){
            return lang;
        }

        Map<String, String> langMap = new HashMap<>();
        langMap.put("zh", "zh_CN");
        langMap.put("en", "en_US");
        return Optional.ofNullable(langMap.get(lang)).orElse("zh_CN");
    }
}
