package org.dsa.modules.remoteDiagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.dsa.modules.remoteDiagnostic.dto.api.DiagnosticFileDto;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticFileEntity;

@Mapper
public interface DiagnosticFileDao extends BaseMapper<DiagnosticFileEntity> {


    @Select("select * from r_diag_file i where i.id = (select t.file_id  from r_diag_task_campaign j left join r_diag_task t on j.task_id = t.id where j.id = #{taskId} ) ")
    DiagnosticFileEntity getInfoByRelease(@Param("taskId") Long taskId);

    @Select("select j.* from r_diag_task i inner join r_diag_file j on i.file_id = j.id  and i.id = #{taskId} ")
    DiagnosticFileEntity getInfoByTask(@Param("taskId") Long taskId);

//    @Select("select j.file_name,j.file_md5,j.file_length from r_diag_task i inner join r_diag_file j on i.file_id = j.id  and i.id = #{taskId} ")
    DiagnosticFileDto getDtoByTask(@Param("taskId") Long taskId);

    @Update("update r_diag_file  set file_default = 0 where id = #{id} ")
    void updateDefault(@Param("id") Long id);


}
