package org.dsa.modules.diagnostic.entity;


import lombok.Data;

import java.io.Serializable;

/**
 * 远程诊断 - 读取故障码
 */
@Data
public class ReadDtcEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    String vin;
    String[] ecuNames;

}
