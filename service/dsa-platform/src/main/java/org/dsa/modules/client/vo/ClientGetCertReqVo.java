package org.dsa.modules.client.vo;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(callSuper = true)
public class ClientGetCertReqVo {

    private String crt;

}