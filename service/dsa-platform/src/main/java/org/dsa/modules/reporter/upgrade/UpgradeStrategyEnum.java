package org.dsa.modules.reporter.upgrade;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UpgradeStrategyEnum {
    //根据客户端传过来的数据类型没有下载 只有升级
    install(1, "installUpgradeStrategy"),
    repair(2, "repairUpgradeStrategy"),
    rollback(3, "rollbackUpgradeStrategy"),
    uninstall(4, "unInstallUpgradeStrategy"),
    ;

    private Integer code;

    private String name;


    public static UpgradeStrategyEnum getByCode(Integer code){
        UpgradeStrategyEnum[] values = UpgradeStrategyEnum.values();

        for (UpgradeStrategyEnum item: values){
            if(item.getCode().equals(code)){
                return item;
            }
        }
        return null;
    }
}
