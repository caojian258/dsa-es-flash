
-- 清空数据
truncate table r_diag_condition;
truncate table r_diag_file;
truncate table r_diag_fun;
truncate table r_diag_task;
truncate table r_diag_task_active;
truncate table r_diag_task_campaign;
truncate table r_diag_task_detail;
truncate table r_diag_task_log;
truncate table r_diag_task_overview;
truncate table r_diag_task_record;
truncate table r_ecu_group;
truncate table r_ecu_identification_info;
truncate table r_ecu_version;
truncate table r_ecu_version_file;
truncate table r_pool_depend;
truncate table r_pool_vehicle;
truncate table r_tag;
truncate table r_task_condition;
truncate table r_task_ecu;
truncate table r_task_file;
truncate table r_vehicle_ecu;
truncate table r_vehicle_ecu_group;
truncate table r_vehicle_ecu_image;
truncate table r_vehicle_pool;
truncate table r_vehicle_tag;
truncate table r_vehicle_type_ecu;
truncate table r_vehicle_version;
truncate table r_vehicle_version_ecu_group;
truncate table r_version_ecu_rely;



-- 删表
drop table r_diag_condition;
drop table r_diag_file;
drop table r_diag_fun;
drop table r_diag_task;
drop table r_diag_task_active;
drop table r_diag_task_campaign;
drop table r_diag_task_detail;
drop table r_diag_task_log;
drop table r_diag_task_overview;
drop table r_diag_task_record;
drop table r_ecu_group;
drop table r_ecu_identification_info;
drop table r_ecu_version;
drop table r_ecu_version_file;
drop table r_pool_depend;
drop table r_pool_vehicle;
drop table r_tag;
drop table r_task_condition;
drop table r_task_ecu;
drop table r_task_file;
drop table r_vehicle_ecu;
drop table r_vehicle_ecu_group;
drop table r_vehicle_ecu_image;
drop table r_vehicle_pool;
drop table r_vehicle_tag;
drop table r_vehicle_type_ecu;
drop table r_vehicle_version;
drop table r_vehicle_version_ecu_group;
drop table r_version_ecu_rely;