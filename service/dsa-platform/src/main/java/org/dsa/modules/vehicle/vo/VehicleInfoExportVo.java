package org.dsa.modules.vehicle.vo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.util.Date;

/**
 * Author: lyf
 * Date: 2023/10/23
 * Description:
 */
@Data
public class VehicleInfoExportVo {
    private static final long serialVersionUID = 1L;


    /**
     * vin码
     */
    @ExcelProperty(index = 0)
    private String vin;
    /**
     * 车型名称
     */
    @ExcelProperty(index = 1)
    private String vehicleType;
    /**
     * 车辆生产日期
     */
    @ExcelProperty(index = 2)
    private String productionDate;
    /**
     * 产地
     */
    @ExcelProperty(index = 3)
    private String placeOfProduction;
    /**
     * 同步时间
     */
    @ExcelProperty(index = 4)
    private Date createTime;
    /**
     * 更新时间
     */
    @ExcelProperty(index = 5)
    private Date updateTime;
    /**
     * 车辆来源 1——TSP同步 2——远程服务器添加
     */
    @ExcelProperty(index = 6)
    private String source;
    /**
     * 车辆极端 AsBuild,AsMaintained
     */
    @ExcelProperty(index = 7)
    private String stage;
    /**
     * 车辆版本主键
     */
    @ExcelIgnore
    private Long versionId;

    /**
     * 当前车辆版本主键
     */
    @ExcelIgnore
    private Long currentVersionId;

    /**
     * 车型信息表id
     */
    @ExcelIgnore
    private Long vehicleTypeId;
}
