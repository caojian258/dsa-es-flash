package org.dsa.modules.reporter.document;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
public class DidDoc extends ElasticEntity {

    private String sessionInfoId;

    private Integer recordId;

    //1 首次 2尾次 3首次尾次
    private Integer recordFlag;

    private String vin;

    private List<Long> vehicleTypeId;

    private String sessionId;

    private String ecuName;

    private String didHex;

    private String didName;

    private Map<String, Object> didNameSuggest;

    private String didNameTi;

    private String parameterName;

    private String parameterNameTi;

    private String value;

    private String valueTi;

    private String nrcHex;

    private String nrcDescription;

    private String nrcDescriptionTi;

    private String errorCode;

    private String errorMessage;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date lastUpdateTime;
}
