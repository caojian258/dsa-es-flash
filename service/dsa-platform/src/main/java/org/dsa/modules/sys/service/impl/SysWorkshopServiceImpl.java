package org.dsa.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.dsa.common.annotation.DataFilter;
import org.dsa.modules.sys.dao.SysWorkshopDao;
import org.dsa.modules.sys.entity.SysWorkshopEntity;
import org.dsa.modules.sys.service.LdapService;
import org.dsa.modules.sys.service.SysUserService;
import org.dsa.modules.sys.service.SysWorkshopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Service("sysWorkshopService")
public class SysWorkshopServiceImpl extends ServiceImpl<SysWorkshopDao, SysWorkshopEntity> implements SysWorkshopService {
	@Resource
	SysUserService sysUserService;

	@Autowired
	private LdapService ldapService;

	@Override
	@DataFilter(subWorkshop = true, user = false, tableAlias = "t1")
	public List<SysWorkshopEntity> queryList(Map<String, Object> params){
		return baseMapper.queryList(params);
	}

	public SysWorkshopEntity getSysWorkshopEntity(Long workshopId){
		return baseMapper.getSysWorkshopEntity(workshopId);
	}

	@Override
	public int querySysWorkshopEntityCount() {
		return baseMapper.querySysWorkshopEntityCount();
	}

	@Override
	public void removeNotInIds(List<Long> organList) {
		baseMapper.removeNotInIds(organList);
	}

	@Override
	public List<String> queryListString() {
		return baseMapper.queryListString();
	}

	@Override
	public void saveOrg(SysWorkshopEntity sysWorkshopEntity) {

		baseMapper.saveOrg(sysWorkshopEntity);
	}

	@Override
	public void disableById(long workshopId) {
		//1.查询经销商下所有用户
		List<Long> userIds = sysUserService.queryUserIdByWorkshopId(workshopId);

		//2.禁用db和ldap用户
		if(userIds != null && userIds.size() > 0) {
			Long[] userIdArray= new Long[userIds.size()];
			sysUserService.deleteBatch(userIds.toArray(userIdArray));
		}

		//3.禁用经销商
		baseMapper.disableById(workshopId);
	}

	@Override
	public List<Long> queryDetpIdList(Long parentId) {
		return baseMapper.queryDetpIdList(parentId);
	}

	@Override
	public List<Long> getSubWorkshopIdList(Long workshopId){
		//部门及子部门ID列表
		List<Long> workshopIdList = new ArrayList<>();

		//获取子部门ID
		List<Long> subIdList = queryDetpIdList(workshopId);
		getWorkshopTreeList(subIdList, workshopIdList);

		return workshopIdList;
	}

	@Override
	public String queryWorkshopNameById(Long workshopId) {
		return baseMapper.queryWorkshopNameById(workshopId);
	}


	/**
	 * 递归
	 */
	private void getWorkshopTreeList(List<Long> subIdList, List<Long> workshopIdList){
		for(Long workshopId : subIdList){
			List<Long> list = queryDetpIdList(workshopId);
			if(list.size() > 0){
				getWorkshopTreeList(list, workshopIdList);
			}

			workshopIdList.add(workshopId);
		}
	}
}
