package org.dsa.modules.remoteDiagnostic.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.dsa.common.exception.RemoteApiException;
import org.dsa.common.utils.DateUtils;
import org.dsa.common.utils.ObjectUtils;
import org.dsa.common.utils.RedisUtils;
import org.dsa.config.SyncConfig;
import org.dsa.modules.onlineUpdate.rule.RuleComponent;
import org.dsa.modules.remoteDiagnostic.config.RemoteDiagnosticConfig;
import org.dsa.modules.remoteDiagnostic.constant.RemoteDiagnosticConstant;
import org.dsa.modules.remoteDiagnostic.dao.*;
import org.dsa.modules.remoteDiagnostic.dto.TaskGradientDto;
import org.dsa.modules.remoteDiagnostic.dto.api.DiagnosticTaskDto;
import org.dsa.modules.remoteDiagnostic.dto.api.MatchCampaignDto;
import org.dsa.modules.remoteDiagnostic.entity.*;
import org.dsa.modules.remoteDiagnostic.enums.TaskDetailTypeEnum;
import org.dsa.modules.remoteDiagnostic.service.DiagnosticTaskDetailService;
import org.dsa.modules.remoteDiagnostic.service.DiagnosticTaskCampaignService;
import org.dsa.modules.remoteDiagnostic.service.RemoteVehiclePoolService;
import org.dsa.modules.remoteDiagnostic.vo.CheckNameReqVo;
import org.dsa.modules.remoteDiagnostic.vo.PublicPageReqVo;
import org.dsa.modules.remoteDiagnostic.vo.Task.DiagnosticApiLoggerReqVo;
import org.dsa.modules.remoteDiagnostic.vo.Task.DiagnosticApiReqVo;
import org.dsa.modules.remoteDiagnostic.vo.Task.DiagnosticApiStatusReqVo;
import org.dsa.modules.reporter.util.FilesUtil;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DiagnosticTaskCampaignServiceImpl implements DiagnosticTaskCampaignService {

    @Autowired
    DiagnosticTaskCampaignDao campaignDao;
    @Autowired
    private DiagnosticTaskRecordDao taskRecordDao;
    @Autowired
    private DiagnosticTaskDetailDao taskDetailDao;
    @Autowired
    private DiagnosticTaskOverviewDao taskOverviewDao;
    @Autowired
    RemoteDiagnosticConfig remoteDiagnosticConfig;
    @Autowired
    SyncConfig syncConfig;
    @Autowired
    DiagnosticTaskLogDao taskLogDao;
    @Autowired
    DiagnosticTaskDao taskDao;
    @Autowired
    private VinCodeDao vinCodeDao;
    @Autowired
    RVehicleTagDao vehicleTagDao;
    @Autowired
    DiagnosticTaskActiveDao activeDao;
    @Autowired
    DiagnosticTaskDetailService detailService;
    @Autowired
    RemoteVehiclePoolService poolService;
    @Autowired
    RuleComponent ruleComponent;
    @Autowired
    RedisUtils redisUtils;
    @Autowired
    RedissonClient redissonClient;
//    @Autowired
//    RedissonClient redisson;

    @Override
    public Page<DiagnosticTaskCampaignEntity> selectPage(PublicPageReqVo vo) {
        Page<DiagnosticTaskCampaignEntity> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);
        LambdaQueryWrapper<DiagnosticTaskCampaignEntity> qw = Wrappers.<DiagnosticTaskCampaignEntity>lambdaQuery();

        if (!StringUtils.isEmpty(vo.getName())) {
//            qw.like(VehicleEcuEntity::getEcuName, vo.getName().trim());
            qw.apply("campaign_name" + " ilike {0}", "%" + vo.getName().trim() + "%");
        }

        if (!StringUtils.isEmpty(vo.getStatus())) {
            qw.eq(DiagnosticTaskCampaignEntity::getStatus, vo.getStatus());
        }
        Page<DiagnosticTaskCampaignEntity> resultPage = campaignDao.selectPage(page, qw);

        return resultPage;
    }

    @Override
    public DiagnosticTaskCampaignEntity getInfo(Long id) {
        Object jsonString = redisUtils.hget(RemoteDiagnosticConstant.CAMPAIGN_KEY, RemoteDiagnosticConstant.CAMPAIGN_ITEM_KEY + id);
        Gson gson = new Gson();
        DiagnosticTaskCampaignEntity entity;
        if (ObjectUtils.isEmpty(jsonString)) {
            entity = campaignDao.selectById(id);
            // 更新缓存
            redisUtils.hset(RemoteDiagnosticConstant.CAMPAIGN_KEY, RemoteDiagnosticConstant.CAMPAIGN_ITEM_KEY + entity.getId(), gson.toJson(entity));
        } else {
            entity = gson.fromJson(jsonString.toString(), DiagnosticTaskCampaignEntity.class);
        }
        return entity;
    }

    @Override
    @Transactional
    public void saveTask(DiagnosticTaskCampaignEntity task) {
        campaignDao.insert(task);
        if (task.getPoolId() != null && task.getPool() != null && task.getId() != null) {
            log.info("诊断任务活动 ----->>>>>>  开始更新车辆池");
            poolService.updatePool(task.getPool());
        }
        if (task.getStatus() == 0) {
            this.saveOrUpdateOverview(task);
        }
        Gson gson = new Gson();
        // 更新缓存
        task.setPool(null);
        redisUtils.hset(RemoteDiagnosticConstant.CAMPAIGN_KEY, RemoteDiagnosticConstant.CAMPAIGN_ITEM_KEY + task.getId(), gson.toJson(task));
    }

    @Override
    @Transactional
    public void updateTask(DiagnosticTaskCampaignEntity task) {
//        RLock lock = redisson.getLock(StaticConstant.VEHICLE_POOL_LOCK + task.getId());
//
//        lock.lock();
//        try {
        LambdaUpdateWrapper<DiagnosticTaskCampaignEntity> qw = Wrappers.<DiagnosticTaskCampaignEntity>lambdaUpdate();

        task.setUpdatedAt(new Date());

        qw.eq(DiagnosticTaskCampaignEntity::getId, task.getId());


        if (task.getPoolId() != null && task.getPoolId() == -1) {
            task.setPoolId(null);
            qw.set(DiagnosticTaskCampaignEntity::getPoolId, null);
        }

//        if (task.getGradient() == null) {
//            qw.set(DiagnosticTaskCampaignEntity::getGradient, null);
//            qw.set(DiagnosticTaskCampaignEntity::getGradientCondition, null);
//            qw.set(DiagnosticTaskCampaignEntity::getGradientValue, null);
//        }
        if (task.getFailedAttemptsNum() == null) {
            qw.set(DiagnosticTaskCampaignEntity::getFailedAttemptsNum, null);
        }
        if (task.getExecutionCycle() == null) {
            qw.set(DiagnosticTaskCampaignEntity::getExecutionCycle, null);
        }
        if (task.getExecutionInterval() == null) {
            qw.set(DiagnosticTaskCampaignEntity::getExecutionInterval, null);
        }
        campaignDao.update(task, qw);

        if (task.getPoolId() != null && task.getPool() != null && task.getId() != null) {
            log.info("诊断任务活动 ----->>>>>>  开始更新车辆池");
            poolService.updatePool(task.getPool());
        }
        if (task.getStatus() == 0) this.saveOrUpdateOverview(task);

        Gson gson = new Gson();
        // 更新缓存
        task.setPool(null);
        redisUtils.hset(RemoteDiagnosticConstant.CAMPAIGN_KEY, RemoteDiagnosticConstant.CAMPAIGN_ITEM_KEY + task.getId(), gson.toJson(task));
//        } catch (Exception e) {
//            log.error(e.getMessage(), e);
//        } finally {
//            lock.unlock();
//        }


    }

    @Override
    public List<DiagnosticTaskCampaignEntity> getTasks() {
        return null;
    }

    @Override
    public Boolean check(CheckNameReqVo vo) {
        LambdaQueryWrapper<DiagnosticTaskCampaignEntity> qw = Wrappers.<DiagnosticTaskCampaignEntity>lambdaQuery();
        qw.eq(DiagnosticTaskCampaignEntity::getCampaignName, vo.getName());
        if (vo.getId() != null) {
            qw.ne(DiagnosticTaskCampaignEntity::getId, vo.getId());
        }
        qw.select(DiagnosticTaskCampaignEntity::getId);
        return campaignDao.selectOne(qw) == null;
    }


    @Override
    @Transactional
    public List<DiagnosticTaskDto> getDiagnosticTaskList(DiagnosticApiReqVo vo) {
        List<DiagnosticTaskDto> list = new ArrayList<>();

        Map<String, Object> param = new HashMap<>();
        param.put("vin", vo.getVin());

        Gson gson = new Gson();

        // 获取 匹配到的任务
        List<DiagnosticTaskCampaignEntity> temp = getMatchCampaign(vo, param, gson);

        // 正向梯度校验
        checkPositiveGradient(temp);

        List<DiagnosticTaskDto> tasks;
        if (temp != null && temp.size() != 0) {
            // 查询并封装为dto
            tasks = campaignDao.getDiagnosticTasksByIds(temp);
        } else {
            return list;
        }

//        List<String> vinCodes = new ArrayList<>();
        tasks.forEach(task -> {
            DiagnosticTaskRecordEntity record = taskRecordDao.getInfoByTaskAndVin(task.getId(), vo.getVin());
            // 判断是否存在记录
            if (record != null) {
                redisUtils.hset(RemoteDiagnosticConstant.CAMPAIGN_KEY, RemoteDiagnosticConstant.RECORD_ITEM_KEY + record.getSessionId(), gson.toJson(record), RemoteDiagnosticConstant.SEVEN_DAYS_TIMEOUT);
                // 判断是否有匹配记录且成功
                if (record.getMatchStatus() == 0) {
                    return;
                }
            }
            // 与客户端交互时，值不能为空
            if (task.getFailedAttemptsNum() == null || task.getFailedAttemptsNum() == 0) task.setFailedAttemptsNum(1);
            if (task.getCycle() == null || task.getCycle() == 0) task.setCycle(1);
            if (task.getFunList() == null) task.setFunList(new ArrayList<>());

            // 判断记录是否存在，存在则使用已存在的sessionId
            if (record != null) {
                task.setSessionId(record.getSessionId());
            } else {
                task.setSessionId(UUID.randomUUID().toString().replaceAll("-", ""));
                saveRecord(task, vo.getVin());
            }

            // 这两个值不给客户端，设置为null
            task.setId(null);
            task.setPoolId(null);

            // 添加到列表
            list.add(task);
        });
        return list;
    }

    private void checkPositiveGradient(List<DiagnosticTaskCampaignEntity> campaigns) {
        List<DiagnosticTaskCampaignEntity> temp = new ArrayList<>();
        Gson gson = new Gson();
        String key;
        boolean flag;
//        for (DiagnosticTaskCampaignEntity campaign : campaigns) {
//
//            DiagnosticTaskCampaignEntity entity = getCampaign(gson, campaign.getId());
//            DiagnosticTaskOverviewEntity overview = getOverview(campaign.getId(), gson);
//            List<TaskGradientDto> gradient = entity.getGradient();
//            flag = false;
//            key = RemoteDiagnosticConstant.TASK_OVERVIEW_KEY + campaign.getId();
//            for (TaskGradientDto dto : gradient) {
//                if (StringUtils.isEmpty(dto.getGradientValue()) || dto.getFlag() == 1) {
//                    continue;
//                }
//                // 成功次数
//                long match;
//                if (redisUtils.hHasKey(key, RemoteDiagnosticConstant.TASK_OVERVIEW_MATCH_KEY)) {
//                    match = Long.parseLong(redisUtils.hget(key, RemoteDiagnosticConstant.TASK_OVERVIEW_MATCH_KEY).toString());
//                } else {
//                    match = overview.getMatchNum();
//                }
//
//                // 成功次数
//                long success;
//                if (redisUtils.hHasKey(key, RemoteDiagnosticConstant.TASK_OVERVIEW_SUCCESS_KEY)) {
//                    success = Long.parseLong(redisUtils.hget(key, RemoteDiagnosticConstant.TASK_OVERVIEW_SUCCESS_KEY).toString());
//                } else {
//                    success = overview.getSuccessNum();
//                }
//
//                // 失败次数
//                long fail;
//                if (redisUtils.hHasKey(key, RemoteDiagnosticConstant.TASK_OVERVIEW_FAIL_KEY)) {
//                    fail = Long.parseLong(redisUtils.hget(key, RemoteDiagnosticConstant.TASK_OVERVIEW_FAIL_KEY).toString());
//                } else {
//                    fail = overview.getFailNum();
//                }
//
//                if (match < dto.getGradient()) {   //① 获取次数未达到总数
//                    flag = true;
//                } else {
//                    if (0 == dto.getGradientCondition()) {  // 条件%
//                        if (match == dto.getGradient()) {   //① 获取次数达到总数，判断是否需要继续发放
//                            double i = (double) success / match;
//                            double j = (double) dto.getGradientValue() / match;
//                            if (i >= j) {
//                                flag = true;
//                            }
//                        }
//                    } else {
//                        // 条件 >
//                        if (match == dto.getGradient()) {   //① 获取次数达到总数，判断是否需要继续发放
//                            if (success >= dto.getGradientValue()) {
//                                flag = true;
//                            }
//                        }
//                    }
//                }
//            }
//        }
    }

    @Override
    @Transactional
    public void uploadTaskResult(DiagnosticApiReqVo vo) {
        Gson gson = new Gson();

        DiagnosticTaskRecordEntity record = getRecord(vo.getSessionId(), vo.getVin(), gson);

        if (record == null) {
            throw new RemoteApiException("未查询到获取记录");
        }

        DiagnosticTaskOverviewEntity overview = getOverview(record.getCampaignId(), gson);

        DiagnosticTaskCampaignEntity task = getCampaign(gson, record.getCampaignId());

        DiagnosticTaskActiveEntity active = getActive(vo.getSessionId(), gson);
        if (active == null) {
            throw new RemoteApiException("未查询到执行任务");
        }

        try {
            DiagnosticTaskDetailEntity diagnosticTaskDetailEntity = new DiagnosticTaskDetailEntity();
            diagnosticTaskDetailEntity.setCampaignId(record.getCampaignId());
            diagnosticTaskDetailEntity.setVin(vo.getVin());

            diagnosticTaskDetailEntity.setSessionId(vo.getSessionId());
            diagnosticTaskDetailEntity.setType(TaskDetailTypeEnum.DiagnosticResult.getValue());
            diagnosticTaskDetailEntity.setCode(vo.getErrorCode() + "");
            diagnosticTaskDetailEntity.setMessage(vo.getErrorMsg());
            diagnosticTaskDetailEntity.setStartTime(vo.getStartTime());
            diagnosticTaskDetailEntity.setEndTime(vo.getEndTime());

            taskDetailDao.insert(diagnosticTaskDetailEntity);

            record.setUpdatedAt(new Date());
            record.setExecutions(record.getExecutions() + 1);

            // 0 代表最后一次执行，最终结果
            if (vo.getFlag() == 0) {
                active.setStatus(1);
                active.setUpdatedAt(new Date());
                activeDao.updateById(active);
            }
            taskRecordDao.updateById(record);

            // 计数 | 梯度校验
            if (vo.getFlag() == 0) {
                if (0 == vo.getErrorCode()) { // 成功
                    overviewAutoincrement(overview, TaskDetailTypeEnum.DiagnosticResult_SUCCESS.getValue(), task);
                } else { // 失败
                    overviewAutoincrement(overview, TaskDetailTypeEnum.DiagnosticResult_FAIL.getValue(), task);
                }
            }

            redisUtils.hdel(RemoteDiagnosticConstant.RECORD_KEY, RemoteDiagnosticConstant.RECORD_ITEM_KEY + record.getSessionId());
            redisUtils.hdel(RemoteDiagnosticConstant.TASK_ACTIVE_KEY, RemoteDiagnosticConstant.TASK_ACTIVE_ITEM_KEY + record.getSessionId());
        } catch (Exception e) {
            redisUtils.hdel(RemoteDiagnosticConstant.RECORD_KEY, RemoteDiagnosticConstant.RECORD_ITEM_KEY + record.getSessionId());
            redisUtils.hdel(RemoteDiagnosticConstant.TASK_ACTIVE_KEY, RemoteDiagnosticConstant.TASK_ACTIVE_ITEM_KEY + record.getSessionId());
            throw e;
        }

    }

    private List<DiagnosticTaskCampaignEntity> getMatchCampaign(DiagnosticApiReqVo vo, Map<String, Object> param, Gson gson) {
        List<DiagnosticTaskCampaignEntity> temp;
        MatchCampaignDto match;
        if (redisUtils.hHasKey(RemoteDiagnosticConstant.VEHICLE_MATCH_POOL, RemoteDiagnosticConstant.VEHICLE_MATCH_POOL_VIN + vo.getVin())) {
            match = gson.fromJson(redisUtils.hget(RemoteDiagnosticConstant.VEHICLE_MATCH_POOL, RemoteDiagnosticConstant.VEHICLE_MATCH_POOL_VIN + vo.getVin()).toString(), MatchCampaignDto.class);

            String local = redisUtils.get(RemoteDiagnosticConstant.VEHICLE_POOL_LOCK);
            // 时间戳在一分钟之内
            if (!StringUtils.isEmpty(local) && Math.abs((Long.parseLong(local) - match.getLocal())) < 60 * 1000) {
                temp = match.getCampaigns();
            } else {
                temp = ruleComponent.matchRelease(param);

                long localTime = System.currentTimeMillis();
                if (StringUtils.isEmpty(local)) {
                    redisUtils.set(RemoteDiagnosticConstant.VEHICLE_POOL_LOCK, localTime);
                    match.setLocal(localTime);
                } else {
                    match.setLocal(Long.parseLong(local));
                }
                match.setCampaigns(temp);
                redisUtils.hset(RemoteDiagnosticConstant.VEHICLE_MATCH_POOL, RemoteDiagnosticConstant.VEHICLE_MATCH_POOL_VIN + vo.getVin(), gson.toJson(match), RemoteDiagnosticConstant.ONE_MONTH_TIMEOUT);
            }
        } else {
            temp = ruleComponent.matchRelease(param);

            String local = redisUtils.get(RemoteDiagnosticConstant.VEHICLE_POOL_LOCK);
            long localTime = System.currentTimeMillis();
            match = new MatchCampaignDto();
            if (StringUtils.isEmpty(local)) {
                redisUtils.set(RemoteDiagnosticConstant.VEHICLE_POOL_LOCK, localTime);
                match.setLocal(localTime);
            } else {
                match.setLocal(Long.parseLong(local));
            }
            match.setCampaigns(temp);
            redisUtils.hset(RemoteDiagnosticConstant.VEHICLE_MATCH_POOL, RemoteDiagnosticConstant.VEHICLE_MATCH_POOL_VIN + vo.getVin(), gson.toJson(match), RemoteDiagnosticConstant.ONE_MONTH_TIMEOUT);
        }
        return temp;
    }

    private DiagnosticTaskRecordEntity getRecord(String sessionId, String vin, Gson gson) {
        DiagnosticTaskRecordEntity record;
        if (redisUtils.hHasKey(RemoteDiagnosticConstant.RECORD_KEY, RemoteDiagnosticConstant.RECORD_ITEM_KEY + sessionId)) {
            record = gson.fromJson(redisUtils.hget(RemoteDiagnosticConstant.RECORD_KEY, RemoteDiagnosticConstant.RECORD_ITEM_KEY + sessionId).toString(), DiagnosticTaskRecordEntity.class);
        } else {
            record = taskRecordDao.getInfoBySessionId(sessionId, vin);
            redisUtils.hset(RemoteDiagnosticConstant.RECORD_KEY, RemoteDiagnosticConstant.RECORD_ITEM_KEY + sessionId, gson.toJson(record), RemoteDiagnosticConstant.SEVEN_DAYS_TIMEOUT);
        }
        return record;
    }

    private DiagnosticTaskCampaignEntity getCampaign(Gson gson, Long id) {
        DiagnosticTaskCampaignEntity task;
        if (redisUtils.hHasKey(RemoteDiagnosticConstant.CAMPAIGN_KEY, RemoteDiagnosticConstant.CAMPAIGN_ITEM_KEY + id)) {
            task = gson.fromJson(redisUtils.hget(RemoteDiagnosticConstant.CAMPAIGN_KEY, RemoteDiagnosticConstant.CAMPAIGN_ITEM_KEY + id).toString(), DiagnosticTaskCampaignEntity.class);
        } else {
            task = campaignDao.selectById(id);
            redisUtils.hset(RemoteDiagnosticConstant.CAMPAIGN_KEY, RemoteDiagnosticConstant.CAMPAIGN_ITEM_KEY + id, gson.toJson(task));
        }
        return task;
    }

    private DiagnosticTaskOverviewEntity getOverview(Long id, Gson gson) {
        DiagnosticTaskOverviewEntity overview;
        if (redisUtils.hHasKey(RemoteDiagnosticConstant.TASK_OVERVIEW_KEY + id, RemoteDiagnosticConstant.TASK_OVERVIEW_ITEM_KEY + id)) {
            overview = gson.fromJson(redisUtils.hget(RemoteDiagnosticConstant.TASK_OVERVIEW_KEY + id, RemoteDiagnosticConstant.TASK_OVERVIEW_ITEM_KEY + id).toString(), DiagnosticTaskOverviewEntity.class);
        } else {
            overview = taskOverviewDao.getInfoByTask(id);
            redisUtils.hset(RemoteDiagnosticConstant.TASK_OVERVIEW_KEY + id, RemoteDiagnosticConstant.TASK_OVERVIEW_ITEM_KEY + id, gson.toJson(overview), RemoteDiagnosticConstant.SEVEN_DAYS_TIMEOUT);
        }
        return overview;
    }

    private DiagnosticTaskActiveEntity getActive(String sessionId, Gson gson) {
        DiagnosticTaskActiveEntity active;
        if (redisUtils.hHasKey(RemoteDiagnosticConstant.TASK_ACTIVE_KEY, RemoteDiagnosticConstant.TASK_ACTIVE_ITEM_KEY + sessionId)) {
            active = gson.fromJson(redisUtils.hget(RemoteDiagnosticConstant.TASK_ACTIVE_KEY, RemoteDiagnosticConstant.TASK_ACTIVE_ITEM_KEY + sessionId).toString(), DiagnosticTaskActiveEntity.class);
        } else {
            active = activeDao.getInfoBySessionId(sessionId);
            redisUtils.hset(RemoteDiagnosticConstant.TASK_ACTIVE_KEY, RemoteDiagnosticConstant.TASK_ACTIVE_ITEM_KEY + sessionId, gson.toJson(active), RemoteDiagnosticConstant.HALF_AN_HOUR_TIMEOUT);
        }
        return active;
    }

    @Override
    public Map<String, Object> checkTask(DiagnosticApiReqVo vo) {

        Gson gson = new Gson();

        DiagnosticTaskRecordEntity record = getRecord(vo.getSessionId(), vo.getVin(), gson);

        if (record == null) {
            throw new RemoteApiException("未查询到诊断任务");
        }


        DiagnosticTaskActiveEntity active = getActive(vo.getSessionId(), gson);

        Map<String, Object> result = new HashMap<>();

//        DiagnosticTaskDetailEntity diagnosticTaskDetailEntity = new DiagnosticTaskDetailEntity();
//        diagnosticTaskDetailEntity.setTaskId(record.getTaskId());
//        diagnosticTaskDetailEntity.setVin(vo.getVin());
//        diagnosticTaskDetailEntity.setSessionId(vo.getSessionId());
//        diagnosticTaskDetailEntity.setType(TaskDetailTypeEnum.RemoteVerification.getValue());
//        diagnosticTaskDetailEntity.setStartTime(new Date());
//        diagnosticTaskDetailEntity.setEndTime(new Date());
//        taskDetailDao.insert(diagnosticTaskDetailEntity);

        // 如果有执行中的记录  且  状态为终止  直接返回
        if (active != null && active.getStatus() == 2) {
            result.put("status", 1);
            return result;
        }

        DiagnosticTaskCampaignEntity task = getCampaign(gson, record.getCampaignId());

        // 任务状态停止  直接返回
        if (task.getStatus() != 0) {
            result.put("status", task.getStatus());
            return result;
        }

        // 有效期不满足 直接返回
        if (!DateUtils.isEffectiveDate(new Date(), task.getStartTime(), task.getEndTime())) {
            result.put("status", 1);
            return result;
        }

        Map<String, Object> param = new HashMap<>();
        param.put("vin", vo.getVin());
        // 获取 匹配到的任务
        List<DiagnosticTaskCampaignEntity> temp = ruleComponent.matchRelease(param);
        if (temp == null) {
            result.put("status", 1);
            return result;
        }
        List<DiagnosticTaskCampaignEntity> list = temp.stream().filter(v -> Objects.equals(v.getId(), task.getId())).collect(Collectors.toList());
        if (list.size() != 0) {
            result.put("status", 0);
        } else {
            result.put("status", 1);
        }
        return result;
    }

    @Override
//    @Transactional
    public void saveLogger(DiagnosticApiLoggerReqVo vo) throws IOException {
        log.info("接收客户端上传日志文件~~~~~~开始");

        Gson gson = new Gson();
        DiagnosticTaskRecordEntity record;
        record = getRecord(vo.getSessionId(), vo.getVin(), gson);

        if (record == null) {
            throw new RemoteApiException("未查询到获取记录");
        }

        String dateFormat = syncConfig.getDirFormat();
        SimpleDateFormat format = new SimpleDateFormat(dateFormat);
        String dayDir = format.format(new Date());

        File file = new File(remoteDiagnosticConfig.getTaskLog() + File.separator + "backup" + File.separator + vo.getFile().getOriginalFilename());
        FileUtils.copyInputStreamToFile(vo.getFile().getInputStream(), file);

        File dir = new File(remoteDiagnosticConfig.getTaskLog() + dayDir);

        if (!dir.exists()) {
            FilesUtil.mkdir(dir);
        }

        if (file.exists()) {
            String md5 = DigestUtils.md5DigestAsHex(new FileInputStream(new File(file.getPath())));
            if (!vo.getMd5().equals(md5)) {
                throw new RemoteApiException("md5码不一致，请检查文件是否正确");
            }
        }

        int i = file.getName().indexOf(".");
        String name = file.getName().substring(0, i) + "_" + System.currentTimeMillis() + file.getName().substring(i);

        File destFile = new File(dir.getPath() + File.separator + name);

        FileUtils.copyFile(file, destFile);

        boolean delete = file.delete();
//        delete = destFile.delete();

        DiagnosticTaskLogEntity taskLog = new DiagnosticTaskLogEntity();
        taskLog.setFileName(destFile.getName());
        taskLog.setCampaignId(record.getCampaignId());
        taskLog.setFilePath(destFile.getPath());
        taskLog.setMd5(vo.getMd5());
        taskLog.setSessionId(vo.getSessionId());
        taskLog.setVin(vo.getVin());
        taskLogDao.insert(taskLog);

        log.info("接收客户端上传日志文件~~~~~~结束");
    }

    @Override
    @Transactional
    public void saveActiveStatus(DiagnosticApiStatusReqVo vo) {
        Gson gson = new Gson();
        DiagnosticTaskRecordEntity record = getRecord(vo.getSessionId(), vo.getVin(), gson);

        if (record == null) {
            throw new RemoteApiException("未查询到获取记录");
        }
        DiagnosticTaskOverviewEntity overview = getOverview(record.getCampaignId(), gson);
        if (overview == null) {
            throw new RemoteApiException("未查询到获取记录");
        }

        try {
            selectAction(vo, record, overview);
            updateOverview(overview, vo);
//            redisUtils.hset(RemoteDiagnosticConstant.TASK_OVERVIEW_KEY, RemoteDiagnosticConstant.TASK_OVERVIEW_ITEM_KEY + overview.getCampaignId(), gson.toJson(overview), RemoteDiagnosticConstant.SEVEN_DAYS_TIMEOUT);
        } catch (Exception e) {
            redisUtils.hdel(RemoteDiagnosticConstant.TASK_ACTIVE_KEY, RemoteDiagnosticConstant.TASK_ACTIVE_ITEM_KEY + vo.getSessionId());
            throw e;
        }
    }

    @Transactional
    private void selectAction(DiagnosticApiStatusReqVo vo, DiagnosticTaskRecordEntity record, DiagnosticTaskOverviewEntity overview) {
         /*
           类型(1:匹配2:下载3:远程校验4.确认执行 5.条件校验6:上传)  action
           状态码(0:成功1:失败2:取消)   errorCode
         */
        switch (vo.getAction()) {
            case 1:
                detailService.saveDetail(record.getCampaignId(), record.getSessionId(), record.getVin(), TaskDetailTypeEnum.MATCH.getValue(), vo.getAction(), vo.getErrorCode() + "");
                if (0 == vo.getErrorCode()) {  // 匹配成功
                    record.setMatchStatus(0);
                    record.setUpdatedAt(new Date());
                    taskRecordDao.updateById(record);
                    redisUtils.hdel(RemoteDiagnosticConstant.RECORD_KEY, RemoteDiagnosticConstant.RECORD_ITEM_KEY + record.getSessionId());
                }
                break;
            case 2:
                detailService.saveDetail(record.getCampaignId(), record.getSessionId(), record.getVin(), TaskDetailTypeEnum.DOWNLOAD.getValue(), vo.getAction(), vo.getErrorCode() + "");
                break;
            case 3:
                detailService.saveDetail(record.getCampaignId(), record.getSessionId(), record.getVin(), TaskDetailTypeEnum.RemoteVerification.getValue(), vo.getAction(), vo.getErrorCode() + "");
                break;
            case 4:
                if (0 == vo.getErrorCode()) {
                    detailService.saveDetail(record.getCampaignId(), record.getSessionId(), record.getVin(), TaskDetailTypeEnum.ConfirmExecution_EXECUTE.getValue(), vo.getAction(), vo.getErrorCode() + "");
                    DiagnosticTaskActiveEntity activeEntity = activeDao.selectOne(Wrappers.<DiagnosticTaskActiveEntity>lambdaQuery().eq(DiagnosticTaskActiveEntity::getSessionId, record.getSessionId()));
                    if (activeEntity == null) {
                        activeDao.insert(new DiagnosticTaskActiveEntity(record.getCampaignId(), record.getSessionId(), record.getVin()));
                    } else {
                        activeEntity.setStatus(0);
                        activeEntity.setUpdatedAt(new Date());
                        activeDao.updateById(activeEntity);
                    }
                } else if (1 == vo.getErrorCode()) {
                    detailService.saveDetail(record.getCampaignId(), record.getSessionId(), record.getVin(), TaskDetailTypeEnum.ConfirmExecution_REJECT.getValue(), vo.getAction(), "0");
                    updateActiveTask(record);
                } else {
                    detailService.saveDetail(record.getCampaignId(), record.getSessionId(), record.getVin(), TaskDetailTypeEnum.ConfirmExecution_CANCEL.getValue(), vo.getAction(), "0");
                    updateActiveTask(record);
                }
                redisUtils.hdel(RemoteDiagnosticConstant.TASK_ACTIVE_KEY, RemoteDiagnosticConstant.TASK_ACTIVE_ITEM_KEY + vo.getSessionId());
                break;
            case 5:
                detailService.saveDetail(record.getCampaignId(), record.getSessionId(), record.getVin(), TaskDetailTypeEnum.ConditionalVerification.getValue(), vo.getAction(), vo.getErrorCode() + "");
                break;
            case 6:
                detailService.saveDetail(record.getCampaignId(), record.getSessionId(), record.getVin(), TaskDetailTypeEnum.UPLOAD_LOG.getValue(), vo.getAction(), vo.getErrorCode() + "");
                break;
        }
    }

    @Override
    public Map<String, Object> selectVehicleCode(DiagnosticApiStatusReqVo vo) {
        Map<String, Object> map = new HashMap<>();
        Gson gson = new Gson();
        DiagnosticTaskActiveEntity active = getActive(vo.getSessionId(), gson);
        if (active == null) {
            map.put("status", 3);
            return map;
        }
        map.put("status", active.getStatus());
        return map;
    }

    @Override
    public Long selectOtxFileSize(String md5) {
        LambdaQueryWrapper<DiagnosticTaskLogEntity> qw = Wrappers.<DiagnosticTaskLogEntity>lambdaQuery();
        qw.eq(DiagnosticTaskLogEntity::getMd5, md5);
        DiagnosticTaskLogEntity taskLog = taskLogDao.selectOne(qw);
        if (taskLog == null) {
            return -1L;
        } else {
            return taskLog.getFileLength();
        }
    }

    private void updateActiveTask(DiagnosticTaskRecordEntity record) {
        DiagnosticTaskActiveEntity info = activeDao.getInfoBySessionId(record.getSessionId());
        if (info != null) {
            info.setStatus(1);
            info.setUpdatedAt(new Date());
            activeDao.updateById(info);
        }
    }

    private void updateOverview(DiagnosticTaskOverviewEntity overview, DiagnosticApiStatusReqVo vo) {
         /*
           类型(1:匹配2:下载3:远程校验4.确认执行 5.条件校验6:上传)  action
           状态码(0:成功1:失败2:取消)   errorCode
         */
        switch (vo.getAction()) {
            case 1:
                if (redisUtils.hHasKey(RemoteDiagnosticConstant.TASK_OVERVIEW_KEY + overview.getCampaignId(), RemoteDiagnosticConstant.TASK_OVERVIEW_MATCH_KEY)) {
                    redisUtils.hincr(RemoteDiagnosticConstant.TASK_OVERVIEW_KEY + overview.getCampaignId(), RemoteDiagnosticConstant.TASK_OVERVIEW_MATCH_KEY, 1L);
                } else {
                    overviewAutoincrement(overview, TaskDetailTypeEnum.MATCH.getValue(), null);
                }
                break;
            case 2:
                if (redisUtils.hHasKey(RemoteDiagnosticConstant.TASK_OVERVIEW_KEY + overview.getCampaignId(), RemoteDiagnosticConstant.TASK_OVERVIEW_DOWNLOAD_KEY)) {
                    redisUtils.hincr(RemoteDiagnosticConstant.TASK_OVERVIEW_KEY + overview.getCampaignId(), RemoteDiagnosticConstant.TASK_OVERVIEW_DOWNLOAD_KEY, 1L);
                } else {
                    overviewAutoincrement(overview, TaskDetailTypeEnum.DOWNLOAD.getValue(), null);
                }
                break;
            case 4:
                if (0 == vo.getErrorCode()) {
                    if (redisUtils.hHasKey(RemoteDiagnosticConstant.TASK_OVERVIEW_KEY + overview.getCampaignId(), RemoteDiagnosticConstant.TASK_OVERVIEW_ACCEPTANCE_KEY)) {
                        redisUtils.hincr(RemoteDiagnosticConstant.TASK_OVERVIEW_KEY + overview.getCampaignId(), RemoteDiagnosticConstant.TASK_OVERVIEW_ACCEPTANCE_KEY, 1L);
                    } else {
                        overviewAutoincrement(overview, TaskDetailTypeEnum.ConfirmExecution_EXECUTE.getValue(), null);
                    }
                } else if (1 == vo.getErrorCode()) {
                    if (redisUtils.hHasKey(RemoteDiagnosticConstant.TASK_OVERVIEW_KEY + overview.getCampaignId(), RemoteDiagnosticConstant.TASK_OVERVIEW_REJECT_KEY)) {
                        redisUtils.hincr(RemoteDiagnosticConstant.TASK_OVERVIEW_KEY + overview.getCampaignId(), RemoteDiagnosticConstant.TASK_OVERVIEW_REJECT_KEY, 1L);
                    } else {
                        overviewAutoincrement(overview, TaskDetailTypeEnum.ConfirmExecution_CANCEL.getValue(), null);
                    }
                } else {
                    if (redisUtils.hHasKey(RemoteDiagnosticConstant.TASK_OVERVIEW_KEY + overview.getCampaignId(), RemoteDiagnosticConstant.TASK_OVERVIEW_CANCEL_KEY)) {
                        redisUtils.hincr(RemoteDiagnosticConstant.TASK_OVERVIEW_KEY + overview.getCampaignId(), RemoteDiagnosticConstant.TASK_OVERVIEW_CANCEL_KEY, 1L);
                    } else {
                        overviewAutoincrement(overview, TaskDetailTypeEnum.ConfirmExecution_REJECT.getValue(), null);
                    }
                }
                break;
        }
    }

    private void overviewAutoincrement(DiagnosticTaskOverviewEntity overview, String type, DiagnosticTaskCampaignEntity task) {

        RLock lock = redissonClient.getLock(RemoteDiagnosticConstant.CAMPAIGN_OVERVIEW_LOCK + overview.getCampaignId());
        // 加锁
        lock.lock();
        try {
            DiagnosticTaskOverviewEntity o = taskOverviewDao.selectById(overview.getId());
            String key = RemoteDiagnosticConstant.TASK_OVERVIEW_KEY + overview.getCampaignId();
            switch (type) {
                case "MATCH":
                    if (redisUtils.hHasKey(key, RemoteDiagnosticConstant.TASK_OVERVIEW_MATCH_KEY)) {
                        redisUtils.hincr(key, RemoteDiagnosticConstant.TASK_OVERVIEW_MATCH_KEY, 1L);
                    } else {
                        redisUtils.hset(key, RemoteDiagnosticConstant.TASK_OVERVIEW_MATCH_KEY, o.getMatchNum() + 1 + "");
                    }
                    break;
                case "DOWNLOAD":
                    if (redisUtils.hHasKey(key, RemoteDiagnosticConstant.TASK_OVERVIEW_DOWNLOAD_KEY)) {
                        redisUtils.hincr(key, RemoteDiagnosticConstant.TASK_OVERVIEW_DOWNLOAD_KEY, 1L);
                    } else {
                        redisUtils.hset(key, RemoteDiagnosticConstant.TASK_OVERVIEW_DOWNLOAD_KEY, o.getDownloadNum() + 1 + "");
                    }
                    break;
                case "ConfirmExecution:EXECUTE":
                    if (redisUtils.hHasKey(key, RemoteDiagnosticConstant.TASK_OVERVIEW_ACCEPTANCE_KEY)) {
                        redisUtils.hincr(key, RemoteDiagnosticConstant.TASK_OVERVIEW_ACCEPTANCE_KEY, 1L);
                    } else {
                        redisUtils.hset(key, RemoteDiagnosticConstant.TASK_OVERVIEW_ACCEPTANCE_KEY, o.getAcceptanceNum() + 1 + "");
                    }
                    break;
                case "ConfirmExecution:CANCEL":
                    if (redisUtils.hHasKey(key, RemoteDiagnosticConstant.TASK_OVERVIEW_CANCEL_KEY)) {
                        redisUtils.hincr(key, RemoteDiagnosticConstant.TASK_OVERVIEW_CANCEL_KEY, 1L);
                    } else {
                        redisUtils.hset(key, RemoteDiagnosticConstant.TASK_OVERVIEW_CANCEL_KEY, o.getCancelNum() + 1 + "");
                    }
                    break;
                case "ConfirmExecution:REJECT":
                    if (redisUtils.hHasKey(key, RemoteDiagnosticConstant.TASK_OVERVIEW_REJECT_KEY)) {
                        redisUtils.hincr(key, RemoteDiagnosticConstant.TASK_OVERVIEW_REJECT_KEY, 1L);
                    } else {
                        redisUtils.hset(key, RemoteDiagnosticConstant.TASK_OVERVIEW_REJECT_KEY, o.getRejectNum() + 1 + "");
                    }
                    break;
                case "SUCCESS":
                    if (redisUtils.hHasKey(key, RemoteDiagnosticConstant.TASK_OVERVIEW_SUCCESS_KEY)) {
                        redisUtils.hincr(key, RemoteDiagnosticConstant.TASK_OVERVIEW_SUCCESS_KEY, 1L);
                    } else {
                        redisUtils.hset(key, RemoteDiagnosticConstant.TASK_OVERVIEW_SUCCESS_KEY, o.getSuccessNum() + 1 + "");
                    }
                    break;
                case "FAIL":
                    if (redisUtils.hHasKey(key, RemoteDiagnosticConstant.TASK_OVERVIEW_FAIL_KEY)) {
                        redisUtils.hincr(key, RemoteDiagnosticConstant.TASK_OVERVIEW_FAIL_KEY, 1L);
                    } else {
                        redisUtils.hset(key, RemoteDiagnosticConstant.TASK_OVERVIEW_FAIL_KEY, o.getFailNum() + 1 + "");
                    }
                    break;
            }
            // 梯度校验
            if (task != null) {
                checkTaskGradient(overview, task);
            }
        } finally {
            lock.unlock();
        }
//        redisUtils.hincr(key, item, 1L);
    }

    private void checkTaskGradient(DiagnosticTaskOverviewEntity overview, DiagnosticTaskCampaignEntity task) {

        List<TaskGradientDto> gradient = task.getGradient();
//        gradient = gradient.stream().sorted(Comparator.comparing(TaskGradientDto::getFlag)).collect(Collectors.toList());
        final String key = RemoteDiagnosticConstant.TASK_OVERVIEW_KEY + overview.getCampaignId();

        if (ObjectUtils.isNotEmpty(gradient)) {
            for (TaskGradientDto taskGradientDto : gradient) {

                //  上传结果 只判断反向梯度
                if (StringUtils.isEmpty(taskGradientDto.getGradientValue()) || taskGradientDto.getFlag() == 0) {
                    continue;
                }

                // 成功次数
                long success;
                if (redisUtils.hHasKey(key, RemoteDiagnosticConstant.TASK_OVERVIEW_SUCCESS_KEY)) {
                    success = Long.parseLong(redisUtils.hget(key, RemoteDiagnosticConstant.TASK_OVERVIEW_SUCCESS_KEY).toString());
                } else {
                    success = overview.getSuccessNum();
                }

                // 失败次数
                long fail;
                if (redisUtils.hHasKey(key, RemoteDiagnosticConstant.TASK_OVERVIEW_FAIL_KEY)) {
                    fail = Long.parseLong(redisUtils.hget(key, RemoteDiagnosticConstant.TASK_OVERVIEW_FAIL_KEY).toString());
                } else {
                    fail = overview.getFailNum();
                }
                if (taskGradientDto.getFlag() == 1) {
                    if (0 == taskGradientDto.getGradientCondition()) {  // 条件%
                        if ((success + fail) == taskGradientDto.getGradient()) {
                            double i = (double) fail / (fail + success);
                            double j = (double) taskGradientDto.getGradientValue() / taskGradientDto.getGradient();
                            if (i >= j) {
                                updateByGradient(overview, task);
                            }
                        }
                    } else {             // 条件 >
                        if ((fail + success) <= taskGradientDto.getGradient()) {
                            if (fail >= taskGradientDto.getGradientValue()) {
                                updateByGradient(overview, task);
                            }
                        }
                    }
                }
            }
        }
    }

    private void updateByGradient(DiagnosticTaskOverviewEntity overview, DiagnosticTaskCampaignEntity task) {
        task.setUpdatedAt(new Date());
        task.setStatus(1);
        campaignDao.updateById(task);

        List<DiagnosticTaskRecordEntity> records = taskRecordDao.getListByTask(task.getId());
        List<DiagnosticTaskDetailEntity> details = new ArrayList<>();

        // 给其它车辆创建失败记录
        if (records != null && records.size() != 0) {
            records.forEach(s -> details.add(new DiagnosticTaskDetailEntity(s.getCampaignId(), s.getSessionId(), TaskDetailTypeEnum.GradientCheck.getValue(), "1", "梯度校验失败", s.getVin())));
            taskDetailDao.inserts(details);
        }


//        List<DiagnosticTaskActiveEntity> actives = activeDao.getListByTask(0, overview.getTaskId());
        activeDao.updateByTaskId(0, overview.getCampaignId());

        redisUtils.hdel(RemoteDiagnosticConstant.CAMPAIGN_KEY, RemoteDiagnosticConstant.CAMPAIGN_ITEM_KEY + task.getId());

    }

    private void saveRecord(DiagnosticTaskDto task, String vin) {
        Gson gson = new Gson();
        DiagnosticTaskRecordEntity record = new DiagnosticTaskRecordEntity();
        record.setCampaignId(task.getId());
        record.setVin(vin);
        record.setSessionId(task.getSessionId());
        taskRecordDao.insert(record);

        redisUtils.hset(RemoteDiagnosticConstant.RECORD_KEY, RemoteDiagnosticConstant.RECORD_ITEM_KEY + record.getSessionId(), gson.toJson(record), RemoteDiagnosticConstant.SEVEN_DAYS_TIMEOUT);
    }

    private void saveDetail(DiagnosticTaskDto task, String vin) {
        DiagnosticTaskDetailEntity diagnosticTaskDetailEntity = new DiagnosticTaskDetailEntity();
        diagnosticTaskDetailEntity.setCampaignId(task.getId());
        diagnosticTaskDetailEntity.setVin(vin);
        diagnosticTaskDetailEntity.setSessionId(task.getSessionId());
        diagnosticTaskDetailEntity.setType(TaskDetailTypeEnum.MATCH.getValue());
        diagnosticTaskDetailEntity.setStartTime(new Date());
        diagnosticTaskDetailEntity.setEndTime(new Date());
        taskDetailDao.insert(diagnosticTaskDetailEntity);
    }

    private void saveOrUpdateOverview(DiagnosticTaskCampaignEntity task) {
        DiagnosticTaskOverviewEntity overview;
        overview = taskOverviewDao.getInfoByTask(task.getId());
        if (overview == null) {
            overview = new DiagnosticTaskOverviewEntity();
            overview.setCampaignId(task.getId());
            taskOverviewDao.insert(overview);
        }
//        overview.setMatchNum(overview.getMatchNum() == null ? 1 : overview.getMatchNum() + 1);

//        if (overview.getId() == null) {
//        }
//        else {
//            overview.setUpdatedAt(new Date());
//            taskOverviewDao.updateById(overview);
//        }
    }

}
