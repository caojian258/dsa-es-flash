package org.dsa.config;

import ch.qos.logback.classic.Logger;
import org.dsa.modules.diagnostic.service.impl.DiagnosticRedisServiceImpl;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;

import jakarta.annotation.Resource;
import java.net.InetAddress;


//@Component
public class RedisKeyExpirationListener extends KeyExpirationEventMessageListener {

    public RedisKeyExpirationListener(RedisMessageListenerContainer listenerContainer) {
        super(listenerContainer);
    }

    Logger logger = (Logger) LoggerFactory.getLogger(this.getClass());

    //@Value("${spring.redis.listener-host-name}")
    private String listenerHostName;

    //@Resource
    DiagnosticRedisServiceImpl diagnosticRedisServiceImpl;

    /**
     * 针对redis数据失效事件，进行数据处理
     * @param message
     * @param pattern
     */
    //@Override
    public void onMessage(Message message, byte[] pattern) {
        String expiredKey = message.toString();
        logger.info("RedisKeyExpiration , Keys :{}",expiredKey);
        try {
            if(!InetAddress.getLocalHost().getHostName().equals(listenerHostName) ){
                this.destroy();
            }else{
                diagnosticRedisServiceImpl.overdue(expiredKey);
            }
        } catch (Exception e) {
            logger.info("RedisKeyExpiration , destroy error :{}",e.getMessage());
        }
    }

    @Override
    public void destroy() throws Exception {
        super.destroy();
    }
}
