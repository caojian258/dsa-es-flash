package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.dsa.modules.reporter.entity.WorkshopGroup;

import java.util.List;

@Mapper
public interface WorkshopGroupMapper extends BaseMapper<WorkshopGroup> {

    List<WorkshopGroup> selectAllGroup();
}
