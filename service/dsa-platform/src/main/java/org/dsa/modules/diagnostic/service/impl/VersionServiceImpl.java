package org.dsa.modules.diagnostic.service.impl;

import ch.qos.logback.classic.Logger;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.*;
import org.dsa.modules.diagnostic.dao.DVersionInfoDao;
import org.dsa.modules.diagnostic.dao.DVersionRecordDao;
import org.dsa.modules.diagnostic.entity.DVersionInfoEntity;
import org.dsa.modules.diagnostic.entity.DVersionRecordEntity;
import org.dsa.modules.diagnostic.entity.VersionInfoEntity;
import org.dsa.modules.diagnostic.service.VersionService;
import org.dsa.modules.sys.entity.FileEntity;
import org.dsa.modules.vehicle.dao.DVehicleInfoDao;
import org.dsa.modules.vehicle.dao.DVehicleTypeDao;
import org.dsa.modules.vehicle.entity.DVehicleInfoEntity;
import org.dsa.modules.vehicle.entity.DVehicleTypeEntity;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

@Service("versionService")
public class VersionServiceImpl implements VersionService {
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass());

    @Value("${file.base.path}")
    private String filePath;

    @Resource
    private RedisUtils redisUtils;

    @Resource
    private DVehicleInfoDao dVehicleInfoDao;

    @Resource
    private DVehicleTypeDao dVehicleTypeDao;

    @Resource
    private DVersionInfoDao dVersionDao;

    @Resource
    private DVersionRecordDao dVersionRecordDao;

    @Value("${file.version.download.path}")
    private String versionDownloadPath;

    @Override
    public List<DVehicleTypeEntity> getTreeList() {
        return dVehicleTypeDao.selectTreeList();
    }

    @Override
    public List<DVersionInfoEntity> list(Long vehicleTypeId, String versionType) {
        return dVersionDao.selectByType(vehicleTypeId,versionType);
    }

    @Override
    public List<DVersionInfoEntity> list(Long vehicleTypeId, DVersionInfoEntity start, DVersionInfoEntity end, String type) {
        LambdaQueryWrapper<DVersionInfoEntity> qw = Wrappers.lambdaQuery();
        qw.eq(DVersionInfoEntity::getVehicleTypeId, vehicleTypeId)
                .eq(DVersionInfoEntity::getVersionType, type)
                .eq(DVersionInfoEntity::getStatus, Constant.versionStatus.OPEN.getCode())
                .gt(Objects.nonNull(start), DVersionInfoEntity::getId, start.getId())
                .le(DVersionInfoEntity::getId, end.getId());
        return dVersionDao.selectList(qw);
    }

    @Override
    public List<DVersionInfoEntity> availableList(Long vehicleTypeId, String versionType) {
        return list(vehicleTypeId, versionType).stream()
                .filter((e) -> Constant.versionStatus.OPEN.getCode().equals(e.getStatus()))
                .sorted(Comparator.comparing(DVersionInfoEntity::getId).reversed())
                .collect(Collectors.toList());
    }

    @Override
    public DVersionInfoEntity getDVersionInfoById(Long id) {
        //根据版本id获取版本信息
        DVersionInfoEntity dVersionInfoEntity = dVersionDao.selectById(id);
        if(dVersionInfoEntity == null){
            throw new RRException( Constant.Msg.VERSION_EMPTY );
        }
        return  dVersionInfoEntity;
    }

    @Override
    public DVersionInfoEntity get(Long vehicleTypeId, String type, String versionNum) {
        LambdaQueryWrapper<DVersionInfoEntity> qw = Wrappers.lambdaQuery();
        qw.eq(DVersionInfoEntity::getVehicleTypeId, vehicleTypeId)
                .eq(DVersionInfoEntity::getVersionType, type)
                .eq(DVersionInfoEntity::getVersionNum, versionNum)
                .eq(DVersionInfoEntity::getStatus, Constant.versionStatus.OPEN.getCode());
        return dVersionDao.selectOne(qw);
    }

    @Override
    public VersionInfoEntity getVersionInfo(Long id) {

        VersionInfoEntity versionInfoEntity = new VersionInfoEntity();
        //根据版本id获取版本信息
        DVersionInfoEntity dVersionInfoEntity = dVersionDao.selectById(id);

        //根据版本id获取版本 修改记录
        List<DVersionRecordEntity> l = dVersionRecordDao.selectListByVersionId(id);

        versionInfoEntity.setDVersionInfoEntity(dVersionInfoEntity);
        versionInfoEntity.setList(l);

        return versionInfoEntity;
    }

    @Override
    public void save(DVersionInfoEntity versionEntity) {
        //最大版本号
        List<DVersionInfoEntity>  l = dVersionDao.selectOpenVersion(versionEntity.getVehicleTypeId(),versionEntity.getVersionType());

        //上级版本
        String parentVersionNum = dVersionDao.selectParentVersionNumByType(versionEntity.getVehicleTypeId(),versionEntity.getVersionType());

        //初版上级版本为0
        if(l != null && l.size()>0){
            parentVersionNum = l.get(0).getVersionNum();
        }else{
            parentVersionNum = "0";
        }

        //YYYY-MM-DD-V(N)
        String versionNum = DateUtils.format(new Date(),"yyyyMMddHHmm")+"_V"+l.size();
        versionEntity.setVersionNum(versionNum);
        versionEntity.setParentVersionNum(parentVersionNum);
        versionEntity.setStatus(versionEntity.getStatus());
        versionEntity.setCreateBy(ShiroUtils.getUserId());
        dVersionDao.insertDVersionInfo(versionEntity);

        //保存版本修改记录
        saveDVersionRecord(Constant.editType.ADD, versionEntity);
    }



    @Override
    public void update(DVersionInfoEntity versionEntity) {
        //修改版本信息
        versionEntity.setCreateBy(ShiroUtils.getUserId());
        dVersionDao.updateById(versionEntity);
        //保存版本修改记录
        saveDVersionRecord(Constant.editType.EDIT,versionEntity);
    }

    @Override
    public List<DVersionRecordEntity> getVersionRecord(Long id) {
        //根据版本id查询版本修改记录
        return dVersionRecordDao.selectListByVersionId(id);
    }

    @Override
    public void checkVersionStatus(Long vehicleTypeId, String versionType) {
        //开发\测试状态的版本
        List<DVersionInfoEntity>  l = dVersionDao.selectOpenVersionByType(vehicleTypeId,versionType);
        if(l != null && l.size() >0){
            throw new RRException( Constant.Msg.VERSION_CHECK_STATUS );
        }
    }

    @Override
    public int selectVersions(Long vehicleTypeId, String versionType) {
        //根据车型信息和版本类型 统计版本数量
        return dVersionDao.selectVersions(vehicleTypeId,versionType);
    }

    @Override
    public DVersionInfoEntity compareVersionFile(String vin,  String versionNum,  String versionType) {
        //校验vin
        if(StringUtils.isEmpty(vin)  ){
            throw new RRException(Constant.Msg.CHECK_VIN);
        }
        //校验版本类型 odx/otx
        if(StringUtils.isEmpty(versionType)){
            throw new RRException(Constant.Msg.VERSION_CHECK_TYPE);
        }else if(!(Constant.versionType.ODX.getName().equals(versionType) || Constant.versionType.OTX.getName().equals(versionType))){
            throw new RRException(Constant.Msg.VERSION_CHECK_TYPE_FAIL);
        }

        //返回对象
        DVersionInfoEntity dVersionInfoEntity = null;

        //根据vin码查车辆对应车型信息
        DVehicleInfoEntity vehicleInfoEntity = dVehicleInfoDao.selectOneByVin(vin);
        //vin码无车辆
        if(vehicleInfoEntity == null){
            throw new RRException(Constant.Msg.CHECK_VIN);
        }

        Long vehicleTypeId = null;
        //根据车型id,查询节点树,匹配版本的车型信息
        List<DVehicleTypeEntity> vehicleTypeEntityList = dVehicleTypeDao.selectListById(vehicleInfoEntity.getVehicleTypeId());
        for(DVehicleTypeEntity vehicleTypeEntity : vehicleTypeEntityList){
            //是否存在版本
            int haveVersion = 0;
            if(Constant.versionType.ODX.getName().equals(versionType)){
                haveVersion = vehicleTypeEntity.getHaveOdxVersion();
            }else{
                haveVersion = vehicleTypeEntity.getHaveOtxVersion();
            }

            //有版本信息的最根部的节点
            if(haveVersion == Constant.VEHICLE_TYPE_HAVE_VERSION.IN.getValue() ){
                //根据节点匹配  根据最小版本 升级
                dVersionInfoEntity = dVersionDao.selectMinOpenVersionByTypeId(vehicleTypeEntity.getId(),versionType);
                //当前节点 传入版本与最新版本比对,是否有全量版本.
                Integer targetType = dVersionDao.selectTargetTypeByTypeId(null,vehicleTypeEntity.getId(),versionType);
                if(targetType != null && targetType > 0 ){
                    dVersionInfoEntity.setTargetType("FULL");
                }else{
                    dVersionInfoEntity.setTargetType("ADD");
                }
                vehicleTypeId = vehicleTypeEntity.getId();
                break;
            }
        }

        //根据vin无匹配版本 不对比版本号直接返回
        if(dVersionInfoEntity==null){
            return null;
        }

        //版本不为空
        if(StringUtils.isNotEmpty(versionNum)){
            //根据版本号找出版本信息
            DVersionInfoEntity versionInfoEntity = dVersionDao.selectOpenVersionByVersionNum(vehicleTypeId, versionType, versionNum);
            if(versionInfoEntity != null) {
                //如果传入的版本号  和根据vin码车型一致
                if (versionInfoEntity.getVehicleTypeId().equals(dVersionInfoEntity.getVehicleTypeId())) {
                    //如果传入版本 == 根据vin查车型最新版本 返回空
                    if (versionInfoEntity.getId().equals(dVersionInfoEntity.getId())) {
                        dVersionInfoEntity.setVersionFileName(null);
                    }else{
                        //当前节点 传入版本与最新版本比对,是否有全量版本.
                        Integer targetType = dVersionDao.selectTargetTypeByTypeId(versionInfoEntity.getId(),versionInfoEntity.getVehicleTypeId(),versionType);
                        if(targetType != null && targetType > 0 ){
                            versionInfoEntity.setTargetType("FULL");
                        }else{
                            versionInfoEntity.setTargetType("ADD");
                        }
                        versionInfoEntity.setVersionNum(dVersionInfoEntity.getVersionNum());
                        versionInfoEntity.setDescription(dVersionInfoEntity.getDescription());
                        versionInfoEntity.setVersionFileName(dVersionInfoEntity.getVersionFileName());
                        versionInfoEntity.setVersionFileMd5(dVersionInfoEntity.getVersionFileMd5());
                        return versionInfoEntity;
                    }
                }
            }else{
                throw new RRException(Constant.Msg.VERSION_NUMBER_EMPTY);
            }
        }

        return dVersionInfoEntity;
    }

    @Override
    public void buildVersionFile(Long vehicleTypeId, String versionType) {

        //是否遇到全量配置文件
        boolean isNotLastFullFile = true;
        //遇到全量配置前的版本集合
        List<DVersionInfoEntity> list = new ArrayList<DVersionInfoEntity>();
        //遇到全量配置后需要更新的集合
        List<DVersionInfoEntity> updateList = new ArrayList<DVersionInfoEntity>();
        //需匹配解压版本集合
        List<DVersionInfoEntity> unzipList = new ArrayList<DVersionInfoEntity>();

        //1. 所有释放状态的版本  倒序
        List<DVersionInfoEntity>  l = dVersionDao.selectOpenVersion(vehicleTypeId,versionType);

        //2.循环设置当前版本对应最新版本文件
        for(DVersionInfoEntity dVersionInfoEntity: l){
            //是否遇到全量文件
            if(isNotLastFullFile){
                if(list.size()==0) {
                    //最后一个版本是全量，更新所有版本，并且结束
                    if(Constant.fileType.FULL.getCode().equals(dVersionInfoEntity.getFileType())) {
                        logger.info("this version is one is full");
                        Map<String,String> fileMap = new HashMap<String,String>();
                        fileMap = copyFullFile(dVersionInfoEntity.getFilePath(), dVersionInfoEntity.getFileName());
                        dVersionDao.updateFullVersionDownloadFile(null, vehicleTypeId, versionType, fileMap.get("fileName"), fileMap.get("md5"));
                        return;
                    }
                }else{
                    //如果遇到全量，全量版本是最后一个需要打包的文件
                    if(Constant.fileType.FULL.getCode().equals(dVersionInfoEntity.getFileType())) {
                        isNotLastFullFile = false;
                    }
                }
                list.add(dVersionInfoEntity);
            }else{
                updateList.add(dVersionInfoEntity);
            }
        }
        logger.info("buildVersionFile list size:{}",list.size());
        logger.info("buildVersionFile updateList size:{}",updateList.size());

        //3.循环需要生成版本的集合   新版本更新旧版本
        for(DVersionInfoEntity dVersionInfoEntity  : list){
            copyAddFile(dVersionInfoEntity,unzipList);
            logger.info("unzipList add version_num {}",dVersionInfoEntity.getVersionNum());
            unzipList.add(dVersionInfoEntity);
            logger.info("buildVersionFile updateParentVersionDownloadFile {}",dVersionInfoEntity);
            dVersionDao.updateParentVersionDownloadFile(dVersionInfoEntity);
        }
        logger.info("buildVersionFile unzipList size:{}",list.size());

        //4.最新版本是增量,版本文件
        if(unzipList.size() >0 ){
            DVersionInfoEntity dVersionInfoEntity = list.get(0);
            copyAddFile(dVersionInfoEntity,unzipList);
            dVersionDao.updateVersionDownloadFileById(dVersionInfoEntity);
        }

        //5. 全量版本之前的版本更新
        if(updateList.size()>0){
            DVersionInfoEntity d = list.get(list.size()-1);
            for(DVersionInfoEntity dVersionInfoEntity : updateList) {
                dVersionInfoEntity.setVersionFileName(d.getVersionFileName());
                dVersionInfoEntity.setVersionFileMd5(d.getVersionFileMd5());
                logger.info("buildVersionFile updateVersionDownloadFileById {}",dVersionInfoEntity);
                dVersionDao.updateVersionDownloadFileById(dVersionInfoEntity);
            }
        }

        /*//6. 最新版本 更新文件 = 最旧版本.
        DVersionInfoEntity lastVersion = l.get(0);
        DVersionInfoEntity afterVersion = list.get(list.size()-1);
        lastVersion.setVersionFileName(afterVersion.getVersionFileName());
        lastVersion.setVersionFileMd5(afterVersion.getVersionFileMd5());
        dVersionDao.updateVersionDownloadFileById(lastVersion);*/
    }

    @Override
    public void updateVehicleTypeVersion(Long id,String versionType) {
        dVehicleTypeDao.updateVehicleTypeVersion(id,versionType);
    }


    //增量版本
    private void copyAddFile(DVersionInfoEntity dVersionInfoEntity, List<DVersionInfoEntity> unzipList) {
        //命令执行返回数据流
        Scanner input = null;
        //命令执行接收结果
        Process process = null;
        //执行结果
        String result = null;
        if(unzipList.size() == 0) {
            //最新版本
            String timePrefix = new Date().getTime() + "";
            String toFileName = timePrefix + Constant.TIME_PREFIX + dVersionInfoEntity.getFileName() ;
            //执行脚本
            logger.info("COPY_ADD_VERSION_FILE fileName:{}",toFileName);
            result = runCommand(Constant.COPY_ADD_VERSION_FILE + dVersionInfoEntity.getFilePath() + " " + versionDownloadPath + toFileName );
            //修改版本信息
            dVersionInfoEntity.setVersionFileName(toFileName);
            dVersionInfoEntity.setVersionFileMd5(result);
        }else{
            //1.设置解压路径  时间戳+版本名
            String toPathName =  new Date().getTime() + Constant.TIME_PREFIX + dVersionInfoEntity.getVersionNum();

            //2.先解压传入版本(当前版本)
            logger.info("UNZIP_VERSION_COPY_FILE - file :{}",dVersionInfoEntity.getFilePath());
            runCommand(Constant.UNZIP_VERSION_COPY_FILE + dVersionInfoEntity.getFilePath() + " " + versionDownloadPath+ toPathName);

            //3.循环当前 之上版本 ,并解压替换
            if(unzipList.size() >0) {
                for (int i = unzipList.size()-1; i >= 0; i--) {
                    logger.info("UNZIP_VERSION_COPY_FILE file :{}",unzipList.get(i).getFilePath());
                    runCommand(Constant.UNZIP_VERSION_COPY_FILE + unzipList.get(i).getFilePath() + " " + versionDownloadPath+ toPathName);
                }
            }

            //4.压缩下载版本
            ZipUtil.toZip(versionDownloadPath+ toPathName ,versionDownloadPath+toPathName+Constant.ZIP_FILE_PREFIX,true);

            //5.获取版本md5码
            logger.info("VERSION_FILE_MD5 - file :{}",versionDownloadPath+ toPathName+Constant.ZIP_FILE_PREFIX);
            result=runCommand(Constant.VERSION_FILE_MD5 + versionDownloadPath+ toPathName+Constant.ZIP_FILE_PREFIX);
            logger.info("VERSION_FILE_MD5 result:{}",result);

            //6.修改版本信息
            dVersionInfoEntity.setVersionFileName(toPathName+Constant.ZIP_FILE_PREFIX);
            dVersionInfoEntity.setVersionFileMd5(result);
        }
    }

    //全量版本
    private Map<String, String> copyFullFile(String filePath ,String toFileName) {
        Map<String,String> map = new HashMap<String,String>();
        //执行结果
        String result = null;
        String timePrefix = new Date().getTime() + "";
        toFileName = timePrefix + Constant.TIME_PREFIX + toFileName;
        map.put("fileName" , toFileName);
        logger.info("copyFullFile fileName:{}",toFileName);
        result = runCommand(Constant.COPY_FULL_VERSION_FILE + filePath+" " + versionDownloadPath + toFileName );
        logger.info("copyFullFile result:{}",result);
        if(StringUtils.isEmpty(result)){
            throw new RRException(Constant.Msg.VERSION_MACK_FILE_FAIL);
        }

        map.put("md5",result);
        return map;
    }

    private String runCommand(String command){
        logger.info("run command {}",command);
        //命令执行返回数据流
        Scanner input = null;
        //命令执行接收结果
        Process process = null;
        //执行结果
        String result = "";
        try {
            //执行脚本
            process = Runtime.getRuntime().exec(command );
            //等命令执行结果
            process.waitFor();
            //取执行结果数据流
            InputStream is = process.getInputStream();
            //数据流转换
            input = new Scanner(is);
            while (input.hasNextLine()){
                String r = input.nextLine();
                logger.info("run command result -r:{}",r);
                if(StringUtils.isNotEmpty(r) && !"null".equals(r)){
                    result += r;
                }
            }
            //关闭数据流
            is.close();
            //关闭转换
            input.close();
            if(StringUtils.isEmpty(result) ){
                throw new RRException(Constant.Msg.VERSION_MACK_FILE_FAIL);
            }
            logger.info("run command result:{}",result);
        } catch (IOException | InterruptedException e) {
            logger.info("IOException:{}",e.getMessage());
            throw new RRException(Constant.Msg.VERSION_MACK_FILE_FAIL);
        }finally {
            //关闭命令执行线程
            process.destroy();
        }
        return result;
    }

    //保存操作记录
    private void saveDVersionRecord(Constant.editType editType, DVersionInfoEntity versionEntity) {
        DVersionRecordEntity dVersionRecordEntity = new DVersionRecordEntity();
        dVersionRecordEntity.setEditType(editType.getCode());
        dVersionRecordEntity.setVersionId(versionEntity.getId());
        dVersionRecordEntity.setStatus(versionEntity.getStatus());
        dVersionRecordEntity.setCreateBy(ShiroUtils.getUserId());
        dVersionRecordEntity.setCreateUserName(ShiroUtils.getUserName());
        dVersionRecordDao.insert(dVersionRecordEntity);
    }


    @Override
    public FileEntity uploadFiles(MultipartFile file, String versionType, HttpServletRequest req) {
        FileEntity fileEntity = new FileEntity();
        File filedir = new File(filePath);
        //如果路径不存在则创建
        if (!filedir.exists())
            filedir.mkdirs();
        //附件名前加时间戳
        String timePrefix = new Date().getTime() + "";
        String filename = timePrefix + Constant.TIME_PREFIX + file.getOriginalFilename();
        File sourcefile = new File(filePath + versionType + Constant.PATH_SPLIT + filename);

        try {
            FileUtils.copyInputStreamToFile( file.getInputStream(), sourcefile );
        } catch (IOException e) {
            logger.error(e.getMessage());
            throw new RRException(Constant.Msg.VERSION_FILE_ERROR);
        }

        if(!ZipUtil.checkDiagFile(sourcefile, versionType)) {
            logger.info("check in zip of dir fail, filename :{}", sourcefile.getPath());
            throw new RRException(Constant.Msg.VERSION_FILE_FAIL);
        }

        fileEntity.setRealName( filename );
        fileEntity.setName(file.getOriginalFilename());
        fileEntity.setUrl(filePath + versionType + Constant.PATH_SPLIT + filename);
        return fileEntity;
    }

    @Override
    public FileEntity uploadDBFiles(MultipartFile file, String versionType) {
        FileEntity fileEntity = new FileEntity();
        String dbFilePath = filePath + versionType + "_DB";
        File filedir = new File(dbFilePath);

        //如果路径不存在则创建
        if (!filedir.exists())
            filedir.mkdirs();

        //附件名前加时间戳
        String timePrefix = new Date().getTime() + "";
        String filename = timePrefix + Constant.TIME_PREFIX + file.getOriginalFilename();
        File sourcefile = new File(dbFilePath + Constant.PATH_SPLIT + filename);

        try {
            FileUtils.copyInputStreamToFile(file.getInputStream(), sourcefile);
        } catch (IOException e) {
            logger.error("uploadDBFiles error.", e);
            throw new RRException(Constant.Msg.VERSION_FILE_ERROR);
        }

        fileEntity.setRealName(filename);
        fileEntity.setName(file.getOriginalFilename());
        fileEntity.setUrl(dbFilePath + Constant.PATH_SPLIT + filename);
        return fileEntity;
    }

    @Override
    public FileEntity uploadOtxMenuFile(MultipartFile file) {
        String menuFilePath = filePath + "OTX" + "_MENU";
        File filedir = new File(menuFilePath);

        //如果路径不存在则创建
        if (!filedir.exists())
            filedir.mkdirs();

        String timePrefix = new Date().getTime() + "";
        String filename = timePrefix + Constant.TIME_PREFIX + file.getOriginalFilename();
        File sourcefile = new File(menuFilePath + Constant.PATH_SPLIT + filename);

        try {
            FileUtils.copyInputStreamToFile(file.getInputStream(), sourcefile);
        } catch (IOException e) {
            logger.error("uploadOtxMenuFile error.", e);
            throw new RRException(Constant.Msg.VERSION_FILE_ERROR);
        }

        FileEntity fileEntity = new FileEntity();
        fileEntity.setRealName(filename);
        fileEntity.setName(file.getOriginalFilename());
        fileEntity.setUrl(menuFilePath + Constant.PATH_SPLIT + filename);
        return fileEntity;
    }
}