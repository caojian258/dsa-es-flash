package org.dsa.modules.reporter.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.dsa.modules.reporter.dao.SystemProtocolItemMapper;
import org.dsa.modules.reporter.entity.SystemProtocolItem;
import org.dsa.modules.reporter.service.SystemProtocolItemIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SystemProtocolItemIServiceImpl extends ServiceImpl<SystemProtocolItemMapper, SystemProtocolItem> implements SystemProtocolItemIService {

    @Autowired
    SystemProtocolItemMapper mapper;

    @Override
    public List<SystemProtocolItem> getPcItems(String pcid, String startDate, String endDate) {
        return mapper.getGroupList(pcid, startDate, endDate);
    }
}
