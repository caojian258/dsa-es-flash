

-- 配置表
--ALTER TABLE sys_config ADD COLUMN "param_group" varchar(50)  NULL;
--ALTER TABLE sys_config ADD COLUMN "language_key" varchar(255)  NULL;
--ALTER TABLE sys_config ADD COLUMN "order_num" int4 default 0  NULL;
--

-- 车辆ecu
CREATE TABLE public.r_vehicle_ecu (
	id bigserial NOT NULL PRIMARY KEY,
	ecu_name varchar(512) unique NOT NULL,
	description_ti varchar(512) NULL,
    short_name_ti varchar(512) NULL,
	status int4 NOT NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now()
);

COMMENT ON TABLE public.r_vehicle_ecu IS 'ECU基本信息表';
COMMENT ON COLUMN public.r_vehicle_ecu.id IS '自增主键';
COMMENT ON COLUMN public.r_vehicle_ecu.ecu_name IS 'ECU名称';
COMMENT ON COLUMN public.r_vehicle_ecu.description_ti IS '描述/备注';
COMMENT ON COLUMN public.r_vehicle_ecu.short_name_ti IS '简称';
COMMENT ON COLUMN public.r_vehicle_ecu.status IS '状态0可用1不可用2开发';


---- 车辆平台
--CREATE TABLE public.r_vehicle_platform (
--	id bigserial NOT NULL PRIMARY KEY,
--	name varchar(512) unique NOT NULL,
--	vehicle_type_id int8 NOT NULL,
--	remark varchar(512) NULL,
--	status int4 NOT NULL,
--	created_at timestamp NOT NULL DEFAULT now(),
--	updated_at timestamp NOT NULL DEFAULT now()
--);
--
---- ecu分组 车辆平台 关联表
--CREATE TABLE public.r_platform_ecu (
--	id bigserial NOT NULL PRIMARY KEY,
--	platform_id int8 NOT NULL,
--	group_id int8 NOT NULL
--);




-- 菜单
--INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query) VALUES(132, 71, 'VehicleEcuList', 'remoteVehicle/ecuList', '', 1, 'menu', 0, NULL);
--INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query) VALUES(133, 71, 'VehiclePlatformList', 'remoteVehicle/platformList', NULL, 1, 'menu', 0, NULL);
--INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query) VALUES(134, 71, 'VehiclePool', 'remoteVehicle/pool', NULL, 1, 'menu', 0, NULL);
--INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query) VALUES(135, 0, 'RemoteDiagnosis', '/', '', 0, 'shezhi', 2, NULL);
--INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query) VALUES(136, 135, 'DiagnosisFunction', '/remoteDiagnosisTasks/funList', '', 1, 'menu', 0, NULL);
--INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query) VALUES(137, 135, 'DiagnosisCondition', '/remoteDiagnosisTasks/conditionList', '', 1, 'zhedie', 1, NULL);
--INSERT INTO public.sys_menu (parent_id, name, url, perms, "type", icon, order_num, query) VALUES(71, 'VehicleTag', 'remoteVehicle/tagList', '', 1, 'config', 0, NULL);


-- 原 远程诊断菜单





--INSERT INTO public.sys_menu ( parent_id, "name", url, perms, "type", icon, order_num, query) VALUES(0, 'Vehicle', 'adiagnostic/', '', 0, 'log', 4, '');
--
--
--INSERT INTO public.sys_menu ( parent_id, name, url, perms, "type", icon, order_num, query) VALUES((select i.menu_id  from sys_menu i where i."name" = 'Vehicle' ), 'VehicleList', 'vlist', '', 1, 'menu', 0, '');
--INSERT INTO public.sys_menu ( parent_id, name, url, perms, "type", icon, order_num, query) VALUES((select i.menu_id from sys_menu i where i."name" = 'Vehicle' ), 'VehicleDID', 'diagnostic/ident', '', 1, 'menu', 2, '');
--INSERT INTO public.sys_menu ( parent_id, name, url, perms, "type", icon, order_num, query) VALUES((select i.menu_id from sys_menu i where i."name" = 'Vehicle' ), 'VehicleVHHS', 'diagnostic/vhhs', '', 1, 'xiangqu', 1, '');
--INSERT INTO public.sys_menu ( parent_id, name, url, perms, "type", icon, order_num, query) VALUES((select i.menu_id from sys_menu i where i."name" = 'Vehicle' ), 'VehicleDTC', 'diagnostic/dtc', '', 1, 'bianji', 3, '');
--
--
--INSERT INTO public.sys_menu (parent_id, "name", url, perms, "type", icon, order_num, query) VALUES(0, 'ECU', 'diagnostic/', '', 0, 'shezhi', 3, '');
--
--
--INSERT INTO public.sys_menu (parent_id, "name", url, perms, "type", icon, order_num, query) VALUES((select i.menu_id from sys_menu i where i."name" = 'ECU' ), 'GW', 'diagnostic/', '', 0, 'config', 1, '');
--INSERT INTO public.sys_menu (parent_id, "name", url, perms, "type", icon, order_num, query) VALUES((select i.menu_id from sys_menu i where i."name" = 'ECU' ), 'IVIBOX', 'diagnostic/', '', 0, 'config', 2, '');
--INSERT INTO public.sys_menu (parent_id, "name", url, perms, "type", icon, order_num, query) VALUES((select i.menu_id from sys_menu i where i."name" = 'ECU' ), 'ADAS', 'diagnostic/', '', 0, 'config', 3, '');
--INSERT INTO public.sys_menu (parent_id, "name", url, perms, "type", icon, order_num, query) VALUES((select i.menu_id from sys_menu i where i."name" = 'ECU' ), 'ADU', 'diagnostic/', '', 0, 'config', 4, '');
--INSERT INTO public.sys_menu (parent_id, "name", url, perms, "type", icon, order_num, query) VALUES((select i.menu_id from sys_menu i where i."name" = 'ECU' ), 'TBOX', 'diagnostic/', '', 0, 'config', 5, '');
--INSERT INTO public.sys_menu (parent_id, "name", url, perms, "type", icon, order_num, query) VALUES((select i.menu_id from sys_menu i where i."name" = 'ECU' ), 'RADAR', 'diagnostic/', '', 0, 'config', 0, '');
--
--
--INSERT INTO public.sys_menu (parent_id, "name", url, perms, "type", icon, order_num, query) VALUES((select i.menu_id from sys_menu i where i."name" = 'GW' ), 'GWDID', 'diagnostic/ident', '', 1, 'sousuo', 2, '{"ecuName":"GW"}');
--INSERT INTO public.sys_menu (parent_id, "name", url, perms, "type", icon, order_num, query) VALUES((select i.menu_id from sys_menu i where i."name" = 'GW' ), 'GWDTC', 'diagnostic/dtc', '', 1, 'bianji', 3, '{"ecuName":"GW"}');
--INSERT INTO public.sys_menu (parent_id, "name", url, perms, "type", icon, order_num, query) VALUES((select i.menu_id from sys_menu i where i."name" = 'IVIBOX' ), 'IVIBOXDID', 'diagnostic/ident', '', 1, 'sousuo', 1, '{"ecuName":"IVIBOX"}');
--INSERT INTO public.sys_menu (parent_id, "name", url, perms, "type", icon, order_num, query) VALUES((select i.menu_id from sys_menu i where i."name" = 'IVIBOX' ), 'IVIBOXDTC', 'diagnostic/dtc', '', 1, 'bianji', 2, '{"ecuName":"IVIBOX"}');
--INSERT INTO public.sys_menu (parent_id, "name", url, perms, "type", icon, order_num, query) VALUES((select i.menu_id from sys_menu i where i."name" = 'ADAS' ), 'ADASDID', 'diagnostic/ident', '', 1, 'sousuo', 1, '{"ecuName":"ADAS"}');
--INSERT INTO public.sys_menu (parent_id, "name", url, perms, "type", icon, order_num, query) VALUES((select i.menu_id from sys_menu i where i."name" = 'ADAS' ), 'ADASDTC', 'diagnostic/dtc', '', 1, 'bianji', 2, '{"ecuName":"ADAS"}');
--INSERT INTO public.sys_menu (parent_id, "name", url, perms, "type", icon, order_num, query) VALUES((select i.menu_id from sys_menu i where i."name" = 'ADU' ), 'ADUDTC', 'diagnostic/dtc', '', 1, 'bianji', 2, '{"ecuName":"ADAU_A"}');
--INSERT INTO public.sys_menu (parent_id, "name", url, perms, "type", icon, order_num, query) VALUES((select i.menu_id from sys_menu i where i."name" = 'ADU' ), 'ADUDID', 'diagnostic/ident', '', 1, 'sousuo', 1, '{"ecuName":"ADAU_A"}');
--INSERT INTO public.sys_menu (parent_id, "name", url, perms, "type", icon, order_num, query) VALUES((select i.menu_id from sys_menu i where i."name" = 'TBOX' ), 'TBOXDTC', 'diagnostic/dtc', '', 1, 'bianji', 2, '{"ecuName":"ADAU_B"}');
--INSERT INTO public.sys_menu (parent_id, "name", url, perms, "type", icon, order_num, query) VALUES((select i.menu_id from sys_menu i where i."name" = 'TBOX' ), 'TBOXDID', 'diagnostic/ident', '', 1, 'sousuo', 1, '{"ecuName":"ADAU_B"}');
--INSERT INTO public.sys_menu (parent_id, "name", url, perms, "type", icon, order_num, query) VALUES((select i.menu_id from sys_menu i where i."name" = 'RADAR' ), 'RADARDID', 'diagnostic/ident', '', 1, 'sousuo', 1, '{"ecuName":"RADAR"}');
--INSERT INTO public.sys_menu (parent_id, "name", url, perms, "type", icon, order_num, query) VALUES((select i.menu_id from sys_menu i where i."name" = 'RADAR' ), 'RADARDTC', 'diagnostic/dtc', '', 1, 'bianji', 2, '{"ecuName":"RADAR"}');



--INSERT INTO public.sys_menu (parent_id, "name", url, perms, "type", icon, order_num, query) VALUES(0, 'Version', '', '', 0, 'zhedie', 2, '');
--
--INSERT INTO public.sys_menu (parent_id, "name", url, perms, "type", icon, order_num, query) VALUES((select i.menu_id from sys_menu i where i."name" = 'Version' ), 'OTX', 'version/otx', '', 1, 'menu', 2, '');
--INSERT INTO public.sys_menu (parent_id, "name", url, perms, "type", icon, order_num, query) VALUES((select i.menu_id from sys_menu i where i."name" = 'Version' ), 'ODX', 'version/odx', 'version:odx,version:odx:select', 1, 'menu', 0, '');
--

-- 车辆池
CREATE TABLE public.r_vehicle_pool (
	id bigserial NOT NULL PRIMARY KEY,
	name varchar(512) unique NOT NULL,
	description_ti varchar(512) NULL,
	status int4 NOT NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now()
);

COMMENT ON TABLE public.r_vehicle_pool IS '车辆池基本信息表';
COMMENT ON COLUMN public.r_vehicle_pool.id IS '自增主键';
COMMENT ON COLUMN public.r_vehicle_pool."name" IS '名称';
COMMENT ON COLUMN public.r_vehicle_pool.description_ti IS '描述/备注';
COMMENT ON COLUMN public.r_vehicle_pool.status IS '状态0可用1不可用2开发';

-- 诊断功能
CREATE TABLE public.r_diag_fun (
	id bigserial NOT NULL PRIMARY KEY,
	fun_name varchar(100) unique NOT NULL,
	fun_val varchar(100) NOT null,
	fun_group varchar(100) null,
	order_num int4 null default 0,
	description_ti varchar(256) NULL,
	status int4 NOT NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now()
);

COMMENT ON TABLE public.r_diag_fun IS '诊断功能基本信息表';
COMMENT ON COLUMN public.r_diag_fun.id IS '自增主键';
COMMENT ON COLUMN public.r_diag_fun.fun_name IS '功能名';
COMMENT ON COLUMN public.r_diag_fun.fun_val IS '功能值/code与客户端约定';
COMMENT ON COLUMN public.r_diag_fun.fun_group IS '功能分组；预留字段';
COMMENT ON COLUMN public.r_diag_fun.order_num IS '排序，预留字段';
COMMENT ON COLUMN public.r_diag_fun.description_ti IS '描述/备注';
COMMENT ON COLUMN public.r_diag_fun.status IS '状态0可用1不可用2开发';


-- 诊断条件
CREATE TABLE public.r_diag_condition (
	id bigserial NOT NULL PRIMARY KEY,
	condition_name varchar(100) unique NOT NULL,
	condition_code varchar(100) NULL,
	condition_val int4 NOT null,
	unity varchar(50) null,
	min_val varchar(100) null,
	max_val varchar(100) null,
	eq_val  varchar(100) null,
	in_val  text null,
	description_ti varchar(256) NULL,
	status int4 NOT NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now()
);

COMMENT ON TABLE public.r_diag_condition IS '诊断条件基本信息表';
COMMENT ON COLUMN public.r_diag_condition.id IS '自增主键';
COMMENT ON COLUMN public.r_diag_condition.condition_name IS '条件名';
COMMENT ON COLUMN public.r_diag_condition.condition_code IS 'code与客户端约定';
COMMENT ON COLUMN public.r_diag_condition.condition_val IS '判断类型(1:''n > min'',2:''n >= min'',3:''n < max'',4:''n <= max'',5:''min < n < max'',6:''min <= n <= max'',  7: ''min < n <= max'' , 8: ''min <= n < max'',9:''n  = val'',10:''n != val'',11:''in'',12:''not in'')';
COMMENT ON COLUMN public.r_diag_condition.unity IS '单位';
COMMENT ON COLUMN public.r_diag_condition.min_val IS '最小值';
COMMENT ON COLUMN public.r_diag_condition.max_val IS '最大值';
COMMENT ON COLUMN public.r_diag_condition.eq_val IS '等于';
COMMENT ON COLUMN public.r_diag_condition.in_val IS 'in';
COMMENT ON COLUMN public.r_diag_condition.description_ti IS '描述/备注';
COMMENT ON COLUMN public.r_diag_condition.status IS '状态0可用1不可用2开发';


-- 车辆tag
CREATE TABLE public.r_tag (
	id bigserial NOT NULL PRIMARY KEY,
	tag_name varchar(512) unique NOT NULL,
	description_ti varchar(512) NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now()
);

COMMENT ON TABLE public.r_tag IS '车辆标签基本信息表';
COMMENT ON COLUMN public.r_tag.id IS '自增主键';
COMMENT ON COLUMN public.r_tag.tag_name IS '标签名';
COMMENT ON COLUMN public.r_tag.description_ti IS '描述/备注';


-- 车辆 tag 关联表
CREATE TABLE public.r_vehicle_tag (
	id bigserial NOT NULL,
	vin varchar(256) NOT NULL,
	tag_id int8 NOT NULL,
	CONSTRAINT r_vehicle_tag_un UNIQUE (vin, tag_id)
);
COMMENT ON TABLE public.r_vehicle_tag IS '车辆信息关联标签';
COMMENT ON COLUMN public.r_vehicle_tag.id IS '自增主键';
COMMENT ON COLUMN public.r_vehicle_tag.vin IS 'vin';
COMMENT ON COLUMN public.r_vehicle_tag.tag_id IS '车辆标签主键';

-- 车辆池 vin码关联表
--CREATE TABLE public.r_pool_vin (
--	id bigserial NOT NULL,
--	p_id int8 NOT NULL,
--	vin varchar(255) NOT NULL,
--	created_at timestamp NOT NULL DEFAULT now(),
--	CONSTRAINT r_pool_vin_un UNIQUE (p_id, vin)
--);



-- 缓存表，触发器清除，30分钟，且设置唯一索引，去重
--CREATE TABLE public.r_cache (
--	c_key varchar(255) not null ,
--	c_val varchar(255) not null ,
--	created_at timestamp NOT NULL DEFAULT now(),
--	CONSTRAINT r_cache_un UNIQUE (c_key, c_val)
--);
--
--CREATE FUNCTION r_cache_delete_old_rows() RETURNS trigger
--    LANGUAGE plpgsql
--    AS $$
--BEGIN
--    DELETE FROM r_cache WHERE created_at  < NOW() - INTERVAL '30 minute';
--    RETURN NEW;
--END;
--$$;
--
--CREATE TRIGGER r_cache_delete_old_rows_trigger
--    AFTER INSERT ON r_cache
--    EXECUTE PROCEDURE r_cache_delete_old_rows();



-- ON conflict(v_id,t_id)  DO NOTHING
--	CONSTRAINT r_vehicle_tag_un UNIQUE (v_id, t_id)



-- 诊断文件管理
CREATE TABLE public.r_diag_file (
	id bigserial NOT NULL PRIMARY KEY,
	function_name varchar(256) unique NOT NULL,
	file_name varchar(256) NOT NULL,
	file_path varchar(512) NOT NULL,
	file_md5 varchar(32) NOT NULL,
	file_length int8 NULL,
	description_ti varchar(256) NULL,
	status int4 NOT NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now()
);

COMMENT ON TABLE public.r_diag_file IS '诊断文件基本信息表';
COMMENT ON COLUMN public.r_diag_file.id IS '自增主键';
COMMENT ON COLUMN public.r_diag_file.function_name IS '功能名';
COMMENT ON COLUMN public.r_diag_file.file_name IS '文件名称;上传文件的名称';
COMMENT ON COLUMN public.r_diag_file.file_path IS '文件路径';
COMMENT ON COLUMN public.r_diag_file.file_md5 IS 'md5标识码';
COMMENT ON COLUMN public.r_diag_file.file_length IS '文件长度/大小';
COMMENT ON COLUMN public.r_diag_file.description_ti IS '描述/备注';
COMMENT ON COLUMN public.r_diag_file.status IS '状态0可用1不可用2开发';


-- 预诊断任务定义
create table public.r_diag_task (
	id bigserial not null primary key,
	task_name varchar(256) not null,
	description_ti varchar(256) null,
	status int4 not null,
	created_at timestamp not null default now(),
	updated_at timestamp not null default now()
);

COMMENT ON TABLE public.r_diag_task IS '诊断任务定义基本信息表';
COMMENT ON COLUMN public.r_diag_task.id IS '自增主键';
COMMENT ON COLUMN public.r_diag_task.task_name IS '名称';
COMMENT ON COLUMN public.r_diag_task.description_ti IS '描述/备注';
COMMENT ON COLUMN public.r_diag_task.status IS '状态0可用1不可用2开发';


-- 预诊断任务发布
create table public.r_diag_task_campaign (
	id bigserial not null primary key,
	campaign_name varchar(256) not null,
	pool_id int8 null,
	task_id int8 not null,
	start_time timestamp not null ,
	end_time timestamp not null,
	model varchar(20) not null,
    failed_attempts_num int4 null,
    failed_num int4 null,
	execution_cycle int4 null,
	execution_interval int8 null,
	priority varchar(20) null,
	gradient json null,
--	gradient_condition int4 null,
--	gradient_value int4 null,
	description_ti varchar(256) null,
	status int4 not null,
	created_at timestamp not null default now(),
	updated_at timestamp not null default now()
);

COMMENT ON TABLE public.r_diag_task_campaign IS '诊断任务发布基本信息表';
COMMENT ON COLUMN public.r_diag_task_campaign.id IS '自增主键';
COMMENT ON COLUMN public.r_diag_task_campaign.campaign_name IS '名称';
COMMENT ON COLUMN public.r_diag_task_campaign.pool_id IS '车辆池ID';
COMMENT ON COLUMN public.r_diag_task_campaign.task_id IS '诊断任务定义ID';
COMMENT ON COLUMN public.r_diag_task_campaign.start_time IS '有效期:开始时间';
COMMENT ON COLUMN public.r_diag_task_campaign.end_time IS '有效期:结束时间';
COMMENT ON COLUMN public.r_diag_task_campaign.model IS '模式(静默silent、常规routine、强制force';
COMMENT ON COLUMN public.r_diag_task_campaign.failed_attempts_num IS '失败尝试次数';
COMMENT ON COLUMN public.r_diag_task_campaign.failed_num IS '失败次数；预留';
COMMENT ON COLUMN public.r_diag_task_campaign.execution_cycle IS '执行周期';
COMMENT ON COLUMN public.r_diag_task_campaign.execution_interval IS '执行间隔';
COMMENT ON COLUMN public.r_diag_task_campaign.priority IS '优先级(高high、中middle、低low';
COMMENT ON COLUMN public.r_diag_task_campaign.gradient IS '梯度(gradient:阈值,gradientCondition:梯度(条件0%1<,gradientValue:参数,flag:0正1反';
COMMENT ON COLUMN public.r_diag_task_campaign.description_ti IS '描述/备注';
COMMENT ON COLUMN public.r_diag_task_campaign.status IS '0开始/1停止';


-- 诊断任务-条件
CREATE TABLE public.r_task_condition (
	id bigserial NOT NULL PRIMARY KEY,
	raw_id int8 not null,
	task_id int8 not null,
	condition_name varchar(100) NOT NULL,
	condition_code varchar(100) NULL,
	condition_val int4 NOT null,
	unity varchar(50) null,
	min_val varchar(100) null,
	max_val varchar(100) null,
	eq_val  varchar(100) null,
	in_val  text null,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now()
);
COMMENT ON COLUMN public.r_task_condition.id IS '自增主键';
COMMENT ON COLUMN public.r_task_condition.raw_id IS '原诊断条件ID';
COMMENT ON COLUMN public.r_task_condition.task_id IS '任务定义ID';
COMMENT ON COLUMN public.r_task_condition.condition_name IS '条件名';
COMMENT ON COLUMN public.r_task_condition.condition_code IS 'code与客户端约定';
COMMENT ON TABLE public.r_task_condition IS '诊断任务关联诊断条件';
COMMENT ON COLUMN public.r_task_condition.condition_val IS '诊断条件';
COMMENT ON COLUMN public.r_task_condition.unity IS '单位';
COMMENT ON COLUMN public.r_task_condition.min_val IS '最小值';
COMMENT ON COLUMN public.r_task_condition.max_val IS '最大值';
COMMENT ON COLUMN public.r_task_condition.eq_val IS '等于';
COMMENT ON COLUMN public.r_task_condition.in_val IS 'in';


-- 诊断任务-ecu 功能 关联表
CREATE TABLE public.r_task_ecu (
	id bigserial NOT NULL PRIMARY KEY,
	task_id int8 not null,
	fun_id int8  null,
	ecu_group_id int8  null,
	ecu_id int8  null,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now()
);

COMMENT ON TABLE public.r_task_ecu IS '诊断任务定义-关联诊断功能|ECU|';
COMMENT ON COLUMN public.r_task_ecu.id IS '自增主键';
COMMENT ON COLUMN public.r_task_ecu.task_id IS '任务定义ID';
COMMENT ON COLUMN public.r_task_ecu.fun_id IS '诊断功能主键';
COMMENT ON COLUMN public.r_task_ecu.ecu_group_id IS 'ECU分组主键';
COMMENT ON COLUMN public.r_task_ecu.ecu_id IS 'ECU主键';

-- 预诊断任务定义
create table public.r_task_file (
	id bigserial not null primary key,
	task_id int8 not null,
	file_id int8 not null
);

COMMENT ON TABLE public.r_task_file IS '诊断任务关联诊断文件';
COMMENT ON COLUMN public.r_task_file.id IS '自增主键';
COMMENT ON COLUMN public.r_task_file.task_id IS '诊断任务定义主键';
COMMENT ON COLUMN public.r_task_file.file_id IS '诊断文件主键';


-- ecu分组
CREATE TABLE public.r_vehicle_ecu_group (
	id bigserial NOT NULL PRIMARY KEY,
	group_name varchar(256) unique NOT NULL,
	description_ti varchar(256) NULL,
	short_name_ti varchar(512) NULL,
	status int4 NOT NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now()
);

COMMENT ON TABLE public.r_vehicle_ecu_group IS 'ECU分组基本信息表';
COMMENT ON COLUMN public.r_vehicle_ecu_group.id IS '自增主键';
COMMENT ON COLUMN public.r_vehicle_ecu_group.group_name IS 'ECU分组名';
COMMENT ON COLUMN public.r_vehicle_ecu_group.description_ti IS '描述/备注';
COMMENT ON COLUMN public.r_vehicle_ecu_group.short_name_ti IS '简称';
COMMENT ON COLUMN public.r_vehicle_ecu_group.status IS '状态0可用1不可用2开发';



-- ecu_group 关联表
CREATE TABLE public.r_ecu_group (
	id bigserial NOT NULL PRIMARY KEY,
	ecu_id int8 NOT NULL,
	group_id int8 NOT NULL,
	CONSTRAINT r_ecu_group_un UNIQUE (ecu_id, group_id)
);

COMMENT ON TABLE public.r_ecu_group IS 'ECU与分组关联表';
COMMENT ON COLUMN public.r_ecu_group.id IS '自增主键';
COMMENT ON COLUMN public.r_ecu_group.ecu_id IS 'ECU主键';
COMMENT ON COLUMN public.r_ecu_group.group_id IS 'ECU分组主键';

-- ECU 识别信息关联表
create table public.r_ecu_identification_info (
	id bigserial not null,
	did varchar(255) not null,
	did_val varchar(255) null,
	identify_group varchar(255) null,
	did_name text null,
	description_ti text null,
	status int4 null default 1,
    ecu_id int8 null,
    group_id int8 null,
    ecu_version_id int8 null,
    version_id int8 null,
	created_at timestamp not null default now(),
	updated_at timestamp not null default now()
);
ALTER TABLE public.r_ecu_identification_info ADD CONSTRAINT r_id_identify_ecu_un UNIQUE (ecu_id, did);
--ALTER TABLE public.r_ecu_identification_info ADD CONSTRAINT r_id_identify_un UNIQUE (ecu_id, group_id, ecu_version_id,version_id, did)
COMMENT ON TABLE public.r_ecu_identification_info IS '识别信息表';
COMMENT ON COLUMN public.r_ecu_identification_info.id IS '自增主键';
COMMENT ON COLUMN public.r_ecu_identification_info.ecu_id IS 'ECU主键';
COMMENT ON COLUMN public.r_ecu_identification_info.group_id IS 'ECU分组主键';
COMMENT ON COLUMN public.r_ecu_identification_info.ecu_version_id IS 'ECU版本主键';
COMMENT ON COLUMN public.r_ecu_identification_info.version_id IS '车辆版本主键';
COMMENT ON COLUMN public.r_ecu_identification_info.did IS '标识';
COMMENT ON COLUMN public.r_ecu_identification_info.did_val IS '值';
COMMENT ON COLUMN public.r_ecu_identification_info.identify_group IS '分组;预期识别expect目标识别target';
COMMENT ON COLUMN public.r_ecu_identification_info.did_name IS '简称';
COMMENT ON COLUMN public.r_ecu_identification_info.description_ti IS '描述';
COMMENT ON COLUMN public.r_ecu_identification_info.status IS '1非强制(默认值);0强制';


-- ecu版本
CREATE TABLE public.r_ecu_version (
	id bigserial NOT NULL PRIMARY KEY,
	version_number int8 not null default 0,
	version_name varchar(256) NOT NULL,
	version_type varchar(20) NOT NULL,
    is_cross int4 NOT NULL default 0,
	description_ti varchar(512) NULL,
	release_node_ti varchar(512) NULL,
	status int4 NOT NULL,
    ecu_id int8 NOT NULL ,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now()
);

COMMENT ON TABLE public.r_ecu_version IS 'ECU版本基本信息表';
COMMENT ON COLUMN public.r_ecu_version.ecu_id IS '关联的ECU主键';
COMMENT ON COLUMN public.r_ecu_version.id IS '自增主键';
COMMENT ON COLUMN public.r_ecu_version.version_number IS '编号，用于排序';
COMMENT ON COLUMN public.r_ecu_version.version_name IS '版本名/版本号';
COMMENT ON COLUMN public.r_ecu_version.version_type IS '版本类型;可选optional;强制mandatory';
COMMENT ON COLUMN public.r_ecu_version.description_ti IS '描述/备注';
COMMENT ON COLUMN public.r_ecu_version.release_node_ti IS '发布说明';
COMMENT ON COLUMN public.r_ecu_version.is_cross IS '版本是否可跳过0可跳过；1不可跳过';
COMMENT ON COLUMN public.r_ecu_version.status IS '状态;0开发1测试2关闭3生效';

-- ecu ecu版本 关联
--CREATE TABLE public.r_ecu_ecu_version (
--	id bigserial NOT NULL PRIMARY KEY,
--	type varchar(256) NULL,
--	ecu_id int8 not null,
--	init_id int8 not null,
--	created_at timestamp NOT NULL DEFAULT now(),
--	updated_at timestamp NOT NULL DEFAULT now()
--);
--
--COMMENT ON TABLE public.r_ecu_ecu_version IS 'ECU与ECU版本关联表';
--COMMENT ON COLUMN public.r_ecu_ecu_version.id IS '自增主键';
--COMMENT ON COLUMN public.r_ecu_ecu_version."type" IS '类型；暂时预留';
--COMMENT ON COLUMN public.r_ecu_ecu_version.ecu_id IS 'ECU主键';
--COMMENT ON COLUMN public.r_ecu_ecu_version.init_id IS 'ECU版本init_id';


-- 车型 ecu分组关联
CREATE TABLE public.r_vehicle_type_ecu (
	id bigserial NOT NULL PRIMARY KEY,
	type_id int8 NOT NULL,
	ecu_group_id int8 NOT NULL,
    ecu_id int8 NULL
);

COMMENT ON TABLE public.r_vehicle_type_ecu IS '车型关联ECU分组表';
COMMENT ON COLUMN public.r_vehicle_type_ecu.id IS '自增主键';
COMMENT ON COLUMN public.r_vehicle_type_ecu.type_id IS '车型主键';
COMMENT ON COLUMN public.r_vehicle_type_ecu.ecu_group_id IS 'ECU分组主键';
COMMENT ON COLUMN public.r_vehicle_type_ecu.ecu_id IS 'ECU分组主键';

ALTER TABLE public.r_vehicle_type_ecu ADD CONSTRAINT r_vehicle_type_ecu_un UNIQUE (type_id,ecu_group_id,ecu_id);


--  ecu版本 文件关联
CREATE TABLE public.r_ecu_version_file (
	id bigserial NOT NULL PRIMARY KEY,
	version_id int8 not NULL,
	file_type varchar(256) NULL,
	flash_type varchar(256) NULL,
	file_name varchar(256) NULL,
	file_md5 varchar(256) NULL,
	file_path text NULL,
	status int4 NOT NULL default 0,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now()
);

COMMENT ON TABLE public.r_ecu_version_file IS 'ECU版本文件';
COMMENT ON COLUMN public.r_ecu_version_file.id IS '自增主键';
COMMENT ON COLUMN public.r_ecu_version_file.version_id IS 'ECU版本主键';
COMMENT ON COLUMN public.r_ecu_version_file.file_type IS '文件类型(发布说明releaseNote、刷写flashFile、测试文档testDocumentation';
COMMENT ON COLUMN public.r_ecu_version_file.flash_type IS '刷写类型，动态下拉框对应sys_config表分组为Version_FlashType的param_value';
COMMENT ON COLUMN public.r_ecu_version_file.file_name IS '文件名';
COMMENT ON COLUMN public.r_ecu_version_file.file_md5 IS 'md5标识码';
COMMENT ON COLUMN public.r_ecu_version_file.file_path IS '文件路径';
COMMENT ON COLUMN public.r_ecu_version_file.status IS '刷写文件状态，仅对刷写文件生效，0增量1全量';


--  ecu version 依赖关系
--CREATE TABLE public.r_version_ecu_rely (
--	id bigserial NOT NULL PRIMARY KEY,
--	version_id int8 not NULL,
--	ecu_id int8  NULL,
--	v_id int8  NULL
--);
--COMMENT ON TABLE public.r_version_ecu_rely IS 'ECU版本依赖表';
--COMMENT ON COLUMN public.r_version_ecu_rely.id IS 'id';
--COMMENT ON COLUMN public.r_version_ecu_rely.version_id IS 'ECU版本主键';
--COMMENT ON COLUMN public.r_version_ecu_rely.ecu_id IS 'ECU主键';
--COMMENT ON COLUMN public.r_version_ecu_rely.v_id IS '关联的ECU的版本的主键';
CREATE TABLE public.r_version_ecu_rely (
	id bigserial NOT NULL PRIMARY KEY,
    vehicle_version_id int8 not  NULL,
    name varchar not  NULL,
	group_id varchar(32) not NULL,
    ecu_group_id int8 not  NULL,
	ecu_id int8 not NULL,
	ecu_version_id int8 not null,
	priority int4 null,
	CONSTRAINT r_ecu_version_unique UNIQUE (vehicle_version_id,ecu_version_id)
);
COMMENT ON TABLE public.r_version_ecu_rely IS 'ECU版本依赖表';
COMMENT ON COLUMN public.r_version_ecu_rely.id IS 'id';
COMMENT ON COLUMN public.r_version_ecu_rely.name IS '名称';
COMMENT ON COLUMN public.r_version_ecu_rely.group_id IS '分组id';
COMMENT ON COLUMN public.r_version_ecu_rely.vehicle_version_id IS '车辆版本主键';
COMMENT ON COLUMN public.r_version_ecu_rely.ecu_group_id IS 'ECU分组主键';
COMMENT ON COLUMN public.r_version_ecu_rely.ecu_version_id IS 'ECU版本主键';
COMMENT ON COLUMN public.r_version_ecu_rely.ecu_id IS 'ECU主键';
COMMENT ON COLUMN public.r_version_ecu_rely.priority IS '优先级';


-- 车辆版本
CREATE TABLE public.r_vehicle_version (
	id bigserial NOT NULL PRIMARY KEY,
	type_id int8 NOT NULL,
	pool_id int8 NULL,
	version_number int8 not null default 0,
	version_name varchar(256) NOT NULL,
	version_type varchar(20) NOT NULL,
    is_cross int4 NOT NULL default 0,
	description_ti varchar(512) NULL,
	release_node_ti varchar(512) NULL,
	status int4 NOT NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now()
);

COMMENT ON TABLE public.r_vehicle_version IS '车辆版本基本信息表';
COMMENT ON COLUMN public.r_vehicle_version.id IS '自增主键';
COMMENT ON COLUMN public.r_vehicle_version.type_id IS '车型主键';
COMMENT ON COLUMN public.r_vehicle_version.pool_id IS '车辆池主键';
COMMENT ON COLUMN public.r_vehicle_version.version_number IS '父级ID';
COMMENT ON COLUMN public.r_vehicle_version.version_name IS '版本名/版本号';
COMMENT ON COLUMN public.r_vehicle_version.version_type IS '版本类型；可选optional;强制mandatory';
COMMENT ON COLUMN public.r_vehicle_version.description_ti IS '描述/备注';
COMMENT ON COLUMN public.r_vehicle_version.release_node_ti IS '发布说明';
COMMENT ON COLUMN public.r_vehicle_version.is_cross IS '版本是否可跳过0可跳过；1不可跳过';
COMMENT ON COLUMN public.r_vehicle_version.status IS '状态;0开发1测试2关闭3生效';



--   ecu group vehicle version 依赖关系
CREATE TABLE public.r_vehicle_version_ecu_group (
	id bigserial NOT NULL PRIMARY KEY,
	version_id int8 not NULL,
	ecu_id int8  NULL,
	group_id int8  NULL,
	ecu_version_id int8  NULL
);
COMMENT ON TABLE public.r_vehicle_version_ecu_group IS '车辆版本关联ECU分组|ECU|ECU版本';
COMMENT ON COLUMN public.r_vehicle_version_ecu_group.id IS '自增主键';
COMMENT ON COLUMN public.r_vehicle_version_ecu_group.version_id IS '车辆版本主键';
COMMENT ON COLUMN public.r_vehicle_version_ecu_group.ecu_id IS 'ECU主键';
COMMENT ON COLUMN public.r_vehicle_version_ecu_group.group_id IS 'ECU分组主键';
COMMENT ON COLUMN public.r_vehicle_version_ecu_group.ecu_version_id IS 'ECU版本主键';


-- 诊断任务会话概览
CREATE TABLE public.r_diag_task_overview (
	id bigserial NOT NULL PRIMARY KEY,
	campaign_id int8 unique NOT NULL,
	match_num int8 default 0 null,
	download_num int8 default 0 null,
	cancel_num int8 default 0 null,
	reject_num int8 default 0 null,
	success_num int8 default 0 NULL,
	acceptance_num int8 default 0 NULL,
	fail_num int8 default 0 NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now()
);

COMMENT ON TABLE public.r_diag_task_overview IS '诊断任务详情表-统计表';
COMMENT ON COLUMN public.r_diag_task_overview.id IS '自增主键';
COMMENT ON COLUMN public.r_diag_task_overview.campaign_id IS '任务发布ID';
COMMENT ON COLUMN public.r_diag_task_overview.match_num IS '匹配次数';
COMMENT ON COLUMN public.r_diag_task_overview.download_num IS '下载次数';
COMMENT ON COLUMN public.r_diag_task_overview.cancel_num IS '取消次数';
COMMENT ON COLUMN public.r_diag_task_overview.reject_num IS '拒绝次数';
COMMENT ON COLUMN public.r_diag_task_overview.success_num IS '成功次数';
COMMENT ON COLUMN public.r_diag_task_overview.fail_num IS '失败次数';
COMMENT ON COLUMN public.r_diag_task_overview.acceptance_num IS '接受次数';
-- 唯一约束
ALTER TABLE public.r_diag_task_overview ADD CONSTRAINT r_diag_task_overview_task_id_key UNIQUE (campaign_id);


-- 诊断任务会话记录
CREATE TABLE public.r_diag_task_record (
	id bigserial NOT NULL PRIMARY KEY,
	campaign_id int8  NOT NULL,
	session_id  varchar(64) unique not null,
	vin  varchar(64) not null,
	executions int4 null default 0,
	match_status int4 not null default 1,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now()
);

COMMENT ON TABLE public.r_diag_task_record IS '诊断任务记录-记录表';
COMMENT ON COLUMN public.r_diag_task_record.id IS '自增主键';
COMMENT ON COLUMN public.r_diag_task_record.campaign_id IS '任务发布ID';
COMMENT ON COLUMN public.r_diag_task_record.session_id IS '会话id';
COMMENT ON COLUMN public.r_diag_task_record.vin IS 'vin';
COMMENT ON COLUMN public.r_diag_task_record.match_status IS '匹配状态1匹配中 0匹配成功';
COMMENT ON COLUMN public.r_diag_task_record.executions IS '执行次数';
-- 唯一约束
--ALTER TABLE public.r_diag_task_record ADD CONSTRAINT r_diag_task_record_un UNIQUE (session_id, vin);


-- 诊断任务会话详情
CREATE TABLE public.r_diag_task_detail (
	id bigserial NOT NULL PRIMARY KEY,
	campaign_id int8  NOT NULL,
	type varchar(64) not null,
	session_id varchar(64) not null,
	vin  varchar(64) not null,
	start_time timestamp  NULL,
	end_time timestamp  NULL,
	action int4 null,
	code varchar(64)  null,
	message varchar(256) null,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now()
);

COMMENT ON COLUMN public.r_diag_task_detail.id IS '自增主键';
COMMENT ON TABLE public.r_diag_task_detail IS '诊断任务记录-详情记录';
COMMENT ON COLUMN public.r_diag_task_detail.campaign_id IS '任务发布ID';
COMMENT ON COLUMN public.r_diag_task_detail."type" IS '操作类型，MATCH匹配、 ConfirmExecution:EXECUTE执行、DOWNLOAD下载、ConfirmExecution:CANCEL取消、ConfirmExecution:REJECT拒绝、TERMINATION终止';
COMMENT ON COLUMN public.r_diag_task_detail.session_id IS '会话id';
COMMENT ON COLUMN public.r_diag_task_detail.vin IS 'vin';
COMMENT ON COLUMN public.r_diag_task_detail.start_time IS '开始时间';
COMMENT ON COLUMN public.r_diag_task_detail.end_time IS '结束时间';
COMMENT ON COLUMN public.r_diag_task_detail.action IS '客户端同步状态时的类型(1:匹配2:下载3:远程校验4.确认执行5.条件校验6:上传日志)';
COMMENT ON COLUMN public.r_diag_task_detail.code IS '状态码0成功1失败2取消';
COMMENT ON COLUMN public.r_diag_task_detail.message IS '信息';


-- 诊断任务脚本下载记录
--CREATE TABLE public.r_diag_task_file_detail (
--	id bigserial NOT NULL PRIMARY KEY,
--	session_id varchar(128) not null,
--	vin  varchar(64) not null,
--	code int4 null,
--	message varchar(256) null,
--	created_at timestamp NOT NULL DEFAULT now(),
--	updated_at timestamp NOT NULL DEFAULT now()
--);

-- 诊断任务会话日志文件
CREATE TABLE public.r_diag_task_log (
	id bigserial NOT NULL PRIMARY KEY,
	campaign_id int8 NOT NULL,
	session_id varchar(64) not null,
	vin  varchar(64) not null,
	md5  varchar(64)  null,
	file_name varchar(512)  NULL,
	file_path varchar(512)  NULL,
	file_length  int8  null,
	status  int4 not null default 1,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now()
);
COMMENT ON TABLE public.r_diag_task_log IS '诊断任务记录-日志文件记录';
COMMENT ON COLUMN public.r_diag_task_log.id IS '自增主键';
COMMENT ON COLUMN public.r_diag_task_log.campaign_id IS '任务发布ID';
COMMENT ON COLUMN public.r_diag_task_log.session_id IS '会话id';
COMMENT ON COLUMN public.r_diag_task_log.vin IS 'vin';
COMMENT ON COLUMN public.r_diag_task_log.md5 IS 'MD5标识码';
COMMENT ON COLUMN public.r_diag_task_log.file_name IS '文件名';
COMMENT ON COLUMN public.r_diag_task_log.file_path IS '文件路径';
COMMENT ON COLUMN public.r_diag_task_log.file_length IS '文件大小';
COMMENT ON COLUMN public.r_diag_task_log.status IS '状态，预留字段';


-- 诊断任务会话 - 执行中记录
CREATE TABLE public.r_diag_task_active (
	id bigserial NOT NULL PRIMARY KEY,
	campaign_id int8  NOT NULL,
	session_id  varchar(64) unique not null,
	vin  varchar(64) not null,
	status int4 not null default 0,
	mes varchar(256) null,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now()
);

COMMENT ON TABLE public.r_diag_task_active IS '诊断任务-活动任务表';
COMMENT ON COLUMN public.r_diag_task_active.id IS '自增主键';
COMMENT ON COLUMN public.r_diag_task_active.campaign_id IS '任务发布主键';
COMMENT ON COLUMN public.r_diag_task_active.session_id IS '会话id';
COMMENT ON COLUMN public.r_diag_task_active.vin IS 'vin';
COMMENT ON COLUMN public.r_diag_task_active.status IS '0执行中(1完成(2失败或其它状态';
COMMENT ON COLUMN public.r_diag_task_active.mes IS '信息';


--  ecu分组-ecu image大头照
CREATE TABLE public.r_vehicle_ecu_image (
	id bigserial NOT NULL PRIMARY KEY,
	file_name varchar(256) NULL,
	file_md5 varchar(256) NULL,
	file_path text NULL,
	ecu_group_id int8  NULL,
    ecu_id int8  NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now()
);

COMMENT ON TABLE public.r_vehicle_ecu_image IS 'ECU/分组图片关联表';
COMMENT ON COLUMN public.r_vehicle_ecu_image.id IS '自增主键';
COMMENT ON COLUMN public.r_vehicle_ecu_image.ecu_group_id IS 'ECU分组ID';
COMMENT ON COLUMN public.r_vehicle_ecu_image.ecu_id IS 'ECUID';
COMMENT ON COLUMN public.r_vehicle_ecu_image.file_name IS '文件名';
COMMENT ON COLUMN public.r_vehicle_ecu_image.file_md5 IS 'md5标识码';
COMMENT ON COLUMN public.r_vehicle_ecu_image.file_path IS '文件路径';


--   车辆池关联车辆标签
--CREATE TABLE public.r_pool_vin_tag (
--	id bigserial NOT NULL PRIMARY KEY,
--	pool_id int8 not NULL,
--	tag_id int8  NULL
--);
--
--COMMENT ON TABLE public.r_pool_vin_tag IS '车辆池关联车辆标签';
--COMMENT ON COLUMN public.r_pool_vin_tag.id IS '自增主键';
--COMMENT ON COLUMN public.r_pool_vin_tag.pool_id IS '车辆池主键';
--COMMENT ON COLUMN public.r_pool_vin_tag.tag_id IS '车辆标签主键';


--   车辆池关联车型、车辆版本、ECU分组
CREATE TABLE public.r_pool_depend (
	id bigserial NOT NULL PRIMARY KEY,
	pool_id int8 not NULL,
	vehicle_type_id int8  NULL,
	vehicle_version_id int8  NULL,
	ecu_version_id int8  NULL,
	tag_id int8  NULL
);

COMMENT ON TABLE public.r_pool_depend IS '车辆池关联表-车型|车辆版本|ECU分组';
COMMENT ON COLUMN public.r_pool_depend.id IS '自增主键';
COMMENT ON COLUMN public.r_pool_depend.pool_id IS '车辆池主键';
COMMENT ON COLUMN public.r_pool_depend.vehicle_type_id IS '车型主键';
COMMENT ON COLUMN public.r_pool_depend.vehicle_version_id IS '车辆版本主键';
COMMENT ON COLUMN public.r_pool_depend.ecu_version_id IS 'ECU版本主键';
COMMENT ON COLUMN public.r_pool_depend.tag_id IS '车辆标签主键';

--   车辆池关联表-生产车间|生产日期|销售区域
CREATE TABLE public.r_pool_vehicle (
	id bigserial NOT NULL PRIMARY KEY,
	pool_id int8 not NULL,
	config_id int8 not NULL,
	param_group varchar(512) not NULL
);

COMMENT ON TABLE public.r_pool_vehicle IS '车辆池关联表-生产车间|生产日期|销售区域';
COMMENT ON COLUMN public.r_pool_vehicle.id IS '自增主键';
COMMENT ON COLUMN public.r_pool_vehicle.pool_id IS '车辆池主键';
COMMENT ON COLUMN public.r_pool_vehicle.config_id IS '配置表主键';
COMMENT ON COLUMN public.r_pool_vehicle.param_group IS '配置表分组(V_SalesAreas销售区域V_PlaceOfProduction生产车间V_ProductionDate生产日期';


CREATE TABLE public.r_version_record (
	id bigserial NOT NULL,
	value_id int8 NULL,
	type varchar NOT NULL,
	status int4 NOT NULL,
	edit_type varchar NOT NULL,
	created_at timestamp NOT NULL DEFAULT now(),
    create_user_name varchar NOT NULL,
	create_by int8 NOT NULL
);

COMMENT ON TABLE public.r_version_record IS '版本操作记录表';
COMMENT ON COLUMN public.r_version_record.id IS '自增主键';
COMMENT ON COLUMN public.r_version_record.type IS '类型';
COMMENT ON COLUMN public.r_version_record.value_id IS '主键';
COMMENT ON COLUMN public.r_version_record.status IS '状态;0开发1测试2关闭3生效(释放)';
COMMENT ON COLUMN public.r_version_record.edit_type IS '操作类型';
COMMENT ON COLUMN public.r_version_record.create_user_name IS '创建用户';
COMMENT ON COLUMN public.r_version_record.create_by IS '创建用户ID';




CREATE INDEX r_diag_task_record_campaign_id_idx ON public.r_diag_task_record (campaign_id,vin);
CREATE INDEX r_diag_task_record_session_id_idx ON public.r_diag_task_record (session_id,vin);


CREATE INDEX r_diag_task_detail_vin_idx ON public.r_diag_task_detail (vin);


CREATE INDEX r_ecu_identification_info_ecu_id_idx ON public.r_ecu_identification_info (ecu_id);
CREATE INDEX r_ecu_identification_info_group_id_idx ON public.r_ecu_identification_info (group_id);
CREATE INDEX r_ecu_identification_info_version_id_idx ON public.r_ecu_identification_info (version_id);
CREATE INDEX r_ecu_identification_info_ecu_version_id_idx ON public.r_ecu_identification_info (ecu_version_id);

CREATE INDEX r_ecu_version_file_version_id_idx ON public.r_ecu_version_file (version_id);

CREATE INDEX r_pool_depend_pool_id_idx ON public.r_pool_depend (pool_id);

CREATE INDEX r_pool_vehicle_pool_id_idx ON public.r_pool_vehicle (pool_id);

CREATE INDEX r_task_condition_task_id_idx ON public.r_task_condition (task_id);

CREATE INDEX r_task_ecu_task_id_idx ON public.r_task_ecu (task_id);

CREATE INDEX r_task_file_task_id_idx ON public.r_task_file (task_id);

CREATE INDEX r_vehicle_ecu_image_ecu_group_id_idx ON public.r_vehicle_ecu_image (ecu_group_id);
CREATE INDEX r_vehicle_ecu_image_ecu_id_idx ON public.r_vehicle_ecu_image (ecu_id);

CREATE INDEX r_vehicle_tag_vin_idx ON public.r_vehicle_tag (vin);
CREATE INDEX r_vehicle_tag_tag_id_idx ON public.r_vehicle_tag (tag_id);

CREATE INDEX r_vehicle_type_ecu_type_id_idx ON public.r_vehicle_type_ecu (type_id);

CREATE INDEX r_vehicle_version_type_id_idx ON public.r_vehicle_version (type_id);

CREATE INDEX r_vehicle_version_ecu_group_version_id_idx ON public.r_vehicle_version_ecu_group (version_id);

CREATE INDEX r_version_ecu_rely_group_id_idx ON public.r_version_ecu_rely (group_id);

CREATE INDEX r_diag_task_log_campaign_id_idx ON public.r_diag_task_log (campaign_id);
CREATE INDEX r_diag_task_log_session_id_idx ON public.r_diag_task_log (session_id);
CREATE INDEX r_diag_task_log_vin_idx ON public.r_diag_task_log (vin);

CREATE INDEX r_ecu_version_ecu_id_idx ON public.r_ecu_version (ecu_id);

CREATE INDEX r_diag_task_campaign_pool_id_idx ON public.r_diag_task_campaign (pool_id);
CREATE INDEX r_diag_task_campaign_task_id_idx ON public.r_diag_task_campaign (task_id);

CREATE INDEX r_version_record_type_idx ON public.r_version_record USING btree (type, value_id);

