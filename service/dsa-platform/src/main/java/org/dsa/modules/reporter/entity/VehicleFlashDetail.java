package org.dsa.modules.reporter.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
@TableName("vehicle_flash_detail")
public class VehicleFlashDetail extends BaseEntity{

    @TableId(type = IdType.AUTO)
    private Long id;

    private Integer flashId;

    private String vin;
    private String version;
    private String ecuName;
    private String did;
    private String versionName;
    private String versionValue;
    private String fid;
    private String resourceId;
    private Date flashTimestamp;
    private String reason;
    private Integer result;
    private String tag;
    private Date collectTime;
    //1 target 0 expected
    private Integer target;

    @TableField(exist = false)
    private Map<String, Object> ecus;
}
