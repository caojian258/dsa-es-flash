package org.dsa.modules.remoteDiagnostic.controller;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.annotation.SysLog;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.R;
import org.dsa.common.validator.ValidatorUtils;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.common.validator.group.UpdateGroup;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskCampaignEntity;
import org.dsa.modules.remoteDiagnostic.service.*;
import org.dsa.modules.remoteDiagnostic.vo.PublicPageReqVo;
import org.dsa.modules.remoteDiagnostic.vo.Task.DiagnosticTaskOverviewPageReqVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;

@Slf4j
@RestController
@RequestMapping("/diagnosticCampaign")
public class DiagnosticCampaignController {

    @Autowired
    DiagnosticTaskCampaignService campaignService;
    @Autowired
    DiagnosticTaskOverviewService overviewService;
    @Autowired
    DiagnosticTaskRecordService recordService;
    @Autowired
    DiagnosticTaskDetailService detailService;
    @Autowired
    DiagnosticTaskActiveService activeService;

    @RequestMapping("/getCampaignPage")
    public R getCampaignPage(@RequestBody PublicPageReqVo vo) {
        return R.ok().put("data", campaignService.selectPage(vo));
    }

    @GetMapping("/getCampaignInfo/{id}")
    public R getReleaseInfo(@PathVariable("id") Long id) {
        if (id == null) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        return R.ok().put("data", campaignService.getInfo(id));
    }

    @SysLog("新增诊断任务发布")
    @PostMapping("/saveCampaign")
//    @RequiresPermissions("sys:user:save")
    public R saveCampaign(@RequestBody DiagnosticTaskCampaignEntity task) {

        ValidatorUtils.validateEntity(513, task, AddGroup.class);
        if (task.getTaskTime() != null && task.getTaskTime().size() == 2) {
            task.setStartTime(Timestamp.valueOf(task.getTaskTime().get(0) + " 00:00:00.000"));
            task.setEndTime(Timestamp.valueOf(task.getTaskTime().get(1) + " 23:59:59.999"));
        }
        campaignService.saveTask(task);
        return R.ok();
    }

    @SysLog("修改诊断任务发布")
    @PostMapping("/updateCampaign")
//    @RequiresPermissions("sys:user:update")
    public R updateCampaign(@RequestBody DiagnosticTaskCampaignEntity task) {

        ValidatorUtils.validateEntity(513, task, UpdateGroup.class);
        if (task.getTaskTime() != null && task.getTaskTime().size() == 2) {
            task.setStartTime(Timestamp.valueOf(task.getTaskTime().get(0) + " 00:00:00.000"));
            task.setEndTime(Timestamp.valueOf(task.getTaskTime().get(1) + " 23:59:59.999"));
        }
        campaignService.updateTask(task);
        return R.ok();
    }


    @RequestMapping("/getTaskOverviewPage")
    public R getTaskOverviewPage(@RequestBody DiagnosticTaskOverviewPageReqVo vo) {

        return R.ok().put("data", overviewService.selectPage(vo));
    }
    @RequestMapping("/getTaskRecordPage")
    public R getTaskRecordPage(@RequestBody PublicPageReqVo vo) {
        return R.ok().put("data", recordService.selectPage(vo));
    }

    @PostMapping("/getTaskActivePage")
    public R getTaskLogList(@RequestBody PublicPageReqVo vo) {
        return R.ok().put("data", activeService.selectPage(vo));
    }

    @GetMapping("/getTaskDetailList/{id}")
    public R getTaskDetailPage(@PathVariable("id") String sessionId) {
        if (StringUtils.isBlank(sessionId)) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        return R.ok().put("data", detailService.getDetailList(sessionId));
    }


    @GetMapping("/getTaskOverviewInfo/{id}")
    public R getTaskOverviewInfo(@PathVariable("id") Long id) {
        if (id == null) {
            return R.error();
        }
        return R.ok().put("data", overviewService.getInfo(id));
    }

    @GetMapping("/getTaskLogList/{id}")
    public R getTaskLogList(@PathVariable("id") String sessionId) {
        if (StringUtils.isBlank(sessionId)) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        return R.ok().put("data", detailService.getTaskLogList(sessionId));
    }

    @RequestMapping("/terminationActive/{id}")
    public R terminationActive(@PathVariable("id") Long id) {
        if (id == null) {
            return R.error();
        }
        activeService.terminationActive(id);
        return R.ok();
    }
}
