package org.openea;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.read.listener.PageReadListener;
import com.alibaba.fastjson.JSON;
import junit.framework.TestCase;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.dsa.modules.diagnostic.entity.VehicleActiveEntity;
import org.dsa.modules.sys.dto.WorkshopExcelDto;
import org.dsa.modules.sys.entity.SysLocationEntity;
import org.dsa.modules.sys.service.SysLocationService;
import org.dsa.modules.sys.service.SysWorkshopsIService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import jakarta.annotation.Resource;;
import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;


@RunWith(SpringRunner.class)
@SpringBootTest
@SpringBootConfiguration
public class test  extends TestCase {

    public static void main(String[] args) {
        List <File> sqliteFiles = new ArrayList<>(16);
        //遍历操作日志文件
        sqliteFiles.forEach(file -> {
            file.getName();
        });

        System.out.println("2022-06-33 0".substring(0,10).replaceAll("-",""));
        for(String s : args){
            System.out.println("---------------"+s);
        }
        String[] s =new String[]{};
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("aa","aa");
        map.put("laa","aa");
        map.put("bb","aa");
        map.put("lbb","aa");
        map.put("cc","aa");
        map.put("lcc","aa");
        
        map.forEach((key,value)->{System.out.println(key);});

        System.out.println(new Sha256Hash("DsaSuperAdmin99126", "1fghj4d23uh28f394h34n56335b").toHex());

        map.keySet().removeIf(key->(key.startsWith("l")));
        map.forEach((key,value)->{System.out.println(key);});

        List<VehicleActiveEntity> vehicleActiveEntityList = new ArrayList<VehicleActiveEntity>();
        VehicleActiveEntity v = new VehicleActiveEntity();
        v.setVin("aaa");

        VehicleActiveEntity v1 = new VehicleActiveEntity();
        v1.setVin("bbb");

        VehicleActiveEntity v2 = new VehicleActiveEntity();
        v2.setVin("ccc");

        vehicleActiveEntityList.add(v);
        vehicleActiveEntityList.add(v1);
        vehicleActiveEntityList.add(v2);

        vehicleActiveEntityList.removeIf(vehicleActiveEntity ->("bbb".equals(vehicleActiveEntity.getVin())));
        System.out.println(vehicleActiveEntityList);

        try {
            System.out.println(InetAddress.getLocalHost().getHostName());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        Long l = new Date().getTime();
        System.out.println(l);
        System.out.println(new Random().nextInt(99));
        System.out.println(Integer.parseInt(l.toString().substring(5)));
        System.out.println(Long.parseUnsignedLong(new Random().nextInt(41)+l.toString().substring(5)));

        System.out.println(UUID.randomUUID());
    }
}