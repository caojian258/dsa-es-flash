package org.dsa.modules.diagnostic.service.impl;

import ch.qos.logback.classic.Logger;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.crypto.SecureUtil;
import org.apache.commons.io.FileUtils;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.Constant;
import org.dsa.modules.diagnostic.dao.*;
import org.dsa.modules.diagnostic.entity.DiagnosticRecordEntity;
import org.dsa.modules.diagnostic.service.LoggerService;
import org.dsa.modules.diagnostic.util.LogsUtils;
import org.dsa.modules.sys.dao.SysLogDao;
import org.dsa.modules.sys.dao.SysUserDao;
import org.dsa.modules.vehicle.dao.DVehicleInfoDao;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service("loggerService")
public class LoggerServiceImpl implements LoggerService {
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass());

    /**
     * session结束时间
     */
    private String sessionEndTime;

    @Resource
    private DSessionInfoDao dSessionInfoDao;

    @Resource
    private DVersionInfoDao dVersionDao;

    @Resource
    private DVersionRecordDao dVersionRecordDao;

    @Resource
    private SysLogDao sysLogDao;
    @Resource
    private SysUserDao sysUserDao;
    @Resource
    private DSessionActionDao dSessionActionDao;
    @Resource
    private DActionInfoDao dActionInfoDao;
    @Resource
    private DActionEcuDao dActionEcuDao;
    @Resource
    private DActionDtcDao dActionDtcDao;
    @Resource
    private DActionVersionDao dActionVersionDao;
    @Resource
    private DVehicleInfoDao dVehicleInfoDao;
    @Resource
    private  LogsUtils logsUtils;

    @Value("${file.record.path}")
    private String recordPath;


    @Override
    public int checkInputRecord(DiagnosticRecordEntity diagnosticRecordEntity) {
        //functionCode not match
        if (!Constant.Function.isFunction(diagnosticRecordEntity.getFunctionCode())) {
            return Constant.Msg.CHECK_FUNCTION.getCode();
        }
        //actionCode not match
        if (!Constant.Action.isAction(diagnosticRecordEntity.getActionCode())) {
            return Constant.Msg.CHECK_ACTION.getCode();
        }
        return 0;
    }

    @Override
    public int saveRecord(DiagnosticRecordEntity diagnosticRecordEntity) {
        if (diagnosticRecordEntity.getFunctionCode().equals(Constant.Function.BEGIN)) {
            //saveBeginRecord(diagnosticRecordEntity);
        } else if (diagnosticRecordEntity.getFunctionCode().equals(Constant.Function.END)) {
            //saveEndRecord(diagnosticRecordEntity);
        } else if (diagnosticRecordEntity.getFunctionCode().equals(Constant.Function.CALIBRATION)) {
            //saveCalibrationRecord(diagnosticRecordEntity);
        } else {
            return 511;
        }
        return 0;
    }

    @Override
    public void saveRecordFile(MultipartFile file, String md5, HttpServletRequest req) {
        File saveFile = dumpFiles(file);
        //文件md5文件完整性校验
        if (ObjectUtil.isNotNull(md5)){
            checkMd5(md5,saveFile);
            Map<String, Object> params = new HashMap<>(16);
            params.put("source",Constant.SESSION_SOURCE.VEHICLE.getDbName());
            params.put("file",saveFile);
            params.put("req",req);
            logsUtils.startLogging(params);
        }
    }
    /**
     * @description 转储文件
     * @param file 日志文件
     */
    private File dumpFiles(MultipartFile file) {
        String dateStr = DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN);
        File saveFile = new File(recordPath+ dateStr + Constant.PATH_SPLIT + file.getOriginalFilename());
        try {
            FileUtils.copyInputStreamToFile(file.getInputStream(), saveFile);
        } catch (IOException e) {
            logger.error("dumpLogFiles(),Failed to save file");
            throw new RRException(Constant.Msg.DIA_LOGGER_FILE_UPLOAD_E);
        }
        return saveFile;
    }


    /**
     * @description md5文件完整性校验
     * @param md5
     * @param file
     */
    public void checkMd5(String md5, File file) {
        if (!SecureUtil.md5(file).equals(md5)) {
            throw new RRException(Constant.Msg.DIA_LOGGER_FILE_UPLOAD_E);
        }
    }


}