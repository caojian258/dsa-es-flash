package org.dsa.modules.diagnostic.controller;

import lombok.extern.slf4j.Slf4j;
import org.dsa.common.annotation.SysLog;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.AESUtils;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.R;
import org.dsa.modules.diagnostic.dto.DDiagPackageVersionDTO;
import org.dsa.modules.diagnostic.dto.PackageCompareDTO;
import org.dsa.modules.diagnostic.dto.PackageCompareReqDTO;
import org.dsa.modules.diagnostic.entity.DDiagPackageVersionEntity;
import org.dsa.modules.diagnostic.service.PackageVersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;

/**
 * @author weishunxin
 * @since 2023-05-29
 */
@Slf4j
@RestController
@RequestMapping("/package/version")
public class PackageVersionController {

    @Autowired
    private PackageVersionService packageVersionService;
    @Value("${project.url}")
    private String projectUrl;

    @GetMapping("/list")
    public R list(@RequestParam Long vehicleTypeId){
        List<DDiagPackageVersionEntity> list = packageVersionService.list(vehicleTypeId);
        return R.ok().put("list", list);
    }

    @GetMapping("/get-info")
    public R getInfo(@RequestParam Long id){
        DDiagPackageVersionDTO data = packageVersionService.getInfo(id);
        return R.ok().put("data", data);
    }

    @SysLog("保存包版本信息")
    @PostMapping("/save")
    public R save(@RequestBody DDiagPackageVersionEntity entity) {
        packageVersionService.savePackage(entity);
        return R.ok();
    }

    @SysLog("更新包版本信息")
    @PostMapping("/update")
    public R update(@RequestBody DDiagPackageVersionEntity entity) {
        packageVersionService.updatePackage(entity);
        return R.ok();
    }

    @SysLog("复制包版本信息")
    @GetMapping("/copy")
    public R copy(@RequestParam Long id) {
        packageVersionService.copyPackage(id);
        return R.ok();
    }

    @GetMapping("/check")
    public R check(@RequestParam Long vehicleTypeId) {
        boolean b = packageVersionService.hasNotReleasedPackage(vehicleTypeId);
        if (b) {
            throw new RRException(Constant.Msg.VERSION_CHECK_STATUS);
        }
        return R.ok();
    }

    @PostMapping("/latest")
    public R latest(@RequestBody @Validated PackageCompareReqDTO compareReqDTO) {
        PackageCompareDTO packageCompareDTO = packageVersionService.comparePackage(compareReqDTO);
        if (Objects.isNull(packageCompareDTO)) {
            packageCompareDTO = this.noPackageData();
        } else {
            String path = packageCompareDTO.getUrl();
            String encrypt = AESUtils.encrypt(path, AESUtils.AES_KEY);
            Assert.notNull(encrypt, "encrypt can not be null.");

            String url;
            url = projectUrl + "/sys/file/security-download?params=" + URLEncoder.encode(encrypt, StandardCharsets.UTF_8);
            packageCompareDTO.setUrl(url);
        }
        return R.ok().put("data", packageCompareDTO);
    }

    private PackageCompareDTO noPackageData() {
        PackageCompareDTO packageCompareDTO = new PackageCompareDTO();
        packageCompareDTO.setUrl("");
        return packageCompareDTO;
    }

}
