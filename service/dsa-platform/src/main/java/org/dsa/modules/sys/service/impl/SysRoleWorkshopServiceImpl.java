package org.dsa.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.dsa.modules.sys.dao.SysRoleWorkshopDao;
import org.dsa.modules.sys.entity.SysRoleWorkshopEntity;
import org.dsa.modules.sys.service.SysRoleWorkshopService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * 角色与部门对应关系
 *
 */
@Service("sysRoleWorkshopService")
public class SysRoleWorkshopServiceImpl extends ServiceImpl<SysRoleWorkshopDao, SysRoleWorkshopEntity> implements SysRoleWorkshopService {

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void saveOrUpdate(Long roleId, List<Long> workshopIdList) {
		//先删除角色与部门关系
		deleteBatch(new Long[]{roleId});

		if(workshopIdList.size() == 0){
			return ;
		}

		//保存角色与菜单关系
		for(Long workshopId : workshopIdList){
			SysRoleWorkshopEntity sysRoleWorkshopEntity = new SysRoleWorkshopEntity();
			sysRoleWorkshopEntity.setWorkshopId(workshopId);
			sysRoleWorkshopEntity.setRoleId(roleId);

			this.save(sysRoleWorkshopEntity);
		}
	}

	@Override
	public List<Long> queryWorkshopIdList(Long[] roleIds) {
		return baseMapper.queryWorkshopIdList(roleIds);
	}

	@Override
	public int deleteBatch(Long[] roleIds){
		return baseMapper.deleteBatch(roleIds);
	}
}
