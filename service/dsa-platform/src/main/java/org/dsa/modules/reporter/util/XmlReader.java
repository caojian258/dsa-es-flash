package org.dsa.modules.reporter.util;

import lombok.extern.slf4j.Slf4j;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.util.*;

/****
 * XML解析器
 */
@Slf4j
public class XmlReader {

    /***
     * XML文件路径
     */
    private String path;

    /***
     * 所用时间
     */
    private long spendTime;

    public XmlReader(){
    }

    public XmlReader(String path){
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path){
        this.path = path;
    }

    public Long getSpendTime(){
        return spendTime;
    }

    /***
     * 常用解析器带节点
     * @param nodeName
     * @return
     */
    public List<Map<String,Object>> parse(String nodeName) throws Exception {
        List<Map<String, Object>> data = new ArrayList<>();
        long startTime = System.currentTimeMillis();
//        try {
            File f = new File(path);
            SAXReader reader = new SAXReader();
            Document doc = (Document) reader.read(f);
            Element root = doc.getRootElement();
            Element foo;
            Iterator i = root.elementIterator(nodeName);
            while ( i.hasNext()){
                foo = (Element) i.next();
                Map<String, Object> nodeValue = new HashMap<>();
                Iterator ie = foo.elementIterator();
                Iterator<Attribute> attrs = foo.attributeIterator();
                while (attrs.hasNext()){
                    Attribute attr = attrs.next();
                    nodeValue.put(attr.getName(), attr.getValue());
                }
                while (ie.hasNext()){
                    Element ss =  (Element) ie.next();
                    if(null != ss){
                        Iterator ssi = ss.elementIterator();
                        if(ssi.hasNext()){
                            while (ssi.hasNext()){
                                Element attrChild = (Element)ssi.next();

                                nodeValue.put(attrChild.getName(), attrChild.getStringValue());
                                Iterator<Attribute> childAttrs = attrChild.attributeIterator();
                                if(childAttrs.hasNext()){
                                    while (childAttrs.hasNext()){
                                        Attribute childAttr = childAttrs.next();
                                        nodeValue.put(childAttr.getName(), childAttr.getValue());
                                    }
                                }
                            }
                        }else{

                            Iterator<Attribute> ssAttrs = ss.attributeIterator();
                            List<Object> attsList = new ArrayList<>();
                            if(ssAttrs.hasNext()){
                                while (ssAttrs.hasNext()){
                                    Attribute ssAttr = ssAttrs.next();
                                    attsList.add(ssAttr.getValue());
                                }

                                List<Object> oldAttsList = (List<Object>) nodeValue.get(ss.getName());
                                if (oldAttsList !=null && oldAttsList instanceof Collection) {
                                    attsList.addAll(oldAttsList);
                                }
                                nodeValue.put(ss.getName(), attsList);

                            }else{
                                nodeValue.put(ss.getName(), ss.getText());
                            }

                        }
                    }

                }
                data.add(nodeValue);
            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        long endTime = System.currentTimeMillis();
        this.spendTime = endTime - startTime;

        return data;
    }

    /***
     * 带回调函数的解析器
     * @param nodeName
     * @param handler
     * @return
     */
    public List<Map<String,Object>> parseCallback(String nodeName, XmlHandler handler) throws Exception {
        List<Map<String, Object>> data = new ArrayList<>();
        long startTime = System.currentTimeMillis();

//        try {
            File f = new File(path);
            SAXReader reader = new SAXReader();
            Document doc = (Document) reader.read(f);
            Element root = doc.getRootElement();
            Iterator i = root.elementIterator(nodeName);
            Element memberElm = root.element(nodeName);
            Map<String, Object> nodeNameAttr = new HashMap<>();
            for (Attribute attribute: memberElm.attributes()){
                nodeNameAttr.put(attribute.getName(), attribute.getValue());
            }
            if(nodeNameAttr.size()>0){
                data.add(nodeNameAttr);
            }

            while ( i.hasNext()){
                List<Map<String,Object>>  nodeValues = handler.handle(i);
                if(null != nodeValues && nodeValues.size()>0){
                    data.addAll(nodeValues);
                }
            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        long endTime = System.currentTimeMillis();
        this.spendTime = endTime - startTime;

        return data;
    }

    /***
     * 带回调函数的解析器
     * @param nodeName
     * @param handler
     * @return
     */
    public List<Map<String,Object>> parseCallback2(String nodeName, XmlHandler handler){
        List<Map<String, Object>> data = new ArrayList<>();
        long startTime = System.currentTimeMillis();

        try {
            File f = new File(path);
            SAXReader reader = new SAXReader();
            Document doc = (Document) reader.read(f);
            Element root = doc.getRootElement();
            Iterator i = root.elementIterator(nodeName);

            List<Element> memberElm = root.elements(nodeName);
            List<Map<String, Object>> nodeNameAttr = new ArrayList<>();
            for (Element element : memberElm) {
                Map<String, Object> nodeNameMap = new HashMap<String, Object>();
                for (Attribute attribute : element.attributes()) {
                    nodeNameMap.put(attribute.getName(), attribute.getValue());
                }
                if ( i.hasNext()){
                    List<Map<String,Object>>  nodeValues = handler.handle(i);
                    if(null != nodeValues && nodeValues.size()>0){
                        nodeNameMap.put("data",nodeValues);
                    }
                }
                nodeNameAttr.add(nodeNameMap);
            }
//            for (Attribute attribute: memberElm.attributes()){
//                nodeNameAttr.put(attribute.getName(), attribute.getValue());
//            }
            data.addAll(nodeNameAttr);

//            while ( i.hasNext()){
//                List<Map<String,Object>>  nodeValues = handler.handle(i);
//                if(null != nodeValues && nodeValues.size()>0){
//                    data.addAll(nodeValues);
//                }
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        long endTime = System.currentTimeMillis();
        this.spendTime = endTime - startTime;

        return data;
    }


    /***
     * 回调函数必须实现的接口
     */
    @FunctionalInterface
    public
    interface XmlHandler {
        public List<Map<String,Object>> handle(Iterator<Element> ele);
    }
}
