package org.dsa.modules.reporter.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.reporter.dao.ScheduleJobLogMapper;
import org.dsa.modules.reporter.entity.ScheduleJobLog;
import org.dsa.modules.reporter.service.ScheduleJobLogService;
import org.dsa.modules.reporter.vo.jobLog.JobLogPageReqVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.sql.Timestamp;
import java.util.Date;

@Service
public class ScheduleJobLogServiceImpl implements ScheduleJobLogService {

    @Autowired
    ScheduleJobLogMapper logMapper;

    @Override
    public void insertLog(Long jobId, String jobName, String jobGroup, String status, String note) {
        ScheduleJobLog logData = new ScheduleJobLog();
        logData.setJobId(jobId);
        logData.setBeanName(jobGroup);
        logData.setStatus(status);

        logData.setCreateTime(new Date());

        if(!StringUtils.isEmpty(note)){
            logData.setError(jobName+": "+note);
        }else{
            logData.setError(jobName+" "+ status);
        }

        logMapper.insert(logData);
    }

    @Override
    public Page<ScheduleJobLog> selectPage(JobLogPageReqVo vo) {

        Page<ScheduleJobLog> page = new Page<>(vo.getPageNo(), vo.getPageSize());
        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);
        LambdaQueryWrapper<ScheduleJobLog> qw =  Wrappers.<ScheduleJobLog>lambdaQuery();

        if(!StringUtils.isEmpty(vo.getGroup())){
//            qw.like(ScheduleJobLog::getBeanName, vo.getGroup());
            qw.apply("bean_name"+" ilike {0}", "%"+vo.getGroup().trim()+"%");
        }

        if(!StringUtils.isEmpty(vo.getStartDate()) && !StringUtils.isEmpty(vo.getEndDate()) ){
            qw.between(ScheduleJobLog::getCreateTime,
                    Timestamp.valueOf(vo.getStartDate()+" 00:00:00.000"),
                    Timestamp.valueOf(vo.getEndDate()+" 23:59:59.999"));
        }

        if(!StringUtils.isEmpty(vo.getJobId())){
            qw.eq(ScheduleJobLog::getJobId, vo.getJobId());
        }

        if(!StringUtils.isEmpty(vo.getStatus())){
            qw.eq(ScheduleJobLog::getStatus, vo.getStatus());
        }

        if(!StringUtils.isEmpty(vo.getError())){
            qw.and((wrapper)->{
                wrapper.apply("error"+" ilike {0}", "%"+vo.getError().trim()+"%")
                        .or()
                        .apply("bean_name"+" ilike {0}", "%"+vo.getError().trim()+"%");
            });
        }
        Page<ScheduleJobLog> logPage = logMapper.selectPage(page, qw);

        return logPage;
    }


}
