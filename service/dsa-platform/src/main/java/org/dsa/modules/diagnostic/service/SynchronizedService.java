package org.dsa.modules.diagnostic.service;


import org.dsa.modules.vehicle.entity.VehicleInfoEntity;

import java.util.List;

/**
 * 诊断功能
 *
 */
public interface SynchronizedService {

    
    /**
     * 3.4.6.1	TSP服务器向远程诊断服务器同步车辆
     * @param vehicleInfoEntity
     */
    void save(List<VehicleInfoEntity> vehicleInfoEntity);

    /**
     * 同步DB信息至Redis
     * @return
     */
    void synRedis();
}

