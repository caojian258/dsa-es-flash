package org.dsa.modules.diagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("d_session_action")
public class DSessionActionEntity implements Serializable
{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 会话id
     */
    private String sessionId;

    /**
     * 命令id
     */
    private String actionId;

    /**
     * 创建时间
     */
    private Date createTime;

}
