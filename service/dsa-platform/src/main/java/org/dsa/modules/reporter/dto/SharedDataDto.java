package org.dsa.modules.reporter.dto;

import lombok.Data;

@Data
public class SharedDataDto {

    private String pcId;

    private String madMake;

    private String madModel;

    private Long vehTypeId;

}
