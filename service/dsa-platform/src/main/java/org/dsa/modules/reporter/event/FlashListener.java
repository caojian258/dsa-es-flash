package org.dsa.modules.reporter.event;

import lombok.extern.slf4j.Slf4j;
import org.dsa.modules.reporter.service.FlashLogService;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import jakarta.annotation.Resource;


@Slf4j
@Component
public class FlashListener implements ApplicationListener<FlashEvent> {

    @Resource
    FlashLogService flashLogService;

    @Async
    @Override
    public void onApplicationEvent(FlashEvent event) {
        log.info("Enter FlashListener {}", System.currentTimeMillis());
        if(null == event){
            log.error("systemTaskExecutor event is null or tag is null");
            return;
        }
        try {
            flashLogService.syncData(event.getVo());
        } catch (Exception e) {

            log.error("systemTaskExecutor:{}", e.getMessage());
        }
    }
}
