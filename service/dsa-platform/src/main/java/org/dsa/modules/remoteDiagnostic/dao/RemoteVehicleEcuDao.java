package org.dsa.modules.remoteDiagnostic.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuEntity;

import java.util.List;

@Mapper
public interface RemoteVehicleEcuDao extends BaseMapper<RemoteVehicleEcuEntity> {

    List<RemoteVehicleEcuEntity> getYList(@Param("platformId") Long id, @Param("name") String name);

    List<RemoteVehicleEcuEntity> getNList(@Param("platformId") Long id, @Param("name") String name);

    //    @Select(
//            " <script> "
//                    + " select i.*,i.id as ecuId from r_vehicle_ecu i where i.status = 0 "
//                    + " <if test='ids!=null and ids.size!=0 '>"
//                    + " and i.id in  "
//                    + " <foreach collection='ids' item='item' open=\"(\" separator=\",\" close=\")\" >"
//                    + " #{item} "
//                    + " </foreach> "
//                    + " </if> "
//                    + " order by i.id "
//                    + " </script>")
    @Select(
            " <script> "
                    + "       select  i.*,i.id as ecuId from r_vehicle_ecu i  where i.status = 0" +
                    "        <if test='ids!=null and ids.size!=0 '>" +
                    "            and i.id in" +
                    "            <foreach collection='ids' item='item' open=\"(\" separator=\",\" close=\")\">" +
                    "                #{item}" +
                    "            </foreach>" +
                    "        </if>" +
                    "        order by i.id"
                    + " </script>")
    @Results({
            @Result(property = "ecuVersions", column = "ecuId",
                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.RemoteEcuVersionDao.getList")),
    })
    List<RemoteVehicleEcuEntity> getList(@Param("ids") List<Long> ids);

    @Select(
            " <script> "
                    + "select i.*,i.id as ecuId,#{id} as vId,j.ecu_version_id as versionId from r_vehicle_ecu i left join (select * from r_version_ecu_rely where group_id = " +
                    " (select group_id from r_version_ecu_rely where ecu_version_id = #{id} )) j on i.id = j.ecu_id  where i.status = 0" +
                    "        <if test='ids!=null and ids.size!=0 '>" +
                    "            and i.id in" +
                    "            <foreach collection='ids' item='item' open=\"(\" separator=\",\" close=\")\">" +
                    "                #{item}" +
                    "            </foreach>" +
                    "        </if>" +
                    "        "
                    + " </script>")
    @Results({
            @Result(property = "ecuVersions", column = "{ecuId=ecuId,versionId=vId}",
                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.RemoteEcuVersionDao.getListById")),
    })
    List<RemoteVehicleEcuEntity> getListById(@Param("ids") List<Long> ids, @Param("id") Long id);


    //    @Select("select i.*,r.v_id as version_id,i.id as ecuId,#{versionId} as vId from r_vehicle_ecu i inner join r_version_ecu_rely r on i.id = r.ecu_id where r.version_id = #{versionId} order by r.id ")
    @Select("select r.*,i.ecu_version_id as version_id,r.id as ecuId,#{versionId} as vId" +
            " from r_version_ecu_rely i inner join r_vehicle_ecu r on i.ecu_id = r.id where i.ecu_version_id != #{versionId} and i.group_id = (select group_id from r_version_ecu_rely where ecu_version_id = #{versionId} ) order by i.id ")
    @Results({
            @Result(property = "ecuVersions", column = "{ecuId=ecuId,versionId=vId}",
                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.RemoteEcuVersionDao.getListById")),
    })
    List<RemoteVehicleEcuEntity> getListByVersionId(@Param("versionId") Long versionId);

    @Select("select i.*,i.id as ecuId from r_vehicle_ecu i where i.id = #{ecuId} ")
    @Results({
            @Result(property = "ecuVersions", column = "ecuId",
                    many = @Many(select = "org.dsa.modules.remoteDiagnostic.dao.RemoteEcuVersionDao.getList")),
    })
    RemoteVehicleEcuEntity getEcu(@Param("ecuId") Long ecuId);

//    @Select(" select distinct i.* from r_vehicle_ecu i left join r_ecu_ecu_version j on i.id = j.ecu_id left join r_ecu_version k on j.version_id = k.id  " +
//            "where i.status = 0  and j.id is not null and (k.id is not null and (k.status = 3 or k.status = 4)) and i.id  in (select j.ecu_id from r_ecu_group j where j.group_id = #{groupId}) ")
//    List<RemoteVehicleEcuEntity> getListByGroupId(@Param("groupId") Long groupId);

    @Select(" select distinct i.* from r_vehicle_ecu i left join  r_ecu_version j on i.id = j.ecu_id and j.status = 3 " +
            " where i.status = 0 and j.id is not null and i.id  in (select j.ecu_id from r_ecu_group j where j.group_id = #{groupId})")
    List<RemoteVehicleEcuEntity> getListByGroupId(@Param("groupId") Long groupId);

    //    @Select("select distinct i.* from  r_vehicle_ecu i left join r_ecu_ecu_version" +
//            " j on i.id = j.ecu_id left join  r_ecu_version k on j.version_id = k.init_id where i.status = 0 and (k.status = 3 or k.status = 4) and i.id != #{id} and j.id is not null ")

    List<RemoteVehicleEcuEntity> getNoIdEcus(@Param("id") Long id);

    @Results({
            @Result(property = "flag", column = "{id=ecuId,versionId=versionId}",
                    one = @One(select = "org.dsa.modules.remoteDiagnostic.dao.RemoteEcuEcuVersionDao.getList")),
    })
    List<RemoteVehicleEcuEntity> getNoEcusByGroup(@Param("name") String name);

    @Select("select i.*,j.group_id as groupId from r_vehicle_ecu i left join r_ecu_group j on i.id = j.ecu_id  where i.id = #{id} ")
    RemoteVehicleEcuEntity getInfo(@Param("id") Long id);

    Long getFlag(@Param("id") Long id, @Param("versionId") Long versionId);


    @Select(" select distinct i.* from r_vehicle_ecu i " +
            " where i.status = 0 and i.id in (select j.ecu_id from r_ecu_group j where j.group_id = #{groupId})")
    List<RemoteVehicleEcuEntity> getEcusByGroupId(@Param("groupId") Long groupId);


    @Select("select distinct i.* from r_vehicle_ecu i left join  r_ecu_version j on i.id = j.ecu_id \n" +
            "             where i.id  in (select j.ecu_id from r_ecu_group j where j.group_id = #{groupId} )")
    List<RemoteVehicleEcuEntity> getFListByGroupId(@Param("groupId") Long groupId);


    List<RemoteVehicleEcuEntity> findVehicleEcuByVehicleVersion(@Param("versionId") Long versionId);

    List<RemoteVehicleEcuEntity> getEcuByPartNumber(@Param("partNumberName") String partNumberName, @Param("partNumberValue") String partNumberValue);
}
