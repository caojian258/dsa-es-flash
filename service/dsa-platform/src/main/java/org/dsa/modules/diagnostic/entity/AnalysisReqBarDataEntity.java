package org.dsa.modules.diagnostic.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 诊断统计--饼状图数据
 */
@Data
public class AnalysisReqBarDataEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 类型
	 */
	private String type;

	/**
	 * 类型
	 */
	private String name;

	/**
	 * 访问次数
	 */
	private List<Integer> data;
}
