package org.dsa.modules.reporter.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dsa.modules.reporter.entity.SystemProtocolItem;

import java.util.List;

public interface SystemProtocolItemIService extends IService<SystemProtocolItem> {

    public List<SystemProtocolItem> getPcItems(String pcid, String startDate, String endDate);
}
