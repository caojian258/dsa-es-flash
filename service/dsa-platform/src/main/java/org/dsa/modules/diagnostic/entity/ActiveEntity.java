package org.dsa.modules.diagnostic.entity;

import lombok.Data;
import java.io.Serializable;

/**
 * 远程诊断激活
 * 
 */
@Data
public class ActiveEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 车辆VIN码
	 */
	private String vin;
	/**
	 * 会话ID
	 */
	private String sessionId;

}
