package org.dsa.modules.reporter.vo.file;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.modules.reporter.pojo.PageParam;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class FileLogPageReqVo extends PageParam {

    private String name;

    private Integer status;

    private String tag;

    private String error;

    private String startDate;

    private String endDate;

    private String fileType;

}
