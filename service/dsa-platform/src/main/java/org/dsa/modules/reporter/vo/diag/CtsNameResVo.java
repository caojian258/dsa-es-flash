package org.dsa.modules.reporter.vo.diag;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(callSuper = true)
public class CtsNameResVo {

    private String ctsName;

    private String ctsTitle;

    private String functionCode;

    private String actionCode;
}
