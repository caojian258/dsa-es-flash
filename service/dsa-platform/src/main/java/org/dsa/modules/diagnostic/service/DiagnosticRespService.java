package org.dsa.modules.diagnostic.service;


import org.dsa.common.utils.R;

/**
 * 诊断功能 执行结果查询
 *
 */
public interface DiagnosticRespService {

    /**
     * 开始诊断
     * @param actionId
     * @return
     */
    R beginResp(String actionId);

    /**
     * 结束诊断
     * @param actionId
     * @return
     */
    R endResp(String actionId);

    /**
     * 车辆健康检查
     */
    R healthyResp(String actionId);
    /**
     * 读取版本信息结果
     */
    R readIdentDataResp(String actionId);
    /**
     * 读取故障码结果
     */
    R readDtcCodeResp(String actionId,int length);
    /**
     *读取故障码冻结帧结果
     */
    R readDtcDataGridResp(String actionId);

    /**
     * 清楚故障码
     */
    R clearDtcResp(String actionId);
}

