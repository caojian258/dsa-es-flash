package org.dsa.modules.remoteDiagnostic.service;

import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuVersionEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuVersionFileEntity;
import org.dsa.modules.remoteDiagnostic.vo.CheckNameReqVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.remoteDiagnostic.vo.EcuVersion.FlashFileReqVo;
import org.dsa.modules.remoteDiagnostic.vo.PublicPageReqVo;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface RemoteEcuVersionService {

    Page<RemoteVehicleEcuVersionEntity> selectPage(PublicPageReqVo vo);

    RemoteVehicleEcuVersionEntity getInfo(Long id);

    void saveVersion(RemoteVehicleEcuVersionEntity version);

    void updateVersion(RemoteVehicleEcuVersionEntity version);

    List<RemoteVehicleEcuVersionEntity> getVersions();

    List<RemoteVehicleEcuVersionEntity> getVersions(Long ecuId);


    Boolean check(CheckNameReqVo vo);

    Map<String, String> upload(File file, String type) throws Exception;

    RemoteVehicleEcuEntity getEcuVersionByEcuId(Long id);

    Map<String, String> uploadImage(File file) throws IOException;

    List<RemoteVehicleEcuVersionFileEntity> getVersionFile(String type, Long id);

    List<RemoteVehicleEcuEntity> selectEcuVersionDepend(Long id, Long ecuId);

    List<RemoteVehicleEcuVersionEntity> getAllVersions(Integer status);

    RemoteVehicleEcuVersionEntity getOneInfo(Long id);

    void copyVersionById(Long id);
}
