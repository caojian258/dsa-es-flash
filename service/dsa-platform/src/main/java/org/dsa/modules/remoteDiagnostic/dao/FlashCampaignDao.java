package org.dsa.modules.remoteDiagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.dsa.modules.remoteDiagnostic.entity.FlashCampaignEntity;
import org.dsa.modules.remoteDiagnostic.vo.Flash.FlashCampaignResVo;

@Mapper
public interface FlashCampaignDao extends BaseMapper<FlashCampaignEntity> {

    Page<FlashCampaignResVo>  selectPageForClient(Page<FlashCampaignResVo> page, String vin, String version );

}
