package org.dsa.modules.reporter.dto;

import lombok.Data;

@Data
public class IdentificationDto {

    private String name;

    private String partNumber;

    private String HWVersion;

    private String SWVersion;

}
