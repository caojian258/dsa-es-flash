package org.dsa.modules.reporter.dto;


import lombok.Data;

import java.util.Date;

@Data
public class AppUnInstallDto extends AppVersionDto{

    private String result;

    private Integer appId;

    private Integer updateOid;

    private Date time;

}
