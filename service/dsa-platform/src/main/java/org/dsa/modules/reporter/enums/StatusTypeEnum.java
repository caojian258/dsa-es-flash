package org.dsa.modules.reporter.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusTypeEnum {

    /***
     *      0,none(无操作)
     *      1,download(下载)
     *      2,install(安装)
     *      3,repair(修复)
     *      4,rollback(回退)
     *      5,uninstall(卸载)
     */
    none(0),
    download(1),
    install(2),
    repair(3),
    rollback(4),
    uninstall(5),
    ;

    @EnumValue
    private final Integer value;
}
