package org.dsa.modules.reporter.upgrade;

import lombok.extern.slf4j.Slf4j;
import org.dsa.modules.reporter.dto.AppVersionDto;
import org.dsa.modules.reporter.entity.AppHistory;
import org.dsa.modules.reporter.entity.ClientUpgrade;
import org.dsa.modules.reporter.enums.AppTypeEnum;
import org.dsa.modules.reporter.service.AppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/***
 * 这里只做回滚成功的记录
 */

@Slf4j
@Service
public class RepairUpgradeStrategy implements UpgradeStrategy{

    @Autowired
    AppService appService;

    @Override
    public void save(ClientUpgrade log) {
        String type = "";
        List<AppVersionDto> versionDtos = new ArrayList<>();

        Integer appId = 0;

        if (log.getInstallAction() == 0) {
            type = AppTypeEnum.SOFTWARE.getValue();
            appId = log.getSoftwareId();
        } else {
            type = AppTypeEnum.COMPONENT.getValue();
            appId = log.getComponentId();
        }

        if (log.getUpdateResult() == 0) {

            AppVersionDto versionDto = new AppVersionDto();
            versionDto.setVersion("");
            versionDto.setName("");
            versionDto.setAppId(appId);
            versionDto.setType(type);
            versionDto.setVersionId(0);
            versionDtos.add(versionDto);

            appService.batchSaveOverview(versionDtos, log.getWorkshopName(), log.getDeviceId(),
                    log.getStartTime(), log.getTag());

            appService.batchAddVersion(versionDtos, log.getWorkshopName(), log.getDeviceId(),
                    log.getStartTime(), log.getTag());
        }
    }

    @Override
    public void saveHis(AppHistory data) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Set<AppVersionDto> versionDtos = new HashSet<>();
        if (data.getStatus() == 0) {

            AppVersionDto versionDto = new AppVersionDto();
            versionDto.setVersion(data.getAppVersion());
            versionDto.setName(data.getAppName());
            versionDto.setAppId(data.getAppId());
            versionDto.setType(data.getAppType());
            versionDto.setVersionId(data.getVersionNumber());
            versionDto.setTimestamp(sdf.format(data.getStartTime()));

            if(null != data.getAppPrevVersion()){
                versionDto.setPrevVersion(data.getAppPrevVersion());
            }

            versionDtos.add(versionDto);

            appService.batchSaveOverview(versionDtos, data.getWorkshopName(), data.getDeviceId(),
                    data.getCollectDate(), data.getTag());

            appService.batchAddVersion(versionDtos, data.getWorkshopName(), data.getDeviceId(),
                    data.getCollectDate(), data.getTag());
        }
    }
}
