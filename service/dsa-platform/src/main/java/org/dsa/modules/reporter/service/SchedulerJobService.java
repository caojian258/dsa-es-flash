package org.dsa.modules.reporter.service;


import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.dsa.modules.reporter.dao.SchedulerJobMapper;
import org.dsa.modules.reporter.entity.ScheduleJob;
import org.dsa.modules.reporter.enums.JobStatusEnum;
import org.dsa.modules.reporter.job.AllJobsListener;
import org.dsa.modules.reporter.job.JobScheduleCreator;
import org.dsa.modules.reporter.vo.job.JobPageReqVo;
import org.quartz.*;
import org.quartz.impl.matchers.KeyMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Slf4j
@Transactional
@Service
public class SchedulerJobService {

	@Autowired
	private Scheduler scheduler;

	@Autowired
	private SchedulerFactoryBean schedulerFactoryBean;

	@Autowired
	private SchedulerJobMapper schedulerJobMapper;

	@Autowired
	private ApplicationContext context;

	@Autowired
	private JobScheduleCreator scheduleCreator;

	@Autowired
	private AllJobsListener allJobsListener;

	@Autowired
	ScheduleJobLogService logService;

	public SchedulerMetaData getMetaData() throws SchedulerException {
		SchedulerMetaData metaData = scheduler.getMetaData();
		return metaData;
	}

	public List<ScheduleJob> getAllJobList() {
		return schedulerJobMapper.selectList(null);
	}

    public boolean deleteJob(ScheduleJob jobInfo) {
        try {
			ScheduleJob getJobInfo = schedulerJobMapper.findByJobName(jobInfo.getJobName());
			schedulerJobMapper.deleteById(getJobInfo);
        	log.info(">>>>> jobName = [" + jobInfo.getJobName() + "]" + " deleted.");
			JobKey jobKey = new JobKey(jobInfo.getJobName(), jobInfo.getJobGroup());
			Scheduler sched = schedulerFactoryBean.getScheduler();
			Trigger trigger;
			if (jobInfo.getCronJob()) {
				trigger = scheduleCreator.createCronTrigger(jobInfo.getJobName(), new Date(),
						jobInfo.getCronExpression(), SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);
			} else {
				trigger = scheduleCreator.createSimpleTrigger(jobInfo.getJobName(), new Date(),
						jobInfo.getRepeatTime(), SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);
			}

			sched.pauseTrigger(trigger.getKey());
			sched.unscheduleJob(trigger.getKey());
			sched.pauseJob(jobKey);

			logService.insertLog(jobInfo.getJobId(), jobInfo.getJobName(), jobInfo.getJobGroup(), JobStatusEnum.PAUSED.getValue(), "deleted");
			return sched.deleteJob(jobKey);
        } catch (SchedulerException e) {
            log.error("Failed to delete job - {}", jobInfo.getJobName(), e);
			logService.insertLog(jobInfo.getJobId(), jobInfo.getJobName(), jobInfo.getJobGroup(), jobInfo.getJobStatus(), e.getMessage());

			return false;
        }
    }

    public boolean pauseJob(ScheduleJob jobInfo) {
        try {
			ScheduleJob getJobInfo = schedulerJobMapper.findByJobName(jobInfo.getJobName());
        	getJobInfo.setJobStatus(JobStatusEnum.PAUSED.getValue());
			schedulerJobMapper.updateById(getJobInfo);
            schedulerFactoryBean.getScheduler().pauseJob(new JobKey(jobInfo.getJobName(), jobInfo.getJobGroup()));
            log.info(">>>>> jobName = [" + jobInfo.getJobName() + "]" + " paused.");

			logService.insertLog(jobInfo.getJobId(), jobInfo.getJobName(), jobInfo.getJobGroup(), "PAUSED", "paused");

			return true;
        } catch (SchedulerException e) {
            log.error("Failed to pause job - {}", jobInfo.getJobName(), e);
			logService.insertLog(jobInfo.getJobId(), jobInfo.getJobName(), jobInfo.getJobGroup(), jobInfo.getJobStatus(), e.getMessage());

			return false;
        }
    }

    public boolean resumeJob(ScheduleJob jobInfo) {
        try {
			ScheduleJob getJobInfo = schedulerJobMapper.findByJobName(jobInfo.getJobName());
        	getJobInfo.setJobStatus(JobStatusEnum.RESUMED.getValue());
			schedulerJobMapper.updateById(getJobInfo);
            schedulerFactoryBean.getScheduler().resumeJob(new JobKey(jobInfo.getJobName(), jobInfo.getJobGroup()));
            log.info(">>>>> jobName = [" + jobInfo.getJobName() + "]" + " resumed.");
			logService.insertLog(jobInfo.getJobId(), jobInfo.getJobName(), jobInfo.getJobGroup(), "RESUMED", "RESUMED");

			return true;
        } catch (SchedulerException e) {
            log.error("Failed to resume job - {}", jobInfo.getJobName(), e);
			logService.insertLog(jobInfo.getJobId(), jobInfo.getJobName(), jobInfo.getJobGroup(), jobInfo.getJobStatus(), "Failed to resume job :"+e.getMessage());

			return false;
        }
    }

    public boolean startJobNow(ScheduleJob jobInfo) {
        try {
        	ScheduleJob getJobInfo = schedulerJobMapper.findByJobName(jobInfo.getJobName());

			if(null != getJobInfo){
				getJobInfo.setJobStatus(JobStatusEnum.STARTED.getValue());
				schedulerJobMapper.updateById(getJobInfo);
			}else{
				getJobInfo = jobInfo;
				getJobInfo.setJobStatus(JobStatusEnum.STARTED.getValue());
				schedulerJobMapper.insert(jobInfo);
			}
			Scheduler sched = schedulerFactoryBean.getScheduler();
			JobKey jobKey = new JobKey(jobInfo.getJobName(), jobInfo.getJobGroup());


			//一次性任务执行后暂停
			if(!jobInfo.getCronJob()){
				sched.triggerJob(jobKey);
			}else{
				sched.triggerJob(jobKey);
			}

			log.info(">>>>> jobName = [" + getJobInfo.getJobName() + "]" + " scheduled and started now.");
			logService.insertLog(jobInfo.getJobId(), jobInfo.getJobName(), jobInfo.getJobGroup(), getJobInfo.getJobStatus(), "scheduled and started");

			return true;
        } catch (SchedulerException e) {
            log.error("Failed to start new job - {}", jobInfo.getJobName(), e);
			logService.insertLog(jobInfo.getJobId(), jobInfo.getJobName(), jobInfo.getJobGroup(), jobInfo.getJobStatus(), "Failed to scheduled and started:"+ e.getMessage());

			return false;
        }
    }

	@SuppressWarnings("deprecation")
	public void saveOrUpdate(ScheduleJob scheduleJob) throws Exception {
		if (!StringUtils.isEmpty(scheduleJob.getCronExpression())) {
			scheduleJob.setJobClass(scheduleJob.getJobClass());
			scheduleJob.setCronJob(true);
		} else {
			scheduleJob.setJobClass(scheduleJob.getJobClass());
			scheduleJob.setCronJob(false);
			scheduleJob.setRepeatTime((long) 1);
		}

		logService.insertLog(scheduleJob.getJobId(), scheduleJob.getJobName(), scheduleJob.getJobGroup(), scheduleJob.getJobStatus(), "created" );

		if (StringUtils.isEmpty(scheduleJob.getJobId())) {
			log.info("Job Info: {}", scheduleJob);
			scheduleNewJob(scheduleJob);
		} else {
			log.info("update Job Info: {}", scheduleJob);
			updateScheduleJob(scheduleJob);
		}
		scheduleJob.setRemark("i am job number " + scheduleJob.getJobId());
		scheduleJob.setInterfaceName("interface_" + scheduleJob.getJobId());
		log.info(">>>>> jobName = [" + scheduleJob.getJobName() + "]" + " created.");
	}

	@SuppressWarnings("unchecked")
	private void scheduleNewJob(ScheduleJob jobInfo) {
		try {
			Scheduler scheduler = schedulerFactoryBean.getScheduler();

			JobDetail jobDetail = JobBuilder
					.newJob((Class<? extends QuartzJobBean>) Class.forName(jobInfo.getJobClass()))
					.withIdentity(jobInfo.getJobName(), jobInfo.getJobGroup()).build();

			if (!scheduler.checkExists(jobDetail.getKey())) {

				jobDetail = scheduleCreator.createJob(
						(Class<? extends QuartzJobBean>) Class.forName(jobInfo.getJobClass()), false, context,
						jobInfo.getJobName(), jobInfo.getJobGroup());

				Trigger trigger;
				String note = "";
				if (jobInfo.getCronJob()) {
					trigger = scheduleCreator.createCronTrigger(jobInfo.getJobName(), new Date(),
							jobInfo.getCronExpression(), SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);

					note =  jobInfo.getJobName() + ",key: "+ trigger.getKey().toString()+
							", startTime: "+trigger.getStartTime()+", cron:"+ ((CronTrigger)trigger).getCronExpression()+
							", timeZone: "+ ((CronTrigger)trigger).getTimeZone();
					jobInfo.setJobStatus(JobStatusEnum.SCHEDULED.getValue());
				} else {
					trigger = scheduleCreator.createSimpleTrigger(jobInfo.getJobName(), new Date(),
							jobInfo.getRepeatTime(), SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);
					note =  jobInfo.getJobName() + ",key: "+ trigger.getKey().toString()+
							", startTime: "+trigger.getStartTime()+", repeat:"+ ((SimpleTrigger)trigger).getRepeatCount()+
							", every: "+ ((SimpleTrigger)trigger).getRepeatInterval()/1000 +" seconds";
					jobInfo.setJobStatus(JobStatusEnum.STARTED.getValue());
				}

				Matcher<JobKey> matcher = KeyMatcher.keyEquals(jobDetail.getKey());

				if(null != jobInfo.getHasListener() && jobInfo.getHasListener()){

					Class clazz = Class.forName(jobInfo.getListenerClass());

					try {
						JobListener listener =  (JobListener)clazz.newInstance();
						scheduler.getListenerManager().addJobListener(listener, matcher);


					}catch (Exception ex){
						log.info(">>>>> jobName = [" + jobInfo.getJobName() + "]" + " listener instance failed.");
						logService.insertLog(jobInfo.getJobId(), jobInfo.getJobName(), jobInfo.getJobGroup(), jobInfo.getJobStatus(), "listener instance failed." );
					}
				}

				scheduler.getListenerManager().addJobListener(allJobsListener, matcher);

				Date date = scheduler.scheduleJob(jobDetail, trigger);

				log.info("scheduleJob result {}", JSONObject.toJSONString(date));

				log.info("scheduleJob {}", date==null? "failed":"success");

				schedulerJobMapper.insert(jobInfo);

				log.info(">>>>> jobName = [" + jobInfo.getJobName() + "]" + " scheduled.");

				logService.insertLog(jobInfo.getJobId(), jobInfo.getJobName(), jobInfo.getJobGroup(), jobInfo.getJobStatus(), note );

			} else {
				log.error("scheduleNewJobRequest.jobAlreadyExist");
				logService.insertLog(jobInfo.getJobId(), jobInfo.getJobName(), jobInfo.getJobGroup(), jobInfo.getJobStatus(), "jobAlreadyExist" );

			}
		} catch (ClassNotFoundException e) {
			log.error("Class Not Found - {}, {}", jobInfo.getJobClass(), JSONObject.toJSONString(e));
			logService.insertLog(jobInfo.getJobId(), jobInfo.getJobName(), jobInfo.getJobGroup(), jobInfo.getJobStatus(), "Class Not Found :"+e.getMessage() );

		} catch (SchedulerException e) {
			log.error("scheduleNewJob SchedulerException: {}", JSONObject.toJSONString(e));

			logService.insertLog(jobInfo.getJobId(), jobInfo.getJobName(), jobInfo.getJobGroup(), jobInfo.getJobStatus(), "SchedulerException:"+e.getMessage() );

		}
	}

	private void updateScheduleJob(ScheduleJob jobInfo) {
		Trigger newTrigger;

		if (jobInfo.getCronJob()) {
			newTrigger = scheduleCreator.createCronTrigger(jobInfo.getJobName(), new Date(),
					jobInfo.getCronExpression(), SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);
		} else {
			newTrigger = scheduleCreator.createSimpleTrigger(jobInfo.getJobName(), new Date(), jobInfo.getRepeatTime(),
					SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);
		}

		//@todo test rescheduleJob listener
		try {
			Scheduler scheduler = schedulerFactoryBean.getScheduler();

			if(null != jobInfo.getHasListener() && jobInfo.getHasListener()){
				Matcher<JobKey> matcher = KeyMatcher.keyEquals(newTrigger.getJobKey());
				try {
					Class clazz = Class.forName(jobInfo.getListenerClass());
					JobListener listener =  (JobListener)clazz.newInstance();
					scheduler.getListenerManager().addJobListener(listener, matcher);
				}catch (Exception ex){
					log.info(">>>>> jobName = [" + jobInfo.getJobName() + "]" + " listener instance failed.");
					logService.insertLog(jobInfo.getJobId(), jobInfo.getJobName(), jobInfo.getJobGroup(), jobInfo.getJobStatus(), "listener instance failed." );
				}
			}

			Date date = scheduler.rescheduleJob(TriggerKey.triggerKey(jobInfo.getJobName()), newTrigger);

		 	log.info("rescheduleJob {}", date==null? "failed":"success");

			jobInfo.setJobStatus(JobStatusEnum.SCHEDULED.getValue());
			schedulerJobMapper.updateById(jobInfo);
			log.info(">>>>> jobName = [" + jobInfo.getJobName() + "]" + " updated and scheduled.");
			logService.insertLog(jobInfo.getJobId(), jobInfo.getJobName(), jobInfo.getJobGroup(), jobInfo.getJobStatus(), "updated and scheduled" );

		} catch (SchedulerException e) {
			log.error(e.getMessage(), e);
			logService.insertLog(jobInfo.getJobId(), jobInfo.getJobName(), jobInfo.getJobGroup(), jobInfo.getJobStatus(), "SchedulerException:"+e.getMessage() );
		}
	}


	public Page<ScheduleJob> selectPage(JobPageReqVo vo){
		Page<ScheduleJob> page = new Page<>(vo.getPageNo(), vo.getPageSize());

		String column = StrUtil.toUnderlineCase(vo.getSortColumn());
		OrderItem oi = new OrderItem(column, vo.getSortAsc());
		page.addOrder(oi);

		LambdaQueryWrapper<ScheduleJob> qw =  Wrappers.<ScheduleJob>lambdaQuery();

		if(!StringUtils.isEmpty(vo.getName())){
			qw.apply("job_name"+" ilike {0}", "%"+vo.getName().trim()+"%");
//			qw.like(ScheduleJob::getJobName, vo.getName());
		}

		if(!StringUtils.isEmpty(vo.getGroup())){
			qw.eq(ScheduleJob::getJobGroup, vo.getGroup());
		}

		if(!StringUtils.isEmpty(vo.getStartDate()) && !StringUtils.isEmpty(vo.getEndDate()) ){
			qw.between(ScheduleJob::getCreatedAt,
					Timestamp.valueOf(vo.getStartDate()+" 00:00:00.000"),
					Timestamp.valueOf(vo.getEndDate()+" 23:59:59.999"));
		}
		if(!StringUtils.isEmpty(vo.getStatus())){
			qw.eq(ScheduleJob::getJobStatus, vo.getStatus());
		}
		if(vo.getCronFlag() != null){
			qw.eq(ScheduleJob::getCronJob, vo.getCronFlag());
		}

		Page<ScheduleJob> pageResult = schedulerJobMapper.selectPage(page, qw);
		return pageResult;
	}
}
