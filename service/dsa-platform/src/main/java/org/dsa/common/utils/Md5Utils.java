package org.dsa.common.utils;

import ch.qos.logback.classic.Logger;
import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

public class Md5Utils {
    private static Logger logger = (Logger) LoggerFactory.getLogger(Md5Utils.class);

    public static String toMD5(String str) {
        logger.info("md5 inputStr:{}",str);
        byte[] secretBytes = null;
        try {
                secretBytes = MessageDigest.getInstance("md5").digest(str.getBytes());
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("没有md5这个算法！");
        }
        String md5code = new BigInteger(1, secretBytes).toString(16);// 16进制数字
        // 如果生成数字未满32位，需要前面补0
        for (int i = 0; i < 32 - md5code.length(); i++) {
                md5code = "0" + md5code;
        }
        logger.info("md5 Str:{}",md5code.toUpperCase());
        return md5code.toUpperCase();
    }

    public static String CompareVersionFileBySign(String vin, String versionNum, String versionType, String requestUser, String requestTime, String systemId) {
        StringBuffer sb = new StringBuffer();
        if(StringUtils.isNotEmpty(vin)){
            sb.append("vin=").append(vin).append(Constant.SECRET_JOIN);
        }
        if(StringUtils.isNotEmpty(versionNum)){
            sb.append("versionNum=").append(versionNum).append(Constant.SECRET_JOIN);
        }
        if(StringUtils.isNotEmpty(versionType)){
            sb.append("versionType=").append(versionType).append(Constant.SECRET_JOIN);
        }
        if(StringUtils.isNotEmpty(requestUser)){
            sb.append("requestUser=").append(requestUser).append(Constant.SECRET_JOIN);
        }
        if(StringUtils.isNotEmpty(requestTime)){
            sb.append("requestTime=").append(requestTime).append(Constant.SECRET_JOIN);
        }
        if(StringUtils.isNotEmpty(systemId)){
            sb.append("systemId=").append(systemId);
        }
        sb.append(Constant.SYSTEM_SECRET.getSecretBySystemId(systemId));
        return toMD5(sb.toString());
    }

    public static String getVehicleInfoBySign(String vin, String requestUser, String requestTime, String systemId) {
        StringBuffer sb = new StringBuffer();
        if(StringUtils.isNotEmpty(vin)){
            sb.append("vin=").append(vin).append(Constant.SECRET_JOIN);
        }
        if(StringUtils.isNotEmpty(requestUser)){
            sb.append("requestUser=").append(requestUser).append(Constant.SECRET_JOIN);
        }
        if(StringUtils.isNotEmpty(requestTime)){
            sb.append("requestTime=").append(requestTime).append(Constant.SECRET_JOIN);
        }
        if(StringUtils.isNotEmpty(systemId)){
            sb.append("systemId=").append(systemId);
        }
        sb.append(Constant.SYSTEM_SECRET.getSecretBySystemId(systemId));
        return toMD5(sb.toString());
    }

    public static void main(String[] args) {
        System.out.println(CompareVersionFileBySign("Vin12345678901234","","ODX","User001",String.valueOf(new Date().getTime()),"10000001"));
    }
}
