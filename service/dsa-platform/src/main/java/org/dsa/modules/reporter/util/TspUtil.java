package org.dsa.modules.reporter.util;

import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.Map;
import java.util.Set;

import static cn.hutool.core.util.RandomUtil.randomString;

public class TspUtil {

    public static String sign(String data, String key) {
        String encodeStr = "";

        try {
            Mac hmac = Mac.getInstance("HmacSHA1");
            SecretKeySpec secretKey = new SecretKeySpec(key.getBytes("utf-8"), "HmacSHA1");
            hmac.init(secretKey);
            byte[] hash = hmac.doFinal(data.getBytes("utf-8"));
            encodeStr = Base64.getEncoder().encodeToString(hash);
        }catch (Exception ex){

        }

        return encodeStr;
    }

    public static String aesEncrypt(byte[] dataBytes, String key) throws Exception {
        byte[] iv = randomString(16).getBytes();
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), "AES");
        cipher.init(Cipher.ENCRYPT_MODE, keyspec, new IvParameterSpec(iv));
        byte[] encrypted = cipher.doFinal(dataBytes);
        byte[] withIV = new byte[encrypted.length + iv.length];
        System.arraycopy(encrypted, 0, withIV, 0, encrypted.length);
        System.arraycopy(iv, 0, withIV, encrypted.length, iv.length);
        return Base64.getEncoder().encodeToString(withIV);
    }

    public static byte[] readStream(InputStream inputStream) throws IOException {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int num = inputStream.read(buffer);
            while (num != -1) {
                baos.write(buffer, 0, num);
                num = inputStream.read(buffer);
            }
            baos.flush();
            return baos.toByteArray();
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    public static HttpEntity buildMultipartForm(File file, String name, Map<String,String> postParam){
        //把文件转换成流对象FileBody
        FileBody fundFileBin = new FileBody(file);
        //设置传输参数
        MultipartEntityBuilder multipartEntity = MultipartEntityBuilder.create();
        multipartEntity.addPart(name, fundFileBin);
        //设计文件以外的参数
        Set<String> keySet = postParam.keySet();
        for (String key : keySet) {
            multipartEntity.addPart(key, new StringBody(postParam.get(key), ContentType.create("text/plain", Consts.UTF_8)));
        }
        HttpEntity entity =  multipartEntity.build();
        return entity;
    }


}
