package org.dsa.modules.onlineUpdate.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.dsa.modules.onlineUpdate.entity.ComponentVersionEntity;
import org.dsa.modules.onlineUpdate.entity.RuleEntity;

import java.util.List;

/**
 * 组件版本Vo
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ComponentVersionVo extends ComponentVersionEntity {

    /**
     * 组件名
     */
    private String componentName;
    /**
     * code
     */
    private Integer code;

    /**
     * 上级版本
     */
    private String baseVersion;

    /**
     * 文件名
     */
    public String fileName;
    /**
     * 组件关联
     */
    public List<CompVGroupVo> groups;

    /**
     * 禁用的组件关联（该软件的）
     */
    public List<CompVGroupVo> disabled;

    /**
     * 组件规则信息
     */
    private RuleEntity rule;

    /**
     * 安装优先级
     */
    private Integer priority;

    /**
     * 发布(发布状态不可修改)
     */
    private Integer publish;

}
