package org.dsa.modules.oss.service.impl;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.oss.dao.SysFileDao;
import org.dsa.modules.oss.dto.FileDto;
import org.dsa.modules.oss.entity.SysFileEntity;
import org.dsa.modules.oss.service.FileService;
import org.dsa.modules.oss.vo.file.FilePageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FileServiceImpl implements FileService {

    @Autowired
    SysFileDao fileDao;

    @Override
    public void record(FileDto dto) {
        SysFileEntity entity = new SysFileEntity();
        BeanUtil.copyProperties(dto, entity);
        fileDao.insert(entity);
    }

    @Override
    public Page<SysFileEntity> queryPage(FilePageVo vo) {

        Page<SysFileEntity> page = new Page<>(vo.getPageNo(), vo.getPageSize());
        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);

        LambdaQueryWrapper<SysFileEntity> qw = new LambdaQueryWrapper<>();

        if(StrUtil.isNotBlank(vo.getName())){
            qw.apply("name ilike {0} ", "%"+vo.getName()+"%");
        }

        if(StrUtil.isNotBlank(vo.getCategory())){
            qw.apply("category ilike {0} ", "%"+vo.getCategory()+"%");
        }

        Page<SysFileEntity> result = fileDao.selectPage(page, qw);

        return result;
    }

    @Override
    public SysFileEntity findOneFile(Long id) {
        return fileDao.selectById(id);
    }
}
