package org.dsa.modules.reporter.upgrade;

import lombok.Data;

@Data
public class ClientRepairDto extends ClientUpgradeDto {

    private String appName;
    private String appVersionName;
    private Integer appVersionNumber;
    private Integer appId;
    private String appType;
}
