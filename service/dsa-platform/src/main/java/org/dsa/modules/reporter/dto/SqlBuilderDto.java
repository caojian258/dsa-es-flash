package org.dsa.modules.reporter.dto;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class SqlBuilderDto implements Serializable {

    private String id;

    private String sql;

    private String condition;

    private Integer top;

    private String dateColumn;

    private String dateAttribute;

}
