package org.dsa.modules.reporter.event;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.dsa.common.utils.RedisUtils;
import org.dsa.config.SyncConfig;
import org.dsa.modules.reporter.constant.RedisConstant;
import org.dsa.modules.reporter.dto.OnlineUpdateFileDto;
import org.dsa.modules.reporter.dto.OnlineUpdateLogDto;
import org.dsa.modules.reporter.entity.FileLog;
import org.dsa.modules.reporter.enums.FileCateEnum;
import org.dsa.modules.reporter.handler.FileHandler;
import org.dsa.modules.reporter.handler.OnlineUpdateLogHandler;
import org.dsa.modules.reporter.service.FileLogService;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import jakarta.annotation.Resource;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.nio.file.StandardCopyOption.ATOMIC_MOVE;

/***
 * 处理在线升级文件 处理完的文件不会在工作目录的
 * 结束后根据返回的数据判断文件是正常解析还是异常解析存库
 * 这里只备份压缩包
 * 异常的文件对应的是特定文件
 * 这里要包含去重 相同文件压缩包 相同的sqlite文件过滤掉
 */
@Slf4j
@Component
public class OnlineUpdateListener implements ApplicationListener<OnlineUpdateEvent> {

    @Resource
    FileHandler fileHandler;

    @Resource
    OnlineUpdateLogHandler logHandler;

    @Resource
    RedisUtils redisUtils;

    @Resource
    FileLogService logService;

    @Resource
    SyncConfig syncConfig;

    @Async("asyncServiceExecutor")
    @Override
    public void onApplicationEvent(OnlineUpdateEvent onlineUpdateEvent) {
        log.info("start handler online update file: {},  tag: {}", onlineUpdateEvent.getName(), onlineUpdateEvent.getTag());
        OnlineUpdateLogDto logDto = new OnlineUpdateLogDto();
        String baseArchive = syncConfig.getOnlineUpdateArchiveDir();
        String baseFault = syncConfig.getOnlineUpdateFaultDir();

        SimpleDateFormat sdf = new SimpleDateFormat(syncConfig.getDirFormat());
        Date day = new Date();
        String dayStr = sdf.format(day);
        File faultDir = new File(baseFault + File.separator+dayStr);

        if(!faultDir.exists()){
            faultDir.mkdirs();
            faultDir.setReadable(true);
            faultDir.setWritable(true);
        }

        File archiveDir = new File(baseArchive + File.separator+dayStr);
        if(!archiveDir.exists()){
            archiveDir.mkdirs();
            archiveDir.setReadable(true);
            archiveDir.setWritable(true);
        }

        OnlineUpdateFileDto fileDto = new OnlineUpdateFileDto();

       FileLog fileLog =  logService.checkFile(FileCateEnum.OnlineUpdate.getValue(), onlineUpdateEvent.getName());
        if(null != fileLog){
            moveFaultFile(onlineUpdateEvent, faultDir);

            logService.failedLog(FileCateEnum.OnlineUpdate.getValue(),
                    getFaultFilePath(onlineUpdateEvent, faultDir), onlineUpdateEvent.getTag(),
                    onlineUpdateEvent.getName() + " has been processed successfully.");

            return;
        }

        try {
            String workDir = syncConfig.getOnlineUpdateWorkDir();
            //解压完成后记日志
            fileDto = fileHandler.decompressOnlineUpdate(onlineUpdateEvent.getPath(), workDir);

            //没有找到sqlite
            if(null == fileDto || null == fileDto.getPath()){

                moveFaultFile(onlineUpdateEvent, faultDir);

                logService.failedLog(FileCateEnum.OnlineUpdate.getValue(), getFaultFilePath(onlineUpdateEvent, faultDir),
                        onlineUpdateEvent.getTag(), onlineUpdateEvent.getName()+": empty zip");
                delFileFormWorkDir(fileDto);
                return;
            }

            if(StrUtil.isBlank(fileDto.getDeviceId())){
                moveFaultFile(onlineUpdateEvent, faultDir);
                logService.failedLog(FileCateEnum.OnlineUpdate.getValue(), getFaultFilePath(onlineUpdateEvent, faultDir),
                        onlineUpdateEvent.getTag(), onlineUpdateEvent.getName()+": can not get deviceId from name");
                delFileFormWorkDir(fileDto);
                return;
            }
        }catch (Exception ex){
            log.error("decompressOnlineUpdate error: {}", JSONObject.toJSONString(ex));
            //处理过程发生异常
            moveFaultFile(onlineUpdateEvent, faultDir);
            logService.failedLog(FileCateEnum.OnlineUpdate.getValue(), onlineUpdateEvent.getPath(),
                    onlineUpdateEvent.getTag(), onlineUpdateEvent.getName()+": "+ex.getMessage());

            delFileFormWorkDir(fileDto);
            return;
        }


        try {

            logDto =  logHandler.saveData(fileDto.getPath(), onlineUpdateEvent.getTag(), fileDto.getName(), fileDto.getSessionId(), fileDto.getDeviceId());
            redisUtils.lSet(RedisConstant.TAG_BASE_KEY, onlineUpdateEvent.getTag());

            //从上传目录到备份目录
            String archiveFile = archiveDir.getAbsolutePath() + File.separator + onlineUpdateEvent.getName();
            try {
                Files.move(Paths.get(onlineUpdateEvent.getPath()), Paths.get(archiveFile), ATOMIC_MOVE);
            }catch (Exception e){
                log.error("OnlineUpdate listener move archive error: {}", JSONObject.toJSONString(e));
            }

            logService.succeedLog(FileCateEnum.OnlineUpdate.getValue(), archiveFile,
                    onlineUpdateEvent.getTag(), "", onlineUpdateEvent.getName()+","+logDto.getSummary());

        } catch (Exception ex){
            log.error("OnlineUpdate listener error: {}", JSONObject.toJSONString(ex));
            logDto.setMsg(ex.getMessage());
            logService.failedLog(FileCateEnum.OnlineUpdate.getValue(), getFaultFilePath(onlineUpdateEvent, faultDir),
                    onlineUpdateEvent.getTag(), onlineUpdateEvent.getName()+":"+ex.getMessage());
            moveFaultFile(onlineUpdateEvent, faultDir);
        }

        //正常情况下清理工作目录
        delFileFormWorkDir(fileDto);
    }

    //从上传目录到异常目录
    protected void moveFaultFile(OnlineUpdateEvent onlineUpdateEvent, File faultDir){


        String faultFile = getFaultFilePath(onlineUpdateEvent, faultDir);

        try {
            Files.move(Paths.get(onlineUpdateEvent.getPath()), Paths.get(faultFile), ATOMIC_MOVE);
        }catch (Exception e){
            log.error("OnlineUpdate listener move fault error: {}", JSONObject.toJSONString(e));
        }
    }

    protected String getFaultFilePath(OnlineUpdateEvent onlineUpdateEvent, File faultDir){
        return faultDir.getAbsolutePath() + File.separator + onlineUpdateEvent.getName();
    }

    protected void delFileFormWorkDir(OnlineUpdateFileDto dto){
        try {
            File file = new File(dto.getDir());
            if(file.exists()){
                FileUtils.deleteDirectory(file);
            }
        }catch (Exception ex){
            log.error("OnlineUpdate listener delFileFormWorkDir error: {}", ex.getMessage());
        }
    }
}
