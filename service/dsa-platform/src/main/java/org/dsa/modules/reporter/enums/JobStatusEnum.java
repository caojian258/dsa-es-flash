package org.dsa.modules.reporter.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum JobStatusEnum {

    INIT("INIT"),
     PAUSED("PAUSED"),
    SCHEDULED("SCHEDULED"),
    STARTED("STARTED"),
    RUNNING("RUNNING"),
    EDITED("EDITED"),
    EXECUTED("EXECUTED"),
    VETOED("VETOED"),
    RESUMED("RESUMED");

    @EnumValue
    private final String value;

}
