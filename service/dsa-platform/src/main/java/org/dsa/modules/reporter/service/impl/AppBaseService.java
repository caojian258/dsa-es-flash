package org.dsa.modules.reporter.service.impl;


public class AppBaseService {

    public String getAppType(String name){
        String nameLowerCase = name.toLowerCase();
        String type = "";
        if("smartstart".equals(nameLowerCase)){
            type = "smartstart";
        }else if("wts".equals(nameLowerCase)){
            type = "runtime";
        }else{
            type = "application";
        }
        return type;
    }

}
