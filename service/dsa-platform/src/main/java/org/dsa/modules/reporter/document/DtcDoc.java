package org.dsa.modules.reporter.document;


import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
@EqualsAndHashCode(callSuper=false)
public class DtcDoc extends ElasticEntity {

    private String sessionInfoId;

    //1 首次 2尾次 3首次尾次
    private Integer recordFlag;

    private String sessionId;

    private String vin;

    //“001”=车载诊断；“002”=“离线诊断”；“003”=远程诊断（Reserved）
    private String diagSource;

    //0=reserved；1=预诊断；2=实时诊断
    private Integer diagCategory;

    private String workshopName;

    private Long workshopId;

    private Long userId;

    private String userName;

    private List<Long> vehicleTypeId;

    private String ecuName;

    private String dtcCode;

    private Map<String, Object> dtcCodeSuggest;

    private String dtcCodeHex;

    private Integer dtcCodeInt;

    private String description;

    private String descriptionTi;

    private String statusByteHex;

    private String status;

    private String statusTi;

    private String nrcHex;

    private String nrcDescription;

    private String nrcDescriptionTi;

    private Integer snapshotEnable;

    private Integer extendedDataEnable;

    private String errorCode;

    private String errorMessage;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date lastUpdateTime;
}
