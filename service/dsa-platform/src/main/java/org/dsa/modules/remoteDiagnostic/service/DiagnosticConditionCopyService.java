package org.dsa.modules.remoteDiagnostic.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticConditionCopyEntity;
import org.dsa.modules.remoteDiagnostic.vo.DiagnosticConditionPageVo;

import java.util.List;

public interface DiagnosticConditionCopyService {

    Page<DiagnosticConditionCopyEntity> selectPage(DiagnosticConditionPageVo vo);

    DiagnosticConditionCopyEntity getInfo(Long id);

    void saveCond(DiagnosticConditionCopyEntity cond);

    void updateCond(DiagnosticConditionCopyEntity cond);

    void inserts(Long taskId, List<DiagnosticConditionCopyEntity> list);

    // 获取所有的ecu且状态为可使用
    List<DiagnosticConditionCopyEntity> getConditions();

    // 获取所有的ecu且状态为可使用
    List<DiagnosticConditionCopyEntity> getConditions(Long taskId);

    // 检查ecu是否重复
    Boolean check(String conditionName);
}
