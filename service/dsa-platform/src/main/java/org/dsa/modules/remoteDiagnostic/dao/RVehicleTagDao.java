package org.dsa.modules.remoteDiagnostic.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.dsa.modules.remoteDiagnostic.entity.RVehicleTagEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleTagEntity;

import java.util.List;

@Mapper
public interface RVehicleTagDao extends BaseMapper<RVehicleTagEntity> {

    @Insert("<script>"
            + " insert into r_vehicle_tag (vin,tag_id) values "
            + " <foreach collection='tags' item='item' separator=','> "
            + " (#{vin},#{item}) "
            + " </foreach> "
            + " ON conflict(vin,tag_id)  DO NOTHING "
            + " </script>")
    void inserts(@Param("vin") String vin, @Param("tags") List<Long> tags);

    @Insert("<script>"
            + " insert into r_vehicle_tag (vin,tag_id) values "
            + " <foreach collection='vinList' item='vin' separator=',' > "
            + "   <foreach collection='tags' item='tid'  separator=',' > "
            + "    (#{vin},#{tid}) "
            + "   </foreach>"
            + " </foreach>"
            + " ON conflict(vin,tag_id)  DO NOTHING "
            + " </script>")
    void batchInserts(@Param("vinList") List<String> vinList, @Param("tags") List<Long> tags);


    @Insert("<script>"
            + " insert into r_vehicle_tag (vin,tag_id) values "
            + " <foreach collection='list' item='item' separator=','> "
            + " (#{item.vin},#{item.tagId}) "
            + " </foreach> "
            + " ON conflict(vin,tag_id)  DO NOTHING "
            + " </script>")
    void insertList(@Param("list") List<RVehicleTagEntity> cachedDataList);

    @Select(" select distinct i.vin from r_vehicle_tag i where i.vin = #{vin} and i.tag_id in (select j.tag_id from r_pool_depend j where j.pool_id = #{poolId} and j.tag_id is not null  )")
    List<String> getVinListByPool(@Param("poolId") Long poolId, @Param("vin") String vin);

    @Select(" select distinct i.vin from r_vehicle_tag i where  i.tag_id in (select j.tag_id from r_pool_depend j where j.pool_id = #{poolId} and j.tag_id is not null )")
    List<String> getVinListByPoolId(@Param("poolId") Long poolId);

    @Select("select i.tag_id from r_vehicle_tag i left join r_tag j on i.tag_id = j.id where vin = #{vin} and j.id is not null  ")
    List<Long> selectIdsByVin(@Param("vin") String vin);

    @Select("select j.* from r_vehicle_tag i left join r_tag j on i.tag_id = j.id where vin = #{vin} and j.id is not null")
    List<RemoteVehicleTagEntity> getVehicleTagList(String vin);
}
