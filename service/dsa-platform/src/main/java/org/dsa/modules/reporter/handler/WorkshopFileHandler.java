package org.dsa.modules.reporter.handler;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.dom4j.Element;
import org.dsa.modules.reporter.dao.WorkshopGroupMapper;
import org.dsa.modules.reporter.dao.WorkshopMapper;
import org.dsa.modules.reporter.dto.WorkshopFileDto;
import org.dsa.modules.reporter.entity.Workshop;
import org.dsa.modules.reporter.entity.WorkshopGroup;
import org.dsa.modules.reporter.enums.FileCateEnum;
import org.dsa.modules.reporter.enums.StateEnum;
import org.dsa.modules.reporter.service.FileLogService;
import org.dsa.modules.reporter.util.XmlReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.File;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Service
@Slf4j
public class WorkshopFileHandler{

    @Autowired
    WorkshopMapper mapper;

    @Autowired
    WorkshopGroupMapper groupMapper;
    @Autowired
    FileLogService logService;

    /***
     * 保存workshop信息到db
     * @param path
     */
    public WorkshopFileDto storeData(String path, String archiveFilePath) throws Exception {

        WorkshopFileDto dto = new WorkshopFileDto();
        dto.setInsertGroupTotal(new AtomicInteger(0));
        dto.setInsertTotal(new AtomicInteger(0));
        dto.setUpdateGroupTotal(new AtomicInteger(0));
        dto.setUpdateTotal(new AtomicInteger(0));

        File file = new File(path);
        // 判断文件是否存在 或 为空
        if (!file.exists() || file.length() == 0) {
            logService.failedLog(FileCateEnum.WORKSHOP.getValue(), archiveFilePath, null,"workshop file is null");
            return dto;
        }

        XmlReader reader = new XmlReader(path);

        List<Map<String, Object>> workshopList = reader.parse("workshops");

        if (!CollectionUtil.isEmpty(workshopList)) {
            dto.setWorkshopTotal(workshopList.size());
            for (Map<String, Object> shop : workshopList) {
                log.info(JSONObject.toJSONString(shop));
                // 提取方法 存入失败日志
                try {
                    analysisWorkshop(shop,dto);
                } catch (Exception e) {
                    logService.failedLog(FileCateEnum.WORKSHOP.getValue(), archiveFilePath, null, shop.toString() + ":[" + e.getMessage() + "]");
                }
            }
        } else {
            logService.failedLog(FileCateEnum.WORKSHOP.getValue(), archiveFilePath, null, "workshops is null");
        }

        List<Map<String, Object>> mapList = reader.parseCallback("workshop-groups", new XmlReader.XmlHandler() {
            @Override
            public List<Map<String, Object>> handle(Iterator<Element> ele) {
                List<Map<String, Object>> mapValues = new ArrayList<>();
                while (ele.hasNext()) {
                    Iterator igi = ((Element) (ele.next())).elementIterator();
                    while (igi.hasNext()) {
                        Element fooi = (Element) (igi.next());
                        Map<String, Object> nod = new HashMap<>();
                        if (null != fooi.elementText("name")) {
                            nod.put("name", fooi.elementText("name"));
                            nod.put("description", fooi.elementText("description"));
                        }
                        if (nod.size() > 0) {
                            mapValues.add(nod);
                        }
                    }
                }
                return mapValues;
            }
        });
        if (mapList != null && mapList.size() > 0) {
            dto.setWorkshopGroupTotal(mapList.size());
            for (Map<String, Object> m : mapList) {
                if (!StringUtils.isEmpty(m.get("name")) && !StringUtils.isEmpty(m.get("description"))) {
                    log.info(JSONObject.toJSONString(m));
                    try {
                        // 提取方法 存入失败日志
                        analysisWorkshopGroup(m, dto);
                    } catch (Exception e) {
                        logService.failedLog(FileCateEnum.WORKSHOP.getValue(), archiveFilePath, null, m.toString() + ":[" + e.getMessage() + "]");
                    }
                } else {
                    logService.failedLog(FileCateEnum.WORKSHOP.getValue(), archiveFilePath, null, " workshop group name or description is null");
                }
            }
        } else {
            logService.failedLog(FileCateEnum.WORKSHOP.getValue(), archiveFilePath, null, "workshop group  is null");
        }


//        dto.setWorkshopGroupTotal(mapList != null ? mapList.size() : 0);
        dto.setMsg("workshop total: " + dto.getWorkshopTotal() + ", workshop_group total: " + dto.getWorkshopGroupTotal()
                + ",WorkshopSuccessTotal: " + (dto.getUpdateTotal().get() + dto.getInsertTotal().get()) + ",WorkshopFailTotal: " + (dto.getWorkshopTotal() - dto.getUpdateTotal().get() - dto.getInsertTotal().get())
                + ",WorkshopGroupSuccessTotal: " + (dto.getInsertGroupTotal().get() + dto.getUpdateGroupTotal().get()) + ",WorkshopGroupFailTotal: " + (dto.getWorkshopGroupTotal() - dto.getUpdateGroupTotal().get() - dto.getInsertGroupTotal().get())
                + ",Processing Results:[ spend time: " + reader.getSpendTime()
                + ",insertWorkshopTotal:" + dto.getInsertTotal().get() + ",updateWorkshopTotal:" + dto.getUpdateTotal().get() +
                ",insertWorkshopGroupTotal:" + dto.getInsertGroupTotal().get() + ",updateWorkshopGroupTotal:" + dto.getUpdateGroupTotal().get() + "]");
        logService.succeedLog(FileCateEnum.WORKSHOP.getValue(), file.getName(), path, null, null, dto.getMsg());
        return dto;
    }

    private void analysisWorkshopGroup(Map<String, Object> m, WorkshopFileDto dto) throws Exception{
        WorkshopGroup g = null;
        QueryWrapper<WorkshopGroup> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", m.get("name") );
        queryWrapper.orderByAsc("id");

        g = groupMapper.selectOne(queryWrapper);

        if(null != g){
            g.setDescription((String) m.get("description"));
            g.setUpdatedAt(new Date());
            groupMapper.updateById(g);
            dto.getUpdateGroupTotal().incrementAndGet();
        }else{
            g = new WorkshopGroup();
            g.setName((String) m.get("name"));
            g.setDescription((String) m.get("description"));
            groupMapper.insert(g);
            dto.getInsertGroupTotal().incrementAndGet();
        }
    }

    private void analysisWorkshop(Map<String, Object> shop, WorkshopFileDto dto) throws Exception {
        if (StringUtils.isEmpty(shop.get("id")) || StringUtils.isEmpty(shop.get("name"))) {
            throw new RuntimeException(" workshop id or name is null");
        }

        Workshop workshop = null;
        Boolean exist = true;
        QueryWrapper<Workshop> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id", shop.get("id"));
        workshop = mapper.selectOne(queryWrapper);

        if (workshop == null) {
            exist = false;
            workshop = new Workshop();
        }
        workshop.setId((String) shop.get("id"));
        workshop.setName((String) shop.get("name"));

        if (!StringUtils.isEmpty(shop.get("address"))) {
            workshop.setAddress((String) shop.get("address"));
        }

        if (!StringUtils.isEmpty(shop.get("country"))) {
            workshop.setCountry((String) shop.get("country"));
        }

        if (!StringUtils.isEmpty(shop.get("contactName"))) {
            workshop.setContactName((String) shop.get("contactName"));
        }

        if (!StringUtils.isEmpty(shop.get("contactWay"))) {
            workshop.setContactWay((String) shop.get("contactWay"));
        }

        if (!StringUtils.isEmpty(shop.get("group"))) {
            workshop.setWorkshopGroup((String) shop.get("group"));
        }
        if (!StringUtils.isEmpty(shop.get("tag"))) {
            workshop.setTag((String) shop.get("tag"));
        }

        if (!StringUtils.isEmpty(shop.get("latitude")) && !StringUtils.isEmpty(shop.get("longitude"))) {
            workshop.setLatitude(Double.valueOf((String) shop.get("latitude")));
            workshop.setLongitude(Double.valueOf((String) shop.get("longitude")));
        }
        workshop.setState(StateEnum.ENABLE.getValue());
        if (exist) {
            workshop.setUpdatedAt(new Date());
            mapper.updateById(workshop);
            dto.getUpdateTotal().incrementAndGet();
        } else {
            mapper.insert(workshop);
            dto.getInsertTotal().incrementAndGet();

        }
    }

}
