package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.dsa.modules.reporter.dto.AppsDto;
import org.dsa.modules.reporter.entity.SystemHistory;

import java.util.List;

@Mapper
public interface SystemHistoryMapper extends BaseMapper<SystemHistory> {

    void saveByClientInfo(SystemHistory system);

    void save(SystemHistory system);

    AppsDto getApplicationsByTag(String tag);

    SystemHistory getOneItemByTag(String tag);

    List<String> getAllAntivirHistory();
}
