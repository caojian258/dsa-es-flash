package org.dsa.modules.reporter.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.reporter.dto.CtsDailyDto;
import org.dsa.modules.reporter.entity.CtsDaily;
import org.dsa.modules.reporter.vo.diag.DiagnosticPageReVo;

public interface CtsDailyService {

    Page<CtsDailyDto> selectPage(DiagnosticPageReVo vo);

    public void insertData(CtsDaily entity);

    public void upsertData(CtsDaily entity);


    public void deleteData(String vin, String day);
}
