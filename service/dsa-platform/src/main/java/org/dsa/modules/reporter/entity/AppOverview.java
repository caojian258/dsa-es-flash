package org.dsa.modules.reporter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
@TableName("client_application_overview")
public class AppOverview extends BaseEntity{

    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 经销商ID
     */
    private String workshop;

    /***
     * 电脑ID
     */
    private String pcid;

    /***
     * 软件类型 smartstart jre application
     */
    private String type;

    /***
     * 应用名称
     */
    private String name;

    /***
     * 应用版本
     */
    private String version;

    /***
     * 上一个版本
     */
    private String prevVersion;


    /***
     * 升级或者安装时间
     */
    private String upgradeTime;


    /***
     * 升级说明
     */
    private String upgradeNote;

    /***
     * 信息收集时间
     */
    private Date collectTime;


    private String collectDate;

    /***
     * 批处理tag
     */
    private String tag;

    private Integer appId;

    @TableField(exist=false)
    private boolean hasChildren;
}
