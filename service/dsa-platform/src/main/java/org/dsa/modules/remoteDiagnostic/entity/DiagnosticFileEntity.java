package org.dsa.modules.remoteDiagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;
import org.dsa.modules.remoteDiagnostic.pojo.BaseEntity;

import java.io.Serializable;

/**
 * ${comments}
 * 
 */
@Data
@ToString
@TableName("r_diag_file")
public class DiagnosticFileEntity extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@TableId(type = IdType.AUTO)
	private Long id;

	private String functionName;

	private String fileName;

	private String filePath;

	private String fileMd5;

	private Long fileLength;

	private String descriptionTi;

	private Integer status;

}
