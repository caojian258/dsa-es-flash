package org.dsa.modules.remoteDiagnostic.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.dsa.modules.remoteDiagnostic.dao.*;
import org.dsa.modules.remoteDiagnostic.entity.*;
import org.dsa.modules.remoteDiagnostic.service.RemoteVehicleEcuGroupService;
import org.dsa.modules.remoteDiagnostic.vo.CheckNameReqVo;
import org.dsa.modules.remoteDiagnostic.vo.PublicPageReqVo;
import org.dsa.modules.remoteDiagnostic.vo.Vehicle.VehicleTypeEcuGroupReqVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class RemoteVehicleEcuGroupServiceImpl implements RemoteVehicleEcuGroupService {

    @Autowired
    RemoteVehicleEcuGroupDao groupDao;
    @Autowired
    RemoteEcuGroupDao ecuGroupDao;
    @Autowired
    RemoteEcuIdentificationInfoDao identificationInfoDao;
    @Autowired
    RemoteVehicleTypeEcuGroupDao vehicleTypeEcuGroupDao;
    @Autowired
    private RemoteVehicleEcuImageDao ecuImageDao;
    @Autowired
    private RemotePoolDependDao poolDependDao;

    @Override
    public Page<RemoteVehicleEcuGroupEntity> selectPage(PublicPageReqVo vo) {
        Page<RemoteVehicleEcuGroupEntity> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);
        LambdaQueryWrapper<RemoteVehicleEcuGroupEntity> qw = Wrappers.<RemoteVehicleEcuGroupEntity>lambdaQuery();

        if (!StringUtils.isEmpty(vo.getName())) {
//            qw.like(VehicleEcuEntity::getEcuName, vo.getName().trim());
            qw.apply("group_name" + " ilike {0}", "%" + vo.getName().trim() + "%");
        }

        if (!StringUtils.isEmpty(vo.getStatus())) {
            qw.eq(RemoteVehicleEcuGroupEntity::getStatus, vo.getStatus());
        }
        Page<RemoteVehicleEcuGroupEntity> resultPage = groupDao.selectPage(page, qw);

        return resultPage;
    }

    @Override
    public RemoteVehicleEcuGroupEntity getInfo(Long id) {

        RemoteVehicleEcuGroupEntity vehicleEcuGroupEntity = groupDao.selectById(id);

        if (vehicleEcuGroupEntity != null) {
            vehicleEcuGroupEntity.setIdentifications(identificationInfoDao.selectList(Wrappers.<RemoteEcuIdentificationInfoEntity>lambdaQuery().eq(RemoteEcuIdentificationInfoEntity::getGroupId, id)));
            vehicleEcuGroupEntity.setImage(ecuImageDao.selectOne(Wrappers.<RemoteVehicleEcuImageEntity>lambdaQuery().eq(RemoteVehicleEcuImageEntity::getEcuGroupId, id)));
        }
        return vehicleEcuGroupEntity;
    }

    @Override
    @Transactional
    public void saveGroup(RemoteVehicleEcuGroupEntity group) throws IOException {
        groupDao.insert(group);
        if (group.getEcus() != null && group.getEcus().size() > 0) {
//            LambdaQueryWrapper<VehiclePlatformEcuEntity> qw = Wrappers.<VehiclePlatformEcuEntity>lambdaQuery();
//            qw.eq(VehiclePlatformEcuEntity::getPlatformId, platform.getId());
            //批量插入
            ecuGroupDao.inserts(group.getId(), group.getEcus());
            if (group.getIdentifications() != null && group.getIdentifications().size() > 0) {
                for (RemoteVehicleEcuEntity ecu : group.getEcus()) {
                    //批量插入
                    identificationInfoDao.insertEcuExists(ecu.getId(), group.getIdentifications());
                }
            }
        }
        if (group.getIdentifications() != null && group.getIdentifications().size() > 0) {
//            LambdaQueryWrapper<VehiclePlatformEcuEntity> qw = Wrappers.<VehiclePlatformEcuEntity>lambdaQuery();
//            qw.eq(VehiclePlatformEcuEntity::getPlatformId, platform.getId());
            //批量插入
            identificationInfoDao.inserts(group.getId(), null, null, group.getIdentifications());
        }

        if (group.getImage() != null) {
            RemoteVehicleEcuImageEntity image = group.getImage();
            image.setEcuGroupId(group.getId());
            if(!StringUtils.isEmpty(image.getFilePath())) image.setFileMd5(DigestUtils.md5DigestAsHex(new FileInputStream(new File(image.getFilePath()))));
            ecuImageDao.insert(image);
        }
    }

    @Override
    @Transactional
    public void updateGroup(RemoteVehicleEcuGroupEntity group) throws IOException {
        group.setUpdatedAt(new Date());

        groupDao.updateById(group);

        if (StringUtils.isEmpty(group.getEcuName())) {
            // 先删除
            ecuGroupDao.delete(Wrappers.<RemoteEcuGroupEntity>lambdaQuery().eq(RemoteEcuGroupEntity::getGroupId, group.getId()));
        } else {
            ecuGroupDao.deleteByName(group.getId(), group.getEcuName());
        }

        if (group.getEcus() != null && group.getEcus().size() > 0) {

            // 后插入
            ecuGroupDao.inserts(group.getId(), group.getEcus());
            if (group.getIdentifications() != null && group.getIdentifications().size() > 0) {
                for (RemoteVehicleEcuEntity ecu : group.getEcus()) {
                    //批量插入
                    identificationInfoDao.insertEcuExists(ecu.getId(), group.getIdentifications());
                }
            }
        }
        // 先删除
        identificationInfoDao.delete(Wrappers.<RemoteEcuIdentificationInfoEntity>lambdaQuery().eq(RemoteEcuIdentificationInfoEntity::getGroupId, group.getId()));
        if (group.getIdentifications() != null && group.getIdentifications().size() > 0) {

            // 后插入
            identificationInfoDao.inserts(group.getId(), null, null, group.getIdentifications());
        }

        if (group.getImage() != null) {
            RemoteVehicleEcuImageEntity image = ecuImageDao.selectOne(Wrappers.<RemoteVehicleEcuImageEntity>lambdaQuery().eq(RemoteVehicleEcuImageEntity::getEcuGroupId, group.getId()));
            if (image == null) image = new RemoteVehicleEcuImageEntity();
            image.setFileName(group.getImage().getFileName());
            image.setFilePath(group.getImage().getFilePath());
            image.setEcuGroupId(group.getId());

            if(!StringUtils.isEmpty(image.getFilePath()) ) image.setFileMd5(DigestUtils.md5DigestAsHex(new FileInputStream(new File(image.getFilePath()))));
            else image.setFileMd5(null);

            if (image.getId() == null) {
                ecuImageDao.insert(image);
            } else {
                image.setUpdatedAt(new Date());
                ecuImageDao.updateById(image);
            }
        }
    }

    @Override
    public List<RemoteVehicleEcuGroupEntity> getGroups() {
        return groupDao.selectGroups();
    }

    @Override
    public List<RemoteVehicleEcuGroupEntity> getGroups(Integer status) {
        return groupDao.selectList(Wrappers.<RemoteVehicleEcuGroupEntity>lambdaQuery().eq(status != null, RemoteVehicleEcuGroupEntity::getStatus, status));
    }

    @Override
    public List<RemoteVehicleEcuGroupEntity> getGroups(List<Long> ids) {
        return groupDao.getGroups(ids);
    }

    @Override
    public List<RemoteVehicleEcuGroupEntity> getGroups(List<Long> ids, Long versionId, Long typeId) {
        List<RemoteVehicleEcuGroupEntity> list = new ArrayList<>();
//        if (versionId != null) {
            list = groupDao.getGroupsByVersion(ids, typeId);
//        } else {
//            list = groupDao.getGroupsByVersion(ids, -1L);
//        }

//        if (list != null) {
//            for (RemoteVehicleEcuGroupEntity remoteVehicleEcuGroupEntity : list) {
//                if (remoteVehicleEcuGroupEntity.getVehicleEcuGroupVersion() == null) {
//                    remoteVehicleEcuGroupEntity.setVehicleEcuGroupVersion(new RemoteVehicleVersionEcuGroupEntity());
//                }
//            }
//        }

        return list;
    }

    @Override
    public Map<String, Object> getGroups(Long id) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("yList", groupDao.getYList(id));
        map.put("nList", groupDao.getNList(id));
        return map;
    }

    @Override
    public Boolean check(CheckNameReqVo vo) {
        LambdaQueryWrapper<RemoteVehicleEcuGroupEntity> qw = Wrappers.<RemoteVehicleEcuGroupEntity>lambdaQuery();
        qw.eq(RemoteVehicleEcuGroupEntity::getGroupName, vo.getName());
        if (vo.getId() != null) {
            qw.ne(RemoteVehicleEcuGroupEntity::getId, vo.getId());
        }
        qw.select(RemoteVehicleEcuGroupEntity::getId);
        return groupDao.selectOne(qw) == null;
    }

    @Override
    public List<RemoteVehicleEcuGroupEntity> getVehicleTypeEcuGroup(VehicleTypeEcuGroupReqVo vo) {

        return groupDao.selectTypeGroup(vo.getTypeId());
    }

    @Override
    @Transactional
    public void saveVehicleTypeEcuGroup(VehicleTypeEcuGroupReqVo vo) {
        vehicleTypeEcuGroupDao.inserts(vo.getTypeId(), vo.getGroupEntities());
    }

    @Override
    @Transactional
    public void updateVehicleTypeEcuGroup(VehicleTypeEcuGroupReqVo vo) {
        LambdaQueryWrapper<RemoteVehicleTypeEcuGroupEntity> qw = Wrappers.<RemoteVehicleTypeEcuGroupEntity>lambdaQuery();
        List<Long> groups = vo.getDelGroupEntities().stream().map(RemoteVehicleEcuGroupEntity::getId).collect(Collectors.toList());

        vehicleTypeEcuGroupDao.delete(Wrappers.<RemoteVehicleTypeEcuGroupEntity>lambdaQuery().eq(RemoteVehicleTypeEcuGroupEntity::getTypeId, vo.getTypeId()).in(RemoteVehicleTypeEcuGroupEntity::getEcuGroupId, groups));

        if (vo.getGroupEntities() != null && vo.getGroupEntities().size() != 0) {
            vehicleTypeEcuGroupDao.inserts(vo.getTypeId(), vo.getGroupEntities());
        }
    }

    @Override
    public void deleteVehicleTypeEcuGroup(VehicleTypeEcuGroupReqVo vo) {
        vehicleTypeEcuGroupDao.delete(Wrappers.<RemoteVehicleTypeEcuGroupEntity>lambdaQuery().eq(RemoteVehicleTypeEcuGroupEntity::getTypeId, vo.getTypeId()).eq(RemoteVehicleTypeEcuGroupEntity::getEcuGroupId, vo.getGroupEntities().get(0).getId()));

    }

    @Override
    public Page<RemoteVehicleEcuGroupEntity> selectEcuPage(PublicPageReqVo vo) {
        Page<RemoteVehicleEcuGroupEntity> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);

        if (!StringUtils.isEmpty(vo.getName())) {
            vo.setName(vo.getName().trim());
        }

        Page<RemoteVehicleEcuGroupEntity> resultPage = groupDao.selectEcuPage(page, vo.getName(), vo.getStatus());

        return resultPage;
    }

    @Override
    public void delDependByType(Long typeId) {
        vehicleTypeEcuGroupDao.delete(Wrappers.<RemoteVehicleTypeEcuGroupEntity>lambdaQuery().eq(RemoteVehicleTypeEcuGroupEntity::getTypeId, typeId));
        poolDependDao.delete(Wrappers.<RemotePoolDependEntity>lambdaQuery().eq(RemotePoolDependEntity::getVehicleTypeId, typeId));
    }

    @Override
    public List<RemoteVehicleTypeEcuGroupEntity> selectGroupByType(Long typeId) {
        return vehicleTypeEcuGroupDao.selectByTypeId(typeId);
    }
}
