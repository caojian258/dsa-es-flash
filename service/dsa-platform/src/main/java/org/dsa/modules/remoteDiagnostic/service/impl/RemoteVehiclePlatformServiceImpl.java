//package org.dsa.modules.remoteDiagnostic.service.impl;
//
//import cn.hutool.core.util.StrUtil;
//import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
//import com.baomidou.mybatisplus.core.metadata.OrderItem;
//import com.baomidou.mybatisplus.core.toolkit.Wrappers;
//import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
//import lombok.extern.slf4j.Slf4j;
//import org.dsa.modules.remoteDiagnostic.dao.RemoteVehicleEcuDao;
//import org.dsa.modules.remoteDiagnostic.dao.RemoteVehiclePlatformDao;
//import org.dsa.modules.remoteDiagnostic.dao.RemoteVehiclePlatformEcuDao;
//import org.dsa.modules.remoteDiagnostic.entity.RemoteVehiclePlatformEcuEntity;
//import org.dsa.modules.remoteDiagnostic.entity.RemoteVehiclePlatformEntity;
//import org.dsa.modules.remoteDiagnostic.service.RemoteVehiclePlatformService;
//import org.dsa.modules.remoteDiagnostic.vo.VehiclePlatformPageReVo;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.Date;
//import java.util.List;
//
//@Slf4j
//@Service
//public class RemoteVehiclePlatformServiceImpl implements RemoteVehiclePlatformService {
//    @Autowired
//    private RemoteVehiclePlatformDao platformDao;
//    @Autowired
//    private RemoteVehicleEcuDao ecuDao;
//    @Autowired
//    private RemoteVehiclePlatformEcuDao platformEcuDao;
//
//    @Override
//    public Page<RemoteVehiclePlatformEntity> selectPage(VehiclePlatformPageReVo vo) {
//        Page<RemoteVehiclePlatformEntity> page = new Page<>(vo.getPageNo(), vo.getPageSize());
//
//        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
//        OrderItem oi = new OrderItem(column, vo.getSortAsc());
//        page.addOrder(oi);
////        LambdaQueryWrapper<VehiclePlatformEntity> qw = Wrappers.<VehiclePlatformEntity>lambdaQuery();
//
////        if(!StringUtils.isEmpty(vo.getName())){
//////            qw.like(VehicleEcuEntity::getEcuName, vo.getName().trim());
////            qw.apply("ecu_name"+" ilike {0}", "%"+vo.getName().trim()+"%");
////        }
////
////        if(!StringUtils.isEmpty(vo.getStatus())){
////            qw.eq(VehiclePlatformEntity::getStatus, vo.getStatus());
////        }
//        Page<RemoteVehiclePlatformEntity> resultPage = platformDao.queryPage(page,vo.getName(),vo.getStatus(),vo.getVehicleTypeIds());
//
//        return resultPage;
//    }
//
//    @Override
//    public RemoteVehiclePlatformEntity getInfo(Long id) {
//        return platformDao.selectById(id);
//    }
//
//    @Override
//    @Transactional
//    public Long savePlatform(RemoteVehiclePlatformEntity platform) {
//        platformDao.insert(platform);
//        if (platform.getEcus() != null && platform.getEcus().size() > 0) {
////            LambdaQueryWrapper<VehiclePlatformEcuEntity> qw = Wrappers.<VehiclePlatformEcuEntity>lambdaQuery();
////            qw.eq(VehiclePlatformEcuEntity::getPlatformId, platform.getId());
//            //批量插入
//            platformEcuDao.inserts(platform.getId(), platform.getEcus());
//        }
//
//        return platform.getId();
//
//    }
//
//    @Override
//    @Transactional
//    public Long updatePlatform(RemoteVehiclePlatformEntity platform) {
//        platform.setUpdatedAt(new Date());
//
//        platformDao.updateById(platform);
//        if (platform.getEcus() != null && platform.getEcus().size() > 0) {
//            LambdaQueryWrapper<RemoteVehiclePlatformEcuEntity> qw = Wrappers.<RemoteVehiclePlatformEcuEntity>lambdaQuery();
//            qw.eq(RemoteVehiclePlatformEcuEntity::getPlatformId, platform.getId());
//            // 先删除
//            platformEcuDao.delete(qw);
//            // 后插入
//            platformEcuDao.inserts(platform.getId(), platform.getEcus());
//        }
//        return platform.getId();
//    }
//
//    @Override
//    public List<RemoteVehiclePlatformEntity> getList() {
//        return null;
//    }
//
//    @Override
//    public Boolean check(String name) {
//        LambdaQueryWrapper<RemoteVehiclePlatformEntity> qw = Wrappers.<RemoteVehiclePlatformEntity>lambdaQuery();
//        qw.eq(RemoteVehiclePlatformEntity::getName, name);
//        return platformDao.selectOne(qw) == null;
//    }
//}
