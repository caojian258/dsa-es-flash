package org.dsa.modules.reporter.enums;


import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AppTypeEnum {

    APP("application"),
    SMART_START("smartstart"),
    RUNTIME("runtime"),
    COMPONENT("component"),
    SOFTWARE("software");

    @EnumValue
    private final String value;
}
