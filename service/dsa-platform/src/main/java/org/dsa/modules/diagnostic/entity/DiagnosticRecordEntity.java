package org.dsa.modules.diagnostic.entity;

import lombok.Data;
import lombok.NonNull;

@Data
public class DiagnosticRecordEntity {

    /**
     * 会话Id
     */
    @NonNull
    private String sessionId;
    /**
     *功能码
     */
    @NonNull
    private String functionCode;
    /**
     *执行码
     */
    @NonNull
    private String actionCode;
    /**
     *状态标识
     */
    @NonNull
    private int ack;
    /**
     *数据来源的诊断仪
     */
    @NonNull
    private String source;
    /**
     *发生时间
     */
    @NonNull
    private String transitTime;
    /**
     *执行结果
     */
    @NonNull
    private String data;
}
