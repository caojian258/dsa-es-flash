package org.dsa.modules.onlineUpdate.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dsa.modules.onlineUpdate.entity.CompVGroupEntity;

import java.util.List;

/**
 * 级联选项
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CascadeOptions {
    private String label;
    private Long value;

    private Boolean disabled;

    private CompVGroupEntity dependency;

    private List<CascadeOptions> children;

    private CascadeOptions selected;
}
