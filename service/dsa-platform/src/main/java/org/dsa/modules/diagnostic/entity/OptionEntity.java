package org.dsa.modules.diagnostic.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * select Option 数据
 * 
 */
@Data
public class OptionEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	private Integer id;
	/**
	 * 显示值
	 */
	private String value;

}
