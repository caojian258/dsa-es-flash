package org.dsa.modules.reporter.vo.api;


import lombok.Data;
import org.dsa.modules.reporter.dto.FlashEcuVersionsDto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.util.List;

@Data
public class FlashLogReVo {

    @NotBlank(message = "签名不能为空")
    private String sign; //签名

    @NotNull(message = "模式不能为空")
    private Integer mode; //模式 1工程 2正式

    @NotBlank(message = "车架号不能为空")
    private String vin; // 车架号

    private String make;// 品牌

    @NotBlank(message = "车型不能为空")
    private String model;// 车型

    private String year;  // 年款牛创置空

    private String trimlevel;  // 牛创置空

    @NotBlank(message = "刷写前大版本不能为空")
    private String sourceVersion; // 刷写前大版本

    @NotBlank(message = "刷写后大版本不能为空")
    private String targetVersion; // 刷写后大版本

    @NotBlank(message = "会话ID不能为空")
    private String sessionId; // 会话ID

    @NotNull(message = "升级时间不能为空")
    private Long updateTime; //升级时间 格式:毫秒值

    //ecu list
    @NotNull(message = "ECU列表不能为空")
    List<FlashEcuVersionsDto> ecus;
}
