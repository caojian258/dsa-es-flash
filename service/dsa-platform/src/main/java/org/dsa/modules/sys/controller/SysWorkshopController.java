package org.dsa.modules.sys.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.R;
import org.dsa.modules.sys.entity.SysUserEntity;
import org.dsa.modules.sys.entity.SysWorkshopEntity;
import org.dsa.modules.sys.service.LdapService;
import org.dsa.modules.sys.service.SysUserService;
import org.dsa.modules.sys.service.SysWorkshopService;
import org.dsa.modules.sys.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;


/**
 * 部门管理
 *
 */
@RestController
@RequestMapping("/sys/workshop")
public class SysWorkshopController extends AbstractController {
	@Autowired
	private SysWorkshopService sysWorkshopService;
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private LdapService ldapService;
	/**
	 * 列表
	 */
	@RequestMapping("/list")
//	@RequiresPermissions("sys:workshop:list")
	public List<SysWorkshopEntity> list(){
		List<SysWorkshopEntity> workshopList = sysWorkshopService.queryList(new HashMap<String, Object>());
		for(SysWorkshopEntity sysWorkshopEntity : workshopList){
			SysWorkshopEntity parentWorkshopEntity = sysWorkshopService.getById(sysWorkshopEntity.getParentId());
			if(parentWorkshopEntity != null){
				sysWorkshopEntity.setParentName(parentWorkshopEntity.getName());
			}
		}
		return workshopList;
	}

	/**
	 * 选择部门(添加、修改菜单)
	 */
	@RequestMapping("/select")
	@RequiresPermissions("sys:workshop:select")
	public R select(){
		List<SysWorkshopEntity> workshopList = sysWorkshopService.queryList(new HashMap<String, Object>());

		//添加一级部门
		if(ShiroUtils.getUserName().equals(Constant.SUPER_ADMIN_USER_NAME)){
			SysWorkshopEntity root = new SysWorkshopEntity();
			root.setWorkshopId(0L);
			root.setName("DSA");
			root.setParentId(-1L);
			root.setOpen(true);
			workshopList.add(root);
		}

		return R.ok().put("workshopList", workshopList);
	}

	/**
	 * 上级部门Id(管理员则为0)
	 */
	@RequestMapping("/info")
//	@RequiresPermissions("sys:workshop:info")
	public R info(){
		long workshopId = 0;
		if(!ShiroUtils.getUserName().equals(Constant.SUPER_ADMIN_USER_NAME)){
			List<SysWorkshopEntity> workshopList = sysWorkshopService.queryList(new HashMap<String, Object>());
			Long parentId = null;
			for(SysWorkshopEntity sysWorkshopEntity : workshopList){
				if(parentId == null){
					parentId = sysWorkshopEntity.getParentId();
					continue;
				}

				if(parentId > sysWorkshopEntity.getParentId().longValue()){
					parentId = sysWorkshopEntity.getParentId();
				}
			}
			workshopId = parentId;
		}

		return R.ok().put("workshopId", workshopId);
	}
	@RequestMapping("/getName/{workshopId}")
//	@RequiresPermissions("sys:workshop:getName")
	public R queryWorkshopNameById(@PathVariable("workshopId") Long workshopId) {
		return R.ok().put("workshopName",sysWorkshopService.queryWorkshopNameById(workshopId));
	}
	
	/**
	 * 信息
	 */
	@RequestMapping("/info/{workshopId}")
//	@RequiresPermissions("sys:workshop:info")
	public R info(@PathVariable("workshopId") Long workshopId){
		SysWorkshopEntity workshop = sysWorkshopService.getById(workshopId);
		
		return R.ok().put("workshop", workshop);
	}
	
	/**
	 * 保存
	 */
	@RequestMapping("/save")
//	@RequiresPermissions("sys:workshop:save")
	public R save(@RequestBody SysWorkshopEntity workshop){

		sysWorkshopService.save(workshop);

//		String id = String.valueOf(workshop.getWorkshopId());
//		LdapWorkshopEntity ldapWorkshopEntity = new LdapWorkshopEntity();
//		ldapWorkshopEntity.setCn(id);
//		ldapWorkshopEntity.setSn(id);
//		ldapWorkshopEntity.setOu(id);
//		ldapWorkshopEntity.setDescription(workshop.getName());
//		ldapWorkshopEntity.setPostalAddress(workshop.getAddress());
//		ldapService.insertWorkshop(ldapWorkshopEntity);

		return R.ok();
	}
	
	/**
	 * 修改
	 */
	@RequestMapping("/update")
//	@RequiresPermissions("sys:workshop:update")
	public R update(@RequestBody SysWorkshopEntity workshop){

		sysWorkshopService.updateById(workshop);

//		String id = workshop.getWorkshopId().toString();
//		LdapWorkshopEntity ldapWorkshopEntity = new LdapWorkshopEntity();
//		ldapWorkshopEntity.setCn(id);
//		ldapWorkshopEntity.setSn(id);
//		ldapWorkshopEntity.setOu(id);
//		ldapWorkshopEntity.setDescription(workshop.getName());
//		ldapWorkshopEntity.setPostalAddress(workshop.getAddress());
//		ldapService.updateWorkshop(ldapWorkshopEntity);

		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@RequestMapping("/delete/{workshopId}")
//	@RequiresPermissions("sys:workshop:delete")
	public R delete(@PathVariable("workshopId") long workshopId){

		//判断是否有子部门
		List<Long> workshopList = sysWorkshopService.queryDetpIdList(workshopId);
		if(workshopList.size() > 0){
			return R.error("请先删除子经销商！");
		}
		/*
		sysWorkshopService.removeById(workshopId);
		*/
		// 校验是否有用户
		LambdaQueryWrapper<SysUserEntity> query = new LambdaQueryWrapper<SysUserEntity>().eq(SysUserEntity::getWorkshopId,workshopId);
		Long userCunt = sysUserService.getBaseMapper().selectCount(query);
		if(userCunt>0){
			return R.error("经销商下的存在用户,不可删除！");
		}

		sysWorkshopService.disableById(workshopId);

		return R.ok();
	}
}
