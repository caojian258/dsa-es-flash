package org.dsa.modules.diagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.diagnostic.entity.DVersionInfoEntity;

import java.util.List;

/**
 * 版本信息表
 */
@Mapper
public interface DVersionInfoDao extends BaseMapper<DVersionInfoEntity> {


    List<DVersionInfoEntity> selectByType(@Param("vehicleTypeId") Long vehicleTypeId,@Param("versionType") String versionType);

    List<DVersionInfoEntity> selectOpenVersionByType(@Param("vehicleTypeId")Long vehicleTypeId,@Param("versionType") String versionType);

    List<DVersionInfoEntity> selectOpenVersion(@Param("vehicleTypeId")Long vehicleTypeId,@Param("versionType") String versionType);

    DVersionInfoEntity selectOpenVersionByVersionNum(@Param("vehicleTypeId") Long vehicleTypeId, @Param("versionType") String versionType, @Param("versionNum")String versionNum);

    DVersionInfoEntity selectMaxOpenVersionByTypeId(@Param("vehicleTypeId")Long vehicleTypeId,@Param("versionType") String versionType);

    DVersionInfoEntity selectMinOpenVersionByTypeId(@Param("vehicleTypeId")Long vehicleTypeId,@Param("versionType") String versionType);

    String selectParentVersionNumByType(@Param("vehicleTypeId") Long vehicleTypeId,@Param("versionType") String versionType);

    Long insertDVersionInfo( DVersionInfoEntity versionEntity);

    int selectVersions(@Param("vehicleTypeId") Long vehicleTypeId,@Param("versionType") String versionType);


    void updateFullVersionDownloadFile(@Param("id") Long id,@Param("vehicleTypeId")  Long vehicleTypeId
            ,@Param("versionType")  String versionType,@Param("versionFileName")  String versionFileName
            ,@Param("versionFileMd5")  String versionFileMd5);

    void updateVersionDownloadFile(Long id, String fileName, String md5);

    void updateParentVersionDownloadFile(DVersionInfoEntity dVersionInfoEntity);

    void updateVersionDownloadFileById(DVersionInfoEntity dVersionInfoEntity);

    Integer selectTargetTypeByTypeId(@Param("id") Long id,@Param("vehicleTypeId")  Long vehicleTypeId
            ,@Param("versionType")  String versionType);
}
