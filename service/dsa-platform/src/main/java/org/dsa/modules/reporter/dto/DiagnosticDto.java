package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class DiagnosticDto implements Serializable {

    private String vinCode;

    private String workshop;

    private String pcid;

    private Integer userid;

    private String username;

    private String sessionId;

    private String functionCode;

    private String actionCode;

    private String titleEn;

    private String titleCn;

    private Date startTime;

    private Date endTime;

    private int resultFlag;

    private String resultMessageEn;

    private String resultMessageCn;

    private String source;

    private String tag;

    private Date maxTime;

    private Date minTime;
}
