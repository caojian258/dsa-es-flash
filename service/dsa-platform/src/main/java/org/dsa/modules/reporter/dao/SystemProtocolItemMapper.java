package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.entity.SystemProtocolItem;

import java.util.List;

@Mapper
public interface SystemProtocolItemMapper extends BaseMapper<SystemProtocolItem> {

    public List<SystemProtocolItem> getGroupList(@Param("pcid") String pcid, @Param("startDate") String startDate, @Param("endDate") String endDate);
}
