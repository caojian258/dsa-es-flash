package org.dsa.modules.remoteDiagnostic.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.ZipUtil;
import com.alibaba.fastjson.JSONObject;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.dsa.common.utils.AESUtils;
import org.dsa.config.SyncConfig;
import org.dsa.modules.onlineUpdate.rule.RuleComponent;
import org.dsa.modules.remoteDiagnostic.constant.FlashConstant;
import org.dsa.modules.remoteDiagnostic.dao.*;
import org.dsa.modules.remoteDiagnostic.dto.flash.*;
import org.dsa.modules.remoteDiagnostic.entity.*;
import org.dsa.modules.remoteDiagnostic.service.EcuFlashService;
import org.dsa.modules.remoteDiagnostic.vo.Flash.FlashFileVo;
import org.dsa.modules.sys.service.SysConfigService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class EcuFlashServiceImpl implements EcuFlashService {

    @Resource
    SysConfigService sysConfigService;

    @Resource
    RemoteVehicleVersionDao vehicleVersionDao;

    @Resource
    RemoteVehicleEcuDao remoteVehicleEcuDao;

    @Resource
    RemoteEcuVersionDao remoteEcuVersionDao;

    @Resource
    RemoteEcuIdentificationInfoDao identificationInfoDao;

    @Resource
    RemoteVehicleEcuVersionFileDao ecuVersionFileDao;

    @Value("${file.ecu.download}")
    private String ecuFlashDownload;

    @Value("${project.url}")
    private String domainUrl;

    @Resource
    SyncConfig syncConfig;

    @Resource
    RemoteVersionEcuRelyDao ecuRelyDao;

    @Resource
    RuleComponent ruleComponent;

    @Override
    public Integer checkEcuVersionList(List<EcuDIDDto> ecuList) {
        //@todo 0 正常 1 关键ecu版本存在有ecu版本不存在 2 版本都不存在 3 关键版本不存在
        Integer flag = 0;
        //这里的关键ECU是VDC
        String keyEcu = sysConfigService.getValue(FlashConstant.KEY_ECU);
        String partNumber = sysConfigService.getValue(FlashConstant.EPN);
        if(CollectionUtil.isEmpty(ecuList)){
            flag = 2;
            return flag;
        }
        Map<String, String> versions = new HashMap<>();
        String ecuVersionDid =  sysConfigService.getValue(FlashConstant.ESV);
        for (EcuDIDDto ecuDto: ecuList) {
            List<EcuDIDItemDto> didList = ecuDto.getDidList();
            if(CollectionUtil.isNotEmpty(didList)){
                for (EcuDIDItemDto did: didList){
                    if(did.getDid().equals(ecuVersionDid) && StrUtil.isNotBlank(did.getDidValue()) ){
                        versions.put(ecuDto.getEcuName(), did.getDidValue());
                    }
                }
            }
        }
        //ecu 版本都为空
        if(CollectionUtil.isEmpty(versions)){
            flag = 2;
            return flag;
        }
        //关键ecu不存在
        if(StrUtil.isEmpty(versions.get(keyEcu))){
            flag = 3;
            return flag;
        }
        // 存在白件
        if(versions.size()<ecuList.size()){
            flag = 1;
            return flag;
        }
        return flag;
    }

    @Override
    public List<EcuVersionDto> buildEcuVersionListByVehicleVersion(Long versionId) {
        List<EcuVersionDto> ss =  findVehicleEcuVersionByVehicleVersion(versionId);
        log.info("buildEcuVersionListByVehicleVersion EcuVersionDto: {}", JSONObject.toJSONString(ss));
        List<Long> ecuVersionIdList = new ArrayList<>();

        for (EcuVersionDto evd: ss) {
            ecuVersionIdList.add(evd.getId());
        }

        List<RemoteVersionEcuRelyEntity> ecuVersionDependencyList = null;

        if(ecuVersionIdList.size()>0){
            ecuVersionDependencyList = findEcuVersionDependency(ecuVersionIdList);
        }

        Integer dependencyFlag = filterEcuVersionId(ecuVersionDependencyList, ecuVersionIdList);

        List<EcuVersionDto> ecuVersionDtoList = null;

        if(dependencyFlag == 0){
            ecuVersionDtoList = ss;
        }else if(dependencyFlag == 1){
            //找到所有的ECU 版本ID
            List<Long> ecuVersionIds = ecuVersionDependencyList.stream().map(i -> i.getEcuVersionId()).collect(Collectors.toList());
            ecuVersionDtoList = findEcuVersionByDependencyEcuVersionId(ecuVersionIds);
        }else{
            //大部分情况下 只有部分版本存在依赖
            List<Long> ecuVersionIds = ecuVersionDependencyList.stream().map(i -> i.getEcuVersionId()).collect(Collectors.toList());
            //这个数组的顺序不变 把 ss中存在在这里的item 删除掉 然后把 这里面的数据加入到ss的尾部
            List<EcuVersionDto> dependencyEcuVersion = findEcuVersionByDependencyEcuVersionId(ecuVersionIds);
            ecuVersionDtoList = ss;
            for (EcuVersionDto versionDto: ss) {
                if(dependencyEcuVersion.contains(versionDto)){
                    ecuVersionDtoList.remove(versionDto);
                }
            }
            ecuVersionDtoList.addAll(dependencyEcuVersion);
        }

        return ecuVersionDtoList;
    }

    @Override
    public List<EcuVersionDto> findEcuVersionByDependencyEcuVersionId(List<Long> versionList) {
        return remoteEcuVersionDao.findEcuVersionDependencyByIds(versionList);
    }

    @Override
    public List<RemoteVersionEcuRelyEntity> findEcuVersionDependency(List<Long> versionList) {
        return ecuRelyDao.findEcuVersionDependencyList(versionList);
    }

    @Override
    public Boolean checkExchange(Map<String, VersionDto> versionMap, List<EcuDIDDto> ecuList) {
        //VDC
        String keyEcu = sysConfigService.getValue(FlashConstant.KEY_ECU);
        Boolean flag = false;
        if(versionMap.size() < ecuList.size()){
            flag = true;
        }
        return flag;
    }

    @Override
    public String searchVehicleVersion(List<EcuDIDDto> ecuList) {
        //整车版本 F1AA
        String didKey = sysConfigService.getValue(FlashConstant.VSV);
        String version = null;
        outer: for (EcuDIDDto item: ecuList) {
            if(CollectionUtil.isNotEmpty(item.getDidList())){
                for (EcuDIDItemDto didItem: item.getDidList()) {
                    if(didItem.getDid().equals(didKey)){
                        version = didItem.getDidValue();
                        break outer;
                    }
                }
            }
        }
        return version;
    }

    @Override
    public Map<String, VersionDto> searchEcuVersion(List<EcuDIDDto> ecuList) {
        //软件号 F189
        String sKey = sysConfigService.getValue(FlashConstant.ESV);
        //零件号 F187
        String pKey = sysConfigService.getValue(FlashConstant.EPN);
        if(StrUtil.isEmpty(sKey) || StrUtil.isEmpty(pKey)){
            return null;
        }

        Map<String, VersionDto> ecuVersionMap = new HashMap<>();
        //ECU软件号 (前12位）+ ECU软件版本（后4位）：F189
        for (EcuDIDDto item: ecuList) {
            if(CollectionUtil.isNotEmpty(item.getDidList())){
                VersionDto version  = new VersionDto();

                for (EcuDIDItemDto didItem: item.getDidList()) {
                    if(didItem.getDid().equals(pKey) && StrUtil.isNotBlank(didItem.getDidValue())){
                        version.setPartNumber(didItem.getDidValue());
                    }
                    if(didItem.getDid().equals(sKey) && StrUtil.isNotBlank(didItem.getDidValue())){
                        String value = "";
                        if(didItem.getDidValue().length()>3){
                            value = didItem.getDidValue().substring(4);
                        }else{
                            value = didItem.getDidValue();
                        }
                        version.setSoftwareVersion(value);
                    }
                }

                //根据零件号和软件版本获取ecu版本
                if(StrUtil.isNotBlank(version.getPartNumber()) && StrUtil.isNotBlank(version.getSoftwareVersion())){
                    RemoteVehicleEcuVersionEntity rvev = remoteEcuVersionDao.findEcuVersionByDid(pKey, version.getPartNumber(), version.getSoftwareVersion());
                    if(rvev != null){
                        version.setVersionId(rvev.getId());
                    }
                }
                ecuVersionMap.put(item.getEcuName(), version);
            }
        }

        return ecuVersionMap;
    }

    @Override
    public RemoteVehicleVersionEntity findVehicleTargetVersion(Long vehicleTypeId, Long versionId) {
        RemoteVehicleVersionEntity entity = null;

        List<RemoteVehicleVersionEntity> entityList =  vehicleVersionDao.findTargetVersion(vehicleTypeId, versionId);

        //找到强制的不可跳过的版本
        List<RemoteVehicleVersionEntity> mandatoryOptionalList = new ArrayList<>();
        List<RemoteVehicleVersionEntity> crossList = new ArrayList<>();
        for (RemoteVehicleVersionEntity ve: entityList){
            if(ve.getIsCross() == 1){
                mandatoryOptionalList.add(ve);
            }else{
                crossList.add(ve);
            }
        }

        if(mandatoryOptionalList.size()>0){
            for (RemoteVehicleVersionEntity ve: entityList){
                if(ve.getVersionType().equals(FlashConstant.MANDATORY)){
                    entity = ve;
                    break;
                }
            }
        }


        //强制的不可跨过的没有就找可选的最后的一个记录
        if(null == entity){
            List<RemoteVehicleVersionEntity> optionalList = new ArrayList<>();
            for (RemoteVehicleVersionEntity ve: mandatoryOptionalList){
                if(ve.getVersionType().equals(FlashConstant.OPTIONAL)){
                    entity = ve;
                    break;
                }
            }
        }

        //不可跨过的没有找到，就从可跨过的选择最新的版本
        if(null == entity){
            //先找可以跨过 强制的第一个版本，没有的话找可以跨过 非强制最后一个版本
            for (RemoteVehicleVersionEntity ve: crossList){
                if(ve.getVersionType().equals(FlashConstant.MANDATORY)){
                    entity = ve;
                    break;
                }
            }

            if( null != entity){
                entity = crossList.get(crossList.size()-1);
            }
        }
        return entity;
    }

    /***
     * 获取整车强制升级列表
     * @param vehicleTypeId
     * @param versionId
     * @return
     */
    @Override
    public List<RemoteVehicleVersionEntity> findVehicleTargetVersions(Long vehicleTypeId, Long versionId) {
        List<RemoteVehicleVersionEntity> targetVersion = new ArrayList<>();
        //按照顺序获取车辆版本
        List<RemoteVehicleVersionEntity> entityList =  vehicleVersionDao.findTargetVersion(vehicleTypeId, versionId);
        if(CollectionUtil.isEmpty(entityList)){
            return targetVersion;
        }

        //对诊断仪来说 强制和不可跳过是或的关系 都是强制升级
        for (RemoteVehicleVersionEntity entity: entityList ) {
            if(entity.getVersionType().equals(FlashConstant.MANDATORY) || entity.getIsCross().equals(FlashConstant.NO_CROSS)){
                entity.setFlashFlag(FlashConstant.MUST_FLASH);
                targetVersion.add(entity);
            }
        }
        //如果没有强制和不可跳过的整车版本获取最新的一个版本
        if(CollectionUtil.isEmpty(targetVersion)){
            targetVersion.add(entityList.get(entityList.size()-1));
        }
        return targetVersion;
    }

    @Override
    public RemoteVehicleVersionEntity findVehicleTargetVersionByPool(List<RemoteVehicleVersionEntity> versions, Map<String, Object> filters) {
        RemoteVehicleVersionEntity targetVersion = null;
        for (RemoteVehicleVersionEntity entity: versions ) {
            boolean result = true;
            if(null != entity.getPoolId()){
                RemoteVehiclePoolEntity pool = new RemoteVehiclePoolEntity();
                pool.setId(entity.getPoolId());
                result =  ruleComponent.checkPoolByFilter(pool, filters);
            }
            if(result){
                targetVersion = entity;
                break;
            }
        }
        return targetVersion;
    }

    /***
     * @logci
     * 1. 首先拿到ecu version id列表
     * 2. 查询ecu版本依赖关系
     *    如果不存在依赖关系 就按照 车辆版本关联顺序返回给客户端ecu版本列表 0
     *    存在依赖并且所有的ecu版本都在依赖关系中，即按照依赖顺序返回给客户端的ecu版本列表 1
     *    存在依赖不是所有ecu版本在依赖关系中，这时对查出来的ecu版本进行排序 2 常见情况
     */
    @Override
    public List<EcuDIDDto> ecuUpdateData(RemoteVehicleVersionEntity version, String vin) {

        List<EcuDIDDto> ecuDIDs = new ArrayList<>();

        List<EcuVersionDto> ecuVersionDtoList = buildEcuVersionListByVehicleVersion(version.getId());

        log.info("ecuUpdateData ecuVersionDtoList:{}", JSONObject.toJSONString(ecuVersionDtoList));
        for (EcuVersionDto dto: ecuVersionDtoList) {

            EcuDIDDto didDto = new EcuDIDDto();
            didDto.setEcuVersion(dto.getVersionName());
            didDto.setEcuName(dto.getEcuName());
            didDto.setVersionType(dto.getVersionType());
            didDto.setIsCross(dto.getIsCross());

            //@todo 构造didList
            Map<String, RemoteEcuIdentificationInfoEntity> didMaps =  findDidByEcuVersionId(dto.getId(), FlashConstant.VERSION_TARGET);
            List<EcuDIDItemDto> didList = new ArrayList<>();
            if(CollectionUtil.isNotEmpty(didMaps)){
                for (RemoteEcuIdentificationInfoEntity identification: didMaps.values() ) {
                    EcuDIDItemDto didItemDto = new EcuDIDItemDto();
                    didItemDto.setDid(identification.getDid());
                    didItemDto.setDidName(identification.getDidName());
                    didItemDto.setDidValue(identification.getDidVal());
                    didItemDto.setOptional(identification.getStatus());
                    didList.add(didItemDto);
                }
            }

            didDto.setDidList(didList);

            List<Long> ecuVersionId = new ArrayList<>();
            ecuVersionId.add(dto.getId());

            List<EcuVersionFileDto> files = findEcuVersionFiles(ecuVersionId);
            List<FlashFileVo> fileVos = new ArrayList<>();
            log.info("{} flash files: {}", dto.getEcuName(),JSONObject.toJSONString(files));

            for (EcuVersionFileDto d : files) {
                FlashFileVo vo = buildFlashFile(d);
                fileVos.add(vo);
            }

            // @todo 刷写文件列表
            String downloadName = "";
            String downloadUrl = "";
            Long  downloadSize = 0L;
            String downloadMd5 = "";

            if(CollectionUtil.isNotEmpty(files)){
                Map<String, Object> zipMap = buildFlashDownloadFile(didDto.getEcuName(), didDto.getEcuVersion(), files, vin);
                downloadName = zipMap.get("name").toString();
                downloadUrl = zipMap.get("url").toString();
                downloadSize = (Long) zipMap.get("size");
                downloadMd5 = zipMap.get("md5").toString();
            }
            didDto.setFileDownloadName(downloadName);
            didDto.setFileDownloadUrl(downloadUrl);
            didDto.setFileDownloadSize(downloadSize);
            didDto.setFileDownloadMd5(downloadMd5);

            didDto.setFlashFiles(fileVos);

            ecuDIDs.add(didDto);
        }
        return ecuDIDs;
    }

    @Override
    public RemoteVehicleVersionEntity findVehicleVersion(Long vehicleTypeId, String version) {
        return vehicleVersionDao.findVersion(vehicleTypeId, version);
    }

    @Override
    public Integer checkTargetVersion(Long vehicleTypeId, String version) {
        return vehicleVersionDao.checkTargetVersion(vehicleTypeId, version);
    }

    /***
     * 这里判断只要有一个ecu的did的值跟对应车辆版本下ecu的did不一致就提示要对齐
     * @param version
     * @param ecuList
     * @return
     */
    @Override
    public Integer checkAlign(RemoteVehicleVersionEntity version, List<EcuDIDDto> ecuList) {

        List<EcuVersionDto> ecuVersionDtoList = buildEcuVersionListByVehicleVersion(version.getId());

        log.info("ecu versions:{}", JSONObject.toJSONString(ecuVersionDtoList));

        Map<String, Map<String, String>> eucDIDs = new HashMap<>();

        for (EcuVersionDto dto: ecuVersionDtoList) {
            Map<String, String> didMap =  findDidMapByEcuVersionId(dto.getId(), FlashConstant.VERSION_TARGET);
            eucDIDs.put(dto.getEcuName(), didMap);
        }

        log.info("ecu dids:{}", JSONObject.toJSONString(eucDIDs));
        Integer needAlign = FlashConstant.NO_VERSION;

        outer: for (EcuDIDDto ecuDIDDto: ecuList){
            String ecu = ecuDIDDto.getEcuName();
            Map<String, String> recordDids = eucDIDs.get(ecu);
            if(CollectionUtil.isNotEmpty(recordDids) && CollectionUtil.isNotEmpty(ecuDIDDto.getDidList())){
                for (EcuDIDItemDto didItem: ecuDIDDto.getDidList()) {
                    if(null != recordDids.get(didItem.getDid()) &&
                            !didItem.getDidValue().equals(recordDids.get(didItem.getDid()))){
                        needAlign = FlashConstant.ALIGN_VERSION;
                    }
                    break outer;
                }
            }
        }

        return needAlign;
    }

    @Override
    public List<RemoteVehicleEcuEntity> findVehicleEcuByVehicleVersion(Long versionId) {
        return remoteVehicleEcuDao.findVehicleEcuByVehicleVersion(versionId);
    }

    @Override
    public List<EcuVersionDto> findVehicleEcuVersionByVehicleVersion(Long versionId) {
        return remoteEcuVersionDao.findEcuVersionByVehicleVersion(versionId);
    }

    @Override
    public Map<String, String> findDidMapByEcuVersionId(Long ecuId, String group) {
        Map<String, String> map = new HashMap<>();
        List<RemoteEcuIdentificationInfoEntity> didList =  identificationInfoDao.findEcuIdentification(ecuId, group);
        if(CollectionUtil.isEmpty(didList)){
            return null;
        }

        for (RemoteEcuIdentificationInfoEntity entity: didList){
            map.put(entity.getDid(), entity.getDidVal());
        }
        return map;
    }

    @Override
    public Map<String, RemoteEcuIdentificationInfoEntity> findDidByEcuVersionId(Long ecuId, String group) {
        Map<String, RemoteEcuIdentificationInfoEntity> map = new HashMap<>();
        List<RemoteEcuIdentificationInfoEntity> didList =  identificationInfoDao.findEcuIdentification(ecuId, group);
        if(CollectionUtil.isEmpty(didList)){
            return null;
        }

        for (RemoteEcuIdentificationInfoEntity entity: didList){
            map.put(entity.getDid(), entity);
        }
        return map;
    }

    @Override
    public List<EcuDIDDto> ecuAlignData(RemoteVehicleVersionEntity version, List<EcuDIDDto> ecuList, String vin) {

        List<EcuVersionDto> ecuVersionDtoList = buildEcuVersionListByVehicleVersion(version.getId());

        Map<String, Map<String, RemoteEcuIdentificationInfoEntity>> eucDIDs = new HashMap<>();

        List<EcuDIDDto> ecuDIDs = new ArrayList<>();
        Map<String, String> ecuVersionTypeMap = new HashMap<>();
        Map<String, String> ecuVersionMap = new HashMap<>();
        Map<String, Integer> ecuCrossMap = new HashMap<>();
        Map<String, Long> ecuVersionIdMap = new HashMap<>();

        for (EcuVersionDto dto: ecuVersionDtoList) {
            ecuVersionTypeMap.put(dto.getEcuName(), dto.getVersionType());
            ecuVersionMap.put(dto.getEcuName(), dto.getVersionName());
            ecuCrossMap.put(dto.getEcuName(), dto.getIsCross());
            ecuVersionIdMap.put(dto.getEcuName(), dto.getId());

            Map<String, RemoteEcuIdentificationInfoEntity> didMaps =  findDidByEcuVersionId(dto.getId(), FlashConstant.VERSION_TARGET);
            eucDIDs.put(dto.getEcuName(), didMaps);
        }

        for (EcuDIDDto ecuDIDDto: ecuList){
            String ecu = ecuDIDDto.getEcuName();

            Map<String, RemoteEcuIdentificationInfoEntity> recordDids = eucDIDs.get(ecu);

            if(CollectionUtil.isNotEmpty(recordDids) && CollectionUtil.isNotEmpty(ecuDIDDto.getDidList())){

                List<EcuDIDItemDto> didList = new ArrayList<>();
                for (EcuDIDItemDto didItem: ecuDIDDto.getDidList()) {
                    RemoteEcuIdentificationInfoEntity didEntity = recordDids.get(didItem.getDid());

                    if(null != recordDids.get(didItem.getDid()) && null != didEntity &&
                            !didItem.getDidValue().equals(didEntity.getDidVal())){
                        EcuDIDItemDto didItemDto = new EcuDIDItemDto();
                        didItemDto.setDid(didItem.getDid());
                        didItemDto.setDidName(didItem.getDidName());
                        didItemDto.setDidValue(didEntity.getDidVal());
                        didItemDto.setOptional(didEntity.getStatus());

                        didList.add(didItemDto);
                    }

                }

                //@todo ecu flash file 待处理
                if(didList.size()>0){
                    EcuDIDDto ecuDid = new EcuDIDDto();
                    ecuDid.setEcuName(ecu);
                    ecuDid.setEcuVersion(ecuVersionMap.get(ecu));
                    ecuDid.setIsCross(ecuCrossMap.get(ecu));
                    ecuDid.setVersionType(ecuVersionTypeMap.get(ecu));
                    ecuDid.setDidList(didList);
                    List<Long> ecuVersionId = new ArrayList<>();
                    ecuVersionId.add(ecuVersionIdMap.get(ecu));
                    List<EcuVersionFileDto> files = findEcuVersionFiles(ecuVersionId);
                    List<FlashFileVo> fileVos = new ArrayList<>();
                    for (EcuVersionFileDto d : files) {
                        if(d.getVersionId().equals(ecuVersionIdMap.get(ecu))){
                            FlashFileVo vo = buildFlashFile(d);
                            fileVos.add(vo);
                        }
                    }

                    String downloadName = "";
                    String downloadUrl = "";
                    Long   downloadSize = 0L;
                    String downloadMd5 = "";
                    if(CollectionUtil.isNotEmpty(files)){
                       Map<String, Object> zipMap =  buildFlashDownloadFile(ecuDid.getEcuName(), ecuDid.getEcuVersion(), files, vin);
                        downloadName = zipMap.get("name").toString();
                        downloadUrl = zipMap.get("url").toString();
                        downloadSize = (Long) zipMap.get("size");
                        downloadMd5 = zipMap.get("md5").toString();
                    }
                    ecuDid.setFileDownloadName(downloadName);
                    ecuDid.setFileDownloadUrl(downloadUrl);
                    ecuDid.setFileDownloadSize(downloadSize);
                    ecuDid.setFileDownloadMd5(downloadMd5);

                    ecuDid.setFlashFiles(fileVos);
                    ecuDIDs.add(ecuDid);
                }
            }
        }

        return ecuDIDs;
    }

    @Override
    public List<EcuVersionFileDto> findEcuVersionFiles(List<Long> ecuVersionId) {
        List<EcuVersionFileDto> dtoList =  ecuVersionFileDao.findFlashFiles(ecuVersionId, FlashConstant.FILE_TYPE);
        return dtoList;
    }

    @Override
    public Long getTimestamp() {
        return System.currentTimeMillis()/1000;
    }

    @Override
    public String findEcuFilePath(String url) {
        return ecuFlashDownload+ url;
    }


    /***
     * 构造 ECU 版本文件列表
     * @param dto
     * @return
     */
    private FlashFileVo buildFlashFile(EcuVersionFileDto dto){
        FlashFileVo vo = new FlashFileVo();
        BeanUtil.copyProperties(dto, vo);
        return vo;
    }


    /***
     * 根据 ECU版本列表打ZIP包
     * @param files
     * @return
     */
    @Override
    public Map<String, Object> buildFlashDownloadFile(String ecuName, String ecuVersion, List<EcuVersionFileDto> files, String vin){

        SimpleDateFormat sdf = new SimpleDateFormat(syncConfig.getDirFormat());
        String dir = sdf.format(new Date());

        String name = StrUtil.cleanBlank(ecuName) + "_" + StrUtil.cleanBlank(ecuVersion) +"_"+vin+".zip";

        String path = ecuFlashDownload + dir + File.separator + name;
        File Dir = new File(ecuFlashDownload + dir);
        if(!Dir.exists()){
            Dir.mkdirs();
        }
        File zipFile = new File(path);
        if(zipFile.exists()){
            zipFile.delete();
        }

        List<File> filePaths = new ArrayList<>(files.size());
        for (EcuVersionFileDto dto: files) {
            filePaths.add(FileUtil.file(dto.getFilePath()));
        }
        File[] paths = filePaths.toArray(new File[files.size()]);
        //压缩文件
        ZipUtil.zip(zipFile, false, paths);

        Map<String, Object> map = new HashMap<>();

        /***
         * path 实际地址 后面文件下载的时候要根据url，拼接生成path地址
         * url 对外的地址 客户端根据这个地址，下载文件
         * name zip文件名 规则是 ecu + version
         */

        try {
            String md5Hex = DigestUtils.md5Hex(new FileInputStream(path));
            map.put("md5", md5Hex);
        } catch (Exception ex){
            log.info("buildFlashDownloadFile error:", JSONObject.toJSONString(ex));
        }

        map.put("path", path);
        map.put("url", buildDownloadUrl(path));
        map.put("name", name);
        map.put("size", zipFile.length());

        log.info("ecuName {} ecuVersion {} zip: {}", JSONObject.toJSONString(map));
        return map;
    }

    /***
     * 查找上一个全量的ECU版本
     * @param currentVehicleVersionId
     * @param currentEcuVersionId
     * @param targetVehicleVersionId
     * @param targetEcuVersionId
     * @return
     */
    @Override
    public Long findFullVolumePreviousVersion(Long currentVehicleVersionId, Long currentEcuVersionId,
                                              Long targetVehicleVersionId, Long targetEcuVersionId,
                                              Long ecuId) {
        //@todo 这里最好是能够找到车辆ecu 升级路径这样就很容易找了
        EcuVersionFileDto ecuVersionFileEntity =   ecuVersionFileDao.findEcuVersionId(currentEcuVersionId, 1, FlashConstant.FILE_TYPE);
        if(null != ecuVersionFileEntity){
            return ecuVersionFileEntity.getVersionId();
        }
        return null;
    }


    /***
     * 判断ecu版本依赖情况 0没有依赖版本 1有依赖版本且版本都在依赖版本里面 2有依赖不是所有版本都有依赖版本
     * @param ecuVersionDependencyList
     * @param versionIds
     * @return
     */
    private Integer filterEcuVersionId(List<RemoteVersionEcuRelyEntity> ecuVersionDependencyList, List<Long> versionIds ){
        if(CollectionUtil.isEmpty(ecuVersionDependencyList)){
            return 0;
        }
        List<Long> existIds = new ArrayList<>();

        for (RemoteVersionEcuRelyEntity en : ecuVersionDependencyList) {
            if(versionIds.contains(en.getEcuVersionId())){
                existIds.add(en.getEcuVersionId());
            }
        }

        if(existIds.size() == versionIds.size()){
            return 1;
        }

        return 2;
    }

    private String buildDownloadUrl(String path){
        String param = AESUtils.encrypt(path, AESUtils.AES_KEY);
        if(null == param){
            return "";
        }

        try {
            return domainUrl+FlashConstant.DOWNLOAD_PATH+"?params="+ URLEncoder.encode(param, StandardCharsets.UTF_8.name());
        }catch (Exception ex){
            log.error("buildDownloadUrl {} error: {}", path, ex);
            return "";
        }
    }
}
