package org.dsa.modules.reporter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@TableName("client_dependence_overview")
public class ClientDependenceOverview extends BaseEntity {

    @TableId(type = IdType.AUTO)
    private Long id;
    private String deviceId;

    private String softwareName;
    private String softwareVersion;
    private Integer softwareVersionNumber;

    private String componentName;
    private String componentVersion;
    private Integer componentVersionNumber;
}
