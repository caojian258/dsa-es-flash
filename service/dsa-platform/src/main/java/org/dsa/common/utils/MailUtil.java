package org.dsa.common.utils;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 发送邮件
 */
@Service("mailUtil")
public class MailUtil {

    private final static Logger logger = LoggerFactory.getLogger(MailUtil.class);

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${mail.from.address}")
    private String emailFrom;


    public void sendTextMail(MailEntity mailEntity) {
        //建立邮件消息
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(emailFrom); // 发送人的邮箱
        message.setSubject(mailEntity.getSubject()); //标题
        message.setTo(mailEntity.getTo().toArray(new String[mailEntity.getTo().size()])); //发给谁  对方邮箱
        message.setText(mailEntity.getContent()); //内容
        try {
            javaMailSender.send(message); //发送
        } catch (MailException e) {
            logger.error("纯文本邮件发送失败->message:{}", e);

        }

    }

    public void sendHtmlMail(MailEntity mailEntity, boolean isShowHtml) {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        //是否发送的邮件是富文本（附件，图片，html等）
        try {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);
            messageHelper.setFrom(emailFrom);// 发送人的邮箱
            messageHelper.setTo(mailEntity.getTo().toArray(new String[mailEntity.getTo().size()]));//发给谁  对方邮箱
            messageHelper.setSubject(mailEntity.getSubject());//标题
            messageHelper.setText(mailEntity.getContent(), isShowHtml);//false，显示原始html代码，无效果
            //发送
            javaMailSender.send(mimeMessage);
        } catch (MessagingException e) {
            logger.error("纯html邮件发送失败->message:{}", e);
        }

    }

    /**
     * 活动部提交后，发邮件给会审人员
     */
    public void sendAuditMail(String requestNumber, String createUserName, List<String> emaillist) {
        logger.info("正在发送审核邮件....当前的活动为：" + requestNumber);
        if (CollectionUtils.isNotEmpty(emaillist)) {
            MailEntity mailEntity = new MailEntity();
            mailEntity.setTo(emaillist);
            mailEntity.setSubject("【变更申请审批提示】"+requestNumber);
            mailEntity.setContent("【变更申请审批提示】"+requestNumber+"已提交（申请人："+createUserName+"）请及时登陆系统完成审批。");
            this.sendTextMail(mailEntity);
        } else {
            logger.info("未找到会审角色人员的邮件信息....当前的活动为：" + requestNumber);
        }
    }

    /**
     * 活动回退后，发邮件给创建人员
     */
    public void sendRejectMail(String requestNumber, String createUserName, List<String> emaillist) {
        logger.info("正在发送审核邮件....当前的活动为：" + requestNumber);
        if (CollectionUtils.isNotEmpty(emaillist)) {
            MailEntity mailEntity = new MailEntity();
            mailEntity.setTo(emaillist);
            mailEntity.setSubject("【变更申请驳回提示】"+requestNumber);
            mailEntity.setContent("【变更申请驳回提示】"+requestNumber+"， 请对变更申请已驳回,请进行修改。");
            this.sendTextMail(mailEntity);
        } else {
            logger.info("未找到会审角色人员的邮件信息....当前的活动为：" + requestNumber);
        }
    }

    /**
     * 活动 发起审批,同时也向执行人发邮件
     */
    public void sendDoneMail(String changeNumber,String createUserName,   List<String> emaillist) {
        logger.info("正在发送审核邮件....当前的活动为：" + changeNumber);
        if (CollectionUtils.isNotEmpty(emaillist)) {
            MailEntity mailEntity = new MailEntity();
            mailEntity.setTo(emaillist);
            mailEntity.setSubject("【变更分派提示】"+changeNumber);
            mailEntity.setContent("【变更分派提示】"+changeNumber+"已分派至您（申请人："+createUserName+"）， 请对变更内容和执行时间进行确认。");
            this.sendTextMail(mailEntity);
        } else {
            logger.info("未找到会审角色人员的邮件信息....当前的活动为：" + changeNumber);
        }
    }

    /**
     * 都审批后,提示执行
     */
    public void sendReadyMail(String changeNumber,String createUserName,   List<String> emaillist) {
        logger.info("正在发送审核邮件....当前的活动为：" + changeNumber);
        if (CollectionUtils.isNotEmpty(emaillist)) {
            MailEntity mailEntity = new MailEntity();
            mailEntity.setTo(emaillist);
            mailEntity.setSubject("【变更待执行提示】"+changeNumber);
            mailEntity.setContent("【变更待执行提示】"+changeNumber+"已完成审批（申请人："+createUserName+"）， 请按计划时间进行变更操作。");
            this.sendTextMail(mailEntity);
        } else {
            logger.info("未找到会审角色人员的邮件信息....当前的活动为：" + changeNumber);
        }
    }

    /**
     * 启用线程发邮件
     *
     * @param requestNumber
     * @param createUserName
     * @param emaillist
     * @param list
     * @param status
     */
    public void singleThreadExecutorSendMail(String requestNumber, String createUserName, List<String> emaillist,List<Map<String,String>> list ,String status) {
        //创建一个可重用固定线程数的线程池
        ExecutorService pool = Executors.newSingleThreadExecutor();
        pool.execute(new Runnable() {
            @Override
            public void run() {
                if(status.equals("audit")){
                    sendAuditMail(requestNumber, createUserName,emaillist);
                } else if (status.equals("reject")) {
                    sendRejectMail(requestNumber, createUserName,emaillist);
                } else if (status.equals("done")){
                    sendDoneMail(list);
                }
                else if (status.equals("ready")){
                    sendReadyMail(list);
                }
            }
        });
    }
    //审批前通知
    public void sendDoneMail(List<Map<String,String>> list) {
        logger.info("正在发送通知邮件");
        if (CollectionUtils.isNotEmpty(list)) {
            for(Map<String,String> m:list) {
                MailEntity mailEntity = new MailEntity();
                mailEntity.setTo(Arrays.asList(m.get("email")));
                mailEntity.setSubject("【变更分派提示】" + m.get("changeNumber"));
                mailEntity.setContent("【变更分派提示】" + m.get("changeNumber") + "已分派至您（申请人：" + m.get("createUserName") + "）， 请对变更内容和执行时间进行确认。");
                this.sendTextMail(mailEntity);
            }
        } else {
            logger.info("正在发送通知邮件");
        }
    }
    /**
     * 都审批后,提示执行
     */
    public void sendReadyMail(List<Map<String,String>> list) {
        logger.info("正在发送通知邮件");
        if (CollectionUtils.isNotEmpty(list)) {
            for(Map<String,String> m:list) {
                MailEntity mailEntity = new MailEntity();
                mailEntity.setTo(Arrays.asList(m.get("email")));
                mailEntity.setSubject("【变更待执行提示】" + m.get("changeNumber"));
                mailEntity.setContent("【变更待执行提示】" + m.get("changeNumber") + "已完成审批（申请人：" + m.get("createUserName") + "）， 请按计划时间进行变更操作。");
                this.sendTextMail(mailEntity);
            }
        }
    }
}
