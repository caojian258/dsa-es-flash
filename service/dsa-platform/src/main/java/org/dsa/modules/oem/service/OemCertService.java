package org.dsa.modules.oem.service;

import org.dsa.modules.oem.vo.SWTSGetCertReqVo;
import org.dsa.modules.oem.vo.WhiteSyncReqVo;

import java.util.List;
import java.util.Map;

public interface OemCertService {

    public Map<String, Object> getCert(SWTSGetCertReqVo vo);

    public Map<String, Object> CSRCertification(SWTSGetCertReqVo vo);

    Map<String, Object> whiteListSync(WhiteSyncReqVo vo);
}
