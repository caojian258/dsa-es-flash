package org.dsa.modules.reporter.dto;

import lombok.Data;

@Data
public class VehicleInfoDto {
    private String vin;
    private String project;
    private String salesName;
    private String mileage;
    private String powerType;
    private String transmissionType;
    private String batteryType;
    private String batteryVoltage;
    private String timestamp;

}
