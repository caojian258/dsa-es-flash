package org.dsa.modules.sys.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.pojo.RowMapper;
import org.dsa.common.utils.RedisUtils;
import org.dsa.common.utils.SqliteUtils;
import org.dsa.modules.sys.dao.SysLocaleDao;
import org.dsa.modules.sys.dao.SysLocalesDao;
import org.dsa.modules.sys.dao.SysTranslationsDao;
import org.dsa.modules.sys.dto.SysTranslationsDto;
import org.dsa.modules.sys.entity.SysLocaleEntity;
import org.dsa.modules.sys.entity.SysLocales;
import org.dsa.modules.sys.entity.SysTranslations;
import org.dsa.modules.sys.service.SysLocaleService;
import jakarta.validation.constraints.NotNull;
import org.dsa.modules.sys.vo.LocalePageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service("SysLocaleServiceImpl")
public class SysLocaleServiceImpl implements SysLocaleService {

    @Autowired
    SysLocaleDao sysLocaleDao;

    @Autowired
    SysLocalesDao sysLocalesDao;

    @Autowired
    SysTranslationsDao sysTranslationsDao;

    @Autowired
    RedisUtils redisUtils;

    @Override
    public Integer getIdByLocale(String locale) {
        return sysLocaleDao.getIdByLocale(locale);
    }

    @Override
    public Integer truncateTable() {
        return sysLocaleDao.truncateTable();
    }

    @Override
    public Integer insert(SysLocaleEntity sysLocaleEntity) {
        return sysLocaleDao.insert(sysLocaleEntity);
    }

    @Override
    public String getLocale(@NotNull String key, String locale) {
        String contentByKeyAndLocale = sysLocaleDao.getContentByKeyAndLocale(key, locale);
        if (contentByKeyAndLocale == null) {
            return key;
        }
        return contentByKeyAndLocale;
    }

    @Async
    @Override
    public void handleSqlite(String path) {

        try {
            SqliteUtils utils = new SqliteUtils(path);
            Map<Integer, String> localeMap = new HashMap<>();

            if(utils.exists("locales")){
                //清空数据
                //这个表的数据append操作

                utils.executeQuery("select * from locales order by id asc", new RowMapper<SysLocales>() {
                    @Override
                    public SysLocales mapRow(ResultSet rs, int index) throws SQLException {
                        SysLocales sl = new SysLocales();
                        sl.setId(rs.getInt("id"));
                        sl.setLocale(rs.getString("locale"));
                        sl.setUnits(rs.getString("units"));
                        sl.setName(rs.getString("name"));
                        sl.setCreateTime(new Date());
                        sysLocalesDao.insertData(sl);
                        localeMap.put(sl.getId(), sl.getLocale());
                        return null;
                    }
                });
            }

            if(utils.exists("translations")){
                //insert replace操作
                utils.executeQuery("select * from translations", new RowMapper<SysTranslations>() {
                    @Override
                    public SysTranslations mapRow(ResultSet rs, int index) throws SQLException {
                        String text = rs.getString("text");

                        SysTranslations sl = new SysTranslations();
                        sl.setLocale(Integer.valueOf(rs.getInt("locale")));
                        sl.setTi(rs.getString("id"));
                        sl.setText(text);
                        sl.setGeometry(rs.getString("geometry"));
                        sl.setCreateTime(new Date());
                        sysTranslationsDao.insertData(sl);
                        String locale = localeMap.get(sl.getLocale());

                        if(StrUtil.isNotBlank(locale) && StrUtil.isNotBlank(sl.getTi())){
                            redisUtils.set(locale+":"+sl.getTi(), sl.getText());
                        }
                        return null;
                    }
                });
            }
        }catch (Exception ex){
            log.error("handleSqlite error:{}", ex);
        }


    }

    @Override
    public List<SysTranslationsDto> queryLocaleTi(String locale) {
        return sysTranslationsDao.selectAllTi(locale);
    }

    @Override
    public Integer countTi(String locale) {
        return sysTranslationsDao.countTi(locale);
    }

    @Override
    public Page<SysTranslationsDto> queryLocaleTiPage(LocalePageVo vo) {
        Page<SysTranslationsDto> page = new Page<>(vo.getPageNo(), vo.getPageSize());
        QueryWrapper<SysLocales> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("locale", vo.getLocale());
        queryWrapper.last("limit 1");
        SysLocales locale = sysLocalesDao.selectOne(queryWrapper);

        return sysTranslationsDao.selectTiPage(page, locale.getId());
    }
}
