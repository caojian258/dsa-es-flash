package org.dsa.modules.oss.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("sys_file")
public class SysFileEntity {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String category;

    /**
     * 文件名
     */
    private String name;

    /**
     * 文件路径
     */
    private String url;

    /**
     * 文件大小 单位:byte
     */
    private Long fileSize;

    /**
     * 文件Md5
     */
    private String fileMd5;

    private String note;

    protected Date createdAt;

    protected Date updatedAt;
}
