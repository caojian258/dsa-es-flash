package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.dto.AppLogStatDto;
import org.dsa.modules.reporter.entity.AppHistory;

import java.util.List;


@Mapper
public interface AppHistoryMapper extends BaseMapper<AppHistory> {
    List<AppHistory> queryByTag(String tag);

    List<AppLogStatDto> queryAppLogStat(@Param("startTime") String startTime, @Param("endTime") String endTime);

    void insertAppHisStat(@Param("startTime") String startTime, @Param("endTime") String endTime);

    AppHistory queryAppHisByDeviceId(@Param("deviceId") String deviceId, @Param("updateId") Integer updateId);

    AppHistory queryAppHisBySessionId(@Param("sessionId") String sessionId, @Param("updateId") Integer updateId);
}
