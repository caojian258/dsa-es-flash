package org.dsa.modules.reporter.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 词条翻译
 */
@Data
@TableName("d_translation")
public class Translation extends BaseEntity{
    /**
     * 键
     */
    private String key;

    /**
     * 语种
     */
    private String locale;

    /**
     * 词条内容
     */
    private String text;

    /**
     * 诊断库字段
     */
    private String geometry;


}
