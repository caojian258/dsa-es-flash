package org.dsa.modules.reporter.job;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.reporter.constant.RedisConstant;
import org.dsa.modules.reporter.dto.ClientLoginDto;
import org.dsa.modules.reporter.entity.*;
import org.dsa.modules.reporter.service.*;
import org.dsa.modules.reporter.upgrade.UpgradeContext;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.*;


/***
 * 在线升级客户端数据处理任务 detail table to his table
 *
 */
@Slf4j
@Component
@DisallowConcurrentExecution
public class SystemDataMinerJob extends QuartzJobBean {

    @Autowired
    RedisUtils redisUtils;

    @Autowired
    ClientUserService clientUserService;

    @Autowired
    ClientUpgradeService clientUpgradeService;


    @Autowired
    UpgradeContext upgradeContext;

    @Autowired
    ApplicationHistoryService historyService;

    @Autowired
    ClientDependenceTreeService treeService;

    private final static String DEFAULT_WORKSHOP = "default workshop";

    private final static String DEFAULT_USER = "default user";

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.info("SystemDataMinerJob Start................");

        Long size = redisUtils.lGetListSize(RedisConstant.TAG_BASE_KEY);

        log.info("tag pool size: {}", size);

        //在这里要过滤已经入库的历史数据 只入库增量数据
        while (size>0){

            String tag = (String) redisUtils.lPop(RedisConstant.TAG_BASE_KEY);
            if(StringUtils.isEmpty(tag)){
                size--;
                continue;
            }

            List<ClientUpgrade> items = clientUpgradeService.queryDataByTag(tag);

            List<Integer> softwareIds = new ArrayList<>();
            List<Integer> componentIds = new ArrayList<>();
            String deviceId = "";

            for (ClientUpgrade record: items){
                deviceId = record.getDeviceId();
                //软件
                if(record.getUpdateTarget()  == 0 ){
                    if(!softwareIds.contains(record.getSoftwareId())){
                        softwareIds.add(record.getSoftwareId());
                    }
                }else{
                    //组件
                    if(!componentIds.contains(record.getComponentId())){
                        componentIds.add(record.getComponentId());
                    }
                }
            }

            Map<Integer, CurrentSoftwareInfo> currentSoftwareInfoMap = new HashMap<>();
            Map<Integer, FutureSoftwareInfo> futureSoftwareInfos = new HashMap<>();
            Map<Integer, CurrentComponentInfo> currentComponentInfoMap = new HashMap<>();
            Map<Integer, FutureComponentInfo> futureComponentInfoMap = new HashMap<>();

            if(CollectionUtil.isNotEmpty(softwareIds)){
                currentSoftwareInfoMap = clientUpgradeService.queryCurrentSoftwareInfoByTag(deviceId, tag, softwareIds);
                futureSoftwareInfos = clientUpgradeService.queryFutureSoftwareInfoByTag(deviceId, tag, softwareIds);
            }

            if(CollectionUtil.isNotEmpty(componentIds)){
                currentComponentInfoMap = clientUpgradeService.queryCurrentComponentInfoByTag(deviceId, tag, componentIds);
                futureComponentInfoMap = clientUpgradeService.queryFutureComponentInfoByTag(deviceId, tag, componentIds);
            }

            historyService.insertHisData(items, currentSoftwareInfoMap, futureSoftwareInfos, currentComponentInfoMap, futureComponentInfoMap);

            List<ClientDependence> dependencyDtos = treeService.queryDependencyByTag(tag);

            if(null != dependencyDtos && dependencyDtos.size()>0){
                treeService.deleteByDeviceId(deviceId);
                //整合数据 从current表拿不到数据的 从future表拿数据 名称也是一样的
                List<ClientDependenceTree> list =  treeService.formatInertData(dependencyDtos, tag);
                if(null != list && list.size()>0){
                    treeService.saveBatch(list);
                }
            }

            List<AppHistory> records =  historyService.queryHistoryByTag(tag);


            if(null == records || records.size()==0){
                size--;
                continue;
            }
            Map<String, Map<String, Object>> deviceHistory = new HashMap<>();

            //这里过滤了none的情况
            try {
                for (AppHistory result: records) {
                    if(StrUtil.isBlank( result.getUsername())){
                        result.setUsername("N/A");
                    }
                    if(StrUtil.isBlank(result.getWorkshopName())){
                        result.setWorkshopName("N/A");
                    }

                    if(StrUtil.isNotBlank( result.getUsername()) && StrUtil.isNotBlank( result.getWorkshopName()) && null == deviceHistory.get(result.getDeviceId())){
                        Map<String, Object> deviceMap = new HashMap<>();
                        deviceMap.put("pcid", result.getDeviceId());
                        deviceMap.put("workshopName", result.getWorkshopName());
                        deviceMap.put("username", result.getUsername());
                        if(null != result.getUserId()){
                            deviceMap.put("userId", result.getUserId());
                        }
                        if(null != result.getWorkshopId()){
                            deviceMap.put("workshopId", result.getWorkshopId());
                        }
                        deviceMap.put("time", result.getStartTime());
                        deviceHistory.put(result.getDeviceId(), deviceMap);
                    }
                    //需要添加tag
                    if(result.getStatusType()>0){
                        upgradeContext.getInstance(result.getStatusType()).saveHis(result);
                    }
                }

            }catch (Exception ex){
                //如果发生异常 再把tag放到队列重试
                log.error("SystemDataMinerJob Upgrade error: {}", JSONObject.toJSONString(ex));
                size--;
                continue;
            }

            //这里要根据 deviceId维度 获取用户名 登录时间 供应商名称
            if(deviceHistory.size()>0){
                for (Map<String, Object> history: deviceHistory.values()) {
                       ClientLoginDto loginDto = new ClientLoginDto();
                       loginDto.setPcid(history.get("pcid").toString());
                       loginDto.setUsername(history.get("username").toString());

                        if(null != history.get("workshopName")){
                            loginDto.setWorkshop(history.get("workshopName").toString());
                        }
                        if(null != history.get("workshopId")){
                            loginDto.setWorkshopId(Long.valueOf(history.get("workshopId").toString()));
                        }
                       loginDto.setTime((Date) history.get("time"));
                       clientUserService.updateLogin(loginDto);
                }
            }
            size--;
        }

        log.info("SystemDataMinerJob End................");
    }
}
