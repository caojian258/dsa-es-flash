package org.dsa.modules.reporter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@TableName("d_workshop_user_group")
public class WorkshopUserGroup extends BaseEntity{

    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer parentId;

    private String displayName;

    private String name;

    private String description;

    private Integer ordered;

}
