package org.dsa.modules.remoteDiagnostic.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.dsa.modules.remoteDiagnostic.dao.DiagnosticFunctionDao;
import org.dsa.modules.remoteDiagnostic.dao.DiagnosticTaskEcuDao;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticFunctionEntity;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskEcuEntity;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskFileEntity;
import org.dsa.modules.remoteDiagnostic.service.DiagnosticFunctionService;
import org.dsa.modules.remoteDiagnostic.vo.CheckNameReqVo;
import org.dsa.modules.remoteDiagnostic.vo.CheckStatusReqVo;
import org.dsa.modules.remoteDiagnostic.vo.DiagnosticFunctionPageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class DiagnosticFunctionServiceImpl implements DiagnosticFunctionService {

    @Autowired
    DiagnosticFunctionDao functionDao;
    @Autowired
    DiagnosticTaskEcuDao taskEcuDao;

    @Override
    public Page<DiagnosticFunctionEntity> selectPage(DiagnosticFunctionPageVo vo) {
        Page<DiagnosticFunctionEntity> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);
        LambdaQueryWrapper<DiagnosticFunctionEntity> qw = Wrappers.<DiagnosticFunctionEntity>lambdaQuery();

        if(!StringUtils.isEmpty(vo.getName())){
//            qw.like(VehicleEcuEntity::getEcuName, vo.getName().trim());
            qw.apply("fun_name"+" ilike {0}", "%"+vo.getName().trim()+"%");
        }
        if(!StringUtils.isEmpty(vo.getGroup())){
            qw.eq(DiagnosticFunctionEntity::getFunGroup, vo.getGroup());
        }
        if(!StringUtils.isEmpty(vo.getStatus())){
            qw.eq(DiagnosticFunctionEntity::getStatus, vo.getStatus());
        }

        return functionDao.selectPage(page, qw);
    }

    @Override
    public DiagnosticFunctionEntity getInfo(Long id) {
        return functionDao.selectById(id);
    }

    @Override
    public void saveFun(DiagnosticFunctionEntity fun) {
        functionDao.insert(fun);
    }

    @Override
    public void updateFun(DiagnosticFunctionEntity fun) {
        fun.setUpdatedAt(new Date());
        functionDao.updateById(fun);
    }

    @Override
    public List<DiagnosticFunctionEntity> getFunctions() {
        return functionDao.selectList(Wrappers.<DiagnosticFunctionEntity>lambdaQuery().eq(DiagnosticFunctionEntity::getStatus, 0).orderByAsc(DiagnosticFunctionEntity::getOrderNum));
    }

    @Override
    public Boolean check(CheckNameReqVo vo) {
        LambdaQueryWrapper<DiagnosticFunctionEntity> qw = Wrappers.<DiagnosticFunctionEntity>lambdaQuery();
        qw.eq(DiagnosticFunctionEntity::getFunName, vo.getName());
        if (vo.getId() != null){
            qw.ne(DiagnosticFunctionEntity::getId, vo.getId());
        }
        qw.select(DiagnosticFunctionEntity::getId);
        return functionDao.selectOne(qw) == null;
    }

    @Override
    public List<String> getFunGroups() {
        return functionDao.getFunGroups();
    }

    @Override
    public Boolean checkStatus(CheckStatusReqVo vo) {
        LambdaQueryWrapper<DiagnosticTaskEcuEntity> qw = Wrappers.<DiagnosticTaskEcuEntity>lambdaQuery();
        qw.eq(DiagnosticTaskEcuEntity::getFunId, vo.getId());
        qw.select(DiagnosticTaskEcuEntity::getId);
        return ObjectUtils.isEmpty(taskEcuDao.selectList(qw));
    }
}
