package org.dsa.common.pojo;

import lombok.Data;

import java.util.List;

/**
 * sqlite数据接受类
 */
@Data
public class SynLogger {

    private Integer id;

    private String vin;

    private String userName;

    private String sessionId;

    private String actionId;

    private String functionCode;

    private String actionCode;

    private String titleEn;

    private String titleCn;

    private String startTime;

    private String endTime;

    private String result;

    private String resultMessageEn;

    private String resultMessageCn;

    private String source;

    private List<SynLogger> details;

    /**
     * 日志路径
     */
    private String logPath;

    /**
     * jsonData -- detail
     */
    private String jsonData;

    private String createTime;

    private String sessionEndTime;

    private String lastUpdateTime;

    private String ecuName;
}
