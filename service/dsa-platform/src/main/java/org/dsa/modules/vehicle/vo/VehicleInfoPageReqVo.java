package org.dsa.modules.vehicle.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.modules.remoteDiagnostic.pojo.PageParam;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class VehicleInfoPageReqVo extends PageParam {

    private Long id;

    private String vin;

    private List<Long> tag;

    private List<Long> vehicleTypeIds;

    private String regular;

}