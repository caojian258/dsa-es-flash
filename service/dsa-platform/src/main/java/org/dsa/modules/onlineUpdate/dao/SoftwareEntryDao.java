package org.dsa.modules.onlineUpdate.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.onlineUpdate.entity.SoftwareEntryEntity;
import org.dsa.modules.onlineUpdate.vo.SoftwareEntryVo;

/**
 * 软件入口
 */
@Mapper
public interface SoftwareEntryDao extends BaseMapper<SoftwareEntryEntity> {
    SoftwareEntryEntity selectOneBySoftwareId(@Param("softwareId") Long softwareId);
    SoftwareEntryVo selectOneBySId(@Param("id") Long id);
}
