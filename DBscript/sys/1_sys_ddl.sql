-- public.d_language_locales definition

-- Drop table

DROP TABLE IF exists public.d_language_locales;


CREATE TABLE public.d_language_locales (
	id bigserial NOT NULL,
	locale varchar(50) NOT NULL,
	"name" varchar(50) NOT NULL,
	unit varchar(50) NOT NULL DEFAULT 'UG_EU'::text,
	CONSTRAINT d_language_locales_pkey PRIMARY KEY (id)
);


DROP TABLE IF exists public.d_language_translation;


CREATE TABLE public.d_language_translation (
	"key" varchar(255) NOT NULL,
	locale varchar(50) NOT NULL,
	"text" text NOT NULL,
	geometry text NULL
);

DROP TABLE IF exists public.sys_captcha;

CREATE TABLE public.sys_captcha (
	uuid bpchar(36) NOT NULL,
	code varchar(6) NOT NULL,
	expire_time timestamp NULL,
	CONSTRAINT sys_captcha_pkey PRIMARY KEY (uuid)
);

DROP TABLE IF exists public.sys_config;

CREATE TABLE public.sys_config (
	id bigserial NOT NULL,
	param_key varchar(50) NULL DEFAULT NULL::character varying,
	param_value varchar(2000) NULL DEFAULT NULL::character varying,
	status int4 NULL DEFAULT 1,
	remark varchar(500) NULL DEFAULT NULL::character varying,
	param_group varchar(50) NULL,
	language_key varchar(255) NULL,
	order_num int4 NULL DEFAULT 0,
	"type" int4 Not NULL DEFAULT 0,
	CONSTRAINT sys_config_pkey PRIMARY KEY (id)
);
COMMENT ON COLUMN public.sys_config."type" IS '数据类型0系统配置1业务配置';

DROP TABLE IF exists public.sys_dict;


CREATE TABLE public.sys_dict (
	id bigserial NOT NULL,
	"name" varchar(100) NOT NULL,
	"type" varchar(100) NOT NULL,
	code varchar(100) NOT NULL,
	value varchar(1000) NOT NULL,
	order_num int4 NULL DEFAULT 0,
	remark varchar(255) NULL DEFAULT NULL::character varying,
	del_flag int4 NULL DEFAULT 0,
	CONSTRAINT sys_dict_pkey PRIMARY KEY (id)
);

DROP TABLE IF exists public.sys_group;
CREATE TABLE public.sys_group (
	group_id bigserial NOT NULL,
	workshop_id int8 NULL,
	"name" varchar(50) NULL DEFAULT NULL::character varying,
	group_category varchar(15) NULL DEFAULT NULL::character varying,
	order_num int4 NULL,
	del_flag int4 NULL DEFAULT 0,
	dept_name varchar(50) NULL DEFAULT NULL::character varying,
	CONSTRAINT sys_group_pkey PRIMARY KEY (group_id)
);

DROP TABLE IF exists public.sys_log;

CREATE TABLE public.sys_log (
	id bigserial NOT NULL,
	username varchar(200) NULL DEFAULT NULL::character varying,
	operation varchar(200) NULL DEFAULT NULL::character varying,
	"method" varchar(200) NULL DEFAULT NULL::character varying,
	params text NULL,
	"time" int8 NOT NULL,
	ip varchar(64) NULL DEFAULT NULL::character varying,
	create_date timestamp NULL,
	display_user_name varchar NULL,
	"result" varchar NULL,
	CONSTRAINT sys_log_pkey PRIMARY KEY (id)
);

DROP TABLE IF exists public.sys_menu;

CREATE TABLE public.sys_menu (
	menu_id bigserial NOT NULL,
	parent_id int8 NULL,
	"name" varchar(50) NULL,
	url varchar(200) NULL,
	perms varchar(500) NULL,
	"type" int4 NULL,
	icon varchar(50) NULL,
	order_num int4 NULL,
	query varchar NULL,
	use_to int4 NULL,
	owning_system int4 null,
	CONSTRAINT sys_menu_pkey PRIMARY KEY (menu_id)
);
COMMENT ON COLUMN public.sys_menu.owning_system IS '0 GenericMenu 1 DsaPlatform 2 LicenseControl';

DROP TABLE IF exists public.sys_oss;

CREATE TABLE public.sys_oss (
	id bigserial NOT NULL,
	url varchar(200) NULL DEFAULT NULL::character varying,
	create_date timestamp NULL,
	CONSTRAINT sys_oss_pkey PRIMARY KEY (id)
);
DROP TABLE IF exists public.sys_role;

CREATE TABLE public.sys_role (
	role_id bigserial NOT NULL,
	role_name varchar(100) NULL DEFAULT NULL::character varying,
	remark varchar(100) NULL DEFAULT NULL::character varying,
	create_user_id int8 NULL,
	create_time timestamp NULL,
	CONSTRAINT sys_role_pkey PRIMARY KEY (role_id)
);
DROP TABLE IF exists public.sys_role_menu;
CREATE TABLE public.sys_role_menu (
	id bigserial NOT NULL,
	role_id int8 NULL,
	menu_id int8 NULL,
	CONSTRAINT sys_role_menu_pkey PRIMARY KEY (id)
);
DROP TABLE IF exists public.sys_role_workshop;
CREATE TABLE public.sys_role_workshop (
	id bigserial NOT NULL,
	role_id int8 NULL,
	workshop_id int8 NULL,
	CONSTRAINT sys_role_workshop_pkey PRIMARY KEY (id)
);

DROP TABLE IF exists public.sys_user;

CREATE TABLE public.sys_user (
	user_id bigserial NOT NULL,
	username varchar(50) NOT NULL,
	chinese_name varchar(100) NULL,
	english_name varchar(100) NULL,
	emp_no varchar(100) NULL DEFAULT '10000'::character varying,
	workshop_id int8 NULL,
	workshop_name varchar(50) NULL DEFAULT NULL::character varying,
	"password" varchar(100) NULL DEFAULT NULL::character varying,
	salt varchar(20) NULL DEFAULT NULL::character varying,
	email varchar(100) NULL DEFAULT NULL::character varying,
	mobile varchar(100) NULL DEFAULT NULL::character varying,
	oem_code varchar NULL,
	status int4 NULL,
	create_user_id int8 NULL,
	create_time timestamp NULL,
	update_time timestamp NULL,
	CONSTRAINT sys_user_pkey PRIMARY KEY (user_id),
	CONSTRAINT sys_user_username_key UNIQUE (username)
);
DROP TABLE IF exists public.sys_user_group;

CREATE TABLE public.sys_user_group (
	id bigserial NOT NULL,
	user_id int8 NULL,
	group_id int8 NULL,
	CONSTRAINT sys_user_group_pkey PRIMARY KEY (id)
);

DROP TABLE IF exists public.sys_user_role;

CREATE TABLE public.sys_user_role (
	id bigserial NOT NULL,
	user_id int8 NULL,
	role_id int8 NULL,
	CONSTRAINT sys_user_role_pkey PRIMARY KEY (id)
);

DROP TABLE IF exists public.sys_user_token;

CREATE TABLE public.sys_user_token (
	user_id bigserial NOT NULL,
	"token" varchar(100) NOT NULL,
	expire_time timestamp NULL,
	update_time timestamp NULL,
	"type" int4 NULL,
	CONSTRAINT sys_user_token_un UNIQUE (user_id, type)
);
COMMENT ON COLUMN public.sys_user_token."type" IS 'token 类型 0 serverToken 1 clientToken';

DROP TABLE IF exists public.sys_workshop;

CREATE TABLE public.sys_workshop (
	workshop_id bigserial NOT NULL,
	parent_id int8 NULL,
	"name" varchar(50) NULL,
	order_num int4 NULL,
	del_flag int4 NULL DEFAULT 0,
	address varchar NULL,
	CONSTRAINT sys_workshop_pkey PRIMARY KEY (workshop_id)
);
DROP TABLE IF exists public.sys_file;

CREATE TABLE public.sys_file (
	id bigserial NOT NULL,
	category varchar(100) NOT NULL,
	"name" varchar(255) NOT NULL,
	url varchar(512) NOT NULL,
	file_size int8 NOT NULL,
	file_md5 varchar(255) NULL,
	note text NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now(),
	CONSTRAINT sys_file_pkey PRIMARY KEY (id)
);
CREATE INDEX idx_sys_category ON public.sys_file USING btree (category);
CREATE INDEX idx_sys_file_name ON public.sys_file USING btree (name);
DROP TABLE IF exists public.schedule_job;

CREATE TABLE public.schedule_job (
	job_id bigserial NOT NULL PRIMARY KEY,
	job_name varchar(200) NULL DEFAULT NULL::character varying,
	job_group varchar(200) NULL DEFAULT NULL::character varying,
	job_class varchar(512) NULL DEFAULT NULL::character varying,
	interface_name varchar(512) NULL DEFAULT NULL::character varying,
	cron_expression varchar(100) NULL DEFAULT NULL::character varying,
	job_status varchar NOT NULL,
	remark varchar(2000) NULL DEFAULT NULL::character varying,
	repeat_time int4 NULL,
	cron_job bool NULL DEFAULT false,
	created_at timestamp NULL DEFAULT now(),
	has_listener bool NULL DEFAULT false,
	listener_class varchar NULL,
	updated_at timestamp NULL DEFAULT now()
);

DROP TABLE IF exists public.schedule_job_log;

CREATE TABLE public.schedule_job_log (
	id bigserial NOT NULL PRIMARY KEY,
	job_id int8 NULL,
	bean_name varchar(200) NULL DEFAULT NULL::character varying,
	params varchar(2000) NULL DEFAULT NULL::character varying,
	status varchar(200) NOT NULL,
	error varchar(2000) NULL DEFAULT NULL::character varying,
	times int4 NULL,
	create_time timestamp NULL DEFAULT now()
);

-- public.d_version_info definition

-- Drop table

-- DROP TABLE public.d_version_info;

CREATE TABLE public.d_version_info (
	id bigserial NOT NULL,
	vehicle_type_id int8 NOT NULL,
	version_type varchar NULL,
	version_num varchar NOT NULL,
	parent_version_num varchar NOT NULL,
	description varchar NULL,
	file_name varchar NOT NULL,
	file_path varchar NOT NULL,
	file_type varchar NULL,
	db_file_path varchar NULL,
	db_file_name varchar NULL,
	menu_file_path varchar NULL,
	menu_file_name varchar NULL,
	status varchar NOT NULL,
	version_file_name varchar NULL,
	version_file_md5 varchar NULL,
	create_time timestamp NOT NULL DEFAULT now(),
	create_by int8 NULL
);

CREATE TABLE public.sys_location (
	id bigserial NOT NULL PRIMARY KEY,
	code int8 NULL,
	"name" varchar(32) NULL DEFAULT NULL::character varying,
	pid int8 NULL,
	"type" varchar(32) NULL DEFAULT NULL::character varying
);

CREATE TABLE public.sys_workshops (
	id bigserial NOT NULL PRIMARY KEY,
	parent_id int8 NULL,
	code varchar(20) NULL,
	"name" varchar(50) NULL,
	short_name varchar(50) NULL,
	country_code int8 NULL DEFAULT '100000'::bigint,
	country_name varchar(50) NULL DEFAULT '中国'::character varying,
	province_code int8 NULL,
	province_name varchar(50) NULL,
	city_code int8 NULL,
	city_name varchar(50) NULL,
	region varchar(50) NULL,
	order_num int4 NULL,
	del_flag int4 NULL DEFAULT 0,
	address varchar NULL,
	note varchar NULL,
	create_time timestamp NULL DEFAULT now(),
	update_time timestamp NULL DEFAULT now()
);

CREATE UNIQUE INDEX sys_workshops_name_idx ON public.sys_workshops USING btree (name);

DROP TABLE IF exists public.sys_notice;

CREATE TABLE public.sys_notice (
	id serial4 NOT NULL,
	title varchar NULL,
	file_name varchar NOT NULL,
	file_path varchar NOT NULL,
	file_md5 varchar NOT NULL,
	locale varchar NOT NULL,
	status int8 NOT NULL,
	expire_day date NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NULL,
	created_user_id int8 NULL,
	updated_user_id int8 NULL,
	CONSTRAINT sys_notice_pkey PRIMARY KEY (id)
);


CREATE TABLE IF NOT EXISTS t_user_license (
	user_id varchar NOT NULL,
	app_code varchar NOT NULL,
	app_name varchar NULL,
	app_name_ti varchar NULL,
	oem_code varchar NOT NULL,
	oem_name varchar NULL,
	oem_name_ti varchar NULL,
	product_code varchar NOT NULL,
	product_name varchar NULL,
	product_name_ti varchar NULL,
	license_type_code varchar NOT NULL,
	license_type_name varchar NULL,
	license_type_name_ti varchar NULL,
	id serial4 NOT NULL,
	user_name varchar NOT NULL,
	CONSTRAINT t_user_license_pk PRIMARY KEY (id)
);


DROP TABLE IF exists public.sys_cache;

CREATE TABLE public.sys_cache (
	"key" varchar(500) unique NOT NULL,
	"value" varchar NOT NULL
);

COMMENT ON TABLE public.sys_cache IS '系统缓存表';


CREATE TABLE public.sys_internationalization (
	"key" varchar(50) NOT NULL,
	"value" varchar NULL,
	"local" varchar(50) NOT NULL,
	unit varchar(50) NOT NULL DEFAULT 'UG_EU'::text
);
ALTER TABLE public.sys_internationalization ADD CONSTRAINT sys_internationalization_un UNIQUE ("key","local");

COMMENT ON TABLE public.sys_internationalization IS '多语言表';
COMMENT ON COLUMN sys_internationalization."key" IS 'key';
COMMENT ON COLUMN sys_internationalization."value" IS 'value';
COMMENT ON COLUMN sys_internationalization."local" IS '语言';

CREATE TABLE public.sys_locales (
	id serial4 NOT NULL,
	locale varchar NULL,
	"name" varchar NULL,
	units varchar NULL,
	create_time timestamp NULL DEFAULT now(),
	CONSTRAINT sys_locales_pkey PRIMARY KEY (id),
	CONSTRAINT sys_locales_unique_key UNIQUE (locale)
);

COMMENT ON TABLE public.sys_locales IS '客户端翻译库语言';


CREATE TABLE public.sys_translations (
	id bigserial NOT NULL,
	ti varchar NOT NULL,
	"text" varchar NULL,
	locale int4 NULL,
	geometry varchar NULL,
	create_time timestamp NULL DEFAULT now(),
	CONSTRAINT sys_translations_pkey PRIMARY KEY (id),
	CONSTRAINT sys_translations_unique_key UNIQUE (ti, locale)
);
CREATE INDEX idx_translations_locale ON public.sys_translations USING btree (locale);
CREATE INDEX idx_translations_ti ON public.sys_translations USING btree (ti, text);

COMMENT ON TABLE public.sys_translations IS '客户端翻译库内容';
