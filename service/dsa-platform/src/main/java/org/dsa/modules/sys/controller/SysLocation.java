package org.dsa.modules.sys.controller;

import org.dsa.common.utils.R;
import org.dsa.modules.sys.entity.SysLocationEntity;
import org.dsa.modules.sys.service.SysLocationService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("sys/location")
public class SysLocation extends AbstractController {

    @Resource
    SysLocationService sysLocationService;

    @RequestMapping("/select/{pid}")
    public R select(@PathVariable("pid") Long pid){
        Long defaultPid = 10000000L;
        if(pid == null || pid == 0){
            pid = defaultPid;
        }
        List<SysLocationEntity> locations =  sysLocationService.selectByPid(pid);
        return R.ok().put("data", locations);
    }
}
