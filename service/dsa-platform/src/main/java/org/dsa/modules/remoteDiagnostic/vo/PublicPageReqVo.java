package org.dsa.modules.remoteDiagnostic.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.modules.remoteDiagnostic.pojo.PageParam;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class PublicPageReqVo extends PageParam {

    private Long id;

    private String name;

    private Integer status;

    // 保留字段，多name时，可使用
    private String script;

    private String type;

}