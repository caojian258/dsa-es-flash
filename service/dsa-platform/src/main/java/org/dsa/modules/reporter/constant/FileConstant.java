package org.dsa.modules.reporter.constant;

public final class FileConstant {

    public final static String DIR_CONFIG_KEY = "LOCAL_REPORT_DIR";

    public final static String BIZ_SESSION = "session";

    public final static String BIZ_FLASH = "flash";

    public final static String BIZ_ONLINE_UPDATE = "online_update";

    public final static String BIZ_WORKSHOP = "workshop";

}
