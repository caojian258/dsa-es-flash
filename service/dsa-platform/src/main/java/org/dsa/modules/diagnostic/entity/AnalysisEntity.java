package org.dsa.modules.diagnostic.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 故障码统计查询条件
 *
 */
@Data
public class AnalysisEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 开始时间
	 */
	private String startTime;

	/**
	 * 结果时间
	 */
	private String endTime;
	/**
	 * ecuNames
	 */
	private String[] ecuNames;
	/**
	 * sources
	 */
	private String[] sources;

	/**
	 * vehicleTypeIds
	 */
	private Integer[] vehicleTypeIds;

	private String vin;

	private String functionCode;

	private String actionCode;

	/**
	 * topN
	 */
	private Integer topN;

}
