package org.dsa.modules.reporter.service.impl;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.dsa.modules.reporter.config.ReportConfig;
import org.dsa.modules.reporter.dao.*;
import org.dsa.modules.reporter.dto.AppOptionDto;
import org.dsa.modules.reporter.dto.ClientBaseInfoDto;
import org.dsa.modules.reporter.dto.ClientDetailDto;
import org.dsa.modules.reporter.entity.*;
import org.dsa.modules.reporter.service.ClientService;
import org.dsa.modules.reporter.util.LocalUtil;
import org.dsa.modules.reporter.util.MapBeanUtil;
import org.dsa.modules.reporter.vo.ChartReVo;
import org.dsa.modules.reporter.vo.client.*;
import org.dsa.modules.sys.dao.SysWorkshopsDao;
import org.dsa.modules.sys.entity.SysWorkshopsEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import jakarta.annotation.Resource;
import java.sql.Timestamp;
import java.util.*;

@Slf4j
@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    SystemOverviewMapper systemOverviewMapper;

    @Autowired
    AppInstallMapper appInstallMapper;

    @Autowired
    AppOverviewMapper appOverviewMapper;


    @Autowired
    AppVersionMapper appVersionMapper;

    @Autowired
    SystemProtocolItemMapper protocolItemMapper;


    @Autowired
    ReportConfig reportConfig;

    @Resource
    SystemHistoryMapper historyMapper;

    @Resource
    SysWorkshopsDao sysWorkshopsDao;

    @Override
    public Page<AppInstall> selectInstallPage(AppInstallPageReVo vo) {

//        if(null == vo.getTag()){
//            SystemOverview client = systemOverviewMapper.getOneByPcid(vo.getPcid());
//            if(null == client){
//                return null;
//            }
//            vo.setTag(client.getTag());
//        }

        Page<AppInstall> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);

        LambdaQueryWrapper<AppInstall> qw = Wrappers.<AppInstall>lambdaQuery();

        if(!StringUtils.isEmpty(vo.getPcid())){
            qw.eq(AppInstall::getPcid, vo.getPcid());
        }

        if(!StringUtils.isEmpty(vo.getTag())){
            qw.eq(AppInstall::getTag, vo.getTag());
        }

        if(!StringUtils.isEmpty(vo.getName())){
            qw.eq(AppInstall::getName, vo.getName());
        }

        if(!StringUtils.isEmpty(vo.getMessage()) ){
            qw.apply("install_msg"+" ilike {0}", "%"+vo.getMessage().trim()+"%")
                    .or()
                    .apply("name"+" ilike {0}", "%"+vo.getMessage().trim()+"%");
        }
        Page<AppInstall> result = appInstallMapper.selectPage(page, qw);
        return result;
    }

    @Override
    public Page<AppOverview> selectAppPage(AppPageReVo vo) {
        Page<AppOverview> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);

        LambdaQueryWrapper<AppOverview> qw = Wrappers.<AppOverview>lambdaQuery();

        if(!StringUtils.isEmpty(vo.getPcid())){
            qw.apply("pcid"+" ilike {0}", "%"+vo.getPcid().trim()+"%");
        }

        if(!StringUtils.isEmpty(vo.getWorkshop())){
            qw.apply("workshop"+" ilike {0}", "%"+vo.getWorkshop().trim()+"%");
        }

        if(!StringUtils.isEmpty(vo.getApplication())){
            if ("WTS".equals(vo.getApplication())) {
                qw.apply("name"+" ilike {0}", "%"+vo.getApplication().trim()+"%");
            } else {
                qw.eq(AppOverview::getName, vo.getApplication());
            }
        }

        if(null != vo.getAppId() && vo.getAppId()>0){
            qw.eq(AppOverview::getAppId, vo.getAppId());
        }

        if(!StringUtils.isEmpty(vo.getMessage())){
            qw.apply("name"+" ilike {0}", "%"+vo.getMessage().trim()+"%")
                    .or()
                    .apply("version" + " ilike {0}", "%"+vo.getMessage().trim()+"%");
        }

        if(!StringUtils.isEmpty(vo.getType())){
            qw.eq(AppOverview::getType, vo.getType());
        }

        if(!StringUtils.isEmpty(vo.getVersion())){
            qw.apply("version"+" ilike {0}", "%"+vo.getVersion().trim()+"%");
        }

        Page<AppOverview> resultPage = appOverviewMapper.selectPage(page, qw);
        List<AppOverview> items = resultPage.getRecords();
        for (AppOverview app: items){
            app.setHasChildren(true);
        }
        resultPage.setRecords(items);
        return resultPage;
    }

    @Override
    public Page<SystemOverview> selectPage(ClientPageReVo vo) {

        Page<SystemOverview> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);
        Boolean flag = false;

        if (vo.getGroup()!= null && vo.getGroup().contains("unknown")){
            flag = true;
        }

        return systemOverviewMapper.queryPage(page, vo.getPcid(),vo.getWorkshop(),
                vo.getUsername(),vo.getAntivir(),vo.getSystemLocale(),vo.getApplicationLocale(),vo.getWindowsVersion(),flag,vo.getGroup());
    }

    @Override
    public ClientDetailDto detail(ClientDetailReVo vo) {

        ClientDetailResVo info = systemOverviewMapper.getDetail(vo.getPcid());
        if(null !=info && StrUtil.isNotBlank(info.getWorkshop())){
          SysWorkshopsEntity sysWorkshopsEntity =  sysWorkshopsDao.findByName(info.getWorkshop());
          if(null != sysWorkshopsEntity){
              info.setWorkshopAddress(sysWorkshopsEntity.getProvinceName()+" "+sysWorkshopsEntity.getCityName()+ " " +(sysWorkshopsEntity.getAddress()!=null? sysWorkshopsEntity.getAddress():""));
              info.setWorkshopCountry(sysWorkshopsEntity.getCountryName());
              info.setWorkshopName(sysWorkshopsEntity.getName());
              info.setWorkshop(sysWorkshopsEntity.getCode());
          }
        }
        ClientDetailDto dto = new ClientDetailDto();
        if(null != info){
            Map<String, Object> maps = MapBeanUtil.object2Map(info);

            List<ClientBaseInfoDto> hardwareValueList = formatBaseInfo(maps, Arrays.asList(reportConfig.getClientHardware().split(",")));
            List<ClientBaseInfoDto> softwareValueList = formatBaseInfo(maps, Arrays.asList(reportConfig.getClientSoftware().split(",")));
            List<ClientBaseInfoDto> workshopValueList = formatBaseInfo(maps, Arrays.asList(reportConfig.getClientWorkshop().split(",")));
            dto.setClientHardwareList(hardwareValueList);
            dto.setClientSoftwareList(softwareValueList);
            dto.setClientWorkshopList(workshopValueList);
        }

        dto.setInfo(info);
        dto.setAppInstallList(null);
        return dto;
    }

    @Override
    public Page<SystemProtocolItem> protocolItemPage(ProtocolItemPageReVo vo) {
        Page<SystemProtocolItem> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);

        LambdaQueryWrapper<SystemProtocolItem> qw = Wrappers.<SystemProtocolItem>lambdaQuery();

        if(!StringUtils.isEmpty(vo.getDate())){
            qw.eq(SystemProtocolItem::getCollectDate, vo.getDate());
        }
        if(!StringUtils.isEmpty(vo.getPcid())){
            qw.eq(SystemProtocolItem::getPcid, vo.getPcid());
        }
        if(!StringUtils.isEmpty(vo.getMessage())){
            qw.apply("message"+" ilike {0}", "%"+vo.getMessage().trim()+"%");
        }

        if(!StringUtils.isEmpty(vo.getTag())){
            qw.eq(SystemProtocolItem::getTag, vo.getTag());
        }

        Page<SystemProtocolItem> resultPage = protocolItemMapper.selectPage(page, qw);

        return resultPage;
    }

    @Override
    public Page<AppVersion> appVersionHistoryPage(AppVersionPageReVo vo) {
        Page<AppVersion> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);

        LambdaQueryWrapper<AppVersion> qw = Wrappers.<AppVersion>lambdaQuery();

        if(!StringUtils.isEmpty(vo.getDate())){
            qw.eq(AppVersion::getCollectDate, vo.getDate());
        }
        if(!StringUtils.isEmpty(vo.getPcid())){
            qw.eq(AppVersion::getPcid, vo.getPcid());
        }

        if(!StringUtils.isEmpty(vo.getMessage())){
            qw.apply("name"+" ilike {0}", "%"+vo.getMessage().trim()+"%")
                    .or()
                    .apply("version"+" ilike {0}", "%"+vo.getMessage().trim()+"%");
        }

        if(!StringUtils.isEmpty(vo.getTag())){
            qw.eq(AppVersion::getTag, vo.getTag());
        }

        Page<AppVersion> resultPage = appVersionMapper.selectPage(page, qw);

        return resultPage;
    }

    @Override
    public Page<SystemHistory> clientHistoryPage(ClientHisPageReVo vo) {

        Page<SystemHistory> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);

        LambdaQueryWrapper<SystemHistory> qw = Wrappers.<SystemHistory>lambdaQuery();

        if(!StringUtils.isEmpty(vo.getPcid())){
//            qw.eq(SystemHistory::getPcid, vo.getPcid());
            qw.apply("pcid"+" ilike {0}", "%"+vo.getPcid().trim()+"%");

        }
        if(!StringUtils.isEmpty(vo.getWorkshop())){
            qw.apply("workshop"+" ilike {0}", "%"+vo.getWorkshop().trim()+"%");

        }

        if(!StringUtils.isEmpty(vo.getUsername())){
//            qw.eq(SystemHistory::getUsername, vo.getUsername());
            qw.apply("username"+" ilike {0}", "%"+vo.getUsername().trim()+"%");

        }

        if (vo.getTaskTime() != null && vo.getTaskTime().size() == 2) {
            qw.between(SystemHistory::getCollectTime, Timestamp.valueOf(vo.getTaskTime().get(0)+" 00:00:00.000"),Timestamp.valueOf(vo.getTaskTime().get(1)+" 23:59:59.999"));
        }
        qw.select(SystemHistory::getPcid,
                SystemHistory::getWorkshop,
                SystemHistory::getUsername,
                SystemHistory::getWindowsVersion,
                SystemHistory::getSystemLocale,
                SystemHistory::getApplicationLocale,
                SystemHistory::getCpuArch,
                SystemHistory::getDesktopResolution,
                SystemHistory::getDiskUsed,
                SystemHistory::getRamUsed,
                SystemHistory::getFirewallFlag,
                SystemHistory::getAntivirInstalled,
                SystemHistory::getCollectTime
        );
        Page<SystemHistory> resultPage = historyMapper.selectPage(page, qw);
        return resultPage;
    }

    @Override
    public List<AppInstallResVo> appInstallList(ClientDetailReVo vo) {
        SystemOverview client = systemOverviewMapper.getOneByPcid(vo.getPcid());
        List<AppInstallResVo> appInstallResVoList = null;
        if(null != client){
            appInstallResVoList = appInstallMapper.getListByPcid(client.getPcid(), client.getTag());
        }
        return appInstallResVoList;
    }

    @Override
    public List<AppVersionResVo> appVersionDateGroup(AppVersionPageReVo vo) {
        Integer offset = (vo.getPageNo()-1)* vo.getPageSize();

        return appVersionMapper.getListGroupByDate(null, vo.getPcid(),"application", null,null, offset, vo.getPageSize());
    }

    @Override
    public List<String> applicationList() {
        return appOverviewMapper.getAppList();
    }

    private List<ClientBaseInfoDto> formatBaseInfo(Map<String, Object> maps,  List<String> itemList){
        List<Map<String,String>> valueList = new ArrayList<>();
        List<ClientBaseInfoDto> dtos = new ArrayList<>();
        for(String k : maps.keySet()) {
            String value = maps.get(k) == null ? "" : maps.get(k).toString();
            String key = StrUtil.toUnderlineCase(k);
            Integer pos = itemList.indexOf(key);
            if (pos>-1) {
                ClientBaseInfoDto dto = new ClientBaseInfoDto();
                if(StrUtil.isNotBlank(value) ){
                    if(key.equals("download_speed")){
                        value = NumberUtil.roundStr(Double.valueOf(value), 2) +" MB/S";
                    }

                    if(key.equals("ram_used") || key.equals("ram_total")) {
                        value = NumberUtil.roundStr(Double.valueOf(value) / (1024 * 1024), 0) +" GB";
                    }

                    if( key.equals("disk_used") || key.equals("disk_total")) {
                        value = NumberUtil.roundStr(Double.valueOf(value) / (1024 * 1024 * 1024), 0) +" GB";
                    }
                }
                dto.setField(key);
                dto.setValue(value);
                dto.setTitle(LocalUtil.get("client."+k));
                dto.setSorted(pos);
                dtos.add(dto);
            }
        }

        Collections.sort(dtos);
        return dtos;
    }

    @Override
    public Page<AppVersion> selectAppHisPage(AppPageReVo vo) {
        Page<AppVersion> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);

        LambdaQueryWrapper<AppVersion> qw = Wrappers.<AppVersion>lambdaQuery();

        if(!StringUtils.isEmpty(vo.getPcid())){
            qw.apply("pcid"+" ilike {0}", "%"+vo.getPcid().trim()+"%");
        }

        if(!StringUtils.isEmpty(vo.getWorkshop())){
            qw.apply("workshop"+" ilike {0}", "%"+vo.getWorkshop().trim()+"%");
        }

        if(!StringUtils.isEmpty(vo.getApplication())){
            if ("WTS".equals(vo.getApplication())) {
                qw.apply("name"+" ilike {0}", "%"+vo.getApplication().trim()+"%");
            } else {
                qw.eq(AppVersion::getName, vo.getApplication());
            }
        }

        if(null != vo.getAppId() && vo.getAppId()>0){
            qw.eq(AppVersion::getAppId, vo.getAppId());
        }
        if(!StringUtils.isEmpty(vo.getType())){
            qw.eq(AppVersion::getType, vo.getType());
        }

        if(!StringUtils.isEmpty(vo.getVersion())){
            qw.apply("version"+" ilike {0}", "%"+vo.getVersion().trim()+"%");
        }
        if (vo.getTaskTime() != null && vo.getTaskTime().size() == 2) {
            qw.between(AppVersion::getCollectTime, Timestamp.valueOf(vo.getTaskTime().get(0)+" 00:00:00.000"),Timestamp.valueOf(vo.getTaskTime().get(1)+" 23:59:59.999"));
        }
        Page<AppVersion> resultPage = appVersionMapper.selectPage(page, qw);

        return resultPage;
    }

    @Override
    public Map<String, Object> getInputs() {
        Map<String, Object> inputs = new HashMap<>();
        inputs.put("antivirs",systemOverviewMapper.getAllAntivir()) ;
        inputs.put("applicationLocales",systemOverviewMapper.getAllApplicationLocale()) ;
        inputs.put("systemLocales",systemOverviewMapper.getAllSystemLocale()) ;
        inputs.put("windowsVersion",systemOverviewMapper.getAllWindowsVersions()) ;
        return inputs;
    }

    @Override
    public List<String> getAllAnivirHistory() {
        return historyMapper.getAllAntivirHistory();
    }

    @Override
    public List<AppOptionDto> queryAppOption() {
        List<AppOptionDto> records = appOverviewMapper.queryAppOption();
        //去重
        Map<Integer, String> map = new HashMap<>();
        for (AppOptionDto dto: records) {
            map.put(dto.getAppId(), dto.getName());
        }
        List<AppOptionDto> options = new ArrayList<>(map.size());

        Iterator<Map.Entry<Integer, String>> entries = map.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry<Integer, String> entry = entries.next();
            AppOptionDto option = new AppOptionDto();
            option.setAppId(entry.getKey());
            option.setName(entry.getValue());
            options.add(option);
        }

        return options;
    }

    @Override
    public List<Map<String, Object>> clientWorkshopStatics(ChartReVo vo) {
        Map<String, Object> condition = vo.getCondition();
        String code = vo.getCode();

        List<Long> workshopIds = sysWorkshopsDao.selectWorkshopByLocation(
                condition.get("country") != null ? condition.get("country").toString():"",
                condition.get("region") != null ? condition.get("region").toString():"",
                condition.get("province") != null ? condition.get("province").toString():"",
                condition.get("city") != null ? condition.get("city").toString():""
        );

        List<Map<String, Object>> items = new ArrayList<>();

        switch (code){
            case "workshop_client_pi":
                items =  systemOverviewMapper.staticClientByWorkshop(workshopIds, vo.getTop());
                break;
            case "client_application_locale_pi":
                items = systemOverviewMapper.staticClientAppLocaleByWorkshop(workshopIds, vo.getTop());
                break;
            case "client_system_locale_pi":
                items = systemOverviewMapper.staticClientSystemLocaleByWorkshop(workshopIds, vo.getTop());
                break;
        }
        return items;
    }
}
