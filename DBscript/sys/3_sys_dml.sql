INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(2, 101, 'User', 'sys/user', '', 1, 'admin', 4, '', 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(3, 101, 'AuthRole', 'sys/role', '', 1, 'role', 3, '', 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(4, 101, 'Menu', 'sys/menu', '', 1, 'menu', 3, '', 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(15, 2, '查看', '', 'sys:user:list,sys:user:info,sys:workshop:select', 2, '', 0, '', 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(16, 2, '新增', '', 'sys:user:save,sys:role:select', 2, '', 0, '', 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(17, 2, '修改', '', 'sys:user:update,sys:role:select', 2, '', 0, '', 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(18, 2, '删除', '', 'sys:user:delete', 2, '', 0, '', 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(19, 3, '查看', '', 'sys:role:list,sys:role:info', 2, '', 0, '', 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(20, 3, '新增', '', 'sys:role:save,sys:menu:list', 2, '', 0, '', 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(21, 3, '修改', '', 'sys:role:update,sys:menu:list', 2, '', 0, '', 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(22, 3, '删除', '', 'sys:role:delete', 2, '', 0, '', 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(23, 4, '查看', '', 'sys:menu:list,sys:menu:info', 2, '', 0, '', 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(24, 4, '新增', '', 'sys:menu:save,sys:menu:select', 2, '', 0, '', 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(25, 4, '修改', '', 'sys:menu:update,sys:menu:select', 2, '', 0, '', 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(26, 4, '删除', '', 'sys:menu:delete', 2, '', 0, '', 0, 0);
-- 系统参数 更改为 系统参数管理和业务参数管理
--INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(27, 101, 'Params', 'sys/config', 'sys:config:list,sys:config:info,sys:config:save,sys:config:update,sys:config:delete', 1, 'config', 7, '', 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(29, 120, 'SysLogger', 'sys/log', 'sys:log:list', 1, 'log', 7, '', 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(31, 0, 'Reporting', '', '', 0, 'system', 40, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(32, 101, 'WORKSHOP', 'sys/workshops', '', 1, 'admin', 5, NULL, 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(33, 32, '查看', 'sys:workshop:list,sys:workshop:info', '', 2, 'admin', 0, NULL, 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(34, 32, '新增', 'sys:workshop:save,sys:workshop:select', '', 2, 'admin', 0, NULL, 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(35, 32, '修改', 'sys:workshop:update,sys:workshop:select', '', 2, 'admin', 0, NULL, 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(36, 0, 'LicenseConfig', '', '', 0, 'role', 4, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(71, 0, 'VehicleType', '', '', 0, 'shezhi', 1, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(72, 71, 'VehicleTypeList', 'vehicle-type/list', '', 1, 'menu', 1, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(74, 71, 'VehicleInfo', 'vehicle/list', '', 1, 'menu', 4, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(75, 101, 'Internationalization', 'language/language', '', 1, 'shezhi', 10, NULL, 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(76, 31, 'WorkshopStatistics', '', '', 0, 'menu', 50, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(77, 76, 'WorkshopOverview', 'workshop/overview', '', 1, 'tubiao', 1, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(79, 76, 'WorkshopUserOverview', 'workshop/user/overview', '', 1, 'tubiao', 3, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(80, 76, 'WorkshopList', 'workshop/index', '', 1, 'menu', 2, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(87, 79, '查看', '', 'workshop:user:page,workshop:user:group,workshop:user:groupChart,workshop:user:workshopChart,workshop:user:chart', 2, '', 0, '', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(88, 80, '列表', '', 'workshop:page,workshop:detail', 2, '', 0, '', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(89, 80, '概览', '', 'workshop:group,workshop:chart', 2, '', 0, '', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(101, 0, 'SystemSetup', '', '', 0, 'job', 80, NULL, 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(102, 31, 'ClientStatistics', '', '', 0, 'config', 51, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(106, 120, 'FileLog', 'job/file', '', 1, 'menu', 2, NULL, 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(107, 120, 'JobLog', '/job/log', '', 1, 'log', 1, NULL, 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(108, 101, 'JobList', '/job/index', '', 1, 'menu', 6, NULL, 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(109, 102, 'ClientApp', 'client/app', '', 1, 'config', 1, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(110, 102, 'ClientList', 'client/index', '', 1, 'menu', 1, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(111, 102, 'ClientOverview', 'client/overview', '', 1, 'tubiao', 0, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(112, 76, 'WorkshopUserList', 'workshop/user/index', '', 1, 'menu', 4, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(113, 31, 'DiagnosisStatistics', '', '', 0, 'tubiao', 55, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(114, 113, 'SessionHistory', 'his/session', '', 1, 'job', 0, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(115, 113, 'HisDtc', 'his/dtcAnalysis', '', 1, 'tubiao', 0, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(116, 113, 'DiagnosticFunction', 'his/reqAnalysis', '', 1, 'tubiao', 0, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(117, 75, '下载', '', 'sys:file:download,sys:file:multiLanguageExcelExport,sys:file:multiLanguageZip', 2, '', 0, '', 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(119, 75, '上传', '', 'sys:file:upload,sys:file:ExcelImport,sys:file:sqliteUpload', 2, '', 0, '', 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(120, 0, 'SystemOperation', '', NULL, 0, 'system', 81, NULL, 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(121, 120, 'MonitorList', '/monitor/database', NULL, 1, 'sql', 8, NULL, 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(122, 120, 'MonitorSql', 'monitor/sql', '', 1, 'sql', 9, NULL, 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(123, 120, 'MonitorRedis', 'monitor/redis', '', 1, 'shezhi', 10, NULL, 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(125, 113, 'FlashHistoryStats', 'flash/overview', '', 1, 'tubiao', 11, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(126, 113, 'FlashList', 'flash-list', '', 1, 'menu', 12, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(127, 113, 'FlashEcuList', 'flashECU-list', '', 1, 'menu', 13, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(129, 102, 'ClientHis', '/client/his', '', 1, 'menu', 2, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(130, 102, 'ClientAppHis', '/client/appHis', '', 1, 'menu', 2, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(132, 190, 'VehicleEcu', 'remoteVehicle/ecuList', '', 1, 'zhedie', 1, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(134, 71, 'VehiclePool', 'remoteVehicle/pool', NULL, 1, 'zhedie', 7, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(135, 139, 'RemoteDiagnosis', '/', '', 0, 'menu', 2, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(136, 135, 'DiagnosisFunction', '/remoteDiagnosisTasks/funList', '', 1, 'menu', 1, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(137, 135, 'DiagnosisCondition', '/remoteDiagnosisTasks/conditionList', '', 1, 'zhedie', 0, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(139, 0, 'Vehicle', 'adiagnostic/', '', 0, 'log', 30, '', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(140, 139, 'VehicleList', 'vlist', '', 1, 'menu', 0, '', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(141, 139, 'VehicleDID', 'diagnostic/ident', '', 1, 'menu', 2, '', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(142, 139, 'VehicleVHHS', 'diagnostic/vhhs', '', 1, 'xiangqu', 1, '', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(143, 139, 'VehicleDTC', 'diagnostic/dtc', '', 1, 'bianji', 3, '', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(144, 0, 'ECU', 'diagnostic/', '', 0, 'shezhi', 3, '', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(145, 144, 'GW', 'diagnostic/', '', 0, 'config', 1, '', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(146, 144, 'IVIBOX', 'diagnostic/', '', 0, 'config', 2, '', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(147, 144, 'ADAS', 'diagnostic/', '', 0, 'config', 3, '', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(148, 144, 'ADU', 'diagnostic/', '', 0, 'config', 4, '', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(149, 144, 'TBOX', 'diagnostic/', '', 0, 'config', 5, '', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(150, 144, 'RADAR', 'diagnostic/', '', 0, 'config', 0, '', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(151, 145, 'GWDID', 'diagnostic/ident', '', 1, 'sousuo', 2, '{"ecuName":"GW"}', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(152, 145, 'GWDTC', 'diagnostic/dtc', '', 1, 'bianji', 3, '{"ecuName":"GW"}', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(153, 146, 'IVIBOXDID', 'diagnostic/ident', '', 1, 'sousuo', 1, '{"ecuName":"IVIBOX"}', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(154, 146, 'IVIBOXDTC', 'diagnostic/dtc', '', 1, 'bianji', 2, '{"ecuName":"IVIBOX"}', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(155, 147, 'ADASDID', 'diagnostic/ident', '', 1, 'sousuo', 1, '{"ecuName":"ADAS"}', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(156, 147, 'ADASDTC', 'diagnostic/dtc', '', 1, 'bianji', 2, '{"ecuName":"ADAS"}', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(157, 148, 'ADUDTC', 'diagnostic/dtc', '', 1, 'bianji', 2, '{"ecuName":"ADAU_A"}', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(158, 148, 'ADUDID', 'diagnostic/ident', '', 1, 'sousuo', 1, '{"ecuName":"ADAU_A"}', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(159, 149, 'TBOXDTC', 'diagnostic/dtc', '', 1, 'bianji', 2, '{"ecuName":"ADAU_B"}', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(160, 149, 'TBOXDID', 'diagnostic/ident', '', 1, 'sousuo', 1, '{"ecuName":"ADAU_B"}', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(161, 150, 'RADARDID', 'diagnostic/ident', '', 1, 'sousuo', 1, '{"ecuName":"RADAR"}', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(162, 150, 'RADARDTC', 'diagnostic/dtc', '', 1, 'bianji', 2, '{"ecuName":"RADAR"}', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(163, 0, 'Version', '', '', 0, 'zhedie', 2, '', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(164, 163, 'PTX', 'version/otx', '', 1, 'menu', 2, '', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(165, 163, 'PDX', 'version/odx', 'version:odx,version:odx:select', 1, 'menu', 0, '', 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(166, 71, 'VehicleTag', 'remoteVehicle/tagList', '', 1, 'config', 9, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(167, 135, 'DiagnosisFile', '/remoteDiagnosisTasks/fileList', '', 1, 'zhedie', 3, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(168, 135, 'DiagnosticTask', '/remoteDiagnosisTasks/taskList', '', 1, 'zhedie', 4, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(169, 190, 'VehicleEcuGroup', '/remoteVehicle/ecuGroupList', '', 1, 'zhedie', 0, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(170, 36, 'product', 'license/product', NULL, 1, 'admin', 3, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(171, 71, 'VehicleVersion', '/remoteVehicle/vehicleVersionList', '', 1, 'zhedie', 6, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(172, 135, 'DiagnosticTaskMonitor', '/remoteDiagnosisTasks/taskMonitor', '', 1, 'tubiao', 5, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(173, 135, 'TaskCampaign', '/remoteDiagnosisTasks/releaseList', '', 1, 'zhedie', 4, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(174, 0, 'OnlineUpdate', ' ', '', 0, 'oss', 0, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(176, 174, 'Component', 'onlineUpdate/component', '', 1, 'zonghe', 0, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(177, 174, 'Software', 'onlineUpdate/software', NULL, 1, 'mudedi', 1, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(180, 120, 'FilePage', '/file-page', '', 1, 'menu', 0, NULL, 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(188, 144, 'ICM', '', '', 0, '', 0, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(189, 188, 'ReadDTC', '\diagnostic', '', 1, 'dangdifill', 0, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(190, 71, 'ECUManage', '/', '', 0, 'menu', 0, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(191, 190, 'ECUVersionList', '/remoteVehicle/ecuVersionList', '', 1, 'zhedie', 2, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(195, 257, 'orderdetail', 'license/orderdetail', NULL, 1, 'admin', 2, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(198, 101, 'LauncherPackageManage', 'launcher/launcher', '', 1, 'zhedie', 11, NULL, 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(199, 198, '上传', '', 'sys:file:upload-launcher', 2, '', 0, NULL, 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(200, 198, '下载', '', 'sys:file:download-launcher,sys:file:download', 2, '', 0, NULL, 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(203, 188, 'Onlin', 'aaa', '', 1, 'config', 4, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(208, 163, 'package', 'version/package', '', 1, 'zonghe', 0, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(213, 174, 'ComponentVersion', 'onlineUpdate/ComponentVersion', '', 1, 'log', 2, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(214, 174, 'SoftwareVersion', 'onlineUpdate/SoftwareVersion', '', 1, 'menu', 4, NULL, 0, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(222, 0, 'SWTS', '', '', 0, '', 0, NULL, 1, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(223, 222, 'app', '/app', '', 1, '', 0, NULL, 1, 1);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(226, 101, 'Notice', 'sys/notice', '', 1, 'editor', 12, NULL, 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(227, 36, 'feature', 'license/feature', NULL, 1, 'admin', 2, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(229, 257, 'fileUpload', '/file-upload/file-upload', '', 1, 'oss', 101, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(232, 36, 'app', 'license/app', NULL, 1, 'admin', 1, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(233, 36, 'oem', 'license/oem', NULL, 1, 'admin', 4, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(234, 36, 'type', 'license/type', NULL, 1, 'admin', 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(235, 257, 'order', 'license/order', NULL, 1, 'admin', 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(257, 0, 'LicenseData', '', '', 0, 'admin', 5, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(258, 36, 'oemlicensepool', 'license/oemlicensepool', NULL, 1, 'admin', 6, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(268, 257, 'licenseNumberState', 'license/license-number-state', NULL, 1, 'admin', 1, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(273, 257, 'floatingDeviceOnlineState', 'license/floating-device-online-state', NULL, 1, 'admin', 3, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(274, 257, 'licenseUsageState', 'license/license-usage-state', NULL, 1, 'config', 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(276, 232, '查看', NULL, 'license:app:list,license:app:info', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(277, 232, '新增', NULL, 'license:app:save', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(278, 232, '删除', NULL, 'license:app:delete', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(279, 233, '查看', NULL, 'license:oem:list,license:oem:info', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(280, 233, '新增', NULL, 'license:oem:save', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(281, 233, '修改', NULL, 'license:oem:update', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(282, 233, '删除', NULL, 'license:oem:delete', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(283, 234, '查看', NULL, 'license:type:list,license:type:info', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(284, 234, '新增', NULL, 'license:type:save', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(285, 234, '修改', NULL, 'license:type:update', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(286, 234, '删除', NULL, 'license:type:delete', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(287, 170, '查看', NULL, 'license:product:list,license:product:info', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(288, 170, '新增', NULL, 'license:product:save', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(289, 170, '修改', NULL, 'license:product:update', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(290, 170, '删除', NULL, 'license:product:delete', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(291, 227, '查看', NULL, 'license:feature:list,license:feature:info', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(292, 227, '新增', NULL, 'license:feature:save', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(293, 227, '修改', NULL, 'license:feature:update', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(294, 227, '删除', NULL, 'license:feature:delete', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(295, 235, '查看', NULL, 'license:order:list,license:order:info', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(296, 235, '新增', NULL, 'license:order:save', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(297, 235, '修改', NULL, 'license:order:update', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(298, 235, '删除', NULL, 'license:order:delete', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(299, 195, '查看', NULL, 'license:orderdetail:list,license:orderdetail:info', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(300, 195, '新增', NULL, 'license:orderdetail:save', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(301, 232, '修改', NULL, 'license:app:update', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(302, 195, '删除', NULL, 'license:orderdetail:delete', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(303, 195, '修改', NULL, 'license:orderdetail:update', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(304, 258, '查看', NULL, 'license:oemlicensepool:list,license:oemlicensepool:info', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(305, 258, '新增', NULL, 'license:oemlicensepool:save', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(306, 258, '修改', NULL, 'license:oemlicensepool:update', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(307, 258, '删除', NULL, 'license:oemlicensepool:delete', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(308, 268, '查看', NULL, 'license:licenseNumberState:list,license:licenseNumberState:info', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(309, 268, '新增', NULL, 'license:licenseNumberState:save', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(310, 268, '修改', NULL, 'license:licenseNumberState:update', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(311, 268, '删除', NULL, 'license:licenseNumberState:delete', 2, NULL, 0, NULL, 0, 2);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(329, 101, 'ServiceParam', '/sys/serviceConfig', 'sys:config:list,sys:config:info,sys:config:save,sys:config:update,sys:config:delete', 1, 'shezhi', 7, NULL, 0, 0);
INSERT INTO public.sys_menu (menu_id, parent_id, "name", url, perms, "type", icon, order_num, query, use_to, owning_system) VALUES(330, 101, 'SystemParam', '/sys/systemConfig', 'sys:config:list,sys:config:info,sys:config:save,sys:config:update,sys:config:delete', 1, 'shezhi', 7, NULL, 0, 1);

INSERT INTO sys_user
( user_id ,username, chinese_name, english_name, emp_no, workshop_id, workshop_name, "password", salt, email, mobile, status, create_user_id, create_time, update_time,oem_code)
values
(0, 'DsaSuperAdmin', 'DsaDiagnostician', '', '10000', 0, '', '779406814fb66c92b8b83214b086c330572b6d7fc7c9973f69d50f3c318b0aed', 'cjsk2bq216auT3WRLnh4', 'dsadiagnostician@dsa.com.cn', '13548124819', 1,null , '2023-08-22 10:02:03.669', null,'bezok6yyy44t'),
(31,'admin','管理员','','10000',0,'','4926696163b6bc5e509a4cc28417bea9a3b32eac3d751f52f8ba077d84ab5cef','XVZrAcMc0T2kEewTLrJg','1@dsa.com.cn','11111111111',1,0,'2022-10-25 10:35:20.196',NULL,'bezok6yyy44t'),
(1,'SuperAdmin','SuperAdmin','','10000',0,'','e416bf6aec99582ddafc7e333488ef5c3e6e673f63527f7bd3815fcb2c5b2f34','xGEAC7sBb8ndl6rrBIUq','1@1.com','11111111111',1,27,'2022-11-28 10:37:31.322',NULL,'bezok6yyy44t'),
(38,'ywc','ywc','','10000',0,'','06feb1b27bf14ad6f382d3667ff10e6d95b660e6cd1f5972ad92070f5fba4079','JmcW7UbKDusOYwjjNQBU','ywc@dsa.com','12312312311',1,0,'2022-12-28 14:07:06.745',NULL,'bezok6yyy44t'),
(39,'ldm','ldm','','10000',0,'金桥4S店','e11e93345c409514be2a399773ce45e4192cf01852094f598b12c931bb3cd1d6','3f3wzRzO6ojDDF5nhj3L','ldm@dsa.com','12312312311',1,0,'2022-12-28 15:03:12.098',NULL,'bezok6yyy44t'),
(27,'hkj','hkj','','10000',0,'','9851b31f1087b32617021b2029955bc9c5f28b7b6e8fa24188b173226ca2a7cb','tApJgE3ZhPsOAPkrNLN7','1@qq.com','11111111111',1,0,'2022-10-21 11:24:15.776',NULL,'bezok6yyy44t'),
(52,'cyj','cyj','','10000',0,'','0cf006f542267418b40d1120e37902cbaa8dd6cc080bc92cb4eb45b0d689a3b3','XdW8yKDuRzarmakKiASB','cyj@dsa.com.cn','11111111111',1,0,'2023-05-11 10:39:09.468',NULL,'bezok6yyy44t'),
(44,'qzq','qzq','','10000',0,'上海松江4S店','c6758d35d493e3803126e8bf24142c089b60cc3ec852e6828c69d34786a80a49','RK2xmTjudhZD3WJ7HD9e','aaa@163.com','11111111111',1,0,'2023-04-04 16:08:10.661',NULL,'bezok6yyy44t'),
(47,'dwl','dwl','','10000',0,'','1172d8604d309b5b08d588067bef0da2407b27a38dd2305a404d697d6a7133e7','1CmK6FWsa5PITGi4I0WS','dwl@dsa.com.cn','11111111111',1,27,'2023-04-11 09:10:04.955',NULL,'bezok6yyy44t'),
(33,'cj','cj','','10000',0,'','110341dd054d695aa5fcb77f0d43a729b8d73ec061f50979961041f3c1e4b463','roNJJUu9eVgKqaRluclG','1@1.com','11111111111',1,0,'2022-11-17 12:09:34.861',NULL,'bezok6yyy44t'),
(46,'Diagnostic001','Diagnostic001','','10000',0,'金桥4S店','c2d3c0ea65dc75f1f6915e87217478b3062afa3612e704a4ee9908f65b9cf6b8','CJhWNGNze5BDDCicWJ5c','123@123.com','11111111111',1,0,'2023-04-10 17:25:57.582',NULL,'bezok6yyy44t');

INSERT INTO public.sys_role (role_id,role_name,remark,create_user_id,create_time) VALUES
	 (47,'Diagnostic','诊断用户',0,'2023-01-11 20:11:17.416'),
	 (44,'SuperAdmin','超级管理员',0,'2022-10-21 11:43:32.075'),
	 (11,'Diagnostician','诊断专家',0,'2021-08-02 11:56:30'),
	 (1,'Admin','管理员',0,'2021-07-16 14:33:14'),
	 (46,'Developer','Developer',0,'2023-01-11 16:04:34.613');
	 
	 
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (4,31),
	 (4,3),
	 (4,-666666),
	 (0,31),
	 (0,1),
	 (5,31),
	 (5,3),
	 (5,-666666),
	 (6,31),
	 (6,3);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (6,-666666),
	 (7,31),
	 (7,3),
	 (7,-666666),
	 (0,5),
	 (0,6),
	 (2,102),
	 (2,109),
	 (2,110),
	 (2,111);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (2,71),
	 (2,72),
	 (2,74),
	 (0,27),
	 (0,30),
	 (8,31),
	 (8,3),
	 (8,-666666),
	 (9,31),
	 (9,3);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (9,-666666),
	 (10,31),
	 (10,3),
	 (10,-666666),
	 (0,29),
	 (0,15),
	 (0,16),
	 (0,17),
	 (0,18),
	 (0,19);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (0,20),
	 (0,21),
	 (0,22),
	 (0,23),
	 (0,24),
	 (0,25),
	 (0,26),
	 (0,7),
	 (0,8),
	 (0,9);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (0,10),
	 (0,11),
	 (0,12),
	 (0,13),
	 (0,14),
	 (0,2),
	 (0,4),
	 (0,3),
	 (0,76),
	 (0,77);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (0,79),
	 (0,80),
	 (0,81),
	 (42,71),
	 (42,72),
	 (42,74),
	 (42,2),
	 (42,76),
	 (42,77),
	 (42,80);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (42,-666666),
	 (42,31),
	 (NULL,-666666),
	 (3,-666666),
	 (2,-666666),
	 (1,76),
	 (1,77),
	 (1,112),
	 (1,79),
	 (1,87);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (1,80),
	 (1,89),
	 (1,88),
	 (1,113),
	 (1,125),
	 (1,127),
	 (1,126),
	 (1,116),
	 (1,115),
	 (1,114);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (1,135),
	 (51,174),
	 (51,176),
	 (51,177),
	 (11,163),
	 (1,168),
	 (1,172),
	 (1,167),
	 (1,136),
	 (1,137);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (1,173),
	 (1,140),
	 (1,141),
	 (1,101),
	 (1,4),
	 (1,25),
	 (1,23),
	 (1,24),
	 (1,26),
	 (1,2);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (1,15),
	 (1,16),
	 (1,18),
	 (1,17),
	 (1,3),
	 (1,19),
	 (1,20),
	 (1,21),
	 (1,22),
	 (1,27);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (1,32),
	 (1,33),
	 (1,34),
	 (1,35),
	 (1,108),
	 (1,75),
	 (1,119),
	 (1,117),
	 (1,122),
	 (1,106);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (1,107),
	 (1,29),
	 (1,121),
	 (1,123),
	 (1,-666666),
	 (1,31),
	 (1,139),
	 (1,120),
	 (51,-666666),
	 (47,144);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (47,146),
	 (47,153),
	 (47,154),
	 (47,147),
	 (47,156),
	 (47,155),
	 (47,150),
	 (47,162),
	 (47,161),
	 (47,149);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (47,160),
	 (47,159),
	 (47,188),
	 (47,189),
	 (47,148),
	 (47,157),
	 (47,158),
	 (47,145),
	 (47,152),
	 (47,151);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (47,139),
	 (47,135),
	 (47,168),
	 (47,172),
	 (47,167),
	 (47,136),
	 (47,137),
	 (47,173),
	 (47,142),
	 (47,143);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (11,165),
	 (11,164),
	 (11,144),
	 (11,146),
	 (11,153),
	 (11,154),
	 (11,147),
	 (11,156),
	 (11,155),
	 (11,150);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (11,162),
	 (11,161),
	 (11,149),
	 (11,160),
	 (11,159),
	 (11,148),
	 (11,157),
	 (11,158),
	 (11,145),
	 (11,152);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (11,151),
	 (11,139),
	 (11,142),
	 (11,143),
	 (11,135),
	 (11,173),
	 (11,136),
	 (11,168),
	 (11,137),
	 (11,167);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (11,172),
	 (11,140),
	 (11,141),
	 (11,113),
	 (11,125),
	 (11,127),
	 (11,126),
	 (11,116),
	 (11,115),
	 (11,114);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (11,-666666),
	 (47,140),
	 (47,141),
	 (47,-666666),
	 (46,76),
	 (46,77),
	 (46,112),
	 (46,79),
	 (46,87),
	 (46,80);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (46,89),
	 (46,88),
	 (46,102),
	 (46,129),
	 (46,110),
	 (46,111),
	 (46,109),
	 (46,130),
	 (46,163),
	 (46,165);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (46,164),
	 (46,144),
	 (46,146),
	 (46,153),
	 (46,154),
	 (46,147),
	 (46,156),
	 (46,155),
	 (46,150),
	 (46,162);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (46,161),
	 (46,149),
	 (46,160),
	 (46,159),
	 (46,188),
	 (46,189),
	 (46,148),
	 (46,157),
	 (46,158),
	 (46,145);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (46,152),
	 (46,151),
	 (46,71),
	 (46,166),
	 (46,190),
	 (46,191),
	 (46,132),
	 (46,169),
	 (46,171),
	 (46,72);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (46,134),
	 (46,74),
	 (46,139),
	 (46,135),
	 (46,168),
	 (46,172),
	 (46,167),
	 (46,136),
	 (46,137),
	 (46,173);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (46,142),
	 (46,143),
	 (46,140),
	 (46,141),
	 (46,101),
	 (46,27),
	 (46,75),
	 (46,119),
	 (46,117),
	 (46,108);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (46,122),
	 (46,121),
	 (46,123),
	 (46,106),
	 (46,107),
	 (46,29),
	 (46,31),
	 (46,2),
	 (46,15),
	 (46,16);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (46,18),
	 (46,17),
	 (46,3),
	 (46,19),
	 (46,20),
	 (46,21),
	 (46,22),
	 (46,4),
	 (46,25),
	 (46,23);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (46,24),
	 (46,26),
	 (46,113),
	 (46,125),
	 (46,127),
	 (46,126),
	 (46,116),
	 (46,115),
	 (46,114),
	 (46,-666666);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (46,120),
	 (44,166),
	 (44,132),
	 (44,171),
	 (44,72),
	 (44,134),
	 (44,74),
	 (44,139),
	 (44,135),
	 (44,168);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (44,172),
	 (44,167),
	 (44,136),
	 (44,137),
	 (44,173),
	 (44,142),
	 (44,143),
	 (44,140),
	 (44,141),
	 (44,101);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (44,27),
	 (44,75),
	 (44,119),
	 (44,117),
	 (44,108),
	 (44,122),
	 (44,121),
	 (44,123),
	 (44,106),
	 (44,107);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (44,29),
	 (44,31),
	 (44,2),
	 (44,15),
	 (44,16),
	 (44,18),
	 (44,17),
	 (44,3),
	 (44,19),
	 (44,20);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (44,21),
	 (44,22),
	 (44,4),
	 (44,25),
	 (44,23),
	 (44,24),
	 (44,26),
	 (44,113),
	 (44,125),
	 (44,127);
INSERT INTO public.sys_role_menu (role_id,menu_id) VALUES
	 (44,126),
	 (44,116),
	 (44,115),
	 (44,114),
	 (44,-666666),
	 (44,71),
	 (44,190),
	 (44,120);


	 
	 
INSERT INTO public.sys_workshop
(workshop_id, parent_id, "name", order_num, del_flag, address)
VALUES(1, 2, 'Shanghai', 0, 0, '');
INSERT INTO public.sys_workshop
(workshop_id, parent_id, "name", order_num, del_flag, address)
VALUES(2, 0, 'East China', 0, 0, '');
INSERT INTO public.sys_workshop
(workshop_id, parent_id, "name", order_num, del_flag, address)
VALUES(3, 1, 'Pudong', 2, 0, '');
INSERT INTO public.sys_workshop
(workshop_id, parent_id, "name", order_num, del_flag, address)
VALUES(4, 1, 'Jing ''an', 0, 0, '');
INSERT INTO public.sys_workshop
(workshop_id, parent_id, "name", order_num, del_flag, address)
VALUES(5, 1, 'Xu Hui', 1, 0, '');
INSERT INTO public.sys_workshop
(workshop_id, parent_id, "name", order_num, del_flag, address)
VALUES(6, 3, 'Lujiazui 4S shop', 0, 0, '');
INSERT INTO public.sys_workshop
(workshop_id, parent_id, "name", order_num, del_flag, address)
VALUES(10, 1, 'Songjiang', 4, 0, '');
INSERT INTO public.sys_workshop
(workshop_id, parent_id, "name", order_num, del_flag, address)
VALUES(12, 10, 'Songjiang 4S shop', 0, 0, '');
INSERT INTO public.sys_workshop
(workshop_id, parent_id, "name", order_num, del_flag, address)
VALUES(7, 4, 'Jing ''an Temple 4S shop', 0, 0, '');
INSERT INTO public.sys_workshop
(workshop_id, parent_id, "name", order_num, del_flag, address)
VALUES(8, 5, 'Xujiahui 4S shop', 0, 0, '');


INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(1, 'cron2', '0 */2 * * * ?', 1, '2 minute', NULL, NULL, 0, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(2, 'cron4', '0 */10 * * * ?', 1, '10 minute', NULL, NULL, 0, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(3, 'cron1', '0 */1 * * * ?', 1, '1 minute', NULL, NULL, 0, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(4, 'cron3', '0 */5 * * * ?', 1, '5 minute', NULL, NULL, 0, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(5, 'cron5', '0 */20 * * * ?', 1, '20 minute', NULL, NULL, 0, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(6, 'cron6', '0 */30 * * * ?', 1, '30 minute', NULL, NULL, 0, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(7, 'cron7', '0 * */1 * * ?', 1, '1 hour', NULL, NULL, 0, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(8, 'BJ', 'BJ', 1, '', 'V_SalesAreas', '', 0, 1);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(9, 'APP', 'APP', 1, 'APP', 'Version_FlashType', '', 0, 1);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(10, 'DATA', 'DATA', 1, 'DATA', 'Version_FlashType', '', 0, 1);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(11, 'develop', '0', 1, '', 'status', 'status.dev', 0, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(12, 'test', '1', 1, '', 'status', 'status.test', 0, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(13, 'close', '2', 1, '', 'status', 'status.close', 0, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(14, 'add', '0', 1, '', 'ecu_version_file', 'status.add', 0, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(15, 'all', '1', 1, '', 'ecu_version_file', 'status.all', 0, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(16, 'release', '3', 1, '', 'status', 'status.release', 0, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(17, 'SH', 'SH', 1, '', 'V_PlaceOfProduction', '', 0, 1);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(18, 'BJ', 'BJ', 1, '', 'V_PlaceOfProduction', '', 0, 1);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(19, 'EXE', 'EXE', 1, 'EXE', 'Version_FlashType', '', 0, 1);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(20, 'SBL', 'SBL', 1, 'SBL', 'Version_FlashType', '', 0, 1);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(21, 'code1', '1', 1, '1', 'ConditionCode', '', 0, 1);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(22, 'code2', '2', 1, '2', 'ConditionCode', '', 0, 1);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(23, 'code3', '3', 1, '3', 'ConditionCode', '', 0, 1);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(24, 'code4', '4', 1, '4', 'ConditionCode', NULL, 0, 1);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(25, 'code5', '5', 1, '5', 'ConditionCode', NULL, 0, 1);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(26, 'LOCAL_STORAGE_CONFIG_KEY', '{"basePath":"/usr/file/data","dirFormat":"yyyy/MM/dd","hasRecord":true,"type":"local","workDir":"/usr/file"}', 1, '本地文件保存目录', 'LOCAL_STORAGE_CONFIG_KEY', 'LOCAL_STORAGE_CONFIG_KEY', 0, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(28, 'REPORTING_CLIENT_APP', '[{"id":100,"name":"Prodis.SWTS","type":"software"},{"id":200,"name":"Pdu-Api","type":"component"},{"id":202,"name":"Swts","type":"component"},{"id":201,"name":"Mcd-Kernel","type":"component"},{"id":203,"name":"I18n","type":"component"},{"id":204,"name":"Otx-Engine","type":"component"},{"id":205,"name":"Config","type":"component"}]', 1, 'reporting要分析的APP', 'REPORTING_CLIENT_APP', 'REPORTING_CLIENT_APP', 0, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(29, 'SWTS', '100', 1, 'SWTS', 'REPORTING_CHART_APPS', '', 10, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(30, 'Swts', '202', 1, '', 'REPORTING_CHART_APPS', '', 100, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(31, 'match', 'MATCH', 1, 'match', 'task_detail_type', 'taskDetail.MATCH', 0, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(32, 'Mcd-Kernel', '201', 1, '', 'REPORTING_CHART_APPS', '', 50, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(33, 'remoteVerification', 'RemoteVerification', 1, 'remoteVerification', 'task_detail_type', 'taskDetail.RemoteVerification', 8, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(34, 'uploadLog', 'UPLOAD:LOG', 1, 'uploadLog', 'task_detail_type', 'taskDetail.UPLOAD:LOG', 10, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(35, 'conditionVerification', 'ConditionalVerification', 1, 'conditionVerification', 'task_detail_type', 'taskDetail.ConditionalVerification', 7, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(36, 'gradient', 'GradientCheck', 1, 'gradient', 'task_detail_type', 'taskDetail.GradientCheck', 9, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(37, 'HIFI SWTS', '170', 1, '', 'REPORTING_CHART_APPS', '', 0, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(38, 'mcdkernel2', '169', 1, '', 'REPORTING_CHART_APPS', '24', 0, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(39, 'execute', 'ConfirmExecution:EXECUTE', 1, 'execute', 'task_detail_type', 'taskDetail.ConfirmExecution:EXECUTE', 1, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(40, 'download', 'DOWNLOAD', 1, 'download', 'task_detail_type', 'taskDetail.DOWNLOAD', 2, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(41, 'cancel', 'ConfirmExecution:CANCEL', 1, 'cancel', 'task_detail_type', 'taskDetail.ConfirmExecution:CANCEL', 3, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(42, 'reject', 'ConfirmExecution:REJECT', 1, 'reject', 'task_detail_type', 'taskDetail.ConfirmExecution:REJECT', 4, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(43, 'termination', 'TERMINATION', 1, 'termination', 'task_detail_type', 'taskDetail.TERMINATION', 5, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(44, 'result', 'DiagnosticResult', 1, 'result', 'task_detail_type', 'taskDetail.DiagnosticResult', 6, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(45, 'swts2', '167', 1, '', 'REPORTING_CHART_APPS', '25', 0, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(46, 'EcuSoftwareVersionDID', 'F189', 1, 'ECU版本号', 'FLASH', '', 0, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(47, 'VehicleSoftwareVersionDID', 'F1AA', 1, '整车版本号', 'FLASH', '', 0, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(48, 'KEY_ECU', 'VDC', 1, '主ECU名称', 'FLASH', '', 0, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(49, 'PartNumber', 'HW187', 1, '', 'ECU_DID', '', 0, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(50, 'EcuHardware', 'HW188', 1, '', 'ECU_DID', '', 0, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(51, 'EcuSoftwareNumber', 'HW189', 1, '', 'ECU_DID', '', 0, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(52, 'EcuCalibrationNumber', 'HW190', 1, '', 'ECU_DID', '', 0, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(53, 'DRV', 'DRV', 1, '', 'Version_FlashType', '', 0, 1);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(54, 'ECU_SELECT', 'BMS,ECU,ABC,ADU,EV_ABM,EV_VDC,EV_BMS', 1, 'ECU list for select', 'REPORTING', '', 0, 0);
INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(27, 'LOCAL_CATEGORY_CONFIG_KEY', '{"type":"local","workDirs":{"session":"/usr/file/session_log/upload","online_update":"/usr/file/online_update","flash":"/usr/file/flash/upload"}}', 1, '日志分类', 'LOCAL_CATEGORY_CONFIG_KEY', 'LOCAL_CATEGORY_CONFIG_KEY', 0, 0);

INSERT INTO public.sys_config (id, param_key, param_value, status, remark, param_group, language_key, order_num, "type") VALUES(54, 'EcuPartNumberDID', 'F187', 1, 'ECU零件号', 'FLASH', '', 0, 0);

-- 需求变更，OTX和ODX名称修改为PDX和PTX
--update sys_menu set "name" = 'PTX' where menu_id =164;
--update sys_menu set "name" = 'PDX' where menu_id =165;