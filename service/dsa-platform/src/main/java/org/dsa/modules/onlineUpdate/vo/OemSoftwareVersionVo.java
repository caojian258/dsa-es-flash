package org.dsa.modules.onlineUpdate.vo;

import lombok.Data;

@Data
public class OemSoftwareVersionVo {

    /**
     * oem
     */
    private String  oemName;
    /**
     * 软件名称
     */
    private String  softwareName;
    /**
     * 软件版本
     */
    private String  softwareVersion;
}
