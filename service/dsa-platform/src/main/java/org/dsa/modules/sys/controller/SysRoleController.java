package org.dsa.modules.sys.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.dsa.common.annotation.SysLog;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.PageUtils;
import org.dsa.common.utils.R;
import org.dsa.common.validator.ValidatorUtils;
import org.dsa.modules.sys.entity.SysRoleEntity;
import org.dsa.modules.sys.service.SysRoleMenuService;
import org.dsa.modules.sys.service.SysRoleService;
import org.dsa.modules.sys.service.SysRoleWorkshopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 角色管理
 *
 */
@RestController
@RequestMapping("/sys/role")
public class SysRoleController extends AbstractController {
	@Autowired
	private SysRoleService sysRoleService;

	@Autowired
	private SysRoleMenuService sysRoleMenuService;

	@Autowired
	private SysRoleWorkshopService sysRoleWorkshopService;

	/**
	 * 角色列表
	 */
	@GetMapping("/list")
	@RequiresPermissions("sys:role:list")
	public R list(@RequestParam Map<String, Object> params){
		//如果不是超级管理员，则只查询自己创建的角色列表
		if(!getUserName().equals(Constant.SUPER_ADMIN_USER_NAME) && !getRoleList().contains(Constant.ADMIN_ROLE)){
			params.put("createUserId", getUserId());
		}

		PageUtils page = sysRoleService.queryPage(params);

		return R.ok().put("page", page);
	}
	
	/**
	 * 角色列表
	 */
	@GetMapping("/select")
	public R select(){
		Map<String, Object> params = new HashMap<>();
		
		//如果不是超级管理员，则只查询自己所拥有的角色列表
		if(!getUserName().equals(Constant.SUPER_ADMIN_USER_NAME) && !getRoleList().contains(Constant.ADMIN_ROLE)){
			params.put("create_user_id", getUserId());
		}
		// 按照Id排序
		List<SysRoleEntity> list = sysRoleService.listByMap(params).stream()
				.sorted(Comparator.comparing(SysRoleEntity::getRoleId)).collect(Collectors.toList());

		return R.ok().put("list", list);
	}



	/**
	 * 角色信息
	 */
	@GetMapping("/info/{roleId}")
	@RequiresPermissions("sys:role:info")
	public R info(@PathVariable("roleId") Long roleId){
		SysRoleEntity role = sysRoleService.getById(roleId);
		
		//查询角色对应的菜单
		List<Long> menuIdList = sysRoleMenuService.queryMenuIdList(roleId);
		role.setMenuIdList(menuIdList);

		//查询角色对应的部门
		List<Long> workshopIdList = sysRoleWorkshopService.queryWorkshopIdList(new Long[]{roleId});
		role.setWorkshopIdList(workshopIdList);

		
		return R.ok().put("role", role);
	}
	
	/**
	 * 保存角色
	 */
	@PostMapping("/save")
	@SysLog("保存角色")
	@RequiresPermissions("sys:role:save")
	public R save(@RequestBody SysRoleEntity role){
		ValidatorUtils.validateEntity(role);
		if(!getUserName().equals(Constant.SUPER_ADMIN_USER_NAME) && !getRoleList().contains(Constant.ADMIN_ROLE)){
			role.setCreateUserId(getUserId());
		}else{
			role.setCreateUserId(0L);
		}
		// 名称重复性校验
		LambdaQueryWrapper<SysRoleEntity> listQuery = new QueryWrapper<SysRoleEntity>().lambda().eq(SysRoleEntity::getRoleName, role.getRoleName());
		List<SysRoleEntity> list = sysRoleService.list(listQuery);
		if(list.size()>0){
			return R.error(Constant.Msg.ROLE_NAME_EXIST_ERROR);
		}
		sysRoleService.saveRole(role);
		
		return R.ok();
	}
	
	/**
	 * 修改角色
	 */
	@PostMapping("/update")
	@SysLog("修改角色")
	@RequiresPermissions("sys:role:update")
	public R update(@RequestBody SysRoleEntity role){
		ValidatorUtils.validateEntity(role);
		if(!getUserName().equals(Constant.SUPER_ADMIN_USER_NAME) && !getRoleList().contains(Constant.ADMIN_ROLE)){
			role.setCreateUserId(getUserId());
		}
//		role.setCreateUserId(getUserId());
		// 名称重复性校验
		LambdaQueryWrapper<SysRoleEntity> listQuery = new QueryWrapper<SysRoleEntity>().lambda().eq(SysRoleEntity::getRoleName, role.getRoleName()).ne(SysRoleEntity::getRoleId, role.getRoleId());
		List<SysRoleEntity> list = sysRoleService.list(listQuery);
		if(list.size()>0){
			return R.error(Constant.Msg.ROLE_NAME_EXIST_ERROR);
		}
		sysRoleService.update(role);
		return R.ok();
	}
	
	/**
	 * 删除角色
	 */
	@PostMapping("/delete")
	@SysLog("删除角色")
	@RequiresPermissions("sys:role:delete")
	public R delete(@RequestBody Long[] roleIds){
		if (sysRoleService.hasAdmin(roleIds)) {
			return R.error("SuperAdmin无法删除");
		}
		sysRoleService.deleteBatch(roleIds);
		return R.ok();
	}
}
