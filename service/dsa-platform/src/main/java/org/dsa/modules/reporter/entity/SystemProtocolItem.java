package org.dsa.modules.reporter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain = true)
@TableName("client_os_protocol_item")
public class SystemProtocolItem implements Serializable {

    @TableId(type = IdType.AUTO)
    private Long id;

    /***
     * 经销商ID
     */
    private String workshop;

    /***
     * 客户端ID
     */
    private String pcid;

    /***
     * 类型
     */
    private String type;

    /***
     * 严重程度
     */
    private String severity;

    /***
     * 上传的日期
     */
    private String collectDate;

    /***
     * 上传的时间
     */
    private Date collectTime;

    /***
     * 上传的时间戳
     */
    private String timestampText;

    /***
     * 参考ID
     */
    private String referenceId;

    /***
     * 消息
     */
    private String message;

    /***
     * 最近一批的数据tag
     */
    private String tag;

}
