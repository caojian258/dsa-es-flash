package org.dsa.modules.reporter.vo.diag;

import lombok.Data;

@Data
public class SessionReportReVo {

    private String sessionId;
}
