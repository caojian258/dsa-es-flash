package org.dsa.modules.sys.service.impl;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.dsa.modules.sys.service.SysTranslationsService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
class SysTranslationsServiceImplTest {

    @Autowired
    SysTranslationsService sysTranslationsService;

    @Test
    void translationTis() {
        String ti = "ti-6bf8b497-e89f-4cdf-b1e8-6b7c3f86ca95";
        List<String> tis = new ArrayList<>();
        tis.add("ti-d7dba02d-99d3-49b3-9e6c-06c743b2de46");
        tis.add(ti);
        String locale = "en-US";
        Map<String, String> tmap = sysTranslationsService.translationTis(tis, locale);
        System.out.println(JSONObject.toJSONString(tmap));
    }

    @Test
    void translationTi() {
        String ti = "ti-6bf8b497-e89f-4cdf-b1e8-6b7c3f86ca95";
        String locale = "en-US";
        String t = sysTranslationsService.translationTi(ti, locale);
        System.out.println(t);
    }
}