package org.dsa.modules.reporter.service;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.freemarker.FreeMarkerProperties;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

@Service
public class HtmlGenerator {     

	@Autowired
	FreeMarkerProperties freeMarkerProperties;

    /**   
     * Generate html string.   
     *    
     * @param template   the name of freemarker template.
     * @param variables  the data of teamlate.   
     * @return htmlStr   
     * @throws IOException 
     * @throws TemplateException 
     * @throws Exception   
     */    
    public String generate(String template, Map<String,Object> variables) throws IOException, TemplateException{     
        BufferedWriter writer = null;   
        String htmlContent = "";
        try{
        	Configuration config = new Configuration(Configuration.VERSION_2_3_23);

//			config.setDirectoryForTemplateLoading(new File(HtmlGenerator.class.getResource("/templates").getFile()));
			String tmp = "/templates";

			if(null != freeMarkerProperties.getTemplateLoaderPath() && freeMarkerProperties.getTemplateLoaderPath().length>0){
				tmp = freeMarkerProperties.getTemplateLoaderPath()[0].replaceAll("classpath:", "");
			}
        	config.setClassForTemplateLoading(HtmlGenerator.class, tmp);
			Template tp = config.getTemplate(template);
        	StringWriter stringWriter = new StringWriter();
        	writer = new BufferedWriter(stringWriter);
			config.setDefaultEncoding(freeMarkerProperties.getCharset().toString());
        	tp.setEncoding(freeMarkerProperties.getCharset().toString());
        	tp.process(variables, writer);
        	htmlContent = stringWriter.toString();     
        	writer.flush();
        }catch (IOException e){
			e.printStackTrace();
		} finally{
        	if(writer!=null)
        		writer.close();     
        }
        return htmlContent;     
    }
}