package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class EcuVersionDto implements Serializable {

    private String ecu;

    /**
     * 多语言翻译标记
     */
    private String ti;

    /***
     * ECU版本信息名称
     */
    private String name;

    /***
     * ECU版本信息具体值
     */
    private String value;


    private String tiValue;

}
