package org.dsa.modules.diagnostic.entity.mqtt.callback;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * mq body data DTC实体类
 *
 */
@Data
public class MqBodyDataDTCVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ECU名称数组
     */
    private String ecuName;
    /**
     * 故障码Code
     */
    private String dtcCode;
    /**
     * 故障码CodeHex
     */
    private String dtcCodeHex;
    /**
     * 故障码Code , Int型
     */
    private String dtcCode_Int;
    /**
     * 故障码描述多语言标识
     */
    private String ti;
    /**
     * 故障码描述
     */
    private String description;
    /**
     * 故障码状态
     */
    private String status;
    /**
     * 故障码状态
     */
    private String tiStatus;
    /**
     * 冻结帧
     */
    private List<MqBodyDataDTCDataGridVO> dTCDataGridVOs;

    /**
     * snapshotEnable是否展开冻结帧
     */
    private Boolean snapshotEnable;

}
