package org.dsa.modules.reporter.util;

import lombok.extern.slf4j.Slf4j;
import org.dsa.modules.sys.service.SysTranslationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class LocalUtil {


    private static MessageSource messageSource;

    public LocalUtil(MessageSource messageSource)
    {
        LocalUtil.messageSource = messageSource;
    }

    public static String get(String msgKey)
    {
        try
        {
            return messageSource.getMessage(msgKey, null, LocaleContextHolder.getLocale());
        }
        catch (Exception e)
        {
            log.info(e.getMessage());
            return msgKey;
        }
    }
}
