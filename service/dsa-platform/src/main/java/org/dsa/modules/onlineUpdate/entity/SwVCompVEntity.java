package org.dsa.modules.onlineUpdate.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * 软件版本关联组件版本
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@TableName("o_sw_v_comp_v")
public class SwVCompVEntity{

	@TableId(type = IdType.AUTO)
	private Long id;
	/**
	 * 软件id
	 */
	private Long softwareId;
	/**
	 * 软件版本id
	 */
	private Long softwareVersionId;
	/**
	 * 依赖的组件id
	 */
	private Long componentId;
	/**
	 * 依赖的组件版本id
	 */
	private Long componentVersionId;
}
