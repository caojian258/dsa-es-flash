package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.entity.SessionEventLog;

@Mapper
public interface SessionEventMapper extends BaseMapper<SessionEventLog> {

    public SessionEventLog selectByVinSessionId(@Param("vinCode") String vinCode, @Param("sessionId") String SessionId);
}
