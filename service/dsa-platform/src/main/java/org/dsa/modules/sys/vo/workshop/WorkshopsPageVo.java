package org.dsa.modules.sys.vo.workshop;

import lombok.Data;
import org.dsa.modules.reporter.pojo.PageParam;

@Data
public class WorkshopsPageVo extends PageParam {

    private String country;

    private String region;

    private String name;

    private String province;

    private String city;

    private String code;

    private Integer delFlag;
}
