package org.dsa.modules.sys.controller;

import org.apache.commons.lang.StringUtils;
import org.dsa.common.annotation.SysLog;
import org.dsa.common.utils.PageUtils;
import org.dsa.common.utils.R;
import org.dsa.modules.sys.dto.SysNoticeDTO;
import org.dsa.modules.sys.entity.FileEntity;
import org.dsa.modules.sys.entity.SysNoticeEntity;
import org.dsa.modules.sys.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author weishunxin
 * @since 2023-06-20
 */
@RestController
@RequestMapping("/sys/notice")
public class NoticeController {

    private static final Set<String> IMAGES = new HashSet<String>(){{
        add("jpg");
        add("jpeg");
        add("png");
    }};

    @Autowired
    private NoticeService noticeService;

    @PostMapping("/upload")
    public R uploadNotice(@RequestParam("file") MultipartFile file) {
        if (file == null || StringUtils.isEmpty(file.getOriginalFilename())) {
            return R.error("上传的文件信息不能为空");
        }

        int index = file.getOriginalFilename().lastIndexOf(".");
        String img = file.getOriginalFilename().substring(index + 1);
        if (!IMAGES.contains(img.toLowerCase())) {
            return R.error("请导入jpg、jpeg、png格式的图片");
        }

        FileEntity fileEntity = noticeService.uploadNotice(file);
        return R.ok().put("data", fileEntity);
    }

    @SysLog("新增公告")
    @PostMapping("/save")
    public R saveNotice(@RequestBody SysNoticeDTO noticeDTO) {
        noticeService.saveNotice(noticeDTO);
        return R.ok();
    }

    @SysLog("修改公告")
    @PostMapping("/update")
    public R updateNotice(@RequestBody SysNoticeEntity entity) {
        noticeService.updateNotice(entity);
        return R.ok();
    }

    @SysLog("删除公告")
    @DeleteMapping("/delete")
    public R deleteNotice(@RequestBody Integer[] ids) {
        noticeService.deleteNotice(ids);
        return R.ok();
    }

    @GetMapping("/list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = noticeService.pageList(params);
        return R.ok().put("page", page);
    }

    @GetMapping("/get/{id}")
    public R get(@PathVariable Integer id) {
        SysNoticeEntity entity = noticeService.get(id);
        return R.ok().put("data", entity);
    }

    @PostMapping("/available-list")
    public R availableList(@RequestBody Map<String, Object> params) {
        String locale = (String) params.get("locale");
        Integer limitNum = (Integer) params.get("limitNum");
        List<SysNoticeEntity> notices = noticeService.availableList(locale, limitNum);
        return R.ok().put("data", notices);
    }

}
