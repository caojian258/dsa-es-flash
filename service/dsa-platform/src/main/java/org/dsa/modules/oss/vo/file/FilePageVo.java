package org.dsa.modules.oss.vo.file;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.modules.oss.pojo.PageParam;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class FilePageVo extends PageParam{

    private String category;

    private String name;
}
