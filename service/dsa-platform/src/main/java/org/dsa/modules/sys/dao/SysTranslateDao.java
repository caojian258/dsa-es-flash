package org.dsa.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.dsa.modules.sys.entity.SysTranslateEntity;

import java.util.List;

/**
 * 数据字典
 *
 */
@Mapper
public interface SysTranslateDao extends BaseMapper<SysTranslateEntity> {

    Integer insertBatch(List<SysTranslateEntity> list);

    int truncateTable();

    List<SysTranslateEntity> listAll();

    List<String> listAllLocales();
}
