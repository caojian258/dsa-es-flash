package org.dsa.modules.remoteDiagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.common.validator.group.UpdateGroup;

import jakarta.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

/**
 * ${comments}
 * 
 */
@Data
@TableName("r_tag")
public class RemoteVehicleTagEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@TableId(type = IdType.AUTO)
	private Long id;

	@NotBlank(message = "名称不能为空", groups = {AddGroup.class, UpdateGroup.class})
	private String tagName;

	private String descriptionTi;

	private Date createdAt;

	private Date updatedAt;

}
