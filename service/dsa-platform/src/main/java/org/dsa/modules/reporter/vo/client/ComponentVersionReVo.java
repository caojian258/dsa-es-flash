package org.dsa.modules.reporter.vo.client;

import lombok.Data;

import java.io.Serializable;

@Data
public class ComponentVersionReVo implements Serializable {

    /***
     * 软件ID
     */
    private Integer softwareId;

    /***
     * 软件版本ID
     */
    private Integer softwareVersionId;

    /***
     * 版本
     */
    private String softwareVersion;
}
