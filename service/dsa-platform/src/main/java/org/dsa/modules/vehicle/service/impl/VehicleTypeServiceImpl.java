package org.dsa.modules.vehicle.service.impl;

import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.utils.*;
import org.dsa.modules.remoteDiagnostic.config.RemoteDiagnosticConfig;
import org.dsa.modules.remoteDiagnostic.constant.RemoteDiagnosticConstant;
import org.dsa.modules.remoteDiagnostic.dao.RemotePoolDependDao;
import org.dsa.modules.remoteDiagnostic.dao.RemoteVehicleTypeEcuGroupDao;
import org.dsa.modules.remoteDiagnostic.entity.RemotePoolDependEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuGroupEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleTypeEcuGroupEntity;
import org.dsa.modules.remoteDiagnostic.utils.AutoExcelWidthStyleStrategy;
import org.dsa.modules.remoteDiagnostic.vo.Vehicle.VehicleInfoExcelReqVo;
import org.dsa.modules.reporter.util.LocalUtil;
import org.dsa.modules.vehicle.dao.DVehicleInfoDao;
import org.dsa.modules.vehicle.dao.DVehicleTypeDao;
import org.dsa.modules.vehicle.entity.DVehicleInfoEntity;
import org.dsa.modules.vehicle.entity.DVehicleTypeEntity;
import org.dsa.modules.vehicle.service.VehicleTypeService;
import org.dsa.modules.vehicle.vo.VehicleInfoExportVo;
import org.dsa.modules.vehicle.vo.VehicleInfoPageReqVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SessionCallback;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jakarta.annotation.Resource;

import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service("VehicleTypeService")
public class VehicleTypeServiceImpl extends ServiceImpl<DVehicleTypeDao, DVehicleTypeEntity> implements VehicleTypeService {

    private final Logger logger = LoggerFactory.getLogger(VehicleTypeServiceImpl.class);

    @Resource
    private DVehicleInfoDao dVehicleInfoDao;
    @Autowired
    private DVehicleTypeDao dVehicleTypeDao;
    @Autowired
    RemoteVehicleTypeEcuGroupDao vehicleTypeEcuGroupDao;
    @Autowired
    private RemotePoolDependDao poolDependDao;
    @Autowired
    RedisUtils redisUtils;
    @Autowired
    RedisTemplate<String, Object> redisTemplate;
    @Autowired
    RemoteDiagnosticConfig remoteDiagnosticConfig;

    @Override
    public List<DVehicleTypeEntity> queryList(String name) {
//        logger.info("查询车型列表开始 {}", params);
        List<DVehicleTypeEntity> dVehicleTypeEntities = baseMapper.queryList(name);
        if (StringUtils.isEmpty(name)) {
            return dVehicleTypeEntities;
        }

        Map<Long, DVehicleTypeEntity> data = new HashMap<>();
        if (ObjectUtils.isNotEmpty(dVehicleTypeEntities)) {
            dVehicleTypeEntities.forEach(e -> {
                if (data.containsKey(e.getId())) {
                    return;
                }
                List<DVehicleTypeEntity> treeData = baseMapper.selectTreeListById(e.getId());
                data.putAll(treeData.stream().collect(Collectors.toMap(DVehicleTypeEntity::getId, Function.identity(), (key1, key2) -> key1)));
            });
        }

        return data.values().stream().sorted(Comparator.comparing(DVehicleTypeEntity::getParentId)
                .thenComparing(DVehicleTypeEntity::getLevel).thenComparing(DVehicleTypeEntity::getOrderNum))
                .collect(Collectors.toList());
    }

    @Override
    public List<DVehicleTypeEntity> queryChildrenList(Map<String, Object> params) {
        return baseMapper.queryChildrenListByParentId((Long) params.get("parentId"));
    }

    @Override
    public Page<DVehicleInfoEntity> getListByVehicleTypeIdAndVin(VehicleInfoPageReqVo vo) {
        Page<DVehicleInfoDao> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);

        if (StringUtils.isNotEmpty(vo.getVin())) {
            vo.setVin(vo.getVin().trim());
        }
        if (StringUtils.isNotEmpty(vo.getRegular())) {
            vo.setRegular(vo.getRegular().trim());
        }


        return dVehicleInfoDao.searchAllByVehicleTypeIdAndVin(page, vo);

//        logger.info("queryDVehicleInfoEntityPage begin");
//        Map<String, Object> map = new HashMap<>();
//        map.put(Constant.PAGE, String.valueOf(params.get(Constant.PAGE)));
//        map.put(Constant.LIMIT, String.valueOf(params.get(Constant.LIMIT)));
//        List<Long> vehicleTypeIds = (List<Long>) params.get("vehicleTypeIds");
//        String vin = (String) params.get("vin");
//        if (StringUtils.isNotEmpty(vin)) {
//            vin = vin.trim();
//        }
//        String regular = (String) params.get("regular");
//        if (StringUtils.isNotEmpty(regular)) {
//            regular = regular.trim();
//        }
//        Long tagId = params.get("tag") == null ? null : Long.parseLong(params.get("tag").toString());
//
//        logger.info("map:{}", map);
//        IPage<DVehicleInfoEntity> page = new Query<DVehicleInfoEntity>().getPage(map);
//
//        IPage<DVehicleInfoEntity> pageObj = dVehicleInfoDao.searchAllByVehicleTypeIdAndVin(page, vehicleTypeIds, vin, tagId, regular);
//        logger.info("queryDVehicleInfoEntityPage end");


//        return new PageUtils(pageObj);



    }

    @Override
    public R checkVehicleType(DVehicleTypeEntity vehicleType) {
//        Long id = dVehicleInfoDao.checkVehicleType(vehicleType);
//        if (ObjectUtils.isNotNull(id)) {
//            if (!ObjectUtil.equal(id, vehicleType.getId())) {
//                return R.error(Constant.Msg.CHECK_ORDER_NUM);
//            }
//        }
        DVehicleInfoEntity dVehicleInfo = dVehicleInfoDao.checkVehicleTypeByValue(vehicleType);
        if (ObjectUtils.isNotNull(dVehicleInfo) && !dVehicleInfo.getId().equals(vehicleType.getId())) {
            return R.error(Constant.Msg.CHECK_HAS_NAME);
        }
        return R.ok().put("success", "");
    }

    @Override
    public Long getVehicleTypeId(String vehicleType) {
        return baseMapper.getVehicleTypeId(vehicleType);
    }


    @Override
    public List<DVehicleTypeEntity> getTreeList() {
        return baseMapper.selectTreeList();
    }

    @Override
    public DVehicleTypeEntity selectById(Long vehicleTypeId) {
        return baseMapper.selectOne(Wrappers.<DVehicleTypeEntity>lambdaQuery().eq(DVehicleTypeEntity::getId, vehicleTypeId));
    }

    @Override
    public String getWholeVehicleType(Long typeId) {
        String wholeVehicleType = "";
        wholeVehicleType = getParentType(wholeVehicleType,typeId);
        return wholeVehicleType;
    }


    public String getParentType(String vehicleType, Long typeId) {
        DVehicleTypeEntity info = dVehicleTypeDao.getInfo(typeId);
        if (info == null) {
            return vehicleType;  // 处理异常情况，返回当前 vehicleType
        }
        if ("".equals(vehicleType)){
            vehicleType = info.getValue();
        }else {
            vehicleType = info.getValue()+" / "+vehicleType;  // 拼接当前车型名称
        }
        if (info.getParentId() == 0 || info.getParentId() == null) {
            return vehicleType;  // 已到达最顶层父级，返回结果
        }
        return getParentType(vehicleType, info.getParentId());  // 递归调用，更新参数进行下一轮递归
    }

    @Override
    @Transactional
    public void batchDelete(List<DVehicleTypeEntity> childrenList) {

        List<Long> ids = childrenList.stream().map(DVehicleTypeEntity::getId).collect(Collectors.toList());

        List<String> poolKeys = new ArrayList<>();
        if (ids.size() != 0) {
            poolKeys = poolDependDao.selectList(Wrappers.<RemotePoolDependEntity>lambdaQuery()
                    .in(RemotePoolDependEntity::getVehicleTypeId, ids).select(RemotePoolDependEntity::getPoolId).groupBy(RemotePoolDependEntity::getPoolId))
                    .stream().map(e -> {
                        return RemoteDiagnosticConstant.VEHICLE_POOL_ITEM_KEY + e.getPoolId();
                    }).collect(Collectors.toList());
        }

        for (DVehicleTypeEntity dVehicleTypeEntity : childrenList) {
            baseMapper.deleteById(dVehicleTypeEntity);
            vehicleTypeEcuGroupDao.delete(Wrappers.<RemoteVehicleTypeEcuGroupEntity>lambdaQuery().eq(RemoteVehicleTypeEcuGroupEntity::getTypeId, dVehicleTypeEntity.getId()));
            poolDependDao.delete(Wrappers.<RemotePoolDependEntity>lambdaQuery().eq(RemotePoolDependEntity::getVehicleTypeId, dVehicleTypeEntity.getId()));
        }

        if (poolKeys.size() != 0) {
            List<String> finalPoolKeys = poolKeys;
            redisTemplate.execute(new SessionCallback<Object>() {
                @Override
                public <K, V> Object execute(RedisOperations<K, V> operations) throws DataAccessException {
                    operations.multi(); // 开启事务
                    finalPoolKeys.forEach(e -> redisTemplate.opsForHash().delete(RemoteDiagnosticConstant.VEHICLE_POOL_KEY, e));
                    return operations.exec();  //提交
                }
            });
            redisTemplate.opsForValue().set((RemoteDiagnosticConstant.VEHICLE_POOL_LOCK), System.currentTimeMillis()+"");
        }
    }

    @Override
    public List<DVehicleTypeEntity> selectListById(Long vehicleTypeId) {
        return baseMapper.selectListById(vehicleTypeId);
    }

    @Override
    @Transactional
    public void updateInfo(DVehicleTypeEntity vehicleType) {
        this.updateById(vehicleType);

        if (vehicleType.getDelGroupList() != null && vehicleType.getDelGroupList().size() > 0) {
            List<Long> groups = vehicleType.getDelGroupList().stream().map(RemoteVehicleEcuGroupEntity::getId).collect(Collectors.toList());

            vehicleTypeEcuGroupDao.delete(Wrappers.<RemoteVehicleTypeEcuGroupEntity>lambdaQuery().eq(RemoteVehicleTypeEcuGroupEntity::getTypeId, vehicleType.getId()).in(RemoteVehicleTypeEcuGroupEntity::getEcuGroupId, groups));

            if (vehicleType.getGroupList() != null && vehicleType.getGroupList().size() > 0) {
                vehicleTypeEcuGroupDao.inserts(vehicleType.getId(), vehicleType.getGroupList());
            }
        }

    }

    @Override
    @Transactional
    public void saveInfo(DVehicleTypeEntity vehicleType) {

        this.updateOrder(vehicleType.getParentId());
        this.save(vehicleType);

        List<RemoteVehicleTypeEcuGroupEntity> remoteVehicleTypeEcuGroupEntities = vehicleTypeEcuGroupDao.selectByTypeId(vehicleType.getParentId());

        if (!ObjectUtils.isEmpty(vehicleType.getDelGroupList())) {
            remoteVehicleTypeEcuGroupEntities.removeIf(type -> vehicleType.getDelGroupList().stream().anyMatch(del -> Objects.equals(del.getId(), type.getEcuGroupId())));
        }
        if (vehicleType.getGroupList() != null && vehicleType.getGroupList().size() != 0) {
            remoteVehicleTypeEcuGroupEntities.addAll(vehicleType.getGroupList().stream().map(add -> {
                RemoteVehicleTypeEcuGroupEntity remoteVehicleTypeEcuGroupEntity = new RemoteVehicleTypeEcuGroupEntity();
                remoteVehicleTypeEcuGroupEntity.setEcuGroupId(add.getId());
                remoteVehicleTypeEcuGroupEntity.setEcuId(add.getEcuId());
                return remoteVehicleTypeEcuGroupEntity;
            }).collect(Collectors.toList()));
        }

        if (remoteVehicleTypeEcuGroupEntities != null && remoteVehicleTypeEcuGroupEntities.size() != 0) {
            vehicleTypeEcuGroupDao.insertsGroup(vehicleType.getId(), remoteVehicleTypeEcuGroupEntities);
        }
//        if (!StringUtils.isEmpty(vehicleType.getSelectName())) {
//
//
//
//        } else {
//            if (remoteVehicleTypeEcuGroupEntities != null && remoteVehicleTypeEcuGroupEntities.size() != 0) {
//                vehicleTypeEcuGroupDao.insertsGroup(vehicleType.getId(), remoteVehicleTypeEcuGroupEntities);
//            }
//        }

    }

    @Override
    public void updateOrder(Long parentId) {
        baseMapper.updateOrder(parentId);
    }

    @Override
    public void updateTypeOrder(List<DVehicleTypeEntity> typeEntities) {
        for (int i = 0; i < typeEntities.size(); i++) {
            typeEntities.get(i).setOrderNum(i);
            baseMapper.updateById(typeEntities.get(i));
        }
    }
    @Override
    public void exportVehicle(HttpServletResponse response, VehicleInfoExcelReqVo vo) {
        try(OutputStream outputStream = response.getOutputStream();) {
            String fileName = remoteDiagnosticConfig.getVehicleInfo() + "_" + System.currentTimeMillis();
            String sheetName = "vehicle info";
            response.setHeader("Content-disposition", "attachment; filename=" + URLEncoder.encode(fileName, "UTF-8") + ".xlsx");
            //设置响应头信息：将响应内容类型设置为 Excel 文件，并指定文件名和编码方式
            response.setContentType("application/vnd.ms-excel");
            response.setCharacterEncoding("utf-8");
            //获取响应的输出流，用于将数据写入响应。
            //创建一个 ExcelWriter 对象，用于写入 Excel 文件。
            ExcelWriter excelWriter = EasyExcel.write(outputStream).build();
            //从数据库中根据自己的方法查询出需要导出的数据
            List<VehicleInfoExportVo> data = new ArrayList<>();
            data = dVehicleInfoDao.getAllForExport(vo.getVehicleTypeIds(), vo.getVin(), vo.getTag(), vo.getRegular());
            for (VehicleInfoExportVo datum : data) {
                datum.setStage(getStage(datum.getVersionId(),datum.getCurrentVersionId()));
                if (datum.getVehicleTypeId() != null){
                    String s = dVehicleInfoDao.getWholeType(datum.getVehicleTypeId());
                    datum.setVehicleType(s);
                }
            }
            //创建一个工作表，指定工作表名称和表头
            WriteSheet sheet = EasyExcel.writerSheet(sheetName).registerWriteHandler(new AutoExcelWidthStyleStrategy()).head(head()).build();
            //将数据写入该工作表
            excelWriter.write(data, sheet);
            // 完成写入操作，关闭 ExcelWriter
            excelWriter.finish();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("下载文件出错：" + e);
        }
    }

    private List<List<String>> head() {
        List<List<String>> headArr = new ArrayList<>();
        headArr.add(Collections.singletonList(LocalUtil.get("vehicle.VIN")));
        headArr.add(Collections.singletonList(LocalUtil.get("vehicle.VehicleType")));
        headArr.add(Collections.singletonList(LocalUtil.get("vehicle.ProductionDate")));
        headArr.add(Collections.singletonList(LocalUtil.get("vehicle.Origin")));
        headArr.add(Collections.singletonList(LocalUtil.get("vehicle.synTime")));
        headArr.add(Collections.singletonList(LocalUtil.get("vehicle.createTime")));
        headArr.add(Collections.singletonList(LocalUtil.get("vehicle.source")));
        headArr.add(Collections.singletonList(LocalUtil.get("vehicle.stage")));
        return headArr;
    }
    private String getStage(Long versionId,Long currentVersionId){
        String result = "";
        if (null != versionId && null != currentVersionId && versionId != currentVersionId){
            result = "AsMaintained";
        }else {
            result = "AsBuild";
        }
        return result;
    }
}
