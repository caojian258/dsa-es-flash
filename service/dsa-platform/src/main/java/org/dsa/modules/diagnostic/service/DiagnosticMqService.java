package org.dsa.modules.diagnostic.service;

import org.dsa.modules.diagnostic.entity.mqtt.callback.MqEntity;

public abstract interface DiagnosticMqService {
    public void onMessage(MqEntity mqEntity);
}
