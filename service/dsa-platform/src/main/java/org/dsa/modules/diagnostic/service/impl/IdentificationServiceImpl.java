package org.dsa.modules.diagnostic.service.impl;

import ch.qos.logback.classic.Logger;
import com.alibaba.fastjson.JSONObject;
import org.dsa.modules.diagnostic.dao.DActionEcuDao;
import org.dsa.modules.diagnostic.dao.DActionVersionDao;
import org.dsa.modules.diagnostic.dao.DSessionActionDao;
import org.dsa.modules.diagnostic.entity.DActionEcuEntity;
import org.dsa.modules.diagnostic.entity.DActionVersionEntity;
import org.dsa.modules.diagnostic.entity.mqtt.callback.MqBodyDataIDENTVO;
import org.dsa.modules.diagnostic.entity.mqtt.callback.MqEntity;
import org.dsa.modules.diagnostic.service.DiagnosticMqService;
import org.dsa.modules.diagnostic.util.MqAsynchronousData;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service("identificationService")
public class IdentificationServiceImpl implements DiagnosticMqService {
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass());

    @Resource
    private DActionVersionDao dActionVersionDao;
    @Resource
    private DActionEcuDao dActionEcuDao;
    @Resource
    private DSessionActionDao dSessionActionDao;

    @Resource
    private MqAsynchronousData<MqBodyDataIDENTVO> mqAsynchronousData;

    @Override
    public void onMessage(MqEntity mqEntity) {
       logger.info("Identification MQ Call back---begin");
       logger.info("Identification MQ Call bakc----actionID:{}",mqEntity.getHeader().getRid());

       try{
            mqAsynchronousData.handle("Identification",mqEntity,new MqBodyDataIDENTVO());
            saveActionVersion(mqEntity);
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        logger.info("Identification MQ Call back-----end");
    }

    private void saveActionVersion(MqEntity mqEntity){
       logger.info("Save Identification data in DB");
       if(mqEntity.getBody().getData()== null || "{}".equals(mqEntity.getBody().getData())){
           return ;
       }

       List<MqBodyDataIDENTVO> identVos = new ArrayList<MqBodyDataIDENTVO>();
       identVos= JSONObject.parseArray( JSONObject.toJSONString(mqEntity.getBody().getData()), MqBodyDataIDENTVO.class);
       //插入版本信息表
       if(identVos!=null && identVos.size() >0 ){
           String sessionId = dSessionActionDao.selectSessionIdByActionId(mqEntity.getHeader().getRid());
           for(MqBodyDataIDENTVO identvo:identVos){
               DActionVersionEntity dActionVersionEntity=new DActionVersionEntity();
               dActionVersionEntity.setActionId(mqEntity.getHeader().getRid());
               dActionVersionEntity.setEcuName(identvo.getEcuName());
               dActionVersionEntity.setTi(identvo.getTi());
               dActionVersionEntity.setName(identvo.getName());
               dActionVersionEntity.setValue(identvo.getValue());
               dActionVersionEntity.setTiValue(identvo.getTiValue());
               dActionVersionEntity.setSessionId(sessionId);
               dActionVersionDao.insert(dActionVersionEntity);
           }
       }
       if(mqEntity.getBody().getEcuName() != null && mqEntity.getBody().getEcuName().length >0 ) {
           //插入ecu表
           for (String ecuName : mqEntity.getBody().getEcuName()) {
               DActionEcuEntity dActionEcuEntity = new DActionEcuEntity();
               dActionEcuEntity.setActionId(mqEntity.getHeader().getRid());
               dActionEcuEntity.setEcuName(ecuName);
               dActionEcuEntity.setDescription(mqEntity.getBody().getMessage());
               dActionEcuEntity.setStatus(mqEntity.getBody().getStatus());
               dActionEcuEntity.setEndTime(new Date());
               //保存诊断执行的ecu
               dActionEcuDao.updateByActionAndEcuName(dActionEcuEntity);
           }
       }
    }
}