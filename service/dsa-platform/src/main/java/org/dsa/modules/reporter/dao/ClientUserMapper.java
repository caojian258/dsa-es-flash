package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.dto.ClientDeviceDto;
import org.dsa.modules.reporter.entity.ClientUser;

import java.util.Map;


@Mapper
public interface ClientUserMapper extends BaseMapper<ClientUser> {

    public ClientUser getOneByUsername(String username);

    public ClientUser getOneByPcid(@Param("pcid") String pcid);

    public Map<String, Object> workshopDevice(@Param("pcid") String pcid);
}
