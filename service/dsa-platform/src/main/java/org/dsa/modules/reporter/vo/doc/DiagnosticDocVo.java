package org.dsa.modules.reporter.vo.doc;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import org.dsa.modules.reporter.document.ElasticEntity;

import java.util.Date;

@Data
public class DiagnosticDocVo extends ElasticEntity {

    private String sessionInfoId;

    private String sessionId;

    private String vin;

    private Long userId;

    private String functionCode;

    private String action;

    private String ctsName;

    private String ecuName;

    private String nrcHex;

    private String nrcDescription;

    private String nrcDescriptionTi;

    private String errorCode;

    private String errorMessage;

    private String specialFunctionName;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date lastUpdateTime;
}
