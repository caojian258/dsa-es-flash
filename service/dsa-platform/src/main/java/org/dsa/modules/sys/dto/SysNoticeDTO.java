package org.dsa.modules.sys.dto;

import lombok.Getter;
import lombok.Setter;
import org.dsa.modules.sys.entity.SysNoticeEntity;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author weishunxin
 * @since 2023-07-10
 */
@Setter
@Getter
public class SysNoticeDTO extends SysNoticeEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private String content;

    private String noticeImage;

}
