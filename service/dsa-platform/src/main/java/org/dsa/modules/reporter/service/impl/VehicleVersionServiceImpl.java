package org.dsa.modules.reporter.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.extern.slf4j.Slf4j;
import org.dsa.modules.reporter.dao.VehicleVersionHistoryMapper;
import org.dsa.modules.reporter.dao.VehicleVersionOverviewMapper;
import org.dsa.modules.reporter.dto.VehicleVersionDto;
import org.dsa.modules.reporter.entity.VehicleVersionHistory;
import org.dsa.modules.reporter.entity.VehicleVersionOverview;
import org.dsa.modules.reporter.enums.VersionSourceEnum;
import org.dsa.modules.reporter.service.VehicleVersionService;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;
import java.util.HashMap;
import java.util.Map;


@Slf4j
@Service
public class VehicleVersionServiceImpl implements VehicleVersionService {

    @Resource
    VehicleVersionOverviewMapper overviewMapper;

    @Resource
    VehicleVersionHistoryMapper historyMapper;

    @Override
    public void addHistory(VehicleVersionDto dto) {
        VehicleVersionHistory entity = new VehicleVersionHistory();
        entity.setVin(dto.getVin());
        entity.setTag(dto.getTag());
        entity.setCollectTime(dto.getCollectTime());
        entity.setVersion(dto.getVersion());
        if (null != dto.getSessionId()) {
            entity.setSessionId(dto.getSessionId());
        }
        entity.setSource(VersionSourceEnum.AsMaintained.getValue());
        historyMapper.insert(entity);
    }

    @Override
    public void updateOverview(VehicleVersionDto dto) {
        VehicleVersionOverview overview = null;
        QueryWrapper<VehicleVersionOverview> qw = new QueryWrapper<>();
        qw.eq("vin", dto.getVin());
        qw.eq("version", dto.getVersion());
        overview = overviewMapper.selectOne(qw);
        if (null == overview) {
            overview = new VehicleVersionOverview();
            overview.setVin(dto.getVin());
            overview.setTag(dto.getTag());
            overview.setCollectTime(dto.getCollectTime());
            overview.setVersion(dto.getVersion());
            if (null != dto.getSessionId()) {
                overview.setSessionId(dto.getSessionId());
            }

            overview.setSource(VersionSourceEnum.AsMaintained.getValue());
            overviewMapper.insert(overview);
        } else {
            overview.setVersion(dto.getVersion());
            overview.setCollectTime(dto.getCollectTime());
            overview.setTag(dto.getTag());
            if (null != dto.getSessionId()) {
                overview.setSessionId(dto.getSessionId());
            }
            overview.setSource(VersionSourceEnum.AsMaintained.getValue());
            overviewMapper.updateById(overview);
        }
    }

    @Override
    public Map<String, Object> getVehicleVersion(String vin) {

        // 查询当前版本
        VehicleVersionOverview currentVersion = overviewMapper.selectOne(Wrappers.<VehicleVersionOverview>lambdaQuery().eq(VehicleVersionOverview::getVin, vin).orderByDesc(VehicleVersionOverview::getId).last(" limit 1"));
        // 查询第一次的版本
        VehicleVersionHistory firstVersion = historyMapper.selectOne(Wrappers.<VehicleVersionHistory>lambdaQuery().eq(VehicleVersionHistory::getVin, vin).orderByDesc(VehicleVersionHistory::getId).last(" limit 1"));
        HashMap<String, Object> result = new HashMap<>();

        result.put("currentVersion", currentVersion == null ? null : currentVersion.getVersion());
        result.put("firstVersion", firstVersion == null ? null : firstVersion.getVersion());

        return result;
    }
}
