package org.dsa.modules.reporter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/***
 * 软件组件操作历史表 汇集了详情表
 */
@Data
@TableName("client_application_history")
public class AppHistory extends BaseEntity{

    @TableId(type = IdType.AUTO)
    private Long id;

    private String deviceId;

    private String sessionId;

    private Integer updateId;

    private Long userId;

    private String username;

    private String userDisplayName;

    private Long workshopId;

    private String workshopName;

    private Integer appId;

    private String appType;

    private String appName;

    private String appVersion;

    private String appPrevVersion;

    private Integer versionNumber;

    private Integer prevVersionNumber;

    /***
     * 0 none 无状态
     * 1,upgrade(升级)
     * 2,repair(修复)
     * 3,rollback(回退)
     * 4,uninstall(卸载)
     */
    private Integer statusType;

    /***
     * 0,success成功
     * 1, fail失败
     * 2 cancel 取消
     */
    private Integer status;

    private Date startTime;

    private Date endTime;

    private Date collectDate;

    private Integer expireWarningDays;

    private Integer updateType;

    private Date expireDate;

    private String releaseNotes;

    private String tag;

    @TableField(exist = false)
    private String statusMsg;
}
