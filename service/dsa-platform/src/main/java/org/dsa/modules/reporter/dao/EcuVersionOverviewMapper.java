package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.entity.EcuVersionOverview;

import java.util.List;

@Mapper
public interface EcuVersionOverviewMapper extends BaseMapper<EcuVersionOverview> {

    public void syncSessionEcuVersion(@Param("sessionId") String sessionId);

    public EcuVersionOverview checkEcuVersionOverviewBySession(@Param("sessionId") String sessionId);

    public EcuVersionOverview checkEcuVersionOverviewByVin(@Param("vin") String vin);

    public EcuVersionOverview checkEcuVersionOverview(@Param("vin") String vin,
                                                      @Param("ecuName") String ecuName,
                                                      @Param("versionName") String versionName);

    void insertsByVin(@Param("vin") String vin, @Param("overviews") List<EcuVersionOverview> ecuVersionOverviews);

}
