package org.dsa.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.dsa.modules.sys.entity.SysNoticeEntity;

/**
 * @author weishunxin
 * @since 2023-06-20
 */
@Mapper
public interface NoticeDao extends BaseMapper<SysNoticeEntity> {

}
