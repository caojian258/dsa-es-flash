package org.dsa.modules.onlineUpdate.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.onlineUpdate.entity.RuleEntity;
import org.dsa.modules.onlineUpdate.vo.RuleVo;

import java.util.List;

/**
 * 规则
 * 
 */
@Mapper
public interface RuleDao extends BaseMapper<RuleEntity> {

    List<RuleEntity> selectEffectiveRule(@Param("code")Long code,@Param("dateTimeNow") String dateTimeNow);

    IPage<RuleVo> queryPage(@Param("page") Page<RuleEntity> page, @Param("name") String name,@Param("startTime") String startTime,@Param("endTime")String endTime,@Param("softwareId")Integer softwareId,@Param("type")Integer type);

    RuleEntity selectSoftwareNewestRule(@Param("softwareId") Long softwareId);

    Integer hasPublishCompVersion(@Param("cVersion") Long cVersion);

}
