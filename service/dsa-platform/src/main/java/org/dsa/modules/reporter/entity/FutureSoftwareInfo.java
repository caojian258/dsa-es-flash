package org.dsa.modules.reporter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
@TableName("client_future_software_info")
public class FutureSoftwareInfo extends BaseEntity{

    @TableId(type = IdType.AUTO)
    private Long id;

    private String deviceId;

    private Integer softwareId;

    private String softwareVersionName;
    private Integer softwareVersionNumber;

    /***
     强制更新： 1=force
     非强制更新： 0=optional
     */
    private Integer updateType;
    private Date expireDate;
    private Integer expireWarningDays;
    private String releaseNotes;
    private String featureDescription;
    private String fileSize;
    private Integer downloadProgress;
    private String updateId;
    private String tag;
    private String sessionId;
}
