package org.dsa.modules.onlineUpdate.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.dsa.modules.onlineUpdate.utils.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * Oem关联软件
 * 
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("o_oem_software_version")
public class OemSoftwareVersionEntity extends BaseEntity {

	/**
	 * 软件id
	 */
	private Integer softwareId;
	/**
	 * 软件版本id
	 */
	private Integer softwareVersionId;
	/**
	 * oem
	 */
	private Integer oemId;

}
