package org.dsa.modules.diagnostic.entity;


import lombok.Data;

import java.io.Serializable;

/**
 * 远程诊断 - 读取版本信息
 */
@Data
public class ReadIdentEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    String vin;
    String[] ecuNames;
}
