package org.dsa.modules.remoteDiagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;
import org.dsa.modules.remoteDiagnostic.pojo.BaseEntity;

import java.io.Serializable;

@Data
@ToString
@TableName("r_diag_task_record")
public class DiagnosticTaskRecordEntity extends BaseEntity implements Serializable {

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long campaignId;

    private Integer executions;

    private String sessionId;

    private Integer matchStatus;

    private String vin;

    @TableField(exist = false)
    private DiagnosticTaskDetailEntity detail;
}
