package org.dsa.modules.sys.service;


import java.util.List;
import java.util.Map;

import org.dsa.common.utils.PageUtils;
import org.dsa.modules.sys.entity.SysGroupEntity;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 组管理
 *
 */
public interface SysGroupService extends IService<SysGroupEntity> {

	List<SysGroupEntity> querySelectList();
	
	/**
	 * 查询事件转派组信息
	 * @return
	 */
	List<SysGroupEntity> queryTransferGroupList();

	/**
	 * 分页查询Group
	 * @param params
	 * @return
	 */
	PageUtils queryPage(Map<String, Object> params);
	/**
	 * 查询组所属部门
	 * @param  groupName 组ID
	 */
	List<SysGroupEntity> queryByName(String groupName);

	List<SysGroupEntity> queryByWorkshopId(Long workshopId);
}
