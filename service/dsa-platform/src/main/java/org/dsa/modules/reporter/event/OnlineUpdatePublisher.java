package org.dsa.modules.reporter.event;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class OnlineUpdatePublisher {

    @Autowired
    private ApplicationEventPublisher publisher;

    public void handlerFile(String path, String tag, String name){
        publisher.publishEvent(new OnlineUpdateEvent(this, tag, path, name));
    }

}
