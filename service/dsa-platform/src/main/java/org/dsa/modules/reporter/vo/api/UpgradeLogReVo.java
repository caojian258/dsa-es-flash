package org.dsa.modules.reporter.vo.api;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class UpgradeLogReVo implements Serializable {

//    @NotBlank(message = "签名不能为空")
    private String sign; //签名

    private SessionReVo session; //用户会话

    private WorkshopReVo workshop;

    private ClientInfoReVo clientInfo;

    private List<DownloadVersionInfoReVo> downloadVersionInfos;

    private List<InstallVersionInfoReVo> installVersionInfos;

    private List<VersionInfoReVo> currentVersionInfos;

}
