package org.dsa.modules.remoteDiagnostic.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.ToString;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.common.validator.group.UpdateGroup;

import java.io.Serializable;

@Data
@ToString
public class ECUDependenceDto implements Serializable {

    @NotNull(message = "groupId is null", groups = {AddGroup.class, UpdateGroup.class})
    private Long groupId;

    @NotNull(message = "ecuId is null", groups = {AddGroup.class, UpdateGroup.class})
    private Long ecuId;

    @NotNull(message = "ecuVersionId is null", groups = {AddGroup.class, UpdateGroup.class})
    private Long ecuVersionId;

    private String ecuVersionName;

    private String groupName;

}
