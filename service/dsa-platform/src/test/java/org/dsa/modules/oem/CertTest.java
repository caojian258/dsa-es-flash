package org.dsa.modules.oem;

import org.apache.commons.io.FileUtils;
import org.dsa.modules.client.config.ClientConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileInputStream;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CertTest {

    @Autowired
    ClientConfiguration clientConfiguration;

    @Test
    public void Test() {

        try {

            File file = new File(clientConfiguration.getClient());
            String s1 = FileUtils.readFileToString(file, "utf-8");
            System.out.println(s1);

            String encodedString = Base64.getEncoder().encodeToString(s1.getBytes());
            System.out.println(" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ");
            System.out.println(encodedString);
            System.out.println(" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ");
            String dString = new String(Base64.getDecoder().decode(encodedString.getBytes()));
            System.out.println(dString);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Test
    public void Test2() {

        try {

            File file = new File(clientConfiguration.getClient());
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            FileInputStream in = new  FileInputStream(file);
            X509Certificate cert = (X509Certificate) cf.generateCertificate(in);
            cert.checkValidity();    //校验格式，错误抛出异常，可捕捉
            Date effDate = new Date();
            Date expDate = cert.getNotAfter();
            long diff = expDate.getTime() - effDate.getTime();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            System.out.println(diff / (1000 * 60 * 60 * 24));
            System.out.println(simpleDateFormat.format(effDate));
            System.out.println(simpleDateFormat.format(expDate));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
