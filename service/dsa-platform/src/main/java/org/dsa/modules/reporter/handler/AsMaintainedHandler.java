package org.dsa.modules.reporter.handler;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.dom4j.Element;
import org.dsa.modules.reporter.dao.FileLogMapper;
import org.dsa.modules.reporter.dao.VehicleFlashDetailMapper;
import org.dsa.modules.reporter.dao.VehicleFlashRecordMapper;
import org.dsa.modules.reporter.entity.FileLog;
import org.dsa.modules.reporter.entity.VehicleFlashDetail;
import org.dsa.modules.reporter.entity.VehicleFlashRecord;
import org.dsa.modules.reporter.enums.FileCateEnum;
import org.dsa.modules.reporter.service.FileLogService;
import org.dsa.modules.reporter.util.XmlReader;
import org.dsa.modules.vehicle.dao.DVehicleInfoDao;
import org.dsa.modules.vehicle.dao.DVehicleTypeDao;
import org.dsa.modules.vehicle.entity.DVehicleInfoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
@Service
public class AsMaintainedHandler {

    @Autowired
    VehicleFlashDetailMapper detailMapper;
    @Autowired
    VehicleFlashRecordMapper recordMapper;
    @Autowired
    FileLogMapper fileLogMapper;
    @Autowired
    FileLogService logService;
    @Autowired
    DVehicleInfoDao dVehicleInfoDao;
    @Autowired
    DVehicleTypeDao dVehicleTypeDao;

    public void readFile(String path) {
        File file = new File(path);
        File[] files = file.listFiles();
        if (files == null) {
            log.info("not find file by asmaintained");
            logService.failedLog(FileCateEnum.AsMaintained.getValue(), path, null, "not find file by asmaintained");
            return;
        }
        List<Map<String, Object>> testerInfoList = new ArrayList<>();
        List<Map<String, Object>> generalInfoList = new ArrayList<>();
        List<Map<String, Object>> mapList = new ArrayList<>();
        StringBuilder sb = new StringBuilder();

        // 开始读取文件
        for (File fileAs : files) {
            if (!fileAs.getName().endsWith("xml")) {
                continue;
            }
            sb = new StringBuilder();
            try {
                QueryWrapper<FileLog> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("file_name", fileAs.getName());
                FileLog fileLog = fileLogMapper.selectOne(queryWrapper);
                if (fileLog != null) {
                    continue;
                }
                XmlReader reader = new XmlReader(fileAs.getPath());
                testerInfoList = reader.parse("TesterInfo");
                generalInfoList = reader.parse("GeneralInfo");

                if (generalInfoList == null || generalInfoList.size() < 1) {
                    logService.failedLog(FileCateEnum.AsMaintained.getValue(), fileAs.getName(),null, "GeneralInfo is null");
                    continue;
                }
                if (testerInfoList == null || testerInfoList.size() < 1) {
                    logService.failedLog(FileCateEnum.AsMaintained.getValue(), fileAs.getName(),null, "TesterInfo is null");
                    continue;
                }

                // 解析 Feature标签
                mapList =  reader.parseCallback2("Feature", new XmlReader.XmlHandler() {
                    @Override
                    public List<Map<String, Object>> handle( Iterator<Element> ele) {
                        List<Map<String, Object>> mapValues = new ArrayList<>();
                        if (ele.hasNext()){
                            Iterator<Element> igi =  ele.next().elementIterator();
                            while (igi.hasNext()){
                                Element fooi = igi.next();
                                int elementSize = fooi.elements().size();
                                if(elementSize>0){
                                    for(int i = 0; i< elementSize;i++){
                                        Map<String, Object> nod = new HashMap<>();
                                        Element element =  fooi.elements().get(i);
                                        nod.put("name", element.attribute("name").getValue());
                                        nod.put("FID", element.attribute("FID").getValue());
                                        nod.put("resourceID", element.attribute("resourceID").getValue());
                                        nod.put("timestamp", element.attribute("timestamp").getValue());
                                        nod.put(element.getName(), element.getStringValue());
                                        if(nod.size()>0){
                                            mapValues.add(nod);
                                        }
                                    }
                                }
                            }
                        }
                        return mapValues;
                    }
                });
                if (mapList == null || mapList.size() == 0) {
                    logService.failedLog(FileCateEnum.AsMaintained.getValue(), fileAs.getPath(),null, "Feature is null");
                    continue;
                }
                // 数据开始入库
                saveData(testerInfoList, generalInfoList, mapList);
                sb.append("AsMaintained size:").append(mapList.size()).append(", spend time: ").append(reader.getSpendTime()).append(",File name :").append(fileAs.getName()).append(";");
                // 处理标识
//                saveFlagFile(fileAs);
                logService.succeedLog(FileCateEnum.AsMaintained.getValue(), fileAs.getPath(), null,null, sb.toString());

            } catch (Exception e) {
                logService.failedLog(FileCateEnum.AsMaintained.getValue(), fileAs.getPath(),null, e.getMessage());
            }

        }
    }
    @Transactional
    private void saveData(List<Map<String, Object>> testerInfoList, List<Map<String, Object>> generalInfoList, List<Map<String, Object>> mapList) throws Exception {
        Map<String, Object> testerInfoMap;
        Map<String, Object> generalInfoMap;
        VehicleFlashDetail vehicleFlashDetail = new VehicleFlashDetail();

        VehicleFlashRecord vehicleFlashRecord = null;

        // GeneralInfo
        generalInfoMap = generalInfoList.get(0);
        String vin = generalInfoMap.get("VehicleIdentNumber") == null ? null : generalInfoMap.get("VehicleIdentNumber").toString();

        vehicleFlashRecord = new VehicleFlashRecord();
        String model = generalInfoMap.get("Model").toString();

        DVehicleInfoEntity dVehicleInfoEntity = dVehicleInfoDao.selectOneByVin(vin);
        if (dVehicleInfoEntity == null) {
            Long vehicleTypeId = dVehicleTypeDao.getVehicleTypeId(model);

            dVehicleInfoEntity = new DVehicleInfoEntity();
            dVehicleInfoEntity.setVin(vin);
            dVehicleInfoEntity.setVehicleTypeId(vehicleTypeId == null ? 0 : vehicleTypeId);
            dVehicleInfoDao.insert(dVehicleInfoEntity);
        }

        vehicleFlashDetail.setVin(vin);
        vehicleFlashDetail.setVersion(model);
        vehicleFlashRecord.setVin(vin);
        vehicleFlashRecord.setModel(model);
        // TesterInfo
        testerInfoMap = testerInfoList.get(0);
        vehicleFlashRecord.setPcid(testerInfoMap.get("TesterId") == null ? null : testerInfoMap.get("TesterId").toString());

        Date date = getDate(testerInfoMap);
        vehicleFlashRecord.setCollectTime(date);
        vehicleFlashDetail.setCollectTime(date);
//        vehicleFlashDetail.setFlashTimestamp(date);

        vehicleFlashRecord.setResult(3);

        Map<String, Object> json = new HashMap<>();
        json.put("feature", mapList);
        vehicleFlashRecord.setFeature(JSONObject.toJSONString(json));

        recordMapper.insert(vehicleFlashRecord);

        vehicleFlashDetail.setFlashId(vehicleFlashRecord.getId().intValue());
        // Feature
        if (mapList.size() > 0) {
            for (Map<String, Object> map : mapList) {
                vehicleFlashDetail.setEcuName(map.get("name") == null ? null : map.get("name").toString());
                vehicleFlashDetail.setDid(map.get("resourceID") == null ? null : map.get("resourceID").toString());
                // 此为子标签
                List<Map<String, Object>> data = (ArrayList)map.get("data");
                for (Map<String, Object> dataMap : data) {
                    vehicleFlashDetail.setId(null);
                    vehicleFlashDetail.setVersionName(dataMap.get("name") == null ? null : dataMap.get("name").toString());
                    vehicleFlashDetail.setVersionValue(dataMap.get("Token") == null ? null : dataMap.get("Token").toString());
                    vehicleFlashDetail.setFid(dataMap.get("FID") == null ? null : dataMap.get("FID").toString());
                    vehicleFlashDetail.setResourceId(dataMap.get("resourceID") == null ? null : dataMap.get("resourceID").toString());
                    vehicleFlashDetail.setFlashTimestamp(dataMap.get("timestamp") == null ? null : getDate(dataMap.get("timestamp").toString()));
                    vehicleFlashDetail.setResult(3);
                    detailMapper.insert(vehicleFlashDetail);
                }
            }
        }
    }

    private Date getDate(Map<String, Object> testerInfoMap) throws ParseException {
        String dateString = testerInfoMap.get("Date") == null ? null : testerInfoMap.get("Date").toString();
        if (dateString == null) {
            return null;
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
        return format.parse(dateString);
    }

    private Date getDate(String dateString) throws ParseException {
        if (dateString == null) {
            return null;
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
        return format.parse(dateString);
    }

//    private void saveFlagFile(File file) {
//        FileLog fileLog;
//        fileLog = new FileLog();
//        fileLog.setFileName(file.getName());
//        fileLog.setFilePath(file.getPath());
//        fileLog.setStatus(1);
//        fileLogMapper.insert(fileLog);
//    }
}
