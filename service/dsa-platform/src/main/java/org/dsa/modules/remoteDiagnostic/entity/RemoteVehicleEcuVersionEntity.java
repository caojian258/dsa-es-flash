package org.dsa.modules.remoteDiagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.common.validator.group.UpdateGroup;
import org.dsa.modules.remoteDiagnostic.pojo.BaseEntity;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@ToString
@TableName("r_ecu_version")
public class RemoteVehicleEcuVersionEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long ecuId;

    private Long versionNumber;

    @NotBlank(message = "版本名称不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String versionName;

    @NotBlank(message = "版本类型不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String versionType;

    private String descriptionTi;

    private String releaseNodeTi;

    @NotNull(message = "状态不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private Integer status;

    private Integer isCross;

    @TableField(exist = false)
    private String ecuName;

    @TableField(exist = false)
    private Long flag;

    @TableField(exist = false)
    private String releaseNode;

    @TableField(exist = false)
    private String description;

    @TableField(exist = false)
    private List<RemoteEcuIdentificationInfoEntity> expectedIdentifications;

    @TableField(exist = false)
    private List<RemoteEcuIdentificationInfoEntity> targetIdentifications;

    @TableField(exist = false)
    private List<RemoteVehicleEcuVersionFileEntity> files;

    @TableField(exist = false)
    private List<RemoteVehicleEcuEntity> ecus;
}
