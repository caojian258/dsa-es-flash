package org.dsa.modules.reporter.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@TableName("d_vehicle_info")
public class VehicleInfo extends BaseEntity{

    @TableId(type = IdType.AUTO)
    private Long id;

    private String vin;

    private Long vehicleTypeId;

    private String productionDate;

    private String placeOfProduction;

    private Integer source;

}
