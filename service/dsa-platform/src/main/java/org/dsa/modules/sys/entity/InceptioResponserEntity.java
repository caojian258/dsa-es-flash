package org.dsa.modules.sys.entity;

import lombok.Data;

@Data
public class InceptioResponserEntity {
    String code;
    String msg;
    DisplayUserEntity data;
}
