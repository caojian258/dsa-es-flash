package org.dsa.modules.onlineUpdate.controller;

import cn.hutool.core.util.ObjectUtil;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.annotation.SysLog;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.R;
import org.dsa.modules.onlineUpdate.entity.ComponentEntity;
import org.dsa.modules.onlineUpdate.service.ComponentService;
import org.dsa.modules.onlineUpdate.utils.PageUtils;
import org.dsa.modules.onlineUpdate.vo.CascadeOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 * 在线升级-组件管理
 *
 */
@Slf4j
@RestController
@RequestMapping("onlineUpdate/component")
public class ComponentController {
    @Autowired
    private ComponentService componentService;

    /**
     * 列表
     */
    @RequestMapping("/list")
//    @RequiresPermissions("onlineUpdate:component:list")
    public R list(@RequestBody Map<String, Object> params){
        try {
            PageUtils page = componentService.queryPage(params);
            return R.ok().put("page", page);
        }catch (Exception e){
            log.error(e.getMessage());
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    @RequestMapping("/select")
//    @RequiresPermissions("onlineUpdate:component:list")
    public R select(){
        try {
            List<HashMap> options = componentService.queryAll();
            return R.ok().put("options", options);
        }catch (Exception e){
            log.error(e.getMessage());
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    @RequestMapping("/update_type")
//    @RequiresPermissions("onlineUpdate:component:list")
    public R selectType(){
        try {
            List<HashMap> options = componentService.queryUpdateTypeList();
            return R.ok().put("options", options);
        }catch (Exception e){
            log.error(e.getMessage());
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
//    @RequiresPermissions("onlineUpdate:component:info")
    public R info(@PathVariable("id") Integer id){
        try {
            ComponentEntity component = componentService.getById(id);
            if (ObjectUtil.isNull(component)){
                throw new RRException(Constant.Msg.DATA_INFO_NULL_ERROR);
            }
            return R.ok().put("component", component);
        }catch (RRException rre){
            log.error(rre.getMessage());
            return R.error(rre.getCode(), "操作失败： "+ rre.getMsg());
        }catch (Exception e){
            log.error(e.getMessage());
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }

    }

    /**
     * 保存
     */
    @SysLog("保存组件")
    @RequestMapping("/save")
//    @RequiresPermissions("onlineUpdate:component:save")
    public R save(@RequestBody ComponentEntity component){
        try {
            componentService.saveOrUpdate2(component);
            return R.ok();
        }catch (RRException rre){
            log.error(rre.getMessage());
            return R.error(rre.getCode(), "操作失败： "+ rre.getMsg());
        }catch (Exception e){
            log.error(e.getMessage());
            if (e.getMessage().contains("ERROR: duplicate key value violates unique")){
                return R.error(Constant.Msg.NAME_DUPLICATION_ERROR);
            }
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }

    }

    /**
     * 修改
     */
    @SysLog("修改组件")
    @RequestMapping("/update")
//    @RequiresPermissions("onlineUpdate:component:update")
    public R update(@RequestBody ComponentEntity component){
        try {
            if (ObjectUtil.isNull(component.getId())){
                throw new RRException(Constant.Msg.CHECK_ID_ERROR);
            }
            componentService.saveOrUpdate2(component);
            return R.ok();
        }catch (RRException rrException){
            log.error("组件更新 error: " + rrException.getMessage());
            return R.error(rrException.getCode(),"操作失败： "+ rrException.getMsg());
        }catch (Exception e){
            log.error("组件更新 error: " + e.getMessage());
            if (e.getMessage().contains("ERROR: duplicate key value violates unique")){
                return R.error(Constant.Msg.NAME_DUPLICATION_ERROR);
            }
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }

    }

    /**
     * 禁用/启用方法
     */
    @SysLog("禁用组件")
    @PostMapping("/ableOrDisabled")
//    @RequiresPermissions("onlineUpdate:component:ableOrDisabled")
    public R ableOrDisabled(@RequestBody ComponentEntity entity){
        try {
            componentService.ableOrDisabledById(entity.getId());
            return R.ok();
        }catch (RRException rrException){
            log.error("组件禁用/启用 error: " + rrException.getMessage());
            return R.error(rrException.getCode(),"操作失败： "+ rrException.getMsg());
        }catch (Exception e){
            log.error("组件禁用/启用 error: " + e.getMessage());
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    /**
     * 获取组件options
     */
    @RequestMapping("/componentOpts")
    public R getComponentOptions(){
        try {
            List<CascadeOptions> options = componentService.getComponentOptions();
            return R.ok().put("components", options);
        }catch (Exception e){
            log.error("componentOptsErr {}",e.getMessage());
            e.printStackTrace();
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }



}
