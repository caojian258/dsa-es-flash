package org.dsa.modules.reporter.service;

public interface DiagnosticStaticService {

    public void ctsDaily(String tag);

    public void dtcDaily(String tag);
}
