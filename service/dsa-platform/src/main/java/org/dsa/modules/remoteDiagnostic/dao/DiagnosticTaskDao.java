package org.dsa.modules.remoteDiagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskEntity;

@Mapper
public interface DiagnosticTaskDao  extends BaseMapper<DiagnosticTaskEntity> {


    @Select("select * from r_diag_task i where i.id = #{taskId} ")
    public DiagnosticTaskEntity getInfo(@Param("taskId") Long taskId);
}
