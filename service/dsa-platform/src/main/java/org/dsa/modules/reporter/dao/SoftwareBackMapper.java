package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.dsa.modules.reporter.entity.SoftwareBack;

@Mapper
public interface SoftwareBackMapper extends BaseMapper<SoftwareBack> {
}
