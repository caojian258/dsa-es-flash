package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.dsa.modules.reporter.entity.ComponentBack;

@Mapper
public interface ComponentBackMapper extends BaseMapper<ComponentBack> {
}
