package org.dsa.modules.sys.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class WorkshopImportMsgDto implements Serializable {

    private Integer total;

    private Integer successTotal;

    private Integer failedTotal;

    private String msg;
}
