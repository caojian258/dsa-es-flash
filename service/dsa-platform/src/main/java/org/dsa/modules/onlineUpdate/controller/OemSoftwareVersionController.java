package org.dsa.modules.onlineUpdate.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import cn.hutool.core.util.ObjectUtil;
import org.dsa.common.annotation.SysLog;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.R;
import org.dsa.modules.onlineUpdate.dto.OemSoftwareVersionDto;
import org.dsa.modules.onlineUpdate.vo.CascadeOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.dsa.modules.onlineUpdate.entity.OemSoftwareVersionEntity;
import org.dsa.modules.onlineUpdate.service.OemSoftwareVersionService;
import org.dsa.common.utils.PageUtils;



/**
 * onlineUpdate
 *
 */
@Slf4j
@RestController
@RequestMapping("onlineUpdate/oemSoftwareVersion")
public class OemSoftwareVersionController {
    @Autowired
    private OemSoftwareVersionService oemSoftwareVersionService;

    /**
     * 列表
     */
    @RequestMapping("/list")
//    @RequiresPermissions("onlineUpdate:oemSoftwareVersion:list")
    public R list(@RequestBody Map<String, Object> params){
        try{
            PageUtils page = oemSoftwareVersionService.queryPage(params);
            return R.ok().put("page", page);
        }catch (Exception e) {
            log.error("software list error:"+ e.getMessage());
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }

    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
//    @RequiresPermissions("onlineUpdate:oemSoftwareVersion:info")
    public R info(@PathVariable("id") Integer id){
         try {
             OemSoftwareVersionEntity oemSoftwareVersion = oemSoftwareVersionService.getById(id);

             if (ObjectUtil.isNull(oemSoftwareVersion)){
                throw new RRException(Constant.Msg.DATA_INFO_NULL_ERROR);
             }
             return R.ok().put("oemSoftwareVersion", oemSoftwareVersion);
         }catch (RRException rre) {
            log.error(String.format("oemSoftwareVersion info error: id: %s msg: %s",id,rre.getMessage()));
            return R.error(rre.getCode(),rre.getMsg());
         } catch (Exception e) {
            log.error("oemSoftwareVersion info error:"+ e.getMessage());
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
         }

    }

    /**
     * 保存
     */
    @SysLog("保存OEM关联软件版本")
    @RequestMapping("/save")
//    @RequiresPermissions("onlineUpdate:oemSoftwareVersion:save")
    public R save(@RequestBody OemSoftwareVersionDto dto){
        try {
            log.info(String.format("save software param {} %s", JSON.toJSONString(dto)));
            if (dto.getVersions().size()==0){
                throw new RRException(Constant.Msg.OEM_NO_BIND_SOFTWARE_ERROR);
            }
            Integer oemId = dto.getVersions().get(0).getOemId();
            int exist = oemSoftwareVersionService.list(new QueryWrapper<OemSoftwareVersionEntity>().eq("oem_id", oemId)).size();
            if (exist>0){
                oemSoftwareVersionService.remove(new QueryWrapper<OemSoftwareVersionEntity>().eq("oem_id", oemId));
            }
            dto.getVersions().forEach(oemSoftwareVersion -> {
                oemSoftwareVersionService.saveOrUpdate2(oemSoftwareVersion);
            });
            return R.ok();
        } catch (RRException rre) {
            log.error("oemSoftwareVersion save error:"+ rre.getMessage());
            return R.error(rre.getCode(),rre.getMsg());
        } catch (Exception e) {
            log.error("oemSoftwareVersion save error:"+ e.getMessage());
            if (e.getMessage().contains("ERROR: duplicate key value violates unique")) {
                return R.error(Constant.Msg.NAME_DUPLICATION_ERROR);
            }
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
//    @RequiresPermissions("onlineUpdate:oemSoftwareVersion:update")
    public R update(@RequestBody OemSoftwareVersionDto dto){
        try {
            log.info(String.format("update software {} %s", JSON.toJSONString(dto)));
            if (dto.getVersions().size()==0){
                throw new RRException(Constant.Msg.OEM_NO_BIND_SOFTWARE_ERROR);
            }
            dto.getVersions().forEach(oemSoftwareVersion -> {
                if (ObjectUtil.isNull(oemSoftwareVersion.getId())) {
                    throw new RRException(Constant.Msg.CHECK_ID_ERROR);
                }
                oemSoftwareVersionService.saveOrUpdate2(oemSoftwareVersion);
            });
            return R.ok();
        } catch (RRException rre) {
            log.error("oemSoftwareVersion update error:"+ rre.getMessage());
            return R.error(rre.getCode(),rre.getMsg());
        } catch (Exception e) {
            log.error("oemSoftwareVersion update error:"+ e.getMessage());
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    @RequestMapping("versionTree")
    public R versionTree(){
        try {
            List<CascadeOptions> options =  oemSoftwareVersionService.queryVersionTree();
            return R.ok().put("options",options);
        }catch (Exception e){
            log.error("oemSoftwareVersion update error:"+ e.getMessage());
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
//    @RequiresPermissions("onlineUpdate:oemSoftwareVersion:delete")
    public R delete(@RequestBody Integer[] ids){
         try {
             log.info(String.format("disabled oemSoftwareVersion param {} %d", JSON.toJSONString(ids)));
		     oemSoftwareVersionService.removeByIds(Arrays.asList(ids));
             return R.ok();
         } catch (RRException rre) {
             log.error("delete oemSoftwareVersion param {} %s" + rre.getMessage());
             return R.error(rre.getCode(),rre.getMsg());
         } catch (Exception e) {
             log.error("delete oemSoftwareVersion error:"+ e.getMessage());
             return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
         }
    }

}
