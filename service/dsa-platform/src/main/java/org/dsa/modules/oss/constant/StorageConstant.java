package org.dsa.modules.oss.constant;

public final class StorageConstant {

    public final static String LOCAL = "local";

    public final static String REMOTE = "remote";

    //文件上传根目录配置
    public final static String LOCAL_CONFIG_KEY = "LOCAL_STORAGE_CONFIG_KEY";

    //业务类型对应文件目录
    public final static String LOCAL_CATEGORY_KEY = "LOCAL_CATEGORY_CONFIG_KEY";
}
