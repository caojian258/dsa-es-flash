package org.dsa.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

/**
 * @author weishunxin
 * @since 2023-06-20
 */
@Setter
@Getter
@TableName("sys_notice")
public class SysNoticeEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String title;

    private String fileName;

    private String filePath;

    private String fileMd5;

    private Integer status;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date expireDay;

    private String locale;

    private Date createdAt;

    private Date updatedAt;

    private Long createdUserId;

    private Long updatedUserId;

    @TableField(exist = false)
    private String url;

}
