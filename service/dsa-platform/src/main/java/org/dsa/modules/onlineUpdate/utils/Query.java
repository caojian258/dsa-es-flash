package org.dsa.modules.onlineUpdate.utils;

import cn.hutool.core.map.MapUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.utils.Constant;
import org.dsa.common.xss.SQLFilter;

import java.util.Map;

/**
 * 查询参数
 *
 */
public class Query<T> {

    public IPage<T> getPage(Map<String, Object> params) {
        return this.getPage(params, null, false);
    }

    public IPage<T> getPage(Map<String, Object> params, String defaultOrderField, boolean isAsc) {
        //分页参数
        long curPage = 1;
        long limit = 10;

        if(params.get(Constant.PAGE) != null){
            curPage = MapUtil.getLong(params, Constant.PAGE);
        }
        if(params.get(Constant.LIMIT) != null){
            limit = MapUtil.getLong(params, Constant.LIMIT);
        }

        //分页对象
        Page<T> page = new Page<>(curPage, limit);

        //分页参数
        params.put(org.dsa.common.utils.Constant.PAGE, page);

        //排序字段
        //防止SQL注入（因为sidx、order是通过拼接SQL实现排序的，会有SQL注入风险）
        String orderField = SQLFilter.sqlInject((String)params.get(org.dsa.common.utils.Constant.ORDER_FIELD));
        String order = (String)params.get(org.dsa.common.utils.Constant.ORDER);

        //前端字段排序
        if(StringUtils.isNotEmpty(orderField) && StringUtils.isNotEmpty(order)){
            OrderItem orderItem = new OrderItem();
            orderItem.setColumn(orderField);
            if(Constant.ASC.equalsIgnoreCase(order)) {
                orderItem.setAsc(true);
            }else {
                orderItem.setAsc(false);
            }
            return page.addOrder(orderItem);
        }
        OrderItem orderItem = new OrderItem();
        orderItem.setColumn(defaultOrderField);
        //默认排序
        if(isAsc) {
            orderItem.setAsc(true);
        }else {
            orderItem.setAsc(false);
        }
        page.addOrder(orderItem);

        return page;
    }
}
