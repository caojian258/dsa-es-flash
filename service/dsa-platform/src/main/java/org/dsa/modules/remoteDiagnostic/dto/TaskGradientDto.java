package org.dsa.modules.remoteDiagnostic.dto;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class TaskGradientDto implements Serializable {

    private Integer flag;

    private Long gradient;

    private Long gradientValue;

    private Integer gradientCondition;

}
