package org.dsa.modules.reporter.handler;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.pojo.RowMapper;
import org.dsa.common.utils.RedisUtils;
import org.dsa.common.utils.SqliteUtils;
import org.dsa.modules.reporter.config.ReportEsConfig;
import org.dsa.modules.reporter.constant.ConfigConstant;
import org.dsa.modules.reporter.constant.RedisConstant;
import org.dsa.modules.reporter.document.*;
import org.dsa.modules.reporter.dto.SessionDto;
import org.dsa.modules.reporter.enums.EsIndexEnum;
import org.dsa.modules.reporter.enums.RecordFlagEnum;
import org.dsa.modules.reporter.service.ElasticsearchRestService;
import org.dsa.modules.reporter.util.JsonReader;
import org.dsa.modules.reporter.util.SnowflakeIdWorker;
import org.dsa.modules.sys.entity.SysUserEntity;
import org.dsa.modules.sys.service.SysUserService;
import org.dsa.modules.vehicle.dao.DVehicleInfoDao;
import org.dsa.modules.vehicle.entity.DVehicleInfoEntity;
import org.dsa.modules.vehicle.entity.DVehicleTypeEntity;
import org.dsa.modules.vehicle.service.VehicleTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.Resource;

import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
@Service
public class SessionLogHandler {

    private final Integer DEFAULT_WEIGHT = 10;

    @Autowired
    private ReportEsConfig esConfig;

    @Autowired
    private ElasticsearchRestService esService;

    @Autowired
    private VehicleTypeService vehicleTypeService;

    @Autowired
    private DVehicleInfoDao dVehicleInfoDao;

    @Autowired
    SysUserService sysUserService;

    @Autowired
    private RedisUtils redisUtils;

    @PostConstruct
    public void initIndexTemplate(){
        Map<String, String> tplList = esConfig.getTemplate();
        if(null != tplList && tplList.size()>0){
            tplList.forEach((key, value) -> {
                try {
                    String name = "template_"+ key;

                    InputStream resource = new ClassPathResource(value).getInputStream();
                    String content = "";
                    BufferedReader reader = new BufferedReader(new InputStreamReader(resource));
                    StringBuffer sb = new StringBuffer();
                    int ch = 0;
                    while ((ch = reader.read()) != -1) {
                        sb.append((char) ch);
                    }
                    content = sb.toString();
                    Boolean exist = esService.checkExistTemplate(name);
                    log.info("es index template {} exist: {}", name, exist);
                    if(!exist){
                        esService.createIndexTemplateByString(name, content);
                    }
                } catch (IOException e) {
                    log.error("error: {}",e.getMessage());
                }
            });
        }
    }

    public SessionDto saveData(String path, String tag, String name, String pcid) throws SQLException, ClassNotFoundException {

        String sessionId = "";
        String vin = "";
        Integer version = 100;
        Map<String, DiagnosticEcuDoc> diagnosticEcuDocMap = new HashMap<>();

        //userId userName workshopId workshopName来自 t_vehicle_report user_id
        SessionDto sessionDto = new SessionDto();
        if(StrUtil.isNotBlank(pcid)){
            sessionDto.setPcid(pcid);
        }
        SnowflakeIdWorker idWorker = new SnowflakeIdWorker(0, 0);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        SqliteUtils utils = new SqliteUtils(path);
        String[] names = name.replace(".sqlite","").split("_");

        if( names.length>2 ) {
            vin = names[1];
            sessionDto.setVinCode(vin);
            if(null != names[2]){
                sessionId = names[2];
                sessionDto.setSessionId(sessionId);
            }
        }

        //通过车辆信息获取车型ID
        List<Long> vehicleTypeIds = new ArrayList<>();
        if(StrUtil.isNotBlank(vin)){
            DVehicleInfoEntity dVehicleInfoEntity = dVehicleInfoDao.selectOneByVin(vin);
            if(null != dVehicleInfoEntity && dVehicleInfoEntity.getVehicleTypeId()>0){
                List<DVehicleTypeEntity> typeList =  vehicleTypeService.selectListById(dVehicleInfoEntity.getVehicleTypeId());
                if(null != typeList){
                    for (DVehicleTypeEntity typeEntity: typeList) {
                        vehicleTypeIds.add(typeEntity.getId());
                    }
                }
            }
        }

        //版本信息
        if(utils.exists("t_version")){
            List<Integer> versions =  utils.executeQuery("select version from t_version order by create_time desc limit 1", new RowMapper<Integer>() {
                @Override
                public Integer mapRow(ResultSet rs, int index) throws SQLException {
                    return rs.getInt("version");
                }
            });
            if(versions !=null && versions.size()>0){
                version = versions.get(0);
                sessionDto.setVersion(version);
            }
        }

        //生成sessionInfoId
        String sessionInfoId = String.valueOf(idWorker.nextId());
        sessionDto.setSessionInfoId(sessionInfoId);

        //诊断报告
        Boolean vsshFlag = utils.exists("t_vehicle_report");

        if(vsshFlag){
            utils.executeQuery("select * from t_vehicle_report limit 1", new RowMapper<VehicleReportDoc>() {
                @Override
                public VehicleReportDoc mapRow(ResultSet rs, int index) throws SQLException {
                    VehicleReportDoc doc = new VehicleReportDoc();
                    doc.setSessionId(sessionDto.getSessionId());
                    doc.setSessionInfoId(sessionDto.getSessionInfoId());
                    doc.setId(String.valueOf(idWorker.nextId()));
                    doc.setVin(rs.getString("vin"));
                    doc.setModel(rs.getString("model"));
                    doc.setBrand(rs.getString("brand"));
                    doc.setVin(sessionDto.getVinCode());
                    doc.setVehicleTypeId(vehicleTypeIds);
                    doc.setWorkshop(rs.getString("workshop"));
                    if(StrUtil.isNotBlank(rs.getString("user_id"))){
                        doc.setUserId(rs.getString("user_id"));
                        if(StrUtil.isNotBlank(doc.getUserId())){
                            SysUserEntity user = null;
                            if(StringUtils.isNumeric(doc.getUserId())){
                                sessionDto.setUserId(Long.valueOf(doc.getUserId()));
                                user =  sysUserService.getUserByUserId(Long.valueOf(doc.getUserId()));

                            }else{
                                sessionDto.setUserName(doc.getUserId());
                                user = sysUserService.queryByUserName(doc.getUserId());
                            }

                            if(null != user){
                                sessionDto.setUserId(user.getUserId());
                                sessionDto.setUserName(user.getUsername());
                                sessionDto.setWorkshop(user.getWorkshopName());
                                sessionDto.setWorkshopId(user.getWorkshopId());
                            }
                        }
                    }

                    String createTime = rs.getString("create_time");
                    if(null != createTime){
                        try {
                            doc.setCreateTime(simpleDateFormat.parse(createTime));
                        }catch (ParseException ex){
                            log.error("Session createTime ParseException: {}", ex.getMessage());
                        }
                    }
                    doc.setCreatedAt(new Date());

                    String indexName = doc.genIndexName(EsIndexEnum.VEHICLE_REPORT.getValue(), doc.getCreateTime());
                    try {
                        Map<String, Object> docMap = BeanUtil.beanToMap(doc);
                        log.info("Session ES store index {} data: {}", indexName, JSONObject.toJSONString(docMap));
                        esService.addIndexDoc(indexName, docMap);
                        sessionDto.setVehicleReportTotal(1);

                    } catch (IOException e) {
                        log.error("Session ES store error: {}", e.getMessage());
                    }

                    return doc;
                }
            });
        }

        //诊断会话
        Boolean sessionFlag = utils.exists("t_session");
        log.info("t_session flag: {}", sessionFlag);

        if(sessionFlag){
            List<SessionDoc> srs =  utils.executeQuery("select * from t_session where session_id='"+sessionId+"' order by id desc limit 1",new RowMapper<SessionDoc>() {
                @Override
                public SessionDoc mapRow(ResultSet rs, int index) throws SQLException {
                    SessionDoc doc = new SessionDoc();

                    sessionDto.setSource(ConfigConstant.DEFAULT_SOURCE);

                    if(StrUtil.isNotBlank(rs.getString("diag_source"))){
                        sessionDto.setSource(rs.getString("diag_source"));
                    }
                    if(null != rs.getString("diag_category")){
                        sessionDto.setCategory( Integer.valueOf(rs.getString("diag_category")));
                    }

                    if(null != sessionDto.getPcid()){
                        doc.setPcid(sessionDto.getPcid());
                    }

                    doc.setId(sessionDto.getSessionInfoId());
                    doc.setSessionId(sessionDto.getSessionId());
                    doc.setUserId(sessionDto.getUserId());
                    doc.setUserName(sessionDto.getUserName());
                    doc.setWorkshopId(sessionDto.getWorkshopId());
                    doc.setWorkshop(sessionDto.getWorkshop());
                    doc.setDiagCategory(sessionDto.getCategory());
                    doc.setDiagSource(sessionDto.getSource());
                    doc.setLogName(name);
                    doc.setLogPath(path);
                    doc.setTag(tag);
                    doc.setSourceVersion(sessionDto.getVersion());
                    doc.setCreatedAt(new Date());
                    doc.setVin(sessionDto.getVinCode());
                    doc.setVehicleTypeId(vehicleTypeIds);
                    String createTime = rs.getString("create_time");

                    if(null != createTime){
                        try {
                            doc.setCreateTime(simpleDateFormat.parse(createTime));
                            sessionDto.setStartTime(doc.getCreateTime());
                        }catch (ParseException ex){
                            log.error("Session createTime ParseException: {}", ex.getMessage());
                        }
                    }
                    String updateTime = rs.getString("last_update_time");
                    if(null != updateTime){
                        try {
                            doc.setLastUpdateTime(simpleDateFormat.parse(updateTime));
                        }catch (ParseException ex){
                            log.error("Session updateTime ParseException: {}", ex.getMessage());
                        }
                    }
                    Map<String, Object> suggest = new HashMap<>();
                    suggest.put("input", doc.getVin());
                    suggest.put("weight", DEFAULT_WEIGHT);
                    doc.setVinSuggest(suggest);

                    String indexName = doc.genIndexName(EsIndexEnum.SESSION.getValue(), doc.getCreateTime());
                    sessionDto.setSessionIndex(indexName);
                    try {

                        Map<String, Object> docMap = BeanUtil.beanToMap(doc);
                        log.info("Session ES store index {} data: {}", indexName, JSONObject.toJSONString(docMap, SerializerFeature.WriteDateUseDateFormat));
                        esService.addIndexDoc(indexName, docMap);

                    } catch (IOException e) {
                        log.error("Session ES store error: {}", e.getMessage());
                    }

                    return doc;
                }
            });
        }

        //诊断操作
        Boolean diagFlag = utils.exists("t_diagnostic");
        HashMap<Integer, String> diagnosticIdMap = new HashMap<>();

        if(diagFlag){
            List<DiagnosticDoc> drs =  utils.executeQuery("select * from t_diagnostic order by id asc", new RowMapper<DiagnosticDoc>(){
                @Override
                public DiagnosticDoc mapRow(ResultSet rs, int index) throws SQLException {
                    DiagnosticDoc doc = new DiagnosticDoc();

                    doc.setId(String.valueOf(idWorker.nextId()));
                    doc.setDiagSource(sessionDto.getSource());
                    doc.setDiagCategory(sessionDto.getCategory());
                    doc.setSessionId(sessionDto.getSessionId());
                    doc.setSessionInfoId(sessionDto.getSessionInfoId());
                    doc.setFunctionCode(rs.getString("function_code"));
                    doc.setAction(rs.getString("action"));
                    doc.setTag(tag);
                    doc.setEcuName(rs.getString("ecu_name"));
                    doc.setSpecialFunctionName(rs.getString("special_function_name"));
                    String userId = rs.getString("user_id");
                    if(StrUtil.isNotBlank(userId)){
                        if(StringUtils.isNumeric(userId)){
                            doc.setUserId(Long.valueOf(userId));
                        }else{
                            doc.setUserName(userId);
                        }
                    }

                    diagnosticIdMap.put(rs.getInt("id"), doc.getId());

                    doc.setErrorCode(rs.getString("error_code"));
                    doc.setErrorMessage(rs.getString("error_message"));
                    doc.setCreatedAt(new Date());
                    doc.setNrcDescription(rs.getString("nrc_description"));
                    doc.setNrcHex(rs.getString("nrc_hex"));
                    doc.setNrcDescriptionTi(rs.getString("nrc_description_ti"));
                    doc.setVin(sessionDto.getVinCode());
                    doc.setVehicleTypeId(vehicleTypeIds);
                    String createTime = rs.getString("create_time");

                    Map<String, Object> suggest = new HashMap<>();
                    suggest.put("input", doc.getEcuName());
                    suggest.put("weight", DEFAULT_WEIGHT);
                    doc.setEcuNameSuggest(suggest);

                    if(null != createTime){
                        try {
                            doc.setCreateTime(simpleDateFormat.parse(createTime));
                        }catch (ParseException ex){
                            log.error("Session createTime ParseException: {}", ex.getMessage());
                        }
                    }

                    String updateTime = rs.getString("last_update_time");
                    if(null != updateTime){
                        try {
                            doc.setLastUpdateTime(simpleDateFormat.parse(updateTime));
                            sessionDto.setEndTime(doc.getLastUpdateTime());
                        }catch (ParseException ex){
                            log.error("Session updateTime ParseException: {}", ex.getMessage());
                        }
                    }

                    if(null != doc.getUserId()  && null == sessionDto.getUserId()){
                        sessionDto.setUserId(doc.getUserId());
                        SysUserEntity user =  sysUserService.getUserByUserId(Long.valueOf(doc.getUserId()));
                        if(null != user){
                            doc.setUserName(user.getUsername());
                            sessionDto.setUserName(user.getUsername());
                            sessionDto.setWorkshop(user.getWorkshopName());
                            sessionDto.setWorkshopId(user.getWorkshopId());
                        }
                    }else if(null != doc.getUserName() && null == sessionDto.getUserId()){
                        SysUserEntity user =  sysUserService.queryByUserName(doc.getUserName());
                        if(null != user){
                            doc.setUserId(user.getUserId());
                            sessionDto.setUserId(user.getUserId());
                            sessionDto.setUserName(user.getUsername());
                            sessionDto.setWorkshop(user.getWorkshopName());
                            sessionDto.setWorkshopId(user.getWorkshopId());
                        }
                    }
                    if(null == doc.getUserName() && null != sessionDto.getUserName()){
                        doc.setUserName(sessionDto.getUserName());
                    }
                    if(null == doc.getUserId() && null != sessionDto.getUserId()){
                        doc.setUserId(sessionDto.getUserId());
                    }
                    return doc;
                }
            });
            if(null !=drs && drs.size()>0){
                String indexName = "";
                List<Map<String, Object>> diagMap = new ArrayList<>();


                for (DiagnosticDoc diagDoc: drs) {

                    //source:vin:cts:day
                    if(StrUtil.isNotBlank(diagDoc.getDiagSource())
                            && StrUtil.isNotBlank(diagDoc.getFunctionCode())
                            && StrUtil.isNotBlank(diagDoc.getVin())
                            && null != diagDoc.getCreateTime()){
                        String ctsKey = ctsKey(tag);
                        String ctsItem = ctsItem(diagDoc);
                        redisUtils.hincr(ctsKey, ctsItem,1);
                    }

                    //这里默认记录最后一次ECU的状态
                    if( StrUtil.isNotBlank(diagDoc.getEcuName())){
                        DiagnosticEcuDoc ecuDoc = diagnosticEcuDocMap.get(diagDoc.getEcuName());
                        if(null == ecuDoc){
                            ecuDoc = new DiagnosticEcuDoc();
                            ecuDoc.setEcuName(diagDoc.getEcuName());
                            ecuDoc.setSessionId(sessionDto.getSessionId());
                            ecuDoc.setSessionInfoId(sessionDto.getSessionInfoId());
                            ecuDoc.setTag(tag);
                            ecuDoc.setVin(vin);
                            ecuDoc.setVehicleTypeId(vehicleTypeIds);
                            ecuDoc.setStatus(Integer.valueOf(diagDoc.getErrorCode()));
                            ecuDoc.setCreateTime(diagDoc.getCreateTime());
                            ecuDoc.setDtcCount(0);
                        }else{
                            ecuDoc.setStatus(Integer.valueOf(diagDoc.getErrorCode()));
                            ecuDoc.setCreateTime(diagDoc.getCreateTime());
                            ecuDoc.setDtcCount(0);
                        }
                        diagnosticEcuDocMap.put(diagDoc.getEcuName(), ecuDoc);
                    }

                    if(indexName.equals("")){
                        indexName = diagDoc.genIndexName(EsIndexEnum.DIAG.getValue(),  diagDoc.getCreateTime());
                    }
                    Map<String, Object> docMap = BeanUtil.beanToMap(diagDoc);
                    diagMap.add(docMap);
                }

                try {
                    log.info("Session ES store {}: {}", indexName, JSONObject.toJSONString(diagMap));
                    esService.bulkAddIndexDoc(indexName, diagMap);
                    sessionDto.setDiagnosticTotal(diagMap.size());

                } catch (IOException e) {
                    log.error("Session ES store error: {}", e.getMessage());
                }
            }
        }

        Boolean asMaintainFlag = utils.exists("t_vehicle_asmaintain");
        if(asMaintainFlag){
            List<VehicleAsMaintainDoc> vasList = utils.executeQuery("select * from t_vehicle_asmaintain order by id asc", new RowMapper<VehicleAsMaintainDoc>() {
                @Override
                public VehicleAsMaintainDoc mapRow(ResultSet rs, int index) throws SQLException {
                    VehicleAsMaintainDoc asMaintainDoc = new VehicleAsMaintainDoc();
                    asMaintainDoc.setSessionId(sessionDto.getSessionId());
                    asMaintainDoc.setSessionInfoId(sessionInfoId);
                    asMaintainDoc.setVin(sessionDto.getVinCode());
                    asMaintainDoc.setVehicleTypeId(vehicleTypeIds);
                    asMaintainDoc.setWorkshop(sessionDto.getWorkshop());
                    asMaintainDoc.setUserId(sessionDto.getUserId().toString());
                    asMaintainDoc.setUserName(sessionDto.getUserName());

                    asMaintainDoc.setEcuName(rs.getString("ecu_name"));
                    asMaintainDoc.setDidHex(rs.getString("did_hex"));
                    asMaintainDoc.setDidName(rs.getString("did_name"));
                    asMaintainDoc.setDidNameTi(rs.getString("did_name_ti"));
                    asMaintainDoc.setParameterName(rs.getString("parameter_name"));
                    asMaintainDoc.setParameterNameTi(rs.getString("parameter_name_ti"));
                    asMaintainDoc.setValue(rs.getString("value"));
                    asMaintainDoc.setValueTi(rs.getString("value_ti"));
                    asMaintainDoc.setDidErrorCode(rs.getInt("did_error_code"));
                    asMaintainDoc.setDidErrorMessage(rs.getString("did_error_message"));

                    Integer diagnosticId = Integer.valueOf(rs.getString("diagnostic_id"));
                    if(null != diagnosticId && null != diagnosticIdMap.get(diagnosticId)){
                        asMaintainDoc.setDiagnosticId(diagnosticIdMap.get(diagnosticId));
                    }

                    String createTime = rs.getString("create_time");
                    if(null != createTime){
                        try {
                            asMaintainDoc.setCreateTime(simpleDateFormat.parse(createTime));
                        }catch (ParseException ex){
                            log.error("Session createTime ParseException: {}", ex.getMessage());
                        }
                    }

                    String updateTime = rs.getString("last_update_time");
                    if(null != updateTime){
                        try {
                            asMaintainDoc.setLastUpdateTime(simpleDateFormat.parse(updateTime));
                        }catch (ParseException ex){
                            log.error("Session updateTime ParseException: {}", ex.getMessage());
                        }
                    }

                    asMaintainDoc.setCreatedAt(new Date());

                    return asMaintainDoc;
                }
            });

            if(null !=vasList && vasList.size()>0){
                List<Map<String, Object>> vasMap = new ArrayList<>();
                String indexName = "";

                for (VehicleAsMaintainDoc asDoc: vasList) {
                    if(indexName.equals("")){
                        indexName = asDoc.genIndexName(EsIndexEnum.AS_MAINTAIN.getValue(), asDoc.getCreateTime());
                    }
                    Map<String, Object> docMap = BeanUtil.beanToMap(asDoc);
                    vasMap.add(docMap);
                }
                try {
                    log.info("Session ES store {}: {}", indexName, JSONObject.toJSONString(vasMap));
                    esService.bulkAddIndexDoc(indexName, vasMap);
                    sessionDto.setAsMaintainTotal(vasMap.size());

                } catch (IOException e) {
                    log.error("Session ES store error: {}", e.getMessage());
                }
            }
        }

        //更新session_info文档, 更新内容: 用户 经销商信息 诊断开始时间 结束时间
        try {
            Map<String, Object> sMap = new HashMap<>();
            sMap.put("startTime", sessionDto.getStartTime());
            sMap.put("endTime", sessionDto.getEndTime());
            sMap.put("userId", sessionDto.getUserId());
            sMap.put("userName", sessionDto.getUserName());
            sMap.put("workshopId", sessionDto.getWorkshopId());
            sMap.put("workshop", sessionDto.getWorkshop());
            esService.updateDoc(sessionDto.getSessionIndex(), sessionDto.getSessionInfoId(), sMap);
        } catch (IOException e) {
            log.error("Session update session index {} error: {}", sessionDto.getSessionIndex(), e.getMessage());
        }

        //DTC 故障码
        Boolean dtcFlag = utils.exists("t_dtc");
        Map<String, Integer> ecuDtcMap = new HashMap<>();

        if(dtcFlag){
            List<DtcDoc> drs =  utils.executeQuery("select * from t_dtc order by id asc", new RowMapper<DtcDoc>() {
                @Override
                public DtcDoc mapRow(ResultSet rs, int index) throws SQLException {
                    DtcDoc doc = new DtcDoc();
                    doc.setId(String.valueOf(idWorker.nextId()));
                    doc.setSessionId(sessionDto.getSessionId());
                    doc.setSessionInfoId(sessionDto.getSessionInfoId());
                    doc.setVin(sessionDto.getVinCode());
                    doc.setVehicleTypeId(vehicleTypeIds);
                    doc.setTag(tag);
                    doc.setDiagSource(sessionDto.getSource());
                    doc.setDiagCategory(sessionDto.getCategory());
                    doc.setUserId(sessionDto.getUserId());
                    doc.setUserName(sessionDto.getUserName());
                    doc.setWorkshopId(sessionDto.getWorkshopId());
                    doc.setWorkshopName(sessionDto.getWorkshop());
                    doc.setRecordFlag(rs.getInt("record_flag"));
                    doc.setEcuName(rs.getString("ecu_name"));
                    doc.setDtcCode(rs.getString("dtc_code"));
                    doc.setDtcCodeHex(rs.getString("dtc_code_hex"));
                    doc.setDtcCodeInt(rs.getInt("dtc_code_int"));
                    doc.setDescription(rs.getString("description"));
                    doc.setDescriptionTi(rs.getString("description_ti"));
                    doc.setStatus(rs.getString("status"));
                    doc.setStatusTi(rs.getString("status_ti"));
                    doc.setStatusByteHex(rs.getString("status_byte_hex"));
                    doc.setErrorCode(rs.getString("error_code"));
                    doc.setErrorMessage(rs.getString("error_message"));
                    doc.setCreatedAt(new Date());
                    doc.setNrcDescription(rs.getString("nrc_description"));
                    doc.setNrcHex(rs.getString("nrc_hex"));
                    doc.setNrcDescriptionTi(rs.getString("nrc_description_ti"));
                    doc.setSnapshotEnable(rs.getInt("snapshot_enable"));
                    doc.setExtendedDataEnable(rs.getInt("extended_data_enable"));

                    Map<String, Object> suggest = new HashMap<>();
                    suggest.put("input", doc.getDtcCode());
                    suggest.put("weight", DEFAULT_WEIGHT);
                    doc.setDtcCodeSuggest(suggest);

                    String createTime = rs.getString("create_time");
                    if(null != createTime){
                        try {
                            doc.setCreateTime(simpleDateFormat.parse(createTime));
                        }catch (ParseException ex){
                            log.error("Session createTime ParseException: {}", ex.getMessage());
                        }
                    }

                    String updateTime = rs.getString("last_update_time");
                    if(null != updateTime){
                        try {
                            doc.setLastUpdateTime(simpleDateFormat.parse(updateTime));
                        }catch (ParseException ex){
                            log.error("Session updateTime ParseException: {}", ex.getMessage());
                        }
                    }
                    return doc;
                }
            });

            if(null !=drs && drs.size()>0){
                String indexName = "";
                List<Map<String, Object>> dtcMap = new ArrayList<>();
                for (DtcDoc dtcDoc: drs) {
                    //这里只统计最后一次出现DTC场景
                    if(dtcDoc.getRecordFlag() == 2 || dtcDoc.getRecordFlag() == 3){
                        Integer dtcCount = ecuDtcMap.get(dtcDoc.getEcuName());
                        if(null == dtcCount){
                            ecuDtcMap.put(dtcDoc.getEcuName(), 1);
                        }else{
                            ecuDtcMap.put(dtcDoc.getEcuName(), dtcCount+1);
                        }
                    }

                    if(indexName.equals("")){
                        indexName = dtcDoc.genIndexName(EsIndexEnum.DTC.getValue(),  dtcDoc.getCreateTime());
                    }

                    // 缓存dtcDaily  vin:ecu:dtc:dtc_int:dtc_hex:ti:description:day
                    if(StrUtil.isNotBlank(dtcDoc.getVin())
                            && StrUtil.isNotBlank(dtcDoc.getEcuName())
                            && StrUtil.isNotBlank(dtcDoc.getDtcCode())
                            && null != dtcDoc.getCreateTime()){
                        String dtcDailyKey = dtcKey(tag);
                        String dtcDailyItem = dtcItem(dtcDoc);
                        redisUtils.hincr(dtcDailyKey, dtcDailyItem, 1);
                    }

                    Map<String, Object> docMap = BeanUtil.beanToMap(dtcDoc);
                    dtcMap.add(docMap);
                }

                try {
                    log.info("Session ES store {}: {}", indexName, JSONObject.toJSONString(dtcMap));
                    esService.bulkAddIndexDoc(indexName, dtcMap);
                    sessionDto.setDtcTotal(dtcMap.size());

                } catch (IOException e) {
                    log.error("Session ES store error: {}", e.getMessage());
                }
            }

        }

        //ECU DTC
        if(diagnosticEcuDocMap.size()>0){
            String ecuIndexName = "";
            List<Map<String, Object>> ecuMap = new ArrayList<>();
            Iterator digEcu = diagnosticEcuDocMap.entrySet().iterator();
            while (digEcu.hasNext()) {
                Map.Entry entry = (Map.Entry) digEcu.next();
                DiagnosticEcuDoc digEcuDoc = (DiagnosticEcuDoc)entry.getValue();
                String ecu = (String) entry.getKey();
                if(null != ecuDtcMap.get(ecu)){
                    digEcuDoc.setDtcCount(ecuDtcMap.get(ecu));
                }else{
                    digEcuDoc.setDtcCount(0);
                }
                Map<String, Object> suggest = new HashMap<>();
                suggest.put("input", ecu);
                suggest.put("weight", DEFAULT_WEIGHT);
                digEcuDoc.setEcuNameSuggest(suggest);

                digEcuDoc.setCreateTime(new Date());
                digEcuDoc.setId(String.valueOf(idWorker.nextId()));
                ecuMap.add(BeanUtil.beanToMap(digEcuDoc));
                if(ecuIndexName.equals("")){
                    ecuIndexName = digEcuDoc.genIndexName(EsIndexEnum.ECU.getValue(),  digEcuDoc.getCreateTime());
                }
            }

            try {
                log.info("Session ES store {}: {}", ecuIndexName, JSONObject.toJSONString(ecuMap));
                esService.bulkAddIndexDoc(ecuIndexName, ecuMap);
                sessionDto.setEcuTotal(ecuMap.size());
            } catch (IOException e) {
                log.error("Session ES store error: {}", JSONObject.toJSONString(e));
            }
        }

        //DID
        Boolean didFlag = utils.exists("t_did");
        if(didFlag){
            List<DidDoc> drs =  utils.executeQuery("select * from t_did order by id asc", new RowMapper<DidDoc>() {
                @Override
                public DidDoc mapRow(ResultSet rs, int index) throws SQLException {
                    DidDoc doc = new DidDoc();
                    doc.setId(String.valueOf(idWorker.nextId()));
                    doc.setSessionId(sessionDto.getSessionId());
                    doc.setSessionInfoId(sessionDto.getSessionInfoId());
                    doc.setTag(tag);
                    doc.setVin(sessionDto.getVinCode());
                    doc.setVehicleTypeId(vehicleTypeIds);
                    doc.setCreatedAt(new Date());
                    try {
                        doc.setRecordFlag(rs.getInt("record_flag"));
                    }catch (Exception ex){
                        log.error("t_did record_flag error: {}", JSONObject.toJSONString(ex));
                        doc.setRecordFlag(RecordFlagEnum.last.getValue());
                    }
                    doc.setEcuName(rs.getString("ecu_name"));
                    doc.setDidHex(rs.getString("did_hex"));
                    doc.setDidName(rs.getString("did_name"));
                    doc.setDidNameTi(rs.getString("did_name_ti"));
                    doc.setParameterName(rs.getString("parameter_name"));
                    doc.setParameterNameTi(rs.getString("parameter_name_ti"));
                    doc.setValue(rs.getString("value"));
                    doc.setValueTi(rs.getString("value_ti"));
                    doc.setNrcDescription(rs.getString("nrc_description"));
                    doc.setNrcHex(rs.getString("nrc_hex"));
                    doc.setNrcDescriptionTi(rs.getString("nrc_description_ti"));
                    doc.setErrorCode(rs.getString("error_code"));
                    doc.setErrorMessage(rs.getString("error_message"));

                    Map<String, Object> suggest = new HashMap<>();
                    suggest.put("input", doc.getDidName());
                    suggest.put("weight", DEFAULT_WEIGHT);
                    doc.setDidNameSuggest(suggest);

                    String createTime = rs.getString("create_time");
                    if(null != createTime){
                        try {
                            doc.setCreateTime(simpleDateFormat.parse(createTime));
                        }catch (ParseException ex){
                            log.error("Session createTime ParseException: {}", ex.getMessage());
                        }
                    }

                    String updateTime = rs.getString("last_update_time");
                    if(null != updateTime){
                        try {
                            doc.setLastUpdateTime(simpleDateFormat.parse(updateTime));
                        }catch (ParseException ex){
                            log.error("Session updateTime ParseException: {}", ex.getMessage());
                        }
                    }
                    return doc;
                }
            });

            if(null !=drs && drs.size()>0){
                String indexName = "";
                List<Map<String, Object>> didMap = new ArrayList<>();
                for (DidDoc didDoc: drs) {
                    if(indexName.equals("")){
                        indexName = didDoc.genIndexName(EsIndexEnum.DID.getValue(),  didDoc.getCreateTime());
                    }
                    Map<String, Object> docMap = BeanUtil.beanToMap(didDoc);
                    didMap.add(docMap);
                }
                try {
                    log.info("Session ES store {}: {}", indexName, JSONObject.toJSONString(didMap));
                    esService.bulkAddIndexDoc(indexName, didMap);
                    sessionDto.setDidTotal(didMap.size());
                } catch (IOException e) {
                    log.error("Session ES store error: {}", e.getMessage());
                }
            }
        }

        //DTC扩展数据 通过vin session_id ecu dtc 去查询关联 dtc extended_data_enable=1时进行关联
        Boolean extendFlag = utils.exists("t_dtc_extended");
        if(extendFlag){

            List<DtcExtendedDoc> extendedDocs =  utils.executeQuery("select * from t_dtc_extended order by id asc", new RowMapper<DtcExtendedDoc>() {
                @Override
                public DtcExtendedDoc mapRow(ResultSet rs, int index) throws SQLException {
                    DtcExtendedDoc doc = new DtcExtendedDoc();
                    doc.setId(String.valueOf(idWorker.nextId()));
                    doc.setSessionId(sessionDto.getSessionId());
                    doc.setSessionInfoId(sessionDto.getSessionInfoId());
                    doc.setTag(tag);
                    doc.setEcuName(rs.getString("ecu_name"));
                    doc.setDtcCode(rs.getString("dtc_code"));
                    doc.setName(rs.getString("name"));
                    doc.setNameTi(rs.getString("name_ti"));
                    doc.setValue(rs.getString("value"));
                    doc.setUnit(rs.getString("unit"));
                    doc.setUnitTi(rs.getString("unit_ti"));
                    doc.setNrcHex(rs.getString("nrc_hex"));
                    doc.setNrcDescription(rs.getString("nrc_description"));
                    doc.setNrcDescriptionTi(rs.getString("nrc_description_ti"));
                    doc.setErrorCode(rs.getString("error_code"));
                    doc.setErrorMessage(rs.getString("error_message"));
                    String createTime = rs.getString("create_time");
                    if(null != createTime){
                        try {
                            doc.setCreateTime(simpleDateFormat.parse(createTime));
                        }catch (ParseException ex){
                            log.error("Session createTime ParseException: {}", ex.getMessage());
                        }
                    }
                    String updateTime = rs.getString("last_update_time");
                    if(null != updateTime){
                        try {
                            doc.setLastUpdateTime(simpleDateFormat.parse(updateTime));
                        }catch (ParseException ex){
                            log.error("Session updateTime ParseException: {}", ex.getMessage());
                        }
                    }
                    return doc;
                }
            });

            if(null !=extendedDocs && extendedDocs.size()>0){
                String indexName = "";
                List<Map<String, Object>> extendMap = new ArrayList<>();
                for (DtcExtendedDoc extendDoc: extendedDocs) {
                    if(indexName.equals("")){
                        indexName = extendDoc.genIndexName(EsIndexEnum.DTC_EXTENDED.getValue(),  extendDoc.getCreateTime());
                    }
                    Map<String, Object> docMap = BeanUtil.beanToMap(extendDoc);
                    extendMap.add(docMap);
                }
                try {
                    log.info("Session ES store {}: {}", indexName, JSONObject.toJSONString(extendMap));
                    esService.bulkAddIndexDoc(indexName, extendMap);
                    sessionDto.setDtcExtendedTotal(extendMap.size());
                } catch (IOException e) {
                    log.error("Session ES store error: {}", e.getMessage());
                }
            }
        }

        //DTC 冻结帧 与扩展数据结构一致 在dtc snapshot_enable=1时进行关联
        Boolean snapshotFlag = utils.exists("t_dtc_snapshot");
        if(snapshotFlag){

            List<DtcSnapshotDoc> snapshotDocs =  utils.executeQuery("select * from t_dtc_snapshot order by id asc", new RowMapper<DtcSnapshotDoc>() {
                @Override
                public DtcSnapshotDoc mapRow(ResultSet rs, int index) throws SQLException {
                    DtcSnapshotDoc doc = new DtcSnapshotDoc();
                    doc.setId(String.valueOf(idWorker.nextId()));
                    doc.setSessionId(sessionDto.getSessionId());
                    doc.setSessionInfoId(sessionDto.getSessionInfoId());
                    doc.setTag(tag);
                    doc.setEcuName(rs.getString("ecu_name"));
                    doc.setDtcCode(rs.getString("dtc_code"));
                    doc.setName(rs.getString("name"));
                    doc.setNameTi(rs.getString("name_ti"));
                    doc.setValue(rs.getString("value"));
                    doc.setUnit(rs.getString("unit"));
                    doc.setUnitTi(rs.getString("unit_ti"));
                    doc.setNrcHex(rs.getString("nrc_hex"));
                    doc.setNrcDescription(rs.getString("nrc_description"));
                    doc.setNrcDescriptionTi(rs.getString("nrc_description_ti"));
                    doc.setErrorCode(rs.getString("error_code"));
                    doc.setErrorMessage(rs.getString("error_message"));
                    String createTime = rs.getString("create_time");
                    if(null != createTime){
                        try {
                            doc.setCreateTime(simpleDateFormat.parse(createTime));
                        }catch (ParseException ex){
                            log.error("Session createTime ParseException: {}", ex.getMessage());
                        }
                    }
                    String updateTime = rs.getString("last_update_time");
                    if(null != updateTime){
                        try {
                            doc.setLastUpdateTime(simpleDateFormat.parse(updateTime));
                        }catch (ParseException ex){
                            log.error("Session updateTime ParseException: {}", ex.getMessage());
                        }
                    }
                    return doc;
                }
            });

            if(null !=snapshotDocs && snapshotDocs.size()>0){
                String indexName = "";
                List<Map<String, Object>> extendMap = new ArrayList<>();
                for (DtcSnapshotDoc snapshotDoc: snapshotDocs) {
                    if(indexName.equals("")){
                        indexName = snapshotDoc.genIndexName(EsIndexEnum.DTC_SNAPSHOT.getValue(),  snapshotDoc.getCreateTime());
                    }
                    Map<String, Object> docMap = BeanUtil.beanToMap(snapshotDoc);
                    extendMap.add(docMap);
                }
                try {
                    log.info("Session ES store {}: {}", indexName, JSONObject.toJSONString(extendMap));
                    esService.bulkAddIndexDoc(indexName, extendMap);
                    sessionDto.setDtcSnapshotTotal(extendMap.size());
                } catch (IOException e) {
                    log.error("Session ES store error: {}", e.getMessage());
                }
            }
        }

        return sessionDto;
    }

    private String getCtsName(String functionCode, String actionCode){
        String ctsName = "";

        if(StrUtil.isNotBlank(functionCode) && StrUtil.isNotBlank(actionCode)){
            ctsName = functionCode+"-"+actionCode;
        }else if(StrUtil.isNotBlank(functionCode)){
            ctsName = functionCode;
        }
        return ctsName;
    }

    private String ctsKey(String tag){
       return String.format(RedisConstant.CTS_DAILY_KEY, tag);
    }

    //source:vin:cts:day
    private String ctsItem(DiagnosticDoc diagDoc){
        String ctsName = getCtsName(diagDoc.getFunctionCode(), diagDoc.getAction());
        return String.format(RedisConstant.CTS_DAILY_ITEM,
                diagDoc.getDiagSource(),
                diagDoc.getVin(),
                ctsName,
                dailyDay(diagDoc.getCreateTime()));
    }

    private String dtcKey(String tag){
        return String.format(RedisConstant.DTC_DAILY_KEY, tag);
    }

    //vin:ecu:dtc:dtc_int:dtc_hex:ti:description:day
    private String dtcItem(DtcDoc dtcDoc){
        return String.format(RedisConstant.DTC_DAILY_ITEM,
                dtcDoc.getVin(),
                dtcDoc.getEcuName(),
                dtcDoc.getDtcCode(),
                dtcDoc.getDtcCodeInt()==null?"":dtcDoc.getDtcCodeInt(),
                dtcDoc.getDtcCodeHex()==null?"":dtcDoc.getDtcCodeHex(),
                dtcDoc.getDescriptionTi()==null?"":dtcDoc.getDescriptionTi(),
                dtcDoc.getDescription()==null?"":dtcDoc.getDescription(),
                dailyDay(dtcDoc.getCreateTime()));
    }

    private String dailyDay(Date day){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return simpleDateFormat.format(day);
    }
}
