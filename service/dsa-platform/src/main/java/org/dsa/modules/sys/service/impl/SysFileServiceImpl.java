package org.dsa.modules.sys.service.impl;

import ch.qos.logback.classic.Logger;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileReader;
import cn.hutool.core.io.file.FileWriter;
import cn.hutool.crypto.SecureUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.util.ListUtils;
import com.alibaba.excel.write.style.column.LongestMatchColumnWidthStyleStrategy;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.serializer.SerializerFeature;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.exception.RRException;
import org.dsa.common.pojo.RowMapper;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.ObjectUtils;
import org.dsa.common.utils.SqliteUtils;
import org.dsa.common.utils.ZipUtil;
import org.dsa.modules.sys.dao.SysLocaleDao;
import org.dsa.modules.sys.dao.SysTranslateDao;
import org.dsa.modules.sys.entity.FileEntity;
import org.dsa.modules.sys.entity.SysTranslateEntity;
import org.dsa.modules.sys.listener.MultiLanguageExcelImportListener;
import org.dsa.modules.sys.listener.UserExcelImportListener;
import org.dsa.modules.sys.listener.VehicleExcelImportListener;
import org.dsa.modules.sys.service.SysFileService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Service("sysFileService")
public class SysFileServiceImpl implements SysFileService {

    private final Logger logger = (Logger) LoggerFactory.getLogger(getClass());

    @Value("${file.base.path}")
    private String filePath;

    @Value("${file.language.path}")
    private String languagePath;

    @Value("${file.language.excel_path}")
    private String languageExcelPath;

    @Value("${file.language.zip_path}")
    private String zipPath;

    @Value("${file.language.sqlite_path}")
    private String sqlitePath;


    @Value("${file.template.path}")
    private String templatePath;


    @Autowired
    SysLocaleDao sysLocaleDao;

    @Autowired
    SysTranslateDao sysTranslateDao;

    /**
     * 文件上传
     *
     * @param file
     * @param req
     * @return
     */
    @Override
    public FileEntity uploadFiles(MultipartFile file, String versionType, HttpServletRequest req) {
        FileEntity fileEntity = new FileEntity();
        File filedir = new File(filePath);
        //如果路径不存在则创建
        if (!filedir.exists())
            filedir.mkdirs();
        //附件名前加时间戳
        String timePrefix = new Date().getTime() + "";
        String filename = timePrefix + Constant.TIME_PREFIX + file.getOriginalFilename();
        File sourcefile = new File(filePath + versionType + Constant.PATH_SPLIT + filename);

        try {
            FileUtils.copyInputStreamToFile(file.getInputStream(), sourcefile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        fileEntity.setRealName(filename);
        fileEntity.setName(file.getOriginalFilename());
        fileEntity.setUrl(filePath + versionType + Constant.PATH_SPLIT + filename);
        return fileEntity;
    }

    @Override
    public FileEntity uploadFile(MultipartFile file, String filePath) {
        FileEntity fileEntity = new FileEntity();
        File fileDir = new File(filePath);
        //如果路径不存在则创建
        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }

        String filename = file.getOriginalFilename();
        File sourceFile = new File(filePath + filename);
        if (sourceFile.exists()) {
            File newFile = new File(filePath + filename);
            //备份原文件
            File oldFile = new File(filePath + filename + ".bak");
            if (oldFile.exists()) {
                //备份文件已存在，将备份文件删除
                oldFile.delete();
            }
            newFile.renameTo(oldFile);
        }
        try {
            FileUtils.copyInputStreamToFile(file.getInputStream(), sourceFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        fileEntity.setRealName(filename);
        fileEntity.setName(file.getOriginalFilename());
        fileEntity.setUrl(filePath + Constant.PATH_SPLIT + filename);
        return fileEntity;
    }

    @Override
    public FileEntity uploadVersionFile(MultipartFile file, String filePath) {
        FileEntity fileEntity = new FileEntity();
        File fileDir = new File(filePath);
        //如果路径不存在则创建
        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }
//        // 版本文件命名
        File sourceFile = new File(filePath + file.getOriginalFilename());
        if (sourceFile.exists()) {
            File newFile = new File(filePath + file.getOriginalFilename());
            //备份原文件
            File oldFile = new File(filePath + file.getOriginalFilename() + ".bak");
            if (oldFile.exists()) {
                //备份文件已存在，将备份文件删除
                FileUtil.del(oldFile);
            }
            newFile.renameTo(oldFile);
        }
        try {
            FileUtils.copyInputStreamToFile(file.getInputStream(), sourceFile);
        } catch (IOException e) {
            logger.error("version-file-upload-error",e);
            throw new RRException("version file upload error!");
        }
        fileEntity.setRealName(file.getOriginalFilename());
        fileEntity.setName(sourceFile.getName());
        fileEntity.setFileSize(FileUtil.size(sourceFile));
        fileEntity.setFileMd5(SecureUtil.md5(sourceFile));
        fileEntity.setUrl(filePath + sourceFile.getName());
        return fileEntity;
    }

    /**
     * 下载文件
     */
    @Override
    public InputStream downloadFiles(String fileName) {

        InputStream i = null;
        try {
            i = new FileInputStream(new File(filePath + fileName));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return i;
    }

    /**
     * 删除文件
     *
     * @param url
     */
    @Override
    public void deletefile(String url) {
        File file = new File(url);// 读取
        if (file.isFile())
            file.delete();// 删除
    }

    /**
     * 多语言导入
     * @param file
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void multiLanguageExcelImport(File file) {
        TimeInterval timer = DateUtil.timer();
        EasyExcel.read(file, new MultiLanguageExcelImportListener(languagePath)).sheet().doRead();
        logger.info("导入数据用时 {}",timer.intervalRestart());
    }

    /**
     * 多语言导出
     * @return
     */
    @Override
    public String multiLanguageExcelExport() {
        Map<String, Object> mapData = getDataToFolder(languagePath);
        List<String> languages = JSONObject.parseObject(JSONObject.toJSONString(mapData.get("languages")),new TypeReference<List<String>>() {});
        List<JSONObject> data = JSONObject.parseObject(JSONObject.toJSONString(mapData.get("dataList")),new TypeReference<List<JSONObject>>() {});
        //导出到指定路径
        File languageExcelFolder = new File(languageExcelPath);
        if (!languageExcelFolder.exists()) {
            FileUtil.mkdir(languageExcelFolder);
        }
        String fileName = languageExcelPath + "languageConfig"+DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN)+".xlsx";
        File file = new File(fileName);
        if (file.exists()) {
           FileUtil.del(file);
        }
        // 这里 需要指定写用哪个class去写，然后写到第一个sheet，名字为模板 然后文件流会自动关闭
        EasyExcel.write(fileName)
                .registerWriteHandler(new LongestMatchColumnWidthStyleStrategy())
                .head(head(languages)).sheet("language_config").doWrite(dataList(data));
        logger.info("url:" + fileName);
        return fileName;
    }

    /**
     * 下载翻译文件包
     * @return
     * @throws FileNotFoundException
     */
    @Override
    public String multiLanguageZip() throws FileNotFoundException {
        //语种set
        Set<String> localeSet = new HashSet<>(16);
        //1.查询本地翻译库文件
        List<String> localeList = sysTranslateDao.listAllLocales();
        List<SysTranslateEntity> localTranslations = sysTranslateDao.listAll();
        localeSet.addAll(localeList);
        //翻译库数据list
        List<SysTranslateEntity> translations = new ArrayList<>(16);
        //2. 获取诊断库，解析数据
//        getDiagDbData(localeSet, translations);
        //3. 整合诊断库与本地库数据生成json，存放到jsonFiles
        integratedDataToJson(localeSet,localTranslations,translations);
        List<File> jsonFiles = Arrays.asList(FileUtil.ls(languagePath));
        //4. 将json文件所在文件夹压缩，输出url
        File folder = new File(zipPath);
        if (!folder.exists()){
            folder.mkdirs();
        }
        String url = zipPath+"language.zip";
        OutputStream outputStream = new FileOutputStream(url);
        ZipUtil.toZip(jsonFiles, outputStream);
        logger.info("生成翻译库zip文件--end");
        return url;
    }

    @Override
    public Map<String,String> downloadVehicleTemplate(String languages) {
        Map<String, String> map = new HashMap<>();
        String path =templatePath+"vehicle_import_template";
        if(StringUtils.isNotEmpty(languages)){
            if(languages.equals("en")){
                path += "_en";
            }
        }
        path += ".xlsx";
        File file = new File(path);
        if(!file.exists()){
            throw new RRException(Constant.Msg.TEMPLATE_FILE_EMPTY);
        }
        map.put("path", path);
        map.put("name", file.getName());
        return map;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void vehicleExcelImport(File file) {
        TimeInterval timer = DateUtil.timer();
        EasyExcel.read(file, new VehicleExcelImportListener()).sheet().doRead();
        logger.info("导入车辆数据用时 {}",timer.intervalRestart());
    }

    @Override
    public Map<String, String> downloadUserTemplate() {
        Map<String, String> map = new HashMap<>();
        String path =templatePath+"user_import_template.xlsx";
        File file = new File(path);
        if(!file.exists()){
            throw new RRException(Constant.Msg.TEMPLATE_FILE_EMPTY);
        }
        map.put("path", path);
        map.put("name", file.getName());
        return map;
    }

    @Override
    public void importUser(File file) {
        TimeInterval timer = DateUtil.timer();
        EasyExcel.read(file, new UserExcelImportListener()).sheet().doRead();
        logger.info("用户导入数据用时 {}",timer.intervalRestart());
    }

    /**
     * 获取诊断库数据
     * @param localeSet
     * @param translations
     */
    private void getDiagDbData(Set<String> localeSet, List<SysTranslateEntity> translations) {
        File dbFolder = new File(sqlitePath);
        Arrays.stream(Objects.requireNonNull(dbFolder.listFiles()))
                .filter(file -> file.getName().endsWith(".db"))
                .forEach(file -> {
                    try {
                        SqliteUtils sqliteUtils = new SqliteUtils(file.getPath());

                        List<String> locales = sqliteUtils.executeQuery("select distinct l.locale as locale from translations t left join locales l on t.locale=l.id", new RowMapper<String>() {
                            @Override
                            public String mapRow(ResultSet rs, int index) throws SQLException {
                                return rs.getString("locale");
                            }
                        });

                        List<SysTranslateEntity> sysTranslateEntityList = sqliteUtils.executeQuery("select t.*,l.locale as localeV  from translations t left join locales l on t.locale =l.id ", new RowMapper<SysTranslateEntity>() {
                            @Override
                            public SysTranslateEntity mapRow(ResultSet rs, int index) throws SQLException {
                                SysTranslateEntity sysTranslateEntity = new SysTranslateEntity();
                                sysTranslateEntity.setKey(rs.getString("id"));
//                                sysTranslateEntity.setLocaleId(rs.getInt("locale"));
                                sysTranslateEntity.setLocale(rs.getString("localeV"));
                                sysTranslateEntity.setText(rs.getString("text"));
                                sysTranslateEntity.setGeometry(rs.getString("geometry"));
                                return sysTranslateEntity;
                            }
                        });
                        localeSet.addAll(locales);
                        translations.addAll(sysTranslateEntityList);
                    } catch (ClassNotFoundException | SQLException e) {
                        logger.error("读取sqlite异常");
                        e.printStackTrace();
                    }
                });
    }

    /**
     * 整合本地翻译库，诊断翻译库生成json到jsonFiles
     * @param localeSet 语种集合
     * @param localTranslations 本地翻译数据
     * @param translations 翻译库数据
     */
    private void integratedDataToJson(Set<String> localeSet, List<SysTranslateEntity> localTranslations, List<SysTranslateEntity> translations) {
        //遍历生成json
        localeSet.forEach(locale -> {
            //?无序
            Map<String, String> map = new TreeMap<>();

            localTranslations.stream()
                    .filter(translation -> Objects.equals(translation.getLocale(), locale))
                    .forEach(translation -> {
                        map.put(translation.getKey(), translation.getText());
                    });
            translations.stream()
                    .filter(translation -> Objects.equals(translation.getLocale(), locale))
                    .forEach(translation -> {
                        map.put(translation.getKey(), translation.getText());
                    });
            String jsonData = JSON.toJSONString(map, SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue,
                    SerializerFeature.WriteDateUseDateFormat);
            String fileName = languagePath + locale + ".json";
            FileWriter fileWriter = new FileWriter(fileName);
            fileWriter.write(jsonData);
        });
    }

    /**
     * 获取指定文件夹下的所有 语种.json文件，提取内容
     * @param folderPath
     * @return
     */
    private Map<String, Object> getDataToFolder(String folderPath) {
        Map<String, Object> result = new HashMap<>(2);
        //语种名称集合
        List<String> languages = new ArrayList<>(16);
        //语种数据集合
        List<JSONObject> dataList = new ArrayList<>(16);
        //读取指定文件夹下的文件
        Arrays.stream(Objects.requireNonNull(new File(folderPath).listFiles()))
                .filter(file -> file.getName().endsWith(".json"))
                .forEach(file -> {
                    FileReader fileReader = new FileReader(file.getAbsolutePath());
                    String dataStr = fileReader.readString().replaceAll("\n\t","");
                    String language = FileUtil.mainName(file);
                    languages.add(language);
                    JSONObject jsonObject = JSONObject.parseObject(dataStr);
                    dataList.add(jsonObject);
                });
        result.put("languages", languages);
        result.put("dataList", dataList);
        return result;
    }

    //自定义表头
    private List<List<String>> head(List<String> languages) {
        List<List<String>> list = ListUtils.newArrayList();
        List<String> head0 = ListUtils.newArrayList();
        head0.add("key");
        List<String> head1 = ListUtils.newArrayList();
        list.add(head0);
        languages.forEach(language -> {
            List<String> languageHead = ListUtils.newArrayList();
            languageHead.add(language);
            list.add(languageHead);
        });
        logger.info("自定义表头为" + JSON.toJSONString(list));
        return list;
    }

    /**
     * 数据转换为导出excel需要的数据结构
     * @param dataList
     * @return
     */
    private List<List<Object>> dataList(List<JSONObject> dataList) {
        List<List<Object>> list = ListUtils.newArrayList();
        //改为一级key
        // key vKey lan1 lan2
        //实际插入值集合
        for (int i = 0; i < dataList.size(); i++) {
            JSONObject languageJson = dataList.get(i);
            //第一次循环languageJson,
            //第二次到n次 循环list,插入vValue
            if (i == 0) {
                //每一行插入的实际值
                List<Object> data = ListUtils.newArrayList();
                languageJson.keySet().forEach(key -> {
                    data.add(key);
                    String value = String.valueOf(languageJson.get(key));;
                    List<Object> dataV = ListUtils.newArrayList();
                    dataV.add(key);
                    dataV.add(value);
                    list.add(dataV);
                });
            } else {
                //第2-N次添加其他语言值到行
                list.forEach(item->{
                    Object obj = languageJson.get(item.size()>0?item.get(0):null);
                    String value = String.valueOf(ObjectUtils.isNotNull(obj)?obj:"");
                    item.add(value);
                });
            }
        }
        return list;
    }
}
