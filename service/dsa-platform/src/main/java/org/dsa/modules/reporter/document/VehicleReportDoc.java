package org.dsa.modules.reporter.document;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class VehicleReportDoc extends ElasticEntity {

     private String sessionInfoId;

     private String sessionId;

     private Integer dtcRecordId;

     private Integer didRecordId;

    private String vin;

    private List<Long> vehicleTypeId;

    private String model;

    private String year;

    private String brand;

    private String workshop;

    private String userId;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

}
