package org.dsa.modules.onlineUpdate.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 在线升级版本操作记录
 */
@Data
@Builder
@AllArgsConstructor
@TableName("o_version_record")
public class VersionRecordEntity {

    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 版本uuid
     */
    private String versionUuid;

    /**
     * 版本状态 0开发
     */
    private Integer status;

    /**
     * 操作
     */
    private String editType;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat
    private Date createdAt;

    /**
     * 创建人id
     */
    private Long createdUserId;

    /**
     * 创建人名称
     */
    private String createdUserName;


}
