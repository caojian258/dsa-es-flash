package org.dsa.modules.onlineUpdate.controller;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.R;
import org.dsa.modules.onlineUpdate.entity.VersionRecordEntity;
import org.dsa.modules.onlineUpdate.service.VersionRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/versionRecord")
public class VersionRecordControl {

    @Autowired
    private VersionRecordService versionRecordService;

    @RequestMapping("/list")
    public R list(@RequestBody Map<String,Object> param){
        try {
            Long versionUuId = MapUtil.getLong(param, "uuid");
            LambdaQueryWrapper<VersionRecordEntity> query = Wrappers.lambdaQuery();
            query.eq(ObjectUtil.isNotNull(versionUuId), VersionRecordEntity::getVersionUuid, versionUuId);
            List<VersionRecordEntity> records = versionRecordService.getBaseMapper().selectList(query);
            return R.ok().put("records", records);
        } catch (Exception e) {
            log.error("record-list-error: {}",e.getMessage());
            e.printStackTrace();
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }


}
