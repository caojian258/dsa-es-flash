package org.dsa.modules.diagnostic.controller;

import org.dsa.common.utils.Constant;
import org.dsa.common.utils.R;
import org.dsa.modules.diagnostic.entity.DiagnosticRecordEntity;
import org.dsa.modules.diagnostic.service.LoggerService;
import org.dsa.modules.sys.controller.AbstractController;
import org.dsa.modules.sys.utils.SpringContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;

/**
 * 日志同步功能
 *
 */
@RestController
@RequestMapping("logger")
public class LoggerController extends AbstractController {
	@Resource
	private LoggerService loggerService;



	/**
	 * 日志同步单条数据
	 */
	@RequestMapping("/record")
	public R record(@RequestBody DiagnosticRecordEntity diagnosticRecordEntity){
		int code;
		//检查FunctionCode Action是否对应
		code = loggerService.checkInputRecord(diagnosticRecordEntity);

		if(code == 0){
			return R.error(code, Constant.Msg.getMessage(code));
		}
		//保存诊断操作日志
		code = loggerService.saveRecord(diagnosticRecordEntity);

		if(code == 0){
			return R.error(code,Constant.Msg.getMessage(code));
		}

		return R.ok();
	}

	/**
	 * 操作记录同步 - 文件上传 - token
	 */
	@RequestMapping("/recordFile")
	public R recordByFile(@RequestParam("file") MultipartFile file,
						  @RequestParam("fileMd5") String fileMd5,
						  HttpServletRequest req){
		try {
			logger.info("文件上传中...");
			if(null!=file && fileMd5 != null){
				logger.info("文件上传中...fileName:{}",file.getOriginalFilename());
				loggerService.saveRecordFile(file,fileMd5,req);
				logger.info("文件上传成功...");
				return R.ok();
			}else {
				return R.error("上传的文件信息不能为空");
			}
		}catch (Exception e){
			logger.error("文件上传失败");
			return R.error(400,e.getMessage());
		}

	}


	/**
	 * 操作记录同步 - 文件上传 - 签名
	 */
	@RequestMapping("/recordFileBySign")
	public R recordFileBySign(@RequestParam("file") MultipartFile file,
							  @RequestParam("fileMd5") String fileMd5,
							  @RequestParam("systemId") String systemId,
							  @RequestParam("requestUser") String requestUser,
							  @RequestParam("requestTime") String requestTime,
							  @RequestParam("sign") String sign){
		logger.info("文件上传中...");
		if(null!=file){
			loggerService.saveRecordFile(file,null,null);
			logger.info("文件上传成功...");
			return R.ok();
		}else {
			return R.error("上传的文件信息不能为空");
		}
	}


	/**
	 * 日志文件 - 文件上传 - token
	 */
	@RequestMapping("/upload")
	public R upload(@RequestParam("file") MultipartFile file, @RequestParam("fileMd5") String md5, HttpServletRequest req){
		logger.info("文件上传中...");
		if(null!=file){
			loggerService.saveRecordFile(file,md5,req);
			logger.info("文件上传成功...");
			return R.ok();
		}else {
			return R.error("上传的文件信息不能为空");
		}
	}


	/**
	 * 日志文件 - 文件上传 - 签名
	 */
	@RequestMapping("/uploadBySign")
	public R uploadBySign(@RequestParam("file") MultipartFile file,
						  @RequestParam("fileMd5") String fileMd5,
						  @RequestParam("systemId") String systemId,
						  @RequestParam("requestUser") String requestUser,
						  @RequestParam("requestTime") String requestTime,
						  @RequestParam("sign") String sign){
		logger.info("文件上传中...");
		if(null!=file){
			loggerService.saveRecordFile(file,null,null);
			logger.info("文件上传成功...");
			return R.ok();
		}else {
			return R.error("上传的文件信息不能为空");
		}
	}


}
