package org.dsa.modules.reporter.vo.client;

import lombok.Data;

import java.io.Serializable;

/***
 * 客户端详情应用安装列表
 */
@Data
public class AppInstallResVo implements Serializable {

    private String name;

    private String version;

    private String installResult;

    private String installTime;

    private String installMsg;
}
