package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.entity.SystemOverview;
import org.dsa.modules.reporter.vo.client.ClientDetailResVo;

import java.util.List;
import java.util.Map;

@Mapper
public interface SystemOverviewMapper extends BaseMapper<SystemOverview> {

    public SystemOverview getOneByPcid(@Param("pcid") String pcid);

    public ClientDetailResVo getDetail(@Param("pcid") String pcid);

    Page<SystemOverview> queryPage(Page<SystemOverview> page, @Param("pcid") String pcid, @Param("workshop") String workshop,
                                   @Param("username") String username,@Param("antivir")  String antivir, @Param("systemLocale") String systemLocale,
                                   @Param("applicationLocale")  String applicationLocale, @Param("windowsVersion") String windowsVersion, @Param("flag") Boolean flag,
                                   @Param("group") List<String> group);

    List<String> getAllAntivir();
    List<String> getAllSystemLocale();
    List<String> getAllApplicationLocale();
    List<String> getAllWindowsVersions();

    List<Map<String, Object>> staticClientByWorkshop(@Param("workshopIds") List<Long> workshopIds, @Param("limit") Integer limit);


    List<Map<String, Object>> staticClientAppLocaleByWorkshop(@Param("workshopIds") List<Long> workshopIds, @Param("limit") Integer limit);


    List<Map<String, Object>> staticClientSystemLocaleByWorkshop(@Param("workshopIds") List<Long> workshopIds, @Param("limit") Integer limit);
}
