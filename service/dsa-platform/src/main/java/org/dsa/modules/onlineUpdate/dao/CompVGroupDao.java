package org.dsa.modules.onlineUpdate.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.onlineUpdate.entity.CompVGroupEntity;
import org.dsa.modules.onlineUpdate.vo.CompVGroupVo;

import java.util.List;

/**
 * ${comments}
 * 
 */
@Mapper
public interface CompVGroupDao extends BaseMapper<CompVGroupEntity> {

    List<CompVGroupEntity> selectGroupByCVersionId(@Param("cVersionId") Long cVersionId);
    List<CompVGroupVo> selectByVersionId(@Param("id") Long id);
    List<CompVGroupVo> disabledOption(@Param("componentId") Long componentId, @Param("groupId") String groupId);

    boolean hasGroup(@Param("id") Long id);

    boolean isPublished(@Param("id") Long id);

}
