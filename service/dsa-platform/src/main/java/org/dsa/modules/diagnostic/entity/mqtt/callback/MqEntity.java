package org.dsa.modules.diagnostic.entity.mqtt.callback;

import lombok.Data;
import org.dsa.modules.diagnostic.entity.mqtt.callback.MqHeaderEntity;

import java.io.Serializable;

/**
 * mq 消息 实体类
 * 
 */
@Data
public class MqEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * mqHeader
	 */
	private MqHeaderEntity header;
	/**
	 * mqBody
	 */
	private MqBodyEntity body;

}
