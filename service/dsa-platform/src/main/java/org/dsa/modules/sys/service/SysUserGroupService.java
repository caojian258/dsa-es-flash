package org.dsa.modules.sys.service;

import java.util.List;
import java.util.Map;

import org.dsa.common.utils.PageUtils;
import org.dsa.modules.sys.entity.SysUserGroupEntity;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 用户与组对应关系
 *
 */
public interface SysUserGroupService extends IService<SysUserGroupEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
    void saveOrUpdate(Long userId, List<Long> groupIdList);
    
    /**
	 * 根据用户ID，获取角色ID列表
	 */
	List<Long> queryGroupIdList(Long userId);
}

