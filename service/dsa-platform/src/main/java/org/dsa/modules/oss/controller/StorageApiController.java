package org.dsa.modules.oss.controller;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.R;
import org.dsa.modules.oss.config.StorageCategoryConfig;
import org.dsa.modules.oss.constant.StorageConstant;
import org.dsa.modules.oss.dto.FileDto;
import org.dsa.modules.oss.service.DiskStorageFactory;
import org.dsa.modules.oss.service.DiskStorageService;
import org.dsa.modules.oss.service.FileService;
import org.dsa.modules.sys.service.SysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.*;

@Slf4j
@RestController
@RequestMapping("storage")
public class StorageApiController {

    @Autowired
    private FileService fileService;

    @Autowired
    private SysConfigService configService;

    private Long fileSize = 0L;

    @RequestMapping("/upload")
    public R formUpload(@RequestParam("file") MultipartFile file, @RequestParam("category") String category, @RequestParam("md5") String md5) throws IOException {
        if (file.isEmpty()) {
            throw new RRException("上传文件不能为空");
        }

        if(StrUtil.isBlank(category)){
            throw new RRException("文件类型不能为空");
        }

        if(StrUtil.isBlank(md5)){
            throw new RRException("文件md5不能为空");
        }

        /***
         * 根据业务分类 找到对应的上传目录
         * "session": "/upload/session",
         * "online_update": "/upload/online_update",
         * "flash": "/upload/flash"
         */
        StorageCategoryConfig config = configService.getConfigObject(StorageConstant.LOCAL_CATEGORY_KEY, StorageCategoryConfig.class);

        if(null != config &&  null != config.getWorkDirs() &&
                !config.getWorkDirs().containsKey(category)){
            return R.error("文件类型不能正确");
        }

        DiskStorageService storageService = DiskStorageFactory.build();
        //先上传到默认的上传目录
        String path = storageService.buildPath(file.getOriginalFilename());
        FileDto fileDto = storageService.upload(file, path);
        if(null == fileDto){
            return R.error("上传失败");
        }

        //这里不同的类型的文件有不能的上传目录
        fileDto.setCategory(category);
        log.info("storage upload: {}", JSONObject.toJSONString(fileDto));

        if(md5 != null && fileDto.getFileMd5() !=null
                && !md5.equals(fileDto.getFileMd5())){
            return R.error("md5校验失败");
        }

        //根据每种业务配置构建上传目录
        String workDir = config.getWorkDirs().get(category);
        if(StrUtil.isNotBlank(workDir)){
            storageService.setWorkPath(workDir);
        }

        if(null != storageService.getWorkPath()){
            String targetPath = storageService.buildWorkPath(file.getOriginalFilename());
            Files.copy(Paths.get(fileDto.getUrl()), Paths.get(targetPath));
        }

        if(null != storageService.getConfig() &&
                null != storageService.getConfig().getHasRecord() && storageService.getConfig().getHasRecord()){
            fileService.record(fileDto);
        }

        return R.ok();
    }

    @RequestMapping("/chunkUpload")
    public R chunkUpload(@RequestParam("file") MultipartFile file, @RequestParam("category") String category,
                         @RequestParam("chunks") Integer chunks,
                         @RequestParam("chunk") Integer chunk,
                         @RequestParam("name") String name) throws IOException {

        if (file.isEmpty()) {
            throw new RRException("上传文件不能为空");
        }

        if(StrUtil.isBlank(category)){
            throw new RRException("文件类型不能为空");
        }

        StorageCategoryConfig config = configService.getConfigObject(StorageConstant.LOCAL_CATEGORY_KEY, StorageCategoryConfig.class);

        if(null != config &&  null != config.getWorkDirs() &&
                !config.getWorkDirs().containsKey(category)){
            return R.error("文件类型不能正确");
        }

        DiskStorageService storageService = DiskStorageFactory.build();

        log.info("file name: {}, size: {}, chunk: {}, chunks: {}", file.getOriginalFilename(), file.getSize(), chunk, chunks);

        String locatePath = storageService.buildPath(name);
        Path mergePath = Paths.get(locatePath);
        Files.write(mergePath, file.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        this.fileSize += file.getSize();
        //最后一次
        if(chunk == chunks){
            FileDto fileDto = new FileDto();
            fileDto.setFileSize(this.fileSize);
            fileDto.setName(name);
            fileDto.setCategory(category);
            fileDto.setUrl(locatePath);
            try {
                File targetFile = new File(locatePath);
                String md5Hex = DigestUtils.md5Hex(new FileInputStream(targetFile));
                fileDto.setFileMd5(md5Hex);
            }catch (Exception ex){
                log.error("error: {}", ex.getMessage());
            }

            String workDir = config.getWorkDirs().get(category);
            if(StrUtil.isNotBlank(workDir)){
                storageService.setWorkPath(workDir);
            }

            if(null != storageService.getWorkPath()){
                String targetPath = storageService.buildWorkPath(fileDto.getName());
                Files.copy(Paths.get( fileDto.getUrl()), Paths.get(targetPath));
            }

            if(null != storageService.getConfig() &&
                    null != storageService.getConfig().getHasRecord() && storageService.getConfig().getHasRecord()){
                fileService.record(fileDto);
            }
        }


        return R.ok();
    }

}
