package org.dsa.config;

import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.dsa.modules.sys.oauth2.OAuth2Filter;
import org.dsa.modules.sys.oauth2.OAuth2Realm;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import jakarta.servlet.Filter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Shiro配置
 */
@Configuration
public class ShiroConfig implements ApplicationContextAware {

    private static ApplicationContext context = null;

    @Bean("securityManager")
    public SecurityManager securityManager(OAuth2Realm oAuth2Realm) {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(oAuth2Realm);
        securityManager.setRememberMeManager(null);
        return securityManager;
    }

    @Bean("shiroFilter")
    public ShiroFilterFactoryBean shirFilter(SecurityManager securityManager) {
        ShiroFilterFactoryBean shiroFilter = new ShiroFilterFactoryBean();
        shiroFilter.setSecurityManager(securityManager);

        //oauth过滤
        Map<String, Filter> filters = new HashMap<>();
        filters.put("oauth2", new OAuth2Filter());
        shiroFilter.setFilters(filters);

        Map<String, String> filterMap = new LinkedHashMap<>();

        filterMap.put("/sys/workshop/*", "anon");

        // 远程诊断
        filterMap.put("/diagnosticApi/**", "anon");
//        filterMap.put("/diagnosticTask/**", "anon");
        //登陆
        filterMap.put("/sys/login", "anon");
        // 注册
        filterMap.put("/register", "anon");
        // checkToken
        filterMap.put("/sys/checkToken", "anon");
        filterMap.put("/sys/locale/**", "anon");
        filterMap.put("/login", "anon");
        filterMap.put("/captcha.jpg", "anon");
        filterMap.put("/druid/**", "anon");

        //tsp车辆信息同步
        filterMap.put("/syn/**", "anon");
//        filterMap.put("/api/**", "anon");

        //离线诊断仪签名校验
        filterMap.put("/logger/recordFileBySign", "anon");
        filterMap.put("/logger/uploadBySign", "anon");
        filterMap.put("/version/compareVersionFileBySign", "anon");
        filterMap.put("/version/downloadByTraceId/**", "anon");
        filterMap.put("/vehicle/getVehicleInfoBySign/**", "anon");
        filterMap.put("/oem/cert/**", "anon");
        if("dev".equals(context.getEnvironment().getActiveProfiles()[0])){
            filterMap.put("/swagger/**", "anon");
            filterMap.put("/v2/api-docs", "anon");
            filterMap.put("/swagger-resources/**", "anon");
            filterMap.put("/swagger-ui.html", "anon");
            filterMap.put("/vehicle/getVehicleInfo/**", "anon");
            filterMap.put("/version/compareVersionFile/**", "anon");
            filterMap.put("/version/download/**", "anon");
            filterMap.put("/logger/recordFile", "anon");
            filterMap.put("/logger/upload", "anon");
            filterMap.put("/diagnostic/**", "anon");
            filterMap.put("/monitor/**", "anon");
            filterMap.put("/druid/**", "anon");
            filterMap.put("/version/**", "anon");
        }

        filterMap.put("/**", "oauth2");

        shiroFilter.setFilterChainDefinitionMap(filterMap);
        return shiroFilter;
    }


    @Bean("lifecycleBeanPostProcessor")
    public LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
        return new LifecycleBeanPostProcessor();
    }

    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager) {
        AuthorizationAttributeSourceAdvisor advisor = new AuthorizationAttributeSourceAdvisor();
        advisor.setSecurityManager(securityManager);
        return advisor;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }
}
