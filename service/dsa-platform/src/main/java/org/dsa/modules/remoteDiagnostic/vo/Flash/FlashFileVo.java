package org.dsa.modules.remoteDiagnostic.vo.Flash;

import lombok.Data;

import java.io.Serializable;

@Data
public class FlashFileVo implements Serializable {

    private String flashType;

    private String fileName;

    private String fileMd5;

    //0增量 1全量
    private Integer status;
}
