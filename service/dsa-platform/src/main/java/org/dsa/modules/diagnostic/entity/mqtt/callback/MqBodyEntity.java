package org.dsa.modules.diagnostic.entity.mqtt.callback;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.io.Serializable;

/**
 * mq body 实体类
 * 
 */
@Data
public class MqBodyEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * ECU名称数组
	 */
	private String[] ecuName;
	/**
	 * 返回结果
	 */
	private Object data =new Object();
	/**
	 * 返回错误码
	 */
	private int status = -1;
	/**
	 * 返回信息，用于界面提示，主要用于失败场景
	 */
	private String message = "";

}
