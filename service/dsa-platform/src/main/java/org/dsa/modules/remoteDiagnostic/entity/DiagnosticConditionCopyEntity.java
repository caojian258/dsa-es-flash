package org.dsa.modules.remoteDiagnostic.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.ToString;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.common.validator.group.UpdateGroup;
import org.dsa.modules.remoteDiagnostic.pojo.BaseEntity;

import java.io.Serializable;

@Data
@ToString
@TableName("r_task_condition")
public class DiagnosticConditionCopyEntity extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long rawId;

    private Long taskId;

    @NotBlank(message="条件名称不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String conditionName;

    private String conditionCode;

    private Integer conditionVal;

    private String unity;

    private String minVal;

    private String maxVal;

    private String eqVal;

    private String inVal;

    @TableField(exist = false)
    private Long copyId;

}
