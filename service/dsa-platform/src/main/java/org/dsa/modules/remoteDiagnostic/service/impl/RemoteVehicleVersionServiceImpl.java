package org.dsa.modules.remoteDiagnostic.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.utils.RedisUtils;
import org.dsa.common.utils.ShiroUtils;
import org.dsa.modules.remoteDiagnostic.constant.RemoteDiagnosticConstant;
import org.dsa.modules.remoteDiagnostic.dao.*;
import org.dsa.modules.remoteDiagnostic.dto.ECUDependenceDto;
import org.dsa.modules.remoteDiagnostic.dto.ECUDependenceGroupDto;
import org.dsa.modules.remoteDiagnostic.entity.*;
import org.dsa.modules.remoteDiagnostic.enums.RemoteVersionStatusEnum;
import org.dsa.modules.remoteDiagnostic.enums.RemoteVersionTypeEnum;
import org.dsa.modules.remoteDiagnostic.service.RemoteVehiclePoolService;
import org.dsa.modules.remoteDiagnostic.service.RemoteVehicleVersionService;
import org.dsa.modules.remoteDiagnostic.vo.CheckNameReqVo;
import org.dsa.modules.remoteDiagnostic.vo.Vehicle.VehicleVersionIdentificationReqVo;
import org.dsa.modules.vehicle.dao.DVehicleTypeDao;
import org.dsa.modules.vehicle.entity.DVehicleTypeEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SessionCallback;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jakarta.annotation.Resource;

import java.util.*;

@Slf4j
@Service
public class RemoteVehicleVersionServiceImpl implements RemoteVehicleVersionService {

    @Autowired
    RemoteVehicleVersionDao vehicleVersionDao;
    @Autowired
    RemoteEcuIdentificationInfoDao identificationInfoDao;
    @Resource
    private DVehicleTypeDao dVehicleTypeDao;
    @Resource
    private RemoteVehicleVersionEcuGroupDao vehicleVersionEcuGroupDao;
    @Autowired
    RemotePoolDependDao dependDao;
    @Autowired
    RemoteVersionRecordDao versionRecordDao;
    @Autowired
    RedisTemplate<String, Object> redisTemplate;
    @Autowired
    RemoteVehiclePoolService poolService;
    @Autowired
    RedisUtils redisUtils;
    @Autowired
    RemoteVersionEcuRelyDao relyDao;

    @Override
    public RemoteVehicleVersionEntity getInfo(Long id) {
        return vehicleVersionDao.selectById(id);
    }

    @Override
    @Transactional
    public void saveVersion(RemoteVehicleVersionEntity version) {

        vehicleVersionDao.insert(version);

        if (version.getEcuGroups() != null && !version.getEcuGroups().isEmpty()) {
            vehicleVersionEcuGroupDao.inserts(version.getId(), version.getEcuGroups());
        }

        if (version.getEcuDependencyGroup() != null && !version.getEcuDependencyGroup().isEmpty()) {

            for (ECUDependenceGroupDto ecuDependenceGroupDto : version.getEcuDependencyGroup()) {
                List<RemoteVersionEcuRelyEntity> list = new ArrayList<>();
                for (ECUDependenceDto ecuDependenceDto : ecuDependenceGroupDto.getDependency()) {
                    RemoteVersionEcuRelyEntity build = RemoteVersionEcuRelyEntity.builder().groupId(ecuDependenceGroupDto.getGroupId()).
                            ecuId(ecuDependenceDto.getEcuId()).
                            ecuGroupId(ecuDependenceDto.getGroupId()).
                            ecuVersionId(ecuDependenceDto.getEcuVersionId()).name(ecuDependenceGroupDto.getName()).build();
                    list.add(build);
                }
                relyDao.inserts(version.getId(), list);
            }
        }

        if (version.getPoolId() != null && version.getPool() != null && version.getId() != null) {
            log.info("车辆版本 ----->>>>>>  开始更新车辆池");
            poolService.updatePool(version.getPool());
        }
        saveRecord(version, RemoteVersionTypeEnum.ADD);
    }

    @Override
    @Transactional
    public void updateVersion(RemoteVehicleVersionEntity version) {

        LambdaUpdateWrapper<RemoteVehicleVersionEntity> qw = Wrappers.<RemoteVehicleVersionEntity>lambdaUpdate();

        version.setUpdatedAt(new Date());

        if (version.getPoolId() == null && version.getPool() == null) {
            version.setPoolId(null);
            qw.set(RemoteVehicleVersionEntity::getPoolId, null);
        }
        qw.eq(RemoteVehicleVersionEntity::getId, version.getId());

        vehicleVersionDao.update(version, qw);

        vehicleVersionEcuGroupDao.delete(Wrappers.<RemoteVehicleVersionEcuGroupEntity>lambdaUpdate().eq(RemoteVehicleVersionEcuGroupEntity::getVersionId, version.getId()));

        if (version.getEcuGroups() != null && !version.getEcuGroups().isEmpty()) {
            vehicleVersionEcuGroupDao.inserts(version.getId(), version.getEcuGroups());
        }

        relyDao.delete(Wrappers.<RemoteVersionEcuRelyEntity>lambdaUpdate().eq(RemoteVersionEcuRelyEntity::getVehicleVersionId, version.getId()));
        if (version.getEcuDependencyGroup() != null && !version.getEcuDependencyGroup().isEmpty()) {

            for (ECUDependenceGroupDto ecuDependenceGroupDto : version.getEcuDependencyGroup()) {
                List<RemoteVersionEcuRelyEntity> list = new ArrayList<>();
                for (ECUDependenceDto ecuDependenceDto : ecuDependenceGroupDto.getDependency()) {
                    RemoteVersionEcuRelyEntity build = RemoteVersionEcuRelyEntity.builder().groupId(ecuDependenceGroupDto.getGroupId()).
                            ecuId(ecuDependenceDto.getEcuId()).
                            ecuGroupId(ecuDependenceDto.getGroupId()).
                            ecuVersionId(ecuDependenceDto.getEcuVersionId()).name(ecuDependenceGroupDto.getName()).build();
                    list.add(build);
                }
                relyDao.inserts(version.getId(), list);
            }
        }

        if (version.getPoolId() != null && version.getPool() != null && version.getId() != null) {
            log.info("车辆版本 ----->>>>>>  开始更新车辆池");
            poolService.updatePool(version.getPool());
        }

        saveRecord(version, RemoteVersionTypeEnum.EDIT);

        if (version.getStatus() != 3) {
            List<String> poolKeys = dependDao.selectList(Wrappers.<RemotePoolDependEntity>lambdaQuery().eq(RemotePoolDependEntity::getVehicleVersionId, version.getId())).stream().map(e -> {
                return RemoteDiagnosticConstant.VEHICLE_POOL_ITEM_KEY + e.getPoolId();
            }).toList();
            if (!poolKeys.isEmpty()) {
                // 有被车辆池使用，且状态为不可用
                redisTemplate.opsForValue().set((RemoteDiagnosticConstant.VEHICLE_POOL_LOCK), System.currentTimeMillis() + "");

                redisTemplate.execute(new SessionCallback<Object>() {
                    @Override
                    public <K, V> Object execute(RedisOperations<K, V> operations) throws DataAccessException {
                        operations.multi(); // 开启事务
                        poolKeys.forEach(e -> redisTemplate.opsForHash().delete(RemoteDiagnosticConstant.VEHICLE_POOL_KEY, e));
                        return operations.exec();  //提交
                    }
                });
            }
        }

    }

    private void saveRecord(RemoteVehicleVersionEntity version, RemoteVersionTypeEnum edit) {
        RemoteVersionRecordEntity versionRecord = new RemoteVersionRecordEntity();
        versionRecord.setType(RemoteVersionTypeEnum.VehicleVersion.getValue());
        versionRecord.setValueId(version.getId());
        versionRecord.setEditType(edit.getValue());
        versionRecord.setStatus(version.getStatus());
        versionRecord.setCreateBy(ShiroUtils.getUserId());
        versionRecord.setCreateUserName(ShiroUtils.getUserName());
        versionRecordDao.insert(versionRecord);
    }

    @Override
    public List<RemoteVehicleVersionEntity> getVersions() {
        return null;
    }

    @Override
    public List<RemoteVehicleVersionEntity> getVersions(Long typeId) {

        return vehicleVersionDao.getListByType2(typeId);
//        return vehicleVersionDao.selectList(Wrappers.<RemoteVehicleVersionEntity>lambdaQuery().eq(RemoteVehicleVersionEntity::getTypeId, typeId).orderByAsc(RemoteVehicleVersionEntity::getVersionNumber));
    }

    @Override
    public List<RemoteVehicleVersionEntity> getVersions2(Long typeId) {

        return vehicleVersionDao.getListByType3(typeId);
//        return vehicleVersionDao.selectList(Wrappers.<RemoteVehicleVersionEntity>lambdaQuery().eq(RemoteVehicleVersionEntity::getTypeId, typeId).orderByAsc(RemoteVehicleVersionEntity::getVersionNumber));
    }

    @Override
    public Boolean check(CheckNameReqVo vo) {
        if ("version".equals(vo.getType())) {
            LambdaQueryWrapper<RemoteVehicleVersionEntity> qw = Wrappers.<RemoteVehicleVersionEntity>lambdaQuery();
            qw.eq(RemoteVehicleVersionEntity::getVersionName, vo.getName());
            qw.select(RemoteVehicleVersionEntity::getId);
            if (vo.getId() != null) {
                qw.ne(RemoteVehicleVersionEntity::getId, vo.getId());
            }
            return vehicleVersionDao.selectOne(qw) == null;

        }
        return true;
    }

    @Override
    public DVehicleTypeEntity getVehicleTypeInfo(Long typeId) {
        return dVehicleTypeDao.getTypeAndEcuGroups(typeId);
    }

    @Override
    public Object getVersionListByType(List<Long> ids) {
        return null;
    }

    @Override
    public RemoteVehicleVersionEntity getVehicleVersionInfo(Long id) {

        return vehicleVersionDao.getInfo(id);
    }

    @Override
    public List<RemoteVehicleVersionEntity> getAllVehicleVersion(Integer status) {
//        LambdaQueryWrapper<RemoteVehicleVersionEntity> qw = Wrappers.<RemoteVehicleVersionEntity>lambdaQuery();
//        if (status != null)
//            qw.eq(RemoteVehicleVersionEntity::getStatus, status);
//        else
//            qw.eq(RemoteVehicleVersionEntity::getStatus, 0);
//        return vehicleVersionDao.selectList(qw);
        if (status == null) {
            status = 3;
        }
        return vehicleVersionDao.getAllList(status);
    }

    @Override
    @Transactional
    public void copyVersionById(Long id, Long type) {
        RemoteVehicleVersionEntity version = vehicleVersionDao.selectById(id);

        RemoteVehicleVersionEntity lastVersion = vehicleVersionDao.selectOne(Wrappers.<RemoteVehicleVersionEntity>lambdaQuery().eq(RemoteVehicleVersionEntity::getTypeId, version.getTypeId()).orderByDesc(RemoteVehicleVersionEntity::getVersionNumber).last(" limit 1"));

        version.setId(null);
        version.setCreatedAt(null);
        version.setUpdatedAt(null);
        version.setTypeId(type);
        version.setVersionName(version.getVersionName() + " copy");
        version.setStatus(RemoteVersionStatusEnum.DEV.getValue());
        version.setVersionNumber(lastVersion.getId());

        vehicleVersionDao.insert(version);

        // 复制车辆版本关联的ECU版本
        List<RemoteVehicleVersionEcuGroupEntity> remoteVehicleVersionEcuGroupEntities = vehicleVersionEcuGroupDao.selectList(Wrappers.<RemoteVehicleVersionEcuGroupEntity>lambdaQuery().eq(RemoteVehicleVersionEcuGroupEntity::getVersionId, id));
        if (remoteVehicleVersionEcuGroupEntities != null && !remoteVehicleVersionEcuGroupEntities.isEmpty()) {
            vehicleVersionEcuGroupDao.inserts2(version.getId(), remoteVehicleVersionEcuGroupEntities);
        }

        // 复制车辆版本关联的ECU版本
        List<RemoteVersionEcuRelyEntity> list = relyDao.selectList(Wrappers.<RemoteVersionEcuRelyEntity>lambdaQuery().eq(RemoteVersionEcuRelyEntity::getVehicleVersionId, id));
        if (list != null && !list.isEmpty()) {
            HashMap<String, String> keyMap = new HashMap<>();
            for (RemoteVersionEcuRelyEntity remoteVersionEcuRelyEntity : list) {
                if (keyMap.containsKey(remoteVersionEcuRelyEntity.getGroupId())) {
                    remoteVersionEcuRelyEntity.setGroupId(keyMap.get(remoteVersionEcuRelyEntity.getGroupId()));
                } else {
                    String key = UUID.randomUUID().toString().replaceAll("-", "");
                    keyMap.put(remoteVersionEcuRelyEntity.getGroupId(), key);
                    remoteVersionEcuRelyEntity.setGroupId(key);
                }
            }
            relyDao.inserts(version.getId(), list);
        }

        this.saveRecord(version, RemoteVersionTypeEnum.ADD);

    }

    @Override
    public Map<String, Object> getIdentificationByVersionAndEcu(VehicleVersionIdentificationReqVo vo) {
        HashMap<String, Object> map = new HashMap<>();

        map.put("target", identificationInfoDao.getIdentificationByVersionAndEcu(RemoteVersionTypeEnum.TARGET.getValue(), vo.getEcuVersionId(), vo.getVersionId()));
        map.put("expect", identificationInfoDao.getIdentificationByVersionAndEcu(RemoteVersionTypeEnum.EXPECT.getValue(), vo.getEcuVersionId(), vo.getVersionId()));

        return map;
    }
}
