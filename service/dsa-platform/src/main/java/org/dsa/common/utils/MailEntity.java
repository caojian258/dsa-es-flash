package org.dsa.common.utils;

import lombok.Data;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
public class MailEntity{
	//标题
    private String title;
    //内容
    private String content;
    //发送人邮件地址
    private String from;
    //接收人邮件地址
    private String email;
    //邮件主题
    private String subject;
    //发送时间
    private Date sentDate;
    //活动名称
    private String activityName;
    //url
    private String approveUrl;
    //多个收件人
    private List<String> to;
    //多个CC
    private List<String> ccTo;
    //附件
    private Map<String, Object> attachments;
    //模板路径
    private String template;
}
