package org.dsa.modules.reporter.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.reporter.dao.WorkshopUserGroupMapper;
import org.dsa.modules.reporter.dao.WorkshopUserGroupsMapper;
import org.dsa.modules.reporter.dao.WorkshopUserMapper;
import org.dsa.modules.reporter.dto.ChartItemDto;
import org.dsa.modules.reporter.dto.WorkshopUserGroupTotalDto;
import org.dsa.modules.reporter.dto.WorkshopUserTotalDto;
import org.dsa.modules.reporter.entity.WorkshopUser;
import org.dsa.modules.reporter.entity.WorkshopUserGroup;
import org.dsa.modules.reporter.service.WorkshopUserService;
import org.dsa.modules.reporter.vo.client.ClientChartResVo;
import org.dsa.modules.reporter.vo.workshop.WorkshopUserPageReVo;
import org.dsa.modules.reporter.vo.workshopUser.ActiveUserVo;
import org.dsa.modules.reporter.vo.workshopUser.UserChartReVo;
import org.dsa.modules.reporter.vo.workshopUser.UserGroupChartReVo;
import org.dsa.modules.sys.dao.SysUserTokenDao;
import org.dsa.modules.sys.dto.SysActiveUserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class WorkshopUserServiceImpl implements WorkshopUserService {

    @Autowired
    WorkshopUserMapper workshopUserMapper;

    @Autowired
    WorkshopUserGroupMapper userGroupMapper;

    @Autowired
    WorkshopUserGroupsMapper userGroupsMapper;


    @Autowired
    SysUserTokenDao sysUserTokenDao;

    @Override
    public Page<WorkshopUser> selectPage(WorkshopUserPageReVo vo) {

//        Page<WorkshopUser> page = new Page<>(vo.getPageNo(), vo.getPageSize());
//
//        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
//        OrderItem oi = new OrderItem(column, vo.getSortAsc());
//        page.addOrder(oi);
//        LambdaQueryWrapper<WorkshopUser> qw = Wrappers.<WorkshopUser>lambdaQuery();
//
//        if(StrUtil.isNotBlank(vo.getName())){
////            qw.like(WorkshopUser::getUsername, vo.getName().trim());
//            qw.apply("username"+" ilike {0}", "%"+vo.getName().trim()+"%");
//        }
//
//        if(StrUtil.isNotBlank(vo.getWorkshop())){
////            qw.like(WorkshopUser::getWorkshopId, vo.getWorkshop().trim());
//            qw.apply("workshop_id"+" ilike {0}", "%"+vo.getWorkshop().trim()+"%");
//        }
//        if(null != vo.getActive()){
//            qw.eq(WorkshopUser::getActive, vo.getActive());
//        }
//        Page<WorkshopUser> resultPage = workshopUserMapper.selectPage(page, qw);
//        return resultPage;

        Page<WorkshopUser> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);

        Boolean flag = false;

        if (vo.getGroup()!= null && vo.getGroup().contains("unknown")){
            flag = true;
        }

        Page<WorkshopUser> resultPage = workshopUserMapper.queryUserPage(page, vo.getName(),vo.getGroup(),vo.getActive(),vo.getWorkshop(),flag);
        return resultPage;
    }

    @Override
    public List<WorkshopUserGroup> getGroup() {
        return userGroupMapper.selectList(null);
    }

    @Override
    public ClientChartResVo userGroups(UserGroupChartReVo vo) {
        List<String> condition = vo.getCondition() == null  ? null : (ArrayList<String>) vo.getCondition().get("workshopGroup");
        if (condition != null && condition.size() == 0) {
            condition = null;
        }
        List<WorkshopUserGroupTotalDto> items = userGroupsMapper.userGroupTotal(vo.getStartDate(), vo.getTop(),condition);
        Set<String> legendData = new HashSet<>();
        Map<String,Map<String, Object>> subBurstStore = new HashMap<>();
        Map<String,Long> stat = new HashMap<>();
        List<ChartItemDto> dtoList = new ArrayList<>();
        ClientChartResVo resVo = new ClientChartResVo();
        Long total = (long) 0;

        for (WorkshopUserGroupTotalDto dto: items){
            legendData.add(dto.getParentGroupName());
            if(null != stat.get(dto.getParentGroupName())){
                stat.put(dto.getParentGroupName(), Long.valueOf(dto.getValue()) + stat.get(dto.getParentGroupName()));
            }else{
                stat.put(dto.getParentGroupName(), Long.valueOf(dto.getValue()));
            }

            Map<String, Object> map = null;
            map = subBurstStore.get(dto.getParentGroupName());
            if(null == map){
                map = new HashMap<>();
            }
            map.put(dto.getGroupName(), dto.getValue());
            subBurstStore.put(dto.getParentGroupName(), map);
        }

        Iterator<Map.Entry<String, Map<String, Object>>> entries = subBurstStore.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry<String, Map<String, Object>> entry = entries.next();
            String key = entry.getKey();
            Map<String, Object> value = entry.getValue();
            ChartItemDto dto = new ChartItemDto();
            dto.setName(key);
            dto.setValue(stat.get(key));
            List<Map<String,Object>> children = new ArrayList<>();

            for (String k: value.keySet()){
                Map<String,Object> m = new HashMap<>();
                m.put("name", k);
                m.put("value", value.get(k));
                total += Long.parseLong(value.get(k).toString());
                children.add(m);
            }
            dto.setChildren(children);
            dtoList.add(dto);
        }
        resVo.setItemTotal(total);
        resVo.setSeriesData(dtoList);
        resVo.setLegendData(legendData);
        return resVo;
    }

    @Override
    public ClientChartResVo workshopGroup(UserChartReVo vo) {

        List<WorkshopUserTotalDto> userList =  workshopUserMapper.workshopUserTotal(vo.getWorkshopGroup(), vo.getWorkshopId(), vo.getWorkshopName(), vo.getStartDate(), vo.getTop());

        ClientChartResVo resVo = new ClientChartResVo();
        Set<String> legendData = new HashSet<>();
        List<ChartItemDto> dtoList = new ArrayList<>();
        for (WorkshopUserTotalDto item: userList){
            ChartItemDto dto = new ChartItemDto();
            String name = item.getWorkshopName();
            legendData.add(name);
            dto.setName(name);
            dto.setValue(item.getValue());
            dtoList.add(dto);
        }
        resVo.setLegendData(legendData);
        resVo.setSeriesData(dtoList);
        return resVo;
    }

    @Override
    public void updateLoginTime(String username, Date loginTime) {
        workshopUserMapper.updateLoginTime(username, loginTime);
    }

    @Override
    public Page<SysActiveUserDto> activeUserPage(ActiveUserVo vo) {
        String workshopName = vo.getWorkshopName();
        String userName = vo.getUserName();

        Page<SysActiveUserDto> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        String column = "update_time";
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);

        return sysUserTokenDao.activeUserPage(page, workshopName, userName);
    }
}
