package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.dsa.modules.reporter.entity.WorkshopUserGroup;
import org.dsa.modules.reporter.vo.ChartReVo;

import java.util.List;
import java.util.Map;

@Mapper
public interface WorkshopUserGroupMapper extends BaseMapper<WorkshopUserGroup> {


}
