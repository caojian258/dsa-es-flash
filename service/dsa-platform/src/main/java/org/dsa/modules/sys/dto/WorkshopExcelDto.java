package org.dsa.modules.sys.dto;

import lombok.Data;

@Data
public class WorkshopExcelDto {

    private String no;

    private String country;

    private String province;

    private String city;

    private String code;

    private String region;

    private String name;

    private String shortName;

    private String note;
}
