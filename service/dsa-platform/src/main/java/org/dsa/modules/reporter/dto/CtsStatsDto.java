package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CtsStatsDto implements Serializable {

    private String collectDate;

    private String ctsName;

    private Long total;

    private Long succeedTotal;

    private Long failedTotal;

    private Long spendTime;

    private Long avgSpendTime;

}
