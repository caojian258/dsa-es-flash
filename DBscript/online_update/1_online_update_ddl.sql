DROP TABLE IF EXISTS o_component;
DROP TABLE IF EXISTS o_component_version;
DROP TABLE IF EXISTS o_comp_v_group;
DROP TABLE IF EXISTS o_software;
DROP TABLE IF EXISTS o_software_version;
DROP TABLE IF EXISTS o_sw_v_comp_v;
DROP TABLE IF EXISTS o_rule;
DROP TABLE IF EXISTS o_rule_user;
DROP TABLE IF EXISTS o_rule_workshop;
DROP TABLE IF EXISTS o_rule_matched_result;
DROP TABLE IF EXISTS o_oem_software_version;
DROP TABLE IF EXISTS o_version_record;


CREATE TABLE public.o_version_record (
	id serial4 NOT NULL,
	version_uuid varchar NOT NULL,
	status varchar NOT NULL,
	edit_type varchar NOT NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	created_user_name varchar NOT NULL,
	created_user_id int8 NOT NULL
);

CREATE TABLE o_component(
    id serial NOT NULL,
    code int8 NOT NULL,
    name varchar NOT NULL,
    status int4 NOT NULL,
    description_ti varchar,
    created_at timestamp NOT NULL,
    updated_at timestamp,
    created_user_id int8,
    updated_user_id int8,
    PRIMARY KEY (id)
);

COMMENT ON TABLE o_component IS '组件表;组件基础数据表';
COMMENT ON COLUMN o_component.id IS '主键';
COMMENT ON COLUMN o_component.code IS '组件编码;客户端组件ID';
COMMENT ON COLUMN o_component.name IS '组件名称';
COMMENT ON COLUMN o_component.status IS '启用/禁用;0启用;1禁用';
COMMENT ON COLUMN o_component.description_ti IS '描述ti;翻译key，生成规则"CNAME_"+name+"_CODE_"+code';
COMMENT ON COLUMN o_component.created_at IS '创建时间';
COMMENT ON COLUMN o_component.updated_at IS '修改时间';
COMMENT ON COLUMN o_component.created_user_id IS '创建人id';
COMMENT ON COLUMN o_component.updated_user_id IS '修改人id';

CREATE UNIQUE INDEX o_component_pk ON o_component(id);

CREATE TABLE o_component_version(
    id serial NOT NULL,
    component_id int8 NOT NULL,
    version_name varchar NOT NULL,
    version_number SERIAL NOT NULL,
    update_type int4 NOT NULL,
    version_type int4,
    base_version_id int8,
    status int4 NOT NULL,
    url varchar,
    file_size int8,
    file_md5 varchar,
    release_note_ti varchar,
    description_ti varchar,
	uuid varchar,
    created_at timestamp NOT NULL,
    updated_at timestamp,
    created_user_id int8,
    updated_user_id int8,
    PRIMARY KEY (id)
);
-- 唯一索引
ALTER TABLE public.o_component_version ADD CONSTRAINT o_component_version_un UNIQUE (component_id,version_name);

COMMENT ON TABLE o_component_version IS '组件版本表;组件版本数据表';
COMMENT ON COLUMN o_component_version.id IS '主键';
COMMENT ON COLUMN o_component_version.component_id IS '组件id';
COMMENT ON COLUMN o_component_version.version_name IS '版本名称;如：V1.0或版本1.0';
COMMENT ON COLUMN o_component_version.version_number IS '版本序号;组件版本号顺序';
COMMENT ON COLUMN o_component_version.update_type IS '强制更新/可选;0为强制更新；1为可选更新';
COMMENT ON COLUMN o_component_version.version_type IS '版本类型;0为全量版本；1为增量版本';
COMMENT ON COLUMN o_component_version.base_version_id IS '基础版本id;增量基于的全量版本id（备用）';
COMMENT ON COLUMN o_component_version.status IS '开发/测试/拒绝/释放;0开发；1测试；2释放；3拒绝';
COMMENT ON COLUMN o_component_version.url IS '文件存储路径';
COMMENT ON COLUMN o_component_version.file_size IS '文件大小';
COMMENT ON COLUMN o_component_version.file_md5 IS '文件md5字符串';
COMMENT ON COLUMN o_component_version.release_note_ti IS '版本说明ti;翻译key，生成规则："CID_" + component_id + "_VERSION_" +version_name';
COMMENT ON COLUMN o_component_version.description_ti IS '描述ti';
-- 组件版本软件版本id可能一致，导致版本操作记录混乱 设置uuid保证版本在操作记录中的版本唯一性
COMMENT ON COLUMN o_component_version.uuid IS '版本唯一标识';
COMMENT ON COLUMN o_component_version.created_at IS '创建时间';
COMMENT ON COLUMN o_component_version.updated_at IS '更新时间';
COMMENT ON COLUMN o_component_version.created_user_id IS '创建人id';
COMMENT ON COLUMN o_component_version.updated_user_id IS '更新人id';


CREATE TABLE o_comp_v_group(
    id serial NOT NULL,
    group_id varchar,
    component_id int8 NOT NULL,
    component_version_id int8 NOT NULL,
    priority serial NOT NULL,
    PRIMARY KEY (id)
);
ALTER TABLE public.o_comp_v_group ADD CONSTRAINT o_comp_v_group_un UNIQUE (component_version_id);

COMMENT ON TABLE o_comp_v_group IS '组件版本group表;组件版本与组件版本之间的关联关系';
COMMENT ON COLUMN o_comp_v_group.id IS '主键';
COMMENT ON COLUMN o_comp_v_group.group_id IS '组件版本groupId;组件版本groupId，使用UUID';
COMMENT ON COLUMN o_comp_v_group.component_id IS '组件id';
COMMENT ON COLUMN o_comp_v_group.component_version_id IS '组件版本id';
COMMENT ON COLUMN o_comp_v_group.priority IS '优先级;组件版本安装优先级，数字越小优先级越高';

CREATE TABLE o_software(
    id serial NOT NULL,
    code int8 NOT NULL,
    name varchar NOT NULL,
    status int4 NOT NULL,
    description_ti varchar,
    created_at timestamp NOT NULL,
    updated_at timestamp,
    created_user_id int8,
    updated_user_id int8,
    PRIMARY KEY (id)
);

COMMENT ON TABLE o_software IS '软件表;软件基础数据表';
COMMENT ON COLUMN o_software.id IS '主键';
COMMENT ON COLUMN o_software.code IS '软件编码;客户端软件Id，如：100';
COMMENT ON COLUMN o_software.name IS '软件名称;如：Prodis.SWTS';
COMMENT ON COLUMN o_software.status IS '启用/禁用;0启用；1禁用';
COMMENT ON COLUMN o_software.description_ti IS '描述ti;翻译key，生成规则："SNAME_"+name+"_CODE_"+code';
COMMENT ON COLUMN o_software.created_at IS '创建时间';
COMMENT ON COLUMN o_software.updated_at IS '修改时间';
COMMENT ON COLUMN o_software.created_user_id IS '创建人id';
COMMENT ON COLUMN o_software.updated_user_id IS '修改人id';


CREATE TABLE o_software_version(
    id serial NOT NULL,
    software_id int8 NOT NULL,
    version_name varchar NOT NULL,
    version_number SERIAL NOT NULL,
    version_type int4 NOT NULL,
    base_version_id int8,
    update_type int4 NOT NULL,
    status int4 NOT NULL,
    last_update_time timestamp NOT NULL,
    last_update_tip_days int4 NOT NULL,
    is_cross int4 NOT NULL,
    release_note_ti varchar,
	uuid varchar,
    created_at timestamp NOT NULL,
    updated_at timestamp,
    created_user_id int8,
    updated_user_id int8,
    PRIMARY KEY (id)
);
-- 版本名称唯一
ALTER TABLE public.o_software_version ADD CONSTRAINT o_software_version_un UNIQUE (software_id,version_name);

COMMENT ON TABLE o_software_version IS '软件版本表;存储软件版本数据';
COMMENT ON COLUMN o_software_version.id IS '主键';
COMMENT ON COLUMN o_software_version.software_id IS '软件id';
COMMENT ON COLUMN o_software_version.version_name IS '版本名称;如：V1.0或版本1.0';
COMMENT ON COLUMN o_software_version.version_number IS '版本序号;软件版本号顺序（版本比对）';
COMMENT ON COLUMN o_software_version.version_type IS '版本类型;0全量；1增量';
COMMENT ON COLUMN o_software_version.base_version_id IS '基础版本id;增量基于全量的全量id';
COMMENT ON COLUMN o_software_version.update_type IS '强制更新/可选;0强制更新；1可选更新';
COMMENT ON COLUMN o_software_version.status IS '开发/测试/释放/拒绝;0开发；1测试；2释放；3拒绝';
COMMENT ON COLUMN o_software_version.last_update_time IS '最晚更新时间;到达最晚更新时间后，必须要更新此版本';
COMMENT ON COLUMN o_software_version.last_update_tip_days IS '最晚更新提前提示天数;提示版本需要更新的提前天数';
COMMENT ON COLUMN o_software_version.is_cross IS '版本是否可跳过;0可跳过；1不可跳过';
COMMENT ON COLUMN o_software_version.release_note_ti IS '版本说明ti;翻译key，生成规则："SID_" + software_id + "_VERSION_" +version_name';
COMMENT ON COLUMN o_software_version.uuid IS '版本唯一标识';
COMMENT ON COLUMN o_software_version.created_at IS '创建时间';
COMMENT ON COLUMN o_software_version.updated_at IS '修改时间';
COMMENT ON COLUMN o_software_version.created_user_id IS '创建人id';
COMMENT ON COLUMN o_software_version.updated_user_id IS '修改人id';


CREATE TABLE o_sw_v_comp_v(
    id serial NOT NULL,
    component_id int8 NOT NULL,
    component_version_id int8 NOT NULL,
    software_id int8 NOT NULL,
    software_version_id int8 NOT NULL,
    PRIMARY KEY (id)
);
ALTER TABLE public.o_sw_v_comp_v ADD CONSTRAINT o_sw_v_comp_v_un UNIQUE (component_version_id,software_version_id);
COMMENT ON TABLE o_sw_v_comp_v IS '软件版本关联组件版本表;存储软件版本与组件版本的关联关系表';
COMMENT ON COLUMN o_sw_v_comp_v.id IS '主键';
COMMENT ON COLUMN o_sw_v_comp_v.component_id IS '关联的组件id';
COMMENT ON COLUMN o_sw_v_comp_v.component_version_id IS '关联的组件版本id';
COMMENT ON COLUMN o_sw_v_comp_v.software_id IS '软件id';
COMMENT ON COLUMN o_sw_v_comp_v.software_version_id IS '软件版本id';


DROP TABLE IF EXISTS o_software_entry;
CREATE TABLE o_software_entry(
    id serial NOT NULL,
    software_id int8 NOT NULL,
    component_id int8 NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE public.o_software_entry ADD CONSTRAINT o_software_entry_un UNIQUE (software_id);

COMMENT ON TABLE o_software_entry IS '软件入口表;';
COMMENT ON COLUMN o_software_entry.id IS '主键';
COMMENT ON COLUMN o_software_entry.software_id IS '软件id';
COMMENT ON COLUMN o_software_entry.component_id IS '组件id';


CREATE TABLE o_rule(
    id serial NOT NULL,
    software_id int8 NOT NULL,
    software_version_id int8 NOT NULL,
    name varchar NOT NULL,
    start_time timestamp NOT NULL,
    end_time timestamp NOT NULL,
    status int4 NOT NULL,
    created_user_id int8,
    created_at timestamp NOT NULL,
    updated_user_id int8,
    updated_at timestamp,
    PRIMARY KEY (id)
);
COMMENT ON TABLE o_rule IS '规则表;规则基本数据表';
COMMENT ON COLUMN o_rule.id IS '自增';
COMMENT ON COLUMN o_rule.software_id IS '软件id';
COMMENT ON COLUMN o_rule.software_version_id IS '软件版本id';
COMMENT ON COLUMN o_rule.name IS '规则名称;如：Prodis.SWTS_V1.0_Test';
COMMENT ON COLUMN o_rule.start_time IS '生效时间;如：2023-03-28 00:00:00.000';
COMMENT ON COLUMN o_rule.end_time IS '结束时间;如：2024-03-28 00:00:00.000';
COMMENT ON COLUMN o_rule.status IS '状态;0正常；1禁用';
COMMENT ON COLUMN o_rule.created_user_id IS '创建人id';
COMMENT ON COLUMN o_rule.created_at IS '创建时间';
COMMENT ON COLUMN o_rule.updated_user_id IS '更新人id';
COMMENT ON COLUMN o_rule.updated_at IS '更新时间';

CREATE TABLE o_rule_user(
    id serial NOT NULL,
    rule_id int8 NOT NULL,
    user_id int8 NOT NULL,
    PRIMARY KEY (id)
);
ALTER TABLE public.o_rule_user ADD CONSTRAINT o_rule_user_un UNIQUE (rule_id,user_id);
COMMENT ON TABLE o_rule_user IS '规则用户表;存储规则关联的用户Id';
COMMENT ON COLUMN o_rule_user.id IS '主键';
COMMENT ON COLUMN o_rule_user.rule_id IS '规则id';
COMMENT ON COLUMN o_rule_user.user_id IS '用户id';

CREATE TABLE o_rule_workshop(
    id serial NOT NULL,
    rule_id int8 NOT NULL,
    workshop_id int8 NOT NULL,
    PRIMARY KEY (id)
);
ALTER TABLE public.o_rule_workshop ADD CONSTRAINT o_rule_workshop_un UNIQUE (workshop_id,rule_id);
COMMENT ON TABLE o_rule_workshop IS '规则经销商表;存储规则关联的经销商Id';
COMMENT ON COLUMN o_rule_workshop.id IS '主键';
COMMENT ON COLUMN o_rule_workshop.rule_id IS '规则id';
COMMENT ON COLUMN o_rule_workshop.workshop_id IS '经销商id';

CREATE TABLE o_rule_matched_result(
    id serial NOT NULL,
    rule_id int8 NOT NULL,
    old_software_version_id int8 NOT NULL,
    result text NOT NULL,
    PRIMARY KEY (id)
);
COMMENT ON TABLE o_rule_matched_result IS '规则匹配结果表;存储升级的软件版本组合结果';
COMMENT ON COLUMN o_rule_matched_result.id IS '主键';
COMMENT ON COLUMN o_rule_matched_result.rule_id IS '规则id';
COMMENT ON COLUMN o_rule_matched_result.old_software_version_id IS '升级前版本id';
COMMENT ON COLUMN o_rule_matched_result.result IS '升级的软件版本完整信息';

CREATE TABLE o_oem_software_version(
    id serial NOT NULL,
    oem_id int8 NOT NULL,
    software_id int8 NOT NULL,
    software_version_id int8 NOT NULL,
    created_at timestamp NOT NULL,
    updated_at timestamp,
    created_user_id int8,
    updated_user_id int8,
    PRIMARY KEY (id)
);

COMMENT ON TABLE o_oem_software_version IS 'OEM关联软件版本表;用于OEM关联软件版本';
COMMENT ON COLUMN o_oem_software_version.id IS '主键';
COMMENT ON COLUMN o_oem_software_version.oem_id IS 'oemId';
COMMENT ON COLUMN o_oem_software_version.software_id IS '软件id';
COMMENT ON COLUMN o_oem_software_version.software_version_id IS '软件版本id';
COMMENT ON COLUMN o_oem_software_version.created_at IS '创建时间';
COMMENT ON COLUMN o_oem_software_version.updated_at IS '更新时间';
COMMENT ON COLUMN o_oem_software_version.created_user_id IS '创建人id';
COMMENT ON COLUMN o_oem_software_version.updated_user_id IS '更新人id';



