package org.dsa.modules.onlineUpdate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.dsa.modules.onlineUpdate.utils.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 软件版本
 * 
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("o_software_version")
public class SoftwareVersionEntity extends BaseEntity {
	/**
	 * 软件id
	 */
	private Long softwareId;
	/**
	 * 版本名称
	 */
	private String versionName;
	/**
	 * 版本序列号;软件版本号顺序（版本比对）
	 */
	private Integer versionNumber;
	/**
	 * 强制更新/可选;0强制更新；1可选更新
	 */
	private Integer updateType;
	/**
	 * 增量/全量;0全量；1增量
	 */
	private Integer versionType;
	/**
	 * 开发/测试/释放/拒绝;0开发；1测试；2释放；3拒绝
	 */
	private Integer status;
	/**
	 * 最晚更新时间;到达最晚更新时间后，必须要更新此版本
	 */
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	@DateTimeFormat
	private Date lastUpdateTime;
	/**
	 * 最晚更新提前提示天数;提前提示版本需要更新的天数
	 */
	private Integer lastUpdateTipDays;
	/**
	 * 版本是否可跳过;0可跳过；1不可跳过
	 */
	private Integer isCross;
	/**
	 * 版本说明ti;版本说明多语言key
	 */
	private String releaseNoteTi;

	/**
	 * uuid记录版本操作使用
	 */
	private String uuid;

}
