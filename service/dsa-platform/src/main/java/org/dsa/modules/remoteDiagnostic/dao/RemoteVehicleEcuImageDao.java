package org.dsa.modules.remoteDiagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuImageEntity;

@Mapper
public interface RemoteVehicleEcuImageDao extends BaseMapper<RemoteVehicleEcuImageEntity> {
}
