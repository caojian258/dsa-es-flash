package org.dsa.modules.remoteDiagnostic.controller;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import jakarta.annotation.Resource;
import jakarta.mail.internet.MimeUtility;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.dsa.common.annotation.SysLog;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.AESUtils;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.R;
import org.dsa.common.validator.ValidatorUtils;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.common.validator.group.UpdateGroup;
import org.dsa.modules.onlineUpdate.rule.RuleComponent;
import org.dsa.modules.remoteDiagnostic.constant.FlashConstant;
import org.dsa.modules.remoteDiagnostic.dto.flash.EcuDIDDto;
import org.dsa.modules.remoteDiagnostic.dto.flash.FlashCampaignEcuDto;
import org.dsa.modules.remoteDiagnostic.dto.flash.VersionDto;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleVersionEntity;
import org.dsa.modules.remoteDiagnostic.service.EcuFlashService;
import org.dsa.modules.remoteDiagnostic.service.FlashCampaignEcuService;
import org.dsa.modules.remoteDiagnostic.service.FlashCampaignIService;
import org.dsa.modules.remoteDiagnostic.service.RemoteVehicleVersionService;
import org.dsa.modules.remoteDiagnostic.vo.Flash.FlashCampaignPageVo;
import org.dsa.modules.remoteDiagnostic.vo.Flash.FlashCampaignResVo;
import org.dsa.modules.remoteDiagnostic.vo.Flash.FlashCheckVo;
import org.dsa.modules.remoteDiagnostic.vo.PublicPageReqVo;
import org.dsa.modules.vehicle.entity.DVehicleInfoEntity;
import org.dsa.modules.vehicle.entity.VehicleInfoEntity;
import org.dsa.modules.vehicle.service.VehicleService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/ecu/flash")
public class EcuFlashController {

    @Resource
    VehicleService VehicleService;

    @Resource
    EcuFlashService ecuFlashService;

    @Resource
    RemoteVehicleVersionService remoteVehicleVersionService;

    @Resource
    FlashCampaignIService flashCampaignIService;

    @Resource
    FlashCampaignEcuService flashCampaignEcuService;

    @Resource
    RuleComponent ruleComponent;

    @RequestMapping("/check")
    public R check(@RequestBody FlashCheckVo vo) {
        VehicleInfoEntity vehicleInfo = null;
        //先校验车架号
        if (StrUtil.isBlank(vo.getVin())) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }

        //检查DID
        if (CollectionUtil.isEmpty(vo.getEcuList())) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }

        //车辆是否存在
        try {
            vehicleInfo = VehicleService.getVehicleInfoByVin(vo.getVin());
        } catch (Exception ex) {
            return R.error(Constant.Msg.CHECK_VIN);
        }

        //找到车辆大版本
        String vehicleVersion = ecuFlashService.searchVehicleVersion(vo.getEcuList());
        Long vehicleVersionId = 0L;

        if (StrUtil.isBlank(vehicleVersion) && vehicleInfo.getVersionId() != null) {
            vehicleVersionId = vehicleInfo.getVersionId();
            RemoteVehicleVersionEntity vehicleVersionEntity = remoteVehicleVersionService.getInfo(vehicleVersionId);
            if (null != vehicleVersionEntity) {
                vehicleVersion = vehicleVersionEntity.getVersionName();
            }
        }

        //车辆关联的版本也没有就报错
        if (StrUtil.isBlank(vehicleVersion)) {
            return R.error(Constant.Msg.VEH_VERSION_MISS);
        }

        //所有ECU版本没有，关键ECU版本不存在就报错
        Integer ecuFlag = ecuFlashService.checkEcuVersionList(vo.getEcuList());
        if (ecuFlag == FlashConstant.ECU_VERSION_ALL_MISS ||
                ecuFlag == FlashConstant.ECU_VERSION_KEY_MISS) {
            return R.error(Constant.Msg.VEH_VERSION_MISS);
        }

        //有白件说明就是要换件
        Boolean ecuExchange = false;
        if (ecuFlag == FlashConstant.ECU_VERSION_MISS) {
            ecuExchange = true;
        }

        //判断是否是换件 缺少一个ecu的版本信息就是换件,也就是版本集合的大小小于ecu列表大小就是换件
        log.info("ecu exchange:{}", ecuExchange);

        //找到每个ECU版本
        Map<String, VersionDto> ecuVersions = ecuFlashService.searchEcuVersion(vo.getEcuList());
        if (CollectionUtil.isEmpty(ecuVersions)) {
            return R.error(Constant.Msg.ECU_VERSION_MISS);
        }

        log.info("ecu versions:{}", JSONObject.toJSONString(ecuVersions));
        Map<String, Object> data = new HashMap<>();
        data.put("vin", vehicleInfo.getVin());


        //根据车型和车辆大版本查找车型版本

        //系统没有对应的版本就报错
        RemoteVehicleVersionEntity version = ecuFlashService.findVehicleVersion(vehicleInfo.getVehicleTypeId(), vehicleVersion.trim());
        log.info("current veh version:{}", JSONObject.toJSONString(version));
        if (null == version) {
            return R.error(Constant.Msg.ECU_VEH_VERSION_MISS);
        }

        vehicleVersionId = version.getId();

        //如果是换件且存在整车版本就对齐
        if (ecuExchange) {
            data.put("result", FlashConstant.ALIGN_VERSION);
            data.put("timestamp", ecuFlashService.getTimestamp());
            return R.ok().put("data", data);
        }

        //车辆池 过滤车辆版本

        //存在整车版本就要查找最新版本，有最新版本就更新，没有最新版本再进行ECU版本比较是否需要对齐
        List<RemoteVehicleVersionEntity> targetVehicleVersionList = ecuFlashService.findVehicleTargetVersions(vehicleInfo.getVehicleTypeId(), version.getId());
        log.info("target veh versions:{}", JSONObject.toJSONString(targetVehicleVersionList));

        //不存在整车版本直接判断是否对齐

        if (!CollectionUtil.isEmpty(targetVehicleVersionList)) {

            List<Long> vehicleVersionIds = new ArrayList<>();
            vehicleVersionIds.add(vehicleVersionId);

            List<Long> ecuVersionIds = ecuVersions.values().stream().map(dto -> {
                return dto.getVersionId();
            }).collect(Collectors.toList());

            //vin||productionDate||placeOfProduction||vehicleType||version||ecuVersion
            Map<String, Object> filters = new HashMap<>();
            filters.put("vin", vehicleInfo.getVin());
            filters.put("vehicleType", vehicleInfo.getVehicleTypeId());
            filters.put("version", vehicleVersionIds);
            if (CollectionUtil.isNotEmpty(ecuVersionIds)) {
                filters.put("ecuVersion", ecuVersionIds);
            }
            if (StrUtil.isNotBlank(vehicleInfo.getProductionDate())) {
                filters.put("productionDate", vehicleInfo.getProductionDate());
            }

            if (StrUtil.isNotBlank(vehicleInfo.getPlaceOfProduction())) {
                filters.put("placeOfProduction", vehicleInfo.getPlaceOfProduction());
            }
            //如果不在车辆池中就不能升级
            RemoteVehicleVersionEntity targetVehicleVersion = ecuFlashService.findVehicleTargetVersionByPool(targetVehicleVersionList, filters);
            log.info("target veh version:{}", JSONObject.toJSONString(targetVehicleVersion));

            if (null != targetVehicleVersion) {
                data.put("result", FlashConstant.HAS_VERSION);
                data.put("timestamp", ecuFlashService.getTimestamp());
                return R.ok().put("data", data);
            }
        }

        //存在当前整车版本且不存在最新版 就遍历每个ecu的DID列表进行比对 只要有一项不一致就返回需要对齐
        Integer result = ecuFlashService.checkAlign(version, vo.getEcuList());
        log.info("ecu checkAlign:{}", result);
        data.put("result", result);
        data.put("timestamp", ecuFlashService.getTimestamp());
        return R.ok().put("data", data);
    }

    //@todo 整车版本
    @RequestMapping("/update")
    public R update(@RequestBody FlashCheckVo vo) {
        VehicleInfoEntity vehicleInfo = null;
        //先校验车架号
        if (StrUtil.isBlank(vo.getVin())) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }

        //检查DID
        if (CollectionUtil.isEmpty(vo.getEcuList())) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }

        try {
            vehicleInfo = VehicleService.getVehicleInfoByVin(vo.getVin());
        } catch (Exception ex) {
            return R.error(Constant.Msg.CHECK_VIN);
        }

        //找到车辆大版本
        String vehicleVersion = ecuFlashService.searchVehicleVersion(vo.getEcuList());
        if (StrUtil.isBlank(vehicleVersion)) {
            return R.error(Constant.Msg.VEH_VERSION_MISS);
        }

        log.info("veh version from did:{}", vehicleVersion);

        //找到每个ECU版本
        Map<String, VersionDto> ecuVersions = ecuFlashService.searchEcuVersion(vo.getEcuList());
        if (CollectionUtil.isEmpty(ecuVersions)) {
            return R.error(Constant.Msg.ECU_VERSION_MISS);
        }

        DVehicleInfoEntity vehicle = VehicleService.getOne(vehicleInfo.getVin());

        //有新版本就需要升级result 1
        Map<String, Object> data = new HashMap<>();
        data.put("vin", vehicleInfo.getVin());

        //@todo 车辆池判断
        //是不是存在当前版本
        RemoteVehicleVersionEntity version = ecuFlashService.findVehicleVersion(vehicle.getVehicleTypeId(), vehicleVersion.trim());

        log.info("current veh version:{}", JSONObject.toJSONString(version));
        if (null == version) {
            return R.error(Constant.Msg.VEH_VERSION_MISS);
        }

        RemoteVehicleVersionEntity targetVehicleVersion = null;

        //查找最新的车辆大版本
        if (version != null) {
            targetVehicleVersion = ecuFlashService.findVehicleTargetVersion(vehicle.getVehicleTypeId(), version.getId());
        } else {
            targetVehicleVersion = ecuFlashService.findVehicleTargetVersion(vehicle.getVehicleTypeId(), null);
        }

        log.info("target veh version:{}", JSONObject.toJSONString(targetVehicleVersion));

        if (null == targetVehicleVersion) {
            data.put("result", FlashConstant.NO_VERSION);
            data.put("timestamp", ecuFlashService.getTimestamp());
            return R.ok().put("data", data);
        }

        //@todo 根据车辆大版本查找ECU版本
        List<EcuDIDDto> ecuDIDs = ecuFlashService.ecuUpdateData(targetVehicleVersion, vehicleInfo.getVin());

        data.put("ecuList", ecuDIDs);
        data.put("timestamp", ecuFlashService.getTimestamp());
        data.put("version", targetVehicleVersion);
        return R.ok().put("data", data);
    }

    @RequestMapping("/align")
    public R alignment(@RequestBody FlashCheckVo vo) {
        VehicleInfoEntity vehicleInfo = null;

        //先校验车架号
        if (StrUtil.isBlank(vo.getVin())) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }

        //检查DID
        if (CollectionUtil.isEmpty(vo.getEcuList())) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }

        //车辆是否存在
        try {
            vehicleInfo = VehicleService.getVehicleInfoByVin(vo.getVin());
        } catch (Exception ex) {
            return R.error(Constant.Msg.CHECK_VIN);
        }

        //找到车辆大版本
        String vehicleVersion = ecuFlashService.searchVehicleVersion(vo.getEcuList());
        if (StrUtil.isBlank(vehicleVersion)) {
            return R.error(Constant.Msg.VEH_VERSION_MISS);
        }

        //找到每个ECU版本
        Map<String, VersionDto> ecuVersions = ecuFlashService.searchEcuVersion(vo.getEcuList());
        if (CollectionUtil.isEmpty(ecuVersions)) {
            return R.error(Constant.Msg.ECU_VERSION_MISS);
        }

        Map<String, Object> data = new HashMap<>();
        DVehicleInfoEntity vehicle = VehicleService.getOne(vehicleInfo.getVin());

        RemoteVehicleVersionEntity version = ecuFlashService.findVehicleVersion(vehicle.getVehicleTypeId(), vehicleVersion.trim());
        if (null == version) {
            return R.error(Constant.Msg.VEH_VERSION_MISS);
        }

        List<EcuDIDDto> ecuDIDs = ecuFlashService.ecuAlignData(version, vo.getEcuList(), vo.getVin());
        if (CollectionUtil.isEmpty(ecuDIDs)) {
            data.put("result", FlashConstant.NO_VERSION);
            data.put("timestamp", ecuFlashService.getTimestamp());
            return R.ok().put("data", data);
        }

        data.put("version", version);
        data.put("result", FlashConstant.ALIGN_VERSION);
        data.put("vin", vehicleInfo.getVin());
        data.put("ecuList", ecuDIDs);
        data.put("timestamp", ecuFlashService.getTimestamp());
        return R.ok().put("data", data);
    }

    @RequestMapping("/custom")
    public R custom(@RequestBody FlashCampaignPageVo vo) {
        VehicleInfoEntity vehicleInfo = null;

        //根据VIN校验车辆信息 不存在或者不全抛异常给客户端
        if (StrUtil.isBlank(vo.getVin())) {
            return R.error(Constant.Msg.CHECK_INPUT.getCode(), Constant.Msg.CHECK_INPUT.getMessage());
        }

        try {
            vehicleInfo = VehicleService.getVehicleInfoByVin(vo.getVin());
        } catch (Exception ex) {
            return R.error(Constant.Msg.CHECK_VIN.getCode(), Constant.Msg.CHECK_VIN.getMessage());
        }

        //分页请求 刷写任务列表
        Page<FlashCampaignResVo> result = flashCampaignIService.queryPageForClient(vo);
        List<FlashCampaignResVo> voList = result.getRecords();
        List<FlashCampaignResVo> records = new ArrayList<>();

        if (CollectionUtil.isNotEmpty(voList)) {
            for (FlashCampaignResVo item : voList) {
                List<FlashCampaignEcuDto> ecuList = flashCampaignEcuService.queryCampaignEcu(item.getId());
                if (CollectionUtil.isNotEmpty(ecuList)) {
                    item.setEcuList(ecuList);
                    records.add(item);
                }
            }
        }

        Map<String, Object> data = new HashMap<>();
        data.put("records", records);
        data.put("pages", result.getPages());
        data.put("total", result.getTotal());
        data.put("size", result.getSize());
        data.put("current", result.getCurrent());

        return R.ok().put("data", data);
    }

    @RequestMapping("/downloadFile")
    public void downloadFile(HttpServletRequest request, HttpServletResponse response) {

        try {
            String path = null;
            String param = request.getParameter("params");

            if (StrUtil.isBlank(param)) {
                throw new RRException(Constant.Msg.CHECK_INPUT);
            }

            log.info("params={}", param);
            path = AESUtils.decrypt(URLDecoder.decode(param, StandardCharsets.UTF_8.name()), AESUtils.AES_KEY);
            log.info("filePath={}", path);

            File downloadFile = new File(path);
            String name = downloadFile.getName();
            // 获取文件的长度
            long fileLength = downloadFile.length();

            // 下载
            response.setContentType("application/x-download");
            response.setHeader("Accept-Ranges", "bytes");
            response.setHeader("Content-Disposition", "attachment;" + getName(name, request));
            long pos = 0;
            if (null != request.getHeader("Range")) {
                // 断点续传
                response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
                try {
                    pos = Long.parseLong(request.getHeader("Range").replaceAll(
                            "bytes=", "").replaceAll("-", ""));
                } catch (NumberFormatException e) {
                    log.error(request.getHeader("Range") + " is not Number!");
                    pos = 0;
                }
            }
            String contentRange = new StringBuffer("bytes=").append(
                    Long.valueOf(pos)).append("-").append(
                    Long.valueOf(fileLength - 1)).append("/").append(
                    Long.valueOf(fileLength)).toString();
            response.setHeader("Content-Range", contentRange);
            log.info("Content-Range={}", contentRange);
            response.setHeader("Content-Length", String.valueOf(fileLength - pos));

            // 获取输入流
            @Cleanup
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(path));
            // 输出流
            @Cleanup
            BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream());

            bis.skip(pos);
            byte[] buff = new byte[4096];
            int bytesRead;
            while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
                bos.write(buff, 0, bytesRead);
            }
        } catch (Exception e) {
            log.error("下载文件异常,输入参数=" + request.getParameter("params"), e);
        }
    }

    private String getName(String filename, HttpServletRequest request) throws Exception {
        String userAgent = request.getHeader("userAgent");
        String new_filename = URLEncoder.encode(filename, "UTF8");
        // 如果没有UA，则默认使用IE的方式进行编码，因为毕竟IE还是占多数的
        String rtn = "filename=\"" + new_filename + "\"";
        if (userAgent != null) {
            userAgent = userAgent.toLowerCase();
            // IE浏览器，只能采用URLEncoder编码
            if (userAgent.indexOf("msie") != -1) {
                rtn = "filename=\"" + new_filename + "\"";
            }
            // Opera浏览器只能采用filename*
            else if (userAgent.indexOf("opera") != -1) {
                rtn = "filename*=UTF-8''" + new_filename;
            }
            // Safari浏览器，只能采用ISO编码的中文输出
            else if (userAgent.indexOf("safari") != -1) {
                rtn = "filename=\"" + new String(filename.getBytes("UTF-8"), "ISO8859-1") + "\"";
            }
            // Chrome浏览器，只能采用MimeUtility编码或ISO编码的中文输出
            else if (userAgent.indexOf("applewebkit") != -1) {
                new_filename = MimeUtility.encodeText(filename, "UTF8", "B");
                rtn = "filename=\"" + new_filename + "\"";
            }
            // FireFox浏览器，可以使用MimeUtility或filename*或ISO编码的中文输出
            else if (userAgent.indexOf("mozilla") != -1) {
                rtn = "filename*=UTF-8''" + new String(filename.getBytes("UTF-8"), "ISO8859-1") + "\"";
            }
        }
        return rtn;

    }
}
