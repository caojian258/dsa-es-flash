package org.dsa.modules.reporter.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.R;
import org.dsa.modules.reporter.entity.FileLog;
import org.dsa.modules.reporter.enums.FileCateEnum;
import org.dsa.modules.reporter.service.FileLogService;
import org.dsa.modules.reporter.vo.file.FileLogPageReqVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/file")
public class FileController {

    @Autowired
    FileLogService fileLogService;

    @RequestMapping(value = "/log", method = { RequestMethod.POST })
    public R logPage(@RequestBody FileLogPageReqVo vo){
       Page<FileLog> logPage =  fileLogService.selectPage(vo);
        return R.ok().put("data", logPage);
    }

    @RequestMapping(value = "/getCate", method = { RequestMethod.GET,RequestMethod.POST })
    public R getCate(){
        return R.ok().put("data", Arrays.stream(FileCateEnum.values()).map(FileCateEnum::getValue).filter( v->( !"AsMaintained".equals(v)) ).collect(Collectors.toList()));
    }

    @GetMapping(value = "/download/{id}")
    public void download(@PathVariable("id") String id, HttpServletRequest req, HttpServletResponse response){

        FileLog fileLog = fileLogService.findOneFile(Long.valueOf(id));

        if(null == fileLog || null == fileLog.getFilePath()){
            return;
        }
        String filePath = fileLog.getFilePath();
        File downloadFile = new File(filePath);
        if (!downloadFile.exists()) {
            throw new RRException(Constant.Msg.FILE_NOT_EXIST);
        }

        InputStream ips = null;
        OutputStream ops = null;
        try {
            ips =  new FileInputStream(downloadFile);
            ops = response.getOutputStream();
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=utf-8" + URLEncoder.encode(downloadFile.getName(), "UTF-8"));
            IOUtils.copy(ips, ops);
            ips.close();
            ops.flush();

        } catch (IOException e) {

        } finally {
            try {
                if(ips != null ) {
                    ips.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if(ops != null) {
                    ops.flush();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
