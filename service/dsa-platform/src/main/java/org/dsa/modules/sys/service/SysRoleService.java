package org.dsa.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dsa.common.utils.PageUtils;
import org.dsa.modules.sys.entity.SysRoleEntity;

import java.util.List;
import java.util.Map;


/**
 * 角色
 *
 */
public interface SysRoleService extends IService<SysRoleEntity> {

	PageUtils queryPage(Map<String, Object> params);

	void saveRole(SysRoleEntity role);

	void update(SysRoleEntity role);

	void deleteBatch(Long[] roleIds);

	
	/**
	 * 查询用户创建的角色ID列表
	 */
	List<Long> queryRoleIdList(Long createUserId);

	/**
	 * 查询用户创建的角色Name列表
	 * @param userId
	 * @return
	 */
	List<String> queryRoleNameList(Long userId);

    List<String> queryAllRoleNameList();

	Boolean hasAdmin(Long[] ids);

    SysRoleEntity getDefaultClientRole();
}
