package org.dsa.modules.diagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("d_version_odx")
public class DVersionOdxEntity implements Serializable
{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 版本号
     */
    private String versionNum;
    /**
     * 上级版本号
     */
    private String parentVersionNum;
    /**
     * 备注
     */
    private String description;
    /**
     * 版本文件存放地址
     */
    private String filePath;
    /**
     * 版本类型（增量/全量）
     */
    private String versionType;
    /**
     * 版本状态
     */
    private String status;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建用户id
     */
    private String createBy;

}
