package org.dsa.modules.remoteDiagnostic.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.metadata.data.ReadCellData;
import com.alibaba.excel.util.ConverterUtils;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.SpringContextUtils;
import org.dsa.modules.remoteDiagnostic.dao.RVehicleTagDao;
import org.dsa.modules.remoteDiagnostic.dao.RemoteVehicleTagDao;
import org.dsa.modules.remoteDiagnostic.entity.RVehicleTagEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleTagEntity;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 直接用map接收数据
 * vin码excel导入数据拦截
 *
 * @author Jiaju Zhuang
 */
@Slf4j
public class TagExcelImportListener extends AnalysisEventListener<Map<Integer, String>> {

//    RedisUtils redisUtils;

    /**
     * 每隔5条存储数据库，实际使用中可以100条，然后清理list ，方便内存回收
     */
    private static final int BATCH_COUNT = 100;
    private static final int CACHE_LENGTH = 2000;

    private String path;
    private String key;

    private List<RVehicleTagEntity> cachedDataList;
//    private LinkedHashSet<TypedTuple<Object>> cachedDataList = new LinkedHashSet<>();
    private Map<Integer, String> headsMap;

    private Map<String, RemoteVehicleTagEntity> tagMap;

    private RemoteVehicleTagDao tagDao;
    private RVehicleTagDao rVehicleTagDao;

    public TagExcelImportListener() {

    }
    public TagExcelImportListener(String path) {
        this.path = path;
    }

    /**
     * 这里会一行行的返回头
     *
     * @param headMap
     * @param context
     */
    @Override
    public void invokeHead(Map<Integer, ReadCellData<?>> headMap, AnalysisContext context) {
        log.info("解析到一条头数据:{}", JSON.toJSONString(headMap));
        headsMap = ConverterUtils.convertToStringMap(headMap, context);
        //获取语种数组,2-size()
        //表头数据校验
        if (CollectionUtils.isEmpty(headsMap) || headsMap.size() == 0) {
            // 抛出表头数据读取异常
            throw new RRException("excel表头设置异常");
        }
        if (headsMap.size() != 2) {
            throw new RRException("表头设置错误");
        }
        // 初始化对象
//        cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
        cachedDataList = new ArrayList<>();

        this.tagDao =  SpringContextUtils.getBean("remoteVehicleTagDao", RemoteVehicleTagDao.class);
        this.rVehicleTagDao =  SpringContextUtils.getBean("RVehicleTagDao", RVehicleTagDao.class);

        this.tagMap = tagDao.selectMap();
//        this.redisUtils = SpringContextUtils.getBean("redisUtils", RedisUtils.class);
        // 如果想转成成 Map<Integer,String>
        // 方案1： 不要implements ReadListener 而是 extends AnalysisEventListener
        // 方案2： 调用 ConverterUtils.convertToStringMap(headMap, context) 自动会转换
    }

    @Override
    public void invoke(Map<Integer, String> data, AnalysisContext context) {
//        log.info("解析到一条数据:{}", JSON.toJSONString(data));
        if (tagMap.get(data.get(1)) == null) {
            RemoteVehicleTagEntity vehicleTagEntity = new RemoteVehicleTagEntity();
            vehicleTagEntity.setTagName(data.get(1));
            tagDao.insert(vehicleTagEntity);
            this.tagMap.put(data.get(1), vehicleTagEntity);
        }
        cachedDataList.add(new RVehicleTagEntity(data.get(0), tagMap.get(data.get(1)).getId()));
//        cachedDataList.add(data.get(0));
//        if (cachedDataList.size() >= BATCH_COUNT) {
//            saveData();
//            cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
//        }

    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {

        rVehicleTagDao.insertList(this.cachedDataList);

        log.info("所有数据解析完成！");
        cachedDataList = null;
    }

}