package org.dsa.modules.remoteDiagnostic.dto.page;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.ToString;
import org.dsa.modules.remoteDiagnostic.dto.api.DiagnosticConditionDto;
import org.dsa.modules.remoteDiagnostic.dto.api.DiagnosticFileDto;
import org.dsa.modules.remoteDiagnostic.dto.api.DiagnosticFunctionDto;

import java.util.Date;
import java.util.List;

@Data
@ToString()
public class DiagnosticTaskRecordDto {

    private Long id;

    private Long taskId;

    private Integer executions;

    private String sessionId;

    private String vin;

    private String type;

    private String code;

    private String message;

    private Date createdAt;

}
