package org.dsa.modules.remoteDiagnostic.vo.VinCode;


import lombok.Data;
import lombok.ToString;

@Data
@ToString(callSuper = true)
public class VinCodeRpsPageVo {
    private String vin;
}
