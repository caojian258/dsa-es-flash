package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.dto.WorkshopUserTotalDto;
import org.dsa.modules.reporter.entity.WorkshopUser;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Mapper
public interface WorkshopUserMapper extends BaseMapper<WorkshopUser> {

    public List<WorkshopUserTotalDto> workshopUserTotal(@Param("workshopGroup") String workshopGroup,
                                                        @Param("workshopId") String workshopId,
                                                        @Param("workshopName") String workshopName,
                                                        @Param("startDate") String startDate,
                                                        @Param("top") Integer top);
    public void updateLoginTime(@Param("username") String username,
                                @Param("loginTime") Date loginTime);

    Page<WorkshopUser> queryUserPage(Page<WorkshopUser> page,@Param("name") String name,@Param("group") List<String> group,@Param("active") Boolean active,@Param("workshop") String workshop,@Param("flag") Boolean flag);
}
