package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.util.Date;

@Data
public class VehicleVersionDto {

    private String vin;

    private String version;

    private String sessionId;

    private String workshop;

    private String pcid;

    private String tag;

    private Integer source;

    private Date collectTime;
}
