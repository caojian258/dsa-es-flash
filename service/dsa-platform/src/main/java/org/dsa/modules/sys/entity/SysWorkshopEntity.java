package org.dsa.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * 部门管理
 *
 */
@Data
@TableName("sys_workshop")
public class SysWorkshopEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 部门ID
	 */
	@TableId(type = IdType.AUTO)
	private Long workshopId;
	/**
	 * 上级部门ID，一级部门为0
	 */
	private Long parentId;

	/**
	 * 全称
	 */
	private String name;

	/**
	 * 地址
	 */
	private String address;

	/**
	 * 上级部门名称
	 */
	@TableField(exist=false)
	private String parentName;

	private Integer orderNum;

	@TableLogic
	private Integer delFlag;

	/**
	 * ztree属性
	 */
	@TableField(exist=false)
	private Boolean open;

	@TableField(exist=false)
	private List<?> list;

}
