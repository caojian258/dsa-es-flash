package org.dsa.modules.remoteDiagnostic.entity;


import com.baomidou.mybatisplus.annotation.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.ToString;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.common.validator.group.UpdateGroup;
import org.dsa.modules.remoteDiagnostic.pojo.BaseEntity;

import java.io.Serializable;

@Data
@ToString
@TableName("r_diag_condition")
public class DiagnosticConditionEntity extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;

    @NotBlank(message="条件名称不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String conditionName;

    private String conditionCode;

    private Integer conditionVal;

    private String unity;

    private String minVal;

    private String maxVal;

    private String eqVal;

    private String inVal;

    private String descriptionTi;

    @NotNull(message="状态不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private Integer status;

}
