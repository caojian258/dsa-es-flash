package org.dsa.modules.reporter.vo.diag;

import lombok.Data;

@Data
public class CtsChartResVo {

    private String collectMonth;

    private String ctsName;

    private String ctsTitle;

    private Long value;
}
