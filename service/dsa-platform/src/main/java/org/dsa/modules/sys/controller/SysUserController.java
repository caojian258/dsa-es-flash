package org.dsa.modules.sys.controller;

import cn.hutool.core.map.MapBuilder;
import cn.hutool.core.map.MapUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.dsa.common.annotation.SysLog;
import org.dsa.common.exception.RRException;
import org.dsa.common.pojo.CodeMessage;
import org.dsa.common.utils.*;
import org.dsa.common.validator.ValidatorUtils;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.common.validator.group.UpdateGroup;
import org.dsa.modules.sys.entity.DisplayUserEntity;
import org.dsa.modules.sys.entity.SysUserEntity;
import org.dsa.modules.sys.entity.SysUserLicenseEntity;
import org.dsa.modules.sys.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * 系统用户
 */
@RestController
@RequestMapping("/sys/user")
public class SysUserController extends AbstractController {
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private SysUserRoleService sysUserRoleService;
    @Autowired
    private SysUserGroupService sysUserGroupService;
    @Autowired
    private SysRoleService sysRoleService;
    @Resource
    private SysCaptchaService sysCaptchaService;
    @Resource
    private SysUserLicenseService sysUserLicenseService;
    @Autowired
    private RedisUtils redisUtils;

    @Value("${license.oem.code}")
    private String oemCode;
    @Value("${license.oem.name}")
    private String oemName;

    /**
     * 所有用户列表
     */
    @GetMapping("/list")
    @RequiresPermissions("sys:user:list")
    public R list(@RequestParam Map<String, Object> params) {
        //只有超级管理员，才能查看所有管理员列表
        if (!getUserName().equals(Constant.SUPER_ADMIN_USER_NAME) && !getRoleList().contains(Constant.ADMIN_ROLE)) {
            params.put("createUserId", getUserId());
        }
        PageUtils page = sysUserService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 查询任务执行人列表
     */
    @RequestMapping("/owner")
    public R taskOwnerList(@RequestParam Map<String, Object> params) {
        logger.error("获取任务执行人列表....");
        String name = params.get("name").toString();
        try {
            List<SysUserEntity> userlist = sysUserService.getUserList(name);
            return R.ok().put("userlist", userlist);
        } catch (Exception e) {
            logger.error("获取任务执行人列表时出错 : " + e);
            return R.error("获取任务执行人列表时出错");
        }
    }

    /**
     * 获取登录的用户信息
     */
    @GetMapping("/info")
    public R info() {
        return R.ok().put("user", getUser());
    }

//	/**
//	 * 修改登录用户密码
//	 */
//	@SysLog("修改密码")
//	@PostMapping("/password")
//	public R password(@RequestBody PasswordForm form){
//		Assert.isBlank(form.getNewPassword(), "新密码不为能空");

		/*//sha256加密
		String password = new Sha256Hash(form.getPassword(), getUser().getSalt()).toHex();
		//sha256加密
		String newPassword = new Sha256Hash(form.getNewPassword(), getUser().getSalt()).toHex();

		//更新密码
		boolean flag = sysUserService.updatePassword(getUserId(), password, newPassword);
		if(!flag){
			return R.error("原密码不正确");
		}*/

//		return R.ok();
//	}

    /**
     * 用户信息
     */
    @GetMapping("/info/{userId}")
    @RequiresPermissions("sys:user:info")
    public R info(@PathVariable("userId") Long userId) {
        SysUserEntity user = sysUserService.getById(userId);
        //获取用户所属的角色列表
        List<Long> roleIdList = sysUserRoleService.queryRoleIdList(userId);
        user.setRoleIdList(roleIdList);
        List<SysUserLicenseEntity> list = sysUserLicenseService.queryById(String.valueOf(userId));
        List<String> strList = new ArrayList<>();
        list.forEach(userLicense -> {
            strList.add(JSON.toJSONString(userLicense));
        });
        user.setLicenseJsonStrList(strList);
        //获取用户所属组列表
        List<Long> groupIdList = sysUserGroupService.queryGroupIdList(userId);
        user.setGroupIdList(groupIdList);
        return R.ok().put("user", user);
    }

    /**
     * 保存用户
     */
    @SysLog("保存用户")
    @PostMapping("/save")
    @RequiresPermissions("sys:user:save")
    public R save(@RequestBody SysUserEntity user) {
        try {
            ValidatorUtils.validateEntity(user, AddGroup.class);
            user.setCreateUserId(getUserId());
            user.setOemCode(oemCode);
            sysUserService.saveUser(user);
            // 存用户license信息
            if (!user.getLicenseJsonStrList().isEmpty()) {
                user.getLicenseJsonStrList().forEach(licenseJsonStr -> {
                    if (StringUtils.isNotBlank(licenseJsonStr)) {
                        SysUserLicenseEntity sysUserLicense = JSONObject.parseObject(licenseJsonStr, SysUserLicenseEntity.class);
                        sysUserLicense.setUserId(String.valueOf(user.getUserId()));
                        sysUserLicense.setUserName(user.getChineseName());
                        sysUserLicense.setOemCode(user.getOemCode());
                        sysUserLicense.setOemName(oemName);
                        sysUserLicenseService.save(sysUserLicense);
                    }
                });
            }
            return R.ok();
        } catch (RRException rrException) {
            logger.error("save user error", rrException);
            return R.error(rrException.getMessage());
        } catch (Exception e) {
            logger.error("save user error", e);
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }

    }

    /**
     * 修改用户
     */
    @SysLog("修改用户")
    @PostMapping("/update")
    @RequiresPermissions("sys:user:update")
    public R update(@RequestBody SysUserEntity user) {
        try {
            ValidatorUtils.validateEntity(user, UpdateGroup.class);
            sysUserService.update(user);
            // 存用户license信息
            if (!user.getLicenseJsonStrList().isEmpty()) {
                // 删除用户license重新创建
                List<Long> userLicenseIds = sysUserLicenseService.queryListByUserId(user.getUserId()).stream().map(SysUserLicenseEntity::getId).toList();
                sysUserLicenseService.delete(userLicenseIds);
                user.getLicenseJsonStrList().forEach(licenseJsonStr -> {
                    if (StringUtils.isNotBlank(licenseJsonStr)) {
                        SysUserLicenseEntity sysUserLicense = JSONObject.parseObject(licenseJsonStr, SysUserLicenseEntity.class);
                        sysUserLicense.setUserId(String.valueOf(user.getUserId()));
                        sysUserLicense.setUserName(user.getChineseName());
                        sysUserLicense.setOemCode(user.getOemCode());
                        sysUserLicense.setOemName(oemName);
                        sysUserLicenseService.save(sysUserLicense);
                    }
                });
            }
            return R.ok();
        } catch (RRException rrException) {
            logger.error("save user error", rrException);
            return R.error(rrException.getMessage());
        } catch (Exception e) {
            logger.error("save user error", e);
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    /**
     * 修改密码
     */
    @SysLog("修改密码")
    @PostMapping("/updatePassword")
    public R updatePassword(@RequestBody Map<String, Object> params, HttpServletRequest request) {
        try {
            String captcha = MapUtils.getString(params, "captcha");
            // 验证码校验
            String key = request.getSession().getId() + "captcha";
            String uuid = redisUtils.get(key);
            boolean validate = sysCaptchaService.validate(uuid, captcha);
            if (!validate) {
                return R.error(2002, "验证码验证失败！");
            }
            DisplayUserEntity user = redisUtils.get(getToken(), DisplayUserEntity.class);
            if (user == null) {
                return R.error(Constant.Msg.ACCOUNT_NOT_LOGIN);
            }
            String userId = user.getUserInfo().getUserId();
            String oldPassword = MapUtils.getString(params, "oldPassword");
            String password = MapUtils.getString(params, "password");
            if (StringUtils.isNotBlank(password)) {
                // 密码强度校验  长度大于16 大写字母、小写字母、数字、特殊字符
                String expr = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%.^&*?]).{16,}$";
                Pattern p = Pattern.compile(expr);
                Matcher m = p.matcher(password);
                if (!m.matches()) {
                    throw new RRException(Constant.Msg.PASSWORD_FORMAT_ERROR);
                }
            }
            SysUserEntity userInfo = sysUserService.getById(Long.valueOf(userId));
            String hex = new Sha256Hash(oldPassword, userInfo.getSalt()).toHex();
            if (!Objects.equals(hex, userInfo.getPassword())) {
                throw new RRException(Constant.Msg.ORIGINAL_PASSWORD_ERROR);
            }
            userInfo.setPassword(new Sha256Hash(password, userInfo.getSalt()).toHex());
            boolean b = sysUserService.updateById(userInfo);
            if (b) {
                redisUtils.delete(getToken());
            }
            return R.ok();
        } catch (RRException rre) {
            logger.error("修改密码异常：{}", rre.getMessage());
            return R.error(rre.getCode(), rre.getMsg());
        } catch (Exception e) {
            logger.error("修改密码异常：{}", e.getMessage());
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }

    }

    /**
     * 删除用户
     */
    @SysLog("删除用户")
    @PostMapping("/delete")
    @RequiresPermissions("sys:user:delete")
    public R delete(@RequestBody Long[] userIds) {
        if (ArrayUtils.contains(userIds, 1L)) {
            return R.error("系统管理员不能删除");
        }

        if (ArrayUtils.contains(userIds, getUserId())) {
            return R.error("当前用户不能删除");
        }

        sysUserService.deleteBatch(userIds);

        return R.ok();
    }

    /**
     * 禁用用户
     */
    @SysLog("禁用用户")
    @PostMapping("/disable")
    public R disable(@RequestBody Map<String, Object> params) {
        List<String> roleList = getRoleList();
        List<String> collect = roleList.stream().filter(str -> str.toLowerCase().contains("admin")).collect(Collectors.toList());
        if (!collect.isEmpty()) {
            Long userId = MapUtil.getLong(params, "userId");
            Integer status = MapUtil.getInt(params, "status");
            SysUserEntity user = sysUserService.getById(userId);
            user.setStatus(status == 1 ? 0 : 1);
            sysUserService.updateById(user);
            return R.ok();
        }
        return R.error(Constant.Msg.NO_OPERATE_AUTH);
    }




}
