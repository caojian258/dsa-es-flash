package org.dsa.modules.onlineUpdate.vo;

import lombok.Data;
import org.dsa.modules.onlineUpdate.entity.RuleEntity;

@Data
public class RuleVo extends RuleEntity {

    private String softwareName;
    private String softwareVersion;
}
