package org.dsa.modules.reporter.constant;

public class FlashConstant {

    public final static String ACTION = "flash";

    public final static Integer SUCCESS = 1;

    public final static Integer FAILED = 2;

    public final static Integer UNDO = 3;
}
