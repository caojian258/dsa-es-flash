package org.dsa.modules.remoteDiagnostic.job;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.extern.slf4j.Slf4j;
import org.dsa.modules.oem.util.Constant;
import org.dsa.modules.remoteDiagnostic.constant.RemoteDiagnosticConstant;
import org.dsa.modules.remoteDiagnostic.dao.RemoteEcuVersionDao;
import org.dsa.modules.remoteDiagnostic.dao.RemoteVehicleEcuDao;
import org.dsa.modules.remoteDiagnostic.dao.RemoteVehicleVersionDao;
import org.dsa.modules.remoteDiagnostic.dao.RemoteVehicleVersionEcuGroupDao;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuVersionEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleVersionEcuGroupEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleVersionEntity;
import org.dsa.modules.reporter.dao.EcuVersionHistoryMapper;
import org.dsa.modules.reporter.dao.EcuVersionOverviewMapper;
import org.dsa.modules.reporter.dao.VehicleVersionHistoryMapper;
import org.dsa.modules.reporter.dao.VehicleVersionOverviewMapper;
import org.dsa.modules.reporter.entity.EcuVersionHistory;
import org.dsa.modules.reporter.entity.EcuVersionOverview;
import org.dsa.modules.reporter.entity.VehicleVersionHistory;
import org.dsa.modules.reporter.entity.VehicleVersionOverview;
import org.dsa.modules.reporter.enums.VersionSourceEnum;
import org.dsa.modules.vehicle.dao.DVehicleInfoDao;
import org.dsa.modules.vehicle.dao.DVehicleTypeDao;
import org.dsa.modules.vehicle.entity.DVehicleInfoEntity;
import org.dsa.modules.vehicle.entity.DVehicleTypeEntity;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Component
@Slf4j
public class SoftVersionSyncVehicleJob {

    @Autowired
    RedissonClient redissonClient;
    @Autowired
    DVehicleInfoDao vehicleInfoDao;
    @Autowired
    VehicleVersionHistoryMapper vehicleVersionHistoryMapper;
    @Autowired
    VehicleVersionOverviewMapper vehicleVersionOverviewMapper;
    @Autowired
    EcuVersionOverviewMapper ecuVersionOverviewMapper;
    @Autowired
    EcuVersionHistoryMapper ecuVersionHistoryMapper;
    @Autowired
    RemoteVehicleVersionDao vehicleVersionDao;
    @Autowired
    DVehicleTypeDao vehicleTypeDao;
    @Autowired
    RemoteVehicleVersionEcuGroupDao vehicleVersionEcuGroupDao;
    @Autowired
    RemoteEcuVersionDao ecuVersionDao;
    @Autowired
    RemoteVehicleEcuDao ecuDao;

    @Scheduled(cron = "0 */5 * * * ?")
    public void execute() {
        RLock lock = redissonClient.getLock(RemoteDiagnosticConstant.SOFT_VERSION_SYNC_VEHICLE);
        boolean b = lock.tryLock();
        try {
            if (b) {
                log.info("版本记录表同步数据>>>>>>>>>>>开始");
                AtomicInteger sum = new AtomicInteger(0);
                try {
                    sync(sum);
                    log.info("版本记录表同步数据>>>>>>>>>>>结束 >>>>>本次更新数据:" + sum.get() + "条");
                } catch (Exception e) {
                    log.error("版本记录表同步数据>>>>>>>>>>>异常");
                    log.error(e.getMessage(), e);
                }
            }
        } finally {
            if (lock.isLocked() && lock.isHeldByCurrentThread()) {
                lock.unlock();
            }
        }
    }

    @Transactional
    private void sync(AtomicInteger sum) {

        List<DVehicleTypeEntity> types = vehicleTypeDao.selectListFilterVersion();
        if (types == null || types.size() == 0) {
            return;
        }
        RemoteVehicleVersionEntity vehicleVersionEntity;
        List<DVehicleInfoEntity> dVehicleInfoEntities;
        List<EcuVersionHistory> ecuVersionHistories;
        List<EcuVersionOverview> ecuVersionOverviews;
        for (DVehicleTypeEntity type : types) {
            vehicleVersionEntity = vehicleVersionDao.selectOne(Wrappers.<RemoteVehicleVersionEntity>lambdaQuery().eq(RemoteVehicleVersionEntity::getTypeId, type.getId()).orderByAsc(RemoteVehicleVersionEntity::getId).last(" limit 1"));
            if (vehicleVersionEntity == null) {
                continue;
            }
            ecuVersionHistories = new ArrayList<>();
            ecuVersionOverviews = new ArrayList<>();
            List<RemoteVehicleVersionEcuGroupEntity> vehicleVersionEcuGroupEntities = vehicleVersionEcuGroupDao.selectList(Wrappers.<RemoteVehicleVersionEcuGroupEntity>lambdaQuery().eq(RemoteVehicleVersionEcuGroupEntity::getVersionId, vehicleVersionEntity.getId()));
            if (vehicleVersionEcuGroupEntities != null && vehicleVersionEcuGroupEntities.size() != 0) {
                List<Long> ecuVersionIds = vehicleVersionEcuGroupEntities.stream().map(RemoteVehicleVersionEcuGroupEntity::getEcuVersionId).collect(Collectors.toList());
                List<RemoteVehicleEcuVersionEntity> ecuVersionList = ecuVersionDao.selectList(Wrappers.<RemoteVehicleEcuVersionEntity>lambdaQuery().in(RemoteVehicleEcuVersionEntity::getId, ecuVersionIds));
                if (ecuVersionList != null && ecuVersionList.size() != 0) {
                    for (RemoteVehicleEcuVersionEntity ecuVersion : ecuVersionList) {
                        RemoteVehicleEcuEntity ecu = ecuDao.selectById(ecuVersion.getEcuId());
                        // 添加历史记录
                        EcuVersionHistory ecuVersionHistory = new EcuVersionHistory();
                        ecuVersionHistory.setCollectTime(ecuVersion.getUpdatedAt());
                        ecuVersionHistory.setEcuName(ecu.getEcuName());
                        ecuVersionHistory.setSource(VersionSourceEnum.AsBuild.getValue());
                        ecuVersionHistory.setVehicleVersion(vehicleVersionEntity.getVersionName());
                        ecuVersionHistory.setVersionName(ecuVersion.getVersionName());
                        ecuVersionHistories.add(ecuVersionHistory);
                        // 添加版本记录
                        EcuVersionOverview ecuVersionOverview = new EcuVersionOverview();
                        ecuVersionOverview.setCollectTime(ecuVersion.getCreatedAt());
                        ecuVersionOverview.setEcuName(ecu.getEcuName());
                        ecuVersionOverview.setSource(VersionSourceEnum.AsBuild.getValue());
                        ecuVersionOverview.setVehicleVersion(vehicleVersionEntity.getVersionName());
                        ecuVersionOverview.setVersionName(ecuVersion.getVersionName());
                        ecuVersionOverviews.add(ecuVersionOverview);
                    }
                }
            }
            // 查询未添加关联的车辆
            dVehicleInfoEntities = vehicleInfoDao.selectList(Wrappers.<DVehicleInfoEntity>lambdaQuery().eq(DVehicleInfoEntity::getVehicleTypeId, type.getId()).eq(DVehicleInfoEntity::getSource, 1).isNull(DVehicleInfoEntity::getVersionId));
            if (dVehicleInfoEntities != null && dVehicleInfoEntities.size() != 0) {
                List<String> vinList = dVehicleInfoEntities.stream().map(DVehicleInfoEntity::getVin).collect(Collectors.toList());
                vehicleVersionOverviewMapper.delete(Wrappers.<VehicleVersionOverview>lambdaUpdate().in(VehicleVersionOverview::getVin, vinList));

                final String versionName = vehicleVersionEntity.getVersionName();
                final Date collectTime = vehicleVersionEntity.getCreatedAt();

                for (DVehicleInfoEntity e : dVehicleInfoEntities) {
                    ecuVersionOverviewMapper.delete(Wrappers.<EcuVersionOverview>lambdaUpdate().eq(EcuVersionOverview::getVin, e.getVin()));
                    // 添加历史记录
                    VehicleVersionHistory vehicleVersionHistory = new VehicleVersionHistory();
                    vehicleVersionHistory.setSource(VersionSourceEnum.AsBuild.getValue());
                    vehicleVersionHistory.setVin(e.getVin());
                    vehicleVersionHistory.setVersion(versionName);
                    vehicleVersionHistory.setCollectTime(collectTime);
                    vehicleVersionHistoryMapper.insert(vehicleVersionHistory);

                    // 添加版本记录
                    VehicleVersionOverview vehicleVersionOverview = new VehicleVersionOverview();
                    vehicleVersionOverview.setSource(VersionSourceEnum.AsBuild.getValue());
                    vehicleVersionOverview.setVin(e.getVin());
                    vehicleVersionOverview.setVersion(versionName);
                    vehicleVersionOverview.setCollectTime(collectTime);
                    vehicleVersionOverviewMapper.insert(vehicleVersionOverview);

                    if (ecuVersionHistories.size() != 0) {
                        ecuVersionHistoryMapper.insertsByVin(e.getVin(), ecuVersionHistories);
                    }
                    if (ecuVersionOverviews.size() != 0) {
                        ecuVersionOverviewMapper.insertsByVin(e.getVin(), ecuVersionOverviews);
                    }

                    e.setVersionId(vehicleVersionEntity.getId());
                    vehicleInfoDao.updateById(e);
                    sum.incrementAndGet();
                }
            }
        }
    }
}
