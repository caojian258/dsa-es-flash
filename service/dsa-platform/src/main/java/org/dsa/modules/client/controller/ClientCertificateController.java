package org.dsa.modules.client.controller;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.dsa.common.utils.R;
import org.dsa.modules.client.config.ClientConfiguration;
import org.dsa.modules.client.util.ClientConstant;
import org.dsa.modules.client.vo.ClientGetCertReqVo;
import org.dsa.modules.client.vo.SWTSGetCertReqVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.cert.*;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RequestMapping("/clientCert/")
@RestController
@Slf4j
public class ClientCertificateController {

    @Autowired
    ClientConfiguration clientConfiguration;

    /**
     * 获取HTTPS证书    已弃用
     *
     * @param vo 请求参数
     * @return code:证书状态1存在2存在且与查询证书不一致0不存在 -1错误
     * expiredDays  过期(剩余)天数
     * crt  证书
     * key  密钥
     */
//    @PostMapping("/getCert")
    public R getCert(@RequestBody ClientGetCertReqVo vo) {

        Map<String, Object> map = new HashMap<>();
        File file = new File(clientConfiguration.getClient());

        if (!file.exists()) {
            map = ClientConstant.getCertResult(0, 0L, "", "");
            return R.ok().put("data", map);
        }

        try {

            String s1 = FileUtils.readFileToString(file, "utf-8");
            String encodedString = "";
            if (!StringUtils.isEmpty(s1)) {
                encodedString = Base64.getEncoder().encodeToString(s1.getBytes());
            } else {
                map = ClientConstant.getCertResult(0, 0L, "", "");
                return R.ok().put("data", map);
            }
            long expiredDay = getExpiredDay(file);
            String key = getClientKey(clientConfiguration.getKey());

            if (StringUtils.isEmpty(vo.getCrt())) {
                map = ClientConstant.getCertResult(1, expiredDay, encodedString, key);
            } else {
                if (encodedString.equals(vo.getCrt())) {
                    map = ClientConstant.getCertResult(1, expiredDay, encodedString, key);
                } else {
                    map = ClientConstant.getCertResult(2, expiredDay, encodedString, key);
                }
            }
            return R.ok().put("data", map);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            map = ClientConstant.getCertResult(-1, 0L, "", "");
            return R.error("读取文件异常").put("data", map);
        }
    }

    private String getClientKey(String key) throws IOException {
        File file = new File(key);
        if (!file.exists()) {
            return "";
        }

        String s1 = null;
        String encodedString = "";
        s1 = FileUtils.readFileToString(file, "utf-8");
        if (!StringUtils.isEmpty(s1)) {
            encodedString = Base64.getEncoder().encodeToString(s1.getBytes());
        }
        return encodedString;
    }

    private Long getExpiredDay(File file) {

        long diff = 0;
        try (FileInputStream in = new FileInputStream(file)) {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            X509Certificate cert = (X509Certificate) cf.generateCertificate(in);
            cert.checkValidity();    //校验格式，错误抛出异常，可捕捉
            Date effDate = new Date();
            Date expDate = cert.getNotAfter();
            diff = expDate.getTime() - effDate.getTime();
        } catch (Exception e) {
            log.info("解析证书异常>>>>", e);
        }
        return diff <= 0 ? 0 : diff / (1000 * 60 * 60 * 24);
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//        System.out.println(diff / (1000 * 60 * 60 * 24));
//        System.out.println(simpleDateFormat.format(effDate));
//        System.out.println(simpleDateFormat.format(expDate));
    }
}
