package org.dsa.common.utils;

import org.apache.commons.lang.StringUtils;

/**
 * @author weishunxin
 * @since 2023-07-10
 */
public class FileHelper {

    public static String getFileName(String filePath) {
        if (StringUtils.isBlank(filePath)) {
            return null;
        }

        int index = filePath.lastIndexOf("/");
        if (index < 0) {
            return null;
        }

        return filePath.substring(index + 1);
    }

}
