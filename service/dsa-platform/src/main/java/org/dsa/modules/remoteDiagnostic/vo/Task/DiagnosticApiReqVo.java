package org.dsa.modules.remoteDiagnostic.vo.Task;

import lombok.Data;
import lombok.ToString;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.common.validator.group.ReqGroup;
import org.dsa.common.validator.group.UpdateGroup;
import org.hibernate.validator.constraints.Length;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.util.Date;

@Data
@ToString()
public class DiagnosticApiReqVo {

    @NotBlank(message = "vin is null", groups = {AddGroup.class, UpdateGroup.class, ReqGroup.class})
    @Length(message = "vin length error", min = 17, max = 17, groups = {AddGroup.class, UpdateGroup.class, ReqGroup.class} )
    private String vin;

    @NotBlank(message = "version is null", groups = {AddGroup.class})
    private String version;

    @NotBlank(message = "sessionId is null", groups = {UpdateGroup.class, ReqGroup.class})
    private String sessionId;

    @NotNull(message = "flag is null", groups = {ReqGroup.class})
    private Integer flag;

//    @NotNull(message = "executions is null", groups = {ReqGroup.class})
    private Integer executions;

//    @NotNull(message = "startTime is null", groups = {ReqGroup.class})
    private Date startTime;

    @NotNull(message = "endTime is null", groups = {ReqGroup.class})
    private Date endTime;

    @NotNull(message = "status is null", groups = {ReqGroup.class})
    private Integer errorCode;

    private String errorMsg;

}
