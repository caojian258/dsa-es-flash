package org.dsa.modules.onlineUpdate.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.dsa.modules.onlineUpdate.entity.SwVCompVEntity;

@EqualsAndHashCode(callSuper = true)
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SwVCompVVo extends SwVCompVEntity {

    /**
     * 依赖的组件名称
     */
    private String relComponentName;
    /**
     * 依赖的组件版本名称
     */
    private String relComponentVersionName;


}
