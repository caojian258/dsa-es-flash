package org.dsa.modules.sys.oauth2;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.sys.entity.DisplayUserEntity;
import org.dsa.modules.sys.entity.UserInfoEntity;
import org.dsa.modules.sys.entity.SysUserEntity;
import org.dsa.modules.sys.entity.SysUserTokenEntity;
import org.dsa.modules.sys.enums.SystemEnum;
import org.dsa.modules.sys.service.ShiroService;
import org.dsa.modules.sys.service.SysRoleMenuService;
import org.dsa.modules.sys.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jakarta.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 认证
 *
 */
@Slf4j
@Component
public class OAuth2Realm extends AuthorizingRealm {
    @Resource
    private ShiroService shiroService;

    @Resource
    private SysRoleService sysRoleService;

    @Autowired
    private SysRoleMenuService sysRoleMenuService;

    @Resource
    private RedisUtils redisUtils;

    private Long tokenTimeOut=180000L;

    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof OAuth2Token;
    }

    /**
     * 授权(验证权限时调用)
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        /*SysUserEntity user = (SysUserEntity)principals.getPrimaryPrincipal();
        Long userId = user.getUserId();

        //用户权限列表
        Set<String> permsSet = shiroService.getUserPermissions(userId);
        */
        DisplayUserEntity displayUserEntity = (DisplayUserEntity) principals.getPrimaryPrincipal();

        //用户权限列表
        Set<String> permsSet = shiroService.getPermissionsByRole(displayUserEntity.getRoles().stream().map(DisplayUserEntity.Role::getRoleCode).collect(Collectors.toList()), SystemEnum.DSA_PLATFORM.getCode());
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.setStringPermissions(permsSet);
        return info;
    }

    /**
     * 认证(登录时调用)
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        String accessToken = (String) token.getPrincipal();

        if("null".equals(accessToken) || accessToken.length() == 0){
            throw new IncorrectCredentialsException("token失效，请重新登录");
        }
        //查询用户信息
        SysUserEntity user = null;
        // 统一用户返回信息
        if (redisUtils.hasKey(accessToken)){
            DisplayUserEntity userFromRedis = redisUtils.get(accessToken, DisplayUserEntity.class);

            //根据accessToken，查询用户信息
            SysUserTokenEntity tokenEntity = shiroService.queryByToken(accessToken);
            //token失效
            if(tokenEntity == null || tokenEntity.getExpireTime().getTime() < System.currentTimeMillis()){
                throw new IncorrectCredentialsException("token失效，请重新登录");
            }
            if(tokenEntity.getUserId()>0){
                user = shiroService.queryUser(tokenEntity.getUserId());
                //账号锁定
                if(user.getStatus() == 0){
                    throw new LockedAccountException("账号已被锁定,请联系管理员");
                }else {
                    if (userFromRedis != null) {
                        return new SimpleAuthenticationInfo(userFromRedis, accessToken, getName());
                    }
                }
            }else{
                user = new SysUserEntity();
                user.setUserId(0L);
                user.setUsername(Constant.SUPER_ADMIN_USER_NAME);
                user.setEmail(Constant.SUPER_ADMIN_USER_NAME);
            }

        }else{
            throw new IncorrectCredentialsException("token失效，请重新登录");
        }


        DisplayUserEntity displayUserEntity;

        log.info("token: {}", accessToken);

        if(redisUtils.hasKey(accessToken)) {
            JSONObject jsonObject = JSONObject.parseObject(redisUtils.get(accessToken, tokenTimeOut)) ;
            //用户信息
            displayUserEntity = jsonObject.toJavaObject(DisplayUserEntity.class);
        }else{
            displayUserEntity = new DisplayUserEntity();
            displayUserEntity.setToken(accessToken);
            UserInfoEntity info = new UserInfoEntity();
            info.setUserId(String.valueOf(user.getUserId()));
            info.setUsername(user.getUsername());
            info.setUserDisplayName(user.getUsername());
            info.setMail(user.getEmail());
            List<DisplayUserEntity.Role> roles = new ArrayList<>();
            if(user.getUserId()>0){
                List<String> roleNameList = sysRoleService.queryAllRoleNameList();
                //处理role
                roleNameList.forEach(rName -> {
                    // functionName
                    List<String> strings = sysRoleMenuService.queryMenuNameList(rName);
                    DisplayUserEntity.Role role = DisplayUserEntity.Role.builder().roleCode(rName).function(strings).build();
                    roles.add(role);
                });
            }
            displayUserEntity.setRoles(roles);
            displayUserEntity.setUserInfo(info);
            redisUtils.set(accessToken, displayUserEntity, tokenTimeOut);
        }

        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(displayUserEntity, accessToken, getName());
        return info;
    }
}
