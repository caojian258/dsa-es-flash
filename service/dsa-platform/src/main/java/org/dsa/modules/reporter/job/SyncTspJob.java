package org.dsa.modules.reporter.job;


import lombok.extern.slf4j.Slf4j;
import org.dsa.config.SyncConfig;
import org.dsa.modules.reporter.service.SynchronizedService;
import org.dsa.modules.reporter.util.FilesUtil;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import jakarta.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/***
 * 诊断会话数据同步厂商TSP
 */
@Slf4j
@Component
@DisallowConcurrentExecution
public class SyncTspJob extends QuartzJobBean {

    @Resource
    private SynchronizedService synchronizedService;

    @Resource
    SyncConfig syncConfig;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        SimpleDateFormat sdf = new SimpleDateFormat(syncConfig.getDirFormat());
        String path = syncConfig.getTspUploadDir()+ File.separator + sdf.format(new Date());
        String archivePath = syncConfig.getTspArchiveDir() + File.separator + sdf.format(new Date());

        FilesUtil.mkdir(new File(archivePath));

        File sessionFile = new File(path);
        File[] files = sessionFile.listFiles();
        if(null != files && files.length>0){
            for (File f: files ) {
                String fileName = f.getName();
                String[] fileNames = fileName.split("_");
                if(fileName.endsWith(".xml") && fileNames.length == 2){
                    Map<String, String> map = new HashMap<>();
                    map.put("vin", fileNames[0]);
                    Boolean result = synchronizedService.syncTsp(f.getPath() , map);
                    if(result){
                        try {
                            String archiveFile = archivePath+File.separator+ f.getName();
                            Files.move(Paths.get(f.getPath()), Paths.get(archiveFile));
                        } catch (IOException e) {
                            log.error("SyncTspJob IOException: {}", e.getMessage());
                        }

                    }
                }
            }
        }
    }

}
