package org.dsa.modules.diagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import org.dsa.modules.vehicle.entity.VehicleInfoEntity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class DSessionReportEntity implements Serializable
{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 会话id
     */
    private String sessionId;

    /**
     * vin码
     */
    private String vin;

    /**
     * 激活用户id
     */
    private Long activeUserId;

    /**
     * 激活用户name
     */
    private String activeUserName;

    /**
     * 诊断用户id
     */
    private Long diagnosticUserId;

    /**
     * 诊断用户name
     */
    private String diagnosticUserName;

    /**
     * 来源
     */
    private String source;

    /**
     * 诊断开始时间
     */
    private Date startTime;

    /**
     * 诊断结束时间
     */
    private Date endTime;

    /**
     * 诊断地点
     */
    private String workShop;

    /**
     * 车型信息
     */
    private VehicleInfoEntity vehicleInfo;

    /**
     * 车辆版本信息
     */
    private List<DActionVersionEntity> versionList;

    /**
     * 车辆故障码
     */
    private List<DActionDtcEntity> dtcList;

    /**
     * 操作记录
     */
    private List<DActionInfoEntity> actionList;

    /**
     * 车辆健康检测
     */
    private List<DActionEcuEntity> ecuList;

}
