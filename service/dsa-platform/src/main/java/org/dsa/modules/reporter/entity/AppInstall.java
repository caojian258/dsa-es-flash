package org.dsa.modules.reporter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
@TableName("client_application_installation")
public class AppInstall extends BaseEntity {

    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 经销商ID
     */
    private String workshop;

    /***
     * 电脑ID
     */
    private String pcid;

    /***
     * 软件类型 smartstart runtime application
     */
    private String type;

    /***
     * 应用名称
     */
    private String name;

    /***
     * 应用版本
     */
    private String version;


    /***
     * 安装结果
     */
    private String installResult;

    /***
     * 安装时间
     */
    private String installTime;


    /***
     * 安装说明
     */
    private String installMsg;

    /***
     *
     */
    private Integer updateOid;

    /***
     * 组件ID
     */
    private Integer appId;

    /***
     * 信息收集时间
     */
    private Date collectTime;

    private String collectDate;

    /***
     * 批处理
     */
    private String tag;
}
