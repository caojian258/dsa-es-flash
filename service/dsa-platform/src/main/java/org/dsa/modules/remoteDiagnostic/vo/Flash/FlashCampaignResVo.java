package org.dsa.modules.remoteDiagnostic.vo.Flash;

import lombok.Data;
import org.dsa.modules.remoteDiagnostic.dto.flash.FlashCampaignEcuDto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class FlashCampaignResVo implements Serializable {

    private Long id;

    private String vin;

    private String version;

    private Date createdAt;

    private List<FlashCampaignEcuDto> ecuList;
}
