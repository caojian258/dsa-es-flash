package org.dsa.modules.reporter.vo.job;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.modules.reporter.pojo.PageParam;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class JobPageReqVo extends PageParam {

    private String name;

    private String group;

    private String status;

    private Boolean cronFlag;

    private String startDate;

    private String endDate;
}
