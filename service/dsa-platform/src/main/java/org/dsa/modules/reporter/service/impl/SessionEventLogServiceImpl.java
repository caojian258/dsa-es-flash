package org.dsa.modules.reporter.service.impl;

import org.dsa.modules.reporter.dao.SessionEventMapper;
import org.dsa.modules.reporter.entity.SessionEventLog;
import org.dsa.modules.reporter.service.SessionEventLogService;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;


@Service
public class SessionEventLogServiceImpl implements SessionEventLogService {

    @Resource
    private SessionEventMapper mapper;


    @Override
    public SessionEventLog selectOneBySessionId(String vin, String sessionId) {
        return mapper.selectByVinSessionId(vin, sessionId);
    }
}
