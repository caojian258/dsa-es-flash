package org.dsa.modules.diagnostic.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dsa.modules.diagnostic.dto.DDiagPackageVersionDTO;
import org.dsa.modules.diagnostic.dto.PackageCompareDTO;
import org.dsa.modules.diagnostic.dto.PackageCompareReqDTO;
import org.dsa.modules.diagnostic.entity.DDiagPackageVersionEntity;

import java.util.List;

/**
 * @author weishunxin
 * @since 2023-05-29
 */
public interface PackageVersionService extends IService<DDiagPackageVersionEntity> {

    /**
     *
     * 根据车型查询包版本列表
     *
     * @param vehicleTypeId vehicleTypeId
     * @return List<DDiagPackageVersionEntity>
     */
    List<DDiagPackageVersionEntity> list(Long vehicleTypeId);

    DDiagPackageVersionDTO getInfo(Long id);

    List<DDiagPackageVersionEntity> list(Long vehicleTypeId, String status, boolean idDesc);

    void savePackage(DDiagPackageVersionEntity entity);

    void updatePackage(DDiagPackageVersionEntity entity);

    void copyPackage(Long id);

    PackageCompareDTO comparePackage(PackageCompareReqDTO compareReqDTO);

    DDiagPackageVersionEntity get(Long vehicleTypeId, String versionNum);

    DDiagPackageVersionEntity getLatest(Long vehicleTypeId);

    PackageCompareDTO getLatestPackage(Long vehicleTypeId, boolean increment, String packageVersion, String odxVersion, String otxVersion, DDiagPackageVersionEntity packageVersionEntity);

    void checkReleasePackage(DDiagPackageVersionEntity entity);

    boolean hasNotReleasedPackage(Long vehicleTypeId);

}
