package org.dsa.modules.sys.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.sys.dto.SysTranslationsDto;
import org.dsa.modules.sys.entity.SysLocaleEntity;
import org.dsa.modules.sys.vo.LocalePageVo;

import java.util.List;

/**
 * 语种
 *
 */
public interface SysLocaleService{

    Integer getIdByLocale(String locale);

    Integer truncateTable();

    Integer insert(SysLocaleEntity sysLocaleEntity);

    String getLocale(String key, String locale);

    void handleSqlite(String path);

    List<SysTranslationsDto> queryLocaleTi(String locale);

    Integer countTi(String locale);

    Page<SysTranslationsDto> queryLocaleTiPage(LocalePageVo vo);
}

