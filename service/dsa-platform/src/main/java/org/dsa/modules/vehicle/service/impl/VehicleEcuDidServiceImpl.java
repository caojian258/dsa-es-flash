package org.dsa.modules.vehicle.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.dsa.modules.vehicle.dao.VehicleEcuDidDao;
import org.dsa.modules.vehicle.entity.VehicleEcuDid;
import org.dsa.modules.vehicle.service.VehicleEcuDidService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("VehicleEcuDidService")
public class VehicleEcuDidServiceImpl extends ServiceImpl<VehicleEcuDidDao, VehicleEcuDid> implements VehicleEcuDidService {

    /***
     * 车型ECU维度的 先删除已经存在的再批量插入
     * @param VehicleTypeId
     * @param ecuName
     * @param didList
     */
    @Override
    public void batchInsertByEcu(Long VehicleTypeId, String ecuName, List<VehicleEcuDid> didList) {
        Integer total = baseMapper.countByVehicleTypeIdEcu(VehicleTypeId, ecuName);
        if(total>0){
            baseMapper.deleteByVehicleTypeEcu(VehicleTypeId, ecuName);
        }
        baseMapper.batchInsert(didList);
    }

    @Override
    public void oneInsert(VehicleEcuDid did) {
        baseMapper.oneInsert(did);
    }

    @Override
    public void batchInsert(List<VehicleEcuDid> didList) {
        baseMapper.batchInsert(didList);
    }

    @Override
    public void deleteByVehicleTypeEcuDid(Long VehicleTypeId, String ecuName, String did) {
        baseMapper.deleteByVehicleTypeEcuDid(VehicleTypeId,ecuName,did);
    }

    @Override
    public void deleteByVehicleTypeEcu(Long VehicleTypeId, String ecuName) {
        baseMapper.deleteByVehicleTypeEcu(VehicleTypeId, ecuName);
    }

    @Override
    public void deleteByVehicleType(Long VehicleTypeId) {
        baseMapper.deleteByVehicleType(VehicleTypeId);
    }

    @Override
    public List<VehicleEcuDid> list(Long VehicleTypeId) {
        LambdaQueryWrapper<VehicleEcuDid> qw = Wrappers.lambdaQuery();
        qw.eq(VehicleEcuDid::getVehicleTypeId, VehicleTypeId);
        return baseMapper.selectList(qw);
    }

}
