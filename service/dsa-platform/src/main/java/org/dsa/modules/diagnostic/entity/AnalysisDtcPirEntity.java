package org.dsa.modules.diagnostic.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 故障码统计 - 饼状图
 * 
 */
@Data
public class AnalysisDtcPirEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 故障码
	 */
	private String name;

	/**
	 * 故障数
	 */
	private int value;

}
