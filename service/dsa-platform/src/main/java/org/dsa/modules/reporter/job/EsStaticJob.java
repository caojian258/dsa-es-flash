package org.dsa.modules.reporter.job;

import cn.hutool.core.collection.CollectionUtil;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.dsa.modules.reporter.entity.CtsDaily;
import org.dsa.modules.reporter.entity.DtcDaily;
import org.dsa.modules.reporter.service.CtsDailyService;
import org.dsa.modules.reporter.service.DiagnosticEsService;
import org.dsa.modules.reporter.service.DtcDailyService;
import org.dsa.modules.vehicle.entity.DVehicleInfoEntity;
import org.dsa.modules.vehicle.service.VehicleService;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/***
 * 定时去ES里面捞数据 统计cts/dtc每天的量
 */
@Slf4j
@Component
@DisallowConcurrentExecution
public class EsStaticJob extends QuartzJobBean {

    @Autowired
    DiagnosticEsService diagnosticEsService;

    @Resource
    DtcDailyService dtcDailyService;

    @Resource
    CtsDailyService ctsDailyService;

    @Autowired
    VehicleService VehicleService;

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String day = sdf.format(new Date());
        List<Map<String, Object>> vinAggs = diagnosticEsService.vinDailyAggregations(day);

        for (Map<String, Object> map: vinAggs) {
            if(null != map.get("key")){
                Long vehId = 0L;
                String vin = map.get("key").toString();
                DVehicleInfoEntity vehicleInfoEntity =  VehicleService.getOne(vin);
                if(null != vehicleInfoEntity){
                    vehId = vehicleInfoEntity.getVehicleTypeId();
                }

                List<DtcDaily> dtcDailyList =  diagnosticEsService.dtcDailyByVinDay(vin, day);
                if(CollectionUtil.isNotEmpty(dtcDailyList)){
                    for (DtcDaily dtc: dtcDailyList){
                        dtcDailyService.upsertData(dtc);
                    }
                }else{
                    dtcDailyService.deleteData(vin, day);
                }

                List<CtsDaily> ctsDailyList =  diagnosticEsService.ctsDailyByVinDay(vin, day);
                if(CollectionUtil.isNotEmpty(ctsDailyList)){
                    for (CtsDaily cts: ctsDailyList){
                        cts.setVehicleTypeId(vehId);
                        ctsDailyService.upsertData(cts);
                    }
                }else{
                    ctsDailyService.deleteData(vin, day);
                }
            }
        }

    }

}
