package org.dsa.modules.reporter.service;

import java.util.List;

public interface DatabaseService {

    public void createPartitionTable();

    public List<String> partitionRanges();
}
