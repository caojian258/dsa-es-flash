package org.dsa.modules.remoteDiagnostic.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.remoteDiagnostic.constant.RemoteDiagnosticConstant;
import org.dsa.modules.remoteDiagnostic.dao.DiagnosticTaskCampaignDao;
import org.dsa.modules.remoteDiagnostic.dao.DiagnosticTaskDetailDao;
import org.dsa.modules.remoteDiagnostic.dao.DiagnosticTaskOverviewDao;
import org.dsa.modules.remoteDiagnostic.dao.DiagnosticTaskRecordDao;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskDetailEntity;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskOverviewEntity;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskRecordEntity;
import org.dsa.modules.remoteDiagnostic.enums.TaskDetailTypeEnum;
import org.dsa.modules.remoteDiagnostic.service.DiagnosticTaskOverviewService;
import org.dsa.modules.remoteDiagnostic.vo.Task.DiagnosticTaskOverviewPageReqVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DiagnosticTaskOverviewServiceImpl implements DiagnosticTaskOverviewService {
    @Autowired
    private DiagnosticTaskOverviewDao taskOverviewDao;
    @Autowired
    private DiagnosticTaskRecordDao taskRecordDao;
    @Autowired
    private DiagnosticTaskDetailDao taskDetailDao;
    @Autowired
    private DiagnosticTaskCampaignDao campaignDao;
    @Autowired
    private RedisUtils redisUtils;

    @Override
    public Page<DiagnosticTaskOverviewEntity> selectPage(DiagnosticTaskOverviewPageReqVo vo) {
        Page<DiagnosticTaskOverviewEntity> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);
//        LambdaQueryWrapper<DiagnosticTaskOverviewEntity> qw = Wrappers.<DiagnosticTaskOverviewEntity>lambdaQuery();

        Page<DiagnosticTaskOverviewEntity> overviewPage = taskOverviewDao.getOverviewPage(page, vo.getName(), vo.getValidityPeriod());

        List<DiagnosticTaskOverviewEntity> records = overviewPage.getRecords();

        if (records != null && records.size() != 0) {
            records.forEach(record -> {
                setCacheVal(record, RemoteDiagnosticConstant.TASK_OVERVIEW_KEY + record.getCampaignId());
            });
        }

        return overviewPage;
    }

    @Override
    public DiagnosticTaskOverviewEntity getInfo(Long id) {
        DiagnosticTaskOverviewEntity overview = taskOverviewDao.getOverviewByTask(id);
        if (overview != null) {
            setCacheVal(overview, RemoteDiagnosticConstant.TASK_OVERVIEW_KEY + overview.getCampaignId());
        }
        return overview;
    }

    @Override
    public void saveTask(DiagnosticTaskOverviewEntity task) {

    }

    @Override
    public void updateTask(DiagnosticTaskOverviewEntity task) {

    }

    @Override
    @Transactional
    public void updateTask(String sessionId, String vin) {
//        DiagnosticTaskRecordEntity record = taskRecordDao.getInfoBySessionId(sessionId,vin);
//        if (record == null) {
//            return;
////            throw new RuntimeException("未查询到获取记录");
//        }
//        DiagnosticTaskOverviewEntity overview = taskOverviewDao.getInfoByTask(record.getCampaignId());
//        if (overview == null) {
//            return;
////            throw new RuntimeException("未查询到获取记录");
//        }
//
//        overview.setUpdatedAt(new Date());
//        overview.setDownloadNum(overview.getDownloadNum() + 1);
//
//        DiagnosticTaskDetailEntity diagnosticTaskDetailEntity = new DiagnosticTaskDetailEntity();
//        diagnosticTaskDetailEntity.setCampaignId(record.getCampaignId());
//        diagnosticTaskDetailEntity.setVin(vin);
//
//        diagnosticTaskDetailEntity.setSessionId(sessionId);
//        diagnosticTaskDetailEntity.setType(TaskDetailTypeEnum.DOWNLOAD.getValue());
//        diagnosticTaskDetailEntity.setCode("0");
//
//        taskDetailDao.insert(diagnosticTaskDetailEntity);
//
//        taskOverviewDao.updateById(overview);
    }

    @Override
    public List<DiagnosticTaskOverviewEntity> getTasks() {
        return null;
    }

    private void setCacheVal(DiagnosticTaskOverviewEntity record, String key) {
        if (redisUtils.hHasKey(key, RemoteDiagnosticConstant.TASK_OVERVIEW_MATCH_KEY)) {
            record.setMatchNum(Long.parseLong(redisUtils.hget(key, RemoteDiagnosticConstant.TASK_OVERVIEW_MATCH_KEY).toString()));
        }

        if (redisUtils.hHasKey(key, RemoteDiagnosticConstant.TASK_OVERVIEW_CANCEL_KEY)) {
            record.setCancelNum(Long.parseLong(redisUtils.hget(key, RemoteDiagnosticConstant.TASK_OVERVIEW_CANCEL_KEY).toString()));
        }

        if (redisUtils.hHasKey(key, RemoteDiagnosticConstant.TASK_OVERVIEW_REJECT_KEY)) {
            record.setRejectNum(Long.parseLong(redisUtils.hget(key, RemoteDiagnosticConstant.TASK_OVERVIEW_REJECT_KEY).toString()));
        }

        if (redisUtils.hHasKey(key, RemoteDiagnosticConstant.TASK_OVERVIEW_ACCEPTANCE_KEY)) {
            record.setAcceptanceNum(Long.parseLong(redisUtils.hget(key, RemoteDiagnosticConstant.TASK_OVERVIEW_ACCEPTANCE_KEY).toString()));
        }

        if (redisUtils.hHasKey(key, RemoteDiagnosticConstant.TASK_OVERVIEW_DOWNLOAD_KEY)) {
            record.setDownloadNum(Long.parseLong(redisUtils.hget(key, RemoteDiagnosticConstant.TASK_OVERVIEW_DOWNLOAD_KEY).toString()));
        }

        if (redisUtils.hHasKey(key, RemoteDiagnosticConstant.TASK_OVERVIEW_SUCCESS_KEY)) {
            record.setSuccessNum(Long.parseLong(redisUtils.hget(key, RemoteDiagnosticConstant.TASK_OVERVIEW_SUCCESS_KEY).toString()));
        }

        if (redisUtils.hHasKey(key, RemoteDiagnosticConstant.TASK_OVERVIEW_FAIL_KEY)) {
            record.setFailNum(Long.parseLong(redisUtils.hget(key, RemoteDiagnosticConstant.TASK_OVERVIEW_FAIL_KEY).toString()));
        }
    }
}
