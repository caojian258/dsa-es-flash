package org.dsa.modules.diagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("d_action_dtc")
public class DActionDtcEntity implements Serializable
{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 功能执行id
     */
    private String sessionId;
    /**
     * 功能执行id
     */
    private String actionId;
    /**
     * 排序
     */
    private int serial;
    /**
     * ecu 名称
     */
    private String ecuName;
    /**
     * 语言
     */
    private String ti;
    /**
     * 故障码
     */
    private String dtcCode;
    /**
     * 故障码
     */
    private String dtcCodeInt;
    /**
     * 故障码CodeHex
     */
    private String dtcCodeHex;
    /**
     * 备注
     */
    private String description;

    /**
     * 状态
     */
    private String status;

    /**
     * 首次故障码个数
     */
    private String dtcCount;

    /**
     * 首次ecu状态 0离线 1在线
     */
    private String ecuStatus;

    /**
     * 末次故障码个数
     */
    private String lastDtcCount;

    /**
     * 末次ecu状态 0离线 1在线
     */
    private String lastEcuStatus;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修复状态
     */
    private String repairStatus;
}
