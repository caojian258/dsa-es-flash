package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class VehicleModelDto implements Serializable {

    private String model;

    private String title;
}
