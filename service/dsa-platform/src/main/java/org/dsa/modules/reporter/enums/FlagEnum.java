package org.dsa.modules.reporter.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum FlagEnum {
    YES("YES"),
    NO("NO"),
    UNKNOWN("N/A");

    @EnumValue
    private final String value;
}
