package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class ClientDependencyDto implements Serializable {
    private String  deviceId;

    private Integer softwareId;
    private Integer softwareVersionNumber;
    private String  softwareName;
    private String  softwareVersionName;

    private Integer componentId;
    private Integer componentVersionNumber;
    private String  componentName;
    private String  componentVersionName;

    private Integer dependencyComponentId;
    private Integer dependencyComponentVersionNumber;
    private String  dependencyComponentName;
    private String  dependencyComponentVersionName;
}
