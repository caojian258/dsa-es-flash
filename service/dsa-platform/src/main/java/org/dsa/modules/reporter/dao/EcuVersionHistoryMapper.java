package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.dto.EcuVersionHistoryDto;
import org.dsa.modules.reporter.entity.EcuVersionHistory;
import org.dsa.modules.reporter.entity.VehicleFlashDetail;

import java.util.List;

@Mapper
public interface EcuVersionHistoryMapper extends BaseMapper<EcuVersionHistory> {

    public void syncSessionEcuVersion(String sessionId);

    public EcuVersionHistory checkSessionEcuData(String sessionId);

    List<EcuVersionHistory> findEcuHisVersion(@Param("vin") String vin,@Param("ecuName") String ecuName,@Param("version") String version,@Param("collectTime") String collectTime);

//    List<EcuVersionHistory> findEcuHisVin(@Param("vin") String vin);

    List<EcuVersionHistory> findEcuHisVin(@Param("vin") String vin,@Param("ecuName") String ecuName,@Param("version") String version);

    List<EcuVersionHistoryDto> selectEcuVersionHistory(@Param("vin") String vin, @Param("version") String version);


    void insertsByVin(@Param("vin") String vin, @Param("histories")  List<EcuVersionHistory> ecuVersionHistories);

}
