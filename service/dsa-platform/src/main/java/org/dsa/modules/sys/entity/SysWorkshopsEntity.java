package org.dsa.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 部门管理
 *
 */
@Data
@TableName("sys_workshops")
public class SysWorkshopsEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 部门ID
	 */
	@TableId(type = IdType.AUTO)
	private Long id;

	/**
	 * 上级部门ID，一级部门为0
	 */
	private Long parentId;

    /***
     * 编码
	 */
	private String code;

	/**
	 * 全称
	 */
	private String name;

    /***
     * 简称
	 */
	private String shortName;

	/***
     * 国家
	 */
	private Long countryCode;

	private String countryName;

    /***
     * 省份
	 */
	private Long provinceCode;

	private String provinceName;


	/***
     * 城市
	 */
	private Long cityCode;

	private String cityName;


	/***
     * 大区
	 */
	private String region;

	/**
	 * 地址
	 */
	private String address;


	private Integer orderNum;


//	@TableLogic
	private Integer delFlag;

	/***
     * 备注
	 */
	private String note;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
     * 修改时间
	 */
	private Date updateTime;

	@TableField(exist=false)
	private Long cityId;

	@TableField(exist=false)
	private Long provinceId;
}
