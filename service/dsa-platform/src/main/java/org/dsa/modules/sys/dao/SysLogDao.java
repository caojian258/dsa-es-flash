package org.dsa.modules.sys.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.dsa.modules.sys.entity.SysLogEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统日志
 *
 */
@Mapper
public interface SysLogDao extends BaseMapper<SysLogEntity> {
	
}
