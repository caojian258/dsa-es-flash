package org.dsa.modules.reporter.vo.chart;

import lombok.Data;

import java.util.List;


@Data
public class BarVo {

    private String name;

    private String stack;

    private String type;

    private List<Long> data;
}
