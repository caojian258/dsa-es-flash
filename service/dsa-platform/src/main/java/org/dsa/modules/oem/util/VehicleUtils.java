package org.dsa.modules.oem.util;

import org.dsa.common.utils.Constant;
import org.dsa.modules.oem.dto.SyncVehicleInfoDTO;
import org.dsa.modules.vehicle.dao.DVehicleTypeDao;
import org.dsa.modules.vehicle.entity.DVehicleTypeEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author weishunxin
 * @since 2023-05-30
 */
@Component(value = "vehicleUtils-V1")
public class VehicleUtils {

    @Autowired
    private DVehicleTypeDao dVehicleTypeDao;

    public Long createLevelInfo(SyncVehicleInfoDTO vehicleInfoDTO, Map<String, Long> map) {
        Long makerId;
        Long modelId;
        Long platformId;
        Long yearId;
        Long trimLevelId;

        if (map.containsKey(Constant.VEHICLE_TYPE.MAKER.getName() + vehicleInfoDTO.getMake())) {
            makerId = map.get(Constant.VEHICLE_TYPE.MAKER.getName() + vehicleInfoDTO.getMake());
        } else {
            makerId = genVehicleType(0L, Constant.VEHICLE_TYPE.MAKER, vehicleInfoDTO.getMake());
            map.put(Constant.VEHICLE_TYPE.MAKER.getName() + vehicleInfoDTO.getMake(), makerId);
        }

        if (map.containsKey(Constant.VEHICLE_TYPE.MODEL.getName() + vehicleInfoDTO.getModelCode())) {
            modelId = map.get(Constant.VEHICLE_TYPE.MODEL.getName() + vehicleInfoDTO.getModelCode());
        } else {
            modelId = genVehicleType(makerId, Constant.VEHICLE_TYPE.MODEL, vehicleInfoDTO.getModelCode());
            map.put(Constant.VEHICLE_TYPE.MODEL.getName() + vehicleInfoDTO.getModelCode(), modelId);
        }

        if (map.containsKey(modelId + "_" + Constant.VEHICLE_TYPE.PLATFORM.getName() + vehicleInfoDTO.getVehiclePlatform())) {
            platformId = map.get(modelId + "_" + Constant.VEHICLE_TYPE.PLATFORM.getName() + vehicleInfoDTO.getVehiclePlatform());
        } else {
            platformId = genVehicleType(modelId, Constant.VEHICLE_TYPE.PLATFORM, vehicleInfoDTO.getVehiclePlatform());
            map.put(modelId + "_" + Constant.VEHICLE_TYPE.PLATFORM.getName() + vehicleInfoDTO.getVehiclePlatform(), platformId);
        }

        if (map.containsKey(platformId + "_" + Constant.VEHICLE_TYPE.YEAR.getName() + vehicleInfoDTO.getYear())) {
            yearId = map.get(platformId + "_" + Constant.VEHICLE_TYPE.YEAR.getName() + vehicleInfoDTO.getYear());
        } else {
            yearId = genVehicleType(platformId, Constant.VEHICLE_TYPE.YEAR, vehicleInfoDTO.getYear());
            map.put(platformId + "_" + Constant.VEHICLE_TYPE.YEAR.getName() + vehicleInfoDTO.getYear(), yearId);
        }

        if (map.containsKey(yearId + "_" + Constant.VEHICLE_TYPE.TRIM_LEVEL.getName() + vehicleInfoDTO.getTrimLevel())) {
            trimLevelId = map.get(yearId + "_" + Constant.VEHICLE_TYPE.TRIM_LEVEL.getName() + vehicleInfoDTO.getTrimLevel());
        } else {
            trimLevelId = genVehicleType(yearId, Constant.VEHICLE_TYPE.TRIM_LEVEL, vehicleInfoDTO.getTrimLevel());
            map.put(yearId + "_" + Constant.VEHICLE_TYPE.TRIM_LEVEL.getName() + vehicleInfoDTO.getTrimLevel(), trimLevelId);
        }

        return trimLevelId;
    }

    private Long genVehicleType(Long parentId, Constant.VEHICLE_TYPE VEHICLE_TYPE, String value) {
        int orderNum = 0;
        if (parentId != 0L) {
            Integer maxOrderNum = dVehicleTypeDao.getMaxOrderNum(parentId);
            if (maxOrderNum != null) {
                orderNum = maxOrderNum + 1;
            }
        }

        DVehicleTypeEntity dVehicleTypeEntity = dVehicleTypeDao.selectByValueAndParentId(value, parentId);
        if (dVehicleTypeEntity == null) {
            dVehicleTypeEntity = new DVehicleTypeEntity();
            dVehicleTypeEntity.setParentId(parentId);
            dVehicleTypeEntity.setLevel(VEHICLE_TYPE.getLevel());
            dVehicleTypeEntity.setValue(value);
            dVehicleTypeEntity.setOrderNum(orderNum);
            dVehicleTypeDao.insertDVehicleType(dVehicleTypeEntity);
        }
        return dVehicleTypeEntity.getId();
    }

}
