package org.dsa.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.sys.entity.SysRoleEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 角色管理
 *
 */
@Mapper
public interface SysRoleDao extends BaseMapper<SysRoleEntity> {
	
	/**
	 * 查询用户创建的角色ID列表
	 */
	List<Long> queryRoleIdList(Long createUserId);

	/**
	 * 查询用户创建的角色Name列表
	 * @param userId
	 * @return
	 */
	List<String> queryRoleNameList(Long userId);

	List<String> queryRoleNameListByUid(@Param("userId") Long userId);

	List<String> queryAllRoleNameList();
}
