package org.dsa.modules.reporter.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.core.text.StrSplitter;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.AuthorizationException;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.reporter.dao.*;
import org.dsa.modules.reporter.dto.*;
import org.dsa.modules.reporter.entity.*;
import org.dsa.modules.reporter.enums.AppTypeEnum;
import org.dsa.modules.reporter.enums.FlagEnum;
import org.dsa.modules.reporter.enums.StatusTypeEnum;
import org.dsa.modules.reporter.service.SystemProtocolItemIService;
import org.dsa.modules.reporter.service.SystemService;
import org.dsa.modules.reporter.util.JsonReader;
import org.dsa.modules.reporter.util.ReportUtil;
import org.dsa.modules.reporter.vo.api.*;
import org.dsa.modules.sys.entity.DisplayUserEntity;
import org.dsa.modules.sys.entity.UserInfoEntity;
import org.dsa.modules.sys.entity.SysUserEntity;
import org.dsa.modules.sys.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
@Service
public class SystemServiceImpl implements SystemService {

    @Autowired
    SystemHistoryMapper historyMapper;

    @Autowired
    SystemMetadataMapper metadataMapper;

    @Autowired
    SystemProtocolItemIService protocolItemIService;

    @Autowired
    SystemOverviewMapper overviewMapper;

    @Autowired
    ClientUpgradeMapper upgradeResultMapper;

    @Autowired
    SysUserService sysUserService;

    @Resource
    RedisUtils redisUtils;

    @Override
    public void addHistory(String filePath, String tag) {
        JsonReader reader = new JsonReader(filePath);
        String jsonStr = reader.getJsonString();
        JSONObject parse = (JSONObject)JSONObject.parse(jsonStr);

        SystemHistory en = new SystemHistory();
        Map<String, Object> jsonFile = new HashMap<>();
        Set<Map.Entry<String, Object>> entrySet = parse.entrySet();

        for (Map.Entry<String, Object> entry: entrySet) {
            jsonFile.put(entry.getKey().toLowerCase() , entry.getValue());
        }
        String[] infos = parse.get("SYSTEM_INFO_OUTPUT").toString().split(",");
        List<String> infoList = Arrays.asList(infos);
        List<String> osInfoList = new ArrayList<>();
        for (String ss : infoList){
            String item = ss.replace("\"", "").replace("/n","");
            osInfoList.add(item);
        }

        BeanUtil.fillBeanWithMap(jsonFile, en, true, new CopyOptions());
        en.setTag(tag);
        en.setSystemInfoOutput(String.join(",", osInfoList));

        en.setAntivirFlag(ReportUtil.sysFlagCheck(en.getAntivirInstalled()));

        en.setProxyFlag(ReportUtil.sysFlagCheck(en.getProxies()));
        en.setFirewallFlag(ReportUtil.sysFirewallCheck((JSONObject) en.getFirewalls()));
        en.setWlanFlag(ReportUtil.sysWlanCheck((JSONArray) en.getWlanNetworks()));

        historyMapper.save(en);
    }

    @Override
    public void insertMetadata(SystemMetadata sm) {
        metadataMapper.insert(sm);
    }

    @Override
    public void delMetadataById(Long id) {
        metadataMapper.deleteById(id);
    }

    @Override
    public void delMetadataByItem(String item) {
        metadataMapper.deleteByItem(item);
    }

    @Override
    public void updateMetadata(SystemMetadata sm) {
        metadataMapper.updateById(sm);
    }

    @Override
    public AppsInfoDto getApplicationsInfo(String tag) {
        AppsInfoDto infoDto = new AppsInfoDto();
        AppsDto dto =  historyMapper.getApplicationsByTag(tag);

        if (dto == null) {
            return null;
        }

        if(null != dto.getInstallationProtocol()){
            JSONArray installJsonArray= JSONArray.parseArray(dto.getInstallationProtocol().toString());
            if(null != installJsonArray && installJsonArray.size()>0){
                List<AppInstallDto> appInstallDtos = new ArrayList<>();
                for (Object install : installJsonArray){
                    AppInstallDto appInstallDto = JSONObject.toJavaObject((JSONObject) install, AppInstallDto.class);
                    appInstallDtos.add(appInstallDto);
                }
                infoDto.setInstallDtos(appInstallDtos);
            }
        }

        if( null != dto.getApplications()){
            JSONArray appJsonArray = JSONArray.parseArray(dto.getApplications().toString());
            if(null != appJsonArray && appJsonArray.size()>0){
                List<AppVersionDto> appVersionDtos = new ArrayList<>();
                for (Object app : appJsonArray){
                    AppVersionDto versionDto = JSONObject.toJavaObject((JSONObject) app, AppVersionDto.class);
                    appVersionDtos.add(versionDto);
                }

                AppVersionDto smartStart = new AppVersionDto();

                smartStart.setName("Smartstart");
                smartStart.setVersion(dto.getSmartstartVersion());

                appVersionDtos.add(smartStart);

                infoDto.setVersionDtos(appVersionDtos);
            }
        }

        infoDto.setFailedInstallationCount(dto.getFailedInstallationCount());
        infoDto.setOkInstallationCount(dto.getOkInstallationCount());
        infoDto.setErrorExceptionCount(dto.getErrorExceptionCount());
        infoDto.setPcid(dto.getPcid());
        infoDto.setWorkshop(dto.getWorkshop());
        infoDto.setTimestamp(dto.getTimestamp());
        infoDto.setTimestampText(dto.getTimestampText());

        return infoDto;
    }

    @Override
    public SystemHistory getOneHistory(String tag) {
        return historyMapper.getOneItemByTag(tag);
    }


    @Override
    public void saveOverview(SystemHistory item) {

        boolean existFlag = false;
        QueryWrapper<SystemOverview> wrapper = new QueryWrapper<>();
        wrapper.eq("pcid", item.getPcid()).eq("workshop", item.getWorkshop());
        SystemOverview overview = overviewMapper.selectOne(wrapper);
        if(null != overview){
            existFlag = true;
        }else{
            overview = new SystemOverview();
        }

        BeanUtil.copyProperties(item, overview);

        if(null != overview.getTimestampText()){
            try {
                //29.07.2022 14:13:23
                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                Date timeStamp = sdf.parse(overview.getTimestampText().trim());
                String ttt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(timeStamp);
                overview.setTimestampText(ttt);
                overview.setLastOnlineTime(timeStamp);
            }catch (ParseException exception){
            }
        }
        if(null == overview.getAntivirFlag()){
            overview.setAntivirFlag(FlagEnum.UNKNOWN.getValue());
        }
        if(null == overview.getFirewallFlag()){
            overview.setFirewallFlag(FlagEnum.UNKNOWN.getValue());
        }
        if(null ==  overview.getWlanFlag()){
            overview.setWlanFlag(FlagEnum.UNKNOWN.getValue());
        }
        if(null == overview.getProxyFlag()){
            overview.setProxyFlag(FlagEnum.UNKNOWN.getValue());
        }
        if(null == overview.getLastOnlineTime()){
            overview.setLastOnlineTime(item.getCollectTime());
        }
        overview.setLastUsername(item.getUsername());
        if(existFlag){
            overview.setUpdatedAt(new Date());
            overviewMapper.update(overview, wrapper);
        }else{
            overviewMapper.insert(overview);
        }
    }

    @Override
    public void saveProtocolItem(SystemHistory item) {
        JSONArray protocolItems = JSONArray.parseArray(item.getProtocolItems().toString());
        if(null != protocolItems && protocolItems.size()>0){
            List<SystemProtocolItem> dtos = new ArrayList<>();

            for (Object protocol : protocolItems){
                String str = (String) protocol;
                List<String> split = StrSplitter.split(str, ';', 0, true, false);
                SystemProtocolItem dto = new SystemProtocolItem();

                dto.setReferenceId(split.get(0).trim());
                dto.setType(split.get(1).trim());
                dto.setSeverity(split.get(2).trim());
                dto.setTag(item.getTag());
                dto.setPcid(item.getPcid());
                dto.setMessage(split.get(5));
                dto.setWorkshop(item.getWorkshop());

                if(StrUtil.isNotEmpty(split.get(6))){
                    try {
                        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
                        Date timeStamp = sdf.parse(split.get(6).trim());
                        String ttt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(timeStamp);
                        dto.setTimestampText(ttt);
                    }catch (ParseException exception){
                    }
                }
                if(null == dto.getTimestampText()){
                    dto.setTimestampText(item.getTimestampText());
                }
                dto.setCollectDate(item.getCollectDate());
                dto.setCollectTime(item.getCollectTime());
                dtos.add(dto);
            }
            protocolItemIService.saveBatch(dtos);
        }

    }

    @Override
    public void addUpgradeLog(UpgradeLogReVo vo, String tag) {

        SystemHistory en = new SystemHistory();
        en.setTag(tag);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        ClientInfoReVo clientInfoReVo = vo.getClientInfo();
        if(null != clientInfoReVo){
            BeanUtil.copyProperties(clientInfoReVo, en);
        }
        SessionReVo sessionReVo = vo.getSession();
        if(null != sessionReVo){
            en.setSessionid(sessionReVo.getId());
            en.setSessionstart(sessionReVo.getStartTime());
            en.setUsername(sessionReVo.getUserId());
            en.setTimestamp(sessionReVo.getEndTime());
            en.setTimestampText(sdf.format(new Date(sessionReVo.getEndTime())));
        }

        WorkshopReVo workshopReVo = vo.getWorkshop();
        if(null != workshopReVo){
            en.setWorkshop(workshopReVo.getId());
        }
        List<DownloadVersionInfoReVo> downLoadVersionInfos =  vo.getDownloadVersionInfos();
        List<InstallVersionInfoReVo> installVersionInfos = vo.getInstallVersionInfos();
        List<VersionInfoReVo> currentVersionInfos = vo.getCurrentVersionInfos();
        en.setApplications(JSONObject.toJSON(currentVersionInfos));
        en.setDownloadProtocol(JSONObject.toJSON(downLoadVersionInfos));
        en.setInstallationProtocol(JSONObject.toJSON(installVersionInfos));

        en.setAntivirFlag(ReportUtil.sysFlagCheck(en.getAntivirInstalled()));
        en.setProxyFlag(ReportUtil.sysFlagCheck(en.getProxies()));
        en.setFirewallFlag(ReportUtil.sysFirewallCheck((JSONObject) en.getFirewalls()));
        en.setWlanFlag(ReportUtil.sysWlanCheck((JSONArray) en.getWlanNetworks()));

        en.setCollectDate(sdf.format(new Date()));
        en.setCollectTime(new Date());

        historyMapper.save(en);
    }

    @Override
    public void addProtocolItem(UpgradeResultReVo vo) {
        SystemProtocolItem item = new SystemProtocolItem();
        DisplayUserEntity displayUserEntity = (DisplayUserEntity) SecurityUtils.getSubject().getPrincipal();
        if(null == displayUserEntity){
            throw new AuthorizationException("请登录");
        }
        UserInfoEntity userInfo = displayUserEntity.getUserInfo();

        SysUserEntity user = sysUserService.queryByUserId(userInfo.getUserId());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:sss");

        item.setCollectTime(new Date(vo.getEndTime()));
        item.setReferenceId(vo.getUpdateId());
        item.setPcid(vo.getDeviceId());
        item.setType( String.valueOf(vo.getStatusType()) );
        item.setSeverity(String.valueOf(vo.getOperateType()));
        item.setWorkshop(user.getWorkshopName());

        item.setTag(vo.getTag());
        item.setMessage( vo.getStatus() == 0 ?"success":"fail");
        item.setTimestampText(sdf.format(new Date(vo.getEndTime())));
        protocolItemIService.save(item);
    }

    @Override
    public AppsInfoDto getApplicationsInfoByUpgradeLog(String tag) {
        ClientUpgrade log = upgradeResultMapper.getOneByTag(tag);
        if(null == log){
            return null;
        }
        AppsInfoDto dto = new AppsInfoDto();
        dto.setTimestamp(log.getEndTime());
        dto.setWorkshop(log.getWorkshopName());
        dto.setPcid(log.getDeviceId());
        dto.setFailedInstallationCount(log.getFailUpdateCount());

        List<AppInstallDto> installDtos = new ArrayList<>();

        List<AppVersionDto> versionDtos = new ArrayList<>();

        List<AppUnInstallDto> unInstallDtos = new ArrayList<>();

        String type = "";
        Integer appId = 0;
        Integer versionId = 0;

        if(log.getUpdateTarget() == 0){
            type = AppTypeEnum.SOFTWARE.getValue();
            appId = log.getSoftwareId();
            versionId = log.getSoftwareCurrentVersionNumber();
        }else{
            type = AppTypeEnum.COMPONENT.getValue();
            appId = log.getComponentId();
            versionId = log.getComponentCurrentVersionNumber();
        }

        // 成功后 判断是下载安装更新
        if(log.getInstallAction() == StatusTypeEnum.install.getValue()){
            AppInstallDto installDto = new AppInstallDto();
            installDto.setInstallationResult(String.valueOf(log.getUpdateResult()));
            installDto.setInstallationMessage(log.getUpdateResult()==0? "success":"fail");
            installDto.setApplicationName("");
            installDto.setUpdateOid( log.getUpdateId());
            installDto.setUpdateVersion("");
            installDto.setAppId(appId);
            installDto.setType(type);

            installDtos.add(installDto);
            if(log.getUpdateResult()==0){
                AppVersionDto versionDto = new AppVersionDto();
                versionDto.setVersion("");
                versionDto.setName("");
                versionDto.setAppId(appId);
                versionDto.setType(type);
                versionDto.setVersionId(versionId);
                versionDtos.add(versionDto);
            }
        }else if(log.getInstallAction() == StatusTypeEnum.repair.getValue() || log.getInstallAction() == StatusTypeEnum.rollback.getValue() ){
            if(log.getUpdateResult()==0){
                AppVersionDto versionDto = new AppVersionDto();
                versionDto.setVersion("");
                versionDto.setName("");
                versionDto.setAppId(appId);
                versionDto.setVersionId(versionId);
                versionDto.setType(type);
                versionDtos.add(versionDto);
            }
        }else if(log.getInstallAction() == StatusTypeEnum.uninstall.getValue() ){

            AppUnInstallDto unInstallDto = new AppUnInstallDto();
            unInstallDto.setAppId(appId);
            unInstallDto.setUpdateOid(log.getUpdateId());
            unInstallDto.setVersion("");
            unInstallDto.setType(type);
            unInstallDto.setName("");
            unInstallDto.setResult(log.getUpdateResult().toString());
            unInstallDtos.add(unInstallDto);

        }
        dto.setUnInstallDtos(unInstallDtos);
        dto.setInstallDtos(installDtos);
        dto.setVersionDtos(versionDtos);
        return dto;
    }

    @Override
    public ClientUpgrade getOneRecord(String tag) {

        return upgradeResultMapper.getOneByTag(tag);
    }

    @Override
    public List<ClientUpgrade> queryDataByTag(String tag) {
        return upgradeResultMapper.queryDataByTag(tag);
    }

    @Override
    public void addClientInfo(ClientInfoReVo clientInfoReVo, String tag) {
        SystemHistory en = new SystemHistory();
        en.setTag(tag);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        BeanUtil.copyProperties(clientInfoReVo, en);

        if(null != clientInfoReVo.getVin()){
            en.setVinCode(clientInfoReVo.getVin());
        }

        if(null != clientInfoReVo.getSessionId()){
            en.setSessionid(clientInfoReVo.getSessionId());
        }

        if(null != clientInfoReVo.getSmartLauncherVersion() ){
            en.setSmartstartVersion(clientInfoReVo.getSmartLauncherVersion());
        }

        if(null != clientInfoReVo.getType()){
            en.setClientType(clientInfoReVo.getType());
        }
        if(null != clientInfoReVo.getMacAddr()){
            en.setMacAddress(clientInfoReVo.getMacAddr());
        }

        if(null != en.getAntivirInstalled()){
            en.setAntivirFlag(FlagEnum.YES.getValue());
        }else{
            en.setAntivirFlag(FlagEnum.NO.getValue());
        }

        if(null != clientInfoReVo.getProxies() && clientInfoReVo.getProxies().size()>0){
            en.setProxies(JSONObject.toJSON(clientInfoReVo.getProxies()));
            en.setProxyFlag(FlagEnum.YES.getValue());
        }else{
            en.setProxyFlag(FlagEnum.NO.getValue());
            en.setProxies(null);
        }

        if(null !=clientInfoReVo.getFirewalls() && clientInfoReVo.getFirewalls().size()>0){
            en.setFirewalls(JSONObject.toJSON(clientInfoReVo.getFirewalls()));
            en.setFirewallFlag(FlagEnum.YES.getValue());
        }else{
            en.setFirewallFlag(FlagEnum.NO.getValue());
            en.setFirewalls(null);
        }

        if(null != clientInfoReVo.getWlanNetworks() && clientInfoReVo.getWlanNetworks().size()>0){
            en.setWlanNetworks(JSONObject.toJSON(clientInfoReVo.getWlanNetworks()));
            en.setWlanFlag(FlagEnum.YES.getValue());
        }else{
            en.setWlanFlag(FlagEnum.NO.getValue());
            en.setWlanNetworks(null);
        }

        if(null != clientInfoReVo.getLocalIps() && clientInfoReVo.getLocalIps().size()>0){
            en.setLocalIp(clientInfoReVo.getLocalIps().get(0));
        }else{
            en.setLocalIp(null);
        }

        Date currentDate = new Date();
        en.setCollectDate(sdf.format(currentDate));
        en.setCollectTime(currentDate);
        en.setTimestamp(System.currentTimeMillis());
        en.setCreatedAt(currentDate);
        en.setUpdatedAt(currentDate);
        historyMapper.saveByClientInfo(en);

    }

}
