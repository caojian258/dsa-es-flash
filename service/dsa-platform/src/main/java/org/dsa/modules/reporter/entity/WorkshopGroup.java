package org.dsa.modules.reporter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;



@Data
@Accessors(chain = true)
@TableName("d_workshop_group")
@ToString
public class WorkshopGroup extends BaseEntity {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String name;

    private String description;

    @TableField(value = "parent_id")
    private int parentId;

    @TableField(exist = false)
    private String value;

}
