package org.dsa.common.utils;

import org.apache.commons.lang.StringUtils;

import java.util.Objects;
import java.util.Random;

/**
 * 常量
 *
 */
public class Constant {
	/** 超级管理员UserName */
	public static final String SUPER_ADMIN_USER_NAME = "DsaSuperAdmin";
    /** 超级管理员UserName */
    public static final String SUPER_ADMIN_USER_PWD = "5bd6f5195a097a0d944cd2340e488eaa9ad418f95a6c5c89ed9283a4f5d8b830";
    /** 超级管理员UserName */
    public static final String SUPER_ADMIN_USER_SALT = "1fghj4d23uh28f394h34n56335b";
    /** 诊断菜单URL */
    public static final String DIAGNOSTIC_STR = "diagnostic";
    /** 用户登陆 - 设备来源 */
    public static final String USER_SSO_EQUIPMENT = "2";
    /** 超级管理员UserName */
    public static final String ADMIN_ROLE = "SuperAdmin";

    /**
     * 当前页码
     */
    public static final String PAGE = "page";
    /**
     * 每页显示记录数
     */
    public static final String LIMIT = "limit";
    /**
     * 排序字段
     */
    public static final String ORDER_FIELD = "sidx";
    /**
     * 排序方式
     */
    public static final String ORDER = "order";
    /**
     *  升序
     */
    public static final String ASC = "asc";

    public static final String EVENT_NO_PRE="I";

    public static final String PROBLEM_EVENT_NO_PRE="P";

    public static final String PROBLEM_ATTACH_TYPE="problem";

    public static final String PROBLEM_SOLVE_TYPE="solve";

    public static final String EVENT_TYPE="event";

    public static final String REQUEST_TYPE="request";

    public static final String CHANGE_TYPE="change";

    public static final String SUB_CHANGE_TYPE="sub_change";

    /**是否需要测试 */
    public static final String IS_TEST="is_test";

    /**是否需要回退/应急预案 */
    public static final String IS_BACK="is_back";

    /**是否有关联及时系统 */
    public static final String IS_RELATE="is_relate";

    /**是否有灾备同步 */
    public static final String IS_SYNC="is_sync";

    /**
     * 组件版本状态校验
     */
    public static final Integer[] VERSION_STATUS_CHECK_ARR = new Integer[]{0,1,2,3};

    /**
     * 组件更新状态校验
     */
    public static final Integer[] COMPONENT_UPDATE_TYPE_CHECK_ARR = new Integer[]{0,1};
    public static final Integer[] STATUS_CHECK_ARR = new Integer[]{0,1};

    /**
     * 压缩文件后缀
     */
    public static final String ZIP_FILE_PREFIX=".zip";

    /**
     * 前缀
     */
    public static final String TIME_PREFIX="_";

    /**
     * 中划线
     */
    public static final String STRIKE = "-";

    /**
     * 路径分割
     */
    public static final String PATH_SPLIT="/";

    /**
     * redis车辆 信息保存时间
     */
    public static final Long VEHICLE_REDIS_EXPIRE = 1800L;
    /**
     * redis前缀  经销商关联诊断车辆
     */
    public static final String REDIS_LIST="L_";

    /**
     * redis前缀  诊断车辆同信息
     */
    public static final String REDIS_VIN="VIN_";

    /**
     * redis前缀 缓存一辆车只能一条命令执行 key sessionId value actionId
     */
    public static final String REDIS_RUN_VIN="RV_";
    /**
     * redis前缀 mqtt key mid value sessionId
     */
    public static final String REDIS_ACTION="RA_";
    /**
     * redis action run status
     */
    public static final String REDIS_ACTION_STATUS="RS_";
    /**
     * redis action run  return Data to page
     */
    public static final String REDIS_ACTION_RESULT ="RP_";

    public static final String[] ECU_ALL = {"GW","IVIBOX","ADAS","ADU","TBOX"};

    public static final String[] FUNCTION_FILTER = {"SESSION_BEGIN","SESSION_END"};

    /**
     * TSP同步品牌限制
     */
    public static final String[] TSP_SYNC_MAKER_FILTER = {"东风","重汽"};


    /**
    *诊断心跳超时（秒）
    */
    public static final long DIAGNOSTIC_TIMEOUT=180;
    /**
     * MQ 监听TOPIC
     */
    public static final String MQ_RESP_TOIPIC = "DIAGNOSTIC_RESP"+ new Random().nextInt();
    /**
     * redis前缀  缓存mq接收action id
     */
    public static final String MQ_TOPIC = "MQTOPIC_";
    /**
     * redis前缀  缓存mq接收action id 缓存60秒防重复接收
     */
    public static final Long MQ_TOPIC_EXPIRE = 60L;

    /**
     * 离线下载文件有效期
     */
    public static final Long VERSION_FILE_DOWNLOAD_TIME= 300L;

    /**
     * 语种
     */
    public static final String[] LOCALS = {"zh-CN","en-US"};

    /**
     * 脚本路径
     */
    public static final String COPY_FULL_VERSION_FILE = "/usr/local/bin/copyFullVersionFile.sh ";
    public static final String COPY_ADD_VERSION_FILE = "/usr/local/bin/copyAddVersionFile.sh ";
    public static final String UNZIP_VERSION_COPY_FILE = "/usr/local/bin/unzipVersionCopyFile.sh ";
    public static final String VERSION_FILE_MD5 = "/usr/local/bin/versionFileMd5.sh ";
    public static final String REDIS_CACHE_LANGUAGE_ZIP = "redis_cache_language_zip";
    /**
     * 基本诊断用户角色
     */
    public static final String DEFAULT_DIAGNOSTIC_USER_ROLE = "BasicDiagnosticUser";
    /**
     * 当前系统名称
     */
    public static final String SYSTEM_NAME = "DsaPlatform";

   /* public static final String COPY_FULL_VERSION_FILE = "/usr/local/bin/copyFullVersionFile.bat ";
    public static final String COPY_ADD_VERSION_FILE = "/usr/local/bin/copyAddVersionFile.bat ";
    public static final String UNZIP_VERSION_COPY_FILE = "/usr/local/bin/unzipVersionCopyFile.bat ";
    public static final String ADD_VERSION_COPY_FILE_MD5 = "/usr/local/bin/addVersionCopyFileMd5.bat ";*/
    /**
     * 签名加密 拼接符号
     */
    public static String SECRET_JOIN = "&";
    /**
     * redis同步离线日志唯一性key
     */
    public static final String REDIS_SYN_LOG_Key = "syn_log";

    /**
     * 登录来源
     */
    public enum LoginSource {
        SERVER_LOGIN(0,"server"),
        CLIENT_LOGIN(1,"client");
        LoginSource(Integer type, String desc) {
            this.type = type;
            this.desc = desc;
        }
        private Integer type;
        private String desc;

        public Integer getType() {
            return type;
        }

        public void setType(Integer type) {
            this.type = type;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }
    }

    /**
     * 诊断会话状态
     */
    public enum Function {
        BEGIN("开始诊断","SESSION_BEGIN",60L,30L),
        VHSS("车辆健康概况","VHSS",180L,100L),
        IDENTIFICATION("版本信息","IDENTIFICATION",180L,100L),
        DTC("故障码","DTC",180L,100L),
        END("结束诊断","SESSION_END",60L,30L),
        CALIBRATION("标定","CALIBRATION",180L , 240L),
        REPORT("车辆报告","REPORT",180L , 240L);

        private String functionName;
        private String functionCode;
        private Long expire;
        private Long midExpire;

        private Function( String functionName, String functionCode,Long expire,Long midExpire) {
            this.functionName = functionName;
            this.functionCode = functionCode;
            this.expire = expire;
            this.midExpire = midExpire;
        }

        //判断functionCode是否有效的
        public static boolean isFunction(String functionCode) {
            boolean r = false;
            for(Function f : values()){
                if(f.getFunctionCode().equals(functionCode) ){
                    r =true;
                    break;
                }
            }
            return r;
        }

        public static String getFunctionNameByCode(String functionCode) {
            String fn = "";
            if(StringUtils.isNotEmpty(functionCode)) {
                for (Function f : values()) {
                    if (f.getFunctionCode().equals(functionCode)) {
                        fn = f.functionName;
                    }
                }
            }
            return fn;
        }
        public static Long getExpireByCode(String functionCode) {
            Long expire = 0L;
            if(StringUtils.isNotEmpty(functionCode)) {
                for (Function f : values()) {
                    if (f.getFunctionCode().equals(functionCode)) {
                        expire = f.getExpire();
                    }
                }
            }
            return expire;
        }
        public String getFunctionName() {
            return functionName;
        }
        public String getFunctionCode() {
            return functionCode;
        }
        public Long getExpire() { return expire; }
        public Long getMidExpire() { return midExpire; }
    }
    /**
     * 诊断操作码
     */
    public enum Action {
        BEGIN("开始诊断","startRemote"),
        VHSS_INIT("'查询","init"),
        IDENTIFICATION_INIT("'初始化","init"),
        DTC_READ("读取故障码","read"),
        DTC_SNAPSHOT("读取冻结帧","snapshot"),
        DTC_CLEARDTC("清除故障码","cleardtc"),
        END("结束诊断","stopRemote"),
        EXECUTE("标定执行","execute"),
        REPORT("车辆报告","init");


        private String actionName;
        private String actionCode;

        private Action( String actionName, String actionCode) {
            this.actionName = actionName;
            this.actionCode = actionCode;
        }
        public String getActionName() {return actionName; }
        public String getActionCode() {
            return actionCode;
        }

        //判断functionCode是否有效的
        public static boolean isAction(String actionCode) {
            boolean r = false;
            for(Action a : values()){
                if(a.getActionCode().equals(actionCode) ){
                    r =true;
                    break;
                }
            }
            return r;
        }
    }
    /**
     * 诊断会话状态
     */
    public enum SESSION_STATUS{
        ACTIVE(0,"已激活","ACTIVE"),
        PROCES(1,"诊断中","PROCES"),
        FINISHED(2,"断开结束","FINISHED"),
        DECONNECT(3,"连接已断开","DECONNECT");

        private int value;
        private String name;
        private String dbName;
        private SESSION_STATUS(int value, String name, String dbName) {
            this.value = value;
            this.name = name;
            this.dbName = dbName;
        }
        public int getValue() {
            return value;
        }
        public String getName() {
            return name;
        }
        public String getDbName() {
            return dbName;
        }
    }
    /**
     * 诊断会话-功能执行状态
     */
    public enum RESULTS {
        SEND(0, "发送", "SEND"),
        SUCCESS(1, "成功", "SUCCESS"),
        FAIL(2, "失败", "FAIL"),
        TIMEOUT(3, "执行超时", "TIMEOUT"),
        TIMEOUT_SUCCESS(4, "执行超时(执行完成)", "TIMEOUT_SUCCESS"),
        TIMEOUT_FAIL(5, "执行超时(执行失败)", "TIMEOUT_FAIL");


        private int value;
        private String disName;
        private String name;

        private RESULTS(int value, String disName, String name) {
            this.value = value;
            this.disName = disName;
            this.name = name;
        }

        public int getValue() {
            return value;
        }

        public String getDisName() {
            return disName;
        }

        public String getName() {
            return name;
        }

        public static String getName(int value) {
            for (RESULTS a : RESULTS.values()) {
                if (a.getValue() == value) {
                    return a.getName();
                }
            }
            return null;
        }

        public static String getDisName(int value) {
            for (RESULTS a : RESULTS.values()) {
                if (a.getValue() == value) {
                    return a.getDisName();
                }
            }
            return null;
        }
    }
    /**
     * 诊断数据来源
     */
    public enum SESSION_SOURCE {
        VEHICLE("001","车载诊断仪","VEHICLE"),
        OFFLINE("002","离线诊断仪","OFFLINE"),
        REMOTE("003","远程诊断仪","REMOTE");

        private String value;
        private String name;
        private String dbName;
        private SESSION_SOURCE(String value, String name, String dbName) {
            this.value = value;
            this.name = name;
            this.dbName = dbName;
        }
        public String getValue() {
            return value;
        }
        public String getName() {
            return name;
        }
        public String getDbName() {
            return dbName;
        }
        //通过dbName获取name
        public static String getNameByDbName(String dbName) {
            String name = "";
            for (SESSION_SOURCE s : values()) {
                if (s.getDbName().equals(dbName)) {
                    name = s.getName();
                    break;
                }
            }
            return name;
        }

        // isSessionSource()
        public static boolean isSessionSourceByDbName(String dbName) {
            boolean r = false;
            for (SESSION_SOURCE s : values()) {
                if (s.getDbName().equals(dbName)) {
                    r = true;
                    break;
                }
            }
            return r;
        }

        //通过dbName获取name
        public static String getDbNameByValue(String value) {
            String name = "";
            for (SESSION_SOURCE s : values()) {
                if (s.getValue().equals(value)) {
                    name = s.getDbName();
                    break;
                }
            }
            return name;
        }

        // isSessionSource()
        public static boolean isSessionSourceByValue(String value) {
            boolean r = false;
            for (SESSION_SOURCE s : values()) {
                if (s.getValue().equals(value)) {
                    r = true;
                    break;
                }
            }
            return r;
        }
    }
    /**
     * 诊断会话-功能执行状态
     */
    public enum ACK {
        ACK(1,"响应","ACK"),
        FIN(2,"执行完成","FIN"),
        NAK(4,"执行失败","NAK"),
        TIMEOUT(8,"执行超时","TIMEOUT"),
        DATA(16,"响应数据","DATA"),

        TIMEOUT_TIMEOUT(128,"执行超时","TIMEOUT_TIMEOUT"),

        TIMEOUT_ACK(128+1,"执行超时(已发送)","TIMEOUT_ACK"),
        TIMEOUT_FIN(128+2,"执行超时(执行完成)","TIMEOUT_FIN"),
        TIMEOUT_NAK(128+4,"执行超时(执行失败)","TIMEOUT_NAK");


        private int value;
        private String disName;
        private String name;
        private ACK(int value, String disName, String name) {
            this.value = value;
            this.disName = disName;
            this.name = name;
        }
        public int getValue() {
            return value;
        }
        public String getDisName() {
            return disName;
        }
        public String getName() {return name;}
        public static String getName(int value){
            for(Constant.ACK a : Constant.ACK.values()){
                if(a.getValue() == value){
                    return a.getName();
                }
            }
            return null;
        }
        public static String getDisName(int value){
            for(Constant.ACK a : Constant.ACK.values()){
                if(a.getValue() == value){
                    return a.getDisName();
                }
            }
            return null;
        }
    }
    /**
     * Mq 接收消息 - 数据来源
     */
    public enum MQ_MESSAGE_SOURCE {
        VEHICLE("001","车载售后诊断仪"),
        OFFLINE("002","离线售后诊断仪"),
        REMOTE("003","远程售后诊断仪");

        private String value;
        private String name;
        private MQ_MESSAGE_SOURCE(String value, String name) {
            this.value = value;
            this.name = name;
        }
        public String getValue() {
            return value;
        }
        public String getName() {
            return name;
        }
    }

    /**
     * 签名加密
     */
    public enum SYSTEM_SECRET{
        OFF_LINE("10000001","ge453r98derfjk2q321m54n1x09m123c"),
        OFF_LINE_REPORTING("10000002","89342qjk4lrhn78ugfvy890dx345jk");

        private String systemId;
        private String secret;
        private SYSTEM_SECRET(String systemId, String secret) {
            this.systemId = systemId;
            this.secret = secret;
        }

        public static String getSecretBySystemId(String systemId) {
            if(StringUtils.isNotEmpty(systemId)){
                for(SYSTEM_SECRET s : SYSTEM_SECRET.values()){
                    if(systemId.equals(s.getSystemId())){
                        return s.getSecret();
                    }
                }
            }
            return null;
        }

        public String getSystemId() {
            return systemId;
        }
        public String getSecret() {
            return secret;
        }
    }


    /**
     * 车型信息
     */
    public enum VEHICLE_TYPE {
        MAKER("VY_MAKER_",1),
        MODEL("VY_MODEL_",2),
        PLATFORM("VY_PLATFORM_",3),
        YEAR("VY_YEAR_",4),
        TRIM_LEVEL("VY_TRIM_LEVEL_",5);


        private String name;
        private int level;
        private VEHICLE_TYPE(String name, int level) {
            this.name = name;
            this.level = level;
        }
        public String getName() {
            return name;
        }
        public int getLevel() {
            return level;
        }

    }

    /**
     * Vehicle Type have_version
     */
    public enum VEHICLE_TYPE_HAVE_VERSION {
        IN("IN",1),
        NOT_ID("NOT_ID",0);

        private String name;
        private int value;
        private VEHICLE_TYPE_HAVE_VERSION(String name, int value) {
            this.name = name;
            this.value = value;
        }
        public String getName() {
            return name;
        }
        public int getValue() {
            return value;
        }

    }

    /**
     * Version Type
     */
    public enum versionType {
        ODX("ODX",1),
        OTX("OTX",2);

        private String name;
        private int type;
        private versionType(String name, int type) {
            this.name = name;
            this.type = type;
        }
        public String getName() {
            return name;
        }
        public int getType() {
            return type;
        }

    }

    /**
     * Version Type
     */
    public enum versionStatus {
        DEV("DEV",1),
        TEST("TEST",2),
        CLOSE("CLOSE",2),
        OPEN("OPEN",2);

        private String code;
        private int value;
        private versionStatus(String code, int value) {
            this.code = code;
            this.value = value;
        }
        public String getCode() {
            return code;
        }
        public int getValue() {
            return value;
        }

    }

    /**
     * Version Type
     */
    public enum editType {
        ADD("ADD","新增"),
        EDIT("EDIT","修改");

        private String code;
        private String name;
        private editType(String code, String name) {
            this.code = code;
            this.name = name;
        }
        public String getCode() {
            return code;
        }
        public String getName() {
            return name;
        }


    }
    /**
     * Version Type
     */
    public enum fileType {
        ADD("1","增量"),
        FULL("0","全量");

        private String code;
        private String name;
        private fileType(String code, String name) {
            this.code = code;
            this.name = name;
        }
        public String getCode() {
            return code;
        }
        public String getName() {
            return name;
        }


    }
    /**
     * Version Type
     */
    public enum FILE_DIR_CHECK {
        ODX("INCEPTIO","INCEPTIO"),
        OTX("on board","off board");

        private String vehicle;
        private String offline;
        private FILE_DIR_CHECK(String vehicle, String offline) {
            this.vehicle = vehicle;
            this.offline = offline;
        }
        public String getVehicle() {
            return vehicle;
        }
        public String getOffline() {
            return offline;
        }


    }

    /**
     * Session End status
     */
    public enum SESSION_END_STATUS {
        INITIATIVE_TO_END("主动结束",0),
        PASSIVE_END("被动结束",1);

        private String msg;
        private Integer code;
        private SESSION_END_STATUS(String msg, Integer code) {
            this.msg = msg;
            this.code = code;
        }

        public Integer getCode() {
            return code;
        }
    }

    /**
     * vehicle source
     */
    public enum VEHICLE_SOURCE {
        /**
         *
         */
        TSP_SYNC("TSP同步",1),
        REMOTE("远程服务器",2);

        private String msg;
        private Integer code;
        private VEHICLE_SOURCE(String msg, Integer code) {
            this.msg = msg;
            this.code = code;
        }

        public Integer getCode() {
            return code;
        }
    }


    /**
	 * 菜单类型
	 */
    public enum MenuType {
        /**
         * 目录
         */
    	CATALOG(0),
        /**
         * 菜单
         */
        MENU(1),
        /**
         * 按钮
         */
        BUTTON(2);

        private int value;

        MenuType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    /**
     * 定时任务状态
     */
    public enum ScheduleStatus {
        /**
         * 正常
         */
    	NORMAL(0),
        /**
         * 暂停
         */
    	PAUSE(1);

        private int value;

        ScheduleStatus(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    /**
     * 云服务商
     */
    public enum CloudService {
        /**
         * 七牛云
         */
        QINIU(1),
        /**
         * 阿里云
         */
        ALIYUN(2),
        /**
         * 腾讯云
         */
        QCLOUD(3);

        private int value;

        CloudService(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
    /**
     * 部门名称
     */
    public enum WorkshopName{
        IT("信息技术部");

        private String dbName;

        private WorkshopName(String dbName){
            this.dbName=dbName;
        }
        public String getDbName() {
            return dbName;
        }
    }

    /**
     * 组类别
     */
    public enum GroupCategory{
        CHANGE_GROUP("change_group"),
        SUPPORT_GROUP("support_group");

        private String dbName;

        private GroupCategory(String dbName){
            this.dbName=dbName;
        }
        public String getDbName() {
            return dbName;
        }
    }
    /**
     * 软件id
     */
    public enum SOFTWARE{
        PRODIS_SWTS("Prodis.SWTS",100);
        SOFTWARE(String name,Integer code) {
            this.name = name;
            this.code = code;
        }
        private String name;
        private Integer code;
    }

    public enum STATUS{
        NORMAL("normal",0),
        DISABLED("disabled",1);
        STATUS(String content, Integer code) {
            this.content = content;
            this.code = code;
        }

        public String getContent() {
            return content;
        }

        public Integer getCode() {
            return code;
        }

        private String content;
        private Integer code;
    }

    /**
     * 组件枚举
     */
    public enum COMPONENT{
        PDU_API("PDU-Api",200),
        MCD_KERNEL("Mcd-Kernel",201),
        SWTS_CORE("SWTS core",202),
        L18N("l18n",203),
        OTX_ENGINE("Otx_Engine",204),
        CONFIG("Config",205),
        SWTS_HMI("SWTS HMI",206);

        COMPONENT(String name, Integer code) {
            this.name = name;
            this.code = code;
        }

        private String name;
        private Integer code;
    }
    /**
     * 版本状态
     */
    public enum VERSION_STATUS{
        DEV("开发","DEV",0),
        TEST("测试","TEST",1),
        RELEASE("释放","RELEASE",2),
        REFUSE("拒绝","REFUSE",3);
        VERSION_STATUS(String content, String code, Integer codeInt) {
            this.code = code;
            this.content = content;
            this.codeInt = codeInt;
        }

        private String code;
        private String content;
        private Integer codeInt;

        public String getCode() {
            return code;
        }

        public String getContent() {
            return content;
        }
        public Integer getCodeInt() {
            return codeInt;
        }

        public static boolean checkStatus(Integer status){
            boolean r = false;
            for (VERSION_STATUS value : values()) {
                if (Objects.equals(status, value.getCodeInt())){
                    r = true;
                    break;
                }
            }
            return r;
        }
    }

    /**
     * 软件更新状态
     */
    public enum COMPONENT_UPDATE_TYPE{

        OPTIONAL_UPDATE("可选更新","optional",1),
        FORCE_UPDATE("必选更新","required",0);

        COMPONENT_UPDATE_TYPE(String content, String code,Integer codeInt) {
            this.code = code;
            this.content = content;
            this.codeInt = codeInt;
        }

        private String code;
        private String content;
        private Integer codeInt;

        public String getCode() {
            return code;
        }

        public String getContent() {
            return content;
        }
        public Integer getCodeInt() {
            return codeInt;
        }

        public static Integer getCodeIntByCode(String code){
            Integer res = null;
            for (COMPONENT_UPDATE_TYPE value : values()) {
                if (value.getCode().equals(code)){
                    res = value.getCodeInt();
                    break;
                }
            }
            return res;
        }
    }

    /**
     * 软件更新状态
     */
    public enum SOFTWARE_UPDATE_TYPE{

        OPTIONAL_UPDATE("可选更新","optional",1),
        FORCE_UPDATE("强制更新","forced",0);

        SOFTWARE_UPDATE_TYPE(String content, String code,Integer codeInt) {
            this.code = code;
            this.content = content;
            this.codeInt = codeInt;
        }

        private String code;
        private String content;
        private Integer codeInt;

        public String getCode() {
            return code;
        }

        public String getContent() {
            return content;
        }
        public Integer getCodeInt(){
            return codeInt;
        }

        public static Integer getCodeIntByCode(String code){
            Integer res = null;
            for (SOFTWARE_UPDATE_TYPE value : values()) {
                if (value.getCode().equals(code)){
                    res = value.getCodeInt();
                    break;
                }
            }
            return res;
        }
    }

    public enum CROSS_VERSION{
        CROSS("cross","可跨版本",0),
        NO_CROSS("noCross","不可跨版本",1);

        private String content;
        private String code;
        private Integer codeInt;

        CROSS_VERSION(String content, String code, Integer codeInt) {
            this.content = content;
            this.code = code;
            this.codeInt = codeInt;
        }

        public String getCode() {
            return code;
        }

        public String getContent() {
            return content;
        }
        public Integer getCodeInt(){
            return codeInt;
        }

        public static Integer getCodeIntByCode(String code){
            Integer res = null;
            for (CROSS_VERSION value : values()) {
                if (value.getCode().equals(code)){
                    res = value.getCodeInt();
                    break;
                }
            }
            return res;
        }

    }


    /**
     * MESSAGE
     */
    public enum Msg{
        ACCOUNT_NOT_LOGIN(401,"token无效,请重新登陆"),
        ACCOUNT_LOCK_ERROR(492,"账号已被锁定,请联系管理员"),
        ACCOUNT_OR_PASSWORD_ERROR(493,"账号或密码错误,请联系管理员"),
        SERVER_REDIS_FAIL(480,"Redis服务异常"),
        LOGIN_USER_LOGIN_COUNT_UNNORMAL(481, "已超过最大输入次数，24小时后请重新尝试"),
        PASSWORD_FORMAT_ERROR(482,"密码必须由字母或数组组成的6-16位的字符串"),
        UPDATE_PASSWORD_ERROR(483, "修改密码失败，只能修改自己的密码！"),
        ORIGINAL_PASSWORD_ERROR(488,"原密码不正确，请输入正确的密码！" ),
        ACTION_RUN_TIMEOUT(490,"诊断命令执行超时"),
        ACTION_RUN_FAIL(491,"诊断命令执行失败"),
        LOGIN_FAILED(493,"密码错误！"),
        ACTION_RUN_ING(499,"诊断命令执行中"),
        SERVER_INTERNAL_EXCEPTION(500,"服务器内部异常"),
        TOKEN_EXPIRE_ERROR(501,"token失效请重新登录！"),
        USER_NO_WORKSHOP_ERROR(504,"用户经销商有误，请联系管理员"),
        NAME_EXIST_ERROR(505,"名称已存在，请修改后重新提交" ),
        USERNAME_EXIST_ERROR(506,"员工号已存在，请修改后重新提交" ),
        ROLE_NAME_EXIST_ERROR(507,"角色名称已存在，请修改后重新保存！"),
        NO_OPERATE_AUTH(508,"没有操作权限"),
        ACCOUNT_GET_USER_INFO_FAIL(509,"根据token获取用户失败"),
        LOGIN_FAL(510,"登陆失败"),
        CHECK_FUNCTION(511,"FunctionCode不在诊断规定范围"),
        CHECK_ACTION(512,"ActionCode不在诊断规定范围内"),
        CHECK_INPUT(513,"入参校验失败"),
        SAVE_DATA(514,"数据保存失败"),
        CHECK_VIN(515,"VIN码无效"),
        CHECK_ACTIVE_USER(516,"非诊当前远程诊断车辆用户,无法结束诊断!"),
        CHECK_BEGIN_USER(517,"当前车辆,正在被其它用户远程诊断!"),
        CHECK_ORDER_NUM(518,"当前层级已存在该排序号，请修改后重新保存！"),
        CHECK_HAS_VEHICLE(519, "该车型或下级车型已被车辆绑定，请删除相关车辆后重试！"),
        CHECK_HAS_CHILD(520, "该车型下存在子节点，请删除子节点后重试！"),
        CHECK_HAS_NAME(521, "车型名称已存在请更改后重新保存！"),

        CHECK_VIN_HAS_TYPE(543, "车辆无有效的车型信息！"),
        CHECK_SYS_ROLE(544,"新增用户所选角色，不是本人创建!"),
        CHECK_GET_SSO_USER_FILE(545,"获取嬴彻用户信息失败!"),

        CHECK_GET_REMOTE_USER_FILE(546,"获取当前远程用户信息失败!"),

        FILE_ZIP_LIMIT(592,"只支持zip类型的文件"),
        FILE_XML_LIMIT(593,"只支持xml类型的文件"),
        FILE_DB_LIMIT(594,"只支持db类型的文件"),
        FILE_PDX_R_LIMIT(595,"只支持pdx-r类型的文件"),
        FILE_NOT_EXIST(596,"文件不存在,请先上传文件!"),
        FILE_UPLOAD_ERROR(597,"文件上传失败,请稍候再试!"),
        FILE_DOWNLOAD_ERROR(598,"文件下载失败,请稍候再试!"),
        EXCEL_EXPORT_ERROR(599,"excel导出失败,请稍候再试!"),
        EXCEL_IMPORT_ERROR(600,"excel导入失败，,请稍候再试!"),
        DIA_RUN(601,"该车辆已有诊断请求正在处理,请稍候再试!"),
        DIA_DEC(602,"该车辆已断开连接,请重新激活!"),
        DIA_END_DEC(603,"该车辆已断开连接!"),
        DIA_CAN_ACTIVE(604,"该车辆已断开连接,请重新激活!"),
        DIA_BEING_STATUS_CONT_ACTIVE(605,"车辆状态不正确不能开始诊断!"),
        DIA_LOGGER_FILE_UPLOAD_E(606,"文件上传失败,请重新上传!"),


       TEMPLATE_FILE_EMPTY(610,"模板文件不存在，请联系系统管理员！"),


        VERSION_EMPTY(680,"无匹配版本信息!"),
        VERSION_NUMBER_EMPTY(681,"版本号不存在!"),
        VERSION_NOT_UPDATE(682,"已最新版本,无需更新!"),
        VERSION_FILE_ERROR(683,"文件上传失败,请重新上传文件!"),
        VERSION_FILE_FAIL(684,"上传文件,目录校验失败,请重新上传正确的版本文件!"),
        VERSION_FILE_DOWNLOAD_TRACE_FAIL(686,"文件下载失败!"),
        VERSION_CHECK_TYPE(687,"版本类型不能为空!"),
        NO_LOG_FILE(688, "该条诊断暂无日志文件！"),
        VERSION_CHECK_STATUS(689,"历史版本状态是开发或测试的数据,请修改版本状态后再新增版本!"),
        VERSION_MACK_FILE_FAIL(690,"版本文件生成下载版本发生错误!"),
        VERSION_MACK_ZIP_FILE_FAIL(691,"生成版本文件出错,请重试或软件供应商!"),
        NO_SUPPORT_LOCALE(700, "暂不支持的语种或表头异常！"),
        VERSION_CHECK_TYPE_FAIL(701,"版本类型不正确!"),
        MESSAGE_692(692,"请检查员工号是否重复!"),
        MESSAGE_693(693,"系统管理员无法删除!"),
        MESSAGE_694(694,"当前用户不能删除!"),
        MESSAGE_695(695,"无法修改本人信息!"),
        MESSAGE_696(696,"读取文件失败,请重试!"),

        VEH_MISS(800, "未查到车辆信息"),
        DID_MISS(801, "缺少ECU DID 信息"),
        VEH_VERSION_MISS(802, "未找到整车版本"),
        ECU_VERSION_MISS(803, "未找到ECU版本"),

        ECU_VEH_VERSION_MISS(804, "整车版本不存在"),


        ECU_VEH_VERSION_STATUS(805, "整车版本不存在或者未释放"),

        VEH_VERSION_MISS_MATCH(806, "整车版本与车型不匹配"),

        WORKSHOP_LOCATION_MISS( 807,"地区不能为空"),

        UNKNOWN_VERSION_STATUS_ERROR(1002, "未知的版本状态"),
        COMPONENT_DISABLED_ERROR(1003, "该组件的组件版本被其他组件版本依赖，请取消组件版本依赖，再进行禁用操作！"),
        NAME_DUPLICATION_ERROR(1004, "name已存在，请修改后重新提交"),
        CLIENT_PARAM_ERROR(1005, "缺少必要参数！"),

        CHECK_ID_ERROR(1012, "id不能为空"),
        CHECK_NAME_ERROR(1013, "name不能为空,或空字符串"),
        CHECK_STATUS_ERROR(1014, "status不能为空"),
        DATA_INFO_NULL_ERROR(1015, "数据详情不存在"),
        SOFTWARE_RELEASE_ERROR(1018, "用户软件版本必须包含组件版本"),
        OEM_NO_BIND_SOFTWARE_ERROR(1023, "OEM未关联版本"),
        FILE_PARAM_ERROR(1025, "文件参数不完整！"),
        SOFTWARE_LAST_UPDATE_DATE_ERROR(1026, "软件最后更新时间不能为空"),
        SOFTWARE_LAST_UPDATE_TIPS_DAYS_ERROR(1027, "软件最后更新提醒时间不能小于1"),
        UNKNOWN_STATUS_ERROR(1028, "未知的状态"),
        ISCROSS_NULL_ERROR(1029, "软件是否可跨版本不能为空！"),
        NO_BIND_SOFTWARE_ERROR(1035, "规则绑定软件错误"),
        CHECK_START_TIME_ERROR(1036, "开始时间不能为空"),
        CHECK_END_TIME_ERROR(1037, "结束时间不能为空"),

        VERSION_DOWNLOAD_PARAM_ERROR(1039, "参数异常，下载失败！"),
        VERSION_NO_BIND_ERROR(1040, "当前版本未绑定组件(软件)！"),
        VERSION_BLANK_ERROR(1041, "版本不能为空"),
        VERSION_URL_BLANK_ERROR(1042, "版本文件不存在，请重新上传！"),
        VERSION_DEV_STATUS_ERROR(1043, "版本状态为开发状态时，只能更改为测试状态"),
        VERSION_RELEASE_STATUS_ERROR(1044, "版本状态为释放状态时，只能更改为拒绝状态"),
        UNKNOWN_COMPONENT_UPDATE_TYPE_ERROR(1045, "未知的组件更新类型错误"),
        CODE_DUPLICATION_ERROR(1046, "code重复，请修改后重新保存！"),
        CHECK_CODE_ERROR(1047, "code入参异常"),
        RELATED_VERSION_CONFLICT_ERROR(1048, "当前组件版本与已关联的组件版本有冲突，请选择该组件的其他版本！"),
        EntryNullError(1049, "软件版本无入口信息"),
        SCRIPT_NAME_BLANK_ERROR(1050, "名称不能为空！"),
        VERSION_TYPE_ERROR(1051,"版本类型不能为空！" ),
        VERSION_CREATE_ERROR(1052, "当前存在开发或测试的版本，不可创建新版本！");
        private int code;
        private String message;

        private Msg(int code ,String message){
            this.code=code;
            this.message=message;
        }

        public int getCode() {
            return code;
        }
        public String getMessage() {
            return message;
        }

        public static String getMessage(int code) {
            for(Msg m : Msg.values()){
                if(m.getCode() == code){
                    return m.getMessage();
                }
            }
            return null;
        }

    }



}
