package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.util.List;

@Data
public class ExecutedCtsDto {

    private List<SessionCts> cts;

}
