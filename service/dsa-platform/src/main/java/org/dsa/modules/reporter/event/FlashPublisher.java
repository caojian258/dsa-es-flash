package org.dsa.modules.reporter.event;

import org.dsa.modules.reporter.vo.api.SyncFlashLogRevo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class FlashPublisher {

    @Autowired
    private ApplicationEventPublisher publisher;

    public void syncResultAsync(SyncFlashLogRevo vo) {

        publisher.publishEvent(new FlashEvent(this, vo));
    }
}
