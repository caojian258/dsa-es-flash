package org.dsa.modules.reporter.vo.diag;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.modules.reporter.pojo.PageParam;

import java.util.List;


@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class DtcPageReVo extends PageParam {

    private List<String> dates;

    private String startTime;

    private String endTime;

    private String vin;

    private List<Long> vehType;

    private Integer top;

    private List<String> ecuName;

    private String dtcCode;

    private List<String> source;
}
