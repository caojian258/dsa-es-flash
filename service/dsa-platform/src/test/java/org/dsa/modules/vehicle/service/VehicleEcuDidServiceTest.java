package org.dsa.modules.vehicle.service;

import junit.framework.TestCase;
import org.dsa.modules.vehicle.entity.VehicleEcuDid;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
public class VehicleEcuDidServiceTest extends TestCase {

    @Autowired
    VehicleEcuDidService vehicleEcuDidService;

    @Test
    public void testBatchInsertByEcu(){
        List<VehicleEcuDid> didList = new ArrayList<>();
        VehicleEcuDid did = new VehicleEcuDid();
        did.setVehicleTypeId(310L);
        did.setModel("QRPNWHCC0000");
        did.setEcuName("BMS");
        did.setEcuDescription("BMS CFC");
        did.setDid("F1AA");
        did.setDidName("BMS CFC");
        did.setDidValue("200OK");
        didList.add(did);

        VehicleEcuDid did2 = new VehicleEcuDid();
        did2.setVehicleTypeId(310L);
        did2.setModel("QRPNWHCC0000");
        did2.setEcuName("BMS");
        did2.setEcuDescription("BMS");
        did2.setDid("F189");
        did2.setDidName("PartNumber");
        did2.setDidValue("1001");

        didList.add(did2);

        vehicleEcuDidService.batchInsertByEcu(310L,"BMS",didList);
    }

    @Test
    public void testOneInsert() {
        VehicleEcuDid did = new VehicleEcuDid();
        did.setVehicleTypeId(310L);
        did.setModel("QRPNWHCC0000");
        did.setEcuName("GW");
        did.setEcuDescription("网关");
        did.setDid("F189");
        did.setDidName("PartNumber");
        did.setDidValue("0000");
        vehicleEcuDidService.oneInsert(did);
    }

    @Test
    public void testBatchInsert() {
        List<VehicleEcuDid> didList = new ArrayList<>();
        VehicleEcuDid did = new VehicleEcuDid();
        did.setVehicleTypeId(310L);
        did.setModel("QRPNWHCC0000");
        did.setEcuName("BMS");
        did.setEcuDescription("BMS DESC");
        did.setDid("F180");
        did.setDidName("Software Version");
        did.setDidValue("200");
        didList.add(did);

        VehicleEcuDid did2 = new VehicleEcuDid();
        did2.setVehicleTypeId(310L);
        did2.setModel("QRPNWHCC0000");
        did2.setEcuName("VCD");
        did2.setEcuDescription("VCD");
        did2.setDid("F189");
        did2.setDidName("PartNumber");
        did2.setDidValue("1001");

        didList.add(did2);

        vehicleEcuDidService.batchInsert(didList);
    }

    @Test
    public void testDeleteByVehicleTypeEcuDid() {
        vehicleEcuDidService.deleteByVehicleTypeEcuDid(310L, "BMS", "F189");
    }

    @Test
    public void testDeleteByVehicleTypeEcu() {
        vehicleEcuDidService.deleteByVehicleTypeEcu(310L, "GW");
    }

    public void testDeleteByVehicleType() {
    }
}