package org.dsa.modules.oem.service;

import org.dsa.modules.oem.dto.SyncVehicleInfoDTO;

import java.util.List;

/**
 * @author weishunxin
 * @since 2023-05-30
 */
public interface SyncService {

    /**
     *
     * 同步车辆信息
     *
     * @param vehicleList vehicleList
     */
    void syncVehicleInfo(List<SyncVehicleInfoDTO> vehicleList);

}
