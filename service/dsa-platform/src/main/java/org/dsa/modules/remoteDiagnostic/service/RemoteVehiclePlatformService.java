//package org.dsa.modules.remoteDiagnostic.service;
//
//import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
//import org.dsa.modules.remoteDiagnostic.entity.RemoteVehiclePlatformEntity;
//import org.dsa.modules.remoteDiagnostic.vo.VehiclePlatformPageReVo;
//
//import java.util.List;
//
//public interface RemoteVehiclePlatformService {
//
//    Page<RemoteVehiclePlatformEntity> selectPage(VehiclePlatformPageReVo vo);
//
//    RemoteVehiclePlatformEntity getInfo(Long id);
//
//    Long savePlatform(RemoteVehiclePlatformEntity ecu);
//
//    Long updatePlatform(RemoteVehiclePlatformEntity ecu);
//
//    List<RemoteVehiclePlatformEntity> getList();
//
//    // 检查平台是否重复
//    Boolean check(String name);
//}
