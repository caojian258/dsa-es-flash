package org.dsa.modules.reporter.job;

import lombok.extern.slf4j.Slf4j;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.reporter.constant.RedisConstant;
import org.dsa.modules.reporter.service.DiagnosticStaticService;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/***
 * 实时统计dtc/cts 每天的次数
 */
@Slf4j
@Component
@DisallowConcurrentExecution
public class DiagnosticStaticJob extends QuartzJobBean {

    @Autowired
    DiagnosticStaticService diagnosticStaticService;

    @Autowired
    RedisUtils redisUtils;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.info("DiagnosticStaticJob start ");

        Long size = redisUtils.lGetListSize(RedisConstant.TAG_DIAG_KEY);

        log.info("diag tag pool size: {}", size);

        while (size>0) {

            String tag = (String) redisUtils.lPop(RedisConstant.TAG_DIAG_KEY);
            if (StringUtils.isEmpty(tag)) {
                size--;
                continue;
            }
            //cts source:vin:cts:day
            log.info("diag static job cts daily: {}", tag);
            diagnosticStaticService.ctsDaily(tag);

            //dtc vin:ecu:dtc:dtc_int:dtc_hex:ti:day
            log.info("diag static job dtc daily: {}", tag);
            diagnosticStaticService.dtcDaily(tag);
        }

        log.info("DiagnosticStaticJob end ");
    }
}
