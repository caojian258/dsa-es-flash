package org.dsa.modules.sys.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.PageUtils;
import org.dsa.common.utils.Query;
import org.dsa.modules.sys.dao.SysGroupDao;
import org.dsa.modules.sys.entity.SysGroupEntity;
import org.dsa.modules.sys.service.SysGroupService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


@Service("sysGroupService")
public class SysGroupServiceImpl extends ServiceImpl<SysGroupDao, SysGroupEntity> implements SysGroupService {
	
//	@Override
//	@DataFilter(subWorkshop = true, user = false, tableAlias = "t1")
//	public List<SysGroupEntity> queryList(Map<String, Object> params){
//		return baseMapper.queryList(params);
//	}

	@Override
	public List<SysGroupEntity> querySelectList(){
		return this.baseMapper.querySelectList();
	}

	@Override
	public PageUtils queryPage(Map<String, Object> params) {
		String groupName = (String)params.get("groupName");


		IPage<SysGroupEntity> page = this.page(
				new Query<SysGroupEntity>().getPage(params),
				new QueryWrapper<SysGroupEntity>()
						.like(StringUtils.isNotBlank(groupName),"name", groupName)
						.eq("del_flag", "0")
		);

		return new PageUtils(page);
	}

	@Override
	public List<SysGroupEntity> queryByName(String groupName) {
		return baseMapper.queryByName(groupName);
	}
	@Override
	public  List<SysGroupEntity> queryByWorkshopId(Long workshopId) {
		return baseMapper.queryByWorkshopId(workshopId);
	}
//	@Override
//	public List<Long> getSubWorkshopIdList(Long workshopId){
//		//部门及子部门ID列表
//		List<Long> workshopIdList = new ArrayList<>();
//
//		//获取子部门ID
//		List<Long> subIdList = queryDetpIdList(workshopId);
//		getWorkshopTreeList(subIdList, workshopIdList);
//
//		return workshopIdList;
//	}

	/**
	 * 查询事件转派组信息
	 */
	@Override
	public List<SysGroupEntity> queryTransferGroupList() {
		return baseMapper.queryTransferGroupList(Constant.GroupCategory.SUPPORT_GROUP.getDbName());
	}

//	/**
//	 * 递归
//	 */
//	private void getWorkshopTreeList(List<Long> subIdList, List<Long> workshopIdList){
//		for(Long workshopId : subIdList){
//			List<Long> list = queryDetpIdList(workshopId);
//			if(list.size() > 0){
//				getWorkshopTreeList(list, workshopIdList);
//			}
//
//			workshopIdList.add(workshopId);
//		}
//	}
}
