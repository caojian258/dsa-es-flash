package org.dsa.modules.reporter.service;


import org.dsa.modules.reporter.dto.SessionCts;
import org.dsa.modules.reporter.dto.SyncSessionLogDto;
import org.dsa.modules.reporter.dto.WtsInfoDto;

import java.util.List;
import java.util.Map;

/**
 * 诊断功能
 *
 */
public interface SynchronizedService {

    public SyncSessionLogDto sessionLogAssemble(String vin, String sessionId);

    public Boolean generateXML(SyncSessionLogDto dto, String path, String sessionId);

    public Boolean syncTsp(String path, Map<String,String> params);

    public Map<String, Map<String, String>> sessionVehicleIdentification(String sessionId);

    public Map<String, Map<String, String>> sessionDtc(String sessionId);

    public List<SessionCts> sessionCTS(String sessionId);

    public WtsInfoDto wtsInfo(String vin, String sessionId);
}

