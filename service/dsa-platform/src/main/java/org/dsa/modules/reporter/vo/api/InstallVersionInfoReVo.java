package org.dsa.modules.reporter.vo.api;

import lombok.Data;

import java.io.Serializable;

@Data
public class InstallVersionInfoReVo implements Serializable {
    private String versionNo;

    private String versionName;

    private String versionReleaseNote;

    private String status;

    private String note;

    private String oldVersionNo;

    private String oldVersionName;

    private String updateTime;

    private String failUpdateCount;
}
