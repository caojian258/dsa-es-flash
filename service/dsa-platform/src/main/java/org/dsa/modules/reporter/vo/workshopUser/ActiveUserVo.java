package org.dsa.modules.reporter.vo.workshopUser;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.modules.reporter.pojo.PageParam;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ActiveUserVo extends PageParam {

    private String workshopName;

    private String userName;
}
