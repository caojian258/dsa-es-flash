package org.dsa.modules.reporter.vo.diag;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.modules.reporter.pojo.PageParam;

import java.util.List;


@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class CtsPageReVo extends PageParam {

    private List<String> dates;

    private String startDate;

    private String endDate;

    private String vinCode;

    private List<Long> vehType;

    private String dtc;

    private Integer top;

    private String ctsName;

    private String ecu;
}
