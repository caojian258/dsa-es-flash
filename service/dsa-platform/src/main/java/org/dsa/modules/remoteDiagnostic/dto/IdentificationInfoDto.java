package org.dsa.modules.remoteDiagnostic.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.dsa.modules.remoteDiagnostic.entity.RemoteEcuIdentificationInfoEntity;

@EqualsAndHashCode(callSuper = true)
@Data
public class IdentificationInfoDto extends RemoteEcuIdentificationInfoEntity {

    @TableField(exist = false)
    private String versionName;

    @TableField(exist = false)
    private String ecuName;
}
