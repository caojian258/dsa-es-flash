CREATE TABLE public.d_action_dtc (
	id bigserial NOT NULL,
	action_id varchar NOT NULL,
	serial int4 NULL,
	ecu_name varchar NOT NULL,
	ti varchar NOT NULL,
	dtc_code varchar NULL,
	dtc_code_int varchar NOT NULL,
	description varchar NOT NULL,
	status varchar NULL,
	create_time timestamp NOT NULL DEFAULT now(),
	session_id varchar NULL,
	dtc_code_hex varchar NULL,
	nrc_hex varchar(255) NULL,
	nrc_description varchar(255) NULL,
	nrc_ti varchar(255) NULL,
	ti_status varchar(255) NULL,
	snapshot_enable bool NULL
);
CREATE INDEX d_action_dtc_create_time_idx ON public.d_action_dtc USING btree (create_time);


CREATE TABLE public.d_action_dtc_grid (
	id bigserial NOT NULL,
	action_id varchar NOT NULL,
	dtc_code varchar NULL,
	ecu_name varchar NOT NULL,
	"name" varchar NOT NULL,
	value varchar NOT NULL,
	unit varchar NULL,
	create_time timestamp NOT NULL DEFAULT now(),
	ti varchar NULL,
	ti_value varchar NULL,
	dtc_code_int varchar NULL,
	dtc_code_hex varchar NULL,
	CONSTRAINT d_action_dtc_grid_pkey PRIMARY KEY (id)
);

CREATE TABLE public.d_action_ecu (
	id bigserial NOT NULL,
	action_id varchar NOT NULL,
	ecu_name varchar NOT NULL,
	description varchar NULL,
	create_time timestamp NOT NULL DEFAULT now(),
	status int4 NULL,
	end_time timestamp NULL,
	session_id varchar NULL,
	CONSTRAINT d_action_ecu_pkey PRIMARY KEY (id)
);

CREATE TABLE public.d_action_info (
	id bigserial NOT NULL,
	action_id varchar NOT NULL,
	function_code varchar NOT NULL,
	action_code varchar NOT NULL,
	description varchar NULL,
	message varchar NULL,
	results varchar NULL,
	mid int8 NULL,
	timeout int2 NULL DEFAULT 0,
	message_en varchar NULL,
	message_cn varchar NULL,
	title_cn varchar(255) NULL,
	title_en varchar(255) NULL,
	start_time timestamp NOT NULL,
	end_time timestamp NULL,
	create_time timestamp NOT NULL DEFAULT now(),
	CONSTRAINT d_action_info_pkey PRIMARY KEY (id)
);

CREATE TABLE public.d_action_version (
	id bigserial NOT NULL,
	action_id varchar NOT NULL,
	serial int4 NULL,
	ecu_name varchar NOT NULL,
	ti varchar NOT NULL,
	semantic varchar NULL,
	"name" varchar NOT NULL,
	did varchar NULL,
	value varchar NULL,
	create_time timestamp NOT NULL DEFAULT now(),
	session_id varchar NULL,
	ti_value varchar NULL,
	nrc_hex varchar(255) NULL,
	nrc_description varchar(255) NULL,
	nrc_ti varchar(255) NULL,
	CONSTRAINT d_action_version_pkey PRIMARY KEY (id)
);

CREATE TABLE public.d_diag_package_version (
	id bigserial NOT NULL,
	vehicle_type_id int8 NOT NULL,
	version_num varchar NOT NULL,
	parent_version_num varchar NOT NULL,
	odx_version_id int8 NULL,
	otx_version_id int8 NULL,
	status varchar NOT NULL,
	file_path varchar NULL,
	file_md5 varchar NULL,
	description varchar NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NULL,
	created_user_id int8 NULL,
	updated_user_id int8 NULL,
	CONSTRAINT d_diag_package_version_pkey PRIMARY KEY (id)
);

CREATE TABLE public.d_function_action (
	id bigserial NOT NULL,
	function_code varchar NOT NULL,
	action_code varchar NOT NULL,
	function_name varchar NOT NULL,
	action_name varchar NULL,
	create_time timestamp NULL DEFAULT now(),
	show_filter int4 NOT NULL DEFAULT 0,
	CONSTRAINT d_function_action_pkey PRIMARY KEY (id)
);


CREATE TABLE public.d_session_action (
	id bigserial NOT NULL,
	session_id varchar NOT NULL,
	action_id varchar NOT NULL,
	create_time timestamp NOT NULL DEFAULT now(),
	CONSTRAINT d_session_action_pkey PRIMARY KEY (id)
);


CREATE TABLE public.d_session_info (
	id bigserial NOT NULL,
	session_id varchar NULL,
	vin varchar NOT NULL,
	active_user_id int8 NULL,
	diagnostic_user_id int8 NULL,
	log_path varchar NULL,
	log_name varchar NULL,
	"source" varchar NULL,
	hardware_info varchar NULL,
	start_time timestamp NULL,
	end_time timestamp NULL,
	active_display_user_name varchar NULL,
	diagnostic_display_user_name varchar NULL,
	end_status int4 NULL,
	workshop varchar NULL,
	pcid varchar NULL,
	create_time timestamp NULL DEFAULT now(),
	update_time timestamp NULL DEFAULT now(),
	CONSTRAINT d_session_info_pkey PRIMARY KEY (id),
	CONSTRAINT sessioniduniqueconstraint UNIQUE (session_id)
);