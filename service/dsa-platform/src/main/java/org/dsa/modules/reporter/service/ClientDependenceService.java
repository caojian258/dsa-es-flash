package org.dsa.modules.reporter.service;

import org.dsa.modules.reporter.entity.ClientDependenceOverview;

import java.util.List;

public interface ClientDependenceService {

    public List<ClientDependenceOverview> getDependenceList(String deviceId);
}
