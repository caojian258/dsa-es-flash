package org.dsa.modules.remoteDiagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.dsa.modules.remoteDiagnostic.entity.*;

import java.util.List;

@Mapper
public interface RemotePoolDependDao extends BaseMapper<RemotePoolDependEntity> {

    @Insert("<script>"
            + " insert into r_pool_depend (pool_id,vehicle_type_id) values "
            + " <foreach collection='values' item='item' separator=','> "
            + " (#{poolId},#{item}) "
            + " </foreach>"
            + " </script>")
    void insertsType(@Param("poolId") Long poolId, @Param("values") List<Long> val);

    @Insert("<script>"
            + " insert into r_pool_depend (pool_id,vehicle_version_id) values "
            + " <foreach collection='values' item='item' separator=','> "
            + " (#{poolId},#{item.id}) "
            + " </foreach>"
            + " </script>")
    void insertsVersion(@Param("poolId") Long poolId, @Param("values") List<RemoteVehicleVersionEntity> val);

    @Insert("<script>"
            + " insert into r_pool_depend (pool_id,ecu_version_id) values "
            + " <foreach collection='values' item='item' separator=','> "
            + " (#{poolId},#{item.id}) "
            + " </foreach>"
            + " </script>")
    void insertsEcu(@Param("poolId") Long poolId, @Param("values") List<RemoteVehicleEcuVersionEntity> val);

    @Insert("<script>"
            + " insert into r_pool_depend (pool_id,tag_id) values "
            + " <foreach collection='values' item='item' separator=','> "
            + " (#{poolId},#{item}) "
            + " </foreach>"
            + " </script>")
    void insertsTag(@Param("poolId") Long poolId, @Param("values") List<Long> val);

    @Select("select i.vehicle_type_id as id from r_pool_depend i where i.pool_id = #{poolId} and i.vehicle_type_id is not null ")
    List<Long> selectType(@Param("poolId") Long poolId);

    @Select("select j.*,k.value  as typeName from r_pool_depend i left join r_vehicle_version j on i.vehicle_version_id = j.id left join d_vehicle_type k on j.type_id = k.id  where i.pool_id = #{poolId} and k.id is not null and i.vehicle_version_id is not null ")
    List<RemoteVehicleVersionEntity> selectVersion(@Param("poolId") Long poolId);

    @Select("select j.*,k.ecu_name as ecuName from r_pool_depend i left join r_ecu_version j on i.ecu_version_id = j.id left join r_vehicle_ecu k on j.ecu_id = k.id where i.pool_id = #{poolId} and i.ecu_version_id is not null ")
    List<RemoteVehicleEcuVersionEntity> selectEcu(@Param("poolId") Long poolId);

    @Select("select i.tag_id as id from r_pool_depend i where i.pool_id = #{poolId} and i.tag_id is not null ")
    List<Long> selectTag(@Param("poolId") Long poolId);
}
