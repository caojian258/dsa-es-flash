package org.dsa.modules.reporter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;


@Data
@Accessors(chain = true)
@TableName("client_workshop_user")
public class ClientUser extends BaseEntity {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String pcid;

    private String firstUsername;

    private String workshop;

    private Long workshopId;

    private String LastUsername;

    private Date firstOnlineTime;

    private String firstOnlineIp;

    private Date lastOnlineTime;

    private String lastOnlineIp;

}
