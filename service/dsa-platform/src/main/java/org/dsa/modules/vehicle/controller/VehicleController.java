package org.dsa.modules.vehicle.controller;

import ch.qos.logback.classic.Logger;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.map.MapBuilder;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.annotation.SysLog;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.*;
import org.dsa.modules.remoteDiagnostic.vo.Vehicle.VehicleInfoExcelReqVo;
import org.dsa.modules.sys.entity.DisplayUserEntity;
import org.dsa.modules.vehicle.entity.DVehicleInfoEntity;
import org.dsa.modules.vehicle.entity.DVehicleTypeEntity;
import org.dsa.modules.vehicle.entity.VehicleInfoEntity;
import org.dsa.modules.vehicle.service.VehicleService;
import org.dsa.modules.vehicle.service.VehicleTypeService;
import org.dsa.modules.vehicle.vo.VehicleInfoPageReqVo;
import org.dsa.modules.vehicle.vo.VehicleInfoUpdateReqVo;
import jakarta.validation.constraints.NotNull;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import jakarta.annotation.Resource;


import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("vehicle")
public class VehicleController {

    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass());

    @Resource
    private VehicleTypeService vehicleTypeService;

    @Resource
    private VehicleService vehicleService;

    @Resource
    private ObjectMapper objectMapper;

    @Value("${Vehicle.ecu}")
    private List<String> vehicleEcuNames;

    @Value("${Vehicle.did}")
    private String vehicleEcuDid;


    /**
     * 车型列表
     */
    @RequestMapping(value = "/typeList", method = {RequestMethod.GET})
    public R list(@RequestParam(required = false) String name) {
        List<DVehicleTypeEntity> list = vehicleTypeService.queryList(StringUtils.isEmpty(name) ? null : name.trim());
        return R.ok().put("data", list);
    }

    /**
     * 车型选择
     */
    @RequestMapping(value = "/typeSelect", method = {RequestMethod.GET})
    public R select(String updateVehicle) {
        List<DVehicleTypeEntity> vehicleTypeList = vehicleTypeService.queryList(null);
        //添加一级部门
//        if(ShiroUtils.getUserName().equals(Constant.SUPER_ADMIN_USER_NAME)){
//            DVehicleTypeEntity root = new DVehicleTypeEntity();
//            root.setId(0L);
//            root.setValue("牛创");
//            root.setParentId(-1L);
//            root.setOpen(true);
//            vehicleTypeList.add(root);
//        }

        //新增车型时 筛选出层级1-4的数据作为可选择的上级节点，限制添加的层级不超过5
        if (ObjectUtils.isNull(updateVehicle)) {
            List<DVehicleTypeEntity> list = vehicleTypeList.stream()
                    .filter(d -> d.getLevel() != 5)
                    .collect(Collectors.toList());
            return R.ok().put("data", list);
        }
        return R.ok().put("data", vehicleTypeList);
    }

    /**
     * 车型详情
     */
    @RequestMapping("/typeInfo/{id}")
    public R info(@PathVariable("id") Long vehicleTypeId) {
        if (ObjectUtils.isNull(vehicleTypeId) || ObjectUtils.isNull(vehicleTypeService.getById(vehicleTypeId))) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        return R.ok().put("data", vehicleTypeService.getById(vehicleTypeId));
    }

    /**
     * 车型保存修改
     *
     * @param vehicleType
     * @return
     */
    @RequestMapping("/typeSave")
    public R save(@RequestBody DVehicleTypeEntity vehicleType) {
        //校验
        if (ObjectUtil.isNotNull(vehicleType.getOrderNum()) && ObjectUtil.isNotNull(vehicleType.getValue())) {
            R r = vehicleTypeService.checkVehicleType(vehicleType);
            if (ObjectUtils.isNull(r.get("success"))) {
                return r;
            }
        } else {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
//        vehicleTypeService.updateOrder(vehicleType.getParentId());
        vehicleTypeService.saveInfo(vehicleType);
        return R.ok();
    }

    /**
     * 车型修改
     */
    @RequestMapping("/typeUpdate")
//    @RequiresPermissions("sys:workshop:update")
    public R update(@RequestBody DVehicleTypeEntity vehicleType) {
        if (ObjectUtil.isNotNull(vehicleType.getOrderNum()) && ObjectUtil.isNotNull(vehicleType.getValue())) {
            R r = vehicleTypeService.checkVehicleType(vehicleType);
            if (ObjectUtils.isNull(r.get("success"))) {
                return r;
            }
        } else {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        vehicleTypeService.updateInfo(vehicleType);
        return R.ok();
    }

    /**
     * 车型删除
     */
    @SysLog("删除车型信息")
    @RequestMapping("/typeDelete/{id}")
    public R delete(@PathVariable("id") Long vehicleTypeId) {
        DisplayUserEntity userEntity = ShiroUtils.getUserEntity();
        if (BeanUtil.isEmpty(userEntity) ||  !Constant.SUPER_ADMIN_USER_NAME.equals(userEntity.getUserInfo().getUsername())){
            return R.error(Constant.Msg.NO_OPERATE_AUTH);
        }
        //判断是否有绑定车辆
//        List<Long> ids = new ArrayList<>();
//        ids.add(vehicleTypeId);
        Map<String, Object> params = new HashMap<>(4);
//        params.put("vehicleTypeIds",ids);
//        params.put("vin",null);
//        params.put(Constant.PAGE,1);
//        params.put(Constant.LIMIT,10);
//        PageUtils pageUtils = vehicleTypeService.getListByVehicleTypeIdAndVin(params);

//        if(pageUtils.getList().size() > 0){
//            return R.error(Constant.Msg.CHECK_HAS_VEHICLE);
//        }

        //判断是否有子节点
//        params.clear();
        params.put("parentId", vehicleTypeId);
        List<DVehicleTypeEntity> childrenList = vehicleTypeService.queryChildrenList(params);

        if (childrenList.size() > 0) {
            for (DVehicleTypeEntity dVehicleTypeEntity : childrenList) {
                List<DVehicleInfoEntity> infoList = vehicleService.getListByType(dVehicleTypeEntity.getId());
                if (infoList != null && infoList.size() != 0) {
                    return R.error(Constant.Msg.CHECK_HAS_VEHICLE);
                }
            }
//            return R.error(Constant.Msg.CHECK_HAS_CHILD);
            vehicleTypeService.batchDelete(childrenList);
        }


//        vehicleTypeService.removeById(vehicleTypeId);
//
//        remoteVehicleEcuGroupService.delDependByType(vehicleTypeId);

        return R.ok();
    }

    /**
     * 车辆列表
     */
//    @SysLog("根据车型查询车辆信息")
//    @PostMapping("/list")
//    public R vehicleList(@RequestBody Map<String, Object> params){
//        if (ObjectUtils.isNull(params.get("page"))||ObjectUtils.isNull(params.get("limit"))){
//            return R.error(Constant.Msg.CHECK_INPUT);
//        }
//        PageUtils pageUtils = vehicleTypeService.getListByVehicleTypeIdAndVin(params);
//        return R.ok().put("page",pageUtils);
//    }

    /**
     * 车辆详情
     */
    @GetMapping("/info/{id}")
    @SysLog("车辆详情")
    public R vehicleList(@PathVariable("id") Long id) {
        if (ObjectUtils.isNull(id) || ObjectUtils.isNull(vehicleService.getVehicleInfoById(id))) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        return R.ok().put("data", vehicleService.getVehicleInfoById(id));
    }

    /**
     * 车辆更新
     */
    @PostMapping("/update")
    @SysLog("车辆更新")
    public R vehicleUpdate(@RequestBody DVehicleInfoEntity entity) {
        if (ObjectUtils.isNull(entity.getId()) || ObjectUtils.isNull(vehicleService.getVehicleInfoById(entity.getId()))) {
            return R.error(Constant.Msg.CHECK_INPUT);
        }
        try {
            Integer res = vehicleService.updateVehicleInfo(entity);
            if (ObjectUtils.isNull(res)) {
                return R.error(Constant.Msg.SAVE_DATA);
            }
        } catch (Exception e) {
            return R.error(Constant.Msg.SAVE_DATA);
        }
        return R.ok();
    }

    /**
     * 车辆新增
     */
    @PostMapping("/save")
    @SysLog("车辆新增")
    public R vehicleAdd(@RequestBody DVehicleInfoEntity entity) {
        //设置车辆来源
        entity.setSource(Constant.VEHICLE_SOURCE.REMOTE.getCode());
        try {
            Integer res = vehicleService.updateVehicleInfo(entity);
            if (ObjectUtils.isNull(res)) {
                return R.error(Constant.Msg.SAVE_DATA);
            }
        } catch (Exception e) {
            return R.error(Constant.Msg.SAVE_DATA);
        }
        return R.ok();
    }

    @SysLog("根据车型查询车辆信息")
    @RequestMapping("/vehicleInfoList")
//    public R vehicleInfoList(@RequestBody Map<String, Object> params){
    public R vehicleInfoList(@RequestBody VehicleInfoPageReqVo vo) {
//        logger.info("vehicleInfoList in {}",params);
        Page<DVehicleInfoEntity> page = vehicleTypeService.getListByVehicleTypeIdAndVin(vo);
        return R.ok().put("data", page);
    }

    /**
     * 根据vin码获取车辆信息
     */
    @SysLog("根据vin码获取车辆信息")
    @RequestMapping("/getVehicleInfo")
    public R getVehicleInfo(@RequestParam String vin) {
        VehicleInfoEntity vehicleInfo = vehicleService.getVehicleInfoByVin(vin);
        return R.ok().put("data", vehicleInfo);
    }

    /**
     * 根据vin码获取车辆信息
     */
    @SysLog("根据vin码获取车辆信息")
    @RequestMapping("/getVehicleInfoBySign")
    public R getVehicleInfoBySign(@RequestParam String vin, @RequestParam String systemId, @RequestParam String requestUser, @RequestParam String requestTime, @RequestParam String sign) {
        if (StringUtils.isEmpty(vin)
                && StringUtils.isEmpty(systemId)
                && StringUtils.isEmpty(requestUser)
                && StringUtils.isEmpty(requestTime)
                && StringUtils.isEmpty(sign)) {
            throw new RRException(Constant.Msg.CHECK_INPUT);
        }
        //md5校验
        String md5 = Md5Utils.getVehicleInfoBySign(vin, requestUser, requestTime, systemId);
        if (!sign.equals(md5)) {
            throw new RRException(Constant.Msg.CHECK_INPUT);
        }
        VehicleInfoEntity vehicleInfo = vehicleService.getVehicleInfoByVin(vin);
        return R.ok().put("data", vehicleInfo);
    }

    @RequestMapping("/typeTreeList")
    public R typeTreeList() {
        List<DVehicleTypeEntity> list = vehicleTypeService.getTreeList();
        return R.ok().put("data", list);
    }

    @SysLog("修改车型排序")
    @PostMapping("/updateTypeOrder")
    public R typeTreeList(@RequestBody List<DVehicleTypeEntity> types) {
        vehicleTypeService.updateTypeOrder(types);
        return R.ok();
    }

    @PostMapping("/get")
    public R get(@RequestBody Map<String, String> params) {
        if (!params.containsKey("vin")) {
            throw new RRException(Constant.Msg.CHECK_VIN);
        }

        String vin = params.get("vin");
        String locale = params.get("locale");

        if (StringUtils.isBlank(locale)) {
            locale = Constant.LOCALS[1];
        } else {
            boolean b = Arrays.asList(Constant.LOCALS).contains(locale);
            if (!b) {
                throw new RRException("unsupported language!");
            }
        }
        // locale字段用于拓展多语言使用 暂未使用
        return R.ok().put("data", vehicleService.get(vin));
    }

    @SysLog("批量修改车辆信息")
    @PostMapping("/batchUpdateInfo")
    public R batchUpdateInfo(@RequestBody VehicleInfoUpdateReqVo vo) {
        if (ObjectUtils.isEmpty(vo.getVinList())) {
            return R.ok();
        }
        if (vo.getHasTsp()){
            if (StringUtils.isNotBlank(vo.getPlaceOfProduction()) || StringUtils.isNotBlank(vo.getProductionDate()) || null != vo.getVehicleTypeId()){
                return R.error(Constant.Msg.CHECK_INPUT);
            }
        }
        vehicleService.batchUpdateInfo(vo);

        return R.ok();
    }


    @PostMapping("/getVehicleConfigurationCode")
    public R getVehicleConfigurationCode(@RequestBody Map<String, Object> params) {
        try {
            Map<String, Object> res = new HashMap<>(3);
            String vin = MapUtils.getString(params, "vin");
            Integer type = MapUtils.getInteger(params, "type");
            Integer [] types = new Integer[]{0,1,2};
            if(!Arrays.asList(types).contains(type)){
                return R.error(500,"type只能为0，1，2").put("data", new HashMap<>(0));
            }
            if (StringUtils.isBlank(vin)) {
                return R.error(Constant.Msg.CHECK_VIN).put("data", new HashMap<>(0));
            }
            List<String> ecuNames = objectMapper.convertValue(MapUtils.getObject(params, "ecuNameList"), new TypeReference<List<String>>() {
            });
            List<Long> vehicleTypeIds = vehicleService.getVehicleTypeIdsByVin(vin);
            if (vehicleTypeIds.isEmpty()){
                return R.error(Constant.Msg.CHECK_VIN).put("data", new HashMap<>(0));
            }
            // 整车刷写
            if (type == 0) {
                List<Map<String, String>> vehicleDidValues = vehicleService.getDidValueByVehicleTypeIdsAndEcuNameAndDid(vehicleTypeIds, vehicleEcuNames, vehicleEcuDid);
                Map<String, List<Map<String, String>>> ecuNameMap = vehicleDidValues.stream().collect(Collectors.groupingBy(x -> x.get("ecuName")));
                List<Object> vehicleDidList = ecuMapToList(ecuNameMap,ecuNames);
                res.put("vin", vin);
                res.put("vecConfigCodes", vehicleDidList);
                // type==0 ecuList为空
                res.put("ecuConfigCodes", Collections.emptyList());
                return R.ok().put("data", res);
            }
            // ecu刷写
            if (type == 1) {
                if (ecuNames.isEmpty()) {
                    ecuNames = vehicleEcuNames;
                }
                List<Map<String, String>> ecuDidValues = vehicleService.getDidValueByVehicleTypeIdsAndEcuNameAndDid(vehicleTypeIds, ecuNames, null);
                Map<String, List<Map<String, String>>> ecuNameMap = ecuDidValues.stream().collect(Collectors.groupingBy(x -> x.get("ecuName")));
                List<Object> ecuDidList = ecuMapToList(ecuNameMap,ecuNames);
                res.put("vin", vin);
                // type==1 vConfCodes为空
                res.put("vecConfigCodes", Collections.emptyList());
                res.put("ecuConfigCodes", ecuDidList);
                return R.ok().put("data", res);
            }
            // 混合刷写
            if (type == 2) {
                if (ecuNames.isEmpty()) {
                    ecuNames = vehicleEcuNames;
                }
                // 整车配置码
                List<Map<String, String>> vehicleDidValue = vehicleService.getDidValueByVehicleTypeIdsAndEcuNameAndDid(vehicleTypeIds, vehicleEcuNames, vehicleEcuDid);
                Map<String, List<Map<String, String>>> ecuNameMap = vehicleDidValue.stream().collect(Collectors.groupingBy(x -> x.get("ecuName")));
                List<Object> vehicleDidList = ecuMapToList(ecuNameMap, vehicleEcuNames);
                // ecu配置码
                List<Map<String, String>> ecuDidValues = vehicleService.getDidValueByVehicleTypeIdsAndEcuNameAndDid(vehicleTypeIds, ecuNames, null);
                Map<String, List<Map<String, String>>> ecuNameMap2 = ecuDidValues.stream().collect(Collectors.groupingBy(x -> x.get("ecuName")));
                List<Object> ecuDidList = ecuMapToList(ecuNameMap2, ecuNames);
                res.put("vin", vin);
                res.put("vecConfigCodes", vehicleDidList);
                res.put("ecuConfigCodes", ecuDidList);
                return R.ok().put("data", res);
            }
            return R.ok().put("data", res);
        } catch (Exception e) {
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION).put("data",new HashMap<>(0));
        }

    }

    @NotNull
    private static List<Object> ecuMapToList(Map<String, List<Map<String, String>>> ecuNameMap, List<String> ecuNames) {
        List<Object> ecuNameList = new ArrayList<>();
        // ecuName输错时，返回ecuName的空配置码，C需要手动申请内存，size不一样会报错，所以有此处理
        Collection<String> subtract = CollectionUtils.subtract(ecuNames, ecuNameMap.keySet());
        if (!subtract.isEmpty()){
            subtract.forEach(ecuName->{
                Map<String, Object> map = MapBuilder.create(new HashMap<String, Object>())
                        .put("ecuName",ecuName)
                        .put("ecuConfCode",  Collections.emptyList())
                        .map();
                ecuNameList.add(map);
            });
        }
        ecuNameMap.keySet().forEach(k->{
            List<Map<String, String>> maps = ecuNameMap.get(k);
            maps.forEach(map->{
                map.remove("ecuName");
            });
            Map<String, Object> map = MapBuilder.create(new HashMap<String, Object>())
                    .put("ecuName",k)
                    .put("ecuConfCode",  maps)
                    .map();
            ecuNameList.add(map);
        });
        return ecuNameList;
    }

    @GetMapping("/getWholeVehicleType/{typeId}")
    public R getWholeVehicleType(@PathVariable("typeId") Long typeId) {
        return R.ok().put("data", vehicleTypeService.getWholeVehicleType(typeId));
    }

    @PostMapping("/exportVehicle")
    public void exportVehicle(HttpServletResponse response, @RequestBody VehicleInfoExcelReqVo vo){
        vehicleTypeService.exportVehicle(response,vo);
    }
}
