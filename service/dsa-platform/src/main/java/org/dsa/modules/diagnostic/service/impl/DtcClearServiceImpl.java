package org.dsa.modules.diagnostic.service.impl;

import ch.qos.logback.classic.Logger;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.diagnostic.dao.DActionEcuDao;
import org.dsa.modules.diagnostic.dao.DActionInfoDao;
import org.dsa.modules.diagnostic.dao.DSessionActionDao;
import org.dsa.modules.diagnostic.dao.DSessionInfoDao;
import org.dsa.modules.diagnostic.entity.DActionEcuEntity;
import org.dsa.modules.diagnostic.entity.mqtt.callback.MqBodyDataIDENTVO;
import org.dsa.modules.diagnostic.entity.mqtt.callback.MqEntity;
import org.dsa.modules.diagnostic.entity.mqtt.callback.MqBodyDataDTCClearVO;
import org.dsa.modules.diagnostic.service.DiagnosticMqService;
import org.dsa.modules.diagnostic.util.MqAsynchronousData;
import org.dsa.modules.diagnostic.util.MqSendUtils;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service("dtcClearServiceImpl")
public class DtcClearServiceImpl implements DiagnosticMqService {

    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass());

    @Resource
    private DActionEcuDao dActionEcuDao;
    @Resource
    private MqAsynchronousData<MqBodyDataDTCClearVO> mqAsynchronousData;

    @Override
    public void onMessage(MqEntity mqEntity) {
        logger.info("dtcClear MQ Call back---begin");
        logger.info("dtcClear MQ Call back----actionID:{}",mqEntity.getHeader().getRid());
        try{
            mqAsynchronousData.handle("dtcClear",mqEntity,new MqBodyDataDTCClearVO());
            saveActionVersion(mqEntity);
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        logger.info("dtcClear MQ Call back-----end");
    }

    private void saveActionVersion(MqEntity mqEntity){
        logger.info("Save dtcClear data in DB");
        List<MqBodyDataDTCClearVO> dtcClearVos = new ArrayList<MqBodyDataDTCClearVO>();
        dtcClearVos = (List<MqBodyDataDTCClearVO>) mqEntity.getBody().getData();
        if(dtcClearVos!=null && dtcClearVos.size() > 0){
            for(MqBodyDataDTCClearVO dtcClearVo:dtcClearVos){
                DActionEcuEntity dActionEcuEntity=new DActionEcuEntity();
                dActionEcuEntity.setActionId(mqEntity.getHeader().getRid());
                dActionEcuEntity.setEcuName(dtcClearVo.getEcuName());
                dActionEcuEntity.setStatus(dtcClearVo.getStatus());
                dActionEcuEntity.setDescription(dtcClearVo.getDescription());
                dActionEcuEntity.setEndTime(new Date());
                dActionEcuDao.updateByActionAndEcuName(dActionEcuEntity);
            }
        }
    }
}
