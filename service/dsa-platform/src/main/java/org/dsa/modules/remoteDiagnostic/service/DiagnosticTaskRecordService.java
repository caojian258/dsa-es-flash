package org.dsa.modules.remoteDiagnostic.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.remoteDiagnostic.dto.page.DiagnosticTaskRecordDto;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskRecordEntity;
import org.dsa.modules.remoteDiagnostic.vo.PublicPageReqVo;

import java.util.List;

public interface DiagnosticTaskRecordService {

    Page<DiagnosticTaskRecordDto> selectPage(PublicPageReqVo vo);

    DiagnosticTaskRecordEntity getInfo(Long id);

    void saveTask(DiagnosticTaskRecordEntity task);

    void updateTask(DiagnosticTaskRecordEntity task);

    void updateTask(String sessionId, String vin);

    List<DiagnosticTaskRecordEntity> getTasks();
}
