package org.dsa.modules.remoteDiagnostic.service.impl;


import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.dsa.modules.remoteDiagnostic.dao.DiagnosticTaskActiveDao;
import org.dsa.modules.remoteDiagnostic.dao.DiagnosticTaskDetailDao;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskActiveEntity;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskDetailEntity;
import org.dsa.modules.remoteDiagnostic.enums.TaskDetailTypeEnum;
import org.dsa.modules.remoteDiagnostic.service.DiagnosticTaskActiveService;
import org.dsa.modules.remoteDiagnostic.vo.PublicPageReqVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
public class DiagnosticTaskActiveServiceImpl implements DiagnosticTaskActiveService {

    @Autowired
    DiagnosticTaskActiveDao activeDao;
    @Autowired
    private DiagnosticTaskDetailDao taskDetailDao;

    @Override
    public Page<DiagnosticTaskActiveEntity> selectPage(PublicPageReqVo vo) {
        Page<DiagnosticTaskActiveEntity> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);
        LambdaQueryWrapper<DiagnosticTaskActiveEntity> qw = Wrappers.<DiagnosticTaskActiveEntity>lambdaQuery();

        qw.eq(DiagnosticTaskActiveEntity::getCampaignId, vo.getId());

        if (StringUtils.isNotBlank(vo.getName())) {
            qw.apply("vin" + " ilike {0}", "%" + vo.getName().trim() + "%");
        }
        if (vo.getStatus() != null) {
            qw.eq(DiagnosticTaskActiveEntity::getStatus, vo.getStatus());
        }

        return activeDao.selectPage(page, qw);
    }

    @Override
    public DiagnosticTaskActiveEntity getInfo(Long id) {
        return null;
    }

    @Override
    public void saveTask(DiagnosticTaskActiveEntity task) {

    }

    @Override
    public void updateTask(DiagnosticTaskActiveEntity task) {

    }

    @Override
    public void updateTask(String sessionId, String vin) {

    }

    @Override
    public List<DiagnosticTaskActiveEntity> getList() {
        return null;
    }

    @Override
    @Transactional
    public void terminationActive(Long id) {
        DiagnosticTaskActiveEntity active = activeDao.selectById(id);
        if (active != null) {
            DiagnosticTaskDetailEntity diagnosticTaskDetailEntity = new DiagnosticTaskDetailEntity();
            diagnosticTaskDetailEntity.setCampaignId(active.getCampaignId());
            diagnosticTaskDetailEntity.setVin(active.getVin());
            diagnosticTaskDetailEntity.setSessionId(active.getSessionId());
            diagnosticTaskDetailEntity.setCode("0");
            diagnosticTaskDetailEntity.setType(TaskDetailTypeEnum.TERMINATION.getValue());
//            diagnosticTaskDetailEntity.setActive(active);
            taskDetailDao.insert(diagnosticTaskDetailEntity);
            active.setStatus(2);
            activeDao.updateById(active);
        }
    }
}
