package org.dsa.modules.diagnostic.service.impl;

import ch.qos.logback.classic.Logger;
import com.alibaba.fastjson.JSONObject;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.diagnostic.dao.DActionDtcDao;
import org.dsa.modules.diagnostic.dao.DActionEcuDao;
import org.dsa.modules.diagnostic.dao.DSessionActionDao;
import org.dsa.modules.diagnostic.entity.DActionDtcEntity;
import org.dsa.modules.diagnostic.entity.DActionEcuEntity;
import org.dsa.modules.diagnostic.entity.mqtt.callback.MqBodyDataDTCVO;
import org.dsa.modules.diagnostic.entity.mqtt.callback.MqEntity;
import org.dsa.modules.diagnostic.service.DiagnosticMqService;
import org.dsa.modules.diagnostic.util.MqAsynchronousData;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service("dtcReadService")
public class DtcReadServiceImpl implements DiagnosticMqService {
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass());

    @Resource
    private RedisUtils redisUtils;
    @Resource
    private DActionDtcDao dActionDtcDao;
    @Resource
    private DSessionActionDao dSessionActionDao;
    @Resource
    private DActionEcuDao dActionEcuDao;
    @Resource
    private MqAsynchronousData<MqBodyDataDTCVO> mqAsynchronousData;

    @Override
    public void onMessage(MqEntity mqEntity) {
        logger.info("DtcRead MQ Call back---begin");
        logger.info("DtcRead MQ Call back----actionID:{}",mqEntity.getHeader().getRid());
        try {
            mqAsynchronousData.handle("DtcRead", mqEntity, new MqBodyDataDTCVO());
            //保存诊断数据
            saveActionVersion(mqEntity);
        }catch (Exception e){
            logger.error(e.getMessage());
        }

        logger.info("DtcRead MQ Call back-----end");
    }

    private void saveActionVersion(MqEntity mqEntity){
        logger.info("Save DtcReadData data in DB");
        if(mqEntity.getBody().getData()!=null ){
            List<MqBodyDataDTCVO> dtcvos= new ArrayList<MqBodyDataDTCVO>();
            dtcvos= JSONObject.parseArray( JSONObject.toJSONString(mqEntity.getBody().getData()),MqBodyDataDTCVO.class);
            if(dtcvos!=null && dtcvos.size() > 0 ){
                //获取sessionid
                String sessionId = dSessionActionDao.selectSessionIdByActionId(mqEntity.getHeader().getRid());
                for(MqBodyDataDTCVO dtcvo:dtcvos){
                    DActionDtcEntity dActionDtcEntity=new DActionDtcEntity();
                    dActionDtcEntity.setActionId(mqEntity.getHeader().getRid());
                    dActionDtcEntity.setEcuName(dtcvo.getEcuName());
                    dActionDtcEntity.setTi(dtcvo.getTi());
                    dActionDtcEntity.setDescription(dtcvo.getDescription());
                    dActionDtcEntity.setDtcCode(dtcvo.getDtcCode());
                    dActionDtcEntity.setDtcCodeInt(dtcvo.getDtcCode_Int());
                    dActionDtcEntity.setDtcCodeHex(dtcvo.getDtcCodeHex());
                    dActionDtcEntity.setSessionId(sessionId);
                    dActionDtcEntity.setStatus(dtcvo.getTiStatus());
                    dActionDtcDao.insert(dActionDtcEntity);
                }
            }
            for(String ecuName : mqEntity.getBody().getEcuName()) {
                DActionEcuEntity dActionEcuEntity = new DActionEcuEntity();
                dActionEcuEntity.setActionId(mqEntity.getHeader().getRid());
                dActionEcuEntity.setEcuName(ecuName);
                dActionEcuEntity.setDescription(mqEntity.getBody().getMessage());
                dActionEcuEntity.setEndTime(new Date());
                dActionEcuEntity.setStatus(mqEntity.getBody().getStatus());
                //保存诊断执行的ecu
                dActionEcuDao.updateByActionAndEcuName(dActionEcuEntity);
            }
        }
    }
}
