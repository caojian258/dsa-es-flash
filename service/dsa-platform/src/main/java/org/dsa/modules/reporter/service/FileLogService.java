package org.dsa.modules.reporter.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.reporter.entity.FileLog;
import org.dsa.modules.reporter.vo.file.FileLogPageReqVo;

import java.util.Date;
import java.util.List;

public interface FileLogService {

    public Boolean checkSuccessed(String fileType, String fileName);

    public void insertSessionLog(String name, String path, String tag, String sessionId, Integer status, String note);

    public void insertLog(String name, String path, String tag, String hash, Integer status, String note);

    public void failedLog(String name, String path, String tag, String note);

    public void succeedLog(String name, String path, String tag, String hash, String note);

    public List<String> failedFiles(Date handlerTime);

    public List<String> succeedFiles(Date handlerTime);

    Page<FileLog> selectPage(FileLogPageReqVo pageReqVO);

    public FileLog findOneFile(Long id);

    public FileLog checkFile(String fileType, String fileName);

    public String pathToFileName(String path);

    public String getArchiveFile(String fileType, String fileName);

    public Integer insertLog(String name,String fileName, String path, String tag, String hash, Integer status, String note);

    public Integer failedLog(String name,String fileName, String path, String tag, String note);

    public Integer succeedLog(String name,String fileName, String path, String tag, String hash, String note);

    public void updateLog(Integer fileId,String filePath);
}
