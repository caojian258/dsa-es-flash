package org.dsa.modules.reporter.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.dsa.modules.reporter.dao.AppOverviewMapper;
import org.dsa.modules.reporter.entity.AppOverview;
import org.dsa.modules.reporter.service.AppOverviewIService;
import org.springframework.stereotype.Service;

@Service
public class AppOverviewIServiceImpl extends ServiceImpl<AppOverviewMapper, AppOverview> implements AppOverviewIService {
}
