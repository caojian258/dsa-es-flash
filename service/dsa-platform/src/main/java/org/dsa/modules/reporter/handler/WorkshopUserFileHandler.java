package org.dsa.modules.reporter.handler;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import lombok.extern.slf4j.Slf4j;
import org.dsa.modules.reporter.dao.WorkshopUserGroupMapper;
import org.dsa.modules.reporter.dao.WorkshopUserGroupsMapper;
import org.dsa.modules.reporter.dao.WorkshopUserMapper;
import org.dsa.modules.reporter.dto.WorkshopUserFileDto;
import org.dsa.modules.reporter.entity.WorkshopUser;
import org.dsa.modules.reporter.entity.WorkshopUserGroup;
import org.dsa.modules.reporter.entity.WorkshopUserGroups;
import org.dsa.modules.reporter.enums.FileCateEnum;
import org.dsa.modules.reporter.service.FileLogService;
import org.dsa.modules.reporter.util.MapBeanUtil;
import org.dsa.modules.reporter.util.XmlReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@Service
public class WorkshopUserFileHandler {

    @Autowired
    WorkshopUserMapper userMapper;

    @Autowired
    WorkshopUserGroupMapper userGroupMapper;

    @Autowired
    WorkshopUserGroupsMapper userGroupsMapper;

    @Autowired
    FileLogService logService;

    public WorkshopUserFileDto storeData(String path,String archiveFilePath) throws Exception {

        WorkshopUserFileDto dto = new WorkshopUserFileDto();
        dto.setInsertGroupTotal(new AtomicInteger(0));
        dto.setInsertTotal(new AtomicInteger(0));
        dto.setUpdateGroupTotal(new AtomicInteger(0));
        dto.setUpdateTotal(new AtomicInteger(0));

        File file = new File(path);
        // 判断文件是否存在 或 为空
        if (!file.exists() || file.length() == 0) {
            logService.failedLog(FileCateEnum.WORKSHOPUSER.getValue(), archiveFilePath, null,"user file is null");
            return dto;
        }

        XmlReader reader = new XmlReader(path);

        // account data
        List<Map<String, Object>> mapList =  reader.parse("account");
        if (mapList != null && mapList.size() > 0) {
            dto.setUserTotal(mapList.size());
            for (Map<String, Object> map: mapList){
                try {
                    // 提取方法 存入失败日志
                    saveWorkshopUser(archiveFilePath, map,dto);
                }catch (Exception e){
                    logService.failedLog(FileCateEnum.WORKSHOP.getValue(), archiveFilePath, null,map.toString()+":["+e.getMessage()+"]");
                }
            }
        }else{
            logService.failedLog(FileCateEnum.WORKSHOPUSER.getValue(), archiveFilePath, null,"user account is null");
        }


        // group data
        List<Map<String, Object>> mapGroupList =  reader.parse("group");

        if (mapGroupList != null && mapGroupList.size() > 0) {
            dto.setUserGroupTotal(mapGroupList.size());

            for (Map<String, Object> map: mapGroupList){
                try {
                    // 提取方法 存入失败日志
                    saveWorkshopGroup(map, dto);
                }catch (Exception e){
                    logService.failedLog(FileCateEnum.WORKSHOPUSER.getValue(), archiveFilePath, null,map.toString()+":["+e.getMessage()+"]");
                }
            }
        }else{
            logService.failedLog(FileCateEnum.WORKSHOPUSER.getValue(), archiveFilePath, null,"user group is null");
        }

        dto.setMsg("userTotal: " + dto.getUserTotal() + ", userGroupTotal: " + dto.getUserGroupTotal()
                + ",UserSuccessTotal: " + (dto.getUpdateTotal().get() + dto.getInsertTotal().get()) + ",UserFailTotal: " + (dto.getUserTotal() - dto.getUpdateTotal().get() - dto.getInsertTotal().get())
                + ",UserGroupSuccessTotal: " + (dto.getInsertGroupTotal().get() + dto.getUpdateGroupTotal().get()) + ",UserGroupFailTotal: " + (dto.getUserGroupTotal() - dto.getUpdateGroupTotal().get() - dto.getInsertGroupTotal().get())
                + ",Processing Results:[ spend time: " + reader.getSpendTime()
                + ",insertUserTotal:" + dto.getInsertTotal().get() + ",updateUserTotal:"
                + dto.getUpdateTotal().get() + ",insertUserGroupTotal:" + dto.getInsertGroupTotal().get()
                + ",updateUserGroupTotal:" + dto.getUpdateGroupTotal().get() + "]");
        logService.succeedLog(FileCateEnum.WORKSHOPUSER.getValue(),file.getName(), archiveFilePath, null, null, dto.getMsg());
        return dto;
    }

    private void saveWorkshopUser(String path, Map<String, Object> map, WorkshopUserFileDto dto) throws Exception {
        WorkshopUser user = (WorkshopUser) MapBeanUtil.map2Object(map, WorkshopUser.class);
        if (null != map.get("userId") && StrUtil.isNotBlank(map.get("userId").toString())) {
            user.setThirdpartId(map.get("userId").toString());
        }
        if (null != map.get("workshopID") && StrUtil.isNotBlank(map.get("workshopID").toString())) {
            user.setWorkshopId(map.get("workshopID").toString());
        }
        if (null != map.get("name") && StrUtil.isNotBlank(map.get("name").toString())) {
            user.setRealName(map.get("name").toString());
        }

        if (null != map.get("address") && StrUtil.isNotBlank(map.get("address").toString())) {
            user.setAddress(map.get("address").toString());
        }

        if(null != map.get("telephone") && StrUtil.isNotBlank(map.get("telephone").toString())){
            user.setTelephone(map.get("telephone").toString());
        }

        if(null != map.get("userName") && StrUtil.isNotBlank(map.get("userName").toString())){
            user.setUsername(map.get("userName").toString());
        }else {
            logService.failedLog(FileCateEnum.WORKSHOPUSER.getValue(), path, null, map.toString()+":[username is null]");
            return;
        }

        if (null != map.get("active") && StrUtil.isNotBlank(map.get("active").toString())) {
            user.setActive(Boolean.valueOf(map.get("active").toString()));
        } else {
            user.setActive(false);
        }
        //modifiedTime "2022-06-24T13:40:24.813+08:00" 2019-01-20T18:23:14.890-08:00
        //creationTime "2022-06-24T13:40:24.813+08:00"
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z");
        SimpleDateFormat defaultFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            if (null != map.get("creationTime") && StrUtil.isNotBlank(map.get("creationTime").toString())) {
                String creationTime = map.get("creationTime").toString().substring(0, 23);
                String utcDate = creationTime + " UTC";
                Date time = format.parse(utcDate);
                user.setCreatedAt(time);
            }
        } catch (Exception ex) {
            log.error("WorkshopUserFileHandler storeData creationTime: {}", ex.getMessage());
            logService.failedLog(FileCateEnum.WORKSHOPUSER.getValue(), path, null, map.toString() + ":[" + ex.getMessage() + "]");
        }

        try {
            if (null != map.get("modifiedTime") && StrUtil.isNotBlank(map.get("modifiedTime").toString())) {
                String modifiedTime = map.get("modifiedTime").toString().substring(0, 23);
                String utcDate = modifiedTime + " UTC";
                Date time = format.parse(utcDate);
                user.setUpdatedAt(time);
            }
        } catch (Exception ex) {
            log.error("WorkshopUserFileHandler storeData modifiedTime: {}", ex.getMessage());
            logService.failedLog(FileCateEnum.WORKSHOPUSER.getValue(), path, null, map.toString() + ":[" + ex.getMessage() + "]");
        }

        QueryWrapper<WorkshopUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username", map.get("userName"));

        WorkshopUser workshopUser = userMapper.selectOne(queryWrapper);
        if(null != workshopUser){
            user.setUpdatedAt(new Date());
            UpdateWrapper<WorkshopUser> updateWrapper = new UpdateWrapper<>();
            updateWrapper.eq("username", map.get("userName"));
            userMapper.update(user, updateWrapper);
            dto.getUpdateTotal().incrementAndGet();
//                    updateUserTotal++;
        } else {
            userMapper.insert(user);
            dto.getInsertTotal().incrementAndGet();
//                    insertUserTotal++;

        }

        if (null != map.get("groupRef") && map.get("groupRef") instanceof Collection) {
            List<String> groups = (List<String>) map.get("groupRef");
            for (String gid : groups) {
                QueryWrapper<WorkshopUserGroups> qw = new QueryWrapper<>();
                qw.eq("username", user.getUsername());
                qw.eq("group_name", gid);
                qw.orderByAsc("id");
                WorkshopUserGroups ug = userGroupsMapper.selectOne(qw);
                if (null == ug) {
                    WorkshopUserGroups gg = new WorkshopUserGroups();
                    gg.setUsername(user.getUsername());
                    gg.setGroupName(gid);
                    gg.setUserId(user.getId() == null ? workshopUser.getId() : user.getId());
                    userGroupsMapper.insert(gg);
                } else {
                    ug.setUsername(user.getUsername());
                    ug.setGroupName(gid);
                    ug.setUpdatedAt(new Date());
                    ug.setUserId(user.getId() == null ? workshopUser.getId() : user.getId());
                    userGroupsMapper.updateById(ug);
                }
            }
        }
    }

    private void saveWorkshopGroup(Map<String, Object> map, WorkshopUserFileDto dto) throws Exception{
        WorkshopUserGroup userGroup = new WorkshopUserGroup();

        userGroup.setDisplayName(map.get("groupDisplayName").toString());
        userGroup.setName(map.get("groupName").toString());
        userGroup.setOrdered(Integer.valueOf(map.get("order").toString()));
        userGroup.setParentId(0);

        QueryWrapper<WorkshopUserGroup> qwugw = new QueryWrapper<>();
        qwugw.eq("name", userGroup.getName() );
        qwugw.eq("parent_id", 0 );
        qwugw.orderByAsc("id");

        WorkshopUserGroup wug = userGroupMapper.selectOne(qwugw);
        Integer parentId = 0;

        if(null == wug){
            userGroupMapper.insert(userGroup);
            parentId = userGroup.getId();
//                    insertUserGroupTotal++;
            dto.getInsertGroupTotal().incrementAndGet();

        }else {
            userGroup.setUpdatedAt(new Date());
            UpdateWrapper<WorkshopUserGroup> updateWrapper = new UpdateWrapper<>();
            updateWrapper.eq("name", userGroup.getName() );
            userGroupMapper.update(userGroup, updateWrapper);

            parentId =  wug.getId();
//                    hasUserGroupTotal++;
            dto.getUpdateGroupTotal().incrementAndGet();

        }

        userGroupsMapper.updateGroupIdByGroupName(parentId, userGroup.getName());

        if(null != map.get("childGroup") &&  map.get("childGroup") instanceof Collection){
            List<Object> childGroup = (List<Object>) map.get("childGroup");
            int size = childGroup.size();
            for (int i = 0; i< size; i += 3){
                dto.setUserGroupTotal(dto.getUserGroupTotal() + 1);
                WorkshopUserGroup userChildGroup = new WorkshopUserGroup();
                userChildGroup.setParentId(parentId);
                userChildGroup.setDisplayName(String.valueOf(childGroup.get(i)));
                userChildGroup.setName(String.valueOf(childGroup.get(i+1)));
                userChildGroup.setOrdered(Integer.valueOf(childGroup.get(i+2).toString()));

                QueryWrapper<WorkshopUserGroup> qwugcw = new QueryWrapper<>();
                qwugcw.eq("name", userChildGroup.getName() );
                qwugcw.eq("parent_id", parentId );
                qwugcw.orderByAsc("id");

                WorkshopUserGroup wugc = userGroupMapper.selectOne(qwugcw);
                Integer childId = null;
                if(null == wugc){
                    userGroupMapper.insert(userChildGroup);
                    childId = userChildGroup.getId();
//                            insertUserGroupTotal++;
                    dto.getInsertGroupTotal().incrementAndGet();


                }else{
                    userChildGroup.setUpdatedAt(new Date());
                    UpdateWrapper<WorkshopUserGroup> updateWrapper = new UpdateWrapper<>();
                    updateWrapper.eq("name", userChildGroup.getName() );
                    updateWrapper.eq("parent_id", parentId );
                    userGroupMapper.update(userChildGroup, updateWrapper);

                    childId = wugc.getId();
//                            hasUserGroupTotal++;
                    dto.getUpdateGroupTotal().incrementAndGet();

                }
                userGroupsMapper.updateGroupIdByGroupName(childId, userChildGroup.getName());
            }
        }
    }
}
