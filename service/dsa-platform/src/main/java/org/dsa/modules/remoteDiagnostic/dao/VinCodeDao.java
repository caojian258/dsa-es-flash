package org.dsa.modules.remoteDiagnostic.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.*;
import org.dsa.modules.remoteDiagnostic.entity.VinCodeEntity;

import java.util.List;

@Mapper
public interface VinCodeDao extends BaseMapper<VinCodeEntity> {

    @Insert("<script>"
            + " insert into r_pool_vin (p_id,vin) values "
            + " <foreach collection='values' item='item' separator=','> "
            + " (#{pId},#{item}) "
            + " </foreach>"
            + " ON conflict(p_id,vin)  DO NOTHING "
            + " </script>")
    void inserts(@Param("pId") Long pId, @Param("values") List<String> val);

    @Delete("<script>"
            + " delete from r_pool_vin where p_id = #{pId} and vin in  "
            + " <foreach collection='values' item='item' open='(' separator=',' close=')'> "
            + " #{item} "
            + " </foreach>"
            + " </script>")
    void deletes(@Param("pId") Long pId, @Param("values") List<String> val);

    @Select("select i.vin from r_pool_vin i where i.p_id = #{poolId}")
    List<String> getVinListByPool(@Param("poolId") Long poolId);

    @Select("<script>"
            + "select i.vin from r_pool_vin i "
            + " <if test='tag!=null' >"
            + "  left join r_vehicle_tag j on i.vin = j.vin "
            + " </if> "
            + " where i.p_id = #{poolId} "
            + " <if test=\"vin!=null and vin!='' \">"
            + "  and i.vin ilike concat('%',#{vin},'%') "
            + " </if> "
            + " <if test='tag!=null' >"
            + "  and j.t_id = #{tag} and j.id is not null "
            + " </if> "
            + " </script>")
    Page<VinCodeEntity> getPoolVinCodeList(Page<VinCodeEntity> page, @Param("poolId") Long id, @Param("vin") String vin, @Param("tag") Long tag);
}
