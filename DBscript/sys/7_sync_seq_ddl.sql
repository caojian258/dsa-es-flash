
select setval('sys_role_role_id_seq', (select max(i.role_id)+1 from sys_role i));
select setval('sys_menu_menu_id_seq', (select max(i.menu_id)+1 from sys_menu i));
select setval('sys_role_menu_id_seq', (select max(i.id)+1 from sys_role_menu i));
select setval('sys_user_user_id_seq', (select max(i.user_id)+1 from sys_user i));
select setval('sys_user_role_id_seq', (select max(i.id)+1 from sys_user_role i));
select setval('sys_config_id_seq', (select max(i.id)+1 from sys_config i));
select setval('d_vehicle_type_id_seq', (select max(i.id)+1 from d_vehicle_type i));
select setval('d_vehicle_info_id_seq', (select max(i.id)+1 from d_vehicle_info i));
select setval('d_language_locales_id_seq', (select max(i.id)+1 from d_language_locales i));
COMMIT;


