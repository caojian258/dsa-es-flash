-- client

truncate table client_application_history;
select setval('client_application_history_id_seq', 1);

truncate table client_application_installation;
select setval('client_application_installation_id_seq', 1);

truncate table client_application_overview;
select setval('client_application_overview_id_seq', 1);

truncate table client_application_repair;
select setval('client_application_repair_id_seq', 1);

truncate table client_application_rollback;
select setval('client_application_rollback_id_seq', 1);

truncate table client_application_uninstallation;
select setval('client_application_uninstallation_id_seq', 1);

truncate table client_application_version;
select setval('client_application_version_id_seq', 1);

truncate table client_component_back;
select setval('client_component_back_id_seq', 1);

truncate table client_current_component_info;
select setval('client_current_component_info_id_seq', 1);

truncate table client_current_software_info;
select setval('client_current_software_info_id_seq', 1);

truncate table client_dependence_info;
select setval('client_dependence_info_id_seq', 1);

truncate table client_dependence_tree;
select setval('client_dependence_tree_id_seq', 1);

truncate table client_future_component_info;
select setval('client_future_component_info_id_seq', 1);

truncate table client_future_software_info;
select setval('client_future_software_info_id_seq', 1);

truncate table client_os_history;
select setval('client_os_history_id_seq', 1);

truncate table client_os_overview;
select setval('client_os_overview_id_seq', 1);

truncate table client_os_protocol_item;
select setval('client_os_protocol_item_id_seq', 1);

truncate table client_software_back;
select setval('client_software_back_id_seq', 1);

truncate table client_upgrade_result;
select setval('client_upgrade_result_id_seq', 1);

truncate table client_workshop_user;
select setval('client_workshop_user_id_seq', 1);



-- workshop
truncate table d_workshop;

truncate table d_workshop_group;
select setval('d_workshop_group_id_seq', 1);

truncate table d_workshop_user;
select setval('d_workshop_user_id_seq', 1);

truncate table d_workshop_user_group;
select setval('d_workshop_user_group_id_seq', 1);

truncate table d_workshop_user_groups;
select setval('d_workshop_user_groups_id_seq', 1);

-- session

truncate table d_action_dtc;
select setval('d_action_dtc_id_seq', 1);

truncate table d_action_dtc_grid;
select setval('d_action_dtc_grid_id_seq', 1);

truncate table d_action_ecu;
select setval('d_action_ecu_id_seq', 1);

truncate table d_action_info;
select setval('d_action_info_id_seq', 1);

truncate table d_action_version;
select setval('d_action_version_id_seq', 1);

truncate table d_session_action;
select setval('d_session_action_id_seq', 1);

truncate table d_session_info;
select setval('d_session_info_id_seq', 1);

truncate table d_session_event_log;
select setval('d_session_event_log_id_seq', 1);

truncate table d_ecu_version;
select setval('d_ecu_version_id_seq', 1);

truncate table file_log;
select setval('file_log_id_seq', 1);


-- flash

truncate table vehicle_flash_record;
select setval('vehicle_flash_record_id_seq', 1);

truncate table vehicle_flash_detail;
select setval('vehicle_flash_detail_id_seq', 1);

truncate table vehicle_version_overview;
select setval('vehicle_version_overview_id_seq', 1);

truncate table vehicle_version_history;
select setval('vehicle_version_history_id_seq', 1);

truncate table ecu_version_history;
select setval('ecu_version_history_id_seq', 1);

truncate table ecu_version_overview;
select setval('ecu_version_overview_id_seq', 1);


-- sys

truncate table sys_file;
select setval('sys_file_id_seq', 1);

-- job

truncate table file_log;
select setval('file_log_id_seq', 1);

truncate table schedule_job_log;
select setval('schedule_job_log_id_seq', 1);