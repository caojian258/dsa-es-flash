package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class ClientUpgradeFullDto implements Serializable {

     private String username;
     private String userDisplayName;
     private Long workshopId;
     private String workshopName;
     private String deviceId;
     private Date startTime;
     private Date end_time;

    private String componentName;
    private String componentCurrentVersionName;
    private String softwareName;
    private String softwareCurrentVersionName;
    private String componentTargetVersionName;
    private String softwareTargetVersionName;
    private String componentRollbackVersionName;
    private String softwareRollbackVersionName;

    private Integer softwareId;
    private Integer softwareCurrentVersionNumber;
    private Integer softwarePreviousVersionNumber;
    private Integer componentId;
    private Integer componentCurrentVersionNumber;
    private Integer componentPreviousVersionNumber;
    private Integer installAction;
    private Integer updateResult;
    private Integer updateTarget;
    private String tag;

    private Date ctime;
}
