package org.dsa.modules.sys.service.impl;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.dsa.common.utils.PageUtils;
import org.dsa.common.utils.Query;
import org.dsa.modules.sys.dao.SysUserLicenseDao;
import org.dsa.modules.sys.entity.SysUserLicenseEntity;
import org.dsa.modules.sys.service.SysUserLicenseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;


@Service("sysUserLicenseService")
public class SysUserLicenseServiceImpl extends ServiceImpl<SysUserLicenseDao, SysUserLicenseEntity> implements SysUserLicenseService {

    @Autowired
    private SysUserLicenseDao sysUserLicenseDao;

    @Override
    public List<SysUserLicenseEntity> queryById(String userId) {
        List<SysUserLicenseEntity> lists = sysUserLicenseDao.selectList(new QueryWrapper<SysUserLicenseEntity>().eq("user_id", userId));
        for (SysUserLicenseEntity list : lists){
            if (list.getAppNameTi() == null){
                list.setAppNameTi("");
            }
            if (list.getLicenseTypeNameTi() == null){
                list.setLicenseTypeNameTi("");
            }
            if (list.getOemNameTi() == null){
                list.setOemNameTi("");
            }
            if (list.getProductNameTi() == null){
                list.setProductNameTi("");
            }
        }
        return lists;
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SysUserLicenseEntity> page = this.page(
                new Query<SysUserLicenseEntity>().getPage(params),
                new QueryWrapper<SysUserLicenseEntity>()
                        .like(ObjectUtil.isNotNull(params.get("key")),"key",params.get("key"))
        );
        return new PageUtils(page);
    }

    @Override
    public List<SysUserLicenseEntity> queryListByUserId(Long userId) {
        return sysUserLicenseDao.selectList(new QueryWrapper<SysUserLicenseEntity>().eq("user_id", Convert.toStr(userId)));
    }

    @Override
    public SysUserLicenseEntity info(Map<String, Object> params) {
        return sysUserLicenseDao.selectById((Serializable) params.get("id"));
    }

    @Override
    public boolean save(SysUserLicenseEntity entity) {
        sysUserLicenseDao.insert(entity);
        return false;
    }

    @Override
    public void update(Map<String, Object> params) {
        sysUserLicenseDao.updateById(JSON.parseObject(JSON.toJSONString(params), SysUserLicenseEntity.class));
    }

    @Override
    public void delete(List<Long> ids) {
        if (!ids.isEmpty()) {
            sysUserLicenseDao.deleteBatchIds(ids);
        }
    }

}
