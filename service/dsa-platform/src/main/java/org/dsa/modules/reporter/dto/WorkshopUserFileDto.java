package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicInteger;

@Data
public class WorkshopUserFileDto implements Serializable {

    private Integer userTotal;

    private Integer userGroupTotal;

    private String msg;

    private AtomicInteger updateTotal;

    private AtomicInteger insertTotal;

    private AtomicInteger updateGroupTotal;

    private AtomicInteger insertGroupTotal;
}
