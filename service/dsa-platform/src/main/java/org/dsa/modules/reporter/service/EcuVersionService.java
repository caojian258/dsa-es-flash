package org.dsa.modules.reporter.service;

import org.dsa.modules.reporter.dto.EcuVersionHistoryDto;


public interface EcuVersionService {

    public void addHistory(EcuVersionHistoryDto dto);

    public void updateOverview(EcuVersionHistoryDto dto);

}
