package org.dsa.modules.reporter.controller;

import org.dsa.modules.reporter.bean.OverseaVo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/pdf")
public class PdfController {

    @RequestMapping("/test")
    public String fmIndex(ModelMap modelMap) {

        Map<String, String> map = new HashMap<>();

        map.put("name", "aoppp");
        map.put("desc", "描述");

        // 添加属性 给模版
        modelMap.addAttribute("user", map);

        return "test";
    }

    @RequestMapping("/overview")
    public String overview(ModelMap modelMap){
        // 模板数据
        OverseaVo overseaVo = new OverseaVo();

        overseaVo.setPolicyNo("1234567890123456");
        overseaVo.setHolderName("okok");
        overseaVo.setInsuredName("丽丽张123丽丽张123丽丽张123丽丽张123");
        overseaVo.setBeneficiaryName("测试受益人姓名");
        overseaVo.setBranchName("北京");
        overseaVo.setCompanyName("科索沃公司");
        overseaVo.setDestination("英国,俄罗斯,冰岛,日内瓦,威尼斯小镇");
        overseaVo.setHolderAdress("北京市屋顶后街金融大街14号中国人寿广场xxx曾x101室");
        overseaVo.setHolderPostCode("123456");
        overseaVo.setInsuredBirthday("2013-11-10");
        overseaVo.setInsuredIDNo("123456789012345678");
        overseaVo.setInsuredName("爱新觉罗启星");
        overseaVo.setInsuredPassportNo("测试护照号码123456789");
        overseaVo.setInsuredPhone("13112345678");
        overseaVo.setInsuredPingyinName("aixinjuluoqixing");
        overseaVo.setInsuredSex("女");
        overseaVo.setIssueDate("2013-11-12");
        overseaVo.setPeriod("十一年");
        overseaVo.setPremium("1009.00");
        overseaVo.setRelation("子女");
        overseaVo.setRemarks("这是一张测试保单,仅为测试,学习所用,请勿转载");
        overseaVo.setAccidentalSumInsured("150000");
        overseaVo.setEmergencySumInsured("500000");
        overseaVo.setMedicalSumInsured("220000");
        modelMap.addAllAttributes(overseaVo.fillDataMap());
        return "overseaAssistance";
    }
}
