package org.dsa.modules.remoteDiagnostic.vo.Flash;

import lombok.Data;
import org.dsa.modules.remoteDiagnostic.dto.flash.EcuDIDDto;

import java.io.Serializable;
import java.util.List;

@Data
public class FlashCheckVo implements Serializable {

    private String vin;

    private Integer timestamp;

    private String locale;

    private List<EcuDIDDto> ecuList;
}
