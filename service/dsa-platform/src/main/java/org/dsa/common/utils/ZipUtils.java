package org.dsa.common.utils;



import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.model.enums.CompressionLevel;
import net.lingala.zip4j.model.enums.CompressionMethod;
import net.lingala.zip4j.model.enums.EncryptionMethod;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author weishunxin
 * @since 2023-06-06
 */
public class ZipUtils {

    /**
     * 把文件压缩成zip格式,支持文件夹和文件混合压缩
     * @param filePaths        需要压缩的文件或文件夹
     * @param zipFileName 压缩后的zip文件路径   ,如"D:/test/aa.zip";
     * @param password 密码
     */
    public static boolean compressFiles(List<String> filePaths, String zipFileName, String password) throws ZipException {
        boolean result = false;
        if(filePaths == null || filePaths.size() == 0 || !endWithZip(zipFileName)) {
            return result;
        }

        ZipFile zipFile = new ZipFile(zipFileName);
        ZipParameters parameters = new ZipParameters();
        parameters.setCompressionMethod(CompressionMethod.DEFLATE);
        parameters.setCompressionLevel(CompressionLevel.NORMAL);

        //设置密码
        if (password != null && !password.isEmpty()) {
            parameters.setEncryptFiles(true);
            parameters.setEncryptionMethod(EncryptionMethod.ZIP_STANDARD);
            zipFile.setUseUtf8CharsetForPasswords(true);
            zipFile.setPassword(password.toCharArray());
        }

        List<File> files = new ArrayList<>();
        for (String filePath : filePaths) {
            files.add(new File(filePath));
        }

        ArrayList<File> zipFileList = new ArrayList<>();
        for (File file : files) {
            if(file.isDirectory()) {
                //如果是文件夹的话
                zipFile.addFolder(file, parameters);
            } else {
                //文件的话，最后一起添加
                zipFileList.add(file);
            }
        }

        //添加所有文件
        zipFile.addFiles(zipFileList, parameters);
        return true;
    }

    /**
     * 把zip文件解压到指定的文件夹
     * @param zipFilePath   zip文件路径, 如 "D:/test/aa.zip"
     * @param saveFileDir   解压后的文件存放路径, 如"D:/test/"
     * @param password   解压密码
     * @throws ZipException ZipException
     */
    public static void decompressZip(String zipFilePath, String saveFileDir, String password) throws ZipException {
        if(!endWithZip(zipFilePath)) {
            return;
        }

        ZipFile zipFile = new ZipFile(zipFilePath);
        if (zipFile.isEncrypted()) {
            zipFile.setUseUtf8CharsetForPasswords(true);
            zipFile.setPassword(password.toCharArray());
        }

        zipFile.extractAll(saveFileDir);
    }

    /**
     * 判断文件名是否以.zip为后缀
     * @param fileName        需要判断的文件名
     * @return 是zip文件返回true,否则返回false
     */
    public static boolean endWithZip(String fileName) {
        if(fileName == null || fileName.length() < 4) {
            return false;
        }else{
            return fileName.toUpperCase().endsWith(".ZIP") ;
        }
    }

}
