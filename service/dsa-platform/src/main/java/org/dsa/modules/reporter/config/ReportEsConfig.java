package org.dsa.modules.reporter.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;


@Data
@Configuration
@ConfigurationProperties(prefix = "report.elasticsearch")
public class ReportEsConfig {

    Map<String, String> template;


}
