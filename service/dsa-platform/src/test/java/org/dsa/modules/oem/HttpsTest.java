package org.dsa.modules.oem;

import clojure.lang.Obj;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.dsa.modules.oem.config.Ed25519RestTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import java.util.Map;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class HttpsTest {




    @Test
    public void test() throws Exception {
        String url = "https://101.132.27.25:8443/dsaPlatformApi/oem/cert/getSWTSCert";

//        Gson gson = new Gson();
//        HashMap<String, String> req = new HashMap<>();
//        req.put("vehicle_project_id", ""); //车系id
//        req.put("device_id", "");
//        HttpHeaders requestHeaders = new HttpHeaders();
//        requestHeaders.add("Content-Type", "application/json");
//        requestHeaders.add("Token", "token1690870616159");
////        RequestCallback requestCallback = request -> {
////            HttpHeaders headers = request.getHeaders();
////            headers.set("Content-Type", "application/json");
////            headers.set("Token", "token1690870616159");
////        };
//        HttpEntity<Object> requestEntity = new HttpEntity<>(null, requestHeaders);
//
//        ResponseEntity<Object> exchange = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Object.class);
//        System.out.println(exchange);


        RestTemplate ed25519RestTemplate = Ed25519RestTemplate.createEd25519RestTemplate();

        HttpHeaders headers = new HttpHeaders();
//        TokenUtils.getHuaWeiAccessToken(new HashMap<>());
        headers.set("Content-Type", "application/json");

        ResponseEntity<Map> response = ed25519RestTemplate.postForEntity(url, new HttpEntity<String>("{\"pcid\":\"1\"}", headers), Map.class);
        System.out.println(response.getBody());

    }
}