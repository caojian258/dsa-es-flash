-- client

drop table client_application_history;

drop table client_application_installation cascade;

drop table client_application_overview;

drop table client_application_repair;

drop table client_application_rollback;

drop table client_application_stats;

drop table client_application_uninstallation cascade;

drop table client_application_version cascade;

drop table client_component_back;

drop table client_current_component_info;

drop table client_current_software_info;

drop table client_dependence_info;

drop table client_dependence_overview;

drop table client_dependence_tree;

drop table client_future_component_info;

drop table client_future_software_info;

drop table client_os_history;

drop table client_os_overview;

drop table client_os_protocol_item cascade;

drop table client_software_back;

drop table client_upgrade_result;

drop table client_workshop_user;