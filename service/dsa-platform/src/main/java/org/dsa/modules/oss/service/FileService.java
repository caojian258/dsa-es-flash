package org.dsa.modules.oss.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.oss.dto.FileDto;
import org.dsa.modules.oss.entity.SysFileEntity;
import org.dsa.modules.oss.vo.file.FilePageVo;

public interface FileService {

    public void record(FileDto dto);

    public Page<SysFileEntity> queryPage(FilePageVo vo);

    public SysFileEntity findOneFile(Long id);
}
