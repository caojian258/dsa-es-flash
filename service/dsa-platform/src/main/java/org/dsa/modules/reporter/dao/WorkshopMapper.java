package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.entity.Workshop;

import java.util.Date;
import java.util.List;

@Mapper
public interface WorkshopMapper extends BaseMapper<Workshop>{

    public List<String> selectWorkshopIdByGroup(@Param("groupName") String groupName);

    public void updateLoginTime(@Param("id") String id, @Param("loginTime") Date loginTime);

    public List<String> selectCountry();

    List<String> selectTag();
}
