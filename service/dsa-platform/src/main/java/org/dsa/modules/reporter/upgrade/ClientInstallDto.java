package org.dsa.modules.reporter.upgrade;

import lombok.Data;

import java.util.Date;

@Data
public class ClientInstallDto extends ClientUpgradeDto{

    private String appName;
    private String appVersionName;
    private Integer appVersionNumber;
    private String appPrevVersion;
    private Integer prevVersionNumber;
    private Integer appId;
    private String appType;
    /***
     * 强制更新： 1=force
     * 非强制更新： 0=optional
     */
    private Integer updateType;

    private Date expireDate;

    private Integer expireWarningDays;
}
