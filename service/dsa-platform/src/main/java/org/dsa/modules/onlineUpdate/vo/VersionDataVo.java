package org.dsa.modules.onlineUpdate.vo;

import lombok.Data;

import java.util.List;

@Data
public class VersionDataVo<T> {

    /**
     * 数据源
     */
    private List<T> dataList;

}
