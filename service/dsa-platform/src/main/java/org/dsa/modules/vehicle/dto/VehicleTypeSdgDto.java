package org.dsa.modules.vehicle.dto;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class VehicleTypeSdgDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;

    private String value;

    private String descriptionTi;

}
