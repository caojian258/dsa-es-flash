package org.dsa.modules.reporter.vo.workshop;

import lombok.Data;

import java.io.Serializable;

@Data
public class WorkshopDetailReVo implements Serializable {
    private String id;

}
