package org.dsa.common.aspect;

import com.alibaba.fastjson.JSONObject;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.dsa.common.annotation.SysLog;
import org.dsa.common.exception.RRException;
import org.dsa.common.exception.RemoteApiException;
import org.dsa.common.utils.*;
import org.dsa.modules.sys.entity.SysLogEntity;
import org.dsa.modules.sys.service.SysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * 系统日志，切面处理类
 *
 */
@Aspect
@Slf4j
@Component
public class SysLogAspect {
	@Autowired
	private SysLogService sysLogService;

	@Pointcut("@annotation(org.dsa.common.annotation.SysLog)")
	public void logPointCut() { 
		
	}

	@Around("logPointCut()")
	public Object around(ProceedingJoinPoint point) throws Throwable {
		long beginTime = System.currentTimeMillis();
		//执行方法
		Object result = null;
		try {
			result = point.proceed();
		}catch (RRException e){
			result = R.error(e.getCode(),e.getMsg());
		}catch (RemoteApiException e){
			result = R.error(e.getCode(), e.getMsg()).put("data", e.getData());
		}catch (Exception e){
			log.error(e.getMessage(), e);
			result = R.error(e.getMessage());
		}finally {
			//执行时长(毫秒)
			long time = System.currentTimeMillis() - beginTime;

			//保存日志
			saveSysLog(point, time,result);
		}

		return result;
	}

	private void saveSysLog(ProceedingJoinPoint joinPoint, long time , Object result ) {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();

		SysLogEntity sysLog = new SysLogEntity();
		SysLog syslog = method.getAnnotation(SysLog.class);
		if(syslog != null){
			//注解上的描述
			sysLog.setOperation(syslog.value());
		}

		//请求的方法名
		String className = joinPoint.getTarget().getClass().getName();
		String methodName = signature.getName();
		sysLog.setMethod(className + "." + methodName + "()");

		//请求的参数
		Object[] args = joinPoint.getArgs();
		List<Object> os = new ArrayList<Object>();
		try{
			for(Object o : args){
				if (!o.getClass().getName().equals("org.dsa.common.xss.XssHttpServletRequestWrapper")){
					os.add(o);
				}
			}
			//String params = new Gson().toJson(args);
			String params = JSONObject.toJSONString(os);
			sysLog.setParams(params);
		}catch (Exception e){

		}
		sysLog.setResult(result == null ? null : result.toString());

		//获取request
		HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
		//设置IP地址
		sysLog.setIp(IPUtils.getIpAddr(request));

		String userName = ShiroUtils.getUserName();
		if (StringUtils.isNotBlank(userName)){
			//用户名
			sysLog.setUsername(userName);
			if (Constant.SUPER_ADMIN_USER_NAME.equals(userName)){
				sysLog.setUsername(ShiroUtils.getUserDisplayName());
			}
		}

		sysLog.setDisplayUserName(ShiroUtils.getUserDisplayName());

		sysLog.setTime(time);
		sysLog.setCreateDate(new Date());
		//保存系统日志
		sysLogService.save(sysLog);
	}
}
