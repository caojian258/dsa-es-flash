package org.dsa.modules.reporter.job;

import cn.hutool.core.collection.CollectionUtil;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.reporter.constant.RedisConstant;
import org.dsa.modules.reporter.dto.AppsInfoDto;
import org.dsa.modules.reporter.entity.SystemHistory;
import org.dsa.modules.reporter.service.AppService;
import org.dsa.modules.reporter.service.SystemService;
import org.dsa.modules.reporter.service.WorkshopService;
import org.dsa.modules.reporter.service.WorkshopUserService;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import jakarta.annotation.Resource;


/***
 * jobDataMap 需要tag 从fileLog中获取 执行任务的sql从d_analysis_cube去拿
 */
@Slf4j
@Component
@DisallowConcurrentExecution
public class UpgradeDataMinerJob extends QuartzJobBean {

    @Autowired
    SystemService systemService;

    @Autowired
    AppService appService;

    @Autowired
    RedisUtils redisUtils;

    @Resource
    WorkshopUserService workshopUserService;

    @Resource
    WorkshopService workshopService;


    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.info("SystemDataMinerJob Start................");

        log.info("tag pool thread name: {}", Thread.currentThread().getName());

        Long size = redisUtils.lGetListSize(RedisConstant.TAG_BASE_KEY);

        log.info("tag pool size: {}", size);

        while (size>0){
            String  tag = (String) redisUtils.lPop(RedisConstant.TAG_BASE_KEY);
            if(StringUtils.isEmpty(tag)){
                size--;
                continue;
            }

            AppsInfoDto infoDto = systemService.getApplicationsInfo(tag);

            try {
                if(null != infoDto && !CollectionUtil.isEmpty(infoDto.getInstallDtos())){
                    appService.batchAddInstall(infoDto.getInstallDtos(),infoDto.getWorkshop(),infoDto.getPcid(),
                            infoDto.getTimestamp(), tag);
                }
            }catch (Exception exception){
                log.error("SystemDataMinerJob error: {}", exception.getMessage());
            }

            try {
                if(null != infoDto && !CollectionUtil.isEmpty(infoDto.getVersionDtos())){

                    appService.batchSaveOverview(infoDto.getVersionDtos(), infoDto.getWorkshop(), infoDto.getPcid(),
                            infoDto.getTimestamp(), tag);

                    appService.batchAddVersion(infoDto.getVersionDtos(), infoDto.getWorkshop(), infoDto.getPcid(),
                            infoDto.getTimestamp(), tag);

                }

            }catch (Exception e){
                log.error("SystemDataMinerJob error: {}", e.getMessage());
            }

            SystemHistory history = systemService.getOneHistory(tag);
            if(null != history){
                systemService.saveOverview(history);
                systemService.saveProtocolItem(history);
                workshopUserService.updateLoginTime(history.getUsername() , history.getCollectTime());
                workshopService.updateLoginTime(history.getWorkshop(), history.getCollectTime());
            }
            size--;
        }

        log.info("SystemDataMinerJob End................");
    }
}
