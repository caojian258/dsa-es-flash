package org.dsa.modules.diagnostic.dto;

import lombok.Data;

import java.util.Date;

@Data
public class FlashActionDto {

    String functionCode;

    String actionCode;

    Date startTime;

    Date endTime;

    String results;
}
