package org.dsa.modules.diagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("d_version_record")
public class DVersionRecordEntity implements Serializable
{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 操作类型
     */
    private String editType;
    /**
     * 版本id
     */
    private Long versionId;
    /**
     * 版本状态
     */
    private String status;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建用户
     */
    private String createUserName;
    /**
     * 创建用户id
     */
    private Long createBy;

}
