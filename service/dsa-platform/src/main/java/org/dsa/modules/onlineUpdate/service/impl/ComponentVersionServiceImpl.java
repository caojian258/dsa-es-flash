package org.dsa.modules.onlineUpdate.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.ShiroUtils;
import org.dsa.modules.onlineUpdate.dao.*;
import org.dsa.modules.onlineUpdate.dto.ComponentVersionDto;
import org.dsa.modules.onlineUpdate.entity.*;
import org.dsa.modules.onlineUpdate.po.SoftwareVersionPo;
import org.dsa.modules.onlineUpdate.service.ComponentVersionService;
import org.dsa.modules.onlineUpdate.service.RuleService;
import org.dsa.modules.onlineUpdate.service.VersionRecordService;
import org.dsa.modules.onlineUpdate.utils.PageUtils;
import org.dsa.modules.onlineUpdate.utils.Query;
import org.dsa.modules.onlineUpdate.utils.VersionRecordUtils;
import org.dsa.modules.onlineUpdate.vo.CascadeOptions;
import org.dsa.modules.onlineUpdate.vo.CompVGroupVo;
import org.dsa.modules.onlineUpdate.vo.ComponentVersionVo;
import jakarta.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service("ComponentVersionService")
public class ComponentVersionServiceImpl extends ServiceImpl<ComponentVersionDao, ComponentVersionEntity> implements ComponentVersionService {


    @Autowired
    private CompVGroupDao compVGroupDao;

    @Autowired
    private ComponentDao componentDao;

    @Autowired
    private ComponentVersionDao componentVersionDao;

    @Autowired
    private SwVCompVDao swVCompVDao;
    @Autowired
    private SoftwareVersionDao softwareVersionDao;
    @Autowired
    private RuleService ruleService;
    @Autowired
    private VersionRecordService versionRecordService;


    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Integer componentId = MapUtil.getInt(params, "componentId");
        IPage<ComponentVersionEntity> page = this.page(
                new Query<ComponentVersionEntity>().getPage(params),
                new QueryWrapper<ComponentVersionEntity>()
                        .eq(ObjectUtil.isNotNull(componentId), "component_id", componentId)
        );
        return new PageUtils(page);
    }

    @Override
    public ComponentVersionVo info(Long id) {
        ComponentVersionEntity componentVersion = this.baseMapper.selectById(id);
        List<CompVGroupVo> groups = compVGroupDao.selectByVersionId(id);
        ComponentVersionVo componentVersionVo = new ComponentVersionVo();
        BeanUtil.copyProperties(componentVersion, componentVersionVo, CopyOptions.create().setIgnoreNullValue(true));
        // 禁用选项 —— 当前组件其他版本关联的组件版本
        List<CompVGroupVo> disabledOptions = compVGroupDao.disabledOption(componentVersion.getComponentId(), groups.size() > 0 ? groups.get(0).getGroupId() : "");
        componentVersionVo.setGroups(groups);
        componentVersionVo.setDisabled(disabledOptions);
        return componentVersionVo;
    }

    @Transactional
    @Override
    public void saveOrUpdate2(ComponentVersionDto entity) {
        checkParam(entity);
        if (ObjectUtil.isNull(entity.getId())) {
            entity.setUuid(UUID.randomUUID().toString());
            componentVersionDao.insert(entity);
            // 插入版本关联文件 (新增组)
            if (!entity.getCompVGroup().isEmpty()) {
                Long componentVersionId = componentVersionDao.selectOne(new QueryWrapper<ComponentVersionEntity>()
                                .select("id")
                                .eq("version_name", entity.getVersionName())
                                .eq("component_id", entity.getComponentId()))
                        .getId();
                entity.setId(componentVersionId);
                updateCompVGroup(entity);
            }
            // 保存插入版本操作记录
            VersionRecordUtils.saveOperateRecord(entity.getUuid(), Constant.editType.ADD.getCode(), entity.getStatus());
        } else {
            //更新
            componentVersionDao.updateById(entity);
            boolean release = Objects.equals(entity.getStatus(), Constant.VERSION_STATUS.RELEASE.getCodeInt());
            // 当前版本所在组成员
            List<CompVGroupVo> groupVos = compVGroupDao.selectByVersionId(entity.getId());
            // 有组
            if (!groupVos.isEmpty() && release) {
                // 1.查找组内的组件版本
                List<Long> cVersionIds = groupVos.stream().map(CompVGroupEntity::getComponentVersionId).collect(Collectors.toList());
                // 删除当前组件版本关联的组重新建组
                compVGroupDao.delete(new LambdaQueryWrapper<CompVGroupEntity>().eq(CompVGroupEntity::getGroupId, groupVos.get(0).getGroupId()));
                // 插入组件版本关联关系
                updateCompVGroup(entity);
                // 重新查询新组
                List<CompVGroupVo> newGroups = compVGroupDao.selectByVersionId(entity.getId());
                // 2.查找关联该组的软件版本
                List<SwVCompVEntity> scRelated = swVCompVDao.selectByCVersionIds(cVersionIds);
                List<Long> svcvIds = scRelated.stream().map(SwVCompVEntity::getId).collect(Collectors.toList());
                // 3.先删软件版本关联组件版本数据
                if (!svcvIds.isEmpty()) {
                    swVCompVDao.deleteBatchIds(svcvIds);
                }
                // 是否需要修改 发布后
                // 插入软件组件版本关联, 每个关联该组的软件版本依赖，都需要重新修改
                Set<Long> svIds = scRelated.stream().map(SwVCompVEntity::getSoftwareVersionId).collect(Collectors.toSet());
                svIds.forEach(svId -> {
                    Long softwareId = softwareVersionDao.selectById(svId).getSoftwareId();
                    newGroups.forEach(groupVo -> {
                        boolean exists = swVCompVDao.exists(new QueryWrapper<SwVCompVEntity>()
                                .eq("software_version_id", svId)
                                .eq("component_version_id", groupVo.getComponentVersionId()));
                        if (!exists) {
                            SwVCompVEntity swVCompVEntity = new SwVCompVEntity();
                            swVCompVEntity.setComponentId(groupVo.getComponentId());
                            swVCompVEntity.setComponentVersionId(groupVo.getComponentVersionId());
                            swVCompVEntity.setSoftwareVersionId(svId);
                            swVCompVEntity.setSoftwareId(softwareId);
                            swVCompVDao.insert(swVCompVEntity);
                        }
                    });
                });
            } else {
                // 释放状态更新组
                if (release) {
                    // 没有组，将当前组件加入组
                    CompVGroupEntity only = null;
                    if(!entity.getCompVGroup().isEmpty()){
                        only = entity.getCompVGroup().get(0);
                    }
                    if (!Objects.isNull(only)) {
                        // 只有一个组成员且为当前组件版本不新建组
                        if (!Objects.equals(only.getComponentVersionId(), entity.getId())) {
                            CompVGroupEntity self = CompVGroupEntity.builder().componentId(entity.getComponentId()).componentVersionId(entity.getId()).build();
                            entity.getCompVGroup().add(self);
                            updateCompVGroup(entity);
                        }
                    }
                }
        }
        // 保存版本操作记录
        VersionRecordUtils.saveOperateRecord(entity.getUuid(), Constant.editType.EDIT.getCode(), entity.getStatus());
    }

}

    private void updateCompVGroup(@NotNull ComponentVersionDto entity) {

        List<CompVGroupEntity> compVGroup = CollUtil.newArrayList(entity.getCompVGroup());
        // 校验 关联的组件版本是否已经在某个group中
        Set<String> groupIds = new HashSet<>();
        entity.getCompVGroup().forEach(cv -> {
            CompVGroupEntity compVGroupEntity = compVGroupDao.selectOne(new QueryWrapper<CompVGroupEntity>()
                    .eq("component_version_id", cv.getComponentVersionId()));
            if (compVGroupEntity != null) {
                groupIds.add(compVGroupEntity.getGroupId());
            }
        });
        // 如果关联的组件版本没有组，建新组
        if (groupIds.size() == 0) {
            String groupId = UUID.randomUUID().toString().toUpperCase();
            compVGroup.forEach(cv -> {
                cv.setGroupId(groupId);
                compVGroupDao.insert(cv);
            });
        }
        // 关联的组件版本只有一个组，无组的加入当前组
        if (groupIds.size() == 1) {
            String groupId = new ArrayList<>(groupIds).get(0);
            List<Long> versionIds = compVGroupDao.selectList(new QueryWrapper<CompVGroupEntity>()
                            .select("component_version_id")
                            .eq("group_id", groupId))
                    .stream()
                    .map(CompVGroupEntity::getComponentVersionId)
                    .collect(Collectors.toList());
            compVGroup.forEach(cv -> {
                //不在组内的插入末尾
                if (!versionIds.contains(cv.getComponentVersionId())) {
                    cv.setGroupId(groupId);
                    compVGroupDao.insert(cv);
                }
            });
        }
        // 如果关联的组件版本有多个组，则校验多个组内是否有版本冲突（tips: 在选择关联 queryRelatedVersion() 时校验 ）
        // 多个组，优先级不变，改为同一个groupId，原来没有组的插入到合并的组中
        if (groupIds.size() > 1) {
            String groupId = new ArrayList<>(groupIds).get(0);
            compVGroup.forEach(cv -> {
                cv.setGroupId(groupId);
                CompVGroupEntity compVGroupEntity = compVGroupDao.selectOne(new QueryWrapper<CompVGroupEntity>()
                        .eq("component_version_id", cv.getComponentVersionId()));
                if (compVGroupEntity == null) {
                    compVGroupDao.insert(cv);
                } else {
                    compVGroupEntity.setGroupId(groupId);
                    compVGroupDao.updateById(compVGroupEntity);
                }
            });

        }
    }

    @Override
    public List<CascadeOptions> queryComponentVersionTree(Long componentId) {
        //查询所有 非自身的 未禁用的 组件
        List<Long> disabledOptions;
        if (componentId != null) {
            disabledOptions = compVGroupDao.disabledOption(componentId, null).stream().map(CompVGroupVo::getComponentVersionId).collect(Collectors.toList());
        } else {
            disabledOptions = new ArrayList<>();
        }
        List<ComponentEntity> componentEntities = componentDao.selectList(new QueryWrapper<ComponentEntity>()
//                .ne(ObjectUtil.isNotNull(componentId), "id", componentId)
                .eq("status", 0)
                .orderByDesc("code"));
        List<CascadeOptions> options = new ArrayList<>(16);
        componentEntities.forEach(c -> {
            List<CascadeOptions> children;
            // 查询所有释放状态的组件版本
            children = componentVersionDao.selectList(new QueryWrapper<ComponentVersionEntity>()
                            .eq("component_id", c.getId())
                            .eq("status", 2)
                            .orderByAsc("version_number"))
                    .stream()
                    .map(v -> CascadeOptions.builder().label(v.getVersionName()).disabled(componentId != null && disabledOptions.contains(v.getId())).value(v.getId()).build())
                    .collect(Collectors.toList());
            if (!children.isEmpty()) {
                CascadeOptions childrenOption = CascadeOptions.builder()
                        .label(c.getName())
                        .value(c.getId())
                        .disabled(Objects.equals(c.getId(), componentId))
                        .children(children)
                        .build();
                options.add(childrenOption);
            }
        });
        return options;
    }

    @Override
    public List<List<Long>> queryRelatedVersion(Map<Long, Long> cvMap, Long cVersionId, boolean isReplace) {
        List<CompVGroupEntity> groups = compVGroupDao.selectGroupByCVersionId(cVersionId);
        List<List<Long>> res = new ArrayList<>();
        groups.forEach(cv -> {
            // 与旧组比较
            if (cvMap.containsKey(cv.getComponentId())) {
                // 组与组内组件版本冲突（组件在不同的组存在多个版本） 返回异常信息
                if ((!Objects.equals(cvMap.get(cv.getComponentId()), cv.getComponentVersionId()))) {
                    throw new RRException(Constant.Msg.RELATED_VERSION_CONFLICT_ERROR);
                }
            }
            List<Long> longs = CollUtil.newArrayList(cv.getComponentId(), cv.getComponentVersionId());
            res.add(longs);
        });

        return res;
    }

    @Override
    public void sortCompVersion(List<CompVGroupVo> cVersions) {
        // 在同一个组内的，获取所有优先级，从小到大排序
        List<Integer> priorities = cVersions.stream().map(CompVGroupVo::getPriority).sorted().collect(Collectors.toList());
        for (int i = 0; i < cVersions.size(); i++) {
            CompVGroupEntity compVGroupEntity = cVersions.get(i);
            // 重新设置优先级
            compVGroupEntity.setPriority(priorities.get(i));
            compVGroupDao.updateById(compVGroupEntity);
        }
    }

    private void checkParam(@NotNull ComponentVersionDto entity) {
        if (StringUtils.isBlank(entity.getVersionName().trim())) {
            throw new RRException(Constant.Msg.VERSION_BLANK_ERROR);
        }
        if (ObjectUtil.isNull(entity.getVersionType())) {
            throw new RRException(Constant.Msg.VERSION_TYPE_ERROR);
        }
        if (ObjectUtil.isNull(entity.getComponentId())) {
            throw new RRException(Constant.Msg.VERSION_NO_BIND_ERROR);
        }
        if (ObjectUtil.isNull(entity.getUpdateType())) {
            throw new RRException(Constant.Msg.UNKNOWN_COMPONENT_UPDATE_TYPE_ERROR);
        }
        // 校验组件更新类型
        if (!Arrays.asList(Constant.COMPONENT_UPDATE_TYPE_CHECK_ARR).contains(entity.getUpdateType())) {
            throw new RRException(Constant.Msg.UNKNOWN_COMPONENT_UPDATE_TYPE_ERROR);
        }
        if (StringUtils.isBlank(entity.getUrl())) {
            throw new RRException(Constant.Msg.VERSION_URL_BLANK_ERROR);
        }
        if (ObjectUtil.isNull(entity.getFileSize()) || StringUtils.isBlank(entity.getFileMd5())) {
            throw new RRException(Constant.Msg.FILE_PARAM_ERROR);
        }
        if (ObjectUtil.isNull(entity.getStatus())) {
            throw new RRException(Constant.Msg.CHECK_STATUS_ERROR);
        }
        if (!Constant.VERSION_STATUS.checkStatus(entity.getStatus())) {
            throw new RRException(Constant.Msg.UNKNOWN_VERSION_STATUS_ERROR);
        }
        // 组件版本状态校验
        if (ObjectUtil.isNotNull(entity.getId()) && ObjectUtil.isNotNull(entity.getStatus())) {
            //组件版本状态(变化):开发->测试, 测试->开发,测试->释放,测试->拒绝,释放->拒绝

            ComponentVersionEntity info = this.baseMapper.selectById(entity.getId());
            if (Constant.VERSION_STATUS.DEV.getCodeInt().equals(info.getStatus())) {
                if (!Constant.VERSION_STATUS.TEST.getCodeInt().equals(entity.getStatus()) && !Constant.VERSION_STATUS.DEV.getCodeInt().equals(entity.getStatus())) {
                    throw new RRException(Constant.Msg.VERSION_DEV_STATUS_ERROR);
                }
            }
            if (Constant.VERSION_STATUS.RELEASE.getCodeInt().equals(info.getStatus())) {
                if (!Constant.VERSION_STATUS.REFUSE.getCodeInt().equals(entity.getStatus()) && !Constant.VERSION_STATUS.RELEASE.getCodeInt().equals(entity.getStatus())) {
                    throw new RRException(Constant.Msg.VERSION_RELEASE_STATUS_ERROR);
                }
            }
        }
    }

    @Override
    public List<SoftwareVersionPo.ComponentVersionDependency> getRelatedByVersionId(Long componentVersionId) {
        List<SoftwareVersionPo.ComponentVersionDependency> relates = new ArrayList<>();
        List<CompVGroupVo> groupVos = compVGroupDao.selectByVersionId(componentVersionId);
        groupVos.forEach(groupVo -> relates.add(SoftwareVersionPo.ComponentVersionDependency.builder()
                .componentId(Long.valueOf(groupVo.getCode()))
                .componentName(groupVo.getComponentName())
                .componentVersionNumber(groupVo.getComponentVersionId())
                .componentVersionName(groupVo.getComponentVersionName())
                .build()));
        return relates;
    }

    @Override
    public Boolean hasRequiredComponent(List<Long> componentVersionIds) {
        return componentVersionDao.hasRequiredComponent(componentVersionIds);
    }

    @Override
    public void updateRequiredDependency(List<Long> componentVersionIds) {
        componentVersionDao.updateRequiredComponent(componentVersionIds);
    }

    @Override
    public ComponentVersionVo queryVoByCVersionId(Long id) {
        return componentVersionDao.selectCompVoByCVersionId(id);
    }

    @Override
    public Map<String, Object> queryInfoByCompVersionId(Long versionId) {
        //版本详情
        LambdaQueryWrapper<ComponentVersionEntity> versionQuery = Wrappers.<ComponentVersionEntity>lambdaQuery()
                .select(
                        ComponentVersionEntity::getId,
                        ComponentVersionEntity::getVersionName,
                        ComponentVersionEntity::getVersionType,
                        ComponentVersionEntity::getUpdateType,
                        ComponentVersionEntity::getUrl,
                        ComponentVersionEntity::getStatus,
                        ComponentVersionEntity::getUuid,
                        ComponentVersionEntity::getReleaseNoteTi)
                .eq(ComponentVersionEntity::getId, versionId);
        ComponentVersionEntity version = componentVersionDao.selectOne(versionQuery);
        Integer i = ruleService.hasPublishCompVersion(versionId);
        version.setIsPublish(i==1);
        //版本groups信息
        List<CompVGroupVo> groups = compVGroupDao.selectByVersionId(versionId);
        // 版本操作记录
        List<VersionRecordEntity> records = VersionRecordUtils.queryRecordByUUID(version.getUuid());
        Map<String, Object> res = MapUtil.newHashMap();
        res.put("version", version);
        res.put("groups", groups);
        res.put("records", records);
        return res;
    }

    @Override
    public void copyVersionByVersionId(Long versionId) {
        // 复制组件版本， 复制组件的基本信息
        ComponentVersionEntity componentVersion = componentVersionDao.selectById(versionId);
        // 初始化信息
        componentVersion.setId(null);
        componentVersion.setVersionName(componentVersion.getVersionName() + " copy");
        componentVersion.setStatus(Constant.VERSION_STATUS.DEV.getCodeInt());
        componentVersion.setVersionNumber(null);
        componentVersion.setCreatedAt(null);
        componentVersion.setCreatedUserId(ShiroUtils.getUserId());
        componentVersion.setUpdatedAt(null);
        componentVersion.setUpdatedUserId(null);
        componentVersionDao.insert(componentVersion);
        VersionRecordUtils.saveOperateRecord(componentVersion.getUuid(), Constant.editType.ADD.getCode(), componentVersion.getStatus());
    }

}