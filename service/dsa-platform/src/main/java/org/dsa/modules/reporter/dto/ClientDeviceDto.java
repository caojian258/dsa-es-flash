package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@Data
public class ClientDeviceDto implements Serializable {

    private String pcid;

    private String workshop;

    private Long workshopId;

    private String workshopAddress;

    private Date firstOnlineTime;

    private Date lastOnlineTime;

    private String username;
}
