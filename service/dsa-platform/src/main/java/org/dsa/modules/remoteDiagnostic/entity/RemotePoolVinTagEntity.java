//package org.dsa.modules.remoteDiagnostic.entity;
//
//import com.baomidou.mybatisplus.annotation.IdType;
//import com.baomidou.mybatisplus.annotation.TableId;
//import com.baomidou.mybatisplus.annotation.TableName;
//import lombok.Data;
//import org.dsa.modules.remoteDiagnostic.pojo.BaseEntity;
//
//import java.io.Serializable;
//
//@Data
//@TableName("r_pool_vin_tag")
//public class RemotePoolVinTagEntity implements Serializable {
//
//
//    private static final long serialVersionUID = 1L;
//
//    @TableId(type = IdType.AUTO)
//    private Long id;
//
//    private Long poolId;
//
//    private Long tagId;
//
//
//}
