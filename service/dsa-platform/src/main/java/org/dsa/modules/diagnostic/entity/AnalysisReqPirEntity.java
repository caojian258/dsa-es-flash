package org.dsa.modules.diagnostic.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 诊断功能分析 - 饼状图
 */
@Data
public class AnalysisReqPirEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 功能码
	 */
	private String functionCode;

	/**
	 * actionCode
	 */
	private String actionCode;

	/**
	 * 请求月份
	 */
	private String requestMonth;

	/**
	 * 请求次数
	 */
	private int requestCount;

}
