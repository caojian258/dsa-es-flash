package org.dsa.remote.service;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.ZipUtil;
import org.dsa.common.utils.AESUtils;
import org.junit.Test;
import org.springframework.util.DigestUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.*;
import java.net.URLDecoder;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;

public class FileTest {

    public static void main(String args[]) throws UnsupportedEncodingException {
        String httpUrl = "http://101.132.27.25:8001/dsaPlatformApi/ecu/flash/downloadFile?params=5ZD2fwqGoPcNeN1S9KXdLwsI3qjsTs8nijFPQTWIQDLqw0EEb%2FX7X6xeZh%2BYRBa%2FKI2vhjftEc%2FXgyljBEpsuvrl3mTwSoVZ5o75LbX%2FABs%3D";
        UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(httpUrl).build();
        MultiValueMap<String, String> queryParams = uriComponents.getQueryParams();
        System.out.println(queryParams.get("params").get(0).toString());

        String path = AESUtils.decrypt( URLDecoder.decode(queryParams.get("params").get(0).toString(), StandardCharsets.UTF_8.name()) , AESUtils.AES_KEY);
        System.out.println(StrUtil.cleanBlank(path));

        ZipUtil.zip(FileUtil.file("C:/dsa-doc/zip.zip"), false,
                FileUtil.file("C:/dsa-doc/aliyun-pg.zip"),
                FileUtil.file("C:/dsa-doc/ccc.zip"),
                FileUtil.file("C:/dsa-doc/niutrion.txt"),
                FileUtil.file("C:/dsa-doc/cube.sql")
        );

//        File srcFile = new File("D:\\usr\\temp2\\Session_50244a9d-a111-45af-ad7d-bddbe358a2a5_0xLKM4rr_20221207100507.zip");
//        File desDir = new File("D:\\usr\\temp");
//        split(srcFile, desDir);
    }

    public static void copyFileToDir(File srcFile, File desDir) {
        desDir.mkdirs();
        // 创建配置文件
        File configFile = new File(desDir, srcFile.getName().split("\\.")[0]
                + ".config");
        // 创建目标文件
        File desFile = new File(desDir, srcFile.getName());
        if (!configFile.exists() && desFile.exists()) {
            System.out.println("已下载过该文件！");
            return;
        }
        RandomAccessFile rafSrc = null;
        RandomAccessFile rafDes = null;
        RandomAccessFile rafConfig = null;
        try {
            rafSrc = new RandomAccessFile(srcFile, "r");
            rafDes = new RandomAccessFile(desFile, "rw");
            rafConfig = new RandomAccessFile(configFile, "rw");

            // 设置目标文件和源文件一样长
            rafDes.setLength(srcFile.length());
            // 设置配置的文件长度为8,防止第一次下载是出现EOF 异常
            rafConfig.setLength(8);

            // 从上次下载的位置开始继续下载！
            long pointer = rafConfig.readLong();
            System.out.println("已下载：" + ((float) pointer / srcFile.length())
                    * 100 + "%");
            rafSrc.seek(pointer);
            rafDes.seek(pointer);

            // 单次传输长度设置小点，好观察是否断点续传
            byte[] buffer = new byte[32];
            int len = -1;
            // 每次复制的开始，必须把源文件的指针和目标文件的指针从上次断开的位置去读
            while ((len = rafSrc.read(buffer)) != -1) {
                rafDes.write(buffer, 0, len);
                // 在配置文件写的时候，每次使文件指针移动到最初的位置 --> 这样永远对只会存储前8个字节
                rafConfig.seek(0);
                // 每复制一次之和，赶紧记录下文件指针的位置，以备断点续传使用。
                rafConfig.writeLong(rafSrc.getFilePointer());
            }

        } catch (IOException e) {
            System.out.println(e);
        } finally {
            try {
                rafSrc.close();
                rafDes.close();
                rafConfig.close();
            } catch (IOException e) {
                System.out.println(e);
            }
            // 在流关闭之后删除配置文件
            System.out.println("下载成功！");
            configFile.delete();
        }
    }

    public static void split(File srcFile, File desDir)  {


        try {
            FileInputStream in = new FileInputStream(srcFile);
            FileOutputStream out = null;
            FileChannel inChannel = in.getChannel();
            FileChannel outChannel = null;

            // 将MB单位转为为字节B
            long m = 1024 * 512;
            // 计算最终会分成几个文件
            int count = (int) (srcFile.length() / m);
            for (int i = 0; i <= count; i++) {
                // 生成文件的路径
                String t = desDir + "/" + i;
                try {
                    out = new FileOutputStream(new File(t));
                    outChannel = out.getChannel();
                    // 从inChannel的m*i处，读取固定长度的数据，写入outChannel
                    if (i != count)
                        inChannel.transferTo(m * i, m, outChannel);
                    else// 最后一个文件，大小不固定，所以需要重新计算长度
                        inChannel.transferTo(m * i, srcFile.length() - m * count, outChannel);
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    out.close();
                    outChannel.close();
                }
            }
            in.close();
            inChannel.close();


        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Test
    public void md5Test() throws IOException {
        String path = "C:\\Users\\HKJ\\Downloads\\vehicle_1679366874281 (1).xlsx";
        File file = new File(path);

        String md5 = DigestUtils.md5DigestAsHex(new FileInputStream(file));

        System.out.println(md5);
    }
}
