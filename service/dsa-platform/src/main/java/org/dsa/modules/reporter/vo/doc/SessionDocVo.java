package org.dsa.modules.reporter.vo.doc;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import org.dsa.modules.reporter.document.ElasticEntity;

import java.util.Date;

@Data
public class SessionDocVo extends ElasticEntity {

    private String sessionId;

    private String vin;

//    private String logPath;

    private String logName;

    //0=reserved；1=预诊断；2=实时诊断
    private String diagCategory;

    //“001”=车载诊断；“002”=“离线诊断”；“003”=远程诊断
    private String diagSource;


    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    private Long userId;

    private String userName;

    private Long workshopId;

    private String workshop;

    private String pcid;

    private Integer sourceVersion;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date lastUpdateTime;
}
