package org.dsa.modules.sys.service.impl;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.MapUtils;
import org.dsa.modules.sys.dao.SysMenuDao;
import org.dsa.modules.sys.entity.SysMenuEntity;
import org.dsa.modules.sys.enums.SystemEnum;
import org.dsa.modules.sys.service.SysMenuService;
import org.dsa.modules.sys.service.SysRoleMenuService;
import org.dsa.modules.sys.service.SysUserService;
import org.dsa.modules.sys.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


@Service("sysMenuService")
public class SysMenuServiceImpl extends ServiceImpl<SysMenuDao, SysMenuEntity> implements SysMenuService {
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private SysRoleMenuService sysRoleMenuService;
	
	@Override
	public List<SysMenuEntity> queryListParentId(Long parentId, List<Long> menuIdList) {
		List<SysMenuEntity> menuList = queryListParentId(parentId);
		if(menuIdList == null){
			return menuList;
		}
		
		List<SysMenuEntity> userMenuList = new ArrayList<>();
		for(SysMenuEntity menu : menuList){
			if(menuIdList.contains(menu.getMenuId())){
				userMenuList.add(menu);
			}
		}
		return userMenuList;
	}

	@Override
	public List<SysMenuEntity> queryListParentId(Long parentId) {
		return baseMapper.queryListParentId(parentId);
	}

	@Override
	public List<SysMenuEntity> queryNotButtonList() {
		return baseMapper.queryNotButtonList();
	}

	@Override
	public List<SysMenuEntity> getUserMenuList(Long userId) {
		//系统管理员，拥有最高权限
		if(!ShiroUtils.getUserName().equals(Constant.SUPER_ADMIN_USER_NAME)){
			return getAllMenuList(null);
		}
		
		//用户菜单列表
		List<Long> menuIdList = sysUserService.queryAllMenuId(userId);

		return getAllMenuList(menuIdList);
	}

	@Override
	public void delete(Long menuId){
		//删除菜单
		this.removeById(menuId);
		//删除菜单与角色关联
		sysRoleMenuService.removeByMap(new MapUtils().put("menu_id", menuId));
	}

	/**
	 * 根据用户角色获取菜单
	 */
	public List<SysMenuEntity> getUserMenuListByRoleList(List<String> roleList, Integer owningSystem) {
		//用户菜单列表
		List<Long> menuIdList = sysRoleMenuService.queryRoleMenu(roleList,owningSystem);

		//获取菜单树
		List<SysMenuEntity> list =  getNotHaveDiagnosticMenuList(menuIdList);

		//过滤诊断菜单
		//list.removeIf(vo->vo.getUrl().startsWith(Constant.DIAGNOSTIC_STR));

		return list;
	}

	/**
	 * 根据用户角色获取诊断菜单
	 */
	public List<SysMenuEntity> getDiagnosticUserMenuListByRoleList(List<String> roleList, String vin) {
		//用户菜单列表
		List<Long> menuIdList = sysRoleMenuService.queryRoleMenu(roleList, SystemEnum.DSA_PLATFORM.getCode());
		//获取菜单树
		List<SysMenuEntity> list =  getHaveDiagnosticMenuList(menuIdList,vin);

		return list;
	}

	/**
	 * 获取所有菜单列表
	 */
	private List<SysMenuEntity> getAllMenuList(List<Long> menuIdList){
		//查询根菜单列表
		List<SysMenuEntity> menuList = queryListParentId(0L, menuIdList);
		//递归获取子菜单
		getMenuTreeList(menuList, menuIdList);
		
		return menuList;
	}

	/**
	 * 递归
	 */
	private List<SysMenuEntity> getMenuTreeList(List<SysMenuEntity> menuList, List<Long> menuIdList){
		List<SysMenuEntity> subMenuList = new ArrayList<SysMenuEntity>();
		
		for(SysMenuEntity entity : menuList){
			//目录
			if(entity.getType() == Constant.MenuType.CATALOG.getValue()){
				entity.setList(getMenuTreeList(queryListParentId(entity.getMenuId(), menuIdList), menuIdList));
			}
			subMenuList.add(entity);
		}
		
		return subMenuList;
	}


	/**
	 * 获取菜单列表  过滤诊断菜单
	 */
	private List<SysMenuEntity> getNotHaveDiagnosticMenuList(List<Long> menuIdList){
		//查询根菜单列表
		List<SysMenuEntity> menuList = queryListParentId(0L, menuIdList);
		//过滤诊断菜单
		menuList.removeIf(vo->vo.getUrl().startsWith(Constant.DIAGNOSTIC_STR));

		//递归获取子菜单
		getNotHaveDiagnosticMenuTreeList(menuList, menuIdList);

		return menuList;
	}

	/**
	 * 递归
	 */
	private List<SysMenuEntity> getNotHaveDiagnosticMenuTreeList(List<SysMenuEntity> menuList, List<Long> menuIdList){
		List<SysMenuEntity> subMenuList = new ArrayList<SysMenuEntity>();

		for(SysMenuEntity entity : menuList){
			//如果是url 包函诊断,过滤不返回
			if(StringUtils.isNotEmpty(entity.getUrl())){
				if(entity.getUrl().startsWith(Constant.DIAGNOSTIC_STR)){
					continue;
				}
			}

			//目录
			if(entity.getType() == Constant.MenuType.CATALOG.getValue()){
				entity.setList(getNotHaveDiagnosticMenuTreeList(queryListParentId(entity.getMenuId(), menuIdList), menuIdList));
			}
			subMenuList.add(entity);
		}
		return subMenuList;
	}

	/**
	 * 获取菜单列表  过滤诊断菜单
	 */
	private List<SysMenuEntity> getHaveDiagnosticMenuList(List<Long> menuIdList,String vin){
		//查询根菜单列表
		List<SysMenuEntity> menuList = queryListParentId(0L, menuIdList);

		//递归获取子菜单
		getHaveDiagnosticMenuTreeList(menuList, menuIdList,vin);

		return menuList;
	}

	/**
	 * 递归
	 */
	private List<SysMenuEntity> getHaveDiagnosticMenuTreeList(List<SysMenuEntity> menuList, List<Long> menuIdList,String vin){
		List<SysMenuEntity> subMenuList = new ArrayList<SysMenuEntity>();

		for(SysMenuEntity entity : menuList){
			//如果是url 包函诊断,过滤不返回
			if(StringUtils.isNotEmpty(entity.getUrl())){
				if (entity.getUrl().startsWith(Constant.DIAGNOSTIC_STR)) {
					JSONObject jsonObject;
					if(StringUtils.isNotEmpty(entity.getQuery())) {
						jsonObject = JSONObject.parseObject(entity.getQuery());
					}else{
						jsonObject = new JSONObject();
					}
					jsonObject.put("vin", vin);
					entity.setQuery(jsonObject.toJSONString());
				}
			}

			//目录
			if(entity.getType() == Constant.MenuType.CATALOG.getValue()){
				entity.setList(getHaveDiagnosticMenuTreeList(queryListParentId(entity.getMenuId(), menuIdList), menuIdList,vin));
			}
			subMenuList.add(entity);
		}

		return subMenuList;
	}

	/**
	 * 客户端菜单列表
	 *
	 * @param roleList roleList
	 * @return List<SysMenuEntity>
	 */
	@Override
	public List<String> clientMenus(List<String> roleList) {
		if (CollectionUtils.isEmpty(roleList)) {
			log.warn("role is empty.");
			return Collections.emptyList();
		}
		List<Long> menuIds = sysRoleMenuService.queryRoleMenu(roleList, SystemEnum.DSA_PLATFORM.getCode());
		if (CollectionUtils.isEmpty(menuIds)) {
			log.warn("menu is empty.");
			return Collections.emptyList();
		}

		LambdaQueryWrapper<SysMenuEntity> qw = Wrappers.lambdaQuery();
		qw.eq(SysMenuEntity::getUseTo, 1).in(SysMenuEntity::getMenuId, menuIds);

		List<SysMenuEntity> menuList = baseMapper.selectList(qw);
		return menuList.stream().map(SysMenuEntity::getName).collect(Collectors.toList());
	}

}
