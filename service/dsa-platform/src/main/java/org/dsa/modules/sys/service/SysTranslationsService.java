package org.dsa.modules.sys.service;

import java.util.List;
import java.util.Map;

public interface SysTranslationsService {

    /***
     * 翻译列表
     * @param tis
     * @param locale
     * @return
     */
    public Map<String, String> translationTis(List<String> tis, String locale);

    /***
     * 单个翻译
     * @param ti
     * @param locale
     * @return
     */
    public String translationTi(String ti, String locale);

}
