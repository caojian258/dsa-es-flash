package org.dsa.modules.sys.service;

import clojure.lang.Obj;
import com.baomidou.mybatisplus.extension.service.IService;
import org.dsa.common.utils.PageUtils;
import org.dsa.modules.sys.entity.SysUserEntity;

import java.util.List;
import java.util.Map;


/**
 * 系统用户
 *
 */
public interface SysUserService extends IService<SysUserEntity> {

	SysUserEntity getUserByUserId(Long userId);

	PageUtils queryPage(Map<String, Object> params);
	List <SysUserEntity>  queryUserList(Map<String, Object> params);
	List<SysUserEntity> querySelectedPage(Map<String, Object> params);


	/**
	 * 查询用户的所有权限
	 * @param userId  用户ID
	 */
	List<String> queryAllPerms(Long userId);
	
	/**
	 * 查询用户的所有菜单ID
	 */
	List<Long> queryAllMenuId(Long userId);

	/**
	 * 根据用户名，查询系统用户
	 */
	SysUserEntity queryByUserName(String username);
	
	/**
	 * 根据用户ID，查询系统用户
	 * @param userId
	 * @return
	 */
	SysUserEntity queryByUserId(String userId);

	/**
	 * 查找变更人员组信息
	 */
	SysUserEntity getUserGroupInfo(Long userId);
	
	/**
	 * 保存用户
	 */
	void saveUser(SysUserEntity user);
	
	/**
	 * 修改用户
	 */
	void update(SysUserEntity user);
	
	/**
	 * 删除用户
	 */
	void deleteBatch(Long[] userIds);

	/**
	 * 修改密码
	 * @param userId       用户ID
	 * @param password     原密码
	 * @param newPassword  新密码
	 */
	boolean updatePassword(Long userId, String password, String newPassword);

	/**
	 * 取用户列表
	 * @return
	 */
	List<SysUserEntity> getUserList(String chineseName);

    List<String> queryUserByRole(int roleId , Long workshopId, Long groupId);

	void removeNotInIds(List<String> userIdList, String workshopId);

	void saveUserInfo(SysUserEntity sysUserEntity);

	List<String> queryUserNameByRole(int roleId, Long workshopId, Long groupId);

	List<SysUserEntity> queryUserEntityByRole(int value, Long workshopId, Long groupId);
	/**
	 * 根据经销商查询用户
	 * @return
	 */
    List<Long> queryUserIdByWorkshopId(long workshopId);


	/***
	 * 批量激活用户
	 * @param userIds
	 */
	void enableBatch(Long[] userIds);

	List<Map<String, Object>> workshopList(String name);
}
