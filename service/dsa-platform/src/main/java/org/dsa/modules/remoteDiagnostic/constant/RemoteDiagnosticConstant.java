package org.dsa.modules.remoteDiagnostic.constant;

public class RemoteDiagnosticConstant {

    /***
     * 数据处理 队列
     */
    public final static String VIN_CODE_FLAG = "vin:code:excel:";


    public final static String REMOTE_DIAGNOSIS = "remote:diagnosis:";

    /***
     * 任务相关
     */
    public final static String CAMPAIGN_KEY = "remote:diagnosis:campaign:";
    public final static String CAMPAIGN_ITEM_KEY = "remote:diagnosis:campaign:item:";

    public final static String RECORD_KEY = "remote:diagnosis:record:";
    public final static String RECORD_ITEM_KEY = "remote:diagnosis:record:item:";

    public final static String TASK_FILE_KEY = "remote:diagnosis:file:";
    public final static String TASK_FILE_ITEM_KEY = "remote:diagnosis:file:item:";

    public final static String TASK_ACTIVE_KEY = "remote:diagnosis:active:";
    public final static String TASK_ACTIVE_ITEM_KEY = "remote:diagnosis:active:item:";

    public final static String TASK_OVERVIEW_KEY = "remote:diagnosis:overview:";
    public final static String TASK_OVERVIEW_ITEM_KEY = "remote:diagnosis:overview:item:";

    public final static String TASK_OVERVIEW_MATCH_KEY = "match";
    public final static String TASK_OVERVIEW_DOWNLOAD_KEY = "download";
    public final static String TASK_OVERVIEW_SUCCESS_KEY = "success";
    public final static String TASK_OVERVIEW_ACCEPTANCE_KEY = "acceptance";
    public final static String TASK_OVERVIEW_CANCEL_KEY = "cancel";
    public final static String TASK_OVERVIEW_REJECT_KEY = "reject";
    public final static String TASK_OVERVIEW_FAIL_KEY = "fail";

    /***
     * 车辆相关
     */
    public final static String VEHICLE_POOL_KEY = "remote:vehicle:pool:";
    public final static String VEHICLE_POOL_ITEM_KEY = "remote:vehicle:pool:item:";

    public final static String VEHICLE_MODEL_AND_PARENT = "remote:vehicle:types:";

    /**
     * 匹配到的车辆
     */
    public final static String VEHICLE_MATCH_POOL = "remote:vehicle:match:";
    public final static String VEHICLE_MATCH_POOL_VIN = "vin:";


    /***
     * 配置表相关
     */
    public final static String VEHICLE_POOL_CONFIG_KEY = "remote:vehicle:pool:config:";

    /**
     * 锁 key
     */
    public final static String CAMPAIGN_ACTIVE_LOCK = "campaign:active:lock:";

    public final static String CAMPAIGN_OVERVIEW_LOCK = "campaign:overview:lock:";

    public final static String OVERVIEW_DATA_UPDATE_LOCK = "overview:update:lock";

    /**
     * 全局配置
     */
    public final static String VEHICLE_POOL_LOCK = "pool:lock";

    /**
     * ecu 灌装证书
     */
    public final static String ECU_FILLING_CERTIFICATE = "ecu:filling:certificate";


    public final static String SOFT_VERSION_SYNC_VEHICLE = "soft:version:sync:vehicle";


    /**
     * 过期时间  1个月
     */
    public final static Long ONE_MONTH_TIMEOUT = 60 * 60 * 24 * 30L;

    // 一小时
    public final static Long AN_HOUR_TIMEOUT = 60 * 60L;

    // 30分钟
    public final static Long HALF_AN_HOUR_TIMEOUT = 30 * 60L;

    // 一星期
    public final static Long SEVEN_DAYS_TIMEOUT = 60 * 60 * 24 * 7L;

    // 一天
    public final static Long ONE_DAY_TIMEOUT = 60 * 60 * 24L;
}
