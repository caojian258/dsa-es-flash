package org.dsa.modules.remoteDiagnostic.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticFunctionEntity;
import org.dsa.modules.remoteDiagnostic.vo.CheckNameReqVo;
import org.dsa.modules.remoteDiagnostic.vo.CheckStatusReqVo;
import org.dsa.modules.remoteDiagnostic.vo.DiagnosticFunctionPageVo;

import java.util.List;

public interface DiagnosticFunctionService {

    Page<DiagnosticFunctionEntity> selectPage(DiagnosticFunctionPageVo vo);

    DiagnosticFunctionEntity getInfo(Long id);

    void saveFun(DiagnosticFunctionEntity fun);

    void updateFun(DiagnosticFunctionEntity fun);

    // 获取所有诊断功能
    List<DiagnosticFunctionEntity> getFunctions();

    Boolean check(CheckNameReqVo vo);

    List<String> getFunGroups();

    Boolean checkStatus(CheckStatusReqVo vo);
}
