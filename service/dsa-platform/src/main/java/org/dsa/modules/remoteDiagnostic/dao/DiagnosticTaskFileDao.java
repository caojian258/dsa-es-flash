package org.dsa.modules.remoteDiagnostic.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskFileEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteEcuGroupEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuEntity;

import java.util.List;

@Mapper
public interface DiagnosticTaskFileDao extends BaseMapper<DiagnosticTaskFileEntity> {

    @Insert("<script>"
            + " insert into r_task_file (task_id,file_id) values "
            + " <foreach collection='files' item='item' separator=','> "
            + " (#{task},#{item}) "
            + " </foreach>"
            + " </script>")
    void inserts(@Param("task") Long task, @Param("files") List<Long> files);

    @Select("select i.file_id from r_task_file i where i.task_id = #{task} ")
    List<Long> selectFiles(@Param("task") Long task);
}
