package org.dsa.modules.remoteDiagnostic.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.common.validator.group.UpdateGroup;
import org.dsa.modules.remoteDiagnostic.pojo.BaseEntity;


import java.io.Serializable;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("r_vehicle_ecu_group")
public class RemoteVehicleEcuGroupEntity extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;


    @NotBlank(message = "名称不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String groupName;

    private String descriptionTi;

    private String shortNameTi;

    @NotNull(message = "状态不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private Integer status;

    @TableField(exist = false)
    private Long platformId;

    @TableField(exist = false)
    private String ecuName;

    @TableField(exist = false)
    private Long ecuId;

    @TableField(exist = false)
    private RemoteVehicleVersionEcuGroupEntity vehicleEcuGroupVersion;

    @TableField(exist = false)
    private List<RemoteVehicleEcuEntity> ecus;

    @TableField(exist = false)
    private List<RemoteVehicleEcuVersionEntity> ecuVersions;

    @TableField(exist = false)
    private List<RemoteEcuIdentificationInfoEntity> identifications;

    @TableField(exist = false)
    private RemoteVehicleEcuImageEntity image;
}
