package org.dsa.modules.onlineUpdate.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.onlineUpdate.entity.SwVCompVEntity;
import org.dsa.modules.onlineUpdate.vo.CompVGroupVo;
import org.dsa.modules.onlineUpdate.vo.ComponentVersionVo;

import java.util.List;

/**
 * ${comments}
 * 
 */
@Mapper
public interface SwVCompVDao extends BaseMapper<SwVCompVEntity> {

    List<CompVGroupVo> selectBySVersionId(Long id);
    List<SwVCompVEntity> selectByCVersionIds(@Param("cVersionIds") List<Long> cVersionIds);
    List<ComponentVersionVo> selectAllComponentVersion(@Param("softwareId") Long softwareId ,@Param("versionNumber") Integer versionNumber);

    Long selectNewestCVersion(@Param("softwareId") Long softwareId, @Param("componentId") Long componentId);
}
