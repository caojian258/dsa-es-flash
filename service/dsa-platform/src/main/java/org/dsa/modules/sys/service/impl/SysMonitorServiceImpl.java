package org.dsa.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.dsa.modules.sys.dao.SysMonitorDao;
import org.dsa.modules.sys.service.SysMonitorService;
import org.dsa.modules.sys.vo.monitor.MonitorVo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class SysMonitorServiceImpl extends ServiceImpl<SysMonitorDao, Object> implements SysMonitorService {

    @Override
    public List<Map<String, String>> getCountTable(MonitorVo monitorVo) {
        return baseMapper.countTable(monitorVo);
    }

}
