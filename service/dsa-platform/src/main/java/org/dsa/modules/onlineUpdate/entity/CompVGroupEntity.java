package org.dsa.modules.onlineUpdate.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 组件版本与组件版本之间的关联关系
 * 
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("o_comp_v_group")
public class CompVGroupEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 自增id
	 */
	@TableId(type = IdType.AUTO)
	private Long id;

	/**
	 * 组件版本GroupId
	 */
	private String groupId;
	/**
	 * 组件id
	 */
	private Long componentId;
	/**
	 * 组件版本id
	 */
	private Long componentVersionId;
	/**
	 * 组件版本安装优先级
	 */
	private Integer priority;


}
