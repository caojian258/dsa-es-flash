package org.dsa.common.utils;

import org.dsa.common.pojo.CodeMessage;
import org.springframework.util.StringUtils;

import javax.net.ssl.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.util.Map;
import java.util.Map.Entry;

/**
 * http、https 请求工具类
 */
public class HttpUtils {

    private static final String DEFAULT_CHARSET = "UTF-8";

    private static final String _GET = "GET"; // GET
    private static final String _POST = "POST";// POST
    public static final int DEF_CONN_TIMEOUT = 30000;
    public static final int DEF_READ_TIMEOUT = 30000;

    public static String KEY_STORE_FILE="d:/usr/file/ssl/client.p12";
    public static String KEY_STORE_PASS="XW50#N6s";

    /**
     * 初始化http请求参数
     *
     * @param url
     * @param method
     * @param headers
     * @return
     * @throws Exception
     */
    private static HttpURLConnection initHttp(String url, String method,
                                              Map<String, String> headers) throws Exception {
        URL _url = new URL(url);
        HttpURLConnection http = (HttpURLConnection) _url.openConnection();
        // 连接超时
        http.setConnectTimeout(DEF_CONN_TIMEOUT);
        // 读取超时 --服务器响应比较慢，增大时间
        http.setReadTimeout(DEF_READ_TIMEOUT);
        http.setUseCaches(false);
        http.setRequestMethod(method);
        http.setRequestProperty("Content-Type","application/json;charset=UTF-8");
        if (null != headers && !headers.isEmpty()) {
            for (Entry<String, String> entry : headers.entrySet()) {
                http.setRequestProperty(entry.getKey(), entry.getValue());
            }
        }
        http.setDoOutput(true);
        http.setDoInput(true);
        http.connect();
        return http;
    }

    /**
     * 初始化http请求参数
     *
     * @param url
     * @param method
     * @return
     * @throws Exception
     */
    private static HttpsURLConnection initHttps(String url, String method,
                                                Map<String, String> headers) throws Exception {
        System.setProperty("jdk.tls.client.protocols", "TLSv1.2");
        KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
        kmf.init(getkeyStore(),KEY_STORE_PASS.toCharArray());
        KeyManager[] keyManagers = kmf.getKeyManagers();

        TrustManager tm = new SSLTrustManager();
        TrustManager[] trustAllCerts = new TrustManager[]{tm};

        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(keyManagers, trustAllCerts, new SecureRandom());
        // 从上述SSLContext对象中得到SSLSocketFactory对象
        SSLSocketFactory ssf = sslContext.getSocketFactory();
        URL _url = new URL(url);
        HttpsURLConnection http = (HttpsURLConnection) _url.openConnection();
        // 设置域名校验
        http.setHostnameVerifier(new HostnameVerifier() {
                                     @Override
                                     public boolean verify(String hostname, SSLSession session) {
                                         return true;
                                     }
                                 }
        );
        // 连接超时
        http.setConnectTimeout(DEF_CONN_TIMEOUT);
        // 读取超时 --服务器响应比较慢，增大时间
        http.setReadTimeout(DEF_READ_TIMEOUT);
        http.setUseCaches(false);
        http.setRequestMethod(method);
        http.setRequestProperty("Content-Type","application/json;charset=UTF-8");

        if (null != headers && !headers.isEmpty()) {
            for (Entry<String, String> entry : headers.entrySet()) {
                http.setRequestProperty(entry.getKey(), entry.getValue());
            }
        }
        http.setSSLSocketFactory(ssf);
        http.setDoOutput(true);
        http.setDoInput(true);
        http.connect();
        return http;
    }

    /**
     *
     * @description 功能描述: get 请求
     * @return 返回类型:
     * @throws Exception
     */
    public static CodeMessage get(String url, Map<String, String> params, Map<String, String> headers) {
        CodeMessage codeMessage = new CodeMessage();

        try{
            HttpURLConnection http = null;

            if (isHttps(url)) {
                http = initHttps(initParams(url, params), _GET, headers);
            } else {
                http = initHttp(initParams(url, params), _GET, headers);
            }
            InputStream in = http.getInputStream();
            BufferedReader read = new BufferedReader(new InputStreamReader(in,
                    DEFAULT_CHARSET));
            String valueString = null;
            StringBuffer bufferRes = new StringBuffer();
            // 优化，不能光使用!=null做判断。有为""的情况，防止多次空循环
            while (!StringUtils.isEmpty(valueString = read.readLine())) {
                bufferRes.append(valueString);
            }

            codeMessage.setCode(String.valueOf(http.getResponseCode()));

            in.close();
            if (http != null) {
                http.disconnect();// 关闭连接
            }
            codeMessage.setMessage(bufferRes.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return codeMessage;
    }

    public static CodeMessage get(String url){
        return get(url, null);
    }

    public static CodeMessage get(String url, Map<String, String> params) {
        return get(url, params, null);
    }

    public static CodeMessage post(String url, String params) {
        CodeMessage codeMessage = new CodeMessage();
        HttpURLConnection http = null;
        try {
            if (isHttps(url)) {
                http = initHttps(url, _POST, null);
            } else {
                http = initHttp(url, _POST, null);
            }
            OutputStream out = http.getOutputStream();
            out.write(params.getBytes(DEFAULT_CHARSET));
            out.flush();
            out.close();

            codeMessage.setCode(String.valueOf(http.getResponseCode()));

            InputStream in = http.getInputStream();
            BufferedReader read = new BufferedReader(new InputStreamReader(in,
                    DEFAULT_CHARSET));
            String valueString = null;
            StringBuffer bufferRes = new StringBuffer();
            while ((valueString = read.readLine()) != null) {
                bufferRes.append(valueString);
            }
            in.close();

            if (http != null) {
                http.disconnect();// 关闭连接
            }
            codeMessage.setMessage(bufferRes.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return codeMessage;
    }

    public static CodeMessage post(String url, String params,Map<String, String> headers) throws Exception {
        CodeMessage codeMessage = new CodeMessage();

        HttpURLConnection http = null;
        if (isHttps(url)) {
            http = initHttps(url, _POST, headers);
        } else {
            http = initHttp(url, _POST, headers);
        }
        OutputStream out = http.getOutputStream();
        out.write(params.getBytes(DEFAULT_CHARSET));

        out.flush();
        out.close();

        codeMessage.setCode(String.valueOf(http.getResponseCode()));

        InputStream in = http.getInputStream();
        BufferedReader read = new BufferedReader(new InputStreamReader(in,
                DEFAULT_CHARSET));
        String valueString = null;
        StringBuffer bufferRes = new StringBuffer();
        while ((valueString = read.readLine()) != null) {
            bufferRes.append(valueString);
        }
        in.close();
        if (http != null) {
            http.disconnect();// 关闭连接
        }

        codeMessage.setMessage(bufferRes.toString());
        return codeMessage;
    }

    /**
     * 功能描述: 构造请求参数
     *
     * @return 返回类型:
     * @throws Exception
     */
    public static String initParams(String url, Map<String, String> params)
            throws Exception {
        if (null == params || params.isEmpty()) {
            return url;
        }
        StringBuilder sb = new StringBuilder(url);
        if (url.indexOf("?") == -1) {
            sb.append("?");
        }
        sb.append(map2Url(params));
        return sb.toString();
    }

    /**
     * map构造url
     *
     * @return 返回类型:
     * @throws Exception
     */
    public static String map2Url(Map<String, String> paramToMap)
            throws Exception {
        if (null == paramToMap || paramToMap.isEmpty()) {
            return null;
        }
        StringBuffer url = new StringBuffer();
        boolean isfist = true;
        for (Entry<String, String> entry : paramToMap.entrySet()) {
            if (isfist) {
                isfist = false;
            } else {
                url.append("&");
            }
            url.append(entry.getKey()).append("=");
            String value = entry.getValue();
            if (!StringUtils.isEmpty(value)) {
                url.append(URLEncoder.encode(value, DEFAULT_CHARSET));
            }
        }
        return url.toString();
    }

    /**
     * 检测是否https
     *
     * @param url
     */
    private static boolean isHttps(String url) {
        return url.startsWith("https");
    }



    public static KeyStore getkeyStore(){
        KeyStore keySotre=null;
        try {
            keySotre = KeyStore.getInstance("PKCS12");
            File file = new File(KEY_STORE_FILE);
            FileInputStream fis = new FileInputStream(file);
            keySotre.load(fis, KEY_STORE_PASS.toCharArray());
            fis.close();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return keySotre;
    }
    public static void main(String[] args) {
        CodeMessage str = new CodeMessage();
        try {
            str = post("https://101.132.27.25:8443/dsaPlatformApi/oem/cert/getSWTSCert", "{\"pcid\":\"1\"}");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        System.out.println(str.getCode()+str.getMessage());
    }

}