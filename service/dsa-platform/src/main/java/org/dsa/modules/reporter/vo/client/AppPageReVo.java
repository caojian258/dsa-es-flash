package org.dsa.modules.reporter.vo.client;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.modules.reporter.pojo.PageParam;

import java.util.List;


@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class AppPageReVo extends PageParam {

    /***
     * 设备ID
     */
    private String pcid;

    /***
     * 经销商名称
     */
    private String workshop;

    /***
     * 应用名称
     */
    private String application;

    /***
     * 应用ID
     */
    private Integer appId;

    /***
     * 开始时间
     */
    private String startDate;

    /***
     * 应用类型 software or component
     */
    private String type;

    /***
     * 版本
     */
    private String version;

    /***
     * 时间段
     */
    List<String> taskTime;

    private String message;

}
