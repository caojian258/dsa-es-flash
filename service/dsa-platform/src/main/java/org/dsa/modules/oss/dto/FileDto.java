package org.dsa.modules.oss.dto;

import lombok.Data;

@Data
public class FileDto {

    /***
     * 分类
     */
    private String category;

    /***
     * 文件名称
     */
    private String name;

    /**
     * 文件路径
     */
    private String url;

    /**
     * 文件大小 单位:byte
     */
    private Long fileSize;

    /**
     * 文件Md5
     */
    private String fileMd5;
}
