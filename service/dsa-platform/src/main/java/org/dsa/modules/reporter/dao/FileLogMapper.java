package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.entity.FileLog;

import java.util.List;

@Mapper
public interface FileLogMapper extends BaseMapper<FileLog> {

    List<FileLog> fileList(@Param("handlerTime") String handlerTime, @Param("name") String name, @Param("status") Integer status);

    List<String> filePathListByDate(@Param("handlerTime") String handlerTime, @Param("name") String name,  @Param("status") Integer status);

    List<String> filePathListByRange(@Param("startDate") String startTime, @Param("endDate") String endTime, @Param("name") String name,  @Param("status") Integer status);

    FileLog  getSessionLog(@Param("fileName") String fileName, @Param("sessionId") String sessionId);

    FileLog checkFile(@Param("fileType") String fileType, @Param("fileName") String fileName);

    FileLog checkFailedFile(@Param("fileType") String fileType, @Param("fileName") String fileName);

    void updatePath(@Param("fileId") Integer fileId, @Param("filePath") String filePath);

}
