package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.dto.VehicleDto;
import org.dsa.modules.reporter.entity.VehicleInfo;


@Mapper
public interface VehicleInfoMapper extends BaseMapper<VehicleInfo> {
    public VehicleInfo getInfo(@Param("vinCode") String vinCode);

    public VehicleDto getInfoWithType(@Param("vinCode") String vinCode);

    public VehicleInfo checkVehicleType(@Param("vehicleType") Long id);

    public VehicleDto fullInfo(@Param("vinCode") String vinCode);
}
