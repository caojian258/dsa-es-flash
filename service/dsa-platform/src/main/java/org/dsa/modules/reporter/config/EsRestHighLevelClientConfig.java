package org.dsa.modules.reporter.config;

import cn.hutool.core.util.StrUtil;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class EsRestHighLevelClientConfig {
    @Value("${spring.elasticsearch.rest.scheme}")
    private String scheme;

    @Value("${spring.elasticsearch.rest.host}")
    private String host;

    @Value("${spring.elasticsearch.rest.port}")
    private String port;

    @Value("${spring.elasticsearch.rest.username}")
    private String username;

    @Value("${spring.elasticsearch.rest.password}")
    private String password;

    @Value("${spring.elasticsearch.rest.ip-address}")
    private String ipAddress;

    @Bean
    public RestHighLevelClient restHighLevelClient() {

        RestHighLevelClient client = null;

        if(StrUtil.isNotBlank(username) && StrUtil.isNotBlank(password)){

            final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
            credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(username, password));

//            client = new RestHighLevelClient(
//                    RestClient.builder(
//                                    new HttpHost(host, Integer.parseInt(port), scheme))
//                            .setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
//                                public HttpAsyncClientBuilder customizeHttpClient(HttpAsyncClientBuilder httpClientBuilder) {
//                                    httpClientBuilder.disableAuthCaching();
//                                    return httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
//                                }
//                            }));
            client = new RestHighLevelClient(RestClient.builder(getHttpHosts(ipAddress, Integer.parseInt(port)))
                    .setHttpClientConfigCallback((HttpAsyncClientBuilder httpAsyncClientBuilder) -> httpAsyncClientBuilder.setDefaultCredentialsProvider(credentialsProvider)));
        }else{
             client = new RestHighLevelClient(RestClient.builder(getHttpHosts(ipAddress, Integer.parseInt(port))));
        }
        return client;
    }

    /**
     * 创建 HttpHost 对象
     *
     * @return 返回 HttpHost 对象数组
     */
    private HttpHost[] createHttpHost() {
        List<String> ipAddressList = new ArrayList<>();
        ipAddressList.add(host+":"+port);

        HttpHost[] httpHosts = new HttpHost[ipAddressList.size()];
        for (int i = 0, len = ipAddressList.size(); i < len; i++) {
            String ipAddress = ipAddressList.get(i);
            String[] values = ipAddress.split(":");

            String ip = values[0];
            int port = Integer.parseInt(values[1]);
            // 创建 HttpHost
            httpHosts[i] = new HttpHost(ip, port, scheme);
        }
        return httpHosts;
    }

    private HttpHost[] getHttpHosts(String clientIps, int esHttpPort) {
        String[] clientIpList = clientIps.split(",");
        HttpHost[] httpHosts = new HttpHost[clientIpList.length];
        for (int i = 0; i < clientIpList.length; i++) {
            httpHosts[i] = new HttpHost(clientIpList[i], esHttpPort, scheme);
        }
        return httpHosts;
    }
}
