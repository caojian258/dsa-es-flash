package org.dsa.modules.vehicle.service.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.AESUtils;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.ObjectUtils;
import org.dsa.modules.remoteDiagnostic.config.RemoteDiagnosticConfig;
import org.dsa.modules.remoteDiagnostic.dao.RVehicleTagDao;
import org.dsa.modules.remoteDiagnostic.dao.RemoteEcuIdentificationInfoDao;
import org.dsa.modules.remoteDiagnostic.dao.RemoteVehicleEcuGroupDao;
import org.dsa.modules.remoteDiagnostic.entity.RVehicleTagEntity;
import org.dsa.modules.remoteDiagnostic.utils.AutoExcelWidthStyleStrategy;
import org.dsa.modules.remoteDiagnostic.vo.Vehicle.VehicleInfoExcelReqVo;
import org.dsa.modules.reporter.util.LocalUtil;
import org.dsa.modules.sys.dao.SysCacheDao;
import org.dsa.modules.vehicle.dao.DVehicleInfoDao;
import org.dsa.modules.vehicle.dao.DVehicleTypeDao;
import org.dsa.modules.vehicle.dto.VehicleInfoDTO;
import org.dsa.modules.vehicle.entity.DVehicleInfoEntity;
import org.dsa.modules.vehicle.entity.DVehicleTypeEntity;
import org.dsa.modules.vehicle.entity.VehicleEcuDid;
import org.dsa.modules.vehicle.entity.VehicleInfoEntity;
import org.dsa.modules.vehicle.service.VehicleEcuDidService;
import org.dsa.modules.vehicle.service.VehicleService;
import org.dsa.modules.vehicle.util.VehicleUtils;
import org.dsa.modules.vehicle.vo.VehicleInfoUpdateReqVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import jakarta.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service("VehicleService")
public class VehicleServiceImpl extends ServiceImpl<DVehicleInfoDao, DVehicleInfoEntity> implements VehicleService {

    private final Logger logger = LoggerFactory.getLogger(VehicleServiceImpl.class);

    @Resource
    private DVehicleInfoDao dVehicleInfoDao;
    @Resource
    private DVehicleTypeDao dVehicleTypeDao;
    @Autowired
    RVehicleTagDao vehicleTagDao;
    @Autowired
    RemoteEcuIdentificationInfoDao remoteEcuIdentificationInfoDao;
    @Resource
    VehicleUtils vehicleUtils;
    @Autowired
    RemoteDiagnosticConfig remoteDiagnosticConfig;
    @Autowired
    private VehicleEcuDidService vehicleEcuDidService;
    @Autowired
    private SysCacheDao sysCacheDao;
    @Autowired
    private RemoteVehicleEcuGroupDao remoteVehicleEcuGroupDao;

    @Value("${project.url}")
    private String projectUrl;
    @Value("${file.vehicle_img.path}")
    private String vehicleImgPath;

    @Override
    public DVehicleInfoEntity getVehicleInfoById(Long id) {
        return dVehicleInfoDao.selectOneById(id);
    }

    @Override
    @Transactional
    public Integer updateVehicleInfo(DVehicleInfoEntity entity) {
        Integer res =0 ;
        if (entity.getSource() == 2) {
            if (ObjectUtil.equal(entity.getProductionDate(), null)) {
                entity.setProductionDate("");
            }
//        if(ObjectUtil.equal(entity.getProductionDate(),null)){
//            entity.setProductionDate("");
//        }
            if (ObjectUtils.isNotNull(entity.getId())) {
                entity.setUpdateTime(new Date());
                res = dVehicleInfoDao.updateById(entity);
            } else {
                //校验VIN
                DVehicleInfoEntity dVehicleInfoEntity = dVehicleInfoDao.selectOneByVin(entity.getVin());
                if (ObjectUtils.isNotNull(dVehicleInfoEntity)) {
                    entity.setId(dVehicleInfoEntity.getId());
                    entity.setUpdateTime(new Date());
                    res = dVehicleInfoDao.updateById(entity);
                } else {
                    res = dVehicleInfoDao.insert(entity);
                }
            }
        }

        vehicleTagDao.delete(Wrappers.<RVehicleTagEntity>lambdaQuery().eq(RVehicleTagEntity::getVin, entity.getVin()));
        if (ObjectUtils.isNotEmpty(entity.getTags())) {
            vehicleTagDao.inserts(entity.getVin(), entity.getTags());
        }

        return res;
    }

    /**
     * 根据vin码获取车辆信息
     *
     * @param vin
     * @return
     */
    @Override
    public VehicleInfoEntity getVehicleInfoByVin(String vin) {
        VehicleInfoEntity vehicleInfoEntity = new VehicleInfoEntity();

        DVehicleInfoEntity dVehicleInfoEntity = dVehicleInfoDao.selectOneByVin(vin);
        if (dVehicleInfoEntity != null) {
            vehicleInfoEntity.setVin(vin);
            vehicleInfoEntity.setProductionDate(StringUtils.isEmpty(dVehicleInfoEntity.getProductionDate()) ? "" : dVehicleInfoEntity.getProductionDate());
            vehicleInfoEntity.setPlaceOfProduction(StringUtils.isEmpty(dVehicleInfoEntity.getPlaceOfProduction()) ? "" : dVehicleInfoEntity.getPlaceOfProduction());

            List<DVehicleTypeEntity> list = dVehicleTypeDao.selectListById(dVehicleInfoEntity.getVehicleTypeId());
            if (list != null && list.size() > 0) {
                for (DVehicleTypeEntity t : list) {
                    if (t.getLevel() == Constant.VEHICLE_TYPE.MAKER.getLevel()) {
                        vehicleInfoEntity.setMaker(t.getValue());
                    } else if (t.getLevel() == Constant.VEHICLE_TYPE.MODEL.getLevel()) {
                        vehicleInfoEntity.setModel(t.getValue());
                    } else if (t.getLevel() == Constant.VEHICLE_TYPE.PLATFORM.getLevel()) {
                        vehicleInfoEntity.setPlatform(t.getValue());
                    } else if (t.getLevel() == Constant.VEHICLE_TYPE.TRIM_LEVEL.getLevel()) {
                        vehicleInfoEntity.setTrimLevel(t.getValue());
                    } else if (t.getLevel() == Constant.VEHICLE_TYPE.YEAR.getLevel()) {
                        vehicleInfoEntity.setYear(t.getValue());
                    }
                }
            } else {
                throw new RRException(Constant.Msg.CHECK_VIN_HAS_TYPE);
            }
        } else {
            throw new RRException(Constant.Msg.CHECK_VIN);
        }

        return vehicleInfoEntity;
    }

    @Override
    public Map<String, String> vehicleExcelExport() throws IOException {
        LambdaQueryWrapper<DVehicleInfoEntity> qw = Wrappers.<DVehicleInfoEntity>lambdaQuery();
        qw.last(" LIMIT 10 ");
        List<DVehicleInfoEntity> dVehicleInfoEntities = dVehicleInfoDao.selectList(qw);

        HashMap<String, String> map = new HashMap<>();
        createNewFileByVehicle(dVehicleInfoEntities, map, true);
        return map;
    }

    @Override
    public Map<String, String> vehicleExcelExport(List<DVehicleInfoEntity> ids) throws IOException {
        LambdaQueryWrapper<DVehicleInfoEntity> qw = Wrappers.<DVehicleInfoEntity>lambdaQuery();
        List<Long> list = ids.stream().map(DVehicleInfoEntity::getId).collect(Collectors.toList());
        qw.in(DVehicleInfoEntity::getId, list);
        List<DVehicleInfoEntity> dVehicleInfoEntities = dVehicleInfoDao.selectList(qw);

        HashMap<String, String> map = new HashMap<>();
        createNewFileByVehicle(dVehicleInfoEntities, map, false);
        return map;
    }

    @Override
    public List<DVehicleInfoEntity> getListByType(Long id) {
        return baseMapper.selectList(Wrappers.<DVehicleInfoEntity>lambdaQuery().eq(DVehicleInfoEntity::getVehicleTypeId, id));
    }

    @Override
    public Map<String, String> vehicleExcelExport(VehicleInfoExcelReqVo vo) throws IOException {
        List<DVehicleInfoEntity> dVehicleInfoEntities;
        if (vo.getVehicleIds() != null && vo.getVehicleIds().size() != 0) {
            dVehicleInfoEntities = dVehicleInfoDao.selectList(Wrappers.<DVehicleInfoEntity>lambdaQuery().in(DVehicleInfoEntity::getId, vo.getVehicleIds()));
        } else {
            dVehicleInfoEntities = dVehicleInfoDao.getAllByVehicleTypeIdAndVin(vo.getVehicleTypeIds(), vo.getVin(), vo.getTag(), vo.getRegular());
        }
        HashMap<String, String> map = new HashMap<>();
        createNewFileByVehicle(dVehicleInfoEntities, map, false);
        return map;
    }

    private void createNewFileByVehicle(List<DVehicleInfoEntity> dVehicleInfoEntities, HashMap<String, String> map, Boolean isTemp) throws IOException {

        //导出到指定路径
        File languageExcelFolder = new File(remoteDiagnosticConfig.getVehicle());

        if (!languageExcelFolder.exists()) {
            FileUtil.mkdir(languageExcelFolder);
        }
        String fileName = languageExcelFolder + File.separator + remoteDiagnosticConfig.getVehicleInfo() + "_" + System.currentTimeMillis() + ".xlsx";
        if (isTemp) {
            fileName = languageExcelFolder + File.separator + remoteDiagnosticConfig.getVehicleInfo() + "_template" + ".xlsx";
        }
//        String fileName = languageExcelFolder + File.separator + remoteDiagnosticConfig.getVehicleInfo() + "_" + System.currentTimeMillis() + ".xlsx";
        File file = new File(fileName);
//        if (!file.exists()) {
//            boolean newFile = file.createNewFile();
//        } else {
//            map.put("path", fileName);
//            map.put("name", file.getName());
//            return;
//        }
        List<List<String>> languages = new ArrayList<>();
        languages.add(Collections.singletonList(LocalUtil.get("vehicle.VIN")));
        languages.add(Collections.singletonList(LocalUtil.get("vehicle.Manufacturer")));
        languages.add(Collections.singletonList(LocalUtil.get("vehicle.Brand")));
        languages.add(Collections.singletonList(LocalUtil.get("vehicle.VehicleType")));
        languages.add(Collections.singletonList(LocalUtil.get("vehicle.Year")));
        languages.add(Collections.singletonList(LocalUtil.get("vehicle.VehicleConfiguration")));
        languages.add(Collections.singletonList(LocalUtil.get("vehicle.Origin")));
        languages.add(Collections.singletonList(LocalUtil.get("vehicle.ProductionDate")));
        languages.add(Collections.singletonList(LocalUtil.get("vehicle.tag")));
        List<List<String>> data = new ArrayList<>();

        ArrayList<String> objects = new ArrayList<>();
        List<DVehicleTypeEntity> types = new ArrayList<>();
        Map<Long, List<DVehicleTypeEntity>> cache = new HashMap<>();

        int size = languages.size() - 3;
        if (isTemp) {
            objects.add("LJ1E6A2U4K7708977");
            objects.add("XPENG");
            objects.add("XPENG");
            objects.add("E28");
            objects.add("2023");
            objects.add("high");
            objects.add("ShangHai");
            objects.add("2021-01-01");
            objects.add("tag1,tag2");
            data.add(objects);
        } else {
            for (DVehicleInfoEntity dVehicleInfoEntity : dVehicleInfoEntities) {
                if (cache.containsKey(dVehicleInfoEntity.getVehicleTypeId())) {
                    types = cache.get(dVehicleInfoEntity.getVehicleTypeId());
                } else {
                    types = dVehicleTypeDao.selectListByIdAsc(dVehicleInfoEntity.getVehicleTypeId());
                    cache.put(dVehicleInfoEntity.getVehicleTypeId(), types);
                }
                objects = new ArrayList<>();
                objects.add(dVehicleInfoEntity.getVin());
                for (int i = 0; i < size; i++) {
                    objects.add(types != null && types.size() > i ? types.get(i).getValue() : null);
                }
                objects.add(dVehicleInfoEntity.getPlaceOfProduction());
                objects.add(dVehicleInfoEntity.getProductionDate());
                data.add(objects);
            }
        }
        types = null;
        cache = null;

        // 这里 需要指定写用哪个class去写，然后写到第一个sheet，名字为模板 然后文件流会自动关闭
        EasyExcel.write(fileName)
                .registerWriteHandler(new AutoExcelWidthStyleStrategy())
                .head(languages).sheet("vehicle info").doWrite(data);

        map.put("path", fileName);
        map.put("name", file.getName());
    }

    @Override
    public DVehicleInfoEntity getOne(String vin) {
        LambdaQueryWrapper<DVehicleInfoEntity> qw = Wrappers.lambdaQuery();
        qw.eq(DVehicleInfoEntity::getVin, vin);
        return baseMapper.selectOne(qw);
    }

    @Override
    public VehicleInfoDTO get(String vin) {
        DVehicleInfoEntity entity = getOne(vin);
        if (Objects.isNull(entity)) {
            logger.error("vin={} is not exist.", vin);
            throw new RRException(Constant.Msg.CHECK_VIN);
        }

        // regionCode cityCode
        VehicleInfoDTO vehicleInfoDTO = new VehicleInfoDTO();
        BeanUtils.copyProperties(entity, vehicleInfoDTO);

        vehicleInfoDTO.setFactoryDate(StringUtils.isEmpty(entity.getProductionDate()) ? "" : entity.getProductionDate());
        vehicleInfoDTO.setPlaceOfProductionCode(StringUtils.isEmpty(entity.getPlaceOfProduction()) ? "" : entity.getPlaceOfProduction());

        List<DVehicleTypeEntity> list = dVehicleTypeDao.selectListById(entity.getVehicleTypeId());
        if (CollectionUtils.isEmpty(list)) {
            logger.error("vin={} does not have vehicle type information.", vin);
            throw new RRException(Constant.Msg.CHECK_VIN_HAS_TYPE);
        }

        String vehicleTypeCode = "";
        String vehicleModelCodes = sysCacheDao.getValueByKey("VEHICLE_MODEL_CODES");
        Set<String> vehicleModelCodeSet = new HashSet<>();
        if (StringUtils.isNotEmpty(vehicleModelCodes)) {
            vehicleModelCodeSet = Arrays.stream(vehicleModelCodes.split(",")).collect(Collectors.toSet());
        }

        for (DVehicleTypeEntity t : list) {
            if (t.getLevel() == Constant.VEHICLE_TYPE.MAKER.getLevel()) {
                vehicleInfoDTO.setMake(t.getValue());
            } else if (t.getLevel() == Constant.VEHICLE_TYPE.MODEL.getLevel()) {
                vehicleInfoDTO.setModelCode(t.getValue());
            } else if (t.getLevel() == Constant.VEHICLE_TYPE.PLATFORM.getLevel()) {
                vehicleInfoDTO.setVehiclePlatform(t.getValue());
            } else if (t.getLevel() == Constant.VEHICLE_TYPE.TRIM_LEVEL.getLevel()) {
                vehicleInfoDTO.setTrimLevel(t.getValue());
            } else if (t.getLevel() == Constant.VEHICLE_TYPE.YEAR.getLevel()) {
                vehicleInfoDTO.setYear(t.getValue());
            }

            if(vehicleModelCodeSet.contains(t.getValue())) {
                vehicleTypeCode = t.getValue();
            }
        }

        if (StringUtils.isNotEmpty(vehicleTypeCode)) {
            String path = vehicleImgPath + vehicleTypeCode + "_vehicle.jpg";
            if (!new File(path).exists()) {
                log.warn("缺失车型[{}]图片信息", vehicleTypeCode);
            } else {
                String encrypt = AESUtils.encrypt(path, AESUtils.AES_KEY);
                Assert.notNull(encrypt, "encrypt can not be null.");
                String url = projectUrl + "/sys/file/security-download?params=" + URLEncoder.encode(encrypt, StandardCharsets.UTF_8);
                vehicleInfoDTO.setVehiclePicture(url);
            }
        }

        DVehicleTypeEntity vehicleType = dVehicleTypeDao.selectById(entity.getVehicleTypeId());
        if (Objects.nonNull(vehicleType)) {
            if (CollectionUtils.isEmpty(vehicleType.getSdgList())) {
                vehicleInfoDTO.setOemVehicleInfoList(Collections.emptyList());
            } else {
                vehicleInfoDTO.setOemVehicleInfoList(vehicleType.getSdgList());
            }

            List<String> ecuList = remoteVehicleEcuGroupDao.getECUGroupNames(entity.getVehicleTypeId());
            if (CollectionUtils.isEmpty(ecuList)) {
                vehicleInfoDTO.setEcuList(Collections.emptyList());
            } else {
                vehicleInfoDTO.setEcuList(ecuList);
            }

        } else {
            vehicleInfoDTO.setEcuList(Collections.emptyList());
            vehicleInfoDTO.setOemVehicleInfoList(Collections.emptyList());
        }

        vehicleInfoDTO.setBrand("");
        this.dealResult(vehicleInfoDTO);
        return vehicleInfoDTO;
    }

    private void dealResult(VehicleInfoDTO vehicleInfoDTO) {
        if (StringUtils.isEmpty(vehicleInfoDTO.getRegionCode())) {
            vehicleInfoDTO.setRegionCode("");
        }

        if (StringUtils.isEmpty(vehicleInfoDTO.getCityCode())) {
            vehicleInfoDTO.setCityCode("");
        }

        if (StringUtils.isEmpty(vehicleInfoDTO.getWorkshopName())) {
            vehicleInfoDTO.setWorkshopName("");
        }

        if (StringUtils.isEmpty(vehicleInfoDTO.getMake())) {
            vehicleInfoDTO.setMake("");
        }

        if (StringUtils.isEmpty(vehicleInfoDTO.getModelCode())) {
            vehicleInfoDTO.setModelCode("");
        }

        if (StringUtils.isEmpty(vehicleInfoDTO.getVehiclePlatform())) {
            vehicleInfoDTO.setVehiclePlatform("");
        }

        if (StringUtils.isEmpty(vehicleInfoDTO.getYear())) {
            vehicleInfoDTO.setYear("");
        }

        if (StringUtils.isEmpty(vehicleInfoDTO.getTrimLevel())) {
            vehicleInfoDTO.setTrimLevel("");
        }
    }

    @Override
    @Transactional
    public void batchUpdateInfo(VehicleInfoUpdateReqVo vo) {
        LambdaUpdateWrapper<DVehicleInfoEntity> wu = Wrappers.lambdaUpdate();

        wu.set(StringUtils.isNotEmpty(vo.getPlaceOfProduction()), DVehicleInfoEntity::getPlaceOfProduction, vo.getPlaceOfProduction());
        wu.set(StringUtils.isNotEmpty(vo.getProductionDate()), DVehicleInfoEntity::getProductionDate, vo.getProductionDate());
        wu.set(vo.getVehicleTypeId() != null, DVehicleInfoEntity::getVehicleTypeId, vo.getVehicleTypeId());
        wu.in(DVehicleInfoEntity::getVin, vo.getVinList());
        if (StringUtils.isNotEmpty(wu.getSqlSet())) {
            baseMapper.update(new DVehicleInfoEntity(), wu);
        }
        if (ObjectUtils.isNotEmpty(vo.getTags())) {
            vehicleTagDao.batchInserts(vo.getVinList(), vo.getTags());
        }
    }

    @Override
    public List<Long> getVehicleTypeIdsByVin(String vin) {
        return baseMapper.searchVehicleTypeIdByVin(vin);
    }

    @Override
    public List<Map<String, String>> getDidValueByVehicleTypeIdsAndEcuNameAndDid(List<Long> vehicleTypeIds, List<String> ecuNames, String did) {
        return baseMapper.getDidValueByVehicleTypeIdsAndEcuNameAndDid(vehicleTypeIds,ecuNames,did);
    }
}
