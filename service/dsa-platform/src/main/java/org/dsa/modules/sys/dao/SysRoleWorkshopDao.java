package org.dsa.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.dsa.modules.sys.entity.SysRoleWorkshopEntity;

import java.util.List;

/**
 * 角色与部门对应关系
 *
 */
@Mapper
public interface SysRoleWorkshopDao extends BaseMapper<SysRoleWorkshopEntity> {
	
	/**
	 * 根据角色ID，获取部门ID列表
	 */
	List<Long> queryWorkshopIdList(Long[] roleIds);

	/**
	 * 根据角色ID数组，批量删除
	 */
	int deleteBatch(Long[] roleIds);
}
