package org.dsa.modules.sys.enums;

import java.util.Objects;

/**
 * 菜单所属系统枚举
 */
public enum SystemEnum {
    /**
     * 通用菜单，子系统公用的菜单，如用户，角色,菜单
     */
    GENERIC_MENU("GenericMenu",0),
    DSA_PLATFORM("DsaPlatform",1),
    LICENSE_CONTROL("License Control",2);
    SystemEnum(String name, Integer code) {
        this.name = name;
        this.code = code;
    }

    private final String name;
    private final Integer code;

    public String getName() {
        return name;
    }

    public Integer getCode() {
        return code;
    }

    public static Integer getCodeBySystemName(String name){
        for (SystemEnum value:values()){
            if (Objects.equals(value.getName(), name)){
                return value.getCode();
            }
        }
        return null;
    }

}
