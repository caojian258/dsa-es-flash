package org.dsa.common.utils;

import junit.framework.TestCase;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ShellUtilTest extends TestCase {

    @Test
    public void testWindows() throws IOException, InterruptedException {
        String basePath = "D:/dsa-product/sqlite/session";
        String filename = "VIN_L1234567890123456_5023b648-c78b-4c52-8e7a-b3f34773add3_.slog";
        File sourceFile = new File(basePath + File.separator + filename);
        String[] filenames = filename.split("\\.");

        List<String> cmds = new ArrayList<>();
        String originalCommandMsg = "C:\\InternalProduct\\DsaPlatform\\50_Sources\\branches\\DsaPlatform_cji\\service\\dsa-platform\\src\\main\\resources\\windows\\sqlcipher.bat";
        cmds.add(originalCommandMsg);
        cmds.add(sourceFile.getAbsolutePath());
        cmds.add("VIN_L1234567890123456_5023b648-c78b-4c52-8e7a-b3f34773add3_.db");
        ShellUtil shellUtil = new ShellUtil(basePath, cmds);
        shellUtil.start();
        System.out.println("msg: " + shellUtil.getMessages());

    }
}