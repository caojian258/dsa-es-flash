package org.dsa.modules.diagnostic.entity.mqtt.callback;

import lombok.Data;

import java.io.Serializable;

@Data
public class MqBodyDataDTCClearVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ECU 名称
     */
    private  String  ecuName;

    /**
     * 清除结果,0代表成功，1代表失败
     */
    private int status;

    /**
     * 清除结果附带的信息
     */
    private String description;
}
