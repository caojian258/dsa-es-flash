package org.dsa.modules.reporter.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.io.Serializable;

@Data
public class ClientBaseInfoDto implements Comparable {

    private String title;

    private String field;

    private String value;

    private Integer sorted;

    @Override
    public int compareTo(@NotNull Object o) {
        int compare =((ClientBaseInfoDto)o).getSorted();
        /* 正序排列 */
        return this.sorted - compare;
    }

    @Override
    public String toString() {
        return "[ title=" + title + ", field=" + field + ", value=" + value + ", sorted=" + sorted + "]";
    }
}
