package org.dsa.license;

import cn.hutool.core.map.MapBuilder;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.dsa.common.exception.RRException;
import org.dsa.common.pojo.CodeMessage;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.HttpUtils;
import org.dsa.common.utils.R;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Slf4j
@RestController
@RequestMapping("/license")
public class DsaLicenseController {

    @Value("${license.project.url}")
    private String licenseUrl;
    @Value("${license.oem.code}")
    private String oemCode;
    @Value("${license.oem.name}")
    private String oemName;

    @Value("${license.oem.url}")
    private String oemLicenseUrl;

    /**
     * 获取订单信息
     */
    @RequestMapping(value = "/products", method = {RequestMethod.POST, RequestMethod.GET})
    public Object queryOemPoolByOemCode() {
        try {
            Map<String, Object> param = MapBuilder
                    .create(new HashMap<String, Object>())
                    .put("oemCode", oemCode)
                    .map();
            CodeMessage resp = HttpUtils.post(licenseUrl+"/license/OrderService/queryOemPoolInfo", JSON.toJSONString(param));
            Object res = JSONObject.parseObject((resp.getMessage())).get("res");
            if (!Objects.isNull(res)) {
                return R.ok().put("res", res);
            }
            return resp.getMessage();
        } catch (Exception e) {
            log.error("查询OEM产品异常", e);
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

    /**
     * @param params params.deviceId
     * @return R.data
     */
    @RequestMapping(value = "/validDays",method = {RequestMethod.POST,RequestMethod.GET})
    public R queryValidDays(@RequestBody Map<String,Object> params, @RequestHeader("token") String token){
        validateParams(params);
        String queryUrl = oemLicenseUrl + "/license/InternetDongleService/queryValidDays";
        Map<String,String> headers = new HashMap<>(1);
        headers.put("token",token);
        CodeMessage res = null;
        try {
            res = HttpUtils.post(queryUrl, JSON.toJSONString(params),headers);
            if (Objects.equals(res.getCode(), "200")){
                log.info("validDays res:{}",res.getMessage());
                JSONObject resJSON = JSON.parseObject(res.getMessage());
                if (resJSON.containsKey("res")){
                    return R.ok().put("data",resJSON.get("res"));
                }
            }
        } catch (Exception e) {
            log.error("validDayHttpError:",e);
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
        return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
    }

    private void validateParams(Map<String, Object> params) {
        if (params == null) {
            throw new RRException("deviceId不能为空！", 1005);
        }
        String deviceId = MapUtils.getString(params, "deviceId");
        Map<String,String> order = (Map<String, String>) MapUtils.getObject(params, "order");
        if (deviceId == null) {
            throw new RRException("deviceId不能为空！", 1005);
        }
        if (order == null) {
            throw new RRException("order不能为空！", 1005);
        }
        if (order.get("oemCode") == null){
            throw new RRException("oemCode不能为空！", 1005);
        }
        if (order.get("appCode") == null){
            throw new RRException("appCode不能为空！", 1005);
        }
        if (order.get("productCode") == null){
            throw new RRException("productCode不能为空！", 1005);
        }
        if (order.get("licenseTypeCode") == null){
            throw new RRException("licenseTypeCode不能为空！", 1005);
        }
    }


}
