package org.dsa.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.sys.entity.SysLocationEntity;

import java.util.List;

@Mapper
public interface SysLocationDao extends BaseMapper<SysLocationEntity> {

    SysLocationEntity findByCode(Long code);

    SysLocationEntity findByNameAndType(@Param("name") String name, @Param("type") String type);

    List<SysLocationEntity> selectByType(String type);

    List<SysLocationEntity> selectByPid(Long pid);


    List<SysLocationEntity> selectById(@Param("ids") List<Long> ids);

    List<SysLocationEntity> selectByCode(@Param("codes") List<Long> codes);
}
