package org.dsa.modules.reporter.job;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.diagnostic.dao.DActionInfoDao;
import org.dsa.modules.diagnostic.dao.DSessionInfoDao;
import org.dsa.modules.diagnostic.dto.FlashActionDto;
import org.dsa.modules.diagnostic.entity.DSessionInfoEntity;
import org.dsa.modules.reporter.constant.FlashConstant;
import org.dsa.modules.reporter.constant.RedisConstant;
import org.dsa.modules.reporter.dao.VehicleFlashRecordMapper;
import org.dsa.modules.reporter.dto.EcuVersionHistoryDto;
import org.dsa.modules.reporter.dto.VehicleVersionDto;
import org.dsa.modules.reporter.entity.VehicleFlashRecord;
import org.dsa.modules.reporter.enums.FlashEnum;
import org.dsa.modules.reporter.service.EcuVersionService;
import org.dsa.modules.reporter.service.FlashLogService;
import org.dsa.modules.reporter.service.VehicleVersionService;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import jakarta.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/***
 * 诊断会话与车辆刷写 数据处理
 */

@Slf4j
@Component
@DisallowConcurrentExecution
public class SessionFlashJob extends QuartzJobBean {

    @Autowired
    RedisUtils redisUtils;

    @Autowired
    VehicleFlashRecordMapper flashRecordMapper;

    @Autowired
    DSessionInfoDao sessionInfoDao;

    @Resource
    DActionInfoDao dActionInfoDao;

    @Resource
    EcuVersionService ecuVersionService;

    @Resource
    VehicleVersionService vehicleVersionService;

    @Resource
    FlashLogService flashLogService;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.info("SessionFlashJob Start................");

        Long size = redisUtils.lGetListSize(RedisConstant.FLASH_VIN_KEY);

        log.info("SessionFlashJob loop size: {}", size);
        //诊断日志中找到对应的flash记录 更新数据到刷写记录中
        while (size>0) {
            Boolean hasUpdate = false;

            String tag = (String) redisUtils.lPop(RedisConstant.FLASH_VIN_KEY);
            if (StringUtils.isEmpty(tag)) {
                size--;
                continue;
            }
            log.info("SessionFlashJob loop session: {}", tag);

            String[] tags = tag.split(":");
            if(tags.length != 3){
                size--;
                continue;
            }
            String vin = tags[0];
            String sessionId = tags[1];

            //完善刷写日志中的开始时间与结束时间 经销商
            QueryWrapper<VehicleFlashRecord> qw = new QueryWrapper<>();
            qw.eq("vin", vin);
            qw.eq("session_id", sessionId);
            qw.eq("tag", tags[2]);
            qw.orderByAsc("id");
            VehicleFlashRecord record = flashRecordMapper.selectOne(qw);
            if(null !=record){
                QueryWrapper<DSessionInfoEntity> qwd = new QueryWrapper<>();
                qwd.eq("vin", vin);
                qwd.eq("session_id", tags[1]);

                DSessionInfoEntity sessionInfo = sessionInfoDao.selectOne(qwd);

                if(null != sessionInfo){

                    record.setEndTime(sessionInfo.getEndTime());
                    record.setPcid(sessionInfo.getPcid());
                    record.setWorkshop(sessionInfo.getWorkshop());
                    record.setUsername(sessionInfo.getDiagnosticDisplayUserName());

                    if(null == record.getStartTime()){
                        record.setStartTime(sessionInfo.getStartTime());
                    }

                    List<String> fs = new ArrayList<>();
                    fs.add(FlashEnum.ONLINE.getValue());
                    fs.add(FlashEnum.REMOTE.getValue());


                    List<FlashActionDto> flashList =  dActionInfoDao.selectFlashListBySessionId(sessionInfo.getSessionId(), fs);

                    if(CollectionUtil.isNotEmpty(flashList)){
                        FlashActionDto startAction = flashList.get(0);
                        FlashActionDto endAction = flashList.get(flashList.size() -1);
                        if(null != startAction.getStartTime()){
                            record.setStartTime(startAction.getStartTime());
                        }

                        if(null != endAction.getEndTime()){
                            record.setEndTime(endAction.getEndTime());
                        }
                    }
                    if(record.getEndTime().compareTo(record.getStartTime()) >0 ){
                        record.setSpendTime(DateUtil.between(record.getEndTime(),record.getStartTime(), DateUnit.SECOND ) );
                    }

                    flashRecordMapper.updateById(record);
                    hasUpdate = true;

                    if(record.getResult() != FlashConstant.FAILED){
                        //添加上一个版本
                        VehicleVersionDto pvvd = new VehicleVersionDto();
                        pvvd.setCollectTime(record.getCollectTime());
                        pvvd.setSessionId(record.getSessionId());
                        pvvd.setVin(record.getVin());
                        pvvd.setVersion(record.getPrevVersion());
                        pvvd.setTag(record.getTag());
                        pvvd.setPcid(record.getPcid());
                        pvvd.setWorkshop(record.getWorkshop());
                        vehicleVersionService.addHistory(pvvd);

                        //添加当前版本
                        VehicleVersionDto vvd = new VehicleVersionDto();
                        vvd.setCollectTime(record.getCollectTime());
                        vvd.setSessionId(record.getSessionId());
                        vvd.setVin(record.getVin());
                        vvd.setVersion(record.getVehicleVersion());
                        vvd.setTag(record.getTag());
                        vvd.setPcid(record.getPcid());
                        vvd.setWorkshop(record.getWorkshop());

                        vehicleVersionService.updateOverview(vvd);

                        vehicleVersionService.addHistory(vvd);

                        List<EcuVersionHistoryDto> ecuVersionList = flashLogService.successRecord(record.getVin(), record.getTag());
                        for (EcuVersionHistoryDto dto: ecuVersionList) {
                            dto.setPcid(record.getPcid());
                            dto.setWorkshop(record.getWorkshop());
                            dto.setSessionId(record.getSessionId());

                            ecuVersionService.updateOverview(dto);
                            ecuVersionService.addHistory(dto);
                        }
                    }
                }
            }

            if(!hasUpdate){
                redisUtils.lSet(RedisConstant.FLASH_VIN_KEY, tag);
            }
            size--;
        }

        log.info("SessionFlashJob End................");
    }

}
