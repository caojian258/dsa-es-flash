package org.dsa.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.dsa.common.utils.PageUtils;
import org.dsa.common.utils.Query;
import org.dsa.modules.sys.dao.SysLogDao;
import org.dsa.modules.sys.entity.SysLogEntity;
import org.dsa.modules.sys.service.SysLogService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("sysLogService")
public class SysLogServiceImpl extends ServiceImpl<SysLogDao, SysLogEntity> implements SysLogService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = (String)params.get("key");

        IPage<SysLogEntity> page = this.page(
            new Query<SysLogEntity>().getPage(params),
            new QueryWrapper<SysLogEntity>()
                    .like(StringUtils.isNotBlank(key),"display_user_name", key)
                    .or()
                    .like(StringUtils.isNotBlank(key), "operation",key)
                    .orderByDesc("id")
        );

        return new PageUtils(page);
    }
}
