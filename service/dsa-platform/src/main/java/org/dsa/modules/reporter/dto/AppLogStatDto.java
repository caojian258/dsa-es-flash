package org.dsa.modules.reporter.dto;


import lombok.Data;

import java.io.Serializable;

@Data
public class AppLogStatDto implements Serializable {

    //应用类型
    private String appType;

    //应用名称
    private String appName;

    //应用ID
    private Integer appId;

    //操作类型
    private Integer statusType;

    //操作结果
    private Integer status;

    //统计
    private Integer total;

    //日期
    private String day;

    private String appVersion;

    private Integer versionNumber;
}
