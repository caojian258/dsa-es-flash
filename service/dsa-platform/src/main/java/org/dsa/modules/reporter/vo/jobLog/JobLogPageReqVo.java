package org.dsa.modules.reporter.vo.jobLog;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.modules.reporter.pojo.PageParam;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class JobLogPageReqVo extends PageParam {

    private String name;

    private String group;

    private String status;

    private Long jobId;

    private String error;

    private String startDate;

    private String endDate;
}
