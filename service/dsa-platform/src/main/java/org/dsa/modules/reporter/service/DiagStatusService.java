package org.dsa.modules.reporter.service;

import org.dsa.modules.reporter.dto.SessionStateDto;
import org.dsa.modules.reporter.dto.SharedDataDto;
import org.dsa.modules.reporter.dto.StatusDto;

public interface DiagStatusService {

    public StatusDto storeData(String path, String tag, String sessionId, String vinCode);

    public SharedDataDto storeVehicleInfo(String sharedDataPath, String vinCode) throws Exception;

    public SessionStateDto storeSessionState(String path, String tag, String sessionId, String vinCode, String pcid, String workshop, String lang) throws Exception;
}
