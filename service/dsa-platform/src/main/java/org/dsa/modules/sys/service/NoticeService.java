package org.dsa.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dsa.common.utils.PageUtils;
import org.dsa.modules.sys.dto.SysNoticeDTO;
import org.dsa.modules.sys.entity.FileEntity;
import org.dsa.modules.sys.entity.SysNoticeEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * @author weishunxin
 * @since 2023-06-20
 */
public interface NoticeService extends IService<SysNoticeEntity> {

    FileEntity uploadNotice(MultipartFile file);

    void saveNotice(SysNoticeDTO noticeDTO);

    void updateNotice(SysNoticeEntity entity);

    void deleteNotice(Integer[] ids);

    PageUtils pageList(Map<String, Object> params);

    SysNoticeEntity get(Integer id);

    List<SysNoticeEntity> availableList(String locale, Integer limitNum);

}
