package org.dsa.modules.diagnostic.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 诊断统计
 */
@Data
public class AnalysisReqDataEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 类型
	 */
	private String type;

	/**
	 * 故障数
	 */
	private List<Integer> data;
	/**
	 * 诊断来源 REMOTE
	 */
	private String source;
	/**
	 * 诊断来源 REMOTE为远程服务器
	 */
	private String sourceName;
	/**
	 * 诊断功能码
	 */
	private String functionCode;
	/**
	 * 诊断actionCode
	 */
	private String actionCode;
	/**
	 * 描述信息
	 */
	private String description;

	/**
	 * 日期
	 */
	private String requestDate;

	/**
	 * 执行次数
	 */
	private int requestCount;

}
