package org.dsa.modules.diagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("d_function_action")
@AllArgsConstructor
@NoArgsConstructor
public class DFunctionActionEntity implements Serializable
{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * 功能码
     */
    private String functionCode;
    /**
     * 操作码
     */
    private String actionCode;
    /**
     * 功能名
     */
    private String functionName;
    /**
     * 操作名
     */
    private String actionName;

    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 是否显示
     */
    private Integer showFilter;
}
