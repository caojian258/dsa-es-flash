package org.dsa.modules.onlineUpdate.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dsa.modules.onlineUpdate.dto.ComponentVersionDto;
import org.dsa.modules.onlineUpdate.entity.ComponentVersionEntity;
import org.dsa.modules.onlineUpdate.po.SoftwareVersionPo;
import org.dsa.modules.onlineUpdate.utils.PageUtils;
import org.dsa.modules.onlineUpdate.vo.CascadeOptions;
import org.dsa.modules.onlineUpdate.vo.CompVGroupVo;
import org.dsa.modules.onlineUpdate.vo.ComponentVersionVo;

import java.util.List;
import java.util.Map;

/**
 * ${comments}
 *
 */
public interface ComponentVersionService extends IService<ComponentVersionEntity> {

    PageUtils queryPage(Map<String, Object> params);

    ComponentVersionVo info(Long id);
    void saveOrUpdate2(ComponentVersionDto componentVersion);

    /**
     * 查询所有非自选的组件及版本树
     */
    List<CascadeOptions> queryComponentVersionTree(Long componentId);

    /**
     * 根据当前选择的组件版本，带出关联的组件版本
     */
    List<List<Long>> queryRelatedVersion(Map<Long,Long> selected, Long cVersionId, boolean isReplace);

    void sortCompVersion(List<CompVGroupVo> cVersion);

    Boolean hasRequiredComponent(List<Long> componentVersionIds);

    List<SoftwareVersionPo.ComponentVersionDependency> getRelatedByVersionId(Long componentVersionId);


    void updateRequiredDependency(List<Long> componentVersionIds);

    ComponentVersionVo queryVoByCVersionId(Long id);

    Map<String, Object> queryInfoByCompVersionId(Long versionId);

    void copyVersionByVersionId(Long versionId);
}

