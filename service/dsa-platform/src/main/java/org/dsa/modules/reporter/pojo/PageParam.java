package org.dsa.modules.reporter.pojo;

import lombok.Data;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class PageParam implements Serializable {

    private static final Integer PAGE_NO = 1;
    private static final Integer PAGE_SIZE = 10;

    private static final Boolean SORT_ASC = true;

    private static final String SORT_COLUMN = "id";

    @NotNull(message = "页码不能为空")
    @Min(value = 1, message = "页码最小值为 1")
    private Integer pageNo = PAGE_NO;

    @NotNull(message = "每页条数不能为空")
    @Min(value = 1, message = "每页条数最小值为 1")
    @Max(value = 1000, message = "每页条数最大值为 100")
    private Integer pageSize = PAGE_SIZE;

    private Boolean sortAsc = SORT_ASC;

    private String sortColumn = SORT_COLUMN;
}
