package org.dsa.modules.diagnostic.service;

import org.dsa.modules.diagnostic.entity.DVersionInfoEntity;
import org.dsa.modules.diagnostic.entity.DVersionRecordEntity;
import org.dsa.modules.diagnostic.entity.VersionInfoEntity;
import org.dsa.modules.sys.entity.FileEntity;
import org.dsa.modules.vehicle.entity.DVehicleTypeEntity;
import org.springframework.web.multipart.MultipartFile;

import jakarta.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 版本功能
 *
 */
public interface VersionService {

    /**
     * 获取版本树
     */
    List<DVehicleTypeEntity> getTreeList();

    /**
     * 根据版本id  和odx/otx类型 查询版本信息
     * @param vehicleTypeId,type
     */
    List<DVersionInfoEntity> list(Long vehicleTypeId, String type);

    List<DVersionInfoEntity> list(Long vehicleTypeId, DVersionInfoEntity s, DVersionInfoEntity e, String type);

    /**
     * 根据版本id  和odx/otx类型 查询版本信息
     * @param vehicleTypeId,type
     */
    List<DVersionInfoEntity> availableList(Long vehicleTypeId, String type);

    /**
     * 根据版本id  获取版本信息
     * @param id
     */
    DVersionInfoEntity getDVersionInfoById(Long id);

    DVersionInfoEntity get(Long vehicleTypeId, String type, String versionNum);

    /**
     * 根据版本id  获取版本信息  用于显示版本详情
     * @param id
     */
    VersionInfoEntity getVersionInfo(Long id);

    /**
     * 保存版本信息
     * @param versionEntity
     */
    void save(DVersionInfoEntity versionEntity);

    /**
     * 修改版本信息
     * @param versionEntity
     */
    void update(DVersionInfoEntity versionEntity);

    /**
     * 根据版本id  获取版本修改记录
     * @param id
     */
    List<DVersionRecordEntity> getVersionRecord(Long id);

    /**
     * 保存版本前判断,历史版本状态
     * @param vehicleTypeId , versionType
     */
    void checkVersionStatus(Long vehicleTypeId, String versionType);

    /**
     * 返回已创建
     * @param vehicleTypeId
     * @param versionType
     */
    int selectVersions(Long vehicleTypeId, String versionType);

    /**
     * 版本比较 生成下载文件
     * @param versionNum
     */
    DVersionInfoEntity compareVersionFile(String vin,  String versionNum,  String versionType);

    /**
     * 保存版本前判断,历史版本状态
     * @param vehicleTypeId
     * @param versionType
     */
    void buildVersionFile(Long vehicleTypeId, String versionType);

    /**
     * 修改车型是否关联版本
     * @param vehicleTypeId
     */
    void updateVehicleTypeVersion(Long vehicleTypeId,String versionType);

    /**
     * 上传文件
     * @param file
     * @param versionType
     * @param req
     * @return
     */
    FileEntity uploadFiles(MultipartFile file, String versionType, HttpServletRequest req);

    /**
     * 上传db文件
     * @param file
     * @param versionType
     * @return
     */
    FileEntity uploadDBFiles(MultipartFile file, String versionType);

    /**
     * 上传otx menu文件
     * @param file
     * @return
     */
    FileEntity uploadOtxMenuFile(MultipartFile file);

}

