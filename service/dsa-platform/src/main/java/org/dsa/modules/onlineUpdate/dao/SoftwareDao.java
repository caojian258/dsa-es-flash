package org.dsa.modules.onlineUpdate.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.dsa.modules.onlineUpdate.entity.SoftwareEntity;

/**
 * ${comments}
 * 
 */
@Mapper
public interface SoftwareDao extends BaseMapper<SoftwareEntity> {
	
}
