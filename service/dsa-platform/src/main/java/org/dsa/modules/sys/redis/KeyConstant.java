package org.dsa.modules.sys.redis;

public class KeyConstant {

    public static String LOGIN_ERROR_TIMES = "login_error_times:%s";

    public static String getLoginErrorTimes(String userName){
        return String.format(LOGIN_ERROR_TIMES, userName);
    }
}
