package org.dsa.modules.reporter.upgrade;

import lombok.extern.slf4j.Slf4j;
import org.dsa.modules.reporter.dto.AppUnInstallDto;
import org.dsa.modules.reporter.entity.AppHistory;
import org.dsa.modules.reporter.entity.ClientUpgrade;
import org.dsa.modules.reporter.enums.AppTypeEnum;
import org.dsa.modules.reporter.service.AppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Slf4j
@Service
public class UnInstallUpgradeStrategy implements UpgradeStrategy{

    @Autowired
    AppService appService;

    @Override
    public void save(ClientUpgrade log) {
        String type = "";
        Integer appId = 0;

        if(log.getInstallAction() == 0){
            type = AppTypeEnum.SOFTWARE.getValue();
            appId = log.getSoftwareId();
        }else{
            type = AppTypeEnum.COMPONENT.getValue();
            appId = log.getComponentId();
        }

        List<AppUnInstallDto> unInstallDtos = new ArrayList<>();

        AppUnInstallDto unInstallDto = new AppUnInstallDto();
        unInstallDto.setAppId(appId);
        unInstallDto.setUpdateOid(log.getUpdateId());
        unInstallDto.setVersion("");
        unInstallDto.setType(type);
        unInstallDto.setName("");
        unInstallDto.setResult(log.getUpdateResult().toString());
        unInstallDtos.add(unInstallDto);

        appService.addUnInstall(unInstallDtos, log.getWorkshopName(),
                log.getDeviceId(), log.getStartTime(), log.getTag());

    }

    @Override
    public void saveHis(AppHistory data) {
        List<AppUnInstallDto> unInstallDtos = new ArrayList<>();

        AppUnInstallDto unInstallDto = new AppUnInstallDto();
        unInstallDto.setAppId(data.getAppId());
        unInstallDto.setUpdateOid(data.getUpdateId());
        unInstallDto.setVersion(data.getAppVersion());
        unInstallDto.setType(data.getAppType());
        unInstallDto.setName(data.getAppName());
        unInstallDto.setResult(data.getStatus().toString());
        unInstallDtos.add(unInstallDto);

        appService.addUnInstall(unInstallDtos, data.getWorkshopName(),
                data.getDeviceId(), data.getStartTime(), data.getTag());
    }
}
