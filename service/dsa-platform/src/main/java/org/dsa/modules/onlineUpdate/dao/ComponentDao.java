package org.dsa.modules.onlineUpdate.dao;

import org.dsa.modules.onlineUpdate.entity.ComponentEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * ${comments}
 * 
 */
@Mapper
public interface ComponentDao extends BaseMapper<ComponentEntity> {
	
}
