package org.dsa.modules.remoteDiagnostic.service;

import org.dsa.modules.remoteDiagnostic.dto.flash.FlashCampaignEcuDto;

import java.util.List;

public interface FlashCampaignEcuService {

    public List<FlashCampaignEcuDto> queryCampaignEcu(Long id);

}
