package org.dsa.modules.remoteDiagnostic.dto.api;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class DiagnosticFunctionDto {

    private String funVal;

    @TableField(exist = false)
    private List<String> ecuList;
}
