package org.dsa.modules.reporter.vo.api;

import lombok.Data;

import java.io.Serializable;

@Data
public class WorkshopReVo implements Serializable {

    private String id;

    private String name;

}
