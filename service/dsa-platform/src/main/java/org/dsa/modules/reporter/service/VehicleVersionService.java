package org.dsa.modules.reporter.service;

import org.dsa.modules.reporter.dto.VehicleVersionDto;

import java.util.Map;

public interface VehicleVersionService {

    public void addHistory(VehicleVersionDto dto);

    public void updateOverview(VehicleVersionDto dto);

    Map<String, Object> getVehicleVersion(String vin);
}
