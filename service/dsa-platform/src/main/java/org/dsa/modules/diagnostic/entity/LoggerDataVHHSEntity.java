package org.dsa.modules.diagnostic.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 离线、车载日志接受类
 * 
 */
@Data
public class LoggerDataVHHSEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * ECU名称数组
	 */
	private String ecuName;
	/**
	 * 在线状态
	 */
	private String onlineStatus;
	/**
	 * 故障码个数
	 */
	private int dtcNumber;
	/**
	 * 故障码列表
	 */
	private List<LoggerDataDTCEntity> dtcList;
	/**
	 * 版本列表
	 */
	private List<LoggerDataActionVersionEntity> didData;
	/**
	 * 版本列表
	 */
	private List<LoggerDataActionVersionEntity> didList;
}
