package org.dsa.modules.reporter.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dsa.modules.reporter.dto.ClientDependencyDto;
import org.dsa.modules.reporter.entity.ClientDependence;
import org.dsa.modules.reporter.entity.ClientDependenceTree;

import java.util.List;
import java.util.Map;


public interface ClientDependenceTreeService extends IService<ClientDependenceTree> {

    public List<ClientDependence> queryDependencyByTag(String tag);

    public List<ClientDependencyDto> queryDependencyDataByTag(String tag);

    public List<ClientDependenceTree> formatInertData(List<ClientDependence> list, String tag);

    public List<ClientDependenceTree>getDependenceListByDeviceId(String pcid);

    List<Map<String, Object>> formatTreeData(List<ClientDependenceTree> list);

    public void deleteByDeviceId(String pcid);
}
