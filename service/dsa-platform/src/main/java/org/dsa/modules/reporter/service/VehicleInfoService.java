package org.dsa.modules.reporter.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.reporter.entity.VehicleInfo;
import org.dsa.modules.reporter.vo.VehInfoPageReVo;

public interface VehicleInfoService {

    public Page<VehicleInfo> selectPage(VehInfoPageReVo vo);

    public VehicleInfo getOneByVinCode(String vinCode);

    public VehicleInfo checkVehicleType(Long vehicleType);

    public void addOne(String vinCode, Long vehicleTypeId);
}
