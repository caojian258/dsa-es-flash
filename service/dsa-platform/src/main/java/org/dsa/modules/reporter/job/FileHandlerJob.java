package org.dsa.modules.reporter.job;

import lombok.extern.slf4j.Slf4j;
import org.dsa.config.SyncConfig;
import org.dsa.modules.reporter.event.OnlineUpdatePublisher;
import org.dsa.modules.reporter.event.SessionPublisher;
import org.dsa.modules.reporter.util.FilesUtil;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import java.io.File;

/***
 * 收集 log 文件传给各个业务处理器 file->event
 */
@Slf4j
@Component
@DisallowConcurrentExecution
public class FileHandlerJob extends QuartzJobBean {

    @Autowired
    SyncConfig syncConfig;

    @Autowired
    OnlineUpdatePublisher onlineUpdatePublisher;

    @Autowired
    SessionPublisher sessionPublisher;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        //上传目录的根目录
        String baseUploadPath = syncConfig.getBaseDir();

        String onlineUpdateUploadPath = syncConfig.getOnlineUpdateUploadDir();
        File onlineUpdateUploadFile = new File(onlineUpdateUploadPath);

        if(null !=onlineUpdateUploadFile.listFiles() &&
                onlineUpdateUploadFile.listFiles().length>0){
            for (File onlineFile: onlineUpdateUploadFile.listFiles()) {
                String fileName = onlineFile.getName();
                String path = onlineFile.getAbsolutePath();
                if(fileName.endsWith(syncConfig.getOnlineUpdateFormat())){
                    String tag = FilesUtil.makeTag();
                    onlineUpdatePublisher.handlerFile(path, tag, fileName);
                }
            }
        }

        String sessionUploadPath = syncConfig.getSessionUploadDir();
        File sessionUploadFile = new File(sessionUploadPath);
        if(null != sessionUploadFile.listFiles() &&
                sessionUploadFile.listFiles().length>0){
            for (File sessionFile: sessionUploadFile.listFiles()) {
                String fileName = sessionFile.getName();
                String path = sessionFile.getAbsolutePath();
                if(fileName.endsWith(syncConfig.getSessionFormat())){
                    String tag = FilesUtil.makeTag();
                    sessionPublisher.handlerFile(path, tag, fileName);
                }
            }
        }
    }
}
