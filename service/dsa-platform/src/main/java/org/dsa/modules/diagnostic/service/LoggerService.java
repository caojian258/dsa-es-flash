package org.dsa.modules.diagnostic.service;


import org.dsa.modules.diagnostic.entity.DiagnosticRecordEntity;
import org.springframework.web.multipart.MultipartFile;

import jakarta.servlet.http.HttpServletRequest;

/**
 * 诊断日志功能
 *
 */
public interface LoggerService {

    /**
     * 检查接口传入 入参是否正确
     *
     *
     */
    int checkInputRecord(DiagnosticRecordEntity diagnosticRecordEntity);

    /**
     * 保存操作记录
     */
    int saveRecord(DiagnosticRecordEntity diagnosticRecordEntity);
    /**
     * 操作日志上传
     */
    void saveRecordFile(MultipartFile file, String md5, HttpServletRequest req);
}

