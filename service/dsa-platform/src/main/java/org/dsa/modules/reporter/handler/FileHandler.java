package org.dsa.modules.reporter.handler;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.dsa.config.SyncConfig;
import org.dsa.modules.reporter.dto.OnlineUpdateFileDto;
import org.dsa.modules.reporter.dto.SessionFileDto;
import org.dsa.modules.reporter.dto.SessionLogFileDto;
import org.dsa.modules.reporter.service.FileLogService;
import org.dsa.modules.reporter.util.FilesUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
@Service
public class FileHandler {

    @Autowired
    SyncConfig syncConfig;

    @Autowired
    FileLogService fileLogService;

    public OnlineUpdateFileDto decompressOnlineUpdate(String source, String target) throws Exception {
        OnlineUpdateFileDto dto = new OnlineUpdateFileDto();

        File  sourceFile = new File(source);
        String sourceFileName = sourceFile.getName();
        String targetFileName = sourceFileName.replace(".zip", "");
        String targetDir = target + File.separator + targetFileName;
        File desDir = new File(targetDir);
        if(desDir.exists()){
            FileUtils.deleteDirectory(desDir);
        }
        FilesUtil.decompressZip(sourceFile, targetDir);
        if(sourceFileName.indexOf("_")>0){
            String[] sourceFileNames = sourceFileName.split("_");
            if(StrUtil.isNotBlank(sourceFileNames[0])){
                dto.setDeviceId(sourceFileNames[0]);
            }
            if(sourceFileNames.length>1 && StrUtil.isNotBlank(sourceFileNames[1])){
                dto.setSessionId(sourceFileNames[1]);
            }
        }
        dto.setDir(targetDir);
        File targetFile = new File(targetDir);
        File[] files = targetFile.listFiles();
        for (File file: files){
            if(file.getName().toLowerCase().endsWith(".db")){
                dto.setPath(file.getAbsolutePath());
                dto.setName(file.getName());
                break;
            }
        }
        return dto;
    }

    //这里要去重 已经处理成功的 一个诊断session多条诊断日志文件的去最后一次的
    public List<String> findReadyFiles(){

        SimpleDateFormat sdf = new SimpleDateFormat(syncConfig.getDirFormat());

        String uploadFileDir = syncConfig.getSessionUploadDir();

        log.info("uploadFileDir: {}", uploadFileDir);

        String targetFileDir = syncConfig.getSessionWorkDir();

        log.info("targetFileDir: {}", targetFileDir);

        File sessionFile = new File(uploadFileDir);
        File[] files = sessionFile.listFiles();
        List<String> targetFiles = new ArrayList<>();
        List<String> fileNameList = new ArrayList<>();

        if(null == files || files.length == 0){
            log.info("uploadFileDir has no files: {}", uploadFileDir);
            return null;
        }

        log.info("uploadFileDir files length: {}", files.length);

        for(File file : files) {
            if(!hasProcessed(file.getName())){
                fileNameList.add(file.getName());
            }
        }

        try {
            List<String> targetFileNames = filterSessionFile(fileNameList);
            for (File file : files){
                if(targetFileNames.contains(file.getName())){
                    targetFiles.add(file.getPath());
                }
            }
        }catch (Exception exception){
            log.error("uploadFileDir filterSessionFile: {}", exception.getMessage());
            for (File file : files){
                if(fileNameList.contains(file.getName())){
                    targetFiles.add(file.getPath());
                }
            }
        }
        return targetFiles;
    }

    private Boolean hasProcessed(String path){
        List<String> paths = fileLogService.succeedFiles(new Date());
        if(CollectionUtil.isEmpty(paths)){
            return false;
        }

        return paths.indexOf(path)>-1;
    }

    /***
     * VIN_{vin}_{sessionId}_{userId}_{deviceId}_{timeStamp}.zip
     * @param source
     * @param target
     * @return
     * @throws Exception
     */
    public SessionFileDto decompressSwtsSession(String source, String target) throws Exception{
        SessionFileDto dto  = new SessionFileDto();

        String md5Hex = DigestUtils.md5Hex(new FileInputStream(source));
        dto.setFileMD5(md5Hex);
        dto.setHandlerTime(new Date());
        File  sourceFile = new File(source);
        //VIN_L1234567890123456_3ff1c0c1-25aa-4444-8236-c2617615064c_ywc1_3b5d3c7d207e37dceeedd301e35e2e58_1694144756.zip
        //VIN_{vin}_{sessionId}_{userId}_{deviceId}_{timeStamp}.zip
        String sourceFileName = sourceFile.getName();
        String targetFileName = sourceFileName.replace(".zip", "");
        String targetDir = target + File.separator + targetFileName;
        File desDir = new File(targetDir);
        if(desDir.exists()){
            FileUtils.deleteDirectory(desDir);
        }
        FilesUtil.decompressZip(sourceFile, targetDir);

        dto.setPath(targetDir);
        String[] names = targetFileName.split("_");
        if(null != names && names.length>4){
            dto.setVinCode(names[1]);
            dto.setSessionId(names[2]);
            dto.setUserName(names[3]);
            dto.setPcid(names[4]);
        }
        dto.setSessionDir(targetDir);
        String sessionDirName = targetDir;
        log.info("sessionDirName path:{}", sessionDirName);

        File sessionDir = new File(sessionDirName);
        if(!sessionDir.canRead()){
            sessionDir.setReadable(true);
        }

        File[] files = sessionDir.listFiles();
        for(File file : files){
            String tmpName = file.getName();
            log.info("decompressSwtsSession tmpName: {}", tmpName);
            if(tmpName.endsWith(".slog")){
                dto.setFileName(tmpName);
                dto.setSqlitePath(file.getAbsolutePath());
                break;
            }
        }

        return dto;
    }

    /***
     * 解压 Session zip文件得到目标目录地址 文件名格式 Session_{session_id}_{workshop}_{timestamp}.zip
     * @param source 文件路径
     * @param target 目录
     * @return
     */
    public SessionFileDto decompressSession(String source, String target) throws Exception {
        SessionFileDto dto  = new SessionFileDto();

        String md5Hex = DigestUtils.md5Hex(new FileInputStream(source));
        dto.setFileMD5(md5Hex);
        dto.setHandlerTime(new Date());
        File  sourceFile = new File(source);
        String sourceFileName = sourceFile.getName();
        String targetFileName = sourceFileName.replace(".zip", "");
        String targetDir = target + File.separator + targetFileName;
        File desDir = new File(targetDir);
        if(desDir.exists()){
            FileUtils.deleteDirectory(desDir);
        }
        FilesUtil.decompressZip(sourceFile, targetDir);
        dto.setFileName(targetFileName);
        dto.setPath(targetDir);
        String[] names = targetFileName.split("_");
        if(null != names && names.length>1){
            dto.setSessionId(names[1]);
            dto.setPcid(names[2]);
        }

        String sessionDirName = target + File.separator + targetFileName + File.separator +names[0]+"_"+names[1];
        log.info("sessionDirName path:{}", sessionDirName);

        File sessionDir = new File(sessionDirName);
        if(!sessionDir.canRead()){
            sessionDir.setReadable(true);
        }

        File[] files = sessionDir.listFiles();

        if(null == files || files.length == 0){
            log.info("sessionDirName {} has no files", sessionDirName);
            return dto;
        }

        for(File file : files){
            String tmpName = file.getName();
            if(tmpName.endsWith(".vin")){
                dto.setVinCode(tmpName.replace(".vin", ""));
            }
            if(tmpName.startsWith("SharedData")){
                dto.setSharedDataPath(file.getPath());
            }

            if(tmpName.startsWith("SessionStates")){
                dto.setSessionStatePath(file.getPath());
            }
        }

        if(null == dto.getVinCode()){
            dto.setVinCode("UNDEFINED");
        }

        //找到sqlite文件
        String sqliteNamePrefixFormat = "VIN_%s_%s";
        String sqliteNamePrefix =  String.format(sqliteNamePrefixFormat, dto.getVinCode(), dto.getSessionId());
        for(File file : files){
            String fileName = file.getName();
            if(fileName.startsWith(sqliteNamePrefix) && fileName.endsWith("sqlite")){
                dto.setSqlitePath(file.getPath());
                break;
            }
        }

        if(new File(target + File.separator + targetFileName+File.separator +"smartstart_status.json").exists()){
            dto.setStatusPath(target + File.separator + targetFileName+File.separator +"smartstart_status.json");
        }


        return dto;
    }


    public String makeTag(){
        return FilesUtil.makeTag();
    }


    /***
     * 相同session下拿最后一条
     * @param nameList
     * @return
     * @throws ParseException
     */
    public List<String> filterSessionFile(List<String> nameList) throws ParseException {

        List<String> targetName = new ArrayList<>();
        Map<String,List<SessionLogFileDto>> sessionMap = new HashMap<>();
        //VIN_L1234567890123456_3ff1c0c1-25aa-4444-8236-c2617615064c_ywc1_3b5d3c7d207e37dceeedd301e35e2e58_1694144756
        for (String name: nameList) {
            String[] nameArr = name.split("_");
            SessionLogFileDto dto = new SessionLogFileDto();
            dto.setFileName(name);
            dto.setPrefix(nameArr[0]);
            dto.setVin(nameArr[1]);
            dto.setSessionId(nameArr[2]);
            dto.setTimestamp( new Date(Long.valueOf(nameArr[5].replace(".zip", ""))));
            List<SessionLogFileDto> dtos = sessionMap.get(dto.getSessionId());

            if(null == dtos){
                dtos = new ArrayList<>();
                dtos.add(dto);
            }else{
                SessionLogFileDto lastSession = null;
                for (SessionLogFileDto slf: dtos){
                    if(dto.getTimestamp().compareTo(slf.getTimestamp())>0){
                        lastSession = dto;
                    }else{
                        lastSession = slf;
                    }
                }
                dtos = new ArrayList<>();
                dtos.add(lastSession);
            }
            sessionMap.put(dto.getSessionId(), dtos);
        }

        Iterator<String> iterator = sessionMap.keySet().iterator();
        while (iterator.hasNext()) {
            String id = iterator.next();
            List<SessionLogFileDto> slfList = sessionMap.get(id);
            targetName.add(slfList.get(0).getFileName());
        }

        return targetName;
    }
}
