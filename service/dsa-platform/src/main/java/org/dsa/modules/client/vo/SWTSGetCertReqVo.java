package org.dsa.modules.client.vo;


import lombok.Data;
import lombok.ToString;
import org.dsa.common.validator.group.ReqGroup;
import org.hibernate.validator.constraints.Length;

import jakarta.validation.constraints.NotBlank;

@Data
@ToString(callSuper = true)
public class SWTSGetCertReqVo {

    @NotBlank(message = "pcid is null", groups = {ReqGroup.class})
    private String pcid;

    @NotBlank(message = "type is null", groups = {ReqGroup.class})
    private String type;

    private String ecuName;

}
