package org.dsa.modules.reporter.handler;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import java.text.ParseException;
import org.dsa.common.pojo.RowMapper;
import org.dsa.common.utils.RedisUtils;
import org.dsa.common.utils.SqliteUtils;
import org.dsa.modules.reporter.constant.RedisConstant;
import org.dsa.modules.reporter.dao.*;
import org.dsa.modules.reporter.dto.OnlineUpdateLogDto;
import org.dsa.modules.reporter.entity.*;
import org.dsa.modules.reporter.service.ClientUpgradeService;
import org.dsa.modules.reporter.util.FilesUtil;
import org.dsa.modules.sys.entity.SysUserEntity;
import org.dsa.modules.sys.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/***
 * 解析online update sqlite 包括组件/软件信息 客户端电脑信息
 */
@Slf4j
@Service
public class OnlineUpdateLogHandler {

    @Autowired
    ClientUpgradeMapper upgradeMapper;

    @Autowired
    ClientDependenceMapper dependenceMapper;

    @Autowired
    CurrentComponentInfoMapper currentComponentInfoMapper;

    @Autowired
    CurrentSoftwareInfoMapper currentSoftwareInfoMapper;

    @Autowired
    FutureComponentInfoMapper futureComponentInfoMapper;

    @Autowired
    FutureSoftwareInfoMapper futureSoftwareInfoMapper;

    @Autowired
    SoftwareBackMapper softwareBackMapper;

    @Autowired
    ComponentBackMapper componentBackMapper;

    @Autowired
    SysUserService userService;

    @Autowired
    ClientUpgradeService  clientUpgradeService;

    @Transactional(rollbackFor = Exception.class)
    public OnlineUpdateLogDto saveData(String path, String tag, String name, String sessionId, String deviceId) throws SQLException, ClassNotFoundException {

        log.info("start handler online update file: {},  tag: {}", name, tag);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        SqliteUtils utils = new SqliteUtils(path);
        OnlineUpdateLogDto dto = new OnlineUpdateLogDto();
        dto.setDeviceId(deviceId);

        Boolean installLog = utils.exists("t_installlog");
        if(installLog){

            RowMapper<ClientUpgrade> rm = new RowMapper<ClientUpgrade>() {
                @Override
                public ClientUpgrade mapRow(ResultSet rs, int index) throws SQLException {

                    ClientUpgrade result = new ClientUpgrade();
                    Integer id = rs.getInt("id");

                        result.setSessionId(rs.getString("session_id"));
                        Integer softwareId =  rs.getInt("software_id");
                        Integer componentId =  rs.getInt("component_id");
                        result.setDeviceId(deviceId);
                        result.setTag(tag);
                        result.setUpdateId(id);
                        result.setComponentId(componentId);
                        result.setSoftwareId(softwareId);
                        try {
                            if(StrUtil.isNotBlank(rs.getString("start_time"))){
                                result.setStartTime(simpleDateFormat.parse(rs.getString("start_time")));
                            }
                        } catch (ParseException e) {
                            log.error("Diagnostic StartTime ParseException: {}", e.getMessage());
                        }
                        try {
                            if(StrUtil.isNotBlank(rs.getString("end_time"))){
                                result.setEndTime(simpleDateFormat.parse(rs.getString("end_time")));
                            }
                        } catch (ParseException e) {
                            log.error("Diagnostic EndTime ParseException: {}", e.getMessage());
                        }
                        try {
                            if(StrUtil.isNotBlank(rs.getString("remove_time"))){
                                result.setRemoveTime(simpleDateFormat.parse(rs.getString("remove_time")));
                            }
                        } catch (ParseException e) {
                            log.error("Diagnostic remove_time ParseException: {}", e.getMessage());
                        }
                        result.setUsername(rs.getString("user_name"));
                        String userDisplayName = rs.getString("user_display_name");
                        result.setUserDisplayName(userDisplayName);
                        if(StrUtil.isNotBlank(result.getUsername())){
                            SysUserEntity user = userService.queryByUserName(result.getUsername());
                            if(null != user){
                                result.setWorkshopId(user.getWorkshopId());
                                result.setWorkshopName(user.getWorkshopName());
                            }
                        }
                        result.setUpdateResult(rs.getInt("update_result"));
                        result.setUpdateTarget(rs.getInt("update_target"));
                        result.setInstallAction(rs.getInt("install_action"));

                        if(StrUtil.isNotBlank(rs.getString("software_current_version")) ){
                            result.setSoftwareCurrentVersion(rs.getString("software_current_version").toUpperCase());
                        }

                        if(StrUtil.isNotBlank(rs.getString("component_current_version"))){
                            result.setComponentCurrentVersion(rs.getString("component_current_version").toUpperCase());
                        }

                        result.setSoftwareCurrentVersionNumber(rs.getInt("software_current_version_number"));
                        result.setSoftwareTargetVersionNumber(rs.getInt("software_target_version_number"));
                        result.setComponentCurrentVersionNumber(rs.getInt("component_current_version_number"));
                        result.setComponentTargetVersionNumber(rs.getInt("component_target_version_number"));

                        if(StrUtil.isNotBlank(rs.getString("software_target_version"))){
                            result.setSoftwareTargetVersion(rs.getString("software_target_version").toUpperCase());
                        }

                        if(StrUtil.isNotBlank(rs.getString("component_target_version"))){
                            result.setComponentTargetVersion(rs.getString("component_target_version").toUpperCase());
                        }

                        upgradeMapper.insert(result);
                    return result;
                }
            };

            try {
                int total = utils.executeStatsQuery("select count(*) from t_installlog");
                dto.setInstallTotal(total);
            }catch (Exception ex){
            }

//            Set<Integer> idSet = clientUpgradeService.queryClientUpdateIdWithoutCache(deviceId);
            String sql = "select * from t_installlog order by start_time asc";
//
//            if(CollectionUtil.isNotEmpty(idSet)){
//                Set<String> idStrSet = idSet.stream().map(String::valueOf).collect(Collectors.toSet());
//                String condition = " id not in ("+ String.join(",", idStrSet) +")";
//                sql = "select * from t_installlog where "+condition+" order by start_time asc";
//            }
            List<ClientUpgrade> res = utils.executeQuery(sql, rm);
            dto.setInstallRecordTotal(res.size());
        }

        Boolean currentComponentInfo = utils.exists("t_currentcomponentinfo");
        if(currentComponentInfo){
            RowMapper<CurrentComponentInfo> componentInfoRowMapper = new RowMapper<CurrentComponentInfo>() {
                @Override
                public CurrentComponentInfo mapRow(ResultSet rs, int index) throws SQLException {
                    CurrentComponentInfo info = new CurrentComponentInfo();
                    info.setDeviceId(deviceId);
                    info.setTag(tag);
                    info.setSessionId(sessionId);
                    info.setSoftwareId(rs.getInt("software_id"));
                    info.setSoftwareVersionNumber(rs.getInt("software_version_number"));
                    info.setComponentId(rs.getInt("component_id"));
                    info.setComponentName(rs.getString("component_name"));
                    if(StrUtil.isNotBlank(rs.getString("component_version_name"))){
                        info.setComponentVersionName(rs.getString("component_version_name").toUpperCase());
                    }

                    info.setComponentVersionNumber(rs.getInt("component_version_number"));
                    info.setComponentInstallPath(rs.getString("component_install_path"));

                    try {
                        if(StrUtil.isNotBlank(rs.getString("start_update_time"))){
                            info.setStartUpdateTime(simpleDateFormat.parse(rs.getString("start_update_time")));
                        }
                    } catch (ParseException e) {
                        log.error("Diagnostic start_update_time ParseException: {}", e.getMessage());
                    }

                    try {
                        if(StrUtil.isNotBlank(rs.getString("end_update_time"))){
                            info.setEndUpdateTime(simpleDateFormat.parse(rs.getString("end_update_time")));
                        }
                    } catch (ParseException e) {
                        log.error("Diagnostic start_update_time ParseException: {}", e.getMessage());
                    }

                    info.setUpdateFailCount(rs.getInt("update_fail_count"));
                    info.setUpdateResult(rs.getInt("update_result"));
                    info.setInstallAction(rs.getInt("install_action"));
                    info.setComponentStatus(rs.getInt("component_status"));
//                    info.setDownloadProgress(rs.getInt("download_progress"));
                    info.setFileSize(rs.getString("file_size"));
                    info.setFileMd5(rs.getString("file_md5"));
                    info.setReleaseNotes(rs.getString("release_notes"));
                    info.setFeatureDescription(rs.getString("feature_description"));
                    info.setUpdateType(rs.getInt("update_type"));
                    info.setCurrentUpdateType(rs.getInt("current_update_type"));
                    currentComponentInfoMapper.insert(info);
                    return info;
                }
            };

            List<CurrentComponentInfo> resCurrentComponents = utils.executeQuery("select * from t_currentcomponentinfo order by start_update_time", componentInfoRowMapper);
            dto.setCurrentComponentTotal(resCurrentComponents.size());
        }


        Boolean currentSoftwareInfo = utils.exists("t_currentsoftwareinfo");
        if(currentSoftwareInfo){
            RowMapper<CurrentSoftwareInfo> softwareInfoRowMapper = new RowMapper<CurrentSoftwareInfo>() {
                @Override
                public CurrentSoftwareInfo mapRow(ResultSet rs, int index) throws SQLException {

                    CurrentSoftwareInfo info = new CurrentSoftwareInfo();
                    info.setDeviceId(deviceId);
                    info.setTag(tag);
                    info.setSessionId(sessionId);
                    info.setSoftwareId(rs.getInt("software_id"));
                    info.setSoftwareName(rs.getString("software_name"));
                    if(StrUtil.isNotBlank(rs.getString("software_version_name"))){
                        info.setSoftwareVersionName(rs.getString("software_version_name").toUpperCase());
                    }
                    info.setSoftwareVersionNumber(rs.getInt("software_version_number"));
                    info.setSoftwareWorkPath(rs.getString("software_work_path"));
                    info.setSoftwareInstallPath(rs.getString("software_install_path"));

                    try {
                        if(StrUtil.isNotBlank(rs.getString("start_update_time"))){
                            info.setStartUpdateTime(simpleDateFormat.parse(rs.getString("start_update_time")));
                        }
                    } catch (ParseException e) {
                        log.error("CurrentSoftwareInfo StartTime ParseException: {}", e.getMessage());
                    }

                    try {
                        if(StrUtil.isNotBlank(rs.getString("end_update_time"))){
                            info.setEndUpdateTime(simpleDateFormat.parse(rs.getString("end_update_time")));
                        }
                    } catch (ParseException e) {
                        log.error("CurrentSoftwareInfo EndTime ParseException: {}", e.getMessage());
                    }

                    info.setUpdateFailCount(rs.getInt("update_fail_count"));
                    info.setUpdateResult(rs.getInt("update_result"));
                    info.setInstallAction(rs.getInt("install_action"));
                    info.setSoftwareStatus(rs.getInt("software_status"));
                    info.setDownloadProgress(rs.getInt("download_progress"));
                    info.setReleaseNotes(rs.getString("release_notes"));
                    info.setFeatureDescription(rs.getString("feature_description"));
                    info.setFileSize(rs.getString("file_size"));
                    info.setUpdateId(rs.getString("update_id"));
                    currentSoftwareInfoMapper.insert(info);
                    return info;
                }
            };

            List<CurrentSoftwareInfo> resCurrentSoftware = utils.executeQuery("select * from t_currentsoftwareinfo order by start_update_time", softwareInfoRowMapper);
            dto.setCurrentSoftwareTotal(resCurrentSoftware.size());
        }

        Boolean dependenceInfo = utils.exists("t_dependenceinfo");
        if(dependenceInfo){
            RowMapper<ClientDependence> cdRowMapper = new RowMapper<ClientDependence>() {
                @Override
                public ClientDependence mapRow(ResultSet rs, int index) throws SQLException {

                    ClientDependence info = new ClientDependence();
                    info.setDeviceId(deviceId);
                    info.setTag(tag);
                    info.setSessionId(sessionId);
                    info.setSoftwareId(rs.getInt("software_id"));
                    info.setSoftwareVersionNumber(rs.getInt("software_version_number"));
                    info.setComponentId(rs.getInt("component_id"));
                    info.setComponentVersionNumber(rs.getInt("component_version_number"));
                    info.setDependencyComponentId(rs.getInt("dependency_component_id"));
                    info.setDependencyComponentVersionNumber(rs.getInt("dependency_component_version_number"));
                    dependenceMapper.insert(info);
                    return info;
                }
            };

            List<ClientDependence> resClientDependence = utils.executeQuery("select * from t_dependenceinfo", cdRowMapper);
            dto.setDependenceTotal(resClientDependence.size());
        }


        Boolean futureComponentInfo = utils.exists("t_futurecomponentinfo");
        if(futureComponentInfo){

            RowMapper<FutureComponentInfo> fcRm = new RowMapper<FutureComponentInfo>() {
                @Override
                public FutureComponentInfo mapRow(ResultSet rs, int index) throws SQLException {
                    FutureComponentInfo info = new FutureComponentInfo();
                    info.setDeviceId(deviceId);
                    info.setTag(tag);
                    info.setSessionId(sessionId);
                    info.setSoftwareId(rs.getInt("software_id"));
                    info.setComponentId(rs.getInt("component_id"));

                    if(StrUtil.isNotBlank(rs.getString("component_version_name"))){
                        info.setComponentVersionName(rs.getString("component_version_name").toUpperCase());
                    }

                    info.setComponentVersionNumber(rs.getInt("component_version_number"));
                    info.setComponentDownloadPath(rs.getString("component_download_path"));
                    info.setUpdateType(rs.getInt("update_type"));
                    info.setReleaseNotes(rs.getString("release_notes"));
                    info.setFeatureDescription(rs.getString("feature_description"));
                    info.setFileMd5(rs.getString("file_md5"));
                    info.setFileSize(rs.getString("file_size"));
                    info.setOrderNumber(rs.getInt("order_number"));
                    info.setDownloadProgress(rs.getInt("download_progress"));
                    info.setComponentDownloadUrl(rs.getString("component_download_url"));
                    futureComponentInfoMapper.insert(info);
                    return info;
                }
            };

            List<FutureComponentInfo> resFutureComponents = utils.executeQuery("select * from t_futurecomponentinfo", fcRm);
            dto.setFeatureComponentTotal(resFutureComponents.size());
        }


        Boolean futureSoftwareInfo = utils.exists("t_futuresoftwareinfo");
        if(futureSoftwareInfo){
            RowMapper<FutureSoftwareInfo> fsRm = new RowMapper<FutureSoftwareInfo>() {
                @Override
                public FutureSoftwareInfo mapRow(ResultSet rs, int index) throws SQLException {
                    FutureSoftwareInfo info = new FutureSoftwareInfo();
                    info.setDeviceId(deviceId);
                    info.setTag(tag);
                    info.setSessionId(sessionId);
                    info.setSoftwareId(rs.getInt("software_id"));
                    if(StrUtil.isNotBlank(rs.getString("software_version_name"))){
                        info.setSoftwareVersionName(rs.getString("software_version_name").toUpperCase());
                    }
                    info.setSoftwareVersionNumber(rs.getInt("software_version_number"));
                    info.setUpdateType(rs.getInt("update_type"));


                    try {
                        if(StrUtil.isNotBlank(rs.getString("expire_date"))){
                            info.setExpireDate(sdf.parse(rs.getString("expire_date")));
                        }

                    }catch (Exception ex){
                        log.error("futuresoftwareinfo expire_date ParseException: {}", ex.getMessage());
                    }
                    info.setExpireWarningDays(rs.getInt("expire_warning_days"));
                    info.setReleaseNotes(rs.getString("release_notes"));
                    info.setFeatureDescription(rs.getString("feature_description"));
                    info.setFileSize(rs.getString("file_size"));
                    info.setDownloadProgress(rs.getInt("download_progress"));
                    info.setUpdateId(rs.getString("update_id"));
                    futureSoftwareInfoMapper.insert(info);

                    return info;
                }
            };

            List<FutureSoftwareInfo> resFutureSoftwareList = utils.executeQuery("select * from t_futuresoftwareinfo", fsRm);
            dto.setFeatureSoftwareTotal(resFutureSoftwareList.size());
        }


        Boolean softwareRollbackTable = utils.exists("t_softwareback");

        if(softwareRollbackTable){
            RowMapper<SoftwareBack> softwareBackRowMapper = new RowMapper<SoftwareBack>() {
                @Override
                public SoftwareBack mapRow(ResultSet rs, int index) throws SQLException {
                    SoftwareBack info = new SoftwareBack();
                    info.setDeviceId(deviceId);
                    info.setTag(tag);
                    info.setSessionId(sessionId);
                    info.setSoftwareId(rs.getInt("software_id"));

                    if(StrUtil.isNotBlank(rs.getString("software_version_name"))){
                        info.setSoftwareVersionName(rs.getString("software_version_name").toUpperCase());
                    }

                    info.setSoftwareVersionNumber(rs.getInt("software_version_number"));
                    info.setUpdateType(rs.getInt("update_type"));

                    try {
                        if(StrUtil.isNotBlank(rs.getString("validity_period"))){
                            info.setValidityPeriod(sdf.parse( rs.getString("validity_period")));
                        }
                    }catch (Exception ex){
                        log.error("t_softwareback validity_period ParseException: {}", ex.getMessage());
                    }
                    info.setNearValidityPeriodDays(rs.getInt("near_validity_period_days"));
                    info.setReleaseNotes(rs.getString("release_notes"));
                    info.setFeatureDescription(rs.getString("feature_description"));
                    info.setFileSize(rs.getString("file_size"));
                    info.setUpdateId(rs.getString("update_id"));
                    softwareBackMapper.insert(info);
                    return info;
                }
            };
            List<SoftwareBack> softwareBacks = utils.executeQuery("select * from t_softwareback", softwareBackRowMapper);
            dto.setSoftwareRollbackTotal(softwareBacks.size());
        }

        Boolean componentRollback = utils.exists("t_componentback");
        if(componentRollback){
            RowMapper<ComponentBack> componentBackRowMapper = new RowMapper<ComponentBack>() {
                @Override
                public ComponentBack mapRow(ResultSet rs, int index) throws SQLException {
                    ComponentBack info = new ComponentBack();
                    info.setTag(tag);
                    info.setDeviceId(deviceId);
                    info.setSessionId(sessionId);
                    info.setSoftwareId(rs.getInt("software_id"));
                    info.setComponentId(rs.getInt("component_id"));

                    if(StrUtil.isNotBlank(rs.getString("component_version_name"))){
                        info.setComponentVersionName(rs.getString("component_version_name").toUpperCase());
                    }

                    info.setComponentVersionNumber(rs.getInt("component_version_number"));
                    info.setComponentVersionPath(rs.getString("component_version_path"));
                    info.setReleaseNotes(rs.getString("release_notes"));
                    info.setFeatureDescription(rs.getString("feature_description"));
                    info.setFileMd5(rs.getString("file_md5"));
                    info.setFileSize(rs.getString("file_size"));
                    info.setUpdateType(rs.getInt("update_type"));
                    componentBackMapper.insert(info);

                    return info;
                }
            };

            List<ComponentBack> componentBacks = utils.executeQuery("select * from t_componentback", componentBackRowMapper);
            dto.setComponentRollbackTotal(componentBacks.size());
        }


        return dto;
    }
}
