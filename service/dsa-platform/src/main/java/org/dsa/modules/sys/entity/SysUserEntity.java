package org.dsa.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import jakarta.validation.constraints.Email;
import lombok.Data;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.common.validator.group.UpdateGroup;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 系统用户
 *
 */
@Data
@TableName("sys_user")
public class SysUserEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**
	 * 用户ID
	 */
	@TableId(type = IdType.AUTO)
	private Long userId;

	/**
	 * 临时变量
	 */
	@TableField(exist=false)
	private String code;

	/**
	 * 中文名
	 */
	private String chineseName;

	/**
	 * 英文名
	 */
	private String englishName;

	/**
	 * 员工号
	 */
	private String empNo;

	/**
	 * 临时变量
	 */
	@TableField(exist=false)
	private String value;

	/**
	 * 用户名
	 */
	@NotBlank(message="员工号不能为空", groups = {AddGroup.class, UpdateGroup.class})
	private String username;

	/**
	 * oem
	 */
	private String oemCode;

	/**
	 * 所属部门
	 */
	@NotNull(message = "经销商Id不能为空", groups = {AddGroup.class, UpdateGroup.class})
	private Long workshopId;

	@NotBlank(message="经销商不能为空", groups = {AddGroup.class, UpdateGroup.class})
	private String workshopName;

	/**
	 * 所属组ID
	 */
	@TableField(exist=false)
	private Long groupId;

	/**
	 * 所属组名
	 */
	@TableField(exist=false)
	private  String groupName;

	/**
	 * 密码
	 */
	@NotBlank(message="密码不能为空", groups = AddGroup.class)
	private String password;

	/**
	 * 盐
	 */
	private String salt;

	/**
	 * 邮箱
	 */
	private String email;

	/**
	 * 手机号
	 */
	private String mobile;

	/**
	 * 状态  0：禁用   1：正常
	 */
	private Integer status;

	/**
	 * 角色ID列表
	 */
	@TableField(exist=false)
	private List<Long> roleIdList;

	/**
	 * 组ID列表
	 */
	@TableField(exist=false)
	private List<Long> groupIdList;

	@TableField(exist=false)
	private List<SysUserGroupEntity> userGroupList;

	@TableField(exist=false)
	private String roleName;

	/**
	 * 创建者ID
	 */
	private Long createUserId;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/*
	同步用户修改时间
	*/
	private Date updateTime;

	/**
	 * 用户license修改
	 */
	@TableField(exist = false)
	private List<String> licenseJsonStrList;

}
