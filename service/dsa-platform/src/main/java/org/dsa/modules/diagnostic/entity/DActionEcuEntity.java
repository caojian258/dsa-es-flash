package org.dsa.modules.diagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("d_action_ecu")
public class DActionEcuEntity implements Serializable
{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 功能执行id
     */
    private String actionId;
    /**
     * 功能码
     */
    private String ecuName;
    /**
     * status,0:success,1:failed
     */
    private int status;

    /**
     * 备注
     */
    private String description;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建时间
     */
    private Date endTime;

    /**
     * 故障码数量
     */
    private Integer ecuCount;

}
