package org.dsa.modules.diagnostic.util;

import ch.qos.logback.classic.Logger;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.diagnostic.dao.*;
import org.dsa.modules.diagnostic.entity.*;
import org.dsa.modules.diagnostic.entity.mqtt.callback.MqBodyDataDTCVO;
import org.dsa.modules.diagnostic.entity.mqtt.callback.MqEntity;
import org.dsa.modules.diagnostic.entity.mqtt.send.MqRequestBodyEntity;
import org.dsa.modules.diagnostic.entity.mqtt.send.MqRequestEntity;
import org.dsa.modules.diagnostic.entity.mqtt.send.MqRequestHeaderEntity;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import jakarta.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Component
public class DiagnosticUtils {
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass());

    @Resource
    private RedisUtils redisUtils;
    @Resource
    private MqSendUtils mqSendUtils;
    @Resource
    private DSessionInfoDao dSessionInfoDao;
    @Resource
    private DSessionActionDao dSessionActionDao;
    @Resource
    private DActionInfoDao dActionInfoDao;
    @Resource
    private DActionDtcDao dActionDtcDao;
    @Resource
    private DActionEcuDao dActionEcuDao;

    /**
     * 发送mq 消息
     */
    public void sendMqMessage(String vin, String actionId, Constant.Function function, Constant.Action action, MqRequestBodyEntity mqRequestBodyEntity) {
        MqRequestHeaderEntity h = new MqRequestHeaderEntity();
        h.setRid("0");
        h.setMid(actionId);
        h.setFunctionCode(function.getFunctionCode());
        h.setAction(action.getActionCode());
        h.setSource("003");
        h.setRespTopic(Constant.MQ_RESP_TOIPIC);

        MqRequestEntity mqRequestEntity = new MqRequestEntity();
        mqRequestEntity.setHeader(h);
        if(mqRequestBodyEntity == null ) {
            mqRequestEntity.setBody(new MqRequestBodyEntity());
        }else{
            mqRequestEntity.setBody(mqRequestBodyEntity);
        }
        logger.info("{} send Mqtt ,Topic:{} Info:{}",function.getFunctionName(),"REQ_"+vin, JSONObject.toJSONString(mqRequestEntity));
        mqSendUtils.sendToMqtt("REQ_"+vin,JSONObject.toJSONString(mqRequestEntity));

        //命令执行session 与 action关系 -> 用于一辆车仅同时仅运行一条诊断   key :S_A_vin ,value：actionId
        logger.info("{} set redis key{},value{}",function.getFunctionName(), Constant.REDIS_RUN_VIN + vin,actionId);
        redisUtils.set(Constant.REDIS_RUN_VIN + vin,actionId,function.getExpire() );

        //命令执行action    诊断命令执行->sessionId： key:R_action  , value:vin
        logger.info("{} set redis key{},value{}",function.getFunctionName(), Constant.REDIS_ACTION + actionId,vin);
        redisUtils.set(Constant.REDIS_ACTION + actionId,vin,function.getExpire() );

        //命令执行action    诊断命令执行->actionId： key:P_actionId  , value: object
        logger.info("{} set redis key{},value{}",function.getFunctionName(), Constant.REDIS_ACTION_RESULT + actionId, Constant.RESULTS.SEND.getValue());
        redisUtils.set(Constant.REDIS_ACTION_STATUS + actionId, Constant.RESULTS.SEND.getValue(),function.getExpire() );
    }

    /**
     *保存action 信息
     */
    public void saveAction(String sessionId,  String actionId, String[] ecuNames, Constant.Function function, Constant.Action action) {
        //sessionId为空,未开始诊断 不保存
        if(StringUtils.isEmpty(sessionId)){
            return;
        }

        DSessionActionEntity sa = new DSessionActionEntity();
        sa.setSessionId(sessionId);
        sa.setActionId(actionId);
        dSessionActionDao.insert(sa);
        logger.info("Begin save DSessionAction:{} ",JSONObject.toJSONString(sa));

        DActionInfoEntity actionInfoEntity = new DActionInfoEntity();
        actionInfoEntity.setActionId(actionId);
        actionInfoEntity.setFunctionCode(function.getFunctionCode());
        actionInfoEntity.setActionCode(action.getActionCode());
        actionInfoEntity.setCreateTime(new Date());
        actionInfoEntity.setStartTime(new Date());
        dActionInfoDao.insert(actionInfoEntity);
        logger.info("{} save DActionInfo:{} ",function.getFunctionName(),JSONObject.toJSONString(actionInfoEntity));
        if(ecuNames !=null && ecuNames.length >0){
            for(String ecuName : ecuNames) {
                DActionEcuEntity dActionEcuEntity = new DActionEcuEntity();
                dActionEcuEntity.setActionId(actionId);
                dActionEcuEntity.setEcuName(ecuName);
                dActionEcuEntity.setCreateTime(new Date());
                dActionEcuDao.insert(dActionEcuEntity);
                logger.info("{} save DActionEcu:{} ",function.getFunctionName(),JSONObject.toJSONString(dActionEcuEntity));
            }
        }

    }
    /**
     *获取4位随机数
     */
    public String get4Code(){
        StringBuffer sb = new StringBuffer();
        String codes ="qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890";
        Random random = new Random();
        for(int i=0;i<4;i++){
            char c = codes.charAt(random.nextInt(codes.length()));
            if(sb.length()==4) break;
            if(!sb.toString().contains(c+"")) sb.append(c);
        }
        return sb.toString();
    }


    /**
     * 发送mq 消息
     */
    public void sendTestMqMessage(String actionId, Constant.Function function, Constant.Action action, String[] ecuNames, JSONObject bodyData) {
        MqRequestHeaderEntity h = new MqRequestHeaderEntity();
        h.setRid(actionId);
        h.setMid("0");
        h.setFunctionCode(function.getFunctionCode());
        h.setAction(action.getActionCode());
        h.setSource("001");
        h.setAck(2);

        MqRequestBodyEntity b = new MqRequestBodyEntity();
        b.setEcuName(ecuNames);
        b.setData(bodyData);

        MqRequestEntity mqRequestEntity = new MqRequestEntity();
        mqRequestEntity.setHeader(h);
        mqRequestEntity.setBody(b);

        logger.info("Begin send Mqtt :{}", JSONObject.toJSONString(mqRequestEntity));
        mqSendUtils.sendToMqtt(Constant.MQ_RESP_TOIPIC,JSONObject.toJSONString(mqRequestEntity));

        logger.info("sendMqMessage ---- Redis key({}) setValue:{}", Constant.REDIS_ACTION + actionId);
    }

}
