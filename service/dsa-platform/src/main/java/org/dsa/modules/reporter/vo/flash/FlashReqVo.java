package org.dsa.modules.reporter.vo.flash;

import lombok.Data;
import lombok.ToString;
import org.dsa.modules.reporter.pojo.PageParam;

import java.util.Date;
import java.util.List;

@Data
@ToString
public class FlashReqVo extends PageParam {

    public String vin;

    public String version;

    public String prevVersion;

    public Integer id;

    public Date collectTime;

    public String ecuName;

    List<Integer> vehicleTypeIds;

    List<String>  taskTime;

    List<String> vehicleTypes;
}
