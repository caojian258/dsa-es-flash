package org.dsa.modules.remoteDiagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskActiveEntity;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskEcuEntity;

import java.util.List;

@Mapper
public interface DiagnosticTaskActiveDao extends BaseMapper<DiagnosticTaskActiveEntity> {



    @Select("select * from r_diag_task_active i where i.session_id = #{sessionId} ")
    DiagnosticTaskActiveEntity getInfoBySessionId(@Param("sessionId") String sessionId);

    @Update("update r_diag_task_active  set status = 2 where id = #{id} ")
    void terminationActive(@Param("id") Long  id);

    @Select("select * from r_diag_task_active i where i.campaign_id = #{taskId} and i.status = #{status} ")
    List<DiagnosticTaskActiveEntity> getListByTask(@Param("v")Integer status,@Param("taskId") Long taskId);

    @Update("update r_diag_task_active set status = 2,mes='Gradiental verification failure' where campaign_id = #{taskId} and status = #{status} ")
    void updateByTaskId(@Param("status") int i,@Param("taskId") Long taskId);

}
