package org.dsa.modules.remoteDiagnostic.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TaskDetailTypeEnum {
//    UPDATE("UPDATE"),
    MATCH("MATCH"),
    DOWNLOAD("DOWNLOAD"),
    UPLOAD_LOG("UPLOAD:LOG"),
    RemoteVerification("RemoteVerification"),
//    REJECT("REJECT"),
//    CANCEL("CANCEL"),
    ConditionalVerification("ConditionalVerification"),
    GradientCheck("GradientCheck"),
    ConfirmExecution_REJECT("ConfirmExecution:REJECT"),
    ConfirmExecution_CANCEL("ConfirmExecution:CANCEL"),
    ConfirmExecution_EXECUTE("ConfirmExecution:EXECUTE"),
    TERMINATION("TERMINATION"),
    DiagnosticResult("DiagnosticResult"),
    DiagnosticResult_SUCCESS("SUCCESS"),
    DiagnosticResult_FAIL("FAIL"),
//    EXECUTE("EXECUTE"),
    ;

    @EnumValue
    private final String value;
}