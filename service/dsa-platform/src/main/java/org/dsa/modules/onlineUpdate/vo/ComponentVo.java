package org.dsa.modules.onlineUpdate.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dsa.modules.onlineUpdate.entity.ComponentEntity;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ComponentVo extends ComponentEntity {

    /**
     * 组件名称
     */
    private String componentName;

    /**
     * 组件版本
     */
    private List<String> versions;

}
