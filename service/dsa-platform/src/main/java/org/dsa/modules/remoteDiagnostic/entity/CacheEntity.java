package org.dsa.modules.remoteDiagnostic.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("r_cache")
public class CacheEntity {

    private String cKey;

    private String cVal;

    private Date createdAt;
}
