package org.dsa.modules.sys.service;

import org.dsa.modules.sys.entity.SysLocationEntity;

import java.util.List;
import java.util.Map;

public interface SysLocationService {

    public SysLocationEntity findByCode(Long code);

    public SysLocationEntity findByNameAndType(String name, String type);

    public List<SysLocationEntity> selectByPid(Long pid);


    public Map<Long, SysLocationEntity> getMapById(List<Long> ids);


    public Map<Long, SysLocationEntity> getMapByCode(List<Long> codes);

    public SysLocationEntity findById(Long id);
}
