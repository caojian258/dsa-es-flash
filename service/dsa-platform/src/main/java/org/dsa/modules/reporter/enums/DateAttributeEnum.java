package org.dsa.modules.reporter.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public enum DateAttributeEnum {

    STRING("string"),
    TIMESTAMP("timestamp"),
    UNIX_TIMESTAMP("unix_timestamp"),
    LONG_UNIX_TIMESTAMP("long_unix_timestamp"),
    ;
    @EnumValue
    private final String value;
}
