package org.dsa.modules.reporter.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EsIndexEnum {
    CLIENT("client"),
    SESSION("dsa_session"),
    DIAG("dsa_diagnostic"),
    DTC("dsa_dtc"),
    DID("dsa_did"),
    DTC_EXTENDED("dsa_dtc_extended"),
    DTC_SNAPSHOT("dsa_dtc_snapshot"),
    VEHICLE_REPORT("dsa_vehicle_report"),
    ECU("dsa_diagnostic_ecu"),
    AS_MAINTAIN("dsa_vehicle_asmaintain"),
    ;

    @EnumValue
    private final String value;
}
