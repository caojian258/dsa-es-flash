package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class ProtocolItemDto implements Serializable {

    /***
     * 类型
     */
    private String type;

    /***
     * 严重程度
     */
    private String severity;

    /***
     * 当前用户名
     */
    private String username;

    /***
     * 诊断仪ID
     */
    private String pcid;

    /***
     * 消息
     */
    private String message;

    /***
     * 时间戳
     */
    private Date timestamp;

}
