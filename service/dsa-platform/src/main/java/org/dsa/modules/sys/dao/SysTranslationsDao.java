package org.dsa.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.sys.dto.SysTranslationsDto;
import org.dsa.modules.sys.entity.SysTranslations;

import java.util.List;

@Mapper
public interface SysTranslationsDao extends BaseMapper<SysTranslations> {

    public void truncateTable();

    public void insertData(@Param("translation") SysTranslations stl);

    public List<SysTranslationsDto> selectAllTi(String locale);

    public Integer countTi(String locale);

    public Page<SysTranslationsDto> selectTiPage(Page<SysTranslationsDto> page, Integer locale);

    public List<SysTranslationsDto> queryTranslationsByLocale(@Param("locale") Integer locale, @Param("tis") List<String> tis);

    public String queryTranslationByLocale(@Param("locale") Integer locale, @Param("ti") String ti);
}
