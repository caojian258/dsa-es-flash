package org.dsa.modules.reporter.controller;

import org.dsa.common.utils.R;
import org.dsa.modules.reporter.event.FlashPublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/report")
public class IndexController {

    @Autowired
    FlashPublisher publisher;

    @RequestMapping(value = "/parseMetadata", method = {RequestMethod.GET,RequestMethod.POST})
    @ResponseBody
    public R test(){

        return R.ok();
    }
}
