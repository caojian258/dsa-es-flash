package org.dsa.modules.diagnostic.dto;

import lombok.Getter;
import lombok.Setter;

import jakarta.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author weishunxin
 * @since 2023-06-01
 */
@Setter
@Getter
public class PackageCompareReqDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotBlank(message = "vin can not be blank.")
    private String vin;

    private String pdxVersion;

    private String ptxVersion;

    private String diagPackageVersion;

    private boolean increment;

    //private String locale;

}
