package org.dsa.modules.onlineUpdate.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.dsa.modules.onlineUpdate.entity.SwVCompVEntity;

/**
 * 新增软件版本依赖
 */
@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class SwVCompVDto extends SwVCompVEntity{

    private Long componentId;
    private Long componentVersionId;
    private SwVCompVEntity dependency;

}
