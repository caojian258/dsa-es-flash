package org.dsa.modules.onlineUpdate.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 软件
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SoftwareVersionPo {
        // softwareInfo
        private Long code;
        private Long softwareId;
        private String softwareName;
        private String softwareDesc;
        private Long softwareVersionNumber;
        private String softwareVersionName;
        private Integer updateType;
        private String updateId;
        private String releaseNotes;
        /**
         * 过期时间
         */
        private String expireDate;
        /**
         * 过期提醒时间
         */
        private Integer expireWarningDays;
        private List<ComponentInfo> componentVersionInfo;


    /**
     * 软件依赖
     */
    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ComponentInfo{
        private Long componentId;
        private String componentName;
        private Long componentVersionNumber;
        private String componentVersionName;
        private Integer updateType;
        private String url;
        private Long fileSize;
        private String fileMd5;
        private String releaseNotes;
        private Integer isEntry;
        /**
         * 版本排序号
         */
        private Integer orderNumber;
        /**
         * 被依赖的组件list
         */
        private List<ComponentVersionDependency> dependencies;
    }

    /**
     * 组件依赖
     */
    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ComponentVersionDependency{
        /**
         * 组件id
         */
        private Long componentId;
        /**
         * 组件名称
         */
        private String componentName;
        /**
         * 组件版本id
         */
        private Long componentVersionNumber;
        /**
         * 组件版本名称
         */
        private String componentVersionName;
    }



}
