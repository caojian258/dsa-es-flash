package org.dsa.modules.sys.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.DateUtils;
import org.dsa.common.utils.PageUtils;
import org.dsa.common.utils.Query;
import org.dsa.modules.sys.dao.SysUserDao;
import org.dsa.modules.sys.entity.LdapUserEntity;
import org.dsa.modules.sys.entity.SysUserEntity;
import org.dsa.modules.sys.entity.SysWorkshopEntity;
import org.dsa.modules.sys.service.*;
import org.dsa.modules.sys.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * 系统用户
 *
 */
@Slf4j
@Service("sysUserService")
public class SysUserServiceImpl extends ServiceImpl<SysUserDao, SysUserEntity> implements SysUserService {
	@Autowired
	private SysUserRoleService sysUserRoleService;
	@Autowired
	private SysRoleService sysRoleService;
    @Autowired
    private SysUserGroupService sysUserGroupService;
	@Autowired
	private SysWorkshopService sysWorkshopService;
	@Autowired
	private LdapService ldapService;


	@Override
	public SysUserEntity getUserByUserId(Long userId) {
		return this.baseMapper.selectById(userId);
	}

	@Override
	public PageUtils queryPage(Map<String, Object> params) {
		String username = (String)params.get("username");
		Long createUserId = (Long)params.get("createUserId");
		String workShopName = (String)params.get("workShopName");
		IPage<SysUserEntity> page = new Query<SysUserEntity>().getPage(params);

		IPage<SysUserEntity> pageObj=this.baseMapper.selectUserListByNameAndCreateId(page,username,createUserId,workShopName);
		return new PageUtils(pageObj);
	}

	@Override
	public List <SysUserEntity>  queryUserList(Map<String, Object> params) {
		String username = (String)params.get("username");
		String workshop = (String)params.get("workshop");
		List<Integer> userIds = (List<Integer>)params.get("userIds");
		if (userIds == null) {
			userIds = new ArrayList<>();
		}
		List<SysUserEntity> sysUserEntities = this.baseMapper.selectRuleUserList(workshop, username, userIds);
		sysUserEntities.forEach(user->{
			user.setWorkshopName(Objects.isNull(user.getWorkshopName())?"": user.getWorkshopName());
		});
		return sysUserEntities;
	}

	@Override
	public List<SysUserEntity> querySelectedPage(Map<String, Object> params) {
		String username = (String)params.get("username");
		String workshop = (String)params.get("workshop");
		List<Integer> userIds = (List<Integer>)params.get("userIds");
		if (userIds == null) {
			userIds = new ArrayList<>();
		}

		return this.baseMapper.selectList(new QueryWrapper<SysUserEntity>()
				.select("user_id","username","chinese_name","english_name","workshop_name","email","mobile")
				.like(StringUtils.isNotBlank(username),"username", username)
				.like(StringUtils.isNotBlank(username),"chinese_name", username)
				.like(StringUtils.isNotBlank(username),"english_name", username)
				.like(StringUtils.isNotBlank(workshop),"workshop_name", workshop)
				.in(userIds.size()>0,"user_id", userIds)
		);
	}

	@Override
	public List<String> queryAllPerms(Long userId) {
		return baseMapper.queryAllPerms(userId);
	}

	@Override
	public List<Long> queryAllMenuId(Long userId) {
		return baseMapper.queryAllMenuId(userId);
	}

	@Override
	public SysUserEntity queryByUserName(String username) {
		SysUserEntity sysUserEntity= baseMapper.queryByUserName(username);
		if(sysUserEntity == null ) {
			return null;
		}

		 //获取用户所属组列表
        List<Long> groupIdList=sysUserGroupService.queryGroupIdList(sysUserEntity.getUserId());
        sysUserEntity.setGroupIdList(groupIdList);
		return sysUserEntity;

	}

	@Override
	@Transactional
	public void saveUser(SysUserEntity user) {
		user.setCreateTime(new Date());

		validatorUser(user);
		//sha256加密
		String salt = RandomStringUtils.randomAlphanumeric(20);
		user.setPassword(new Sha256Hash(user.getPassword(), salt).toHex());
		user.setSalt(salt);
		this.save(user);
		
		//检查角色是否越权
//		checkRole(user);
		
		//保存用户与角色关系
		sysUserRoleService.saveOrUpdate(user.getUserId(), user.getRoleIdList());
        //保存用户与组关系
//        sysUserGroupService.saveOrUpdate(user.getUserId(), user.getGroupIdList());

//		saveUserToLdap(user);

	}

	private void validatorUser(SysUserEntity user) {
		//用户名校验
		LambdaQueryWrapper<SysUserEntity> query = new LambdaQueryWrapper<SysUserEntity>()
				.eq(SysUserEntity::getUsername, user.getUsername())
				.ne(ObjectUtil.isNotNull(user.getUserId()),SysUserEntity::getUserId,user.getUserId());
		SysUserEntity user1 = this.baseMapper.selectOne(query);
		if (user1 != null) {
			//用户名已存在
			throw new RRException(Constant.Msg.USERNAME_EXIST_ERROR);
		}
		if (StringUtils.isNotBlank(user.getPassword().trim())){
			// 密码强度校验  长度大于16 大写字母、小写字母、数字、特殊字符
			String expr = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*?]).{16,}$";
			Pattern p = Pattern.compile(expr);
			Matcher m = p.matcher(user.getPassword());
			if (!m.matches()){
				throw new RRException(Constant.Msg.PASSWORD_FORMAT_ERROR);
			}
		}

	}

	@Override
	@Transactional
	public void update(SysUserEntity user) {

		validatorUser(user);
		if(StringUtils.isBlank(user.getPassword())){
			user.setPassword(null);
		}else{
			user.setPassword(new Sha256Hash(user.getPassword(), user.getSalt()).toHex());
		}
		this.updateById(user);
		
		//检查角色是否越权
//		checkRole(user);
		
		//保存用户与角色关系
		sysUserRoleService.saveOrUpdate(user.getUserId(), user.getRoleIdList());
        //保存用户与组关系
        sysUserGroupService.saveOrUpdate(user.getUserId(), user.getGroupIdList());

//		updateLdapUser(user);
	}

	@Override
	public void deleteBatch(Long[] userId) {
		if(userId!=null && userId.length > 0) {
			this.baseMapper.disableByIds(Arrays.asList(userId));
//			disableLdapByIds(Arrays.asList(userId));
		}
	}

	@Override
	public boolean updatePassword(Long userId, String password, String newPassword) {
		SysUserEntity userEntity = new SysUserEntity();
		userEntity.setPassword(newPassword);
		return this.update(userEntity,
				new QueryWrapper<SysUserEntity>().eq("user_id", userId).eq("password", password));
	}
	
	/**
	 * 检查角色是否越权
	 */
	private void checkRole(SysUserEntity user){
		if(user.getRoleIdList() == null || user.getRoleIdList().size() == 0){
			return;
		}
		//如果不是超级管理员，则需要判断用户的角色是否自己创建
		if(ShiroUtils.getUserName().equals(Constant.SUPER_ADMIN_USER_NAME)){
			return ;
		}
		// 如果角色是SuperAdmin 则可以任意修改
		if (ShiroUtils.getUserEntity().getRoles().contains(Constant.ADMIN_ROLE)) {
			return;
		}
		
		//查询用户创建的角色列表
		List<Long> roleIdList = sysRoleService.queryRoleIdList(user.getCreateUserId());
		//判断是否越权
		if(!roleIdList.containsAll(user.getRoleIdList())){
			throw new RRException(Constant.Msg.CHECK_SYS_ROLE);
		}
	}

	/**
	 * 取用户列表
	 * @return
	 */
	@Override
	public List<SysUserEntity> getUserList(String chineseName){
		//排除部门负责人和经理
		List<String> roleList=new ArrayList<>();
		return this.baseMapper.getUserList(chineseName,roleList, Constant.WorkshopName.IT.getDbName());
	}

    @Override
    public List<String> queryUserByRole(int roleId ,Long workshopId,Long groupId) {
		return this.baseMapper.queryUserByRole(roleId ,workshopId,groupId);
    }

	@Override
	public SysUserEntity queryByUserId(String userId) {
		SysUserEntity sysUserEntity=this.baseMapper.queryByUserId(Long.valueOf(userId));
		if(null == sysUserEntity){
			return null;
		}
		 //获取用户所属组列表
        List<Long> groupIdList=sysUserGroupService.queryGroupIdList(sysUserEntity.getUserId());
        sysUserEntity.setGroupIdList(groupIdList);
		return sysUserEntity;
	}

	@Override
	public SysUserEntity getUserGroupInfo(Long userId) {
		return this.baseMapper.getUserGroupInfo(userId);
	}

	@Override
	public void removeNotInIds(List<String> userIdList, String workshopId) {
		this.baseMapper.removeNotInIds(userIdList ,workshopId);
	}

	@Override
	public void saveUserInfo(SysUserEntity sysUserEntity) {
		this.baseMapper.saveUserInfo(sysUserEntity);
	}

	@Override
	public List<String> queryUserNameByRole(int roleId, Long workshopId, Long groupId) {
		return this.baseMapper.queryUserNameByRole(roleId ,workshopId,groupId);
	}

	@Override
	public List<SysUserEntity> queryUserEntityByRole(int roleId, Long workshopId, Long groupId) {
		SysWorkshopEntity d = sysWorkshopService.getSysWorkshopEntity(workshopId);

		return this.baseMapper.queryUserEntityByRole(roleId ,workshopId,d.getParentId(),groupId);
	}

	@Override
	public List<Long> queryUserIdByWorkshopId(long workshopId) {
		return this.baseMapper.queryUserIdByWorkshopId(workshopId);
	}

	@Override
	public void enableBatch(Long[] userIds) {
		if(userIds!=null && userIds.length > 0) {
			this.baseMapper.enableByIds(Arrays.asList(userIds));
		}
	}

	@Override
	public List<Map<String, Object>> workshopList(String name) {
		return this.baseMapper.workshopList(name);
	}


	/**
	 * 同步ldap 创建 用户
	 */
	private void saveUserToLdap(SysUserEntity sysUserEntity) {
		LdapUserEntity ldapUserEntity = new LdapUserEntity();
		ldapUserEntity.setUid(sysUserEntity.getUsername());
		ldapUserEntity.setCn(sysUserEntity.getUsername());
		ldapUserEntity.setSn(sysUserEntity.getUsername());
		ldapUserEntity.setStatus(sysUserEntity.getStatus()==1?"Active":"Disable");
		ldapUserEntity.setTelephone(sysUserEntity.getMobile());
		ldapUserEntity.setAccountCreateDate(DateUtils.format(new Date(),DateUtils.DATE_PATTERN));
		ldapUserEntity.setOu(String.valueOf(sysUserEntity.getWorkshopId()));
		ldapService.insertLdapUser(ldapUserEntity);
	}
	/**
	 * 同步ldap 修改 用户
	 */
	private void updateLdapUser(SysUserEntity sysUserEntity) {
		LdapUserEntity ldapUserEntity = new LdapUserEntity();
		ldapUserEntity.setUid(sysUserEntity.getUsername());
		ldapUserEntity.setCn(sysUserEntity.getUsername());
		ldapUserEntity.setSn(sysUserEntity.getUsername());
		ldapUserEntity.setStatus(sysUserEntity.getStatus()==1?"Active":"Disable");
		ldapUserEntity.setTelephone(sysUserEntity.getMobile());
		ldapUserEntity.setAccountCreateDate(DateUtils.format(new Date(),DateUtils.DATE_PATTERN));
		ldapUserEntity.setOu(String.valueOf(sysUserEntity.getWorkshopId()));
		ldapService.updateLdapUser(ldapUserEntity);
	}
	/**
	 * 同步ldap 禁用 用户
	 */
	private void disableLdapByIds(List<Long> userIds){
		for(Long userId : userIds){
			SysUserEntity sysUserEntity=this.baseMapper.queryByUserId(userId);
			LdapUserEntity ldapUserEntity = new LdapUserEntity();
			ldapUserEntity.setUid(sysUserEntity.getUsername());
			ldapUserEntity.setStatus("Disable");
			ldapService.disableLdapUser(ldapUserEntity);
		}
	}
}