package org.dsa.modules.reporter.upgrade;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.dsa.modules.reporter.entity.AppHistory;
import org.dsa.modules.reporter.entity.ClientUpgrade;
import org.springframework.stereotype.Service;

/***
 * 目前下载日志不错处理
 */
@Slf4j
@Service
public class DownloadUpgradeStrategy implements UpgradeStrategy{

    @Override
    public void save(ClientUpgrade result) {
        log.info("Download save: {}", JSONObject.toJSONString(result));
    }

    @Override
    public void saveHis(AppHistory data) {

    }
}
