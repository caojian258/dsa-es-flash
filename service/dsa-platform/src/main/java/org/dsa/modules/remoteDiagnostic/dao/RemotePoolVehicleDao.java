package org.dsa.modules.remoteDiagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.remoteDiagnostic.entity.RemotePoolDependEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemotePoolVehicleEntity;

import java.util.List;

@Mapper
public interface RemotePoolVehicleDao extends BaseMapper<RemotePoolVehicleEntity> {
    void inserts(@Param("poolId") Long id, @Param("ids") List<Long> ids, @Param("group") String group);

    List<Long> selectListByGroup(@Param("poolId") Long id, @Param("group") String group);

    List<Long> selectPoolByGroup(@Param("group") String group);


}
