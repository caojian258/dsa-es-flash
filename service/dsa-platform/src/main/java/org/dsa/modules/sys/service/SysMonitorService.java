package org.dsa.modules.sys.service;

import org.dsa.modules.sys.vo.monitor.MonitorVo;

import java.util.List;
import java.util.Map;

public interface SysMonitorService {

    List<Map<String, String>> getCountTable(MonitorVo monitorVo);

}
