package org.dsa.modules.reporter.vo.workshopUser;

import lombok.Data;

import java.io.Serializable;

@Data
public class WorkshopsReVo implements Serializable {

    private Long id;

    private String name;
}
