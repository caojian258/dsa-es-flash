package org.dsa.modules.sys.vo.workshop;

import lombok.Data;

import jakarta.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class WorkshopNameValidatorVo implements Serializable {

    @NotNull(message = "Workshop ID Not NUll")
    private Long workshopId;

    @NotNull(message = "Workshop Name Not NUll")
    private String name;
}
