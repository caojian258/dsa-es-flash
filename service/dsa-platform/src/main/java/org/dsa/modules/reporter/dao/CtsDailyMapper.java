package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.dto.CtsDailyDto;
import org.dsa.modules.reporter.entity.CtsDaily;

import java.util.List;

@Mapper
public interface CtsDailyMapper extends BaseMapper<CtsDaily> {

    Page<CtsDailyDto> selectPageByDate(Page<CtsDaily> page,
                                       @Param("ctsName") String ctsName,
                                       @Param("vin") String vin,
                                       @Param("sources") List<String> sources,
                                       @Param("vehicleTypeIds") List<Long> vehicleTypeIds,
                                       @Param("startTime") String startTime,
                                       @Param("endTime") String endTime);

    public void oneInsert(@Param("entity") CtsDaily entity);

    public void oneUpsert(@Param("entity") CtsDaily entity);

    public void deleteData(@Param("vin") String vin, @Param("day") String day);
}
