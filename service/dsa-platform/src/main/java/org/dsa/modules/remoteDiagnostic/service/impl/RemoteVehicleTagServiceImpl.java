package org.dsa.modules.remoteDiagnostic.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.utils.RedisUtils;
import org.dsa.config.SyncConfig;
import org.dsa.modules.remoteDiagnostic.config.RemoteDiagnosticConfig;
import org.dsa.modules.remoteDiagnostic.constant.RemoteDiagnosticConstant;
import org.dsa.modules.remoteDiagnostic.dao.RVehicleTagDao;
import org.dsa.modules.remoteDiagnostic.dao.RemotePoolDependDao;
import org.dsa.modules.remoteDiagnostic.dao.RemoteVehicleTagDao;
import org.dsa.modules.remoteDiagnostic.entity.RVehicleTagEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemotePoolDependEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleTagEntity;
import org.dsa.modules.remoteDiagnostic.listener.TagExcelImportListener;
import org.dsa.modules.remoteDiagnostic.service.RemoteVehicleTagService;
import org.dsa.modules.remoteDiagnostic.utils.AutoExcelWidthStyleStrategy;
import org.dsa.modules.remoteDiagnostic.vo.CheckNameReqVo;
import org.dsa.modules.remoteDiagnostic.vo.VehiclePlatformPageReVo;
import org.dsa.modules.remoteDiagnostic.vo.VehicleTag.VehicleTagReqVo;
import org.dsa.modules.vehicle.dao.DVehicleInfoDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SessionCallback;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import jakarta.annotation.Resource;
import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class RemoteVehicleTagServiceImpl implements RemoteVehicleTagService {

    @Autowired
    RemoteVehicleTagDao tagDao;
    @Autowired
    RVehicleTagDao vehicleTagDao;
    @Autowired
    RemotePoolDependDao dependDao;
    @Autowired
    SyncConfig syncConfig;
    @Autowired
    RemoteDiagnosticConfig remoteDiagnosticConfig;
    @Resource
    private DVehicleInfoDao dVehicleInfoDao;
    //    @Autowired
//    RedisUtils redisUtils;
    @Autowired
    RedisTemplate<String, Object> redisTemplate;

    @Override
    public Page<RemoteVehicleTagEntity> selectPage(VehiclePlatformPageReVo vo) {
        Page<RemoteVehicleTagEntity> page = new Page<>(vo.getPageNo(), vo.getPageSize());

        String column = StrUtil.toUnderlineCase(vo.getSortColumn());
        OrderItem oi = new OrderItem(column, vo.getSortAsc());
        page.addOrder(oi);
        LambdaQueryWrapper<RemoteVehicleTagEntity> qw = Wrappers.<RemoteVehicleTagEntity>lambdaQuery();

        if (!StringUtils.isEmpty(vo.getName())) {
//            qw.like(VehicleEcuEntity::getEcuName, vo.getName().trim());
            qw.apply("tag_name" + " ilike {0}", "%" + vo.getName().trim() + "%");
        }

        Page<RemoteVehicleTagEntity> resultPage = tagDao.selectPage(page, qw);

        return resultPage;
    }

    @Override
    public RemoteVehicleTagEntity getInfo(Long id) {
        return tagDao.selectById(id);
    }

    @Override
    public void saveTag(RemoteVehicleTagEntity tag) {
        tagDao.insert(tag);
    }

    @Override
    public void updateTag(RemoteVehicleTagEntity tag) {
        tag.setUpdatedAt(new Date());
        tagDao.updateById(tag);
    }

    @Override
    @Transactional
    public void deleteTag(Long id) {

        // 找到key
        List<String> poolKeys = dependDao.selectList(Wrappers.<RemotePoolDependEntity>lambdaQuery().eq(RemotePoolDependEntity::getTagId, id)).stream().map(e -> {
            return RemoteDiagnosticConstant.VEHICLE_POOL_ITEM_KEY + e.getPoolId();
        }).collect(Collectors.toList());

        // 删除中间表
        vehicleTagDao.delete(Wrappers.<RVehicleTagEntity>lambdaUpdate().eq(RVehicleTagEntity::getTagId, id));
        dependDao.delete(Wrappers.<RemotePoolDependEntity>lambdaUpdate().eq(RemotePoolDependEntity::getTagId, id));
        // 删除主表
        tagDao.deleteById(id);

        // 删除缓存
        if (poolKeys.size() != 0) {
            redisTemplate.execute(new SessionCallback<Object>() {
                @Override
                public <K, V> Object execute(RedisOperations<K, V> operations) throws DataAccessException {
                    operations.multi(); // 开启事务
                    poolKeys.forEach(e -> redisTemplate.opsForHash().delete(RemoteDiagnosticConstant.VEHICLE_POOL_KEY, e));
                    return operations.exec();  //提交
                }
            });
            redisTemplate.opsForValue().set((RemoteDiagnosticConstant.VEHICLE_POOL_LOCK), System.currentTimeMillis()+"");
        }

    }

    @Override
    public List<RemoteVehicleTagEntity> getList(Boolean flag) {
        if (flag != null && flag) {
            return tagDao.getList();
        } else {
            return tagDao.selectList(Wrappers.<RemoteVehicleTagEntity>lambdaQuery());
        }
    }

    @Override
    public void updateVehicleTag(VehicleTagReqVo vo) {

        if (!StringUtils.isEmpty(vo.getVin())) {
            vehicleTagDao.delete(Wrappers.<RVehicleTagEntity>lambdaUpdate().eq(RVehicleTagEntity::getVin, vo.getVin()));
        }
//        else {
//            qw.in(RVehicleTagEntity::getVin, vo.getVinList());
//        }
        if (vo.getTags() != null && vo.getTags().size() != 0) {
            if (!StringUtils.isEmpty(vo.getVin())) {
                vehicleTagDao.inserts(vo.getVin(), vo.getTags());
            } else {
                vehicleTagDao.batchInserts(vo.getVinList(), vo.getTags());
//                for (Long id : vo.getIds()) {
//                    vehicleTagDao.inserts(id, vo.getTags());
//                }
            }
        }
    }

    @Override
    public Boolean check(CheckNameReqVo vo) {
        LambdaQueryWrapper<RemoteVehicleTagEntity> qw = Wrappers.<RemoteVehicleTagEntity>lambdaQuery();
        qw.eq(RemoteVehicleTagEntity::getTagName, vo.getName());
        if (vo.getId() != null) {
            qw.ne(RemoteVehicleTagEntity::getId, vo.getId());
        }
        qw.select(RemoteVehicleTagEntity::getId);
        return tagDao.selectOne(qw) == null;
    }

    @Override
    public List<Long> getVehicleTagInfo(String vin) {

        return vehicleTagDao.selectIdsByVin(vin);
    }

    @Override
    public Map<String, String> templateExcelExport() {
        HashMap<String, String> map = new HashMap<>();
        List<List<String>> languages = new ArrayList<>();
        languages.add(Collections.singletonList("vin"));
        languages.add(Collections.singletonList("tag"));
        List<List<String>> data = new ArrayList<>();


        //导出到指定路径
        File languageExcelFolder = new File(remoteDiagnosticConfig.getTag());

        if (!languageExcelFolder.exists()) {
            FileUtil.mkdir(languageExcelFolder);
        }
        String fileName = languageExcelFolder + File.separator + "tagTemplate.xlsx";
        File file = new File(fileName);
//        if (file.exists()) {
//            map.put("url", fileName);
//            map.put("name", file.getName());
//            return map;
//        }
//        LambdaQueryWrapper<DVehicleInfoEntity> qw = Wrappers.<DVehicleInfoEntity>lambdaQuery();
//        qw.last(" LIMIT 10 ");
//        List<DVehicleInfoEntity> dVehicleInfoEntities = dVehicleInfoDao.selectList(qw);
        ArrayList<String> objects = new ArrayList<>();
        objects.add("LGAG4DY37N8005694");
        objects.add("PT");
        data.add(objects);

//        for (DVehicleInfoEntity dVehicleInfoEntity : dVehicleInfoEntities) {
//            objects = new ArrayList<>();
//            objects.add(dVehicleInfoEntity.getVin());
//            objects.add("template");
//            data.add(objects);
//        }

        // 这里 需要指定写用哪个class去写，然后写到第一个sheet，名字为模板 然后文件流会自动关闭
        EasyExcel.write(fileName)
                .registerWriteHandler(new AutoExcelWidthStyleStrategy())
                .head(languages).sheet("tag info").doWrite(data);

        map.put("url", fileName);
        map.put("name", file.getName());
        return map;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void readExcel(File file) {
        TimeInterval timer = DateUtil.timer();
        EasyExcel.read(file, new TagExcelImportListener(remoteDiagnosticConfig.getTag())).sheet().doRead();
        if (file.exists()) {
            boolean delete = file.delete();
        }
    }

    @Override
    public List<RemoteVehicleTagEntity> getVehicleTagList(String vin) {
        return vehicleTagDao.getVehicleTagList(vin);
    }
}