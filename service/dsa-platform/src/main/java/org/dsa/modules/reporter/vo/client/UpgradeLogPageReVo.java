package org.dsa.modules.reporter.vo.client;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.modules.reporter.pojo.PageParam;

import jakarta.validation.constraints.NotNull;


@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class UpgradeLogPageReVo extends PageParam{

    @NotNull(message = "客户端不能为空")
    private String pcid;

    private String message;
}
