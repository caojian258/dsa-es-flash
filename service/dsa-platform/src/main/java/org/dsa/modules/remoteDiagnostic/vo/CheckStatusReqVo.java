package org.dsa.modules.remoteDiagnostic.vo;

import lombok.Data;
import lombok.ToString;
import org.dsa.common.validator.group.AddGroup;
import org.dsa.common.validator.group.UpdateGroup;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

@Data
@ToString(callSuper = true)
public class CheckStatusReqVo {

    @NotBlank(message = "类型不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String type;

    @NotNull(message = "状态不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private Long id;
}
