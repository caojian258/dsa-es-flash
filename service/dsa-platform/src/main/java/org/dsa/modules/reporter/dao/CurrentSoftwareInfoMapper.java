package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.entity.CurrentSoftwareInfo;

import java.util.List;

@Mapper
public interface CurrentSoftwareInfoMapper extends BaseMapper<CurrentSoftwareInfo> {

    public CurrentSoftwareInfo querySoftwareInfo(@Param("id") Integer id, @Param("versionNumber") Integer versionNumber, @Param("tag") String tag);

    public List<CurrentSoftwareInfo> querySoftwareInfoByTag(@Param("deviceId") String deviceId, @Param("tag") String tag, @Param("idList") List<Integer> idList);

}
