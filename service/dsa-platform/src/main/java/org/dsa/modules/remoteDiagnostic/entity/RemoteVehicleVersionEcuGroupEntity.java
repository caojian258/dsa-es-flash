package org.dsa.modules.remoteDiagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
@TableName("r_vehicle_version_ecu_group")
public class RemoteVehicleVersionEcuGroupEntity implements Serializable {
    //ecu group vehicle version 关联
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long versionId;

    private Long groupId;

    private Long ecuId;

    private Long ecuVersionId;

    @TableField(exist = false)
    private RemoteVehicleEcuGroupEntity ecuGroup;
}
