package org.dsa.modules.onlineUpdate.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.dsa.modules.onlineUpdate.entity.CompVGroupEntity;

@EqualsAndHashCode(callSuper = true)
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CompVGroupVo extends CompVGroupEntity {

    /**
     * 关联的组件名称
     */
    private String componentName;
    /**
     * 关联的组件版本
     */
    private String componentVersionName;
    /**
     * 是否禁用
     */
    private boolean disabled;
    /**
     * code
     */
    private Integer code;
}
