CREATE TABLE `t_request_attach` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `request_number` varchar(30) DEFAULT NULL COMMENT '请求号',
  `type` varchar(50) DEFAULT NULL COMMENT '类型',
  `attach_name` varchar(100) DEFAULT NULL COMMENT '附件名',
  `attach_url` varchar(200) DEFAULT NULL COMMENT '附件路径',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `created_by` varchar(50) DEFAULT NULL COMMENT '创建人',
  `modify_by` varchar(50) DEFAULT NULL COMMENT '修改人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `is_delete` varchar(6) DEFAULT '0' COMMENT '是否删除(0:正常,1：删除)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='请求附件表';

CREATE TABLE `t_change_attach` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `request_number` varchar(30) DEFAULT NULL COMMENT '请求号',
  `change_number` varchar(30) DEFAULT NULL COMMENT '变更号',
  `type` varchar(50) DEFAULT NULL COMMENT '类型',
  `attach_name` varchar(100) DEFAULT NULL COMMENT '附件名',
  `attach_url` varchar(200) DEFAULT NULL COMMENT '附件路径',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `created_by` varchar(50) DEFAULT NULL COMMENT '创建人',
  `modify_by` varchar(50) DEFAULT NULL COMMENT '修改人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `is_delete` varchar(6) DEFAULT '0' COMMENT '是否删除(0:正常,1：删除)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='请求附件表';



