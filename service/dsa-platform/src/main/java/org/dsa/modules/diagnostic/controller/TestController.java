package org.dsa.modules.diagnostic.controller;

import com.alibaba.fastjson.JSONObject;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.R;
import org.dsa.modules.diagnostic.entity.mqtt.send.MqRequestBodyEntity;
import org.dsa.modules.diagnostic.util.DiagnosticUtils;
import org.dsa.modules.sys.controller.AbstractController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.annotation.Resource;

/**
 * 诊断功能
 *
 */
@RestController
@RequestMapping("test")
public class TestController extends AbstractController {

	@Resource
	private DiagnosticUtils diagnosticUtils;
	/**
	 * TSP向远程诊断仪同步车辆信息
	 */
	@RequestMapping("/sendMqttMessage")
	public R sendMqttMessage(@RequestBody String vin){
		JSONObject bodyData =new JSONObject();
		bodyData.put("mode",-1);
		bodyData.put("maker",-1);
		bodyData.put("userId","");
		bodyData.put("username","");
		bodyData.put("token","");

		MqRequestBodyEntity mqRequestBodyEntity = new MqRequestBodyEntity();
		mqRequestBodyEntity.setData(bodyData);
		diagnosticUtils.sendMqMessage(vin,"aaaatest", Constant.Function.BEGIN,Constant.Action.BEGIN, mqRequestBodyEntity);

		return R.ok();
	}


}
