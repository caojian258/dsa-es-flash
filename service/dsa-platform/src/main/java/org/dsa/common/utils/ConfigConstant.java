package org.dsa.common.utils;


/**
 * 系统参数相关Key
 *
 */
public class ConfigConstant {
    /**
     * 云存储配置KEY
     */
    public final static String CLOUD_STORAGE_CONFIG_KEY = "CLOUD_STORAGE_CONFIG_KEY";

    /**
     * 证书subject
     */
    public final static  String subject ="license";

    /**
     * 公钥别称
     */
    public final static  String publicAlias="publicCert";

    /**
     * 访问公钥库的密码
     */
    public final static  String storePass="public_dsa_20220101";

    /**
     * 证书生成路径
     */
    public final static  String licensePath="/usr/local/license/license.lic";

    /**
     * 密钥库存储路径
     */
    public final static  String publicKeysStorePath="/usr/local/license/publicCerts.keystore";

}
