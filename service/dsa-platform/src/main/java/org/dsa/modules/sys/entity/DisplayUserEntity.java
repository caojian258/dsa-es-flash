package org.dsa.modules.sys.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DisplayUserEntity {
    private String token;
    private UserInfoEntity userInfo;
    private List<Role> roles;
    private List<SysUserLicenseEntity> licenses;
    private List<String> menuList;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class Role{
        private String roleCode;
        private List<String> function;
    }

}
