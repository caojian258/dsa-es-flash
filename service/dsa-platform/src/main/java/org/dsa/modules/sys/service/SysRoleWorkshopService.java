package org.dsa.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dsa.modules.sys.entity.SysRoleWorkshopEntity;

import java.util.List;


/**
 * 角色与部门对应关系
 *
 */
public interface SysRoleWorkshopService extends IService<SysRoleWorkshopEntity> {
	
	void saveOrUpdate(Long roleId, List<Long> workshopIdList);
	
	/**
	 * 根据角色ID，获取部门ID列表
	 */
	List<Long> queryWorkshopIdList(Long[] roleIds) ;

	/**
	 * 根据角色ID数组，批量删除
	 */
	int deleteBatch(Long[] roleIds);
}
