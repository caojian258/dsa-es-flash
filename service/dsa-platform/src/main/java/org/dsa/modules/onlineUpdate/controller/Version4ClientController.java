package org.dsa.modules.onlineUpdate.controller;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.dsa.common.annotation.SysLog;
import org.dsa.common.exception.RRException;
import org.dsa.common.pojo.CodeMessage;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.EnvUtils;
import org.dsa.common.utils.HttpUtils;
import org.dsa.common.utils.R;
import org.dsa.modules.onlineUpdate.entity.ComponentVersionEntity;
import org.dsa.modules.onlineUpdate.entity.RuleEntity;
import org.dsa.modules.onlineUpdate.entity.SoftwareEntity;
import org.dsa.modules.onlineUpdate.entity.SoftwareVersionEntity;
import org.dsa.modules.onlineUpdate.po.SoftwareVersionPo;
import org.dsa.modules.onlineUpdate.rule.RuleComponent;
import org.dsa.modules.onlineUpdate.service.ComponentVersionService;
import org.dsa.modules.onlineUpdate.service.SoftwareService;
import org.dsa.modules.onlineUpdate.service.SoftwareVersionService;
import org.dsa.modules.reporter.service.FileLogService;
import org.dsa.modules.sys.service.SysFileService;
import org.dsa.modules.sys.service.SysLocaleService;
import jakarta.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import jakarta.annotation.Resource;
import jakarta.mail.internet.MimeUtility;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * 客户端调用
 */
@Slf4j
@RestController
@RequestMapping("/version")
public class Version4ClientController {

    @Autowired
    private SoftwareService softwareService;
    @Autowired
    private SoftwareVersionService softwareVersionService;
    @Autowired
    private SysFileService sysFileService;
    @Autowired
    private SysLocaleService sysLocaleService;

    @Autowired
    private RuleComponent ruleComponent;

    @Resource
    FileLogService logService;


    @Autowired
    private EnvUtils envUtils;

    @Autowired
    private ComponentVersionService componentVersionService;

    /**
     * 断点续传
     * @param componentVersionNumber   组件版本号
     */
    @RequestMapping("/downloadComponentFile")
    public void breakpointResume(Integer componentId, Integer componentVersionNumber , HttpServletRequest request, HttpServletResponse response) {
        try {
            if (null == componentVersionNumber) {
                throw new RRException(Constant.Msg.VERSION_DOWNLOAD_PARAM_ERROR);
            }
            ComponentVersionEntity info = componentVersionService.getById(componentVersionNumber);
            if (ObjectUtil.isNull(info)) {
                throw new RRException(Constant.Msg.VERSION_DOWNLOAD_PARAM_ERROR);
            }
            String filePath = info.getUrl();

            File file = new File(filePath);
            if (!file.exists()) {
                throw new RRException(Constant.Msg.FILE_NOT_EXIST);
            }

            // 获取文件的长度
            long fileLength = file.length();
            // 下载
            response.setContentType("application/x-download");
            response.setHeader("Accept-Ranges", "bytes");
            response.setHeader("Content-Disposition", "attachment;" + getName(FileUtil.mainName(filePath)+"."+FileUtil.extName(filePath), request));
            long pos = 0;
            if (null != request.getHeader("Range")) {
                // 断点续传
                response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
                try {
                    // 断点
                    pos = Long.parseLong(request.getHeader("Range").replaceAll(
                            "bytes=", "").replaceAll("-", ""));
                } catch (NumberFormatException e) {
                    log.error(request.getHeader("Content-Range") + " is not Number!");
                    pos = 0;
                }
            }
            String contentRange = new StringBuffer("bytes ").append(
                    Long.valueOf(pos)).append("-").append(
                    Long.valueOf(fileLength - 1)).append("/").append(
                    Long.valueOf(fileLength)).toString();
            response.setHeader("Content-Range", contentRange);
            log.info("Content-Range={}", contentRange);
            response.setHeader("Content-Length", String.valueOf(fileLength - pos));

            // 获取输入流
            @Cleanup
            BufferedInputStream bis = new BufferedInputStream(Files.newInputStream(Paths.get(filePath)));
            // 输出流
            @Cleanup
            BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream());
            bis.skip(pos);
            byte[] buff = new byte[4096];
            int bytesRead;
            while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
                bos.write(buff, 0, bytesRead);
            }
        }catch (Exception e){
            log.error("下载文件异常,输入参数=" + componentVersionNumber, e);
        }
    }

    /**
     * @param params
     * @return
     * @ 版本比对
     */
    @RequestMapping("/compareSoftwareVersion")
    public R versionsCompare(@RequestBody Map<String, Object> params,@RequestHeader String token) {
        log.info("version compare {}", params);
        try {
            List<SoftwareVersionPo> list = new ArrayList<>(16);
            String locale = MapUtil.getStr(params, "locale");
            // 软件入参list
            List<Map<String, Object>> data = (List<Map<String, Object>>) MapUtil.get(params, "data", Object.class);
            if (ObjectUtil.isNull(locale)) {
                throw new RRException(Constant.Msg.CLIENT_PARAM_ERROR);
            }
            // 每个软件的版本比对
            data.forEach(param -> {
                param.put("token", token);
                list.add(versionCompare(param,locale));
            });
            Map<String,Object> result = new HashMap<>();
            result.put("softwareInfo", list);
            return R.ok().put("data",result);
        } catch (RRException rre) {
            log.error("version compare error {}", rre.getMsg());
            Map<String,Object> result = new HashMap<>();
            result.put("softwareInfo", new ArrayList<SoftwareVersionPo>(){{
                add(buildNullObj(rre.getCode(),0));
            }});
            return R.error(rre.getCode(), rre.getMsg()).put("data", result);
        } catch (Exception e) {
            log.error("version compare error {}", e.getMessage());
            Map<String,Object> result = new HashMap<>();
            result.put("softwareInfo", new ArrayList<SoftwareVersionPo>(){{
                add(buildNullObj(500,0));
            }});
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION).put("data",result);
        }
    }
    private SoftwareVersionPo versionCompare(Map<String, Object> param,String locale) {
        Integer softwareCode = MapUtils.getInteger(param, "softwareId");
        if (softwareCode == null) {
            softwareCode = 0;
        }
        try {
            Integer oldSoftwareVersionId = MapUtils.getInteger(param, "softwareVersionNumber", 0);
            // 校验入参是否完整
            if (ObjectUtil.isNull(softwareCode) || ObjectUtil.isNull(oldSoftwareVersionId) || ObjectUtil.isNull(locale)) {
                throw new RRException(Constant.Msg.CLIENT_PARAM_ERROR);
            }
            // 校验软件
            SoftwareEntity software = softwareService.getOne(new QueryWrapper<SoftwareEntity>().eq("code", softwareCode));
            if (ObjectUtil.isNull(software)) {
                throw new RRException(Constant.Msg.CLIENT_PARAM_ERROR);
            }
            SoftwareVersionEntity softwareVersion = softwareVersionService.getById(oldSoftwareVersionId);
            if (oldSoftwareVersionId==0){
                // 发布的初始版本
                softwareVersion = softwareVersionService.getOldestVersionBySoftwareId(software.getId());
                // 初始版本不存在
                if (Objects.isNull(softwareVersion)) {
                    log.error("compareVersionErr {} 匹配失败，软件初始版本不存在",oldSoftwareVersionId);
                    return buildNullObj(1001,softwareCode);
                }
                // 初始版本
                param.put("softwareVersionNumber",0);
                List<RuleEntity> rules = ruleComponent.match(param);
                if (rules.isEmpty()) {
                    log.error("compareVersionErr 未匹配到有效规则 {}",oldSoftwareVersionId);
                    return buildNullObj(1001,softwareCode);
                }
                //初次安装 并将所有组件版本设置为强制
                return getSoftwareVersionPo(locale, software, 0L, rules);
            }
            // 规则匹配，根据条件（用户，workshop 以及软件和软件版本信息）匹配唯一的符合升级的规则
            List<RuleEntity> rules = ruleComponent.match(param);
            if (!rules.isEmpty()){
                return getSoftwareVersionPo(locale, software, softwareVersion.getId(), rules);
            }
            log.error("compareVersionErr 匹配失败,无可匹配的版本{}",oldSoftwareVersionId);
            return buildNullObj(1001,softwareCode);
        }catch (RRException rrException){
            log.error("compareVersionErr",rrException);
            return buildNullObj(rrException.getCode(),softwareCode);
        }catch (Exception e){
            log.error("compareVersionErr",e);
            return buildNullObj(500,softwareCode);
        }
    }


    /**
     * @param locale 多语言
     * @param software  软件
     * @param softwareVersionId 旧软件版本Id
     * @param match 匹配的规则
     * @return SoftwareVersionPo
     */
    @NotNull
    private SoftwareVersionPo getSoftwareVersionPo(String locale, SoftwareEntity software, Long softwareVersionId, List<RuleEntity> match) {
        SoftwareVersionEntity latestVersion = softwareVersionService.getById(match.get(0).getSoftwareVersionId());
        // 检查缓存，有缓存直接返回匹配结果
        if (envUtils.isPro()){
            // 生产环境缓存
            String jsonStr = ruleComponent.getCacheDataByRuleIdAndOldVersionId(match.get(0).getId(),softwareVersionId);
            if (jsonStr != null) {
                SoftwareVersionPo softwareVersionPo = JSONUtil.toBean(JSONUtil.parseObj(jsonStr), SoftwareVersionPo.class,true);
                softwareVersionPo.setUpdateId("");
                return softwareVersionPo;
            }
        }
        // 拼接客户端所需数据结构
        SoftwareVersionPo softwareVersionPo = dealSoftwareVersionPo(locale, software, latestVersion,softwareVersionId);
        // 缓存匹配结果
        if (envUtils.isPro()){
            ruleComponent.saveMatchRuleRecord(match, softwareVersionId,JSON.toJSONString(softwareVersionPo));
        }
        return softwareVersionPo;
    }

    private SoftwareVersionPo dealSoftwareVersionPo(String locale, SoftwareEntity software, SoftwareVersionEntity latestVersion, Long oldSoftwareVersion) {
        //组件依赖的组件
        List<SoftwareVersionPo.ComponentInfo> componentInfoList = softwareVersionService.getRelatedCompVersion(latestVersion.getId(), locale, oldSoftwareVersion);
        // 软件依赖的组件
        return SoftwareVersionPo.builder()
                                .code(0L)
                                .softwareId(software.getCode())
                                .softwareName(software.getName())
                                .softwareDesc(getLocale(software.getDescriptionTi(), locale))
                                .softwareVersionNumber(latestVersion.getId())
                                .softwareVersionName(latestVersion.getVersionName())
                                .updateType(latestVersion.getUpdateType())
                                .updateId("")
                                .expireDate(DateUtil.format(latestVersion.getLastUpdateTime(), DatePattern.NORM_DATE_PATTERN))
                                .expireWarningDays(latestVersion.getLastUpdateTipDays())
                                .releaseNotes(getLocale(latestVersion.getReleaseNoteTi(), locale))
                                .componentVersionInfo(componentInfoList)
                                .build();
    }
    private String getLocale(String key, String locale) {
        return sysLocaleService.getLocale(key, locale);
    }

    private SoftwareVersionPo buildNullObj(Integer code,Integer softwareCode) {
        List<SoftwareVersionPo.ComponentInfo> componentInfoList = new ArrayList<>(1);
        componentInfoList.add(SoftwareVersionPo.ComponentInfo.builder()
                .componentId(0L)
                .componentName("")
                .componentVersionNumber(0L)
                .componentVersionName("")
                .updateType(0)
                .url("")
                .isEntry(0)
                .fileSize(0L)
                .fileMd5("")
                .orderNumber(0)
                .releaseNotes("")
                .dependencies(new ArrayList<>())
                .build());
        return SoftwareVersionPo.builder()
                        .code(Long.valueOf(code))
                        .softwareId(Long.valueOf(softwareCode))
                        .softwareName("")
                        .softwareDesc("")
                        .softwareVersionNumber(0L)
                        .softwareVersionName("")
                        .updateType(0)
                        .updateId("")
                        .expireDate("")
                        .expireWarningDays(0)
                        .releaseNotes("")
                        .componentVersionInfo(componentInfoList)
                        .build();
    }

    private String getName(String filename, HttpServletRequest request) throws Exception {
        String userAgent = request.getHeader("userAgent");
        String new_filename = URLEncoder.encode(filename, "UTF8");
        // 如果没有UA，则默认使用IE的方式进行编码，因为毕竟IE还是占多数的
        String rtn = "filename=\"" + new_filename + "\"";
        if (userAgent != null) {
            userAgent = userAgent.toLowerCase();
            // IE浏览器，只能采用URLEncoder编码
            if (userAgent.indexOf("msie") != -1) {
                rtn = "filename=\"" + new_filename + "\"";
            }
            // Opera浏览器只能采用filename*
            else if (userAgent.indexOf("opera") != -1) {
                rtn = "filename*=UTF-8''" + new_filename;
            }
            // Safari浏览器，只能采用ISO编码的中文输出
            else if (userAgent.indexOf("safari") != -1) {
                rtn = "filename=\"" + new String(filename.getBytes("UTF-8"), "ISO8859-1") + "\"";
            }
            // Chrome浏览器，只能采用MimeUtility编码或ISO编码的中文输出
            else if (userAgent.indexOf("applewebkit") != -1) {
                new_filename = MimeUtility.encodeText(filename, "UTF8", "B");
                rtn = "filename=\"" + new_filename + "\"";
            }
            // FireFox浏览器，可以使用MimeUtility或filename*或ISO编码的中文输出
            else if (userAgent.indexOf("mozilla") != -1) {
//    	          rtn = "filename*=UTF-8''" + new_filename;
//    	    	   new_filename = MimeUtility.encodeText(filename, "UTF8", "B");
                rtn = "filename*=UTF-8''" + new String(filename.getBytes("UTF-8"), "ISO8859-1") + "\"";
            }
        }
        return rtn;

    }

    // 版本升级

    @RequestMapping("/compareRelease")
    public R compareRelease(@RequestBody Map<String,Object> param){
        try {
            return R.ok().put("res", ruleComponent.matchRelease(param));
        }catch (Exception e){
            e.printStackTrace();
            return R.error(Constant.Msg.SERVER_INTERNAL_EXCEPTION);
        }
    }

}
