package org.dsa.modules.diagnostic.quartz;

import lombok.extern.slf4j.Slf4j;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.IPUtils;
import org.dsa.common.utils.PortUtil;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.diagnostic.service.DiagnosticService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import jakarta.annotation.Resource;

/**
 * @author qzq
 */
@Slf4j
public class QuartzSynLoggerDetail extends QuartzJobBean {

    @Resource
    private DiagnosticService diagnosticService;
    @Resource
    private RedisUtils redisUtils;

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        //定时器保证只在一个服务上执行 ip+端口
        String value = IPUtils.getLocalIp()+":"+getLocalPort();
        //是本机
        if (value.equals(redisUtils.get(Constant.REDIS_SYN_LOG_Key))) {
            try {
                log.info("同步离线诊断日志开始，quartzUser: " + value);
                diagnosticService.synchronizeLogs();
            } catch (Exception e) {
                e.printStackTrace();
//            JobExecutionException ex = new JobExecutionException(e);
                //抛出异常取消所有触发器
//            ex.setUnscheduleAllTriggers(true);
                //抛出异常后立即重新执行(产生大量日志，设置时间间隔)
//            ex.setRefireImmediately(true);
//            throw ex;
            }
        }
    }
    private String getLocalPort(){
        String localPort = "";
        try {
            localPort = PortUtil.getLocalPort();
        } catch (Exception e) {
            log.info("获取本服务端口号失败");
            e.printStackTrace();
        }
        return localPort;
    }
}
