//package org.dsa.modules.remoteDiagnostic.dao;
//
//
//import com.baomidou.mybatisplus.core.mapper.BaseMapper;
//import org.apache.ibatis.annotations.Insert;
//import org.apache.ibatis.annotations.Mapper;
//import org.apache.ibatis.annotations.Param;
//import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuGroupEntity;
//import org.dsa.modules.remoteDiagnostic.entity.RemoteVehiclePlatformEcuEntity;
//
//import java.util.List;
//
//@Mapper
//public interface RemoteVehiclePlatformEcuDao extends BaseMapper<RemoteVehiclePlatformEcuEntity> {
//
//    @Insert("<script>"
//            + " insert into r_platform_ecu (platform_id,ecu_id) values "
//            + " <foreach collection='ecus' item='item' separator=','> "
//            + " (#{platformId},#{item.id}) "
//            + " </foreach>"
//            + " </script>")
//    void inserts(@Param("platformId") Long id, @Param("ecus") List<RemoteVehicleEcuGroupEntity> groups);
//}
