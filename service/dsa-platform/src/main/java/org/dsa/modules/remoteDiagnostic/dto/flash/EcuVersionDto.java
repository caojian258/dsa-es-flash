package org.dsa.modules.remoteDiagnostic.dto.flash;

import lombok.Data;

import java.io.Serializable;

@Data
public class EcuVersionDto implements Serializable {

    private Long id;

    private Long ecuId;

    private String ecuName;

    private String shortNameTi;

    private String versionName;

    //版本是否强制mandatory/optional
    private String versionType;

    private String descriptionTi;

    private String releaseNodeTi;

    private Integer status;

    //版本是否可以跨过1可以0不可
    private Integer isCross;

    private Long sorted;
}
