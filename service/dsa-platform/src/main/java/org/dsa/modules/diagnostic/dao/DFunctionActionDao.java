package org.dsa.modules.diagnostic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.dsa.modules.diagnostic.entity.DFunctionActionEntity;

import java.util.List;

/**
 * 操作码
 * 
 */
@Mapper
public interface DFunctionActionDao extends BaseMapper<DFunctionActionEntity> {


	/**
	 * 用于session查询显示select信息
	 */
    List<DFunctionActionEntity> selectShowList();
}
