package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.dto.WorkshopUserGroupTotalDto;
import org.dsa.modules.reporter.entity.WorkshopUserGroups;

import java.util.List;
import java.util.Map;

@Mapper
public interface WorkshopUserGroupsMapper extends BaseMapper<WorkshopUserGroups> {

    public void updateGroupIdByGroupName(@Param("groupId") Integer groupId, @Param("groupName") String groupName);

    public List<WorkshopUserGroupTotalDto> userGroupTotal(@Param("startDate") String startDate, @Param("top") Integer top,@Param("condition") List<String> condition);
}
