package org.dsa.modules.remoteDiagnostic.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskOverviewEntity;
import org.dsa.modules.remoteDiagnostic.vo.Task.DiagnosticTaskOverviewPageReqVo;

import java.util.List;

public interface DiagnosticTaskOverviewService {

    Page<DiagnosticTaskOverviewEntity> selectPage(DiagnosticTaskOverviewPageReqVo vo);

    DiagnosticTaskOverviewEntity getInfo(Long id);

    void saveTask(DiagnosticTaskOverviewEntity task);

    void updateTask(DiagnosticTaskOverviewEntity task);

    void updateTask(String sessionId, String vin);

    List<DiagnosticTaskOverviewEntity> getTasks();
}
