package org.dsa.modules.reporter.vo.api;

import lombok.Data;
import org.dsa.modules.reporter.dto.ClientBaseInfoDto;

import java.io.Serializable;
import java.util.List;

@Data
public class DeviceInfoVo implements Serializable {

    private List<ClientBaseInfoDto> baseList;

    private List<ClientBaseInfoDto> workshopList;

    private List<ClientBaseInfoDto> LoginList;
}
