package org.dsa.modules.reporter.dto;

import lombok.Data;


@Data
public class WtsInfoDto {

    private String softVersion;

    private String account;

    private String profile;

    private String expiration;

    private String language;

    private String date;
}
