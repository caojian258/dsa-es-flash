package org.dsa.modules.diagnostic.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * did
 *
 */
@Data
public class LoggerDataActionVersionEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ecuName
     */
    private String name;
    /**
     * ti
     */
    private String ti;
    /**
     * did值
     */
    private String value;
}
