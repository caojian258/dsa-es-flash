package org.dsa.modules.reporter.event;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class SessionPublisher {
    @Autowired
    private ApplicationEventPublisher publisher;

    /***
     * @todo 根据压缩包处理文件 解压到工作目录
     * @param tag
     * @param path
     * @param name
     */
    public void handlerFile(String path, String tag, String name){
        log.info("start publish session log data: {}", tag);
        publisher.publishEvent(new SessionEvent(this, tag, path, name));
    }

}
