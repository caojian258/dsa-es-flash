package org.dsa.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * 语种
 */
@Data
public class SysLocaleEntity {

    /**
     * 自增主键
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 语言类型
     */
    private String locale;

    /**
     * 语言名称
     */
    private String name;

    /**
     * 单位
     */
    private String unit;
}
