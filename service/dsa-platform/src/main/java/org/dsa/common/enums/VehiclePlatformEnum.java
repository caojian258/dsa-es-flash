package org.dsa.common.enums;

/**
 *
 * OEM支持的车型枚举
 *
 * @author weishunxin
 * @since 2023-06-02
 */
public enum VehiclePlatformEnum {

    EH300("EH300"),
    EH500("EH500");

    private final String code;

    VehiclePlatformEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static boolean contains(String code) {
        if (code == null) {
            return false;
        }

        for (VehiclePlatformEnum value : VehiclePlatformEnum.values()) {
            if (code.equals(value.getCode())) {
                return true;
            }
        }

        return false;
    }

}
