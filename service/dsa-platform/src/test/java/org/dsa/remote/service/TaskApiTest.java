package org.dsa.remote.service;

import org.apache.commons.math3.analysis.function.Sinc;
import org.dsa.modules.remoteDiagnostic.config.RemoteDiagnosticConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.*;
import java.nio.channels.FileChannel;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class TaskApiTest {

//    @Autowired
//    RemoteDiagnosticConfig remoteDiagnosticConfig;

    @Test
    public void fileTest() {
        String path = "D:\\usr\\temp\\Session_50244a9d-a111-45af-ad7d-bddbe358a2a5_0xLKM4rr_20221207100507.zip";
        String desPath = "D:\\usr\\temp2\\Session_50244a9d-a111-45af-ad7d-bddbe358a2a5_0xLKM4rr_20221207100507.zip";
        String temp = "D:\\usr\\temp2\\temp";

        File file = new File(path);
        File desFile = new File(desPath);
        File tempFile = new File(temp);
        try (FileInputStream bis = new FileInputStream(new File(path)); FileChannel isC = bis.getChannel();
             FileOutputStream bos = new FileOutputStream(new File(temp)); FileChannel osC = bos.getChannel();
             FileInputStream bisT = new FileInputStream(new File(temp));
             FileOutputStream des = new FileOutputStream(new File(desPath));) {

            if (!tempFile.exists()) tempFile.createNewFile();
            if (!desFile.exists()) {
                isC.transferTo(0, file.length(), osC);
            } else if (desFile.length() < file.length()) {
                isC.transferTo(desFile.length(), file.length(), osC);
            } else {
                return;
            }

            System.out.println(file.length());
            System.out.println(temp.length());
            // 尝试修改为实时保存
//            FileOutputStream fos = new FileOutputStream(file);
//            bos = new BufferedOutputStream(fos);
            byte[] buffer = new byte[512];
            int len;
            int index = 0;
            while ((len = bisT.read(buffer)) != -1) {
                des.write(buffer, 0, len);
                des.flush();
                index++;
//                if (index == 3) break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            boolean delete = tempFile.delete();
        }

    }
}
