package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.dto.AppOptionDto;
import org.dsa.modules.reporter.entity.ClientDependenceTree;

import java.util.List;

@Mapper
public interface ClientDependenceTreeMapper extends BaseMapper<ClientDependenceTree> {

    public List<ClientDependenceTree> queryByDeviceId(String id);

    public List<AppOptionDto> queryAppName(@Param("pcid") String deviceId, @Param("idList") List<Integer> ids);

    public void deleteByDeviceId(@Param("pcid") String deviceId);

}
