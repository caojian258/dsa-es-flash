package org.dsa.modules.reporter.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.reporter.entity.ScheduleJobLog;
import org.dsa.modules.reporter.vo.jobLog.JobLogPageReqVo;

public interface ScheduleJobLogService {

    public void insertLog(Long jobId, String jobName, String jobGroup, String status, String note);

    public Page<ScheduleJobLog> selectPage(JobLogPageReqVo vo);
}
