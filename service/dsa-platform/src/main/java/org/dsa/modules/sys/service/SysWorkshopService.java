package org.dsa.modules.sys.service;


import com.baomidou.mybatisplus.extension.service.IService;
import org.dsa.modules.sys.entity.SysWorkshopEntity;

import java.util.List;
import java.util.Map;

/**
 * 部门管理
 *
 */
public interface SysWorkshopService extends IService<SysWorkshopEntity> {

	List<SysWorkshopEntity> queryList(Map<String, Object> map);

	/**
	 * 查询子部门ID列表
	 * @param parentId  上级部门ID
	 */
	List<Long> queryDetpIdList(Long parentId);

	/**
	 * 获取子部门ID，用于数据过滤
	 */
	List<Long> getSubWorkshopIdList(Long workshopId);

	/**
	 * 通过部门id查询部门名
	 * @param workshopId
	 * @return
	 */
	String queryWorkshopNameById(Long workshopId);

	/**
	 * 根据部门id,查询部门信息
	 */
	SysWorkshopEntity getSysWorkshopEntity(Long workshopId);

	/**
	 * 部门总数
	 */
	int querySysWorkshopEntityCount();


	/**
	 * 删除不在同步list中的组织信息
	 * @param organList
	 */
	void removeNotInIds(List<Long> organList);

	List<String> queryListString();

	void saveOrg(SysWorkshopEntity sysWorkshopEntity);

	void disableById(long workshopId);
}
