package org.dsa.modules.remoteDiagnostic.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RemoteVersionTypeEnum {

    // 数据类型
    ECUVersion("ecu_version"),
    ECU("ecu"),
    ECUGroup("ecu_group"),
    VehiclePool("vehicle_pool"),
    VehicleVersion("vehicle_version"),

    // 操作类型
    EDIT("EDIT"),
    ADD("ADD"),

    // 识别信息类型
    TARGET("target"),
    EXPECT("expect");

    @EnumValue
    private final String value;
}
