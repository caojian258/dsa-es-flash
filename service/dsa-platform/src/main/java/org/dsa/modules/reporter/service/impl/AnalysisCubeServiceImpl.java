package org.dsa.modules.reporter.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.ArrayUtils;
import org.dsa.modules.reporter.dao.AnalysisCubeMapper;
import org.dsa.modules.reporter.dto.ChartItemDto;
import org.dsa.modules.reporter.entity.AnalysisCube;
import org.dsa.modules.reporter.enums.DateAttributeEnum;
import org.dsa.modules.reporter.service.AnalysisCubeService;
import org.dsa.modules.reporter.util.LocalUtil;
import org.dsa.modules.reporter.vo.client.ClientChartResVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AnalysisCubeServiceImpl implements AnalysisCubeService {

    @Autowired
    AnalysisCubeMapper cubeMapper;

    @Override
    public String buildSql(AnalysisCube cube, Map<String, Object> where) {

        if(StrUtil.isNotBlank(cube.getSql()) ){
            if(CollectionUtil.isEmpty(where)){
                return cube.getSql();
            }
            String sql = cube.getSql();
            StringBuilder sb = new StringBuilder();
            String condition = formatCondition(cube, where);
            log.info("buildSql where: {}", JSONObject.toJSONString(where));
            log.info("buildSql condition: {}", condition);
            int groupByPos =  sql.indexOf("group by");
            sb.append(sql, 0, groupByPos);
            if(StrUtil.isNotBlank(condition)){
                if(groupByPos>1 && groupByPos<sql.length()){
                    sb.append(sb.toString().contains("where") ? " and " :  " where ").append(condition);
                }
            }
            sb.append(" ").append(sql.substring(groupByPos));
            if(null != cube.getTop() && cube.getTop()>0 && where.get("top") !=null && Integer.parseInt(where.get("top").toString())>0 ){
                sb.append(" order by value desc limit ").append(where.get("top").toString());
            }
            return sb.toString();
        }

        StringBuilder sb = new StringBuilder();
        sb.append("select ");
        sb.append(cube.getTableColumn()).append(",").append(cube.getAggregator());
        sb.append(" from ").append(cube.getTableName());

        log.info("buildSql no sql where: {}", where);

        String condition = formatCondition(cube, where);

        log.info("buildSql no sql condition: {}", JSONObject.toJSONString(condition));

        if(StrUtil.isNotBlank(condition)){
            sb.append(" where ").append(condition);
        }

        sb.append(" group by ").append(cube.getMeasure());

        if(where.get("top") !=null && cube.getTop() !=null &&  cube.getTop() >0 && Integer.parseInt(where.get("top").toString())>0 ){
            sb.append(" order by value desc limit ").append(where.get("top").toString());
        }
        return sb.toString();
    }

    @Override
    public List<Map<String, Object>> getData(String sql) {
        return cubeMapper.getData(sql);
    }

    @Override
    public String formatCondition(AnalysisCube cube, Map<String, Object> mc) {
        String cc = cube.getCondition();
        if(StrUtil.isBlank(cc)){
            return null;
        }
        String dateCondition = handleDateCondition(cube, mc);

        for (Map.Entry<String, Object> entry : mc.entrySet()) {
            String mapKey = entry.getKey();
            String mapValue = entry.getValue().toString();
            if (StringUtils.isBlank(mapValue) || "[]".equals(mapValue)) {
                continue;
            }
            // 处理数组，多条件
            if (mapValue.contains("[")) {
                mapValue = mapValue.replace("[","('").replace("]","')").replaceAll(", ","','");
            }
            if (mapValue.contains("unknown")){
                mapValue = mapValue.replace("'unknown'","null,''");
            }
            if (mapValue.contains("N/A")){
                mapValue = mapValue.replace("'N/A'","null,''");
            }
            String k = "{"+mapKey+"}";
            if(cc.contains(k)){
                cc = cc.replace(k , mapValue);
                log.info("buildSql formatCondition: {}", cc);
            }
        }
        if (cc.contains("{")){
            String[] ccs = cc.split("and");
            for (String s : ccs) {
                if (s.contains("{")) {
                    // 去掉无效判断
                    cc = cc.replace("and" + s, "").equals(cc) ? (cc.replace(s + "and", "").equals(cc) ? cc.replace(s, "") : cc.replace(s + "and", "")) : cc.replace("and" + s, "");
                }
            }
        }

        if(StrUtil.isNotBlank(dateCondition) && StrUtil.isNotBlank(cc)){
            cc +=" and " + dateCondition;
        }else if(StrUtil.isNotBlank(dateCondition)){
            cc = dateCondition;
        }
        log.info("buildSql formatCondition result: {}", cc);
        return cc;
    }

    @Override
    public AnalysisCube getCube(String table, String dimension, String visible) {

        return cubeMapper.getOneCube(table, dimension, visible);
    }

    @Override
    public AnalysisCube getCubeByCode(String code) {
        return cubeMapper.getCubeByCode(code);
    }

    @Override
    public ClientChartResVo formatChartData(List<Map<String, Object>> items, String di, String visible) {
        ClientChartResVo resVo = new ClientChartResVo();
        Set<String> legendData = new LinkedHashSet<>();
        List<ChartItemDto> dtoList = new ArrayList<>();
        Long itemTotal = 0L;

        switch (visible){
            case "bar":
                Set<String> xAxisData = new LinkedHashSet<>();
                Map<String,Map<String, Object>> barStore = new HashMap<>();

                for (Map<String, Object> item: items){
                    if(null !=item.get(di)){
                        String name = String.valueOf(item.get(di));
                        legendData.add(name);
                        xAxisData.add(String.valueOf(item.get("collect_month")));
                        Map<String, Object> map = null;
                        map = barStore.get(name);
                        if(null == map){
                            map = new HashMap<>();
                        }
                        itemTotal +=  Long.valueOf(item.get("value").toString());
                        map.put(String.valueOf(item.get("collect_month")), item.get("value"));
                        barStore.put(name, map);
                    }
                }

                //这里要notice xAxisData 保证这里面的数据按照顺序填满
                log.info("bar store:{}", JSONObject.toJSONString(barStore));

                String[] xAxisDatas = new String[xAxisData.size()];
                xAxisData.toArray(xAxisDatas);

                for (String key : barStore.keySet()) {
                    ChartItemDto dto = new ChartItemDto();
                    dto.setName(key);
                    dto.setType("bar");
                    dto.setStack(di);
                    dto.setBarWidth("50%");
                    dto.setBarMaxWidth("50%");

                    long[] childrenTotal = new long[xAxisData.size()];

                    Map<String,Object> bs = barStore.get(key);
                    for (String k: bs.keySet()){
                        int index = ArrayUtils.indexOf(xAxisDatas, k);
                        if(index>-1){
                            childrenTotal[index] = Integer.valueOf(bs.get(k).toString());
                        }
                    }
                    dto.setData(childrenTotal);
                    dtoList.add(dto);
                }

                resVo.setXAxisData(xAxisData);
                resVo.setSeriesData(dtoList);
                resVo.setLegendData(legendData);
                break;
            case "sunburst":
                Map<String,Map<String, Object>> subBurstStore = new HashMap<>();
                Map<String,Long> stat = new HashMap<>();

                for (Map<String, Object> item: items){
                    if(null !=item.get(di)){
                       String title = String.valueOf(item.get("name"));
                        legendData.add(title);
                        Map<String, Object> map = null;
                        map = subBurstStore.get(title);
                        if(null == map){
                            map = new HashMap<>();
                        }
                        String name = String.valueOf(item.get(di));
                        itemTotal +=  Long.valueOf(item.get("value").toString());
                        map.put(name, item.get("value"));
                        subBurstStore.put(title, map);
                        Long t = 0L;
                        if(null !=stat.get(title)){
                            t =  stat.get(title) + Long.valueOf(item.get("value").toString());
                        }else{
                            t = Long.valueOf(item.get("value").toString());
                        }
                        stat.put(title, t);
                    }
                }

                for (String key : subBurstStore.keySet()) {
                    ChartItemDto dto = new ChartItemDto();
                    dto.setName(key);
                    dto.setValue(stat.get(key));
                    List<Map<String,Object>> children = new ArrayList<>();
                    Map<String,Object> sm = subBurstStore.get(key);
                    for (String k: sm.keySet()){
                        Map<String,Object> m = new HashMap<>();
                        m.put("name", k);
                        m.put("value", sm.get(k));
                        children.add(m);
                    }
                    dto.setChildren(children);
                    dtoList.add(dto);
                }
                resVo.setSeriesData(dtoList);
                resVo.setLegendData(legendData);

                break;
            case "pi":
                Map<String, Object> resultItems = new HashMap<>();
                List<Map<String, Object>> rr =  items.stream().distinct().collect(Collectors.toList());
                Map<String, Long> piMap = new HashMap<>(rr.size());

                for (Map<String, Object> i: rr) {
                    if(null !=i.get(di)) {
                        piMap.put(i.get(di).toString(), 0L);
                        legendData.add(i.get(di).toString());
                    }
                }

                for (Map<String, Object> item: items) {
                    if(null !=item.get(di)){
                       Long c =  Long.valueOf(item.get("value").toString());
                       Long e =  piMap.get(item.get(di).toString());
                       piMap.put(item.get(di).toString(), e+c);
                    }
                }
                log.info("{}", JSONObject.toJSONString(piMap));
                for (Map<String, Object> ii: rr) {
                    if(null !=ii.get(di)) {
                        ChartItemDto dto = new ChartItemDto();
                        String name = String.valueOf(ii.get(di));
                        String code = "";
                        if(null == ii.get("code") || ii.get("code").equals("")){
                            code = "N/A";
                        }else{
                            code = ii.get("code").toString();
                        }

                        dto.setType(name);
                        if(di.equals("province")){
                            dto.setType(code);
                        }
                        if (name.equals("unknown")){
                            name = LocalUtil.get("workshop.unknown");
                        }
                        dto.setName(name);
                        itemTotal +=  piMap.get(name);
                        dto.setValue(piMap.get(name));
                        dtoList.add(dto);
                    }
                }

//                for (Map<String, Object> item: items) {
//                   if(null !=item.get(di)){
//                       ChartItemDto dto = new ChartItemDto();
//                       String name = String.valueOf(item.get(di));
//                       String code = "";
//                       if(null == item.get("code") || item.get("code").equals("")){
//                           code = "N/A";
//                       }else{
//                           code = item.get("code").toString();
//                       }
//
//                       dto.setType(name);
//                       if(di.equals("province")){
//                           dto.setType(code);
//                       }
//                       if (name.equals("unknown")){
//                           name = LocalUtil.get("workshop.unknown");
//                       }
//
//                       dto.setName(name);
//                       itemTotal +=  Long.valueOf(item.get("value").toString());
//                       dto.setValue(item.get("value"));
//                       dtoList.add(dto);
//                   }
//                }

                resVo.setLegendData(legendData);
                resVo.setSeriesData(dtoList);
                break;
        }

        resVo.setItemTotal(itemTotal);
        resVo.setTotal(Long.valueOf(dtoList.size()));
        return resVo;
    }

    private String handleDateCondition(AnalysisCube cube, Map<String, Object> mc){
        String startDate = null;
        String endDate = null;
        if(null == cube.getDateColumn() || null == mc || mc.size() == 0){
            return null;
        }
        if(mc.containsKey("startDate") && mc.get("startDate")!=null){
            startDate = mc.get("startDate").toString();
        }

        if(mc.containsKey("endDate") && mc.get("endDate")!=null){
            endDate = mc.get("endDate").toString();
        }

        return formatDateCondition(cube, startDate, endDate);
    }

    private String formatDateCondition(AnalysisCube cube, String startDate, String endDate){
        String condition = "";

        if(StrUtil.isNotBlank(startDate) && StrUtil.isNotBlank(endDate)){
            if (cube.getDateAttribute().equals(DateAttributeEnum.STRING.getValue())){
                condition = cube.getDateColumn() + " between '"+startDate+"' and '"+endDate+"'";
            }else if(cube.getDateAttribute().equals(DateAttributeEnum.TIMESTAMP.getValue()) ){
                condition = "to_char("+cube.getDateColumn()+", 'yyyy-mm-dd') between '"+startDate+"' and '"+endDate+"'";
            }else if(cube.getDateAttribute().equals(DateAttributeEnum.UNIX_TIMESTAMP.getValue())){
                condition = "to_char("+cube.getDateColumn()+", 'yyyy-mm-dd') between '"+startDate+"' and '"+endDate+"'";
            }else if(cube.getDateAttribute().equals(DateAttributeEnum.LONG_UNIX_TIMESTAMP.getValue())) {
                condition = "to_char(" + cube.getDateColumn() + ", 'yyyy-mm-dd') between '" + startDate + "' and '" + endDate + "'";
            }

            }else if(StrUtil.isNotBlank(startDate)){
            if (cube.getDateAttribute().equals(DateAttributeEnum.STRING.getValue())){
                condition = cube.getDateColumn() + " >= '"+startDate+"'";
            }else if(cube.getDateAttribute().equals(DateAttributeEnum.TIMESTAMP.getValue()) ){
                condition = "to_char("+cube.getDateColumn()+", 'yyyy-mm-dd') >= '"+startDate+"'";
            }else if(cube.getDateAttribute().equals(DateAttributeEnum.UNIX_TIMESTAMP.getValue())){
                condition = "to_char("+cube.getDateColumn()+", 'yyyy-mm-dd') >= '"+startDate+"'";
            }else if(cube.getDateAttribute().equals(DateAttributeEnum.LONG_UNIX_TIMESTAMP.getValue())) {
                condition = "to_char(" + cube.getDateColumn() + ", 'yyyy-mm-dd') >= '" + startDate + "'";
            }
        }
        return condition;
    }
}
