package org.dsa.modules.reporter.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.dto.AppLogStatDto;
import org.dsa.modules.reporter.dto.ClientUpgradeFullDto;
import org.dsa.modules.reporter.entity.ClientUpgrade;
import org.dsa.modules.reporter.upgrade.*;


import java.util.List;
import java.util.Set;

@Mapper
public interface ClientUpgradeMapper extends BaseMapper<ClientUpgrade> {

    public List<ClientUpgrade> queryDataByTag(String tag);

    public ClientUpgrade getOneByTag(String tag);

    public List<AppLogStatDto> queryAppLogStat(Long startTime, Long endTime);

    /***
     * 无法去重和拿到不同操作的数据故取消
     * */
    public List<ClientUpgradeFullDto> getOneFullData(String tag);

    public void insertSoftwareUpgradeByTag(@Param("tag") String tag);

    public void insertComponentUpgradeByTag(@Param("tag") String tag);

    public void insertSoftwareDownloadByTag(@Param("tag") String tag);

    public void insertComponentDownloadByTag(@Param("tag") String tag);

    public void insertSoftwareInstallByTag(@Param("tag") String tag);

    public void insertComponentInstallByTag(@Param("tag") String tag);

    public void insertSoftwareRepairByTag(@Param("tag") String tag);

    public void insertComponentRepairByTag(@Param("tag") String tag);

    //@todo need to test
    public void insertSoftwareRollbackByTag(@Param("tag") String tag);

    public void insertComponentRollbackByTag(@Param("tag") String tag);

    public void insertSoftwareUnInstallByTag(@Param("tag") String tag);

    public void insertComponentUnInstallByTag(@Param("tag") String tag);

    public List<ClientDownloadDto> querySoftwareDownloadListByTag(@Param("tag") String tag, @Param("result") Integer result);

    public List<ClientDownloadDto> queryComponentDownloadListByTag(@Param("tag") String tag, @Param("result") Integer result);


    public List<ClientInstallDto> querySoftwareInstallListByTag(@Param("tag") String tag, @Param("result") Integer result);

    public List<ClientInstallDto> queryComponentInstallListByTag(@Param("tag") String tag, @Param("result") Integer result);

    public List<ClientRepairDto> querySoftwareRepairListByTag(@Param("tag") String tag, @Param("result") Integer result);

    public List<ClientRepairDto> queryComponentRepairListByTag(@Param("tag") String tag, @Param("result") Integer result);

    public List<ClientRollbackDto> querySoftwareRollbackListByTag(@Param("tag") String tag, @Param("result") Integer result);

    public List<ClientRollbackDto> queryComponentRollbackListByTag(@Param("tag") String tag, @Param("result") Integer result);

    public List<ClientUnInstallDto> querySoftwareUnInstallListByTag(@Param("tag") String tag, @Param("result") Integer result);

    public List<ClientUnInstallDto> queryComponentUnInstallListByTag(@Param("tag") String tag, @Param("result") Integer result);


    public Set<Integer> queryClientUpdateId(String deviceId);
}
