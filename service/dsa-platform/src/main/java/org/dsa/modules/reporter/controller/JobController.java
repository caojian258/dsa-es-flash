package org.dsa.modules.reporter.controller;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.annotation.SysLog;
import org.dsa.common.utils.R;
import org.dsa.modules.reporter.config.CronJobConfig;
import org.dsa.modules.reporter.config.ReportConfig;
import org.dsa.modules.reporter.entity.ScheduleJob;
import org.dsa.modules.reporter.entity.ScheduleJobLog;
import org.dsa.modules.reporter.enums.JobStatusEnum;
import org.dsa.modules.reporter.service.ScheduleJobLogService;
import org.dsa.modules.reporter.service.SchedulerJobService;
import org.dsa.modules.reporter.util.CronUtils;
import org.dsa.modules.reporter.util.LocalUtil;
import org.dsa.modules.reporter.vo.job.JobCreateReqVO;
import org.dsa.modules.reporter.vo.job.JobPageReqVo;
import org.dsa.modules.reporter.vo.jobLog.JobLogPageReqVo;
import org.quartz.SchedulerException;
import org.quartz.SchedulerMetaData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import java.util.*;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/job")
public class JobController {

	private final SchedulerJobService scheduleJobService;

	private final ScheduleJobLogService jobLogService;

	@Autowired
	ReportConfig reportConfig;

	@RequestMapping(value = "/program", method= {RequestMethod.GET })
	public Object program() {
		List<Map<String, String>> programList = new ArrayList<>();

		List<CronJobConfig> jobs = reportConfig.getJobs();

		for (CronJobConfig jobConfig: jobs){

			Map<String, String> job = new HashMap<>();
			String key = jobConfig.getName();
			job.put("key", key);
			job.put("title", LocalUtil.get("job.title."+key));
			job.put("class", jobConfig.getClassName());
			programList.add(job);
		}

		return R.ok().put("data", programList);
	}

	@SysLog("saveOrUpdate 定时任务")
	@RequestMapping(value = "/saveOrUpdate", method = {RequestMethod.POST })
	public Object saveOrUpdate(@Valid @RequestBody JobCreateReqVO vo) {

		R r = new R();

		List<CronJobConfig> jobList = reportConfig.getJobs();
		Map<String, CronJobConfig> jobs = new HashMap<>();
		for (CronJobConfig job: jobList){
			jobs.put(job.getName(), job);
		}

		if(null == jobs.get(vo.getJobGroup())){
			r.put("code",500);
			r.put("msg", LocalUtil.get("job.message.jobClass_missMatch"));
			return r;
		}
		CronJobConfig jobConfig = jobs.get(vo.getJobGroup());

		ScheduleJob job = new ScheduleJob();

		if(null == vo.getCronJob()){
			vo.setCronJob(false);
		}else{
			if(vo.getCronJob()){
				if(StrUtil.isBlank(vo.getCronExpression())){
					r.put("code",500);
					r.put("msg", LocalUtil.get("job.message.express_empty"));
					return r;
				}

				if(!CronUtils.isValid(vo.getCronExpression())){
					r.put("code",500);
					r.put("msg", LocalUtil.get("job.message.express_invalid"));
					return r;
				}
				job.setCronExpression(vo.getCronExpression());
			}
		}


		job.setJobId(vo.getJobId());
		job.setJobName(vo.getJobName());
		job.setJobGroup(vo.getJobGroup());
		job.setCronJob(vo.getCronJob());
		job.setJobClass(jobConfig.getClassName());
		job.setHasListener(jobConfig.getHasListener());
		if(StrUtil.isNotBlank(jobConfig.getListenerClass())){
			job.setListenerClass(jobConfig.getListenerClass());
		}

		log.info("params, job = {}", job);

		if(StrUtil.isBlank(job.getJobStatus())){
			job.setJobStatus(JobStatusEnum.INIT.getValue());
		}

		if(null == job.getHasListener()){
			job.setHasListener(false);
		}
		try {
			scheduleJobService.saveOrUpdate(job);
			r.put("code",0);
		} catch (Exception e) {
			log.error("saveOrUpdate job ex: {}", JSONObject.toJSONString(e));
			r.put("code",500);
			r.put("msg", e.getMessage());
		}
		return r;
	}

	@RequestMapping("/metaData")
	public Object metaData() throws SchedulerException {
		SchedulerMetaData metaData = scheduleJobService.getMetaData();
		return metaData;
	}

	@RequestMapping("/getAllJobs")
	public Object getAllJobs() throws SchedulerException {
		List<ScheduleJob> jobList = scheduleJobService.getAllJobList();
		return jobList;
	}

	@SysLog("启动 定时任务")
	@RequestMapping(value = "/runJob", method =  RequestMethod.POST )
	public Object runJob(@RequestBody ScheduleJob job) {
		log.info("params, job = {}", job);
		R r = new R();
		try {
			scheduleJobService.startJobNow(job);
			r.put("code",0);
		} catch (Exception e) {
			r.put("code",500);
			log.error("runJob ex:{}", e.getMessage());
		}
		return r;
	}

	@SysLog("停止 定时任务")
	@RequestMapping(value = "/pauseJob", method = { RequestMethod.POST })
	public Object pauseJob(@RequestBody ScheduleJob job) {
		log.info("params, job = {}", job);
		R r = new R();
		try {
			scheduleJobService.pauseJob(job);
			r.put("code",0);
		} catch (Exception e) {
			r.put("code",500);
			log.error("pauseJob ex:{}", e.getMessage());
		}
		return r;
	}

	@SysLog("恢复 定时任务")
	@RequestMapping(value = "/resumeJob", method = { RequestMethod.POST })
	public Object resumeJob(@RequestBody ScheduleJob job) {
		log.info("params, job = {}", job);
		R r = new R();
		try {
			scheduleJobService.resumeJob(job);
			r.put("code",0);
		} catch (Exception e) {
			r.put("code",500);
			log.error("resumeJob ex:{}", e.getMessage());
		}
		return r;
	}

	@SysLog("删除 定时任务")
	@RequestMapping(value = "/deleteJob", method = { RequestMethod.POST })
	public Object deleteJob(@RequestBody ScheduleJob job) {
		log.info("params, job = {}", job);
		R r = new R();
		try {
			scheduleJobService.deleteJob(job);
			r.put("code",0);
		} catch (Exception e) {
			r.put("code",500);
			log.error("deleteJob ex:{}", e.getMessage());
		}
		return r;
	}

	@GetMapping(value = "/lang",produces = {"application/json;charset=UTF-8"})
	public Object getLang(){
		return R.ok(LocalUtil.get("user.appname"));
	}


	@RequestMapping(value = "/log", method = { RequestMethod.POST })
	public R jobLog(@RequestBody JobLogPageReqVo vo){

		Page<ScheduleJobLog> logPage =  jobLogService.selectPage(vo);

		return R.ok().put("data", logPage);
	}

	@RequestMapping(value = "/job", method = { RequestMethod.POST })
	public R jobPage(@RequestBody JobPageReqVo vo){
		Page<ScheduleJob> jobPage =  scheduleJobService.selectPage(vo);
		return R.ok().put("data", jobPage);
	}
}
