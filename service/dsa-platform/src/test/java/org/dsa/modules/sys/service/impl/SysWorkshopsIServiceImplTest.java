package org.dsa.modules.sys.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.read.listener.PageReadListener;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import junit.framework.TestCase;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.utils.PageUtils;
import org.dsa.modules.sys.dao.SysWorkshopsDao;
import org.dsa.modules.sys.dto.WorkshopExcelDto;
import org.dsa.modules.sys.entity.SysLocationEntity;
import org.dsa.modules.sys.entity.SysWorkshopsEntity;
import org.dsa.modules.sys.service.SysLocationService;
import org.dsa.modules.sys.service.SysWorkshopsIService;
import org.dsa.modules.sys.vo.workshop.WorkshopClientVo;
import org.dsa.modules.sys.vo.workshop.WorkshopFillVo;
import org.dsa.modules.sys.vo.workshop.WorkshopPageVo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SysWorkshopsIServiceImplTest extends TestCase {

    @Autowired
    SysLocationService sysLocationService;

    @Autowired
    SysWorkshopsIService sysWorkshopsIService;

    @Autowired
    SysWorkshopsDao workshopsDao;

    @Test
    public void testFindById(){
        String templateName = "workshops_template.xlsx";
        String fileName = "C:\\usr\\file\\template\\workshops_data_batch.xlsx";
        String templateFileName = "C:\\usr\\file\\template\\"+templateName;

        WorkshopFillVo fillData = new WorkshopFillVo();
        fillData.setNo("1");
        fillData.setName("张三");
        fillData.setShortName("简称");
        fillData.setCityName("城市");
        fillData.setCode("test");
        fillData.setCountryName("中国");
        fillData.setProvinceName("省份");
        fillData.setNote("备注");
        fillData.setRegion("北方");

        WorkshopClientVo vo = new WorkshopClientVo();

        List<SysWorkshopsEntity> entityList =  sysWorkshopsIService.selectWorkshop(vo);
        List<WorkshopFillVo> voList = new ArrayList<>(entityList.size());
        int i = 1;
        for (SysWorkshopsEntity entity: entityList) {
            WorkshopFillVo fillVo = new WorkshopFillVo();
            BeanUtil.copyProperties(entity, fillVo);
            fillVo.setNo(String.valueOf(i++));
            voList.add(fillVo);
        }


        EasyExcel.write(fileName).withTemplate(templateFileName).sheet().doFill(voList);

    }
    @Test
    public void disabledById(){
        sysWorkshopsIService.disabledById(2L);
    }

    @Test
    public void testPage(){
        WorkshopPageVo vo = new WorkshopPageVo();
        vo.setSortColumn("id");
        vo.setSortAsc(false);
        vo.setPageNo(1);
        vo.setPageSize(10);
        vo.setRegion("南区");
        vo.setName("万达");

        Page<SysWorkshopsEntity> page =  sysWorkshopsIService.queryPage(vo);
        System.out.println(JSONObject.toJSONString(page.getRecords()));
    }

    @Test
    public void testLocation(){
        String fileName = "C:\\upload\\workshop.xlsx";
        // 这里 需要指定读用哪个class去读，然后读取第一个sheet 文件流会自动关闭
        // 这里默认每次会读取100条数据 然后返回过来 直接调用使用数据就行
        // 具体需要返回多少行可以在`PageReadListener`的构造函数设置

        EasyExcel.read(fileName, WorkshopExcelDto.class, new PageReadListener<WorkshopExcelDto>(dataList -> {
            for (WorkshopExcelDto demoData : dataList) {
                if(null != demoData && StringUtils.isNotEmpty(demoData.getName())){
                    Long code = 100000L;

                    System.out.println("item:");
                    String p = demoData.getProvince().trim().replace("自治区", "");
                    SysLocationEntity province = sysLocationService.findByNameAndType(p, "1");
                    SysLocationEntity city =  sysLocationService.findByNameAndType(demoData.getCity().trim(), "2");
                    if(null == city){
                        city =  sysLocationService.findByNameAndType(demoData.getCity().trim(), "3");
                    }
                    SysWorkshopsEntity workshopsEntity = new SysWorkshopsEntity();
                    workshopsEntity.setCode(demoData.getCode());
                    workshopsEntity.setName(demoData.getName());
                    workshopsEntity.setShortName(demoData.getShortName());
                    workshopsEntity.setRegion(demoData.getRegion());
                    if(null != city){
                        workshopsEntity.setCityCode(city.getCode());
                        workshopsEntity.setCityName(city.getName());
                    }else{
                        workshopsEntity.setCityName(demoData.getCity().trim());
                    }

                    if(null != province){
                        workshopsEntity.setProvinceCode(province.getCode());
                        workshopsEntity.setProvinceName(province.getName());
                    }else{
                        workshopsEntity.setProvinceName(p);
                    }

                    workshopsEntity.setCountryCode(code);
                    workshopsEntity.setCountryName(demoData.getCountry());
                    workshopsEntity.setNote(demoData.getNote());
                    workshopsEntity.setCreateTime(new Date());
                    workshopsEntity.setUpdateTime(new Date());

                    System.out.println( JSON.toJSONString(workshopsEntity));

                    sysWorkshopsIService.save(workshopsEntity);
                }
            }
        })).headRowNumber(3).sheet().doRead();
    }

}