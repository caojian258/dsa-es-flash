package org.dsa.modules.vehicle.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@TableName("d_vehicle_info")
public class DVehicleInfoEntity implements Serializable
{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * vin码
     */
    private String vin;
    /**
     * 车型信息表id
     */
    private Long vehicleTypeId;
    /**
     * 车型名称
     */
    @TableField(exist = false)
    private String vehicleType;
    /**
     * 车辆生产日期
     */
    private String productionDate;
    /**
     * 产地
     */
    private String placeOfProduction;

    /**
     * 销售区域
     */
    private String regionCode;

    /**
     * 销售城市
     */
    private String cityCode;

    /**
     * 4S店名称
     */
    private String workshopName;


    /**
     * 同步时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 车辆来源 1——TSP同步 2——远程服务器添加
     */
    private Integer source;

    /**
     * 车辆版本主键
     */
    private Long versionId;

    /**
     * 当前车辆版本主键
     */
    @TableField(exist = false)
    private Long currentVersionId;

    @TableField(exist = false)
    private List<Long> tags;
}
