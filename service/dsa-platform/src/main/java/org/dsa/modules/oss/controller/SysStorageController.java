package org.dsa.modules.oss.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.dsa.common.utils.R;
import org.dsa.modules.oss.entity.SysFileEntity;
import org.dsa.modules.oss.service.FileService;
import org.dsa.modules.oss.vo.file.FilePageVo;
import org.dsa.modules.reporter.entity.FileLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import java.io.*;
import java.net.URLEncoder;

@Slf4j
@RestController
@RequestMapping("sys/storage")
public class SysStorageController {

    @Autowired
    FileService fileService;

    @RequestMapping("/page")
    public R page(@Valid @RequestBody FilePageVo vo){
        Page<SysFileEntity> page =  fileService.queryPage(vo);
        return R.ok().put("data", page);
    }

    @GetMapping(value = "/download/{id}")
    public void download(@PathVariable("id") String id, HttpServletRequest req, HttpServletResponse response){

        SysFileEntity fileLog = fileService.findOneFile(Long.valueOf(id));

        if(null == fileLog || null == fileLog.getUrl()){
            log.error("sys/storage/download {} not exist", id);
            return;
        }
        String filePath = fileLog.getUrl();
        File downloadFile = new File(filePath);

        if(!downloadFile.exists()){
            log.error("sys/storage/download {} not exist", id);
            return;
        }
        InputStream ips = null;
        OutputStream ops = null;
        try {
            ips =  new FileInputStream(downloadFile);
            ops = response.getOutputStream();
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=utf-8" + URLEncoder.encode(downloadFile.getName(), "UTF-8"));
            IOUtils.copy(ips, ops);
            ips.close();
            ops.flush();

        } catch (IOException e) {
            log.error("sys/storage/download error {}", e.getMessage());
        } finally {
            try {
                if(ips != null ) {
                    ips.close();
                }
            } catch (IOException e) {
                log.error("sys/storage/download close error {}", e.getMessage());
            }
            try {
                if(ops != null) {
                    ops.flush();
                }
            } catch (IOException e) {
                log.error("sys/storage/download flash error {}", e.getMessage());
            }
        }
    }
}
