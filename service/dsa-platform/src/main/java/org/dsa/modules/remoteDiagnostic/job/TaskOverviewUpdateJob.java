package org.dsa.modules.remoteDiagnostic.job;

import lombok.extern.slf4j.Slf4j;
import org.dsa.common.utils.DateUtils;
import org.dsa.common.utils.ObjectUtils;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.remoteDiagnostic.constant.RemoteDiagnosticConstant;
import org.dsa.modules.remoteDiagnostic.dao.DiagnosticTaskOverviewDao;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskOverviewEntity;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

@Component
@Slf4j
public class TaskOverviewUpdateJob {

    @Autowired
    RedissonClient redissonClient;
    @Autowired
    private DiagnosticTaskOverviewDao taskOverviewDao;
    @Autowired
    private RedisUtils redisUtils;

    @Scheduled(cron = "0 */5 * * * ?")
    public void execute() {
        RLock lock = redissonClient.getLock(RemoteDiagnosticConstant.OVERVIEW_DATA_UPDATE_LOCK);
        boolean b = lock.tryLock();
        try {
            if (b) {

                log.info("执行诊断任务概览更新>>>>>>>>>>>开始");
                AtomicInteger sum = new AtomicInteger(0);

                try {
                    Set<String> keys = redisUtils.getKeys(RemoteDiagnosticConstant.TASK_OVERVIEW_KEY + "*");
                    if (ObjectUtils.isNotNull(keys)) {
                        keys.forEach(e -> {
                            String[] split = e.split(":");
                            long id;
                            if (split.length != 0) {
                                id = Long.parseLong(split[split.length - 1]);
                            } else {
                                return;
                            }
                            DiagnosticTaskOverviewEntity entity = taskOverviewDao.getInfoByTask(id);

                            updateCacheVal(e, entity);

                            entity.setUpdatedAt(new Date());

                            taskOverviewDao.updateById(entity);

                            sum.getAndIncrement();
                        });
                    }
                    log.info("执行诊断任务概览更新>>>>>>>>>>>结束 >>>>>本次更新数据:" + sum.get() + "条");
                } catch (Exception e) {
                    log.error("执行诊断任务概览更新>>>>>>>>>>>异常 >>>>>>已更新数据:" + sum.get() + "条");
                    log.error(e.getMessage(), e);
                }
            }
        } finally {
            if (lock.isLocked() && lock.isHeldByCurrentThread()) {
                lock.unlock();
            }
        }
    }

    private void updateCacheVal(String key, DiagnosticTaskOverviewEntity entity) {
        if (redisUtils.hHasKey(key, RemoteDiagnosticConstant.TASK_OVERVIEW_MATCH_KEY)) {
            entity.setMatchNum(Long.parseLong(redisUtils.hget(key, RemoteDiagnosticConstant.TASK_OVERVIEW_MATCH_KEY).toString()));
        }

        if (redisUtils.hHasKey(key, RemoteDiagnosticConstant.TASK_OVERVIEW_CANCEL_KEY)) {
            entity.setCancelNum(Long.parseLong(redisUtils.hget(key, RemoteDiagnosticConstant.TASK_OVERVIEW_CANCEL_KEY).toString()));
        }

        if (redisUtils.hHasKey(key, RemoteDiagnosticConstant.TASK_OVERVIEW_REJECT_KEY)) {
            entity.setRejectNum(Long.parseLong(redisUtils.hget(key, RemoteDiagnosticConstant.TASK_OVERVIEW_REJECT_KEY).toString()));
        }

        if (redisUtils.hHasKey(key, RemoteDiagnosticConstant.TASK_OVERVIEW_ACCEPTANCE_KEY)) {
            entity.setAcceptanceNum(Long.parseLong(redisUtils.hget(key, RemoteDiagnosticConstant.TASK_OVERVIEW_ACCEPTANCE_KEY).toString()));
        }

        if (redisUtils.hHasKey(key, RemoteDiagnosticConstant.TASK_OVERVIEW_DOWNLOAD_KEY)) {
            entity.setDownloadNum(Long.parseLong(redisUtils.hget(key, RemoteDiagnosticConstant.TASK_OVERVIEW_DOWNLOAD_KEY).toString()));
        }

        if (redisUtils.hHasKey(key, RemoteDiagnosticConstant.TASK_OVERVIEW_SUCCESS_KEY)) {
            entity.setSuccessNum(Long.parseLong(redisUtils.hget(key, RemoteDiagnosticConstant.TASK_OVERVIEW_SUCCESS_KEY).toString()));
        }

        if (redisUtils.hHasKey(key, RemoteDiagnosticConstant.TASK_OVERVIEW_FAIL_KEY)) {
            entity.setFailNum(Long.parseLong(redisUtils.hget(key, RemoteDiagnosticConstant.TASK_OVERVIEW_FAIL_KEY).toString()));
        }
    }

}
