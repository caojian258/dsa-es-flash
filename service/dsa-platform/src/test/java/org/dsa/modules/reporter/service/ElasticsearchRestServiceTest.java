package org.dsa.modules.reporter.service;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSONObject;
import junit.framework.TestCase;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.reporter.config.ReportEsConfig;
import org.dsa.modules.reporter.constant.RedisConstant;
import org.dsa.modules.reporter.dao.DtcDailyMapper;
import org.dsa.modules.reporter.document.DiagnosticDailyDoc;
import org.dsa.modules.reporter.document.SessionDoc;
import org.dsa.modules.reporter.dto.SessionDto;
import org.dsa.modules.reporter.entity.CtsDaily;
import org.dsa.modules.reporter.entity.DtcDaily;
import org.dsa.modules.reporter.handler.SessionLogHandler;
import org.dsa.modules.reporter.util.FilesUtil;
import org.dsa.modules.reporter.util.JsonReader;
import org.dsa.modules.reporter.util.SnowflakeIdWorker;
import org.dsa.modules.reporter.vo.diag.DtcChartReVo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.ResourceUtils;

import jakarta.annotation.Resource;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
public class ElasticsearchRestServiceTest extends TestCase {

    @Autowired
    ElasticsearchRestService esService;

    @Autowired
    ReportEsConfig esConfig;

    @Autowired
    DiagnosticEsService diagnosticEsService;

    @Autowired
    DtcDailyMapper dtcDailyMapper;

    @Autowired
    DtcDailyService dtcDailyService;

    @Autowired
    CtsDailyService ctsDailyService;


    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private SessionLogHandler sessionLogHandler;

    @Autowired
    private DiagnosticStaticService diagnosticStaticService;

    @Test
    public void vinAggs(){
        String day = "2023-08-04";
        List<Map<String,Object>> ms =  diagnosticEsService.vinDailyAggregations(day);
        System.out.println(JSONObject.toJSONString(ms));
    }

    @Test
    public void dailyReportTest(){
        String tag  = "1691490329742";
        diagnosticStaticService.ctsDaily(tag);
        diagnosticStaticService.dtcDaily(tag);
    }

    @Test
    public void insertESData() throws SQLException, ClassNotFoundException {

        String path = "D:\\dsa-product\\sqlite\\VIN_L1234567890123456_76c87dae-53d4-4cc9-9011-9e2560ac98dd.sqlite";
        String name = "VIN_L1234567890123456_76c87dae-53d4-4cc9-9011-9e2560ac98dd.sqlite";
        String tag = FilesUtil.makeTag();

        SessionDto dto = sessionLogHandler.saveData(path, tag, name, "abc");
        System.out.println(JSONObject.toJSONString(dto));
        redisUtils.lSet(RedisConstant.TAG_DIAG_KEY, tag);

    }

    @Test
    public void clearDocBySessionId() throws IOException {
        String sessionId = "4770c18b-a132-4f1b-8752-5bd3229dc1d2";
        String year = "2023";
        //删除单个文档
        String id = "1139231457948991488";
//        esService.deleteDocById("dsa_did-2023", id);
        Map<String, Object> map = new HashMap<>();
        map.put("sessionInfoId", "1138538493367025664");
        map.put("vin", "L1234567890123456");
        //根据条件删除文档
        List<String> is = new ArrayList<>();
        is.add("dsa_did-2023");
        is.add("dsa_dtc-2023");

//        esService.deleteDocByQuery(is,  map);

        //删除整个会话文档
        //更新cts_daily dtc_daily统计

    }

    @Test
    public void hashsetTest(){
        String key = String.format(RedisConstant.CTS_DAILY_KEY, "2023-08-07");
        //source:vin:cts:day
        String item = String.format(RedisConstant.CTS_DAILY_ITEM, "003", "L1234567890123456", "identification", "2023-08-07");
        Integer value = 10;
//        redisUtils.hset(key, item, value+"");
//        redisUtils.hdel(key,item);
//        redisUtils.hincr(key,item, value);
//

        Map<Object, Object> ctsMap = redisUtils.hmget(key);

        Iterator entInfo = ctsMap.entrySet().iterator();
        while (entInfo.hasNext()){
            Map.Entry entry = (Map.Entry) entInfo.next();
            String key1 = entry.getKey().toString();
            String value1 = entry.getValue().toString();
            System.out.println(key1);
            String[] result = key1.split(":");
            System.out.println(JSONObject.toJSONString(result));
            System.out.println(value1);
        }
        redisUtils.delete(key);

    }

    @Test
    public void testAddDoc() throws IOException {
        String path = "C:\\Users\\jian8\\Documents\\cts_daily.json";

        JsonReader reader = new JsonReader(path);
        String content = reader.getJsonString();

        String index = "dsa_diagnostic_daily";

        List<DiagnosticDailyDoc> items = new ArrayList<>();

        items = JSONObject.parseArray(content, DiagnosticDailyDoc.class);
        SnowflakeIdWorker idWorker = new SnowflakeIdWorker(0, 0);

        for (DiagnosticDailyDoc doc: items) {
            doc.setId(idWorker.nextId());
            Map<String, Object> map = BeanUtil.beanToMap(doc);
            log.info("{}", map);
            esService.addIndexDoc(index, map);
        }


//        esService.bulkAddIndexDoc(index, items);

    }

    @Test
    public void testDeleteIndex() throws IOException {

        esService.deleteIndex("index_api_invoked_time" );

    }

    //通过sessionId删除某个诊断会话
    @Test
    public void testDeleteBySessionId(){
        String sessionInfoId = "1139491735777312768";
        String day = "";
        String sessionId = "4770c18b-a132-4f1b-8752-5bd3229dc1d2";
        SessionDoc doc = diagnosticEsService.querySessionBySessionId(sessionId, day);
//        if(null != doc && null != doc.getCreateTime()){

//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//            day = sdf.format(doc.getCreateTime());

            diagnosticEsService.deleteSessionDoc(sessionInfoId, day);
//        }
        System.out.println(JSONObject.toJSONString(doc));
//        diagnosticEsService.deleteSessionDoc(doc.getId(), day);
    }

    //统计更新当天的数据
    @Test
    public void testDailyByVin(){
        String day = "2023-08-04";
        String vin = "LM017801230000099";
        Long vehicleTypeId = 4L;

        List<DtcDaily> dtcDailyList =  diagnosticEsService.dtcDailyByVinDay(vin, day);
        System.out.println(JSONObject.toJSONString(dtcDailyList));
//        for (DtcDaily dtc: dtcDailyList){
//            dtcDailyService.upsertData(dtc);
//        }

        List<CtsDaily> ctsDailyList =  diagnosticEsService.ctsDailyByVinDay(vin, day);
        System.out.println(JSONObject.toJSONString(ctsDailyList));
        for (CtsDaily cts: ctsDailyList){
            cts.setVehicleTypeId(vehicleTypeId);
            ctsDailyService.upsertData(cts);
        }

    }
    @Test
    public void testCtsDaily(){
        CtsDaily ctsDaily = new CtsDaily();
        ctsDaily.setSource("002");
        ctsDaily.setCtsName("DTC-read");
        ctsDaily.setFunctionCode("DTC");
        ctsDaily.setActionCode("read");
        ctsDaily.setCtsDay("2023-06-20");
        ctsDaily.setVin("L1234567890123456");
        ctsDaily.setVehicleTypeId(1007L);
        ctsDaily.setCtsNum(5L);
        ctsDailyService.insertData(ctsDaily);
    }
    //从es dtc 文档里面拿数据 放到统计表中
    @Test
    public void testDtcDaily(){
        DtcChartReVo vo = new DtcChartReVo();

//        List<String> dates = new ArrayList<>();
//        dates.add("2023-06-01");
//        dates.add("2023-08-01");
//        vo.setDates(dates);
//        vo.setTop(100);
//        vo.setPageNo(1);
//        vo.setPageSize(1000);

//        List<CtsDaily> maps =  diagnosticEsService.ctsDailyByVinDay("LJ1E6A2U4K7708977", "2023-06-25");
//        System.out.println(JSONObject.toJSONString(maps));
//        for (CtsDaily cts: maps){
//            cts.setVehicleTypeId(1012L);
//            ctsDailyService.insertData(cts);
//        }

        List<DtcDaily> maps =  diagnosticEsService.dtcDailyByVinDay("LJ1E6A2U4K7708977", "2023-06-25");
        System.out.println(JSONObject.toJSONString(maps));
        for (DtcDaily dtc: maps){
            dtcDailyService.insertData(dtc);
        }
//        for (Map<String , Object> map: maps) {
//            DtcDaily dtcDaily = new DtcDaily();
//            dtcDaily.setVin("L1234567890123456");
//            dtcDaily.setVehicleTypeId(1007L);
//            dtcDaily.setDtcCode(map.get("subGroupName2").toString());
//            dtcDaily.setDtcDay(map.get("key").toString());
//            dtcDaily.setEcuName(map.get("subGroupName1").toString());
//            dtcDaily.setDtcNum(Long.valueOf(map.get("groupCount").toString()));
//            dtcDaily.setCreatedAt(new Date());
//            dtcDaily.setUpdatedAt(new Date());
//            dtcDaily.setTi("TI");

//        }
    }
}