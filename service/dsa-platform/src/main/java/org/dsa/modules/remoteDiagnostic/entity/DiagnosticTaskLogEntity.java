package org.dsa.modules.remoteDiagnostic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;
import org.dsa.modules.remoteDiagnostic.pojo.BaseEntity;

import java.io.Serializable;

@Data
@ToString
@TableName("r_diag_task_log")
public class DiagnosticTaskLogEntity extends BaseEntity implements Serializable {

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long campaignId;

    private String sessionId;

    private String md5;

    private String fileName;

    private String filePath;

    private Long fileLength;

    private String vin;

    private Integer status;

}
