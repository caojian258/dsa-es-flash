package org.dsa.modules.onlineUpdate.utils;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.dsa.common.utils.ShiroUtils;
import org.dsa.modules.onlineUpdate.dao.VersionRecordDao;
import org.dsa.modules.onlineUpdate.entity.VersionRecordEntity;
import org.dsa.modules.sys.entity.DisplayUserEntity;
import org.dsa.modules.sys.utils.SpringContextHolder;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class VersionRecordUtils {

    /**
     * @desc 保存版本操作记录
     * @param versionUuId 版本uuid
     * @param editType 操作类型
     * @param status 版本状态
     */
    public static void saveOperateRecord(String versionUuId, String editType, Integer status) {
        VersionRecordDao versionRecordDao = SpringContextHolder.getBean(VersionRecordDao.class);
        DisplayUserEntity userEntity = ShiroUtils.getUserEntity();
        VersionRecordEntity record = VersionRecordEntity.builder()
                .versionUuid(versionUuId)
                .status(status)
                .editType(editType)
                .createdAt(new Date())
                .createdUserId(Long.valueOf(userEntity.getUserInfo().getUserId()))
                .createdUserName(userEntity.getUserInfo().getUserDisplayName())
                .build();

        versionRecordDao.insert(record);
    }

    /**
     * 根据版本Id查询操作记录
     */
    public static List<VersionRecordEntity> queryRecordByUUID(String versionUuId) {
        VersionRecordDao versionRecordDao = SpringContextHolder.getBean(VersionRecordDao.class);
        LambdaQueryWrapper<VersionRecordEntity> queryWrapper = Wrappers.<VersionRecordEntity>lambdaQuery()
                .eq(VersionRecordEntity::getVersionUuid, versionUuId)
                .orderByDesc(VersionRecordEntity::getCreatedAt);
        return versionRecordDao.selectList(queryWrapper);
    }



}
