package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.dto.EcuVersionHistoryDto;
import org.dsa.modules.reporter.dto.FlashEcuChartBarDto;
import org.dsa.modules.reporter.dto.FlashEcuChartPiDto;
import org.dsa.modules.reporter.entity.VehicleFlashDetail;
import org.dsa.modules.reporter.vo.flash.FlashReqVo;

import java.sql.Timestamp;
import java.util.Date;

import java.util.List;
import java.util.Map;

@Mapper
public interface VehicleFlashDetailMapper extends BaseMapper<VehicleFlashDetail> {

    Map<String, Object> findNumResult(Integer flashId);

    List<VehicleFlashDetail> findByTimeAndFlashID(Integer flashId);

//    List<VehicleFlashDetail> findByVersionAndVin(@Param("flashId") Integer flashId,@Param("vin") String vin,@Param("version") String version,@Param("collectTime") String collectTime,@Param("ent") String ent,@Param("ecuName") String ecuName);

//    List<VehicleFlashDetail> findByVersionAndVinGroupName(@Param("flashId") Integer flashId,@Param("vin") String vin,@Param("version") String version,@Param("collectTime") String collectTime,@Param("ent") String ent);

    List<VehicleFlashDetail> findByVersionAndVin(@Param("flashId") Integer flashId,@Param("vin") String vin,@Param("version") String version,@Param("ecuName") String ecuName);
//
    List<VehicleFlashDetail> findByVersionAndVinGroupName(@Param("flashId") Integer flashId,@Param("vin") String vin,@Param("version") String version);

    Page<VehicleFlashDetail> findEcuName(Page<VehicleFlashDetail> page,@Param("vin") String vin,@Param("ecuName") String ecuName,@Param("vehicleTypeIds") List<Integer> vehicleTypeIds, @Param("startDate") String startDate, @Param("endDate") String endDate);

    List<VehicleFlashDetail> findByEcuAndVin(@Param("vin") String vin,@Param("version") String version, @Param("ecuName") String ecuName, @Param("startDate") String startDate, @Param("endDate") String endDate);

    List<VehicleFlashDetail> findEcuName(@Param("vin") String vin,@Param("ecuName") String ecuName);

    List<VehicleFlashDetail> findEcuHisName(@Param("vin") String vin);

    List<VehicleFlashDetail> findEcuHisVersion(@Param("vin") String vin,@Param("collectTime") String collectTime,@Param("ecuName") String ecuName,@Param("version") String version);

    List<String> getEcuNames();

    List<EcuVersionHistoryDto> findVehicleEcuVersion(@Param("vin") String vin, @Param("tag") String tag);

    List<FlashEcuChartPiDto> flashEcuChartPi(@Param("startDate") String startDate,@Param("endDate") String endDate, @Param("vin") String vin,@Param("vehicleTypeIds") List<Long> vehicleType);

    List<FlashEcuChartBarDto> flashEcuChartBar(@Param("startDate") String startDate, @Param("endDate") String endDate, @Param("vin") String vin, @Param("vehicleTypeIds") List<Long> vehicleType);


}
