package org.dsa.modules.reporter.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class DiagnosticSessionCtsDto implements Serializable {

    private String vinCode;

    private String workshop;

    private String pcid;

    private String sessionId;

    private String functionCode;

    private String actionCode;

    private String titleEn;

    private String titleCn;

    private Date startTime;

    private Date endTime;
}
