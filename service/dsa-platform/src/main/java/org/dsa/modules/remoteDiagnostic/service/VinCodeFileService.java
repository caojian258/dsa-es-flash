package org.dsa.modules.remoteDiagnostic.service;

import org.dsa.modules.remoteDiagnostic.vo.VinCodePageVo;

import java.io.File;
import java.util.List;

public interface VinCodeFileService {

    void readExcel(File file,String key);

    Boolean clearKey(String key);

    List<Object> getDataByKey(VinCodePageVo vo);
}
