package org.dsa.modules.sys.service;


import com.baomidou.mybatisplus.extension.service.IService;
import org.dsa.modules.sys.entity.SysMenuEntity;

import java.util.List;


/**
 * 菜单管理
 *
 */
public interface SysMenuService extends IService<SysMenuEntity> {

	/**
	 * 根据父菜单，查询子菜单
	 * @param parentId 父菜单ID
	 * @param menuIdList  用户菜单ID
	 */
	List<SysMenuEntity> queryListParentId(Long parentId, List<Long> menuIdList);

	/**
	 * 根据父菜单，查询子菜单
	 * @param parentId 父菜单ID
	 */
	List<SysMenuEntity> queryListParentId(Long parentId);
	
	/**
	 * 获取不包含按钮的菜单列表
	 */
	List<SysMenuEntity> queryNotButtonList();
	
	/**
	 * 获取用户菜单列表
	 */
	List<SysMenuEntity> getUserMenuList(Long userId);

	/**
	 * 删除
	 */
	void delete(Long menuId);

	/**
	 * 根据角色查询拥有的菜单树
	 */
    List<SysMenuEntity> getUserMenuListByRoleList(List<String> roleList, Integer owningSystem);

	/**
	 * 根据角色查询拥有的诊断菜单树
	 */
    List<SysMenuEntity> getDiagnosticUserMenuListByRoleList(List<String> roleList, String vin);

	/**
	 * 客户端菜单列表
	 *
	 * @param roleList roleList
	 * @return List<SysMenuEntity>
	 */
	List<String> clientMenus(List<String> roleList);
}
