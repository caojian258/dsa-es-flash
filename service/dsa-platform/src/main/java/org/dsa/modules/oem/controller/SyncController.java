package org.dsa.modules.oem.controller;

import org.dsa.common.utils.R;
import org.dsa.modules.oem.dto.SyncVehicleInfoDTO;
import org.dsa.modules.oem.service.SyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author weishunxin
 * @since 2023-05-30
 */
@RequestMapping("/oem/sync")
@RestController
public class SyncController {

    @Autowired
    private SyncService syncService;

    @PostMapping("/vehicle-info")
    public R syncVehicleInfo(@RequestBody List<SyncVehicleInfoDTO> vehicleList) {
        syncService.syncVehicleInfo(vehicleList);
        return R.ok();
    }

}
