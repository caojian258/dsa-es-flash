package org.dsa.modules.diagnostic.dto;

import lombok.Data;

import java.util.Date;

@Data
public class SessionActionDto {

    private String workshop;

    private String pcid;

    private String username;

    private String vin;

    private String sessionId;

    private String functionCode;

    private String actionCode;

    private String actionId;

    private Date startTime;

    private Date endTime;

    private String results;

}
