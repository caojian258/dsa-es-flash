package org.dsa.modules.reporter.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.dsa.modules.reporter.constant.ConfigConstant;
import org.dsa.modules.reporter.document.*;
import org.dsa.modules.reporter.dto.VehicleReportDto;
import org.dsa.modules.reporter.entity.CtsDaily;
import org.dsa.modules.reporter.entity.DtcDaily;
import org.dsa.modules.reporter.enums.EsIndexEnum;
import org.dsa.modules.reporter.service.DiagnosticEsService;
import org.dsa.modules.reporter.service.ElasticsearchRestService;
import org.dsa.modules.reporter.vo.VehicleReportVo;
import org.dsa.modules.reporter.vo.diag.DiagnosticPageReVo;
import org.dsa.modules.reporter.vo.diag.DtcChartReVo;
import org.dsa.modules.reporter.vo.diag.DtcPageReVo;
import org.dsa.modules.reporter.vo.diag.SessionPageReVo;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.*;
import org.elasticsearch.script.Script;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.*;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramInterval;
import org.elasticsearch.search.aggregations.bucket.histogram.ExtendedBounds;
import org.elasticsearch.search.aggregations.bucket.histogram.Histogram;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.Cardinality;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;


@Slf4j
@Service
public class DiagnosticEsServiceImpl implements DiagnosticEsService {

    @Autowired
    private ElasticsearchRestService restService;

    private final static String DEFAULT_ORDER_BY = "createTime";
    private final static String DEFAULT_ORDER = "desc";
    private final static String DEFAULT_UNMAPPED_TYPE = "date";

    private final static Integer TERM_AGG_MAX = 100000;


    @Override
    public Map<String, Object> sessionPage(SessionPageReVo vo) {

        String index = restService.formatIndex(EsIndexEnum.SESSION.getValue(), vo.getDates());

        SearchRequest searchRequest = new SearchRequest(index);
        searchRequest.types("_doc");
        searchRequest.indices(index);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        Integer from = (vo.getPageNo()-1) * vo.getPageSize();
        Integer size = vo.getPageSize();
        searchSourceBuilder.from(from);
        searchSourceBuilder.size(size);
        String orderBy = DEFAULT_ORDER_BY;
        String order = DEFAULT_ORDER;
        String unmappedType = DEFAULT_UNMAPPED_TYPE;

        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();

        if(StrUtil.isNotBlank(vo.getVin())){
            if(vo.getVin().length() == 17){
                boolQueryBuilder.must(QueryBuilders.termQuery("vin", vo.getVin()));
            }else{
                WildcardQueryBuilder wildcardQueryBuilder = QueryBuilders.wildcardQuery("vin", "*"+vo.getVin()+"*");
                boolQueryBuilder.must(wildcardQueryBuilder);
            }
        }

        if(null != vo.getUserId() && vo.getUserId()>0){
            boolQueryBuilder.must(QueryBuilders.termQuery("userId", vo.getUserId()));
        }

        if(null != vo.getDiagCategory()){
            boolQueryBuilder.must(QueryBuilders.termQuery("diagCategory", vo.getDiagCategory()));
        }
        if(StrUtil.isNotBlank(vo.getDiagSource())){
            boolQueryBuilder.must(QueryBuilders.termQuery("diagSource", vo.getDiagSource()));
        }
        if(CollectionUtil.isNotEmpty(vo.getDates()) && vo.getDates().size() == 2){
            String startTime = vo.getDates().get(0);
            String endTime = vo.getDates().get(1);
            boolQueryBuilder.must(QueryBuilders.rangeQuery("createTime").gte(startTime+" 00:00:00").lte(endTime+ " 23:59:59"));
        }

        //至少匹配一个
        if(CollectionUtil.isNotEmpty(vo.getVehType())){
            TermsQueryBuilder vehTypeQuery = QueryBuilders.termsQuery("vehicleTypeId",  vo.getVehType());
            boolQueryBuilder.should(vehTypeQuery);
            boolQueryBuilder.minimumShouldMatch(1);
        }
        searchSourceBuilder.query(boolQueryBuilder);

        searchSourceBuilder.sort(new FieldSortBuilder(orderBy).order(SortOrder.fromString(order)).unmappedType(unmappedType));
        searchSourceBuilder.fetchSource(true);
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        searchRequest.source(searchSourceBuilder);
        searchRequest.indices(index);
        Map<String, Object> searchMap = restService.search(searchRequest, from, size);

        return searchMap;
    }

    @Override
    public List<SessionDoc> sessionListById(List<String> infoIds, List<String> dates) {
        List<SessionDoc> items = new ArrayList<>();

        String index = restService.formatIndex(EsIndexEnum.SESSION.getValue(), dates);
        SearchRequest searchRequest = new SearchRequest(index);
        searchRequest.types("_doc");
        searchRequest.indices(index);

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        String orderBy = DEFAULT_ORDER_BY;
        String order = DEFAULT_ORDER;
        String unmappedType = DEFAULT_UNMAPPED_TYPE;

        searchSourceBuilder.sort(new FieldSortBuilder(orderBy).order(SortOrder.fromString(order)).unmappedType(unmappedType));
        searchSourceBuilder.fetchSource(true);
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        searchSourceBuilder.query(QueryBuilders.termsQuery("id", infoIds));
        searchRequest.source(searchSourceBuilder);
        searchRequest.indices(index);

        log.info("es sessionListById request: {}", searchRequest.source().toString());

        try {
            SearchResponse response = null;
            response = restService.getClient().search(searchRequest, RequestOptions.DEFAULT);

            for (SearchHit his: response.getHits().getHits()) {
                SessionDoc doc = new SessionDoc();
                BeanUtil.fillBeanWithMap(his.getSourceAsMap(), doc, true);
                items.add(doc);
            }
        } catch (IOException e) {
            log.error("es search error: {}", e.getMessage());
        }

        return items;
    }

    @Override
    public Map<String, Object> diagnosticPage(DiagnosticPageReVo vo) {

        String index = restService.formatIndex(EsIndexEnum.DIAG.getValue(), vo.getDates());

        SearchRequest searchRequest = new SearchRequest(index);
        searchRequest.types("_doc");
        searchRequest.indices(index);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        Integer from = (vo.getPageNo()-1) * vo.getPageSize();
        Integer size = vo.getPageSize();
        searchSourceBuilder.from(from);
        searchSourceBuilder.size(size);

        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();

        if(null != vo.getUserId() && vo.getUserId()>0){
            boolQueryBuilder.must(QueryBuilders.termQuery("userId", vo.getUserId()));
        }
        if(StrUtil.isNotBlank(vo.getEcu())){
            boolQueryBuilder.must(QueryBuilders.termQuery("ecuName", vo.getEcu()));
        }
        if(StrUtil.isNotBlank(vo.getCtsName())){
            String[] ctsNames = vo.getCtsName().split("-");
            if(ctsNames.length == 2){
                boolQueryBuilder.must(QueryBuilders.termQuery("functionCode", ctsNames[0]));
                boolQueryBuilder.must(QueryBuilders.termQuery("action", ctsNames[1]));
            }else{
                boolQueryBuilder.must(QueryBuilders.termQuery("functionCode", ctsNames[0]));
            }
        }

        if(StrUtil.isNotBlank(vo.getVin())){
            if(vo.getVin().length() == 17){
                boolQueryBuilder.must(QueryBuilders.termQuery("vin", vo.getVin()));
            }else{
                WildcardQueryBuilder wildcardQueryBuilder = QueryBuilders.wildcardQuery("vin", "*"+vo.getVin()+"*");
                boolQueryBuilder.must(wildcardQueryBuilder);
            }
        }

        if(CollectionUtil.isNotEmpty(vo.getDates()) && vo.getDates().size() == 2){
            String startTime = vo.getDates().get(0);
            String endTime = vo.getDates().get(1);
            boolQueryBuilder.must(QueryBuilders.rangeQuery("createTime").gte(startTime+" 00:00:00").lte(endTime+ " 23:59:59"));
        }

        searchSourceBuilder.query(boolQueryBuilder);

        String orderBy = DEFAULT_ORDER_BY;
        String order = DEFAULT_ORDER;
        String unmappedType = DEFAULT_UNMAPPED_TYPE;

        searchSourceBuilder.sort(new FieldSortBuilder(orderBy).order(SortOrder.fromString(order)).unmappedType(unmappedType));
        searchSourceBuilder.fetchSource(true);
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        searchRequest.source(searchSourceBuilder);
        searchRequest.indices(index);

        Map<String, Object> searchMap = restService.search(searchRequest, from, size);
        return searchMap;
    }

    @Override
    public List<Map<String, Object>> diagnosticDaily(DiagnosticPageReVo vo) {
        //这里要考虑分页问题

        List<Map<String, Object>> items = new ArrayList<>();

        Script script = new Script("doc['functionCode'].value +'-'+ doc['action'].value");
        String index = restService.formatIndex(EsIndexEnum.DIAG.getValue(), vo.getDates());
        String format = "yyyy-MM-dd";
        String aggName = "ctsAggs";
        String subAggName = "perCtsCount";
        String pageAggName = "page";

        List<String> dates = vo.getDates();

        TermsAggregationBuilder perCtsCount = AggregationBuilders.terms(subAggName).script(script).size(TERM_AGG_MAX);
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.mustNot(QueryBuilders.termQuery("functionCode", ""));
        if(StrUtil.isNotBlank(vo.getVin())){
            if(vo.getVin().length() == 17){
                boolQueryBuilder.must(QueryBuilders.termQuery("vin", vo.getVin()));
            }else{
                PrefixQueryBuilder vinQuery = QueryBuilders.prefixQuery("vin", vo.getVin());
                boolQueryBuilder.must(vinQuery);
            }
        }
        if(StrUtil.isNotBlank(vo.getEcu())){
            boolQueryBuilder.must(QueryBuilders.termQuery("ecuName", vo.getEcu()));
        }

        if(StrUtil.isNotBlank(vo.getCtsName()) ){
            String[] ctsNames = vo.getCtsName().split("-");
            if(ctsNames.length == 2){
                boolQueryBuilder.must(QueryBuilders.termQuery("functionCode", ctsNames[0]));
                boolQueryBuilder.must(QueryBuilders.termQuery("action", ctsNames[1]));
            }else{
                boolQueryBuilder.must(QueryBuilders.termQuery("functionCode", ctsNames[0]));
            }
        }

        if(CollectionUtil.isNotEmpty(vo.getDates()) && vo.getDates().size() == 2){
            String startTime = vo.getDates().get(0);
            String endTime = vo.getDates().get(1);
            boolQueryBuilder.must(QueryBuilders.rangeQuery("createTime").gte(startTime+" 00:00:00").lte(endTime+ " 23:59:59"));
        }

        List<FieldSortBuilder> sortList = new ArrayList<>();
        Integer from = (vo.getPageNo()-1)*vo.getPageSize();

        AggregationBuilder aggregationBuilder = AggregationBuilders.dateHistogram(aggName)//自定义名称
                .dateHistogramInterval(DateHistogramInterval.DAY)//设置间隔
                .minDocCount(1)//返回空桶
                .field("createTime")//指定时间字段
                .format(format)//设定返回格式
                .extendedBounds(new ExtendedBounds(dates.get(0), dates.get(1))).//设定范围
                subAggregation(perCtsCount)
                .subAggregation(PipelineAggregatorBuilders.bucketSort(pageAggName,sortList).from(from).size(vo.getPageSize()));

        try {
            items = restService.groupDateHistogramAggregation(index,
                     aggName,
                     subAggName,
                     boolQueryBuilder,
                    aggregationBuilder);
        }catch (Exception ex){
            log.error("cts diagnosticDaily: {}", ex.getMessage());
        }

        return items;
    }

    @Override
    public List<Map<String, Object>> diagnosticPi(DiagnosticPageReVo vo) {
        String index = restService.formatIndex(EsIndexEnum.DIAG.getValue(), vo.getDates());

        String aggName = "ctsCountAggs";

        List<Map<String, Object>> items = new ArrayList<>();

        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.mustNot(QueryBuilders.termQuery("functionCode", ""));
        if(StrUtil.isNotBlank(vo.getVin())){
            if(vo.getVin().length() == 17){
                boolQueryBuilder.filter(QueryBuilders.termQuery("vin", vo.getVin()));
            }else{
                PrefixQueryBuilder vinQuery = QueryBuilders.prefixQuery("vin", vo.getVin());
                boolQueryBuilder.filter(vinQuery);
            }
        }
        if(CollectionUtil.isNotEmpty(vo.getSources())){
            TermsQueryBuilder sourceQuery = QueryBuilders.termsQuery("diagSource", vo.getSources());
            boolQueryBuilder.filter(sourceQuery);

        }

        if(CollectionUtil.isNotEmpty(vo.getVehicleTypeIds())){
            TermsQueryBuilder vehTypeQuery = QueryBuilders.termsQuery("vehicleTypeId",  vo.getVehicleTypeIds());
            boolQueryBuilder.should(vehTypeQuery);
            boolQueryBuilder.minimumShouldMatch(1);
        }
        if(StrUtil.isNotBlank(vo.getEcu())){
            boolQueryBuilder.filter(QueryBuilders.termQuery("ecuName", vo.getEcu()));
        }
        if(StrUtil.isNotBlank(vo.getCtsName()) ){
            String[] ctsNames = vo.getCtsName().split("-");
            if(ctsNames.length == 2){
                boolQueryBuilder.filter(QueryBuilders.termQuery("functionCode", ctsNames[0]));
                boolQueryBuilder.filter(QueryBuilders.termQuery("action", ctsNames[1]));
            }else{
                boolQueryBuilder.filter(QueryBuilders.termQuery("functionCode", ctsNames[0]));
            }
        }

        if(CollectionUtil.isNotEmpty(vo.getDates()) && vo.getDates().size() == 2){
            String startTime = vo.getDates().get(0);
            String endTime = vo.getDates().get(1);
            boolQueryBuilder.must(QueryBuilders.rangeQuery("createTime").gte(startTime+" 00:00:00").lte(endTime+ " 23:59:59"));
        }

        Script script = new Script("doc['functionCode'].value +'-'+ doc['action'].value");
        TermsAggregationBuilder perCtsCount = AggregationBuilders.terms(aggName)
                .script(script)
                .size(vo.getTop());

        try {
            items =  restService.termsAggregation(index, aggName, boolQueryBuilder, perCtsCount);
        }catch (Exception ex){
            log.error("cts diagnostic pi: {}", ex.getMessage());
        }
        return items;
    }

    @Override
    public List<Map<String, Object>> diagnosticMonthBar(DiagnosticPageReVo vo) {

        String index = restService.formatIndex(EsIndexEnum.DIAG.getValue(), vo.getDates());
        String aggName = "ctsMonthAggs";
        List<Map<String, Object>> items = new ArrayList<>();

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.mustNot(QueryBuilders.termQuery("functionCode", ""));
        if(StrUtil.isNotBlank(vo.getVin())){
            if(vo.getVin().length() == 17){
                boolQueryBuilder.must(QueryBuilders.termQuery("vin", vo.getVin()));
            }else{
                PrefixQueryBuilder vinQuery = QueryBuilders.prefixQuery("vin", vo.getVin());
                boolQueryBuilder.must(vinQuery);
            }
        }
        if(StrUtil.isNotBlank(vo.getEcu())){
            boolQueryBuilder.filter(QueryBuilders.termQuery("ecuName", vo.getEcu()));
        }
        if(StrUtil.isNotBlank(vo.getCtsName()) ){
            String[] ctsNames = vo.getCtsName().split("-");
            if(ctsNames.length == 2){
                boolQueryBuilder.filter(QueryBuilders.termQuery("functionCode", ctsNames[0]));
                boolQueryBuilder.filter(QueryBuilders.termQuery("action", ctsNames[1]));
            }else{
                boolQueryBuilder.filter(QueryBuilders.termQuery("functionCode", ctsNames[0]));
            }
        }

        if(CollectionUtil.isNotEmpty(vo.getDates()) && vo.getDates().size() == 2){
            String startTime = vo.getDates().get(0);
            String endTime = vo.getDates().get(1);
            boolQueryBuilder.must(QueryBuilders.rangeQuery("createTime").gte(startTime+" 00:00:00").lte(endTime+ " 23:59:59"));
        }

        if(CollectionUtil.isNotEmpty(vo.getSources())){
            TermsQueryBuilder sourceQuery = QueryBuilders.termsQuery("diagSource", vo.getSources());
            boolQueryBuilder.filter(sourceQuery);
        }

        if(CollectionUtil.isNotEmpty(vo.getVehicleTypeIds())){
            TermsQueryBuilder vehTypeQuery = QueryBuilders.termsQuery("vehicleTypeId",  vo.getVehicleTypeIds());
            boolQueryBuilder.should(vehTypeQuery);
            boolQueryBuilder.minimumShouldMatch(1);
        }

        List<String> dates = vo.getDates();
        searchSourceBuilder.query(boolQueryBuilder);
        String format = "yyyy-MM";
        String subAggName = "perCtsCount";
        Script script = new Script("doc['functionCode'].value +'-'+ doc['action'].value");
        TermsAggregationBuilder perCtsCount = AggregationBuilders.terms(subAggName).script(script).size(TERM_AGG_MAX);

        String pageAggName = "page";
        List<FieldSortBuilder> sortList = new ArrayList<>();
        Integer from = (vo.getPageNo()-1)*vo.getPageSize();

        AggregationBuilder aggregationBuilder = AggregationBuilders.dateHistogram(aggName)//自定义名称
                .dateHistogramInterval(DateHistogramInterval.MONTH)//设置间隔
                .minDocCount(1)//返回空桶
                .field("createTime")//指定时间字段
                .format(format)//设定返回格式
                .extendedBounds(new ExtendedBounds(dates.get(0).substring(0, 7), dates.get(1).substring(0, 7))).//设定范围
                        subAggregation(perCtsCount)
                .subAggregation(PipelineAggregatorBuilders.bucketSort(pageAggName,sortList).from(from).size(vo.getPageSize()));

        try {
            items = restService.groupDateHistogramAggregation(index, aggName, subAggName, boolQueryBuilder, aggregationBuilder);
        }catch (Exception ex){
            log.error("cts diagnostic month: {}", ex.getMessage());
        }

        return items;
    }

    @Override
    public Map<String, Object> diagnosticSessionPage(DiagnosticPageReVo vo) {
        Map<String, Object> result  = new HashMap<>();
        result.put("total", 0);
        result.put("totalPage", 0);
        result.put("pageSize", vo.getPageSize());
        result.put("pageNo", vo.getPageNo());

        String index = restService.formatIndex(EsIndexEnum.DIAG.getValue(), vo.getDates());
        List<String> sessionIdList = new ArrayList<>();
        SearchRequest searchRequest = new SearchRequest(index);
        searchRequest.types("_doc");
        searchRequest.indices(index);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        Integer from = (vo.getPageNo()-1) * vo.getPageSize();
        Integer size = vo.getPageSize();
        searchSourceBuilder.from(from);
        searchSourceBuilder.size(size);

        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();

        if(StrUtil.isNotBlank(vo.getVin())){
            if(vo.getVin().length() == 17){
                boolQueryBuilder.must(QueryBuilders.termQuery("vin", vo.getVin()));
            }else{
                PrefixQueryBuilder vinQuery = QueryBuilders.prefixQuery("vin", vo.getVin());
                boolQueryBuilder.must(vinQuery);
            }
        }

        if(CollectionUtil.isNotEmpty(vo.getDates()) && vo.getDates().size() == 2){
            String startTime = vo.getDates().get(0);
            String endTime = vo.getDates().get(1);
            boolQueryBuilder.must(QueryBuilders.rangeQuery("createTime").gte(startTime+" 00:00:00").lte(endTime+ " 23:59:59"));
        }

        if(CollectionUtil.isNotEmpty(vo.getVehicleTypeIds())){
            TermsQueryBuilder vehTypeQuery = QueryBuilders.termsQuery("vehicleTypeId",  vo.getVehicleTypeIds());
            boolQueryBuilder.should(vehTypeQuery);
            boolQueryBuilder.minimumShouldMatch(1);
        }

        if(StrUtil.isNotEmpty(vo.getEcu())){
            TermsQueryBuilder ecuQuery = QueryBuilders.termsQuery("ecuName",  vo.getEcu());
            boolQueryBuilder.must(ecuQuery);
        }

        //
        if(StrUtil.isNotBlank(vo.getCtsName()) ){
            String[] ctsNames = vo.getCtsName().split("-");
            if(ctsNames.length == 2){
                boolQueryBuilder.must(QueryBuilders.termQuery("functionCode", ctsNames[0]));
                boolQueryBuilder.must(QueryBuilders.termQuery("action", ctsNames[1]));
            }else{
                boolQueryBuilder.must(QueryBuilders.termQuery("functionCode", ctsNames[0]));
            }
        }
        searchSourceBuilder.query(boolQueryBuilder);
        String aggName = "diagSession";
        String pageAggName = "bucketSort";
        String aggCount = "diagCount";

        BucketOrder order = BucketOrder.aggregation("_key", false);
        AggregationBuilder aggregationBuilder = AggregationBuilders.terms(aggName)
                .minDocCount(1)
                .field("sessionInfoId")
                .order(order)
                .size(TERM_AGG_MAX);

        List<FieldSortBuilder> sortList = new ArrayList<>();
        aggregationBuilder.subAggregation(PipelineAggregatorBuilders.bucketSort(pageAggName,sortList).from(from).size(vo.getPageSize()));
        //CARDINALITY_PRECISION_THRESHOLD 大数据量情况下 要看情况设置精度
        AggregationBuilder aggCountBuilder = AggregationBuilders.cardinality(aggCount).field("sessionInfoId");

        searchSourceBuilder.aggregation(aggregationBuilder).size(0);
        searchSourceBuilder.aggregation(aggCountBuilder);
        searchRequest.source(searchSourceBuilder);
        log.info("searchSourceBuilder:{}", searchSourceBuilder.toString());

        try {
            SearchResponse searchResponse = restService.getClient().search(searchRequest, RequestOptions.DEFAULT);
            Terms terms = searchResponse.getAggregations().get(aggName);
            terms.getBuckets().forEach(item ->{
                sessionIdList.add(item.getKeyAsString());
            });
            result.put("ids", sessionIdList);
            Cardinality cardinality =  searchResponse.getAggregations().get(aggCount);
            if(null != cardinality){
                Double totalPage = Math.ceil(Double.valueOf(cardinality.getValue())/size);
                result.put("total", cardinality.getValue());
                result.put("totalPage", (int)Math.round(totalPage));
            }

            log.info(JSONObject.toJSONString(cardinality));

        }catch (Exception ex){
            log.error("terms aggregation error: {}", JSONObject.toJSONString(ex));
        }

        return result;
    }

    @Override
    public Map<String, Object> dtcPage(DtcPageReVo vo) {
        String index = restService.formatIndex(EsIndexEnum.DTC.getValue(), vo.getDates());

        SearchRequest searchRequest = new SearchRequest(index);
        searchRequest.types("_doc");
        searchRequest.indices(index);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        Integer from = (vo.getPageNo()-1) * vo.getPageSize();
        Integer size = vo.getPageSize();
        searchSourceBuilder.from(from);
        searchSourceBuilder.size(size);

        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();

        if(StrUtil.isNotBlank(vo.getVin())){
            if(vo.getVin().length() == 17){
                boolQueryBuilder.must(QueryBuilders.termQuery("vin", vo.getVin()));
            }else{
                PrefixQueryBuilder vinQuery = QueryBuilders.prefixQuery("vin", vo.getVin());
                boolQueryBuilder.must(vinQuery);
            }
        }

        if(StrUtil.isNotBlank(vo.getDtcCode())){
            boolQueryBuilder.must(QueryBuilders.termQuery("dtcCode", vo.getDtcCode()));
        }


        if(CollectionUtil.isNotEmpty(vo.getSource())){
            TermsQueryBuilder  sourceQuery =  QueryBuilders.termsQuery("diagSource", vo.getSource());
            boolQueryBuilder.should(sourceQuery);
            boolQueryBuilder.minimumShouldMatch(1);

        }
        if(CollectionUtil.isNotEmpty(vo.getDates()) && vo.getDates().size() == 2){
            String startTime = vo.getDates().get(0);
            String endTime = vo.getDates().get(1);
            boolQueryBuilder.must(QueryBuilders.rangeQuery("createTime").gte(startTime+" 00:00:00").lte(endTime+ " 23:59:59"));
        }

        if(CollectionUtil.isNotEmpty(vo.getVehType())){
            TermsQueryBuilder vehTypeQuery = QueryBuilders.termsQuery("vehicleTypeId",  vo.getVehType());
            boolQueryBuilder.should(vehTypeQuery);
            boolQueryBuilder.minimumShouldMatch(1);
        }

        if(CollectionUtil.isNotEmpty(vo.getEcuName())){
            TermsQueryBuilder ecuQuery = QueryBuilders.termsQuery("ecuName",  vo.getEcuName());
            boolQueryBuilder.must(ecuQuery);
        }

        searchSourceBuilder.query(boolQueryBuilder);
        String orderBy = DEFAULT_ORDER_BY;
        String order = DEFAULT_ORDER;
        String unmappedType = DEFAULT_UNMAPPED_TYPE;

        searchSourceBuilder.sort(new FieldSortBuilder(orderBy).order(SortOrder.fromString(order)).unmappedType(unmappedType));
        searchSourceBuilder.fetchSource(true);
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        searchRequest.source(searchSourceBuilder);
        searchRequest.indices(index);

        Map<String, Object> searchMap = restService.search(searchRequest, from, size);
        return searchMap;
    }

    @Override
    public Map<String, Object> dtcSessionPage(DtcPageReVo vo) {
        Map<String, Object> result  = new HashMap<>();
        result.put("total", 0);
        result.put("totalPage", 0);
        result.put("pageSize", vo.getPageSize());
        result.put("pageNo", vo.getPageNo());

        String index = restService.formatIndex(EsIndexEnum.DTC.getValue(), vo.getDates());
        List<String> sessionIdList = new ArrayList<>();
        SearchRequest searchRequest = new SearchRequest(index);
        searchRequest.types("_doc");
        searchRequest.indices(index);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        Integer from = (vo.getPageNo()-1) * vo.getPageSize();
        Integer size = vo.getPageSize();
        searchSourceBuilder.from(from);
        searchSourceBuilder.size(size);

        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();

        if(StrUtil.isNotBlank(vo.getVin())){
            if(vo.getVin().length() == 17){
                boolQueryBuilder.must(QueryBuilders.termQuery("vin", vo.getVin()));
            }else{
                PrefixQueryBuilder vinQuery = QueryBuilders.prefixQuery("vin", vo.getVin());
                boolQueryBuilder.must(vinQuery);
            }
        }

        if(CollectionUtil.isNotEmpty(vo.getDates()) && vo.getDates().size() == 2){
            String startTime = vo.getDates().get(0);
            String endTime = vo.getDates().get(1);
            boolQueryBuilder.must(QueryBuilders.rangeQuery("createTime").gte(startTime+" 00:00:00").lte(endTime+ " 23:59:59"));
        }

        if(CollectionUtil.isNotEmpty(vo.getVehType())){
            TermsQueryBuilder vehTypeQuery = QueryBuilders.termsQuery("vehicleTypeId",  vo.getVehType());
            boolQueryBuilder.should(vehTypeQuery);
            boolQueryBuilder.minimumShouldMatch(1);
        }

        if(CollectionUtil.isNotEmpty(vo.getEcuName())){
            TermsQueryBuilder ecuQuery = QueryBuilders.termsQuery("ecuName",  vo.getEcuName());
            boolQueryBuilder.filter(ecuQuery);
        }

        if(StrUtil.isNotBlank(vo.getDtcCode())){
            TermsQueryBuilder ecuQuery = QueryBuilders.termsQuery("dtcCode",  vo.getDtcCode());
            boolQueryBuilder.must(ecuQuery);
        }

        searchSourceBuilder.query(boolQueryBuilder);
        String aggName = "dtcSession";
        String pageAggName = "bucketSort";
        String aggCount = "sessionCount";

        BucketOrder order = BucketOrder.aggregation("_key", false);
        AggregationBuilder aggregationBuilder = AggregationBuilders.terms(aggName)
                .minDocCount(1)
                .field("sessionInfoId")
                .order(order)
                .size(TERM_AGG_MAX);

        List<FieldSortBuilder> sortList = new ArrayList<>();
        aggregationBuilder.subAggregation(PipelineAggregatorBuilders.bucketSort(pageAggName,sortList).from(from).size(vo.getPageSize()));
        //CARDINALITY_PRECISION_THRESHOLD 大数据量情况下 要看情况设置精度
        AggregationBuilder aggCountBuilder = AggregationBuilders.cardinality(aggCount).field("sessionInfoId");

        searchSourceBuilder.aggregation(aggregationBuilder).size(0);
        searchSourceBuilder.aggregation(aggCountBuilder);
        searchRequest.source(searchSourceBuilder);
        log.info("searchSourceBuilder:{}", searchSourceBuilder.toString());

        try {
            SearchResponse searchResponse = restService.getClient().search(searchRequest, RequestOptions.DEFAULT);
            Terms terms = searchResponse.getAggregations().get(aggName);
            terms.getBuckets().forEach(item ->{
                sessionIdList.add(item.getKeyAsString());
            });
            result.put("ids", sessionIdList);
            Cardinality cardinality =  searchResponse.getAggregations().get(aggCount);
            if(null != cardinality){
                Double totalPage = Math.ceil(Double.valueOf(cardinality.getValue())/size);
                result.put("total", cardinality.getValue());
                result.put("totalPage", (int)Math.round(totalPage));
            }

            System.out.println(JSONObject.toJSONString(cardinality));

        }catch (Exception ex){
            log.error("terms aggregation error: {}", JSONObject.toJSONString(ex));
        }
        return result;
    }

    @Override
    public List<Map<String, Object>> dtcPi(DtcChartReVo vo) {
        String index = restService.formatIndex(EsIndexEnum.DTC.getValue(), vo.getDates());
        SearchRequest searchRequest = new SearchRequest(index);
        searchRequest.types("_doc");
        searchRequest.indices(index);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.mustNot(QueryBuilders.termQuery("dtcCode", ""));

        if(StrUtil.isNotBlank(vo.getVin())){
            if(vo.getVin().length() == 17){
                boolQueryBuilder.must(QueryBuilders.termQuery("vin", vo.getVin()));
            }else{
                PrefixQueryBuilder vinQuery = QueryBuilders.prefixQuery("vin", vo.getVin());
                boolQueryBuilder.must(vinQuery);
            }
        }

        if(CollectionUtil.isNotEmpty(vo.getDates()) && vo.getDates().size() == 2){
            String startTime = vo.getDates().get(0);
            String endTime = vo.getDates().get(1);
            boolQueryBuilder.must(QueryBuilders.rangeQuery("createTime").gte(startTime+" 00:00:00").lte(endTime+ " 23:59:59"));
        }

        if(CollectionUtil.isNotEmpty(vo.getVehType())){
            TermsQueryBuilder vehTypeQuery = QueryBuilders.termsQuery("vehicleTypeId",  vo.getVehType());
            boolQueryBuilder.should(vehTypeQuery);
            boolQueryBuilder.minimumShouldMatch(1);
        }

        //这里通过filter实现并集条件
        if(CollectionUtil.isNotEmpty(vo.getEcuNames())){
            TermsQueryBuilder ecuQuery = QueryBuilders.termsQuery("ecuName",  vo.getEcuNames());
            boolQueryBuilder.filter(ecuQuery);
        }

        String aggName = "dtcCountAggs";

        searchSourceBuilder.query(boolQueryBuilder);
        List<Map<String, Object>> items = new ArrayList<>();
        TermsAggregationBuilder termAgg = AggregationBuilders.terms(aggName)
                .field("dtcCode")
                .minDocCount(1)
                .size(vo.getTop());
        try {
            items =  restService.termsAggregation(index, aggName, boolQueryBuilder, termAgg);
        }catch (Exception ex){
            log.error("dtc pi: {}", ex.getMessage());
        }

        return items;
    }

    @Override
    public List<Map<String, Object>> dtcMonthBar(DtcChartReVo vo) {
        String index = restService.formatIndex(EsIndexEnum.DTC.getValue(), vo.getDates());
        SearchRequest searchRequest = new SearchRequest(index);
        searchRequest.types("_doc");
        searchRequest.indices(index);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.mustNot(QueryBuilders.termQuery("dtcCode", ""));
        if(StrUtil.isNotBlank(vo.getVin())){
            if(vo.getVin().length() == 17){
                boolQueryBuilder.must(QueryBuilders.termQuery("vin", vo.getVin()));
            }else{
                PrefixQueryBuilder vinQuery = QueryBuilders.prefixQuery("vin", vo.getVin());
                boolQueryBuilder.must(vinQuery);
            }
        }

        if(CollectionUtil.isNotEmpty(vo.getVehType())){
            TermsQueryBuilder vehTypeQuery = QueryBuilders.termsQuery("vehicleTypeId",  vo.getVehType());
            boolQueryBuilder.should(vehTypeQuery);
            boolQueryBuilder.minimumShouldMatch(1);
        }

        if(CollectionUtil.isNotEmpty(vo.getEcuNames())){
            TermsQueryBuilder ecuQuery = QueryBuilders.termsQuery("ecuName",  vo.getEcuNames());
            boolQueryBuilder.filter(ecuQuery);
        }

        if(CollectionUtil.isNotEmpty(vo.getDates())){
            String startTime = vo.getDates().get(0);
            String endTime = vo.getDates().get(1);
            boolQueryBuilder.must(QueryBuilders.rangeQuery("createTime").gte(startTime+" 00:00:00").lte(endTime+ " 23:59:59"));
        }
        searchSourceBuilder.query(boolQueryBuilder);
        List<String> dates = vo.getDates();
        String format = "yyyy-MM";
        String aggName = "dtcBarCount";
        String subAggName = "perDtcCount";
        String pageAggName = "page";
        List<FieldSortBuilder> sortList = new ArrayList<>();
//        Integer from = (vo.getPageNo()-1)*vo.getPageSize();
        Integer from =0;
        Integer top = vo.getTop();
        AggregationBuilder subAggBuilder = AggregationBuilders.terms(subAggName).field("dtcCode").size(vo.getTop()).minDocCount(1);

        AggregationBuilder aggregationBuilder = AggregationBuilders.dateHistogram(aggName)
                .dateHistogramInterval(DateHistogramInterval.MONTH)
                .minDocCount(1)
                .field("createTime")
                .format(format)
                .extendedBounds(new ExtendedBounds(dates.get(0).substring(0, 7), dates.get(1).substring(0, 7)))
                .subAggregation(subAggBuilder)
                .subAggregation(PipelineAggregatorBuilders.bucketSort(pageAggName,sortList).from(from).size(top));

        List<Map<String, Object>> items = new ArrayList<>();
        try {
            items = restService.groupDateHistogramAggregation(index, aggName, subAggName, boolQueryBuilder, aggregationBuilder);
        }catch (Exception ex){
            log.error("cts diagnostic month: {}", ex.getMessage());
        }

        return items;
    }

    /***
     * @todo dtc_daily_histogram_aggs.json
     * @param vo
     * @return
     */
    @Override
    public List<Map<String, Object>> dtcDailyBar(DtcChartReVo vo) {

        String index = restService.formatIndex(EsIndexEnum.DTC.getValue(), vo.getDates());
        SearchRequest searchRequest = new SearchRequest(index);
        searchRequest.types("_doc");
        searchRequest.indices(index);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        Script script = new Script("doc['ecuName'].value +'-'+ doc['dtcCode'].value");

        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.mustNot(QueryBuilders.termQuery("dtcCode", ""));
        if(StrUtil.isNotBlank(vo.getVin())){
            if(vo.getVin().length() == 17){
                boolQueryBuilder.must(QueryBuilders.termQuery("vin", vo.getVin()));
            }else{
                PrefixQueryBuilder vinQuery = QueryBuilders.prefixQuery("vin", vo.getVin());
                boolQueryBuilder.must(vinQuery);
            }
        }
        if(CollectionUtil.isNotEmpty(vo.getDates())){
            String startTime = vo.getDates().get(0);
            String endTime = vo.getDates().get(1);
            boolQueryBuilder.must(QueryBuilders.rangeQuery("createTime").gte(startTime+" 00:00:00").lte(endTime+ " 23:59:59"));
        }
        if(CollectionUtil.isNotEmpty(vo.getVehType())){
            TermsQueryBuilder vehTypeQuery = QueryBuilders.termsQuery("vehicleTypeId",  vo.getVehType());
            boolQueryBuilder.should(vehTypeQuery);
            boolQueryBuilder.minimumShouldMatch(1);
        }

        if(CollectionUtil.isNotEmpty(vo.getEcuNames())){
            TermsQueryBuilder ecuQuery = QueryBuilders.termsQuery("ecuName",  vo.getEcuNames());
            boolQueryBuilder.filter(ecuQuery);
        }

        searchSourceBuilder.query(boolQueryBuilder);
        List<String> dates = vo.getDates();
        String format = "yyyy-MM-dd";
        String aggName = "dtcDateBarCount";
        String subAggName = "perDtcCount";
        String pageAggName = "page";
        List<FieldSortBuilder> sortList = new ArrayList<>();
        Integer from = (vo.getPageNo()-1)*vo.getPageSize();

        TermsAggregationBuilder subAggBuilder = AggregationBuilders.terms(subAggName).script(script).size(TERM_AGG_MAX);

//        AggregationBuilder subAggBuilder = AggregationBuilders
//                .terms(subAggName)
//                .field("dtcCode")
//                .size(vo.getTop())
//                .minDocCount(1);

        AggregationBuilder aggregationBuilder = AggregationBuilders.dateHistogram(aggName)
                .dateHistogramInterval(DateHistogramInterval.DAY)
                .minDocCount(1)
                .field("createTime")
                .format(format)
                .extendedBounds(new ExtendedBounds(dates.get(0), dates.get(1)))
                .subAggregation(subAggBuilder)
                .subAggregation(PipelineAggregatorBuilders.bucketSort(pageAggName,sortList).from(from).size(vo.getPageSize()));

        List<Map<String, Object>> items = new ArrayList<>();

        try {
            items = restService.groupDateHistogramAggregation(index,
                    aggName,
                    subAggName,
                    boolQueryBuilder,
                    aggregationBuilder);
        }catch (Exception ex){
            log.error("cts diagnosticDaily: {}", ex.getMessage());
        }

        return items;
    }

    @Override
    public VehicleReportDto vehicleReport(VehicleReportVo vo) {
        List<String> dates = new ArrayList<>();
        if(null == vo.getEndTime()){
            vo.setEndTime(vo.getStartTime());
        }
        dates.add(vo.getStartTime());
        dates.add(vo.getEndTime());

        String index = restService.formatIndex(EsIndexEnum.VEHICLE_REPORT.getValue(), dates);
        VehicleReportDto dto = null;
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("sessionInfoId", vo.getSessionInfoId());
           Map<String, Object> reportMap =  restService.queryOneDoc(index, map);
           if(CollectionUtil.isNotEmpty(reportMap)){
               dto = BeanUtil.fillBeanWithMap(reportMap, new VehicleReportDto(), false);
           }
        } catch (Exception e) {
            log.error("vehicleReport: {}", e.getMessage());
        }
        return dto;
    }

    @Override
    public SessionDoc vehicleReportSession(VehicleReportVo vo) {
        String index = restService.formatIndexByDate(EsIndexEnum.SESSION.getValue(), vo.getStartTime());
        SessionDoc sessionDoc = null;
        try {
            Map<String, Object> doc = restService.queryDocById(index, vo.getSessionInfoId());
            sessionDoc =  BeanUtil.fillBeanWithMap(doc, new SessionDoc(), false);
        }catch (Exception ex){
            log.error("vehicleReportSession: {}", ex.getMessage());
        }

        return sessionDoc;
    }

    @Override
    public List<DtcDoc> queryDtcDocBySessionAndRecordId(String sessionInfoId, Integer recordId, String date) {
        String index = restService.formatIndexByDate(EsIndexEnum.DTC.getValue(), date);

        List<DtcDoc> result = new ArrayList<>();
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("sessionInfoId", sessionInfoId);
            map.put("recordId", recordId);

            List<Map<String, Object>> docs =  restService.queryDoc(index, map, "createTime", "desc", "date");
            docs.forEach(data ->{
                DtcDoc dtcDoc = BeanUtil.fillBeanWithMap(data, new DtcDoc(), false);
                result.add(dtcDoc);
            });
        } catch (IOException e) {
            log.error("queryDtcDocBySessionAndRecordId: {}", e.getMessage());
        }

        return result;
    }


    @Override
    public List<DidDoc> queryDidDocBySessionAndRecordId(String sessionInfoId, Integer recordId, String date) {
        String index = restService.formatIndexByDate(EsIndexEnum.DID.getValue(), date);
        List<DidDoc> result = new ArrayList<>();

        try {
            Map<String, Object> map = new HashMap<>();
            map.put("sessionInfoId", sessionInfoId);
            map.put("recordId", recordId);

            List<Map<String, Object>> docs =  restService.queryDoc(index, map, "createTime", "desc", "date");
            docs.forEach(data ->{
                DidDoc dtcDoc = BeanUtil.fillBeanWithMap(data, new DidDoc(), false);
                result.add(dtcDoc);
            });
        } catch (Exception e) {
            log.error("queryDidDocBySessionAndRecordId :{}", e.getMessage());
        }
        return result;
    }

    @Override
    public VehicleReportDoc queryVehicleReportDoc(String sessionInfoId, String date) {
        String index = restService.formatIndexByDate(EsIndexEnum.VEHICLE_REPORT.getValue(), date);
        Map<String, Object> map = new HashMap<>();
        map.put("sessionInfoId", sessionInfoId);
        VehicleReportDoc doc = null;
        try {
            Map<String, Object> reportMap  = restService.queryOneDoc(index, map);
            doc = BeanUtil.fillBeanWithMap(reportMap, new VehicleReportDoc(), false);
        } catch (IOException e) {
            log.error("queryVehicleReportDoc error:{}", JSONObject.toJSONString(e));
        }

        return doc;
    }

    @Override
    public List<DiagnosticEcuDoc> queryDiagnosticVhss(String sessionInfoId, String date) {
        String index = restService.formatIndexByDate(EsIndexEnum.ECU.getValue(), date);
        List<DiagnosticEcuDoc> ecuDocList = new ArrayList<>();

        Map<String, Object> map = new HashMap<>();
        map.put("sessionInfoId", sessionInfoId);

        List<Map<String, Object>> docs = null;
        try {
            docs = restService.queryDoc(index, map, "createTime", "desc", "date");
            docs.forEach(data ->{
                DiagnosticEcuDoc ecuDoc = BeanUtil.fillBeanWithMap(data, new DiagnosticEcuDoc(), false);
                ecuDocList.add(ecuDoc);
            });
        } catch (Exception e) {
            log.error("queryDiagnosticVhss :{}", e.getMessage());
        }

        return ecuDocList;
    }

    @Override
    public List<String> vinRecommend(String vin, String date, Integer size) {
        String index = restService.formatIndexByDate(EsIndexEnum.SESSION.getValue(), date);
        List<String> vinList = new ArrayList<>();

        try {
            List<String> _source = new ArrayList<>(Arrays.asList("vin"));
            List<Map<String, Object>> result =  restService.completionSuggest(index,"vinSuggest",
                    vin, _source, size);
            for (Map<String, Object> map: result){
                vinList.add(map.get("vin").toString());
            }
        } catch (IOException e) {
            log.error("vinRecommend :{}", e.getMessage());
        }
        return vinList;
    }

    @Override
    public List<String> ecuRecommend(String ecu, String date, Integer size) {
        String index = restService.formatIndexByDate(EsIndexEnum.DIAG.getValue(), date);
        List<String> ecuList = new ArrayList<>();
        try {
            List<String> _source = new ArrayList<>(Arrays.asList("ecuName"));
            List<Map<String, Object>> result =  restService.completionSuggest(index,"ecuNameSuggest",
                    ecu, _source, size);
            for (Map<String, Object> map: result){
                ecuList.add(map.get("ecuName").toString());
            }
        }catch (Exception ex){
            log.error("ecuRecommend :{}", ex.getMessage());
        }

        return ecuList;
    }

    @Override
    public List<String> dtcRecommend(String dtc, String date, Integer size) {
        String index = restService.formatIndexByDate(EsIndexEnum.DTC.getValue(), date);
        List<String> dtcList = new ArrayList<>();
        try {
            List<String> _source = new ArrayList<>(Arrays.asList("dtcCode"));
            List<Map<String, Object>> result =  restService.completionSuggest(index,"dtcCodeSuggest",
                    dtc, _source, size);
            for (Map<String, Object> map: result){
                dtcList.add(map.get("dtcCode").toString());
            }
        }catch (Exception ex){
            log.error("dtcRecommend :{}", ex.getMessage());
        }
        return dtcList;
    }

    @Override
    public List<DtcDoc> sessionDtcForVehicleReport(String sessionInfoId, String date, List<Integer> recordFlags) {
        String index = restService.formatIndexByDate(EsIndexEnum.DTC.getValue(), date);
        List<DtcDoc> dtcDocList = new ArrayList<>();

        Map<String, Object> map = new HashMap<>();
        map.put("sessionInfoId", sessionInfoId);
        if(CollectionUtil.isNotEmpty(recordFlags)){
            map.put("recordFlag", recordFlags);
        }

        List<Map<String, Object>> docs = null;
        try {
            docs = restService.queryDoc(index, map, "createTime", "asc", "date");
            docs.forEach(data ->{

                DtcDoc dtcDoc = BeanUtil.fillBeanWithMap(data, new DtcDoc(), false);

                dtcDocList.add(dtcDoc);
            });
        } catch (IOException e) {
            log.error("sessionDtcForVehicleReport error:{}", JSONObject.toJSONString(e));
        }
        return dtcDocList;
    }

    @Override
    public List<DidDoc> sessionDidForVehicleReport(String sessionInfoId, String date, List<Integer> recordFlags) {
        String index = restService.formatIndexByDate(EsIndexEnum.DID.getValue(), date);
        List<DidDoc> didDocList = new ArrayList<>();

        Map<String, Object> map = new HashMap<>();
        map.put("sessionInfoId", sessionInfoId);
        if(CollectionUtil.isNotEmpty(recordFlags)){
            map.put("recordFlag", recordFlags);
        }

        List<Map<String, Object>> docs = null;
        try {
            docs = restService.queryDoc(index, map, "createTime", "asc", "date");
            docs.forEach(data ->{
                DidDoc didDoc = BeanUtil.fillBeanWithMap(data, new DidDoc(), false);
                didDocList.add(didDoc);
            });
        } catch (IOException e) {
            log.error("sessionDidForVehicleReport error:{}", JSONObject.toJSONString(e));
        }
        return didDocList;
    }

    @Override
    public List<DiagnosticEcuDoc> sessionEcuForVehicleReport(String sessionInfoId, String date) {
        String index = restService.formatIndexByDate(EsIndexEnum.ECU.getValue(), date);
        Map<String, Object> map = new HashMap<>();
        map.put("sessionInfoId", sessionInfoId);
        List<Map<String, Object>> docs = null;
        List<DiagnosticEcuDoc> ecuList = new ArrayList<>();

        try {
            docs = restService.queryDoc(index, map, "createTime", "asc", "date");
            docs.forEach(data ->{
                DiagnosticEcuDoc doc = BeanUtil.fillBeanWithMap(data, new DiagnosticEcuDoc(), false);
                ecuList.add(doc);
            });
        } catch (IOException e) {
            log.error("sessionEcuForVehicleReport error:{}", JSONObject.toJSONString(e));
        }
        return ecuList;
    }

    @Override
    public List<DiagnosticDoc> sessionDiagnosticVehicleReport(String sessionInfoId, String date) {
        String index = restService.formatIndexByDate(EsIndexEnum.DIAG.getValue(), date);
        Map<String, Object> map = new HashMap<>();
        map.put("sessionInfoId", sessionInfoId);
        List<Map<String, Object>> docs = null;
        List<DiagnosticDoc> docList = new ArrayList<>();
        try {
            docs = restService.queryDoc(index, map, "createTime", "asc", "date");
            docs.forEach(data ->{
                DiagnosticDoc doc = BeanUtil.fillBeanWithMap(data, new DiagnosticDoc(), false);
                docList.add(doc);
            });
        } catch (IOException e) {
            log.error("sessionDiagnosticVehicleReport error:{}", JSONObject.toJSONString(e));
        }
        return docList;
    }

    @Override
    public List<DtcSnapshotDoc> sessionDtcSnapshotVehicleReport(String sessionInfoId, String date, String ecuName, String dtcCode) {
        String index = restService.formatIndexByDate(EsIndexEnum.DTC_SNAPSHOT.getValue(), date);
        Map<String, Object> map = new HashMap<>();
        map.put("sessionInfoId", sessionInfoId);
        map.put("ecuName", ecuName);
        map.put("dtcCode", dtcCode);

        List<Map<String, Object>> docs = null;
        List<DtcSnapshotDoc> docList = new ArrayList<>();
        try {
            docs = restService.queryDoc(index, map, "createTime", "asc", "date");
            docs.forEach(data ->{
                DtcSnapshotDoc doc = BeanUtil.fillBeanWithMap(data, new DtcSnapshotDoc(), false);
                docList.add(doc);
            });
        } catch (IOException e) {
            log.error("sessionDiagnosticVehicleReport error:{}", JSONObject.toJSONString(e));
        }

        return docList;
    }

    @Override
    public List<DtcSnapshotDoc> sessionDtcSnapshotVehicleReport(String sessionInfoId, String date) {
        String index = restService.formatIndexByDate(EsIndexEnum.DTC_SNAPSHOT.getValue(), date);
        Map<String, Object> map = new HashMap<>();
        map.put("sessionInfoId", sessionInfoId);

        List<Map<String, Object>> docs = null;
        List<DtcSnapshotDoc> docList = new ArrayList<>();
        try {
            docs = restService.queryDoc(index, map, "createTime", "asc", "date");
            docs.forEach(data ->{
                DtcSnapshotDoc doc = BeanUtil.fillBeanWithMap(data, new DtcSnapshotDoc(), false);
                docList.add(doc);
            });
        } catch (Exception e) {
            log.error("sessionDtcSnapshotVehicleReport error:{}", JSONObject.toJSONString(e));
        }

        return docList;
    }

    @Override
    public List<DtcExtendedDoc> sessionDtcExtendedVehicleReport(String sessionInfoId, String date) {
        String index = restService.formatIndexByDate(EsIndexEnum.DTC_EXTENDED.getValue(), date);
        Map<String, Object> map = new HashMap<>();
        map.put("sessionInfoId", sessionInfoId);

        List<Map<String, Object>> docs = null;
        List<DtcExtendedDoc> docList = new ArrayList<>();
        try {
            docs = restService.queryDoc(index, map, "createTime", "asc", "date");
            docs.forEach(data ->{
                DtcExtendedDoc doc = BeanUtil.fillBeanWithMap(data, new DtcExtendedDoc(), false);
                docList.add(doc);
            });
        } catch (Exception e) {
            log.error("sessionDtcExtendedVehicleReport error:{}", JSONObject.toJSONString(e));
        }

        return docList;
    }

    @Override
    public List<String> sessionEcuList() {
        String index = EsIndexEnum.ECU.getValue();

        return null;
    }

    @Override
    public void deleteSessionDoc(String sessionInfoId, String date) {

        try {
            //诊断会话
            String sessionIndex = restService.formatIndexByDate(EsIndexEnum.SESSION.getValue(), date);
            restService.deleteDocById(sessionIndex, sessionInfoId);

            Map<String, Object> queryMap = new HashMap<>(1);
            queryMap.put("sessionInfoId", sessionInfoId);
            //诊断操作
            String diagnosticIndex = restService.formatIndexByDate(EsIndexEnum.DIAG.getValue(), date);
            restService.deleteDocByQuery(diagnosticIndex, queryMap);

            //ECU
            String ecuIndex = restService.formatIndexByDate(EsIndexEnum.ECU.getValue(), date);
            restService.deleteDocByQuery(ecuIndex, queryMap);
            //DTC
            String dtcIndex = restService.formatIndexByDate(EsIndexEnum.DTC.getValue(), date);
            restService.deleteDocByQuery(dtcIndex, queryMap);

            //DID
            String didIndex = restService.formatIndexByDate(EsIndexEnum.DID.getValue(), date);
            restService.deleteDocByQuery(didIndex, queryMap);

            //REPORT
            String reportIndex = restService.formatIndexByDate(EsIndexEnum.VEHICLE_REPORT.getValue(), date);
            restService.deleteDocByQuery(reportIndex, queryMap);

            //DTC扩展数据
            String dtcExtendIndex = restService.formatIndexByDate(EsIndexEnum.DTC_EXTENDED.getValue(), date);
            restService.deleteDocByQuery(dtcExtendIndex, queryMap);

            //DTC冻结帧
            String dtcSnapshotIndex = restService.formatIndexByDate(EsIndexEnum.DTC_SNAPSHOT.getValue(), date);
            restService.deleteDocByQuery(dtcSnapshotIndex, queryMap);

        }catch (Exception ex){
            log.error("deleteSessionDoc: {}", ex.getMessage());
        }

    }

    @Override
    public List<DtcDaily> dtcDailyByVinDay(String vin, String date){

        List<String> dates = new ArrayList<>();
        List<DtcDaily> items = new ArrayList<>();
        dates.add(date);
        dates.add(date);

        String index = restService.formatIndex(EsIndexEnum.DTC.getValue(), dates);
        SearchRequest searchRequest = new SearchRequest(index);
        searchRequest.types("_doc");
        searchRequest.indices(index);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        Script script = new Script("doc['ecuName'].value +','+ doc['dtcCode'].value+','+doc['dtcCodeHex'].value+','+doc['dtcCodeInt'].value+','+doc['descriptionTi'].value");
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.mustNot(QueryBuilders.termQuery("dtcCode", ""));
        boolQueryBuilder.must(QueryBuilders.termQuery("vin", vin));
        boolQueryBuilder.must(QueryBuilders.rangeQuery("createTime").gte(date+" 00:00:00").lte(date+ " 23:59:59"));

        searchSourceBuilder.query(boolQueryBuilder);

        String format = "yyyy-MM-dd";
        String aggName = "dtcDateBarCount";
        String subAggName = "perDtcCount";
        String pageAggName = "page";
        List<FieldSortBuilder> sortList = new ArrayList<>();
        TermsAggregationBuilder subAggBuilder = AggregationBuilders.terms(subAggName).script(script).size(TERM_AGG_MAX);

        AggregationBuilder aggregationBuilder = AggregationBuilders.dateHistogram(aggName)
                .dateHistogramInterval(DateHistogramInterval.DAY)
                .minDocCount(1)
                .field("createTime")
                .format(format)
                .subAggregation(subAggBuilder)
                .subAggregation(PipelineAggregatorBuilders.bucketSort(pageAggName,sortList).from(0).size(1000));

        searchSourceBuilder.query(boolQueryBuilder);
        searchSourceBuilder.aggregation(aggregationBuilder).size(0);
        searchRequest.source(searchSourceBuilder);

        log.info("index {}: {}",index,searchSourceBuilder.toString());


        try {

            SearchResponse searchResponse = restService.getClient().search(searchRequest, RequestOptions.DEFAULT);
            Aggregation agg = searchResponse.getAggregations().get(aggName);
            List<? extends Histogram.Bucket> buckets = ((Histogram) agg).getBuckets();
            for (Histogram.Bucket bucket : buckets) {

                Terms perCtsCountTerm = (Terms) bucket.getAggregations().asMap().get(subAggName);
                for (Terms.Bucket twoTermBucket : perCtsCountTerm.getBuckets()) {

                    DtcDaily dtcDaily = new DtcDaily();
                    dtcDaily.setDtcDay(bucket.getKeyAsString());
                    dtcDaily.setDtcNum(twoTermBucket.getDocCount());
                    dtcDaily.setVin(vin);

                    if(twoTermBucket.getKeyAsString().indexOf(",")>-1){
                        String[] keys = twoTermBucket.getKeyAsString().split(",");
                        dtcDaily.setEcuName(keys[0]);
                        dtcDaily.setDtcCode(keys[1]);
                        dtcDaily.setDtcCodeHex(keys[2]);
                        dtcDaily.setDtcCodeInt(keys[3]);
                        dtcDaily.setTi(keys[4]);
                    }
                    items.add(dtcDaily);
                }
            }
        }catch (Exception ex){
            log.error("terms dtcDailyByVinDay error: {}", JSONObject.toJSONString(ex));
        }

        return items;
    }

    @Override
    public List<CtsDaily> ctsDailyByVinDay(String vin, String date) {

        List<String> dates = new ArrayList<>();
        List<CtsDaily> items = new ArrayList<>();
        dates.add(date);
        dates.add(date);

        Script script = new Script("doc['functionCode'].value +'-'+ doc['action'].value+','+doc['diagSource'].value");
        String index = restService.formatIndex(EsIndexEnum.DIAG.getValue(), dates);
        String format = "yyyy-MM-dd";
        String aggName = "ctsAggs";
        String subAggName = "perCtsCount";
        String pageAggName = "page";

        TermsAggregationBuilder perCtsCount = AggregationBuilders.terms(subAggName).script(script).size(TERM_AGG_MAX);
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.mustNot(QueryBuilders.termQuery("functionCode", ""));

        boolQueryBuilder.must(QueryBuilders.termQuery("vin", vin));

        boolQueryBuilder.must(QueryBuilders.rangeQuery("createTime").gte(date+" 00:00:00").lte(date+ " 23:59:59"));
        List<FieldSortBuilder> sortList = new ArrayList<>();
        Integer from = 0;

        AggregationBuilder aggregationBuilder = AggregationBuilders.dateHistogram(aggName)//自定义名称
                .dateHistogramInterval(DateHistogramInterval.DAY)//设置间隔
                .minDocCount(1)//返回空桶
                .field("createTime")//指定时间字段
                .format(format)//设定返回格式
                .extendedBounds(new ExtendedBounds(dates.get(0), dates.get(1))).//设定范围
                        subAggregation(perCtsCount)
                .subAggregation(PipelineAggregatorBuilders.bucketSort(pageAggName,sortList).from(from).size(1000));

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        SearchRequest searchRequest = new SearchRequest(index);
        searchRequest.types("_doc");
        searchRequest.indices(index);

        searchSourceBuilder.query(boolQueryBuilder);
        searchSourceBuilder.aggregation(aggregationBuilder).size(0);
        searchRequest.source(searchSourceBuilder);

        log.info("index {}: {}", index,searchSourceBuilder.toString());

        try {
            SearchResponse searchResponse = restService.getClient().search(searchRequest, RequestOptions.DEFAULT);
            Aggregation agg = searchResponse.getAggregations().get(aggName);
            List<? extends Histogram.Bucket> buckets = ((Histogram) agg).getBuckets();
            for (Histogram.Bucket bucket : buckets) {

                Terms perCtsCountTerm = (Terms) bucket.getAggregations().asMap().get(subAggName);
                for (Terms.Bucket twoTermBucket : perCtsCountTerm.getBuckets()) {

                    CtsDaily ctsDaily = new CtsDaily();
                    ctsDaily.setCtsDay(bucket.getKeyAsString());
                    ctsDaily.setCtsNum(twoTermBucket.getDocCount());
                    ctsDaily.setVin(vin);

                    if(twoTermBucket.getKeyAsString().indexOf(",")>-1){
                        String[] keys = twoTermBucket.getKeyAsString().split(",");
                        ctsDaily.setCtsName(keys[0]);
                        if(keys[1].equals("null")){
                            ctsDaily.setSource(ConfigConstant.DEFAULT_SOURCE);
                        }else{
                            ctsDaily.setSource(keys[1]);
                        }
                        String[] ctsList = keys[0].split("-");
                        //这里处理一下functionCode == actionCode 场景的异常数据
                        if(ctsList.length == 2){
                            ctsDaily.setFunctionCode(ctsList[0]);
                            if(!ctsList[0].equals(ctsList[1])){
                                ctsDaily.setActionCode(ctsList[1]);
                            }else{
                                ctsDaily.setCtsName(ctsList[0]);
                            }
                        }else{
                            ctsDaily.setCtsName(ctsList[0]);
                            ctsDaily.setFunctionCode(ctsList[0]);
                        }
                    }
                    items.add(ctsDaily);
                }
            }

        }catch (Exception ex){
            log.error("cts diagnosticDaily: {}", ex.getMessage());
        }

        return items;
    }

    @Override
    public SessionDoc querySessionBySessionId(String sessionId, String date) {
        String sessionIndex = restService.formatIndexByDate(EsIndexEnum.SESSION.getValue(), date);
        Map<String, Object> map = new HashMap<>();
        map.put("sessionId", sessionId);
        SessionDoc sessionDoc = null;

        try {
            Map<String, Object> data = restService.queryOneDoc(sessionIndex, map);
            sessionDoc = BeanUtil.fillBeanWithMap(data, new SessionDoc(), false);
        }catch (Exception ex){

        }

        return sessionDoc;
    }

    @Override
    public List<Map<String, Object>> vinDailyAggregations(String date) {
        String index = restService.formatIndexByDate(EsIndexEnum.SESSION.getValue(), date);
        String aggName = "vinDailyAggs";

        List<Map<String, Object>> items = new ArrayList<>();

        SearchRequest searchRequest = new SearchRequest(index);
        searchRequest.types("_doc");
        searchRequest.indices(index);

        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();

        boolQueryBuilder.must(QueryBuilders.rangeQuery("createTime").gte(date+" 00:00:00").lte(date+ " 23:59:59"));

        TermsAggregationBuilder termAgg = AggregationBuilders.terms(aggName)
                .field("vin")
                .minDocCount(1)
                .size(TERM_AGG_MAX);
        try {
            items =  restService.termsAggregation(index, aggName, boolQueryBuilder, termAgg);
        }catch (Exception ex){
            log.error("vin {} pi: {}", date, ex.getMessage());
        }
        return items;
    }

}
