package org.dsa.modules.vehicle.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.baomidou.mybatisplus.extension.handlers.GsonTypeHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.dsa.common.handler.JsonTypeHandler;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleEcuGroupEntity;
import org.dsa.modules.vehicle.dto.VehicleTypeSdgDto;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@ToString
@EqualsAndHashCode
@TableName(value = "d_vehicle_type", autoResultMap = true)
public class DVehicleTypeEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 显示信息
     */
    private String value;
    /**
     * 上级节点id
     */
    private Long parentId;
    /**
     * 排序
     */
    private int orderNum;
    /**
     * 级别
     */
    private int level;
    /**
     * 是否有版本文件关联
     */
    private int haveOdxVersion;
    /**
     * 是否有版本文件关联
     */
    private int haveOtxVersion;
    /**
     * 创建时间
     */
    private Date createTime;

    @TableField(typeHandler = JsonTypeHandler.class)
    private List<VehicleTypeSdgDto> sdgList;

    private String descriptionTi;

    private String remarkTi;

    @TableField(typeHandler = JsonTypeHandler.class)
    private List<String> ecuList;
    /**
     * 上级部门名称
     */
    @TableField(exist = false)
    private String parentName;
    /**
     * ztree属性
     */
    @TableField(exist = false)
    private Boolean open;
    @TableField(exist = false)
    private List<?> list;
    @TableField(exist = false)
    private List<RemoteVehicleEcuGroupEntity> groupList;
    @TableField(exist = false)
    private List<RemoteVehicleEcuGroupEntity> delGroupList;
    @TableField(exist = false)
    private String selectName;
//    @TableField(exist=false)
//    private List<RemoteVehicleTypeEcuGroupEntity> saveGroupList;
}
