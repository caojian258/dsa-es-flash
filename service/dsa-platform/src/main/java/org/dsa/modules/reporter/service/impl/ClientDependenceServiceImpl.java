package org.dsa.modules.reporter.service.impl;

import org.dsa.modules.reporter.dao.ClientDependenceOverviewMapper;
import org.dsa.modules.reporter.entity.ClientDependenceOverview;
import org.dsa.modules.reporter.service.ClientDependenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientDependenceServiceImpl implements ClientDependenceService {

    @Autowired
    private ClientDependenceOverviewMapper overviewMapper;

    @Override
    public List<ClientDependenceOverview> getDependenceList(String deviceId) {
        return overviewMapper.queryByDeviceId(deviceId);
    }
}
