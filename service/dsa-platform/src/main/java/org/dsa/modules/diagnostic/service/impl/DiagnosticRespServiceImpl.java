package org.dsa.modules.diagnostic.service.impl;

import ch.qos.logback.classic.Logger;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.R;
import org.dsa.common.utils.RedisUtils;
import org.dsa.modules.diagnostic.entity.mqtt.callback.MqBodyDataVHHSVO;
import org.dsa.modules.diagnostic.service.DiagnosticRespService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import jakarta.annotation.Resource;
import java.util.ArrayList;
import java.util.List;


@Service("diagnosticRespService")
public class DiagnosticRespServiceImpl implements DiagnosticRespService {
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass());

    @Resource
    private RedisUtils redisUtils;

    /**
     * 开始诊断
     * @return
     */
    @Override
    public R beginResp(String actionId) {
        return handle(Thread.currentThread().getStackTrace()[2].getMethodName(),actionId);
    }

    /**
     * 结束诊断
     * @return
     */
    @Override
    public R endResp(String actionId) {
        return handle(Thread.currentThread().getStackTrace()[2].getMethodName(),actionId);
    }

    /**
     * 车辆健康状态诊断
     * @return
     */
    @Override
    public R healthyResp(String actionId) {
        return handle(Thread.currentThread().getStackTrace()[2].getMethodName(),actionId);
    }

    @Override
    public R readIdentDataResp(String actionId) {
        return handle(Thread.currentThread().getStackTrace()[2].getMethodName(),actionId);
    }

    @Override
    public R readDtcCodeResp(String actionId,int length) {
        return handle(Thread.currentThread().getStackTrace()[2].getMethodName(),actionId);
    }

    @Override
    public R readDtcDataGridResp(String actionId) {
        return handle(Thread.currentThread().getStackTrace()[2].getMethodName(),actionId);
    }

    @Override
    public R clearDtcResp(String actionId) {
        return handle(Thread.currentThread().getStackTrace()[2].getMethodName(),actionId);
    }


    private R handle(String function,String actionId){
        logger.info("{} ---- begin",function);
        logger.info("{} ---- actionId:{}",function,actionId);
        //1.判断入参是否正确
        if(StringUtils.isEmpty(actionId)){
            throw new RRException( Constant.Msg.CHECK_INPUT );
        }

        if(redisUtils.hasKey(Constant.REDIS_ACTION_STATUS + actionId)){
            int status = redisUtils.get(Constant.REDIS_ACTION_STATUS + actionId,Integer.class);
            if(status == Constant.RESULTS.SEND.getValue()){
                logger.info("{} -- return error 499  -- end",function);
                String s = redisUtils.get(Constant.REDIS_ACTION_RESULT + actionId);
                if(StringUtils.isEmpty(s)){
                    return R.error(Constant.Msg.ACTION_RUN_ING);
                }else {
                    logger.info("{} -- return result  -- {}",function,s);
                    return R.error(Constant.Msg.ACTION_RUN_ING).put("data", JSONObject.parseArray(s));
                }
            }else if(status == Constant.RESULTS.SUCCESS.getValue()){
                String s = redisUtils.get(Constant.REDIS_ACTION_RESULT + actionId);
                //清除缓存
                redisUtils.delete(Constant.REDIS_ACTION_STATUS + actionId);
                redisUtils.delete(Constant.REDIS_ACTION_RESULT + actionId);

                logger.info("{} -- return ok  -- end",function);
                if(StringUtils.isEmpty(s)){
                    return R.ok();
                }else {
                    logger.info("{} -- return result  -- {}",function,s);
                    return R.ok().put("data", JSONObject.parseArray(s));
                }
            }else if(status == Constant.RESULTS.FAIL.getValue()){
                logger.info("{} -- return NAK  -- end",function);
                return R.error(Constant.Msg.ACTION_RUN_FAIL);
            }else if(status == Constant.RESULTS.TIMEOUT.getValue()){
                logger.info("{} -- return TIMEOUT  -- end",function);
                return R.error(Constant.Msg.ACTION_RUN_TIMEOUT);
            }
        }else{
            logger.info("{} --return error TIMEOUT-- end",function);
            return R.error(Constant.Msg.ACTION_RUN_TIMEOUT);
        }

        return R.error(Constant.Msg.ACTION_RUN_ING);
    }
}