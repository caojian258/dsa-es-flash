package org.dsa.modules.remoteDiagnostic.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticFunctionEntity;
import org.dsa.modules.remoteDiagnostic.entity.DiagnosticTaskEntity;
import org.dsa.modules.remoteDiagnostic.vo.CheckNameReqVo;
import org.dsa.modules.remoteDiagnostic.vo.CheckStatusReqVo;
import org.dsa.modules.remoteDiagnostic.vo.PublicPageReqVo;
import org.dsa.modules.remoteDiagnostic.vo.Task.TaskConditionReqVo;

import java.util.List;

public interface DiagnosticTaskService {

    Page<DiagnosticTaskEntity> selectPage(PublicPageReqVo vo);

    DiagnosticTaskEntity getInfo(Long id);

    void saveTask(DiagnosticTaskEntity task);

    void updateTask(DiagnosticTaskEntity task);

    List<DiagnosticTaskEntity> getTasks();

    Boolean check(CheckNameReqVo vo);

    List<DiagnosticFunctionEntity> getFunctionByTest(TaskConditionReqVo vo);

    Boolean checkStatus(CheckStatusReqVo vo);
}
