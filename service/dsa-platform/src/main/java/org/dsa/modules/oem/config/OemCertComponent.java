package org.dsa.modules.oem.config;


import lombok.Data;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Data
@ConfigurationProperties(prefix = "oem")
public class OemCertComponent {

//    private Map<String, String> forward;

    private cert cert;

    @Data
    @ToString
    public static class cert {
        private String url;

        private String csrUrl;

        private String whiteSyncUrl;
    }
}
