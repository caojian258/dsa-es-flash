package org.dsa.modules.sys.vo.workshop;

import lombok.Data;

import java.io.Serializable;

@Data
public class WorkshopClientVo implements Serializable {

    private Long country = 100000L;

    private String region;

    private Long province;

    private Long city;

    private String name;

    private Integer status;
}
