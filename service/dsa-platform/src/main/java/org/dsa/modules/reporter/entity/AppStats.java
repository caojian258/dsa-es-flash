package org.dsa.modules.reporter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/***
 * 软件、组件历史记录统计表
 */
@Data
@TableName("client_application_stats")
public class AppStats extends BaseEntity{

    @TableId(type = IdType.AUTO)
    private Long id;

    private String updateId;

    private Integer appId;

    private String appName;

    private String appVersion;

    private Integer versionNumber;

    private String appType;

    /***
     * 0,none (无操作)
     * 1,download(下载)
     * 2,install(安装)
     * 3,repair(修复)
     * 4,rollback(回退)
     * 5,uninstall(卸载)
     */
    private Integer statusType;

    /***
     * 0,success成功
     * 1, fail失败
     */
    private Integer status;

    private Long total;

    private Date collectDate;

    private Integer versionId;
}
