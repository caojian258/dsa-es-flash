package org.dsa.modules.sys.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.dsa.modules.sys.dto.WorkshopImportMsgDto;
import org.dsa.modules.sys.entity.SysWorkshopsEntity;
import org.dsa.modules.sys.vo.workshop.*;

import java.util.List;
import java.util.Map;

public interface SysWorkshopsIService extends IService<SysWorkshopsEntity> {

    public Map<String, String> excelTemplate();

    public Map<String, String> exportToExcel(WorkshopClientVo vo);

    public WorkshopImportMsgDto importFromExcel(String path);

    public SysWorkshopsEntity findById(Long id);

    public SysWorkshopsEntity findByName(String name);

    Page<SysWorkshopsEntity> queryPage(WorkshopPageVo vo);

    Page<SysWorkshopsEntity> queryWorkshopsPage(WorkshopsPageVo vo);

    public void disabledById(Long id);

    public void saveWorkshop(WorkshopVo entity);

    public List<SysWorkshopsEntity> selectWorkshop(WorkshopClientVo vo);

    public String queryWorkshopNameById(Long id);

    public boolean checkWorkshopCode(WorkshopCodeValidatorVo code);

    public void enabledById(Long id);

    public List<String> regions();
}
