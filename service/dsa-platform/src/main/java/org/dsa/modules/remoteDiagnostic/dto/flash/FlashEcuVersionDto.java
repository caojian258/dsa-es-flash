package org.dsa.modules.remoteDiagnostic.dto.flash;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString(callSuper = true)
public class FlashEcuVersionDto implements Serializable {

    private Long versionId;

    private String vin;

    private Long vehicleVersion;

    private Long ecuId;

    private Long ecuVersionId;
}
