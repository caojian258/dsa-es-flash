package org.dsa.common.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicReference;

@Component
public class EnvUtils {

    @Autowired
    private Environment environment;
    private final AtomicReference<String> env = new AtomicReference<>();
    private final AtomicReference<String> databaseName = new AtomicReference<>();

    private void init() {
    	// 初始化环境
        initEnv();
        // 初始化自定义属性
        initDatabaseName();
    }

    private void initEnv() {
        if (env.get() != null) {
            return;
        }

        env.set(environment.getActiveProfiles()[0]);
    }

    private void initDatabaseName() {
        if (databaseName.get() != null) {
            return;
        }

        databaseName.set(environment.getProperty("spring.datasource.clickhouse.database"));
    }

    public boolean isPro() {
        init();
        return "pro".equals(env.get());
    }

    public boolean notPro() {
        return !isPro();
    }
}
