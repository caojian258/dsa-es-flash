package org.dsa.modules.reporter.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.dsa.modules.reporter.entity.Translation;

import java.util.List;

/**
 * 数据字典
 *
 */
@Mapper
public interface TranslationMapper extends BaseMapper<Translation> {

    Integer insertBatch(List<Translation> list);

    int truncateTable();

    List<Translation> listAll(@Param("locale") String locale);

    Translation translateByTiLocale(@Param("ti") String ti, @Param("locale") String locale);

    Translation translateByTi(@Param("ti") String ti);

}
