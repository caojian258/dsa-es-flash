package org.dsa.modules.diagnostic.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 故障码统计 - 柱状图
 * 
 */
@Data
public class AnalysisDtcBarDataEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 故障码
	 */
	private String name;

	/**
	 * 类型
	 */
	private String type;



	/**
	 * 故障数
	 */
	private List<Integer> data;
}
