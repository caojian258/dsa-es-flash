package org.dsa.modules.reporter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@TableName("client_dependence_info")
public class ClientDependence extends BaseEntity {
    @TableId(type = IdType.AUTO)
    private Long id;

    private String deviceId;

    private Integer softwareId;
    private Integer softwareVersionNumber;

    private Integer componentId;
    private Integer componentVersionNumber;

    private Integer dependencyComponentId;
    private Integer dependencyComponentVersionNumber;

    private String tag;
    //没有用到
    private String sessionId;
}
