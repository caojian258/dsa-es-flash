package org.dsa.modules.oem.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.dsa.common.utils.Constant;
import org.dsa.modules.oem.dto.SyncVehicleInfoDTO;
import org.dsa.modules.oem.service.SyncService;
import org.dsa.modules.oem.util.VehicleUtils;
import org.dsa.modules.vehicle.dao.DVehicleInfoDao;
import org.dsa.modules.vehicle.dao.DVehicleTypeDao;
import org.dsa.modules.vehicle.entity.DVehicleInfoEntity;
import org.dsa.modules.vehicle.entity.DVehicleTypeEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author weishunxin
 * @since 2023-05-30
 */
@Slf4j
@Service
public class SyncServiceImpl implements SyncService {

    @Autowired
    private VehicleUtils vehicleUtils;
    @Autowired
    private DVehicleInfoDao dVehicleInfoDao;
    @Autowired
    private DVehicleTypeDao dVehicleTypeDao;

    /**
     *
     * 同步车辆信息
     *
     * @param vehicleList vehicleList
     */
    @Override
    public void syncVehicleInfo(List<SyncVehicleInfoDTO> vehicleList) {
        if (CollectionUtils.isEmpty(vehicleList)) {
            log.warn("sync vehicle list is empty.");
            return;
        }

        Map<String, Long> map = new HashMap<>();
        for (SyncVehicleInfoDTO vehicleInfo : vehicleList) {
            // check params
            if (checkVehicleInfo(vehicleInfo)) {

                Long levelId = vehicleUtils.createLevelInfo(vehicleInfo, map);

                DVehicleTypeEntity typeEntity = new DVehicleTypeEntity();
                typeEntity.setId(levelId);
                typeEntity.setEcuList(vehicleInfo.getEcuList());
                updateVehicleType(typeEntity);

                saveVehicleInfo(vehicleInfo, levelId);

            }
        }

        // help GC
        map.clear();
    }

    private boolean checkVehicleInfo(SyncVehicleInfoDTO vehicleInfoDTO) {
        if (Objects.isNull(vehicleInfoDTO)) {
            return false;
        }

        if (StringUtils.isBlank(vehicleInfoDTO.getVin())
                || StringUtils.isBlank(vehicleInfoDTO.getVehiclePlatform())
                || StringUtils.isBlank(vehicleInfoDTO.getMake())
                || StringUtils.isBlank(vehicleInfoDTO.getTrimLevel())
                || StringUtils.isBlank(vehicleInfoDTO.getYear())
                || StringUtils.isBlank(vehicleInfoDTO.getModelCode())
                || CollectionUtils.isEmpty(vehicleInfoDTO.getEcuList())) {
            log.error("sync vehicle miss necessary params. vehicleInfoDTO=[{}]", JSON.toJSONString(vehicleInfoDTO));
            return false;
        }

        return true;
    }

    private void saveVehicleInfo(SyncVehicleInfoDTO vehicleInfoDTO, Long vehicleTypeId) {
        DVehicleInfoEntity entity = dVehicleInfoDao.selectOneByVin(vehicleInfoDTO.getVin());
        Date now = new Date();
        if (Objects.isNull(entity)) {
            entity = new DVehicleInfoEntity();
            entity.setCreateTime(now);
            entity.setVehicleTypeId(vehicleTypeId);
            entity.setVin(vehicleInfoDTO.getVin());
            entity.setProductionDate(vehicleInfoDTO.getFactoryDate());
            entity.setPlaceOfProduction(vehicleInfoDTO.getPlaceOfProductionCode());
            entity.setRegionCode(vehicleInfoDTO.getRegionCode());
            entity.setCityCode(vehicleInfoDTO.getCityCode());
            entity.setSource(Constant.VEHICLE_SOURCE.TSP_SYNC.getCode());
            dVehicleInfoDao.insert(entity);
        } else {
            entity.setUpdateTime(now);
            entity.setVehicleTypeId(vehicleTypeId);
            entity.setProductionDate(vehicleInfoDTO.getFactoryDate());
            entity.setPlaceOfProduction(vehicleInfoDTO.getPlaceOfProductionCode());
            entity.setRegionCode(vehicleInfoDTO.getRegionCode());
            entity.setCityCode(vehicleInfoDTO.getCityCode());
            dVehicleInfoDao.updateById(entity);
        }
    }

    private void updateVehicleType(DVehicleTypeEntity entity) {
        dVehicleTypeDao.updateEcuList(entity);
    }

}
