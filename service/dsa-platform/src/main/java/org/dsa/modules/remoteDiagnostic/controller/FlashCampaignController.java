package org.dsa.modules.remoteDiagnostic.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpStatus;
import org.apache.shiro.SecurityUtils;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.R;
import org.dsa.modules.remoteDiagnostic.entity.FlashCampaignEcuEntity;
import org.dsa.modules.remoteDiagnostic.entity.FlashCampaignEntity;
import org.dsa.modules.remoteDiagnostic.entity.RemoteVehicleVersionEntity;
import org.dsa.modules.remoteDiagnostic.service.FlashCampaignIService;
import org.dsa.modules.remoteDiagnostic.service.RemoteVehicleVersionService;
import org.dsa.modules.remoteDiagnostic.vo.Flash.FlashCampaignAuditVo;
import org.dsa.modules.remoteDiagnostic.vo.Flash.FlashCampaignPageVo;
import org.dsa.modules.remoteDiagnostic.vo.Flash.FlashCampaignVo;
import org.dsa.modules.remoteDiagnostic.vo.Flash.FlashVehicleCheckVo;
import org.dsa.modules.sys.entity.DisplayUserEntity;
import org.dsa.modules.sys.entity.FileEntity;
import org.dsa.modules.vehicle.entity.VehicleInfoEntity;
import org.dsa.modules.vehicle.service.VehicleService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;

@Slf4j
@RestController
@RequestMapping("/flash")
public class FlashCampaignController {

    @Resource
    VehicleService VehicleService;

    @Resource
    FlashCampaignIService flashCampaignIService;

    @Resource
    RemoteVehicleVersionService remoteVehicleVersionService;

    @RequestMapping("/campaign/page")
    public R campaignPage(@RequestBody FlashCampaignPageVo vo){
        //分页请求 可刷文件列表
        Page<FlashCampaignEntity> result =  flashCampaignIService.queryPage(vo);

        return R.ok().put("data", result);
    }

    @RequestMapping("/campaign/{id}")
    public R campaignDetail(@PathVariable("id") Long id){
        FlashCampaignEntity result = flashCampaignIService.getById(id);
        return R.ok().put("data", result);
    }

    @RequestMapping("/vehicle")
    public R vehicle(@RequestBody FlashVehicleCheckVo re){
        VehicleInfoEntity vehicleInfo = null;

        //根据VIN校验车辆信息 不存在或者不全抛异常给客户端
        if(StrUtil.isBlank(re.getVin())){
            return R.error(Constant.Msg.CHECK_INPUT.getCode(), Constant.Msg.CHECK_INPUT.getMessage());
        }

        try {
            vehicleInfo = VehicleService.getVehicleInfoByVin(re.getVin());
        }catch (Exception ex){
            return R.error(Constant.Msg.CHECK_VIN.getCode(), Constant.Msg.CHECK_VIN.getMessage());
        }

        RemoteVehicleVersionEntity vehicleVersionEntity = remoteVehicleVersionService.getInfo(re.getVehicleVersionId());
        if(null == vehicleVersionEntity || vehicleVersionEntity.getStatus() != 3){
            return R.error(Constant.Msg.ECU_VEH_VERSION_STATUS.getCode(), Constant.Msg.ECU_VEH_VERSION_STATUS.getMessage());
        }

        Long vehicleTypeId = vehicleVersionEntity.getTypeId();

        if(vehicleTypeId != vehicleInfo.getVehicleTypeId()){
            return R.error(Constant.Msg.VEH_VERSION_MISS_MATCH.getCode(), Constant.Msg.VEH_VERSION_MISS_MATCH.getMessage());
        }

        return R.ok().put("data", vehicleInfo);
    }

    @RequestMapping("/upload")
    public R fileUpload(@RequestParam("file") MultipartFile mFile){

        if (!mFile.getOriginalFilename().endsWith(".zip")) {
            return R.error("请导入zip类型的文件");
        }

        try {
            String path = flashCampaignIService.upload(mFile);
            String md5Hex = DigestUtils.md5Hex(new FileInputStream(path));

            HashMap<String, String> map = new HashMap<>();
            map.put("path", path);
            map.put("name", mFile.getOriginalFilename());
            map.put("hash", md5Hex);
            return R.ok().put("data", map);
        }catch (Exception ex){
            return R.error(Constant.Msg.VERSION_FILE_ERROR);
        }
    }

    @RequestMapping("/campaign/download")
    public void campaignDownload(@RequestBody FileEntity fileEntity, HttpServletRequest req, HttpServletResponse response){
        InputStream ips = null;
        OutputStream ops = null;

        try {
            File file = new File(fileEntity.getUrl());
            ips =  new FileInputStream(file);
            ops = response.getOutputStream();
            response.setContentType("application/x-download");
            response.setHeader("Content-Disposition", "attachment;filename=utf-8'zh_cn'" + URLEncoder.encode(fileEntity.getName(), "UTF-8"));
            IOUtils.copy(ips, ops);
            ips.close();
            ops.flush();
            file.delete();
        } catch (IOException e) {
            log.error("下载文件出错："+e);
        } finally {
            try {
                if(ips != null ) {
                    ips.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if(ops != null) {
                    ops.flush();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    @RequestMapping("/campaign/save")
    public R campaignSave(@Valid @RequestBody FlashCampaignVo vo){
        FlashCampaignEntity entity = null;

        //整车版本不存在或者未释放
        RemoteVehicleVersionEntity vehicleVersionEntity = remoteVehicleVersionService.getInfo(vo.getVehicleVersionId());
        if(null == vehicleVersionEntity || vehicleVersionEntity.getStatus() != 3){
            return R.error(Constant.Msg.ECU_VEH_VERSION_STATUS.getCode(), Constant.Msg.ECU_VEH_VERSION_STATUS.getMessage());
        }

        VehicleInfoEntity vehicleInfo = null;
        try {
            vehicleInfo = VehicleService.getVehicleInfoByVin(vo.getVin());
        }catch (Exception ex){
            return R.error(Constant.Msg.CHECK_VIN.getCode(), Constant.Msg.CHECK_VIN.getMessage());
        }

        //这里要做车型判断
        Long vehicleTypeId = vehicleVersionEntity.getTypeId();

        if(vehicleTypeId != vehicleInfo.getVehicleTypeId()){
            return R.error(Constant.Msg.VEH_VERSION_MISS_MATCH.getCode(), Constant.Msg.VEH_VERSION_MISS_MATCH.getMessage());
        }

        entity =  flashCampaignIService.queryCampaign(vo.getVin(), vo.getVehicleVersionId());
        if(null == entity){
            entity = new FlashCampaignEntity();
            entity.setVin(vo.getVin());
            entity.setVehicleVersionId(vo.getVehicleVersionId());
            entity.setCreatedAt(new Date());
            entity.setUpdatedAt(new Date());
            DisplayUserEntity user = getUser();

            if(null != user && null != user.getUserInfo()){
                entity.setUserId(Long.valueOf(user.getUserInfo().getUserId()));
            }
            entity.setVehicleVersion(vehicleVersionEntity.getVersionName());
            flashCampaignIService.saveCampaign(entity);
        }
        FlashCampaignEcuEntity ecuEntity = flashCampaignIService.queryEcuVersion(entity.getId(), vo.getEcuId(), vo.getEcuVersionId());
        if(ecuEntity == null){
            flashCampaignIService.saveEcuVersion(entity.getId(), vo.getEcuId(), vo.getEcuVersionId());
        }
        return R.ok();
    }

    @RequestMapping("/campaign/update")
    public R campaignUpdate(@RequestBody FlashCampaignVo vo){
        FlashCampaignEntity entity = new FlashCampaignEntity();
        BeanUtil.copyProperties(vo, entity);
        if(!getUserName().equals(Constant.SUPER_ADMIN_USER_NAME)){
            entity.setUserId(getUserId());
        }

        flashCampaignIService.saveCampaign(entity);
        return R.ok();
    }

    @RequestMapping("/campaign/audit")
    public R campaignAudit(@RequestBody FlashCampaignAuditVo vo){
        if(vo.getStatus() ==0 ){
            flashCampaignIService.enabledById(vo.getCampaignId(), getUserId());
        }else{
            flashCampaignIService.disabledById(vo.getCampaignId(), getUserId());
        }
        return R.ok();
    }

    protected DisplayUserEntity getUser() {
        if(SecurityUtils.getSubject().getPrincipal()==null) {
            new RRException("invalid token", HttpStatus.SC_UNAUTHORIZED);
        }
        return (DisplayUserEntity) SecurityUtils.getSubject().getPrincipal();
    }

    protected Long getUserId() {
        return Long.valueOf(getUser().getUserInfo().getUserId());
    }

    protected String getUserName() {
        return getUser().getUserInfo().getUsername();
    }
}
