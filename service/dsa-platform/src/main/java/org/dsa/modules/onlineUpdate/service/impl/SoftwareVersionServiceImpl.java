package org.dsa.modules.onlineUpdate.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.dsa.common.exception.RRException;
import org.dsa.common.utils.Constant;
import org.dsa.common.utils.PageUtils;
import org.dsa.common.utils.Query;
import org.dsa.common.utils.ShiroUtils;
import org.dsa.modules.onlineUpdate.dao.*;
import org.dsa.modules.onlineUpdate.dto.RuleDto;
import org.dsa.modules.onlineUpdate.dto.SoftwareVersionDto;
import org.dsa.modules.onlineUpdate.entity.*;
import org.dsa.modules.onlineUpdate.po.SoftwareVersionPo;
import org.dsa.modules.onlineUpdate.service.ComponentVersionService;
import org.dsa.modules.onlineUpdate.service.RuleService;
import org.dsa.modules.onlineUpdate.service.SoftwareVersionService;
import org.dsa.modules.onlineUpdate.utils.VersionRecordUtils;
import org.dsa.modules.onlineUpdate.vo.CascadeOptions;
import org.dsa.modules.onlineUpdate.vo.CompVGroupVo;
import org.dsa.modules.onlineUpdate.vo.ComponentVersionVo;
import org.dsa.modules.onlineUpdate.vo.SoftwareVersionVo;
import org.dsa.modules.sys.service.SysLocaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service("softwareVersionService")
public class SoftwareVersionServiceImpl extends ServiceImpl<SoftwareVersionDao, SoftwareVersionEntity> implements SoftwareVersionService {

    @Autowired
    private SoftwareDao softwareDao;

    @Autowired
    private ComponentDao componentDao;
    @Autowired
    private SysLocaleService sysLocaleService;
    @Autowired
    private ComponentVersionService componentVersionService;
    @Autowired
    private SwVCompVDao swVCompVDao;
    @Autowired
    private SoftwareEntryDao softwareEntryDao;
    @Autowired
    private RuleService ruleService;

    @Value("${project.url}")
    private String projectUrl;
    @Value("${file.component.url}")
    private String componentPath;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Integer softwareId = MapUtil.getInt(params, "softwareId");
        IPage<SoftwareVersionEntity> page = this.page(
                new Query<SoftwareVersionEntity>().getPage(params),
                new QueryWrapper<SoftwareVersionEntity>()
                        .eq(ObjectUtil.isNotNull(softwareId), "software_id", softwareId)
        );
        return new PageUtils(page);
    }

    @Override
    public SoftwareVersionVo info(Integer id) {
        SoftwareVersionEntity softwareVersion = this.baseMapper.selectById(id);
        if (ObjectUtil.isNull(softwareVersion)) {
            throw new RRException(Constant.Msg.DATA_INFO_NULL_ERROR);
        }
        SoftwareVersionVo softwareVersionVo = new SoftwareVersionVo();
        BeanUtil.copyProperties(softwareVersion, softwareVersionVo);
        List<CompVGroupVo> groups = swVCompVDao.selectBySVersionId(softwareVersion.getId());
        softwareVersionVo.setGroups(groups);
        LambdaQueryWrapper<SoftwareEntryEntity> infoQuery = new LambdaQueryWrapper<SoftwareEntryEntity>()
                .eq(SoftwareEntryEntity::getSoftwareId, softwareVersion.getSoftwareId());
        SoftwareEntryEntity softwareEntryEntity = softwareEntryDao.selectOne(infoQuery);
        softwareVersionVo.setComponentId(softwareEntryEntity.getComponentId());
        return softwareVersionVo;
    }

    @Transactional
    @Override
    public void saveOrUpdate2(SoftwareVersionDto entity) {
        //参数校验
        checkParam(entity);
        if (ObjectUtil.isNull(entity.getId())) {
            entity.setUuid(UUID.randomUUID().toString());
            updateVersionType(entity);
            this.baseMapper.insert(entity);
            // 插入版本依赖
            insertSwVCompV(entity);
            // 插入版本操作记录
            VersionRecordUtils.saveOperateRecord(entity.getUuid(),Constant.editType.ADD.getCode(),entity.getStatus());
        } else {
            updateVersionType(entity);
            this.baseMapper.updateById(entity);
            if (!entity.getGroups().isEmpty()) {
                // 删除原始依赖
                swVCompVDao.delete(new QueryWrapper<SwVCompVEntity>().eq("software_version_id", entity.getId()));
                // 插入新依赖
                insertSwVCompV(entity);
            }
            // 插入版本操作记录
            VersionRecordUtils.saveOperateRecord(entity.getUuid(), Constant.editType.EDIT.getCode(),entity.getStatus());
        }
    }



    private void updateVersionType(SoftwareVersionDto entity) {
        // 如果选择了强制更新的依赖，则将所有选择的依赖变为强制更新,将软件版本的更新类型设置为强制更新类型
        List<Long> componentVersionIds = entity.getGroups().stream().map(SwVCompVEntity::getComponentVersionId).collect(Collectors.toList());
        Boolean existRequiredComponent = componentVersionService.hasRequiredComponent(componentVersionIds);
        if (existRequiredComponent) {
            componentVersionService.updateRequiredDependency(componentVersionIds);
            entity.setUpdateType(Constant.SOFTWARE_UPDATE_TYPE.FORCE_UPDATE.getCodeInt());
        } else {
            entity.setUpdateType(Constant.SOFTWARE_UPDATE_TYPE.OPTIONAL_UPDATE.getCodeInt());
        }
    }

    private void insertSwVCompV(SoftwareVersionDto entity) {
        entity.getGroups().forEach(rel -> {
            rel.setSoftwareId(entity.getSoftwareId());
            rel.setSoftwareVersionId(entity.getId());
            swVCompVDao.insert(rel);
        });
        // 入口只和软件组件绑定与版本无关
        LambdaQueryWrapper<SoftwareEntryEntity> deleteQuery = new LambdaQueryWrapper<SoftwareEntryEntity>().eq(SoftwareEntryEntity::getSoftwareId, entity.getSoftwareId());
        boolean exists = softwareEntryDao.exists(deleteQuery);
        if (exists){
            softwareEntryDao.delete(deleteQuery);
        }
        softwareEntryDao.insert(SoftwareEntryEntity.builder()
                .softwareId(entity.getSoftwareId())
                .componentId(entity.getComponentId())
                .build());
    }


    @Override
    public List<SoftwareVersionPo.ComponentInfo> getRelatedCompVersion(Long softwareVersionId, String locale, Long oldSoftwareVersion) {

        List<SoftwareVersionPo.ComponentInfo> related = new ArrayList<>(16);
        SoftwareVersionEntity softwareVersionEntity = this.baseMapper.selectById(softwareVersionId);
        // 要返回软件版本的完整组件信息——从初始版本到当前版本的所有组件的最新版本
        List<ComponentVersionVo> relatedList = swVCompVDao.selectAllComponentVersion(softwareVersionEntity.getSoftwareId(), softwareVersionEntity.getVersionNumber());
        // 从软件入口信息表获取当前版本的入口
        SoftwareEntryEntity softwareEntryEntity = softwareEntryDao.selectOne(new QueryWrapper<SoftwareEntryEntity>().lambda().eq(SoftwareEntryEntity::getSoftwareId, softwareVersionEntity.getSoftwareId()));
        if (softwareEntryEntity == null) {
            throw new RRException(Constant.Msg.EntryNullError);
        }
        relatedList.forEach(componentVersionVo -> {
            componentVersionVo = componentVersionService.queryVoByCVersionId(componentVersionVo.getId());
            String releaseNote = getLocale(componentVersionVo.getReleaseNoteTi()==null?"":componentVersionVo.getReleaseNoteTi(), locale);
            String url = projectUrl + componentPath + "?componentId=" + componentVersionVo.getComponentId() + "&componentVersionNumber=" + componentVersionVo.getId();
            // componentId修改为code
            List<SoftwareVersionPo.ComponentVersionDependency> versionRelatedList = componentVersionService.getRelatedByVersionId(componentVersionVo.getId());
            related.add(
                    SoftwareVersionPo.ComponentInfo.builder()
                            .componentId(Long.valueOf(componentVersionVo.getCode()))
                            .componentName(componentVersionVo.getComponentName())
                            .componentVersionNumber(componentVersionVo.getId())
                            .componentVersionName(componentVersionVo.getVersionName())
                            // oldSoftwareVersion==0 初次安装，所有组件设置为强制为0
                            .updateType(oldSoftwareVersion==0?0:componentVersionVo.getUpdateType())
                            .url(url)
                            .isEntry(ObjectUtil.equal(softwareEntryEntity.getComponentId(), componentVersionVo.getComponentId()) ? 1 : 0)
                            .fileSize(componentVersionVo.getFileSize())
                            .fileMd5(componentVersionVo.getFileMd5())
                            .orderNumber(componentVersionVo.getPriority() == null ? 0 : componentVersionVo.getPriority())
                            .releaseNotes(releaseNote)
                            .dependencies(versionRelatedList)
                            .build());
        });
        return related;
    }


    private String getLocale(String key, String locale) {
        return sysLocaleService.getLocale(key, locale);
    }

    private void checkParam(SoftwareVersionDto entity) {

        if (ObjectUtil.isNull(entity.getVersionName().trim())) {
            throw new RRException(Constant.Msg.VERSION_BLANK_ERROR);
        }
        if (ObjectUtil.isNull(entity.getUpdateType())) {
            throw new RRException(Constant.Msg.UNKNOWN_COMPONENT_UPDATE_TYPE_ERROR);
        }
        if (ObjectUtil.isNull(entity.getSoftwareId())) {
            throw new RRException(Constant.Msg.VERSION_NO_BIND_ERROR);
        }
        if (ObjectUtil.isNull(entity.getLastUpdateTime())) {
            throw new RRException(Constant.Msg.SOFTWARE_LAST_UPDATE_DATE_ERROR);
        }
        if (ObjectUtil.isNull(entity.getLastUpdateTipDays())) {
            throw new RRException(Constant.Msg.SOFTWARE_LAST_UPDATE_TIPS_DAYS_ERROR);
        }
        if (entity.getLastUpdateTipDays() < 1) {
            throw new RRException(Constant.Msg.SOFTWARE_LAST_UPDATE_TIPS_DAYS_ERROR);
        }
        if (ObjectUtil.isNull(entity.getIsCross())) {
            throw new RRException(Constant.Msg.ISCROSS_NULL_ERROR);
        }
        // 状态校验
        if (!Arrays.asList(Constant.VERSION_STATUS_CHECK_ARR).contains(entity.getStatus())) {
            throw new RRException(Constant.Msg.UNKNOWN_VERSION_STATUS_ERROR);
        }
        // 软件版本状态更改校验
        if (ObjectUtil.isNotNull(entity.getId())) {
            //组件版本状态(变化):开发->测试, 测试->开发,测试->释放,测试->拒绝,释放->拒绝
            SoftwareVersionEntity info = this.baseMapper.selectById(entity.getId());
            if (Constant.VERSION_STATUS.DEV.getCodeInt().equals(info.getStatus()) && !Constant.VERSION_STATUS.DEV.getCodeInt().equals(entity.getStatus())) {
                if (!Constant.VERSION_STATUS.TEST.getCodeInt().equals(entity.getStatus())) {
                    throw new RRException(Constant.Msg.VERSION_DEV_STATUS_ERROR);
                }
            }
        }
        // 软件状态更改为释放时，校验是否包含组件版本
        if (Constant.VERSION_STATUS.RELEASE.getCodeInt().equals(entity.getStatus())) {
            if (entity.getGroups().size()==0) {
                throw new RRException(Constant.Msg.SOFTWARE_RELEASE_ERROR);
            }
        }
    }

    @Override
    public Map<String, Object> querySoftwareEntry(Long softwareId) {
        LambdaQueryWrapper<SoftwareEntity> sQuery = new QueryWrapper<SoftwareEntity>().lambda().eq(SoftwareEntity::getId, softwareId);
        boolean exists = softwareDao.exists(sQuery);
        if (!exists) {
            throw new RRException(Constant.Msg.DATA_INFO_NULL_ERROR);
        }
        // 软件版本关联的组件版本
        LambdaQueryWrapper<SwVCompVEntity> sIdQuery = new QueryWrapper<SwVCompVEntity>().lambda().eq(SwVCompVEntity::getSoftwareId, softwareId);
        List<SwVCompVEntity> swVCompVEntities = swVCompVDao.selectList(sIdQuery);
        // 查找上级版本的入口
        SoftwareEntryEntity softwareEntryEntity = softwareEntryDao.selectOneBySoftwareId(softwareId);
        List<CascadeOptions> history = new ArrayList<>();
        Map<Long, Boolean> booleanMap = new HashMap<>();
        // 选择组件名称
        swVCompVEntities.forEach(sc -> {
            Boolean exist = booleanMap.getOrDefault(sc.getComponentId(), false);
            if (!exist){
                // 组件名称
                ComponentEntity componentEntity = componentDao.selectById(sc.getComponentId());
                history.add(CascadeOptions.builder().label(componentEntity.getName()).value(sc.getComponentId()).build());
                booleanMap.put(sc.getComponentId(), true);
            }
        });
        return new HashMap<String, Object>() {{
            put("history", history);
            put("entry", softwareEntryEntity);
        }};
    }

    @Override
    public SoftwareVersionEntity getOldestVersionBySoftwareId(Long id) {
        return this.baseMapper.getOldestVersionBySoftwareId(id);
    }

    @Override
    public Map<String, Object> queryInfoBySoftwareVersionId(Long versionId) {
        // 版本详情
        SoftwareVersionEntity info = this.baseMapper.selectById(versionId);
        //版本关联
        List<CompVGroupVo> groupVos = swVCompVDao.selectBySVersionId(versionId);
        // 规则信息
        RuleDto ruleDto = ruleService.queryBySwVersionId(versionId);
        // 版本操作记录
        List<VersionRecordEntity> records = VersionRecordUtils.queryRecordByUUID(info.getUuid());
        Map<String,Object> res = MapUtil.newHashMap();
        res.put("version", info);
        res.put("groups", groupVos);
        res.put("rule", ruleDto);
        res.put("records", records);
        return res;
    }

    @Override
    public void copyVersionByVersionId(Long versionId) {
        // 复制组件版本， 复制组件的基本信息
        SoftwareVersionEntity softwareVersion = this.baseMapper.selectById(versionId);
        LambdaQueryWrapper<SwVCompVEntity> queryWrapper = Wrappers.<SwVCompVEntity>lambdaQuery()
                .eq(SwVCompVEntity::getSoftwareVersionId,softwareVersion.getId());
        List<SwVCompVEntity> groupVos = swVCompVDao.selectList(queryWrapper);
        // 初始化信息
        softwareVersion.setId(null);
        softwareVersion.setVersionName(softwareVersion.getVersionName()+" copy");
        softwareVersion.setStatus(Constant.VERSION_STATUS.DEV.getCodeInt());
        softwareVersion.setVersionNumber(null);
        softwareVersion.setCreatedAt(null);
        softwareVersion.setCreatedUserId(ShiroUtils.getUserId());
        softwareVersion.setUpdatedAt(null);
        softwareVersion.setUpdatedUserId(null);
        this.baseMapper.insert(softwareVersion);
        // 插入版本关联
        groupVos.forEach(g->{
            g.setId(null);
            g.setSoftwareVersionId(softwareVersion.getId());
            swVCompVDao.insert(g);
        });
        VersionRecordUtils.saveOperateRecord(softwareVersion.getUuid(),Constant.editType.ADD.getCode(),softwareVersion.getStatus() );
    }
}