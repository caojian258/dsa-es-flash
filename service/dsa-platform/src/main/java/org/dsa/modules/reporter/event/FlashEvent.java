package org.dsa.modules.reporter.event;

import org.dsa.modules.reporter.vo.api.FlashLogReVo;
import org.dsa.modules.reporter.vo.api.SyncFlashLogRevo;
import org.springframework.context.ApplicationEvent;

/***
 * SP metadata更新事件
 */
public class FlashEvent extends ApplicationEvent {

    private SyncFlashLogRevo vo;

    public FlashEvent(Object source) {
        super(source);
    }

    public FlashEvent(Object source, SyncFlashLogRevo vo){
        super(source);
        this.vo = vo;
    }

    public SyncFlashLogRevo getVo(){
        return vo;
    }
}
