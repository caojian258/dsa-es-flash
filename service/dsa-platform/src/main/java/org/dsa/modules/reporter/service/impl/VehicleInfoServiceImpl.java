package org.dsa.modules.reporter.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dsa.modules.reporter.constant.VehicleConstant;
import org.dsa.modules.reporter.dao.VehicleInfoMapper;
import org.dsa.modules.reporter.entity.VehicleInfo;
import org.dsa.modules.reporter.service.VehicleInfoService;
import org.dsa.modules.reporter.vo.VehInfoPageReVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VehicleInfoServiceImpl implements VehicleInfoService {

    @Autowired
    VehicleInfoMapper infoMapper;

    @Override
    public Page<VehicleInfo> selectPage(VehInfoPageReVo vo) {
        Page<VehicleInfo> page = new Page<>(vo.getPageNo(), vo.getPageSize());
        OrderItem oi = new OrderItem(vo.getSortColumn(), vo.getSortAsc());
        page.addOrder(oi);
        LambdaQueryWrapper<VehicleInfo> qw =  Wrappers.<VehicleInfo>lambdaQuery();

        Page<VehicleInfo> resultPage = infoMapper.selectPage(page, qw);
        return resultPage;
    }

    @Override
    public VehicleInfo getOneByVinCode(String vinCode) {
        return infoMapper.getInfo(vinCode);
    }

    @Override
    public VehicleInfo checkVehicleType(Long vehicleType) {
        return infoMapper.checkVehicleType(vehicleType);
    }

    @Override
    public void addOne(String vinCode, Long vehicleTypeId) {
        VehicleInfo vehicleInfo = new VehicleInfo();
        vehicleInfo.setVin(vinCode);
        vehicleInfo.setVehicleTypeId(vehicleTypeId);
        vehicleInfo.setSource(VehicleConstant.AsMaintained);
        infoMapper.insert(vehicleInfo);
    }

}
