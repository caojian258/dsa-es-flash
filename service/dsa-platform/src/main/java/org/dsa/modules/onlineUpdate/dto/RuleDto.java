package org.dsa.modules.onlineUpdate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.dsa.modules.onlineUpdate.entity.RuleEntity;

import java.util.List;


@EqualsAndHashCode(callSuper = true)
@Data
public class RuleDto extends RuleEntity {
    private List<Long> userIds;
    private List<Long> workshopIds;

}
