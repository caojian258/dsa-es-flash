package org.dsa.modules.remoteDiagnostic.dto.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class DiagnosticConditionDto implements Serializable {

    private String conditionName;

    private String conditionCode;

    private Integer conditionVal;

    private String unity;

    private String minVal;

    private String maxVal;

    private String eqVal;

    private String inVal;
}
